package bo.pages;

import common.Config;
import common.Constants;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import utilities.Driver;
import java.util.logging.Logger;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by mnabizadeh on 12/18/18.
 */

public class PriceCheck {
    private Driver driver;
    private WebDriver webDriver;
    private final Logger LOGGER = Logger.getLogger(PriceCheck.class.getName());

    public PriceCheck(Driver driver){
        this.driver = driver;
        webDriver = driver.getDriver();
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(className = "login_btn")
    private static WebElement backofficeLogin;

    @FindBy (className="yw-toggle-advanced-search")
    private static WebElement switchSearchMode;

    @FindBy(xpath = "//input[contains(@id,'1-real')][contains(@class,'z-bandbox-input')]")
    private static WebElement backofficeArticleDescription;

    @FindBy(xpath = "//span[contains(@class,'ye-com_hybris_cockpitng_editor_defaultenum')]")
    private static WebElement backofficeCustomerPriceList;

    @FindBy(xpath = "//span[text()='ug-0001'][@class='z-comboitem-text']")
    private static WebElement ug0001;

    public static final By backofficeUserBy = By.name("j_username");
    public static final By backofficePasswordBy = By.name("j_password");

    private static final By CustomerPLBy =By.xpath("//span[contains(@class,'ye-com_hybris_cockpitng_editor_defaultenum')]");


    @FindBy(xpath = "//span[text()='PBX A/T Hardcore [34301] - Discount Tire Product Catalog : Online']")
    private static WebElement onlineArticle;

    @FindBy(xpath = "//span[contains(text(),'$')]")
    private static WebElement onlinePrice;

    @FindBy(className = "yw-textsearch-searchbutton")
    private static WebElement search;

    @FindBy(xpath = "//span[@class='z-label'] [text()='PBX A/T Hardcore [34301] - Discount Tire Product Catalog : Online']")
    private static WebElement dropDown;

    public final String BACKOFFICE_ARTICLE_DESCRIPTION = "Article Description";
    public final String BACKOFFICE_CUSTOMER_PRICE_LIST = "Customer Price List";
    public final String UG_0001 = "ug-0001";
    public final String ONLINE_ARTICLE = "Online Article";
    public final String NEW_PRICE = "Price";
    public final String SEARCH = "Search";
    public final String DROPDOWN = "DropDown";
    private static final String SWITCH_SEARCH_MODE = "Switch search mode";

    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started");
        switch (elementName) {
            case SWITCH_SEARCH_MODE:
                return switchSearchMode;
            case BACKOFFICE_ARTICLE_DESCRIPTION:
                return backofficeArticleDescription;
            case BACKOFFICE_CUSTOMER_PRICE_LIST:
                return webDriver.findElements(CustomerPLBy).get(1);
            case UG_0001:
                return ug0001;
            case  ONLINE_ARTICLE:
                return onlineArticle;
            case NEW_PRICE:
                return onlinePrice;
            case SEARCH:
                return search;
            case DROPDOWN:
                return dropDown;
            default:
                Assert.fail("FAIL: Could not find icon that matched string passed from step");
                return null;
        }
    }

    /**
     * Logs in to Backoffice using UN and PW
     */
    public void loginToBackoffice() {
        LOGGER.info("loginToBackoffice started");
        WebElement boUsername = webDriver.findElement(backofficeUserBy);
        boUsername.sendKeys(Config.getBackofficeUserName());
        driver.waitForMilliseconds(Constants.ONE_THOUSAND);
        WebElement boPassword = webDriver.findElement(backofficePasswordBy);
        boPassword.sendKeys(Config.getBackofficePassword());
        driver.waitForMilliseconds(Constants.ONE_THOUSAND);
        driver.waitForElementClickable(backofficeLogin);
        driver.waitForMilliseconds(Constants.ONE_THOUSAND);
        backofficeLogin.click();
        driver.waitForPageToLoad();
        LOGGER.info("loginToBackoffice completed");
    }
}
