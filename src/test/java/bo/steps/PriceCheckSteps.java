package bo.steps;

import cucumber.api.Scenario;
import cucumber.api.java.en.When;
import cucumber.api.java.en.Then;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import sap.pages.MerchandiseCycle;
import utilities.Driver;
import bo.pages.PriceCheck;
import sap.pages.CommonActions;

/**
 * Created by mnabizadeh on 12/18/18.
 */

public class PriceCheckSteps {

    private Driver driver;
    private WebDriver webDriver;
    private Scenario scenario;
    private PriceCheck priceCheck;
    private CommonActions commonActions;
    private MerchandiseCycle merchandiseCycle;

    public PriceCheckSteps(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        priceCheck = new PriceCheck(driver);
        commonActions = new CommonActions(driver);
        merchandiseCycle = new MerchandiseCycle(driver);
    }


    @When("^I login with Backoffice Username and Password$")
    public void i_login_with_Backoffice_Username_and_Password() throws Throwable {
        priceCheck.loginToBackoffice();
    }

    @When("^I expand \"([^\"]*)\"$")
    public void i_expand(String text) throws Throwable {
        commonActions.clickOnText(text);
    }

    @When("^I select the \"([^\"]*)\" button on the backoffice price rows page$")
    public void i_select_the_button_on_the_backoffice_price_rows_page(String elementName) throws Throwable {
        commonActions.click(priceCheck.returnElement(elementName));
    }


    @When("^I enter \"([^\"]*)\" information on \"([^\"]*)\" in the backoffice price rows page$")
    public void i_enter_information_on_in_the_backoffice_price_rows_page(String value, String element) throws Throwable {
        commonActions.sendKeys(priceCheck.returnElement(element), value);
    }

    @When("^I select \"([^\"]*)\" information from \"([^\"]*)\" dropdown$")
    public void i_select_information_from_dropdown(String value, String element) throws Throwable {
        commonActions.clickAndSelectOnExactText(value, priceCheck.returnElement(element));
    }

    @When("^I click on \"([^\"]*)\" dropdown$")
    public void i_click_on_dropdown(String elementName) throws Throwable {
        commonActions.click((priceCheck.returnElement(elementName)));
    }

    @When("^I verify \"([^\"]*)\" is displayed backoffice price page$")
    public void i_verify_is_displayed_backoffice_price_page(String elementName) throws Throwable {
        driver.isElementDisplayed(priceCheck.returnElement(elementName));
    }


    @Then("^I verify \"([^\"]*)\" matches \"([^\"]*)\" displayed in the backoffice page$")
    public void i_verify_matches_displayed_in_the_backoffice_page(String elementName, String text) throws Throwable {
        driver.verifyTextDisplayed(priceCheck.returnElement(elementName),text);
    }

    @Then("^I check \"([^\"]*)\" displayed on backoffice matches new price entered in sap$")
    public void i_check_displayed_on_backoffice_matches_new_price_entered_in_sap(String elementName) throws Throwable {
        String newPrice = merchandiseCycle.getNewPrice();
        newPrice = "$" + newPrice;
        driver.verifyTextDisplayed(priceCheck.returnElement(elementName),newPrice);
    }

    @Then("^I enter \"([^\"]*)\" information on \"([^\"]*)\" in the backoffice price rows page \"([^\"]*)\"$")
    public void i_enter_information_on_in_the_backoffice_price_rows_page(String value, String element, String dropDownElement) throws Throwable {
        commonActions.sendKeysDropDown(priceCheck.returnElement(element), value, priceCheck.returnElement(dropDownElement));
    }
}
