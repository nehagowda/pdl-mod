package common;

import dtc.data.ConstantsDtc;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.junit.Assert;

import java.io.File;
import java.io.RandomAccessFile;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.logging.Logger;

/**
 * Created by csahoo on 07/09/2018.
 */

public class CommonExcel {
    private final static Logger LOGGER = Logger.getLogger(CommonExcel.class.getName());

    /**
     * Saves DTC Order Number and extended assortment flag to external excel file
     *
     * @param filePath        external excel file name
     * @param transactionData Passed through as scenario data
     * @param flag            extended assortment flag
     */
    public void saveDtcOrderNumberFlagToExcel(String filePath, String transactionData, String flag) throws Exception {
        LOGGER.info("saveDtcOrderNumberFlagToExcel started");
        try {
            File excel = new File(filePath);
            FileInputStream file = new FileInputStream(excel);
            HSSFWorkbook workbook = new HSSFWorkbook(file);

            if (filePath.contains(ConstantsDtc.EXTENDED_ASSORTMENT)) {
                HSSFSheet sheet = workbook.getSheet("test");
                int lastRowIndex = sheet.getLastRowNum();
                Row orderRow = sheet.createRow(lastRowIndex + 1);
                orderRow.createCell(2).setCellValue(transactionData);
                orderRow.createCell(3).setCellValue(flag);
            } else {
                Assert.fail("FAIL: File Path '" + filePath + "' is not a valid endpoint for this method!");
            }
            excelLockReleaseWithWritePermission(excel, file, workbook, filePath);
        } catch (IOException err) {
            Thread.sleep(Constants.FIVE); //wait for other process to unlock file

            try {
                FileLock lock = new RandomAccessFile(new File(filePath), "rw").getChannel().tryLock();
                lock.release();
                saveDtcOrderNumberFlagToExcel(filePath, transactionData, flag);
            } catch (Exception e) {
                Assert.fail("FAIL: File located at '" + filePath +
                        "' is currently locked by another process and cannot be accessed!");
            }
        }
        LOGGER.info("saveDtcOrderNumberFlagToExcel completed");
    }

    /**
     * Saves data to specified tab of external excel file
     *
     * @param filePath      External excel file name
     * @param data          Data to place in excel spreadsheet
     * @param tabName       Excel sheet tab name
     * @param append        'true' or 'false' whether to append to end of the data in the file
     */
    public void saveDataToExcel(String filePath, String data, String tabName, boolean append)
            throws Exception {
        LOGGER.info("saveDataToExcel started: " + data + " added to " + tabName + " tab of " + filePath + " file");
        int columnIndex = 0;
        saveDataToExcel(filePath, data, tabName, columnIndex, append);
        LOGGER.info("saveDataToExcel completed: " + data + " added to " + tabName + " tab of " + filePath + " file");
    }

    /**
     * Saves data to specified tab and column of external excel file
     *
     * @param filePath      External excel file name
     * @param data          Data to place in excel spreadsheet
     * @param tabName       Excel sheet tab name
     * @param columnIndex   Zero-based index of column to populate
     * @param append        'true' or 'false' whether to append to end of the data in the file
     *
     * @throws InterruptedException
     */
    public void saveDataToExcel(String filePath, String data, String tabName, int columnIndex, boolean append)
            throws InterruptedException {
        LOGGER.info("saveDataToExcel started: " + data + " added to " + " column index " + columnIndex + " of " +
                tabName + " tab of " + filePath + " file");
        int rowIndex = 1;

        try {
            File excel = new File(filePath);
            FileInputStream file = new FileInputStream(excel);
            HSSFWorkbook workbook = new HSSFWorkbook(file);
            HSSFSheet sheet = workbook.getSheet(tabName);
            boolean found = true;

            if (append) {
                do {
                    HSSFRow row = sheet.getRow(rowIndex);
                    found = true;
                    try {
                        HSSFCell cell = row.getCell(columnIndex);
                        if (cell != null) {
                            String value = cell.getStringCellValue();
                            if (!value.trim().equals("")) {
                                rowIndex++;
                                found = false;
                            }
                        }
                    } catch (Exception e) { }
                } while(!found);
            }

            Row orderRow = sheet.getRow(rowIndex);

            if (orderRow == null)
                orderRow = sheet.createRow(rowIndex);

            Cell cell = orderRow.getCell(columnIndex);

            if (cell == null)
                cell = orderRow.createCell(columnIndex);

            cell.setCellValue(data);

            excelLockReleaseWithWritePermission(excel, file, workbook, filePath);
        } catch (IOException err) {
            Thread.sleep(Constants.FIVE); //wait for other process to unlock file

            try {
                FileLock lock = new RandomAccessFile(new File(filePath), "rw").getChannel().tryLock();
                lock.release();
                saveDataToExcel(filePath, data, tabName, append);
            } catch (Exception e) {
                Assert.fail("FAIL: File located at '" + filePath +
                        "' is currently locked by another process and cannot be accessed!");
            }
        }
        LOGGER.info("saveDataToExcel completed: " + data + " added to " + " column index " + columnIndex + " of " +
                tabName + " tab of " + filePath + " file");
    }

    /**
     * Gets the column index with the specified header name
     *
     * @param filePath          External excel file name
     * @param tabName           Excel sheet tab name
     * @param columnHeaderName  The name of the header
     *
     * @return
     * @throws InterruptedException
     */
    public int excelGetColumnIndex (String filePath, String tabName, String columnHeaderName) throws InterruptedException {
        LOGGER.info("saveDataToExcel started for '" + columnHeaderName + "' column of '" + tabName +
                "' tab of '" + filePath + "' excel file");
        int columnIndex = 0;
        try {
            File excel = new File(filePath);
            FileInputStream file = new FileInputStream(excel);
            HSSFWorkbook workbook = new HSSFWorkbook(file);
            HSSFSheet sheet = workbook.getSheet(tabName);
            boolean found = true;

            do {
                HSSFRow row = sheet.getRow(0);
                found = true;
                try {
                    HSSFCell cell = row.getCell(columnIndex);
                    if (cell != null) {
                        String value = cell.getStringCellValue();
                        if (!value.trim().equals(columnHeaderName)) {
                            columnIndex++;
                            found = false;
                        }
                    } else {
                        columnIndex = -1;
                    }
                } catch (Exception e) { }
            } while (!found && columnIndex >= 0);
        } catch (Exception e) {}

        LOGGER.info("saveDataToExcel completed for '" + columnHeaderName + "' column of '" + tabName +
                "' tab of '" + filePath + "' excel file");

        return columnIndex;
    }

    /**
     * If file has write permission, it opens a FileOutputSteam to the file path, locks the file Saves it,
     * releases the lock, then closes the file out stream, workbook, and file.
     *
     * @param excel
     * @param file
     * @param workbook
     * @param filePath
     */
    private void excelLockReleaseWithWritePermission(File excel, FileInputStream file, HSSFWorkbook workbook, String filePath) {
        LOGGER.info("excelLockReleaseWithWritePermission started");
        try {
            if (excel.canWrite()) {
                FileOutputStream fileOut = new FileOutputStream(filePath);
                FileChannel channel = fileOut.getChannel();
                FileLock lock = channel.lock();
                workbook.write(fileOut);
                lock.release();
                fileOut.close();
                workbook.close();
                file.close();
            } else {
                FileLock lock = file.getChannel().tryLock();
                lock.release();
            }
        } catch (IOException err) {
            LOGGER.info("Error writing file at '" + filePath + "': " + err);
        }
        LOGGER.info("excelLockReleaseWithWritePermission completed");
    }
}
