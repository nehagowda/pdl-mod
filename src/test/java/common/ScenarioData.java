package common;

import org.openqa.selenium.Cookie;
import org.openqa.selenium.WebDriver;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by aaronbriel on 11/10/16.
 */
public class ScenarioData {

    private String sessionId;
    private String currentOrderNumber;
    private String currentTransactionTotal;
    private String currentPurchaseOrderNumber;
    private String currentEmpID;
    private String currentUpdatedPrice;
    private String articleDocumentNumber;
    private String cleanedNWBCPONumber;
    private String currentIDocNumber;
    private String currentNumberOfTires;
    private String currentMatchedCompetitor;
    private boolean expediteYourExperience;
    private boolean isStaggeredProduct = false;
    private ArrayList<String> purchaseOrderNumberList = new ArrayList<>();
    private static final String SESSION_COOKIE_NAME = "JSESSIONID";
    public HashMap<String, String> poVendor = new HashMap();
    public HashMap<String, String> genericData = new HashMap();
    public LinkedHashMap<String, String[]> productInfoList = new LinkedHashMap<>();
    public ArrayList<String> cartInstantPromotionPrice = new ArrayList<>();
    public ArrayList<String> cartMailInPromotion = new ArrayList<>();
    private ArrayList<String> drivingPriorityList = new ArrayList<>();
    private boolean accountCreated;

    public ScenarioData() {
        //Clearing so that the data won't carry between scenarios and example iterations
        productInfoList.clear();
        cartInstantPromotionPrice.clear();
        cartMailInPromotion.clear();
        drivingPriorityList.clear();
        accountCreated = false;
        expediteYourExperience = false;
        isStaggeredProduct = false;
    }

    /**
     * Gets the id of the current session
     *
     * @return sessionId String of current session id
     */
    public String getDtSessionId() {
        return this.sessionId;
    }

    /**
     * Gets the default store for current tests and passes it back as a string
     *
     * @param driver WebDriver instance
     */
    public void setDtSessionId(WebDriver driver) {
        //TODO: retest when new safaridriver is stable
        if (Config.getBrowser().equalsIgnoreCase(Constants.SAFARI_BROWSER) ||
                Config.isMobile()) {
            this.sessionId = "SKIPPED_FOR_SAFARI_AND_MOBILE";
        } else {
            Cookie cookie = driver.manage().getCookieNamed(SESSION_COOKIE_NAME);
            try {
                this.sessionId = cookie.getValue();
            } catch (NullPointerException ex) {
            }
        }
    }

    /**
     * Gets the current order number
     *
     * @return String of current order number
     */
    public String getCurrentOrderNumber() {
        return currentOrderNumber;
    }

    /**
     * Sets currentOrderNumber to passed in value
     *
     * @param orderNumber value to set as currentOrderNumber's value
     */
    public void setCurrentOrderNumber(String orderNumber) {
        currentOrderNumber = orderNumber;
    }

    /**
     * Gets the current Purchase Order number
     *
     * @return String of current Purchase Order number
     */
    public String getCurrentPurchaseOrderNumber() {
        return currentPurchaseOrderNumber;
    }

    /**
     * Sets the current Purchase Order Number to passed in value
     *
     * @param savedPurchaseOrderNumber value to set as currentPurchaseOrderNumber's value
     */
    public void setCurrentPurchaseOrderNumber(String savedPurchaseOrderNumber) {
        currentPurchaseOrderNumber = savedPurchaseOrderNumber;
    }

    /**
     * Sets the current Purchase Order Number to passed in value
     *
     * @param vendor (key) to set as vendor value
     * @param po     value to set as po value
     */
    public void setPOVendorData(String vendor, String po) {
        poVendor.put(vendor, po);
    }

    /**
     * Gets the po vendor based on vendor
     *
     * @param vendor (key) vendor value
     * @return po vendor combination based on vendor
     */
    public String getPOVendorData(String vendor) {
        return poVendor.get(vendor);
    }

    /**
     * Gets the current article document number
     *
     * @return String of current article document number
     */
    public String getCurrentArticleDocumentNumber() {
        return articleDocumentNumber;
    }

    /**
     * Sets the article document number to passed in value
     *
     * @param cleanedArticleDocumentNumber value to set as articleDocumentNumber's value
     */
    public void setArticleDocumentNumber(String cleanedArticleDocumentNumber) {
        articleDocumentNumber = cleanedArticleDocumentNumber;
    }

    /**
     * Gets the current transaction total
     *
     * @return String of current transaction total
     */
    public String getCurrentTransactionTotal() {
        return currentTransactionTotal;
    }

    /**
     * Sets currentTransactionTotal to passed in value
     *
     * @param transactionTotal value set as currentTransactionTotal's value
     */
    public void setCurrentTransactionTotal(String transactionTotal) {
        currentTransactionTotal = transactionTotal;
    }

    /**
     * Gets getNWBCCurrentPONumber
     *
     * @return String of current NWBC PO number
     */
    public String getNWBCCurrentPONumber() {
        return cleanedNWBCPONumber;
    }

    /**
     * Sets cleanedNWBCPONumber to passed in value
     *
     * @param poNumber value set as cleanedNWBCPONumber's value
     */
    public void setNWBCCurrentPONumber(String poNumber) {
        cleanedNWBCPONumber = poNumber;
    }

    /**
     * sets currentIDocNumber to passed in value
     *
     * @param IDocNumber value set as currentIDocNumbers' value
     */
    public void setCurrentIDocNumber(String IDocNumber) {
        currentIDocNumber = IDocNumber;
    }

    /**
     * Gets currentIDocNumber
     *
     * @return String of current IDoc number
     */
    public String getCurrentIDocNumber() {
        return currentIDocNumber;
    }

    public void setCurrentPONumberWithVendorList(String purchaseOrderNumberWithVendor) {
        purchaseOrderNumberList.add(purchaseOrderNumberWithVendor);
    }

    public ArrayList<String> getPurchaseOrderNumberList() {
        return purchaseOrderNumberList;
    }

    /**
     * Sets the current data based on key and value
     *
     * @param key   primary key
     * @param value value to set
     */
    public void setData(String key, String value) {
        genericData.put(key, value);
    }

    /**
     * Gets the data based on Key value
     *
     * @param key (key) primary key
     * @return data based on key provided
     */
    public String getData(String key) {
        return genericData.get(key);
    }

    /**
     * Sets the current Emp ID to passed in value
     *
     * @param savedEmpID value to set as currentEmpID value
     */
    public void setCurrentEmpID(String savedEmpID) {
        currentEmpID = savedEmpID;
    }

    /**
     * Gets the current Emp ID to passed in value
     *
     */
    public String getCurrentEmpID() {

        return currentEmpID;
    }

    /**
     * Sets the current Price to passed in value
     *
     * @param savedPrice value to set as currentPrice value
     */
    public void setCurrentPrice(String savedPrice) {
        currentUpdatedPrice = savedPrice;
    }

    /**
     * Gets the current price
     *
     * @return String of current Price
     */
    public String getCurrentPrice() {

        return currentUpdatedPrice;
    }

    /**
     * Sets the current Number of Tires
     *
     * @param savedTireResults value to set as currentNumberOfTires
     */
    public void setCurrentNumberOfTires(String savedTireResults){
        currentNumberOfTires = savedTireResults;
    }

    /**
     * Gets the current number of tires
     *
     * @return String of currentNumberOfTires
     */
    public String getCurrentNumberOfTires(){
        return currentNumberOfTires;
    }

    /**
     * Sets matched competitor
     *
     * @param savedMatchedCompetitor
     */
    public void setMatchedCompetitor(String savedMatchedCompetitor){
        currentMatchedCompetitor = savedMatchedCompetitor;
    }

    /**
     * Gets the current matched competitor
     *
     * @return
     */
    public String getMatchedCompetitor(){
        return currentMatchedCompetitor;
    }

    public void setExpediteYourExperience(boolean expedite) {
        expediteYourExperience = expedite;
    }

    public boolean expediteYourExperience() {
        return expediteYourExperience;
    }

    public boolean isStaggeredProduct() {
        return isStaggeredProduct;
    }

    public void setStaggeredProduct(boolean staggeredProduct) {
        isStaggeredProduct = staggeredProduct;
    }

    /**
     * sets the "vehicle" key value in generic data as a full string: "year make model trim"
     *
     * @param year  - Vehicle year
     * @param make  - Vehicle make
     * @param model - Vehicle model
     * @param trim  - Vehicle trim
     */
    public void setVehicleData(String year, String make, String model, String trim) {
        String vehicleData = year + " " + make + " " + model + " " + trim;
        genericData.put(Constants.VEHICLE.toLowerCase(), vehicleData.trim());
    }

    /**
     * Set account created to true or false
     *
     * @param created - true / false whether an account is created
     */
    public void createAccount(boolean created) {
        accountCreated = created;
    }

    /**
     * Return true or false whether there is an account created
     *
     * @return true / false
     */
    public boolean accountCreated() {
        return accountCreated;
    }

    /**
     * Add a driving priority to the drivingPriority list
     *
     * @param drivingPriority The name of the driving priority
     */
    public void addDrivingPriority(String drivingPriority) {
        drivingPriorityList.add(drivingPriority);
    }

    /**
     * Set the entire list of driving priorities
     *
     * @param drivingPriorities List of driving priorities
     */
    public void setDrivingPriorityList(List<String> drivingPriorities) {
        drivingPriorityList.clear();
        for (String drivingPriority : drivingPriorities) {
            addDrivingPriority(drivingPriority);
        }
    }

    /**
     * Get the list of driving priorities
     *
     * @return List of driving priorities
     */
    public List<String> getDrivingPriorityList() {
        return drivingPriorityList;
    }
}