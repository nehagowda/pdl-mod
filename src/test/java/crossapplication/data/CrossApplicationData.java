package crossapplication.data;

import common.Constants;
import java.io.File;
import java.lang.reflect.Field;
import java.util.logging.Logger;

public class CrossApplicationData {

    private final Logger LOGGER = Logger.getLogger(CrossApplicationData.class.getName());

    public String environment;
    public String store;
    public String zipCode;
    public String articleNumber;
    public String articleNumber2;

    private enum CrossApplicationDataType {
        Stage,
        QA;

        public static CrossApplicationDataType crossApplicationDataForName(String environment) throws IllegalArgumentException {
            for (CrossApplicationDataType c : values()) {
                if (c.toString().equalsIgnoreCase(environment)) {
                    return c;
                }
            }
            throw crossApplicationDataNotFound(environment);
        }

        private static IllegalArgumentException crossApplicationDataNotFound(String outcome) {
            return new IllegalArgumentException(("Invalid environment [" + outcome + "]"));
        }
    }

    public CrossApplicationData() {
    }

    /**
     * Stores and returns specific environment data based on the environment passed in
     *
     * @param environment Name of environment
     * @return CrossApplicationData  Data values for environment
     */
    public CrossApplicationData getCrossApplicationData(String environment) {
        CrossApplicationDataType crossApplicationDataType = CrossApplicationDataType.crossApplicationDataForName(environment);
        CrossApplicationData crossApplicationData = new CrossApplicationData();

        switch (crossApplicationDataType) {
            case QA:
                crossApplicationData.environment = environment;
                crossApplicationData.store = "COD 36";
                crossApplicationData.zipCode = "80601";
                crossApplicationData.articleNumber = "44286";
                crossApplicationData.articleNumber2 = "30431";
                break;
            case Stage:
                crossApplicationData.environment = environment;
                crossApplicationData.store = "COD 36";
                crossApplicationData.zipCode = "80601";
                break;
        }
        return crossApplicationData;
    }

    /**
     * Returns field value for environment through reflection
     *
     * @param environment Feature to extract data field value from
     * @param dataField   The data field value to extract
     * @return String    The data field value
     * @throws Exception Access exception
     */
    public String getFieldValue(String environment, String dataField) throws Exception {
        LOGGER.info("getFieldValue started");
        CrossApplicationData crossApplicationData = new CrossApplicationData();
        CrossApplicationData environmentData = getCrossApplicationData(environment);
        Field featureField = crossApplicationData.getClass().getField(dataField);
        LOGGER.info("getFieldValue completed");
        return featureField.get(environmentData).toString();
    }

    /**
     * Returns string concatenation of environment data
     *
     * @param crossApplicationData CrossApplicationData instance
     * @return String of environment data
     */
    public String getCrossApplicationDataString(CrossApplicationData crossApplicationData) {
        LOGGER.info("getCrossApplicationDataString started");
        StringBuilder result = new StringBuilder();
        String newLine = System.getProperty(Constants.LINE_SEPARATOR_SYSTEM_PROPERTY);
        Field[] fields = crossApplicationData.getClass().getDeclaredFields();
        result.append(newLine);

        //print field names paired with their values if not null
        for (Field field : fields) {
            try {
                if (field.get(crossApplicationData) != null) {
                    result.append("  ");
                    result.append(field.getName());
                    result.append(": ");
                    result.append(field.get(crossApplicationData));
                    result.append(newLine);
                }
            } catch (IllegalAccessException ex) {
                LOGGER.info(ex.toString());
            }
        }
        LOGGER.info("getCrossApplicationDataString completed");
        return result.toString();
    }

    /**
     * This script will access each folder inside the filepath and deletes all files inside each folder.
     * <p>
     * Intentionally filepath was not parameterized, because use of this method with inaccurate file path will lead
     * to deletion of files in the location passed
     */
    public void deleteNexusScreenshots() {
        LOGGER.info("deleteNexusScreenshots started");
        String filePath = Constants.PARENT_FOLDER_NAME_SCREENSHOT;
        File dir = new File(filePath);
        File[] dirContents = dir.listFiles();

        for (File dirContent : dirContents) {
            try {
                String path = dirContent.getAbsolutePath();
                File localDir = new File(path);
                File[] localDirContents = localDir.listFiles();
                for (File localDirContent : localDirContents) {
                    localDirContent.delete();
                }
            } catch (Exception ioe) {
                ioe.printStackTrace();
            }
        }
        LOGGER.info("deleteNexusScreenshots completed");
    }

    /**
     * This method is created to call before driver.takeScreenshotAndSaveToALocation method and should have the same input parameters like
     * take screenshot method so that this method will delete existing pictures and then create new pictures
     * with driver.takeScreenshotAndSaveToALocation method.
     * <p>
     * Intentionally filepath was not parameterized, because use of this method with inaccurate file path will lead
     * to deletion of files in the location passed
     *
     * @param folderName - Pass the folder name same as folder name being passed to driver.takeScreenshotAndSaveToALocation method
     * @param fileName   - pass the file name same as folder name being passed to driver.takeScreenshotAndSaveToALocation
     */
    public void deleteSpecificFileWhichContainsName(String folderName, String fileName) {
        LOGGER.info("deleteSpecificFileWhichContainsName started for file in location: " +
                fileName + "and folder: " + folderName);
        String filePath = Constants.PARENT_FOLDER_NAME_SCREENSHOT + folderName;
        File dir = new File(filePath);
        File[] dirContents = dir.listFiles();
        try {
            for (File dirContent : dirContents) {
                String existingFileName = dirContent.getName();
                if (existingFileName.contains(fileName)) {
                    dirContent.delete();
                }
            }
        } catch (NullPointerException ne) {
            ne.printStackTrace();
        }
        LOGGER.info("deleteSpecificFileWhichContainsName completed for file in location: " +
                fileName + "and folder: " + folderName);
    }
}