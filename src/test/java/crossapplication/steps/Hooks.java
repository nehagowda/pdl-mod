package crossapplication.steps;

import common.Config;
import common.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import dtc.data.ConstantsDtc;
import sap.pages.SapUtility;
import utilities.CommonUtils;
import utilities.Driver;
import utilities.SauceUtils;
import visualtesting.pages.Applitools;

import java.util.logging.Logger;

/**
 * Created by mginevan on 7/17/18.
 */
public class Hooks {

    private Driver driver;
    private SapUtility sapUtility;
    private final Logger LOGGER = Logger.getLogger(Hooks.class.getName());


    public Hooks(Driver driver) {
        this.driver = driver;
        sapUtility = new SapUtility();
    }

    @Before
    public void beforeScenario() {
        driver.initialize();

        //Creating orders artifact for Jenkins post build step
        CommonUtils.appendFile(ConstantsDtc.ORDERS_FILE, "");
    }

    @Before
    public void printScenario(Scenario scenario) {
        CommonUtils.scenarioLoggerHeading("STARTING SCENARIO: " + getScenarioInfo(scenario, false));
    }

    @After
    public void afterScenario(Scenario scenario) throws Throwable{
        if (!Config.getBrowser().equalsIgnoreCase(Constants.NONE)) {
            //Passing test details to Saucelabs
            if (Config.getIsSaucelabs())
                SauceUtils.updateResults(scenario.getName(), driver.getRemoteSessionId(), !scenario.isFailed());

            //Embedding DT session id to cucumber report
            scenario.write("JSESSIONID:" + driver.scenarioData.getDtSessionId());

            if ((Config.getIsApplitools()) && (Applitools.eyes.getIsOpen())) {
                try {
                    Applitools.eyes.close(false);
                } finally {
                    Applitools.eyes.abortIfNotClosed();
                }
            }

            CommonUtils.scenarioLoggerHeading("COMPLETED SCENARIO: " + getScenarioInfo(scenario, true));
            if (Config.getSapTestStabilityMetricFlag().equalsIgnoreCase(Constants.ON)) {
                try {
                    sapUtility.updateTestResultForScenario
                            (driver.scenarioData.getData(SapUtility.FEATURE_FILE_NAME_KEY),
                            driver.scenarioData.getData(SapUtility.SCENARIO_NAME_KEY),
                            driver.scenarioData.getData(SapUtility.TEST_STATUS_KEY));
                } catch (Exception e) {
                    LOGGER.info("Failed to capture Metrics");
                    e.printStackTrace();
                }
            }
            driver.embedScreenshot(scenario);
            new Driver().destroyDriver();

        }
    }

    /**
     * Get the Scenario info
     *
     * @param scenario              - The Scenario object
     * @param includeTestStatus     - true or false whether to include the test status - passed or failed
     * @return String               - The text representing all the desired scenario
     */
    private String getScenarioInfo(Scenario scenario, boolean includeTestStatus) {
        String baseUrl = Config.getBaseUrl();
        if (baseUrl.isEmpty() || baseUrl.equals(Constants.HTTPS) || baseUrl.equals(Constants.HTTP) ||
                baseUrl.equals(Constants.HTTP + Constants.LOCALHOST)) {
            baseUrl = "";
        } else {
            baseUrl = Config.getBaseUrl() + Constants.STRING_DELIMITER;
        }
        String buildNumber = CommonUtils.getBuildNumber();
        if (buildNumber == null)
            buildNumber = "";
        else
            buildNumber = buildNumber + " : " + Constants.STRING_DELIMITER;

        String scenarioInfo = scenario.getName() + Constants.STRING_DELIMITER + baseUrl + buildNumber +
                Config.getBrowser().toUpperCase();
        driver.scenarioData.setData(SapUtility.FEATURE_FILE_NAME_KEY, scenario.getId().split(";")[0].
                replace("-", " "));
        driver.scenarioData.setData(SapUtility.SCENARIO_NAME_KEY, scenario.getName() );
        if (includeTestStatus) {
            scenarioInfo = scenarioInfo + Constants.STRING_DELIMITER +
                    "\n" + Constants.TEST_STATUS + ": " + scenario.getStatus().toUpperCase();
            driver.scenarioData.setData(SapUtility.TEST_STATUS_KEY, scenario.getStatus().toUpperCase());
        }
        return scenarioInfo;
    }
}
