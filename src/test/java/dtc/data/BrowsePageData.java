package dtc.data;

/**
 * Created by aaronbriel on 6/5/17.
 */
public class BrowsePageData {

    public String defaultCheckout = "default";
    public String defaultCustomer = "default_customer_az";
    public String defaultItemCode = "29935";
    public String defaultItemName = "Silver Edition III";
    public String viewAllSeasonLink = "All-Season";
    public String checkoutWithAppointment = "with appointment";

}
