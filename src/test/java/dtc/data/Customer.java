package dtc.data;

import common.Config;
import common.Constants;
import utilities.CommonUtils;

import java.lang.reflect.Field;
import java.util.logging.Logger;

/**
 * Created by aaronbriel on 9/22/16.
 */
public class Customer {

    private final Logger LOGGER = Logger.getLogger(Customer.class.getName());

    public String customerType;
    public String firstName;
    public String lastName;
    public String address1;
    public String address2;
    public String country;
    public String state;
    public String city;
    public String zip;
    public String zipPlus4;
    public String phone;
    public String phoneType;
    public String nexusEmail = "nexusqa@discounttire.com";
    public String email = "DTC_IT_QA_Auto@discounttire.com";
    public String emailDtd = "ima@discounttire.com";
    public String emailLower = "qatest@discounttire.com";
    public String emailUpper = "QATEST@DISCOUNTTIRE.COM";
    public String emailAt = "DTC_IT_QA_Auto@americastire.com";
    public String paypalUser = "dtd_test_paypal@mailinator.com";
    public String paypalPass = "dtctest1";
    public String paypalAddress;
    private String emailTimeStamp;
    public String emailPassword;
    public String stateTaxQA;
    public String cityTaxQA;
    public String otherTaxQA;
    public String stateTaxSTG;
    public String cityTaxSTG;
    public String otherTaxSTG;
    public String incorrectAddress1;
    public String invalidAddress;

    //region Credit Card Info
    public String ccNumAmEx = "378282246310005";
    public String ccNumDiscover = "6011111111111117";
    public String ccNumVisa = "4111111111111111";
    public String ccNumVisa2 = "4445222299990007";
    public String ccNumCCO = "6501590705828755";
    public String ccNumAmEx_bopis = "341111597242000";
    public String ccNumDiscover_bopis = "6011000990911111";
    public String ccNumMaster_bopis = "5444009999222205";
    public String ccNumCCO_bopis = "6501590058801417";
    public String ccNumCCO_2_bopis = "6501590058801409";
    public String expDateMM;
    public String expDateYY;
    public String ccNum = ccNumVisa;
    public String cvn = "1234";
    public String cvn2 = "382";
    public String cvn3 = "111";
    public String cvv = "253";
    public String cvv2 = "079";
    //endregion Credit Card Info
    public String zeroTax = "0.0";
    public final double default_OverSizeFee = 10.00;

    private static final String DEFAULT_CONFIG_CUSTOMER = "DEFAULT_CONFIG_CUSTOMER";
    private static final String DEFAULT_CUSTOMER_DT_QA = "DEFAULT_CUSTOMER_AZ";
    private static final String DEFAULT_CUSTOMER_AT_QA = "DEFAULT_CUSTOMER_CA";
    private static final String DEFAULT_CUSTOMER_DT_STG = "DEFAULT_CUSTOMER_AZ";
    private static final String DEFAULT_CUSTOMER_AT_STG = "DEFAULT_CUSTOMER_CA";

    private void setEmailTimeStamp(String scenarioTimeStamp) {
        this.emailTimeStamp = scenarioTimeStamp;
    }

    public enum CustomerType {
        DEFAULT_CUSTOMER_AZ,
        DEFAULT_CUSTOMER_FL,
        DEFAULT_CUSTOMER_FL1,
        DEFAULT_CUSTOMER_PA,
        DEFAULT_CUSTOMER_IN,
        DEFAULT_CUSTOMER_NY,
        DEFAULT_CUSTOMER_VA,
        DEFAULT_CUSTOMER_NC,
        DEFAULT_CUSTOMER_MS,
        DEFAULT_CUSTOMER_MA,
        DEFAULT_CUSTOMER_MI,
        DEFAULT_CUSTOMER_CT,
        DEFAULT_CUSTOMER_GA,
        DEFAULT_CUSTOMER_LA,
        DEFAULT_CUSTOMER_IL,
        DEFAULT_CUSTOMER_SC,
        DEFAULT_CUSTOMER_OH,
        DEFAULT_CUSTOMER_OH_2,
        DEFAULT_CUSTOMER_TX,
        DEFAULT_CUSTOMER_CAN,
        DEFAULT_CUSTOMER_NV,
        DEFAULT_CUSTOMER_NV2,
        DEFAULT_CUSTOMER_AK,
        DEFAULT_CUSTOMER_AR,
        DEFAULT_CUSTOMER_CO,
        DEFAULT_CUSTOMER_DE,
        DEFAULT_CUSTOMER_ID,
        DEFAULT_CUSTOMER_IA,
        DEFAULT_CUSTOMER_KS,
        DEFAULT_CUSTOMER_ME,
        DEFAULT_CUSTOMER_MO,
        DEFAULT_CUSTOMER_MT,
        DEFAULT_CUSTOMER_NE,
        DEFAULT_CUSTOMER_NH,
        DEFAULT_CUSTOMER_NM,
        DEFAULT_CUSTOMER_ND,
        DEFAULT_CUSTOMER_OK,
        DEFAULT_CUSTOMER_OR,
        DEFAULT_CUSTOMER_RI,
        DEFAULT_CUSTOMER_SD,
        DEFAULT_CUSTOMER_TN,
        DEFAULT_CUSTOMER_UT,
        DEFAULT_CUSTOMER_WV,
        DEFAULT_CUSTOMER_WY,
        DEFAULT_CUSTOMER_AL,
        DEFAULT_CUSTOMER_KY,
        DEFAULT_CUSTOMER_MD,
        DEFAULT_CUSTOMER_MN,
        DEFAULT_CUSTOMER_NJ,
        DEFAULT_CUSTOMER_WI,
        DEFAULT_CUSTOMER_WA,
        PAYPAL_CUSTOMER_AZ,
        PAYPAL_CUSTOMER_OH,
        APO_CUSTOMER,
        FPO_CUSTOMER,
        EMAIL_VALIDATION_CUSTOMER,
        DEFAULT_CUSTOMER_HI,
        DEFAULT_CUSTOMER_IT,
        DEFAULT_CUSTOMER_MX,
        DEFAULT_CUSTOMER_VT,
        UPPERCASE_CUSTOMER_CAN,
        LOWERCASE_CUSTOMER_CAN,
    	DEFAULT_CUSTOMER_CA,
    	CAR_CARE_ONE_CUSTOMER,
        DEFAULT_CUSTOMER_DTD,
        DEFAULT_CUSTOMER_CA_2,
    	DEFAULT_MYACCOUNT_CUSTOMER,
        DEFAULT_CUSTOMER_BOPIS_VISA,
        DEFAULT_CUSTOMER_BOPIS_MASTERCARD,
        DEFAULT_CUSTOMER_BOPIS_DISCOVER,
        DEFAULT_CUSTOMER_BOPIS_AMEX,
        DEFAULT_CUSTOMER_BOPIS_CC1,
        DEFAULT_CUSTOMER_BOPIS_CC1_2,
        CERTIFICATES_CUSTOMER_US,
        CERTIFICATES_CUSTOMER_CAN,
        MY_ACCOUNT_USER_A,
        MY_ACCOUNT_USER_B,
        MY_ACCOUNT_USER_C,
        MY_ACCOUNT_USER_QATEST,
        MY_ACCOUNT_NEW_CUSTOMER,
        MY_ACCOUNT_REGISTERED_CUSTOMER,
        CUSTOMER_INTEGRATION_AZ,
        CUSTOMER_INTEGRATION_TX,
        CUSTOMER_INTEGRATION_APPOINTMENTS_AZ;

        public static CustomerType customerForName(String customer) throws IllegalArgumentException {
            for (CustomerType c : values()) {
                if (c.toString().equalsIgnoreCase(customer)) {
                    return c;
                }
            }
            throw customerNotFound(customer);
        }

        private static IllegalArgumentException customerNotFound(String outcome) {
            return new IllegalArgumentException(("Invalid customer [" + outcome + "]"));
        }
    }

    public Customer() {
    }

    /**
     * Stores and returns specific customer dtc.data based on the customer type passed in
     *
     * @param custType Customer type by state
     * @return Customer values
     */
    public Customer getCustomer(String custType) {
        LOGGER.info("getCustomer started with custType: " + custType);

        if (custType.equals(DEFAULT_CONFIG_CUSTOMER)) {
            if (Config.getSiteRegion().equalsIgnoreCase(Constants.DT)) {
                if (Config.getDataSet().equalsIgnoreCase(Constants.QA))
                    custType = DEFAULT_CUSTOMER_DT_QA;
                else
                    custType = DEFAULT_CUSTOMER_DT_STG;
            } else {
                if (Config.getDataSet().equalsIgnoreCase(Constants.QA))
                    custType = DEFAULT_CUSTOMER_AT_QA;
                else
                    custType = DEFAULT_CUSTOMER_AT_STG;
            }
        }

        CustomerType customerType = CustomerType.customerForName(custType);
        Customer customer = new Customer();

        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT))
            email = email.replaceAll(ConstantsDtc.DISCOUNT_TIRE, ConstantsDtc.AMERICAS_TIRE);

        switch (customerType) {
            case DEFAULT_CUSTOMER_AZ:
                customer.customerType = custType;
                customer.firstName = "WEB";
                customer.lastName = "INT";
                customer.address1 = "E 67 Elwood St";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Arizona";
                customer.city = "Phoenix";
                customer.zip = "85040";
                customer.zipPlus4 = "85040-1025";
                customer.phone = "480-606-6054";
                customer.phoneType = "Home";
                customer.email = email;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.056";
                customer.cityTaxQA = "0.02051";
                customer.otherTaxQA = "0.013";
                customer.stateTaxSTG = "0.056";
                customer.cityTaxSTG = zeroTax;
                customer.otherTaxSTG = "0.007";
                break;
            case DEFAULT_CUSTOMER_NV:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "NV-TEST";
                customer.address1 = "5940 S Rainbow Blvd Ste 400";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Nevada";
                customer.city = "Las Vegas";
                customer.zip = "89118";
                customer.zipPlus4 = "89118-2507";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = email;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_NV2:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "NV-TEST";
                customer.address1 = "4505 S Maryland Pkwy";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Nevada";
                customer.city = "Las Vegas";
                customer.zip = "89154";
                customer.zipPlus4 = "89154-2507";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = email;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_FL:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "FL-TEST";
                customer.address1 = "3700 E Fiske Blvd";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Florida";
                customer.city = "Rockledge";
                customer.zip = "32955";
                customer.zipPlus4 = "32955-4502";
                customer.phone = "777-888-8888";
                customer.phoneType = "Home";
                customer.email = email;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.06";
                customer.cityTaxQA = zeroTax;
                customer.otherTaxQA = "0.005";
                customer.stateTaxSTG = "0.06";
                customer.cityTaxSTG = zeroTax;
                customer.otherTaxSTG = "0.01";
                break;
            case DEFAULT_CUSTOMER_FL1:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "FL-TEST";
                customer.address1 = "754 Siena Palm Drive Unit 201";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Florida";
                customer.city = "Celebration";
                customer.zip = "34747";
                customer.zipPlus4 = "34747-4411";
                customer.phone = "777-888-8888";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_NY:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "NY-TEST";
                customer.address1 = "49 Bay 14th St Apt 2";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "New York";
                customer.city = "Brooklyn";
                customer.zip = "11214";
                customer.zipPlus4 = "11214-3653";
                customer.phone = "777-888-8888";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_VA:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "VA-TEST";
                customer.address1 = "10147 Mosby Woods Dr";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Virgina   ";
                customer.city = "Fairfax";
                customer.zip = "22030";
                customer.zipPlus4 = "22030-1719";
                customer.phone = "777-888-8888";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_NC:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "NC-TEST";
                customer.address1 = "150 Hoopers Creek Rd";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "North Carolina";
                customer.city = "Fletcher";
                customer.zip = "28732";
                customer.zipPlus4 = "7586";
                customer.phone = "777-888-8888";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_MS:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "MS-TEST";
                customer.address1 = "438 Goodman Rd E";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "mississippi";
                customer.city = "Southaven";
                customer.zip = "38671";
                customer.zipPlus4 = "9557";
                customer.phone = "777-888-8888";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_MA:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "MA-TEST";
                customer.address1 = "29 Flint Rd";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "massachusetts";
                customer.city = "Acton";
                customer.zip = "01720";
                customer.zipPlus4 = "";
                customer.phone = "777-888-8888";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_MI:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "MI-TEST";
                customer.address1 = "675 E Napier Ave";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Michigan";
                customer.city = "Benton Harbor";
                customer.zip = "49022";
                customer.zipPlus4 = "5813";
                customer.phone = "777-888-8888";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_CT:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "CT-TEST";
                customer.address1 = "380 Auburn Rd";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Connecticut";
                customer.city = "Hartford";
                customer.zip = "06119";
                customer.zipPlus4 = "1003";
                customer.phone = "777-888-8888";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_PA:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "PA-TEST";
                customer.address1 = "2626 Tunnel Blvd";
                customer.address2 = "Apt 102";
                customer.country = "United States";
                customer.state = "pennsylvania";
                customer.city = "Pittsburgh";
                customer.zip = "15203";
                customer.zipPlus4 = "";
                customer.phone = "777-888-8888";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_IN:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "IN-TEST";
                customer.address1 = "17 Center Drive";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Indiana";
                customer.city = "Crawfordsville";
                customer.zip = "47933";
                customer.zipPlus4 = "";
                customer.phone = "777-888-8888";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_GA:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "GA-TEST";
                customer.address1 = "800 Spring St NW";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Georgia";
                customer.city = "Atlanta";
                customer.zip = "30308";
                customer.zipPlus4 = "30308-1015";
                customer.phone = "888-888-9999";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.emailPassword = "Password4QA!";
                customer.ccNum = ccNumVisa2;
                customer.cvn = cvn2;
                customer.stateTaxQA = "0.04";
                customer.cityTaxQA = zeroTax;
                customer.otherTaxQA = "0.03";
                customer.stateTaxSTG = "0.04";
                customer.cityTaxSTG = zeroTax;
                customer.otherTaxSTG = "0.03";                
                break;
            case DEFAULT_CUSTOMER_LA:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "LA-TEST";
                customer.address1 = "2018 Ambassador Caffery Pkwy";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Louisiana";
                customer.city = "Lafayette";
                customer.zip = "70506";
                customer.zipPlus4 = "70506-2810";
                customer.phone = "888-888-8888";
                customer.phoneType = "Home";
                customer.email = emailUpper;
                customer.emailPassword = "Password4QA!";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = zeroTax;
                customer.cityTaxQA = zeroTax;
                customer.otherTaxQA = zeroTax;
                customer.stateTaxSTG = "0.05";
                customer.cityTaxSTG = zeroTax;
                customer.otherTaxSTG = "0.0475";                
                break;
            case DEFAULT_CUSTOMER_IL:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "IL-TEST";
                customer.address1 = "207 South Washington";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Illinois";
                customer.city = "Naperville";
                customer.zip = "60540";
                customer.zipPlus4 = "";
                customer.phone = "888-888-8888";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.emailPassword = "Password4QA!";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_SC:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "SC-TEST";
                customer.address1 = "225 Ware Rd";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "South Carolina";
                customer.city = "Chesnee";
                customer.zip = "29323";
                customer.zipPlus4 = "29323-9019";
                customer.phone = "888-888-8888";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.emailPassword = "Password4QA!";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_OH:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "OH-TEST";
                customer.address1 = "1421 Saint Clair Ave NE";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Ohio";
                customer.city = "Cleveland";
                customer.zip = "44114";
                customer.zipPlus4 = "44114-2001";
                customer.phone = "555-555-5555";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn2;
                customer.stateTaxQA = "0.0575";
                customer.cityTaxQA = zeroTax;
                customer.otherTaxQA = "0.0175";
                customer.stateTaxSTG = "0.0575";
                customer.cityTaxSTG = zeroTax;
                customer.otherTaxSTG = "0.0175";
                break;
            case DEFAULT_CUSTOMER_OH_2:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "OH-TEST-2";
                customer.address1 = "590 Taylor Rd";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Ohio";
                customer.city = "Gahanna";
                customer.zip = "43230";
                customer.zipPlus4 = "43230-6269";
                customer.phone = "555-555-5555";
                customer.phoneType = "Home";
                customer.email = email;
                customer.ccNum = ccNumVisa2;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_TX:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "TX-TEST";
                customer.address1 = "3301 Palmer Hwy";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Texas";
                customer.city = "Texas City";
                customer.zip = "77590";
                customer.zipPlus4 = "77590-6509";
                customer.phone = "777-777-7777";
                customer.phoneType = "Home";
                customer.email = email;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0625";
                customer.cityTaxQA = "0.0175";
                customer.otherTaxQA = "0.0025";
                customer.stateTaxSTG = "0.0625";
                customer.cityTaxSTG = "0.01";
                customer.otherTaxSTG = "0.01";                
                break;
            case DEFAULT_CUSTOMER_CAN:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "CAN-TEST";
                customer.address1 = "372 Rundleview Drive NE";
                customer.address2 = "";
                customer.country = "Canada";
                customer.state = "Alberta";
                customer.city = "Calgary";
                customer.zip = "T1Y 1H3";
                customer.phone = "480-987-8974";
                customer.phoneType = "Home";
                customer.email = email;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = zeroTax;
                customer.cityTaxQA = zeroTax;
                customer.otherTaxQA = zeroTax;
                customer.stateTaxSTG = zeroTax;
                customer.cityTaxSTG = zeroTax;
                customer.otherTaxSTG = zeroTax;                
                break;
            case PAYPAL_CUSTOMER_AZ:
                customer.customerType = custType;
                customer.firstName = "Test";
                customer.lastName = "Test";
                customer.address1 = "20225 N Scottsdale Rd";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Arizona";
                customer.city = "Scottsdale";
                customer.zip = "85255";
                customer.phone = "480-696-9874";
                customer.phoneType = "Home";
                customer.email = paypalUser;
                customer.paypalUser = paypalUser;
                customer.paypalPass = paypalPass;
                customer.paypalAddress = "20225 N. Scottsdale, Scottsdale, AZ 85255";
                customer.stateTaxQA = zeroTax;
                customer.cityTaxQA = zeroTax;
                customer.otherTaxQA = zeroTax;
                customer.stateTaxSTG = zeroTax;
                customer.cityTaxSTG = zeroTax;
                customer.otherTaxSTG = zeroTax;                
                break;
            case PAYPAL_CUSTOMER_OH:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "OH-TEST-PAYPAL";
                customer.address1 = "1421 Saint Clair Ave NE";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Ohio";
                customer.city = "Cleveland";
                customer.zip = "44114";
                customer.phone = "555-555-5555";
                customer.phoneType = "Home";
                customer.email = email;
                customer.paypalUser = paypalUser;
                customer.paypalPass = paypalPass;
                customer.paypalAddress = "10813 Bellaire Rd, Cleveland, OH 44111";
                break;
            case APO_CUSTOMER:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "APO-TEST";
                customer.address1 = "Unit 2050";
                customer.address2 = "";
                customer.state = "AP";
                customer.country = "United States";
                customer.city = "APO";
                customer.zip = "96278-2050";
                customer.zipPlus4 = "96278-2050";
                customer.email = email;
                customer.phone = "1112223333";
                customer.phoneType = "Mobile";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = zeroTax;
                customer.cityTaxQA = zeroTax;
                customer.otherTaxQA = zeroTax;
                customer.stateTaxSTG = zeroTax;
                customer.cityTaxSTG = zeroTax;
                customer.otherTaxSTG = zeroTax;                
                break;
            case FPO_CUSTOMER:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "FPO-TEST";
                customer.address1 = "PSC 473 Box 4120";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "AP";
                customer.city = "FPO";
                customer.zip = "96349";
                customer.zipPlus4 = "96349-0042";
                customer.email = email;
                customer.phone = "4806066000";
                customer.phoneType = "Mobile";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = zeroTax;
                customer.cityTaxQA = zeroTax;
                customer.otherTaxQA = zeroTax;
                customer.stateTaxSTG = zeroTax;
                customer.cityTaxSTG = zeroTax;
                customer.otherTaxSTG = zeroTax;                
                break;
            case EMAIL_VALIDATION_CUSTOMER:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "EMAIL-TEST";
                customer.address1 = "21555 N. 56th Street";
                customer.address2 = "Apt #2098";
                customer.country = "United States";
                customer.state = "Arizona";
                customer.city = "Phoenix";
                customer.zip = "85054";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email ="qatest@discounttire.com";
                customer.emailPassword = "Discount1";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case DEFAULT_CUSTOMER_HI:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "HI-TEST";
                customer.address1 = "1441 Kapiolani Blvd Ste 1114";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Hawaii";
                customer.city = "Honolulu";
                customer.zip = "96814";
                customer.phone = "666-666-6666";
                customer.phoneType = "Mobile";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = zeroTax;
                customer.cityTaxQA = zeroTax;
                customer.otherTaxQA = zeroTax;
                customer.stateTaxSTG = zeroTax;
                customer.cityTaxSTG = zeroTax;
                customer.otherTaxSTG = zeroTax;
                break;
            case UPPERCASE_CUSTOMER_CAN:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "CAN-TEST";
                customer.address1 = "1121 AVENUE D N";
                customer.address2 = "";
                customer.country = "CANADA";
                customer.state = "SASKATCHEWAN";
                customer.city = "SASKATOON";
                customer.zip = "S7L1N7";
                customer.phone = "777-888-9999";
                customer.phoneType = "HOME";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = zeroTax;
                customer.cityTaxQA = zeroTax;
                customer.otherTaxQA = zeroTax;
                customer.stateTaxSTG = zeroTax;
                customer.cityTaxSTG = zeroTax;
                customer.otherTaxSTG = zeroTax;
                break;
            case LOWERCASE_CUSTOMER_CAN:
                customer.customerType = custType;
                customer.firstName = "ima";
                customer.lastName = "can-test";
                customer.address1 = "445 rue lavigueur";
                customer.address2 = "";
                customer.country = "canada";
                customer.state = "quebec";
                customer.city = "quebec city";
                customer.zip = "g1r1b6";
                customer.phone = "666-666-6666";
                customer.phoneType = "Mobile";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = zeroTax;
                customer.cityTaxQA = zeroTax;
                customer.otherTaxQA = zeroTax;
                customer.stateTaxSTG = zeroTax;
                customer.cityTaxSTG = zeroTax;
                customer.otherTaxSTG = zeroTax;
                break;
            case DEFAULT_CUSTOMER_CA:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "CA-TEST";
                customer.address1 = "36 Leila Ave";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "California";
                customer.city = "Redding";
                customer.zip = "96002";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = emailAt;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.cityTaxQA = "0.00";
                customer.otherTaxQA = "0.005";
                customer.stateTaxSTG = "0.0725";
                customer.cityTaxSTG = "0.00";
                customer.otherTaxSTG = "0.00";
                break;
            case DEFAULT_CUSTOMER_AK:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "AK-TEST";
                customer.address1 = "4821 S Binnacle Dr";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Alaska";
                customer.city = "Wasilla";
                customer.zip = "99623";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_AR:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "AR-TEST";
                customer.address1 = "706 W 4th St";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Arkansas";
                customer.city = "North Little Rock";
                customer.zip = "72114";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_CO:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "CO-TEST";
                customer.address1 = "1245 Alpine Avenue";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Colorado";
                customer.city = "Boulder";
                customer.zip = "80304";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_DE:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "CO-TEST";
                customer.address1 = "1037 North DuPont Highway";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Delaware";
                customer.city = "Dover";
                customer.zip = "19901";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_ID:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "ID-TEST";
                customer.address1 = "1415 S Biggs St";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Idaho";
                customer.city = "Boise";
                customer.zip = "83709";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_IA:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "IA-TEST";
                customer.address1 = "2833 Crossroads Blvd";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Iowa";
                customer.city = "Waterloo";
                customer.zip = "50702";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_KS:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "KS-TEST";
                customer.address1 = "15208 W 119TH ST";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Kansas";
                customer.city = "Olathe";
                customer.zip = "66062";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_ME:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "ME-TEST";
                customer.address1 = "145 Eden St";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Maine";
                customer.city = "Bar Harbor";
                customer.zip = "04609";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_MO:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "MO-TEST";
                customer.address1 = "201 S Buchanan St";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Missouri";
                customer.city = "Maryville";
                customer.zip = "64468";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_MT:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "MT-TEST";
                customer.address1 = "1925 Grand Ave Ste 129";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Montana";
                customer.city = "Billings";
                customer.zip = "59102";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_NE:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "NE-TEST";
                customer.address1 = "1400 R St";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Nebraska";
                customer.city = "Lincoln";
                customer.zip = "68588";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_NH:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "NH-TEST";
                customer.address1 = "60 N Mast St";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "New Hampshire";
                customer.city = "Goffstown";
                customer.zip = "03045";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_NM:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "NM-TEST";
                customer.address1 = "6543 Pato Rd NW";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "New Mexico";
                customer.city = "Albuquerque";
                customer.zip = "87120";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_ND:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "ND-TEST";
                customer.address1 = "1107 5th St N";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "North Dakota";
                customer.city = "Fargo";
                customer.zip = "58102";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_OK:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "OK-TEST";
                customer.address1 = "7501 S Shields Blvd";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Oklahoma";
                customer.city = "Oklahoma City";
                customer.zip = "73149";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_OR:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "OR-TEST";
                customer.address1 = "100 NW 10th Avenue";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Oregon";
                customer.city = "Portland";
                customer.zip = "97209";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = zeroTax;
                customer.stateTaxSTG = zeroTax;
                customer.cityTaxQA = zeroTax;
                customer.cityTaxSTG = zeroTax;
                customer.otherTaxQA = zeroTax;
                customer.otherTaxSTG = zeroTax;
                break;
            case DEFAULT_CUSTOMER_RI:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "RI-TEST";
                customer.address1 = "32 TOLL GATE RD";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Rhode Isalnd";
                customer.city = "WARWICK";
                customer.zip = "02886";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_SD:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "SD-TEST";
                customer.address1 = "130 Kansas City Street";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "South Dakota";
                customer.city = "Rapid City";
                customer.zip = "57701";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_TN:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "TN-TEST";
                customer.address1 = "11083 Parkside Dr";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Tennessee";
                customer.city = "Knoxville";
                customer.zip = "37934";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_UT:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "UT-TEST";
                customer.address1 = "5207 South State Street";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Utah";
                customer.city = "Salt Lake City";
                customer.zip = "84107";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_VT:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "VT-TEST";
                customer.address1 = "41 CHERRY STREET";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Vermont";
                customer.city = "Burlington";
                customer.zip = "05401";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_WV:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "WV-TEST";
                customer.address1 = "933 Nestle Quarry Rd";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "West Virginia";
                customer.city = "Falling Waters";
                customer.zip = "25419";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_WY:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "WY-TEST";
                customer.address1 = "2020 Carey Ave, Ste 700";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Wyoming";
                customer.city = "Cheyenne";
                customer.zip = "82001";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_AL:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "WY-TEST";
                customer.address1 = "1755 Denali Ct";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Alabama";
                customer.city = "Auburn";
                customer.zip = "36832";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_KY:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "KY-TEST";
                customer.address1 = "193 Pavilion Pkwy";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Kentucky";
                customer.city = "Newport";
                customer.zip = "41071";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_MN:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "MN-TEST";
                customer.address1 = "6531 York Avenue South";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Minnesota";
                customer.city = "Minneapolis";
                customer.zip = "55435";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_NJ:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "NJ-TEST";
                customer.address1 = "34 Erwin Park Rd";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "New Jersey";
                customer.city = "Montclair";
                customer.zip = "07042";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_MD:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "MD-TEST";
                customer.address1 = "7320 Baltimore Avenue";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Maryland";
                customer.city = "College Park";
                customer.zip = "20740";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_WI:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "WI-TEST";
                customer.address1 = "232 State Street";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Wisconsin";
                customer.city = "Madison";
                customer.zip = "53703";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.stateTaxSTG = "0.0725";
                break;
            case DEFAULT_CUSTOMER_WA:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "WA-TEST";
                customer.address1 = "2310 Perkins Ln W";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Washington";
                customer.city = "Seattle";
                customer.zip = "98199";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = nexusEmail;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                break;
            case CAR_CARE_ONE_CUSTOMER:
                customer.customerType = custType;
                customer.firstName = "QA Test";
                customer.lastName = "QA Test";
                customer.address1 = "20225 N Scottsdale Rd";
                customer.address2 = "";
                customer.state = "Arizona";
                customer.country = "United States";
                customer.city = "Scottsdale";
                customer.zip = "85255";
                customer.email = "puja.patel@discounttire.com";
                customer.phone = "4806066000";
                customer.phoneType = "Mobile";
                customer.ccNum = ccNumCCO;
                customer.cvn = cvv;
                break;
            case DEFAULT_CUSTOMER_CA_2:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "CA-TEST";
                customer.address1 = "12018 Central Ave";
                customer.address2 = "";
                customer.state = "California";
                customer.country = "United States";
                customer.city = "Chino";
                customer.zip = "91710";
                customer.email = "puja.patel@discounttire.com";
                customer.phone = "1112223333";
                customer.phoneType = "Mobile";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.stateTaxQA = "0.0725";
                customer.cityTaxQA = "0.00";
                customer.otherTaxQA = "0.005";
                customer.stateTaxSTG = "0.0725";
                customer.cityTaxSTG = "0.00";
                customer.otherTaxSTG = "0.005";
                break;
            case DEFAULT_MYACCOUNT_CUSTOMER:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "TEST-USER";
                customer.address1 = "20225 N Scottsdale Rd";
                customer.address2 = "";
                customer.state = "Arizona";
                customer.country = "United States";
                customer.city = "Scottsdale";
                customer.zip = "85255";
                customer.email ="qatest@discounttire.com";
                customer.emailPassword = "Password4QA!";
                customer.phone = "4806066000";
                customer.phoneType = "Mobile";
                customer.ccNum = ccNumCCO;
                customer.cvn = cvv;
                break;
            case DEFAULT_CUSTOMER_DTD:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "AZ-TEST";
                customer.address1 = "E 67 Elwood St";
                customer.address2 = "Scottsdale";
                customer.country = "United States";
                customer.state = "Arizona";
                customer.city = "Phoenix";
                customer.zip = "85040";
                customer.phone = "4806666666";
                customer.phoneType = "Home";
                customer.email = emailDtd;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.phoneType = "Mobile";
                customer.stateTaxQA = "0.056";
                customer.cityTaxQA = "0.023";
                customer.otherTaxQA = "0.007";
                customer.stateTaxSTG = "0.056";
                customer.cityTaxSTG = "0.023";
                customer.otherTaxSTG = "0.007";
                break;
            case DEFAULT_CUSTOMER_BOPIS_VISA:
                customer.customerType = custType;
                customer.firstName = "HYBRIS";
                customer.lastName = "INT";
                customer.address1 = "100 N Gilchrist Ave";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "FL";
                customer.city = "Tampa";
                customer.zip = "33606";
                customer.zipPlus4 = "33606-1437";
                customer.phone = "480-606-5821";
                customer.phoneType = "Home";
                customer.email = emailUpper;
                customer.emailPassword = "Password4QA!";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.expDateMM = "12";
                customer.expDateYY = "20";
                customer.stateTaxQA = "0.056";
                customer.cityTaxQA = "0.02051";
                customer.otherTaxQA = "0.013";
                customer.stateTaxSTG = "0.056";
                customer.cityTaxSTG = "0.02051";
                customer.otherTaxSTG = "0.013";
                break;
            case DEFAULT_CUSTOMER_BOPIS_MASTERCARD:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "BOPIS-MASTERCARD-TEST";
                customer.address1 = "100 N Gilchrist Ave";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Florida";
                customer.city = "Tampa";
                customer.zip = "33606";
                customer.zipPlus4 = "33606-1437";
                customer.phone = "480-606-5820";
                customer.phoneType = "Home";
                customer.email = email;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.expDateMM = "12";
                customer.expDateYY = "20";
                customer.stateTaxQA = "0.056";
                customer.cityTaxQA = "0.02051";
                customer.otherTaxQA = "0.013";
                customer.stateTaxSTG = "0.056";
                customer.cityTaxSTG = "0.02051";
                customer.otherTaxSTG = "0.013";
                break;
            case DEFAULT_CUSTOMER_BOPIS_DISCOVER:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "BOPIS-DISCOVER-CARD-TEST";
                customer.address1 = "100 N Gilchrist Ave";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Florida";
                customer.city = "Tampa";
                customer.zip = "33606";
                customer.zipPlus4 = "33606-1437";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = email;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.expDateMM = "12";
                customer.expDateYY = "20";
                customer.stateTaxQA = "0.056";
                customer.cityTaxQA = "0.02051";
                customer.otherTaxQA = "0.013";
                customer.stateTaxSTG = "0.056";
                customer.cityTaxSTG = "0.02051";
                customer.otherTaxSTG = "0.013";
                break;
            case DEFAULT_CUSTOMER_BOPIS_AMEX:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "BOPIS-AMEX-CARD-TEST";
                customer.address1 = "100 N Gilchrist Ave";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Florida";
                customer.city = "Tampa";
                customer.zip = "33606";
                customer.zipPlus4 = "33606-1437";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = email;
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.expDateMM = "12";
                customer.expDateYY = "20";
                customer.stateTaxQA = "0.056";
                customer.cityTaxQA = "0.02051";
                customer.otherTaxQA = "0.013";
                customer.stateTaxSTG = "0.056";
                customer.cityTaxSTG = "0.02051";
                customer.otherTaxSTG = "0.013";
                break;
            case DEFAULT_CUSTOMER_BOPIS_CC1:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "BOPIS-CC-ONE-TEST";
                customer.address1 = "950 Forrer Blvd";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Ohio";
                customer.city = "Dayton";
                customer.zip = "45420";
                customer.zipPlus4 = "45420-1469";
                customer.phone = "480-606-2356";
                customer.phoneType = "Home";
                customer.email = email;
                customer.ccNum = ccNum;
                customer.cvv = cvv;
                customer.expDateMM = "6";
                customer.expDateYY = "21";
                customer.stateTaxQA = "0.056";
                customer.cityTaxQA = "0.02051";
                customer.otherTaxQA = "0.013";
                customer.stateTaxSTG = "0.056";
                customer.cityTaxSTG = "0.02051";
                customer.otherTaxSTG = "0.013";
                break;
            case DEFAULT_CUSTOMER_BOPIS_CC1_2:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "BOPIS-CC-ONE-TEST";
                customer.address1 = "950 Forrer Blvd";
                customer.address2 = "";
                customer.country = "United States";
                customer.state = "Ohio";
                customer.city = "Dayton";
                customer.zip = "45420";
                customer.zipPlus4 = "45420-1469";
                customer.phone = "666-666-6666";
                customer.phoneType = "Home";
                customer.email = email;
                customer.ccNum = ccNum;
                customer.cvv = "017";
                customer.expDateMM = "6";
                customer.expDateYY = "21";
                customer.stateTaxQA = "0.056";
                customer.cityTaxQA = "0.02051";
                customer.otherTaxQA = "0.013";
                customer.stateTaxSTG = "0.056";
                customer.cityTaxSTG = "0.02051";
                customer.otherTaxSTG = "0.013";
                break;
            case CERTIFICATES_CUSTOMER_US:
                customer.customerType = custType;
                customer.firstName = "DTD";
                customer.lastName = "STAG REAR";
                customer.address1 = "16631 N 56TH ST";
                customer.address2 = "APT 1001";
                customer.country = "United States";
                customer.state = "AZ";
                customer.city = "SCOTTSDALE";
                customer.zip = "85254-1241";
                customer.phone = "6692324057";
                customer.email = "DTC_IT_QA_AUTO@DISCOUNTTIRE.COM";
                break;
            case CERTIFICATES_CUSTOMER_CAN:
                customer.customerType = custType;
                customer.firstName = "IMA";
                customer.lastName = "CAN-TEST";
                customer.address1 = "372 RUNDLEVIEW DRIVE NE";
                customer.address2 = "";
                customer.country = "Canada";
                customer.state = "AB";
                customer.city = "CALGARY";
                customer.zip = "T1Y 1H8";
                customer.phone = "4806066000";
                customer.email = "DTC_IT_QA_AUTO@DISCOUNTTIRE.COM";
                break;
            case MY_ACCOUNT_USER_A:
                customer.customerType = custType;
                customer.firstName = "Autouser";
                customer.lastName = "QaA";
                customer.email = "autouser_a@gmail.com";
                customer.emailPassword = "Discount1";
                customer.address1 = "20225 N Scottsdale Rd";
                customer.address2 = "";
                customer.phone = "1112223334";
                customer.country = "United States";
                customer.city = "Scottsdale";
                customer.state = "Arizona";
                customer.zip = "85255";
                customer.zipPlus4 = "85255-6456";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.incorrectAddress1 = "20225 N scottssdale rd";
                customer.invalidAddress = "APT 100000";
                customer.phoneType = "Mobile";
                break;
            case MY_ACCOUNT_USER_B:
                customer.customerType = custType;
                customer.firstName = "Autouser";
                customer.lastName = "QaB";
                customer.email = "autouser_b@gmail.com";
                customer.emailPassword = "Discount1";
                customer.address1 = "16631 N 56th St";
                customer.address2 = "Apt 1001";
                customer.phone = "1111222233";
                customer.country = "United States";
                customer.city = "Scottsdale";
                customer.state = "Arizona";
                customer.zip = "85254";
                customer.zipPlus4 = "85254-1241";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.incorrectAddress1 = "16631 North 56th St@";
                customer.phoneType = "Mobile";
                break;
            case MY_ACCOUNT_USER_C:
                customer.customerType = custType;
                customer.firstName = "Autouser";
                customer.lastName = "QaC";
                customer.email = "autouser_c@gmail.com";
                customer.emailPassword = "Discount1";
                customer.address1 = "950 Forrer Blvd";
                customer.address2 = "";
                customer.phone = "1111122222";
                customer.country = "United States";
                customer.city = "Dayton";
                customer.state = "Ohio";
                customer.zip = "45420";
                customer.zipPlus4 = "45420-1469";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.incorrectAddress1 = "950 Forrer Blvdd";
                customer.phoneType = "Mobile";
                break;
            case MY_ACCOUNT_NEW_CUSTOMER:
                customer.customerType = custType;
                customer.firstName = "Autouser";
                customer.lastName = "new";
                customer.email = "autouser_"+ CommonUtils.getRandomNumber(1000000,9999999)+"@gmail.com";
                customer.emailPassword = "Discount1";
                customer.address1 = "20225 N Scottsdale Rd";
                customer.address2 = "";
                String phoneNumber = String.valueOf(CommonUtils.getRandomNumber(10000,99999))+String.
                        valueOf(+ CommonUtils.getRandomNumber(10000,99999));
                customer.phone = phoneNumber.substring(0,10);
                customer.country = "United States";
                customer.city = "Scottsdale";
                customer.state = "Arizona";
                customer.zip = "85255";
                customer.zipPlus4 = "85255-6456";
                customer.incorrectAddress1 = "20225 N Scottsdalee Rd";
                customer.phoneType = "Mobile";
                break;
            case MY_ACCOUNT_USER_QATEST:
                customer.customerType = custType;
                customer.firstName = "User";
                customer.lastName = "qatest";
                customer.email = "qatest345@gmail.com";
                customer.emailPassword = "Discount1";
                customer.address1 = "950 Forrer Blvd";
                customer.address2 = "";
                customer.phone = "1234567890";
                customer.country = "United States";
                customer.city = "Dayton";
                customer.state = "Ohio";
                customer.zip = "45420";
                customer.zipPlus4 = "45420-1469";
                customer.incorrectAddress1 = "950 Forrerr Blvd";
                customer.phoneType = "Mobile";
                break;
            case CUSTOMER_INTEGRATION_AZ:
                customer.customerType = custType;
                customer.firstName = "Integration";
                customer.lastName = "Suiteone";
                customer.email = "integration.testsuiteone@gmail.com";
                customer.emailPassword = "Discount1";
                customer.address1 = "2311 w spur dr";
                customer.address2 = "";
                customer.phone = "928-214-0480";
                customer.country = "United States";
                customer.city = "phoenix ";
                customer.state = "Arizona";
                customer.zip = "85085";
                customer.zipPlus4 = "85085-5757";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.incorrectAddress1 = "2311 w spurr dr";
                customer.invalidAddress = "APT 100000";
                customer.phoneType = "Mobile";
                break;
            case CUSTOMER_INTEGRATION_TX:
                customer.customerType = custType;
                customer.firstName = "Discount";
                customer.lastName = "two";
                customer.email = "discountusertwo@outlook.com";
                customer.emailPassword = "Discount1";
                customer.address1 = "7226 Birchwood Dr";
                customer.address2 = "";
                customer.phone = "687-677-6100";
                customer.country = "United States";
                customer.city = "Dallas ";
                customer.state = "Texas";
                customer.zip = "75240";
                customer.zipPlus4 = "75240-3610";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.incorrectAddress1 = "7226 Birchhwood Dr";
                customer.invalidAddress = "APT 100000";
                customer.phoneType = "Mobile";
                break;
            case CUSTOMER_INTEGRATION_APPOINTMENTS_AZ:
                customer.customerType = custType;
                customer.firstName = "Rajasekhar";
                customer.lastName = "C";
                customer.email = "automationtestdt@gmail.com";
                customer.emailPassword = "Discount1";
                customer.address1 = "6633 E Greenway Pkwy";
                customer.address2 = "Apt 2019";
                customer.phone = "471-852-9630";
                customer.country = "United States";
                customer.city = "Scottsdale ";
                customer.state = "Arizona";
                customer.zip = "85254";
                customer.zipPlus4 = "85254-2035";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.incorrectAddress1 = "6633 E Greenwway Pkwy";
                customer.invalidAddress = "APT 100000";
                customer.phoneType = "Mobile";
                break;
            case MY_ACCOUNT_REGISTERED_CUSTOMER:
                customer.customerType = custType;
                customer.firstName = "Neha";
                customer.lastName = "Nin";
                customer.email = "automationtestdt@gmail.com";
                customer.emailPassword = "Discount1";
                customer.address1 = "20225 N Scottsdale rd";
                customer.address2 = "";
                customer.phone = "123-432-1233";
                customer.country = "United States";
                customer.city = "Scottsdale";
                customer.state = "Arizona";
                customer.zip = "85255";
                customer.zipPlus4 = "85255-6456";
                customer.ccNum = ccNum;
                customer.cvn = cvn;
                customer.incorrectAddress1 = "20225 N Scottsdalee rd";
                customer.invalidAddress = "APT 100000";
                customer.phoneType = "Mobile";
                break;
        }
        LOGGER.info("getCustomer completed");
        return customer;
    }

    /**
     * Returns customer dtc.data based on input customer type
     *
     * @param customer customer type
     * @return String of customer dtc.data
     */
    public String getCustomerDataString(Customer customer) {
        LOGGER.info("getCustomerDataString started");
        StringBuilder result = new StringBuilder();
        String newLine = System.getProperty(Constants.LINE_SEPARATOR_SYSTEM_PROPERTY);
        Field[] fields = customer.getClass().getDeclaredFields();

        result.append(newLine);
        boolean done = false;

        //print field names paired with their values if not null
        for (Field field : fields) {
            try {
                if (field.get(customer) != null) {
                    if (CommonUtils.containsIgnoreCase(field.getName(), Constants.LOGGER))
                        continue;
                    if (field.getName().equalsIgnoreCase(CommonUtils.removeSpaces(ConstantsDtc.PHONE_TYPE)))
                        done = true;
                    if (isPaypalCustomer(customer) &&
                            field.getName().toLowerCase().contains(ConstantsDtc.EMAIL.toLowerCase())) {
                        //Skipping default customer class-scoped dtc.data for Paypal customer.
                    } else if (!isPaypalCustomer(customer) &&
                            field.getName().toLowerCase().contains(ConstantsDtc.PAYPAL.toLowerCase())) {
                        //Skipping paypal customer class-scoped dtc.data for default checkout customer
                    } else {
                        result.append("  ");
                        result.append(field.getName());
                        result.append(": ");
                        result.append(field.get(customer));
                        result.append(newLine);
                    }
                }
            } catch (IllegalAccessException ex) {
                LOGGER.info(ex.toString());
            }
            if (done) {
                break;
            }
        }

        LOGGER.info("getCustomerDataString completed");
        return result.toString();
    }

    /**
     * Helper method for constructing and clicking an autoClass named element
     *
     * @param customer Customer type
     */
    private boolean isPaypalCustomer(Customer customer) {
        return customer.customerType.toLowerCase().contains(ConstantsDtc.PAYPAL.toLowerCase());
    }
}