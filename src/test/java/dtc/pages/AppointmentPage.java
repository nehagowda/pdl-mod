package dtc.pages;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import dtc.data.Customer;
import orderxmls.pages.OrderXmls;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.CommonUtils;
import utilities.Driver;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created by aaronbriel on 9/28/16. Updated by aarora
 */
public class AppointmentPage {

    private WebDriver webDriver;
    private Driver driver;
    private CommonActions commonActions;
    private Customer customer;
    private final Logger LOGGER = Logger.getLogger(AppointmentPage.class.getName());

    public AppointmentPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        customer = new Customer();
        PageFactory.initElements(webDriver, this);
    }

    public static final String SPECIAL_ORDER_MESSAGE = "You selected product that is \"Special Order\". " +
            "A representative from the store you selected will verify product availability and contact you " +
            "(within 1 business day) regarding installation.";
    public static final String CALL_US_MESSAGE = "Call us at 800-589-6789";
    private static final String CANCEL = "Cancel";
    public static final String EST_COMPLETION_TIME = "Est. Completion Time:";
    public static final String PEAK = "Peak";
    public static final String TIME = "Time:";
    public static final String FULL_DAY_HOLIDAY = "Full Day Holiday";
    public static final String PARTIAL_DAY_HOLIDAY = "Partial Day Holiday";
    public static final String PARTIAL_DAY_LOCAL_OUTAGE = "Partial Day Local Outage";
    public static final String SUNDAY_HOLIDAY = "Sunday Holiday";
    public static final String EXPEDITE_YOUR_STORE_EXPERIENCE = "Expedite your Store Experience";
    public static ArrayList<String> listOfAppointmentDates= new ArrayList<String>();
    private static final String SHOP_TIRES = "Shop tires";
    private static final String SHOP_WHEELS = "Shop wheels";

    @FindBy(className = "appointment-steps__review-button")
    public static WebElement nextStepForDateTimeCustomerInfoButton;

    @FindBy(id = "first-name")
    public static WebElement firstNameBoxAppointmentFlow;

    @FindBy(id = "last-name")
    public static WebElement lastNameBoxAppointmentFlow;

    @FindBy(id = "phone-number")
    public static WebElement phoneNumberBoxAppointmentFlow;

    @FindBy(id = "email-address")
    public static WebElement emailBoxAppointmentFlow;

    @FindBy(id = "firstNameId")
    public static WebElement firstNameBoxServiceFlow;

    @FindBy(id = "lastNameId")
    public static WebElement lastNameBoxServiceFlow;

    @FindBy(xpath = "//*[contains(text(),'Shop Tires')]")
    private static WebElement shopTires;

    @FindBy(xpath = "//*[contains(text(),'Shop Wheels')]")
    private static WebElement shopWheels;

    @FindBy(id = "phoneId")
    public static WebElement phoneNumberBoxServiceFlow;

    @FindBy(id = "accountEmailId")
    public static WebElement emailBoxServiceFlow;

    @FindBy(className = "react-selectize-search-field-and-selected-values")
    public static WebElement phoneDropDownWebServiceFlow;

    @FindBy(id = "phoneTypeId")
    public static WebElement phoneDropDownMobile;

    @FindBy(className = "appointment-steps__edit")
    public static WebElement appointmentEditLink;

    @FindBy(linkText = "Change store")
    public static WebElement changeStoreLink;

    @FindBy(className = "picker__nav--next")
    public static WebElement nextMonthArrow;

    @FindBy(css = ".appointment-details__select-time-row-time--available > span")
    public static WebElement pickTimeContainer;

    @FindBy(name = "appointmentDate")
    public static WebElement appointmentDate;

    @FindBy(name = "appointmentTime")
    public static WebElement appointmentTime;

    @FindBy(className = "picker__month")
    public static WebElement currentMonth;

    @FindBy(id = "addressFlag")
    public static WebElement enterAddressCheckbox;

    @FindBy(className = "checkout-payment__address1")
    public static WebElement address1;

    @FindBy(className = "checkout-payment__address2")
    public static WebElement address2;

    @FindBy(className = "checkout-payment__city")
    public static WebElement city;

    @FindBy(className = "checkout-payment__zip")
    public static WebElement zip;

    @FindBy(id = "addressProvince")
    public static WebElement stateDropDownMobile;

    @FindBy(className = "picker__list-message-walk-ins")
    public static WebElement walkInsWelcome;

    @FindBy(xpath = "//button[@type='submit']")
    public static WebElement submitBtn;

    @FindBy(className = "picker__button--close")
    public static WebElement datePickerCloseBtn;

    @FindBy(className = "appointment-summary__message")
    public static WebElement appointmentDateTime;

    @FindBy(className = "appointment-details__select-date-month")
    public static WebElement apptMonthAndYear;

    @FindBy(xpath = "//a[contains(@href, 'appointment-info')]")
    public static WebElement editApptLink;

    @FindBy(className = "datepicker-message")
    public static WebElement datepickerMessage;

    @FindBy(className = "appointment-details__select-time-header-current-date")
    public static WebElement apptTimeListHeaderDate;

    @FindBy(className = "appointment-details__select-date--selected")
    public static WebElement apptSelectedDate;

    @FindBy(name = "recipientFirstName")
    public static WebElement recipientFirstName;

    @FindBy(name = "recipientLastName")
    public static WebElement recipientLastName;

    @FindBy(name = "recipientPhone")
    public static WebElement recipientPhone;

    @FindBy(name = "recipientEmail")
    public static WebElement recipientEmail;

    @FindBy(css = ".appointment-details__select-time-store-holiday")
    public static WebElement storeFullHolidayMessage;

    @FindBy(css = "div[class*=appointment-details__select-time-row-partial-closed-message]>div")
    public static WebElement storePartialDayHolidayMessage;

    @FindBy(css = ".appointment-details__select-time-reason-code")
    public static WebElement storeReasonCode;

    @FindBy(css = ".appointment-details__select-time-store-closed")
    public static WebElement sundayHolidayMessage;

    @FindBy(css = ".appointment-steps__title")
    public static WebElement appointmentStepsTitle;

    @FindBy(xpath = "//a[contains(@href, '/schedule-appointment/vehicle-info')]")
    private static WebElement addVehicleLink;

    @FindBy(css = "[class*='vehicle-information-form__selected-vehicle']")
    private static WebElement selectedVehicleContainer;

    @FindBy(css = "[class*='vehicle-summary__container']")
    private static WebElement vehicleSummaryContainer;

    private static final By recipientPhoneNumberRadioLabel = By.cssSelector(".form-group__radio-label[for='recipientContactPhone']");

    private static final By recipientEmailAddressRadioLabel = By.cssSelector(".form-group__radio-label[for='recipientContactEmail']");

    private static final By recipientReceiveTextMessagesLabel = By.cssSelector(".form-group__radio-label[for='smsUpdateRecipient-yes']");

    private static final By recipientDoNotReceiveTextMessagesLabel = By.cssSelector(".form-group__radio-label[for='smsUpdateRecipient-no']");

    private static final By pickTimeContainerBy = By.cssSelector(".appointment-details__select-time-row-times " +
            "> .appointment-details__select-time-row-time");

    private static final By myInformationBy = By.className("my-order-info__customer");

    private static final By myInformationDetailsPageBy = By.cssSelector
            ("[class*='appointment-contact-address__my-appointment-contact-address'] address");

    public static final By serviceOptionMessageBy = By.className("appointment-summary__message");

    public static final By addNewVehicleBy = By.cssSelector("[class*='vehicle-information-form__title']");

    @FindBy(className = "checkout-content__title--active")
    public static WebElement activeSectionTitleElement;

    public static final String pickDayInfocus = "picker__day--infocus";

    public static final String pickDayDisabled = "picker__day--disabled";

    public static final String pickDay = "picker__day";

    public static final String pickDayHighlighted = "picker__day--highlighted";

    public static final String phoneTypeIdWeb = "phoneTypeId_chosen";

    public static final By serviceOptionBy = By.className("input");

    public static final By apptMonthAndYearBy = By.className("appointment-details__select-date-month");

    public static final By appointmentSummaryActionsBy = By.className("appointment-summary__actions");

    public static final By vehicleInfoFormHeaderContainerBy =
            By.cssSelector("[class*='vehicle-information-form__header-container']");

    public static final By editVehicleServiceAppointmentBy =
            By.cssSelector("[class*='fitment-banner__edit-vehicle-button-label']");

    public static final By serviceAppointmentVehicleContainerBy =
            By.cssSelector("[class*='vehicle-information-form__container']");

    private int appointmentDayStart = 0;
    private static final int APPOINTMENT_DAYS_ATTEMPTED = 4;
    private static String passableDate;
    private static String passableTime;

    public static String getPassableDate() {
        return passableDate;
    }

    public static String getPassableTime() {
        return passableTime;
    }

    public void setPassableDate(String passableDate) {
        AppointmentPage.passableDate = passableDate;
    }

    public void setPassableTime(String passableTime) {
        AppointmentPage.passableTime = passableTime;
    }

    /**
     * Schedules an appointment Time and Date
     *
     * @param customer used in failed assertion message
     */
    public void makeAppointment(Customer customer) {
        LOGGER.info("makeAppointment started");
        try {
            selectDate();
            selectTime();
        } catch (Exception e) {
            Assert.fail("FAIL: Making appointment (setting date and time) for user " +
                    customer.getCustomerDataString(customer) + " FAILED with error: " + e);
        }
        LOGGER.info("makeAppointment completed");
    }

    /**
     * Selects services from a string of options, separated by commas. Iterates over list to select
     * each individual service.
     *
     * @param options String of services to be selected, separated by commas.
     */
    public void selectService(String options) {
        LOGGER.info("selectService started");
        List<String> optionsToSelect = Lists.newArrayList(Splitter.on(",").trimResults().split(options));
        driver.waitForElementVisible(appointmentStepsTitle);

        for (String option : optionsToSelect) {
            driver.jsScrollToElementClick(driver.getElementWithText(serviceOptionBy, option), false);
            driver.scenarioData.genericData.put(options, Constants.JSON_PARAM_QUANTITY + ":"
                    + "1" + "," + OrderXmls.RETAIL_PRICE + ":" + "0.0" + "," + OrderXmls.SALES_EXTENDED + ":" +
                    "0.0");
        }
        LOGGER.info("selectService completed");
    }

    /**
     * Clicks the 'Set Appointment Details' button on the customer appointment page
     */
    public void clickSetAppointmentDetailsForDateAndTime() {
        LOGGER.info("clickSetAppointmentDetailsForDateAndTime started");
        driver.jsScrollToElementClick(nextStepForDateTimeCustomerInfoButton, false);
        LOGGER.info("clickSetAppointmentDetailsForDateAndTime completed");
    }

    /**
     * Clicks the 'Next Step' button on the customer appointment page
     * Has an implicit wait as well as a try/catch
     */
    public void clickNextStepForCustomerInformation() {
        LOGGER.info("clickNextStepForCustomerInformation started");
        commonActions.clickDefaultFormSubmitButton();
        LOGGER.info("clickNextStepForCustomerInformation completed");
    }

    /**
     * Clicks the 'Next Step' button on the customer appointment page
     * Has an implicit wait as well as a try/catch
     */
    public void clickContinueForAppointmentCustomerDetailsPage() {
        LOGGER.info("clickNextStepForAppointmentCustomerInformation started");
        driver.waitForElementVisible(submitBtn);
        if (!Config.isSafari()) {
            driver.jsScrollToElement(submitBtn);
            submitBtn.click();
        } else {
            driver.jsScrollToElementClick(submitBtn);
        }
        LOGGER.info("clickNextStepForAppointmentCustomerInformation completed");
    }

    /**
     * Selects a date from the month modal window, by clicking on the next month and selecting a date that
     * that is NOT disabled (Sundays)
     */
    public void selectDate() {
        LOGGER.info("selectDate started");

        driver.waitForPageToLoad();
        driver.jsScrollToElementClick(webDriver.findElement(CommonActions.apptDatesDayBy));

        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        driver.waitForElementVisible(CheckoutPage.apptDateContainer);
        driver.waitForMilliseconds();

        // Picks up every day on calendar, then iterates through to the highlighted day and sets found = true.
        // (cont) Then goes through again until it finds the next day that is not highlighted or disabled and clicks it.
        List<WebElement> dayList = CheckoutPage.apptDateContainer.findElements(By.className(pickDay));
        boolean found = false;
        for (WebElement day : dayList) {
            if (day.getAttribute(Constants.CLASS).contains(pickDayHighlighted)) {
                found = true;
            } else if (found && !day.getAttribute(Constants.CLASS).contains(pickDayDisabled)
                    && !day.getAttribute(Constants.CLASS).contains(pickDayHighlighted)) {
                day.click();
                break;
            }
        }

        setPassableDate(appointmentDate.getAttribute(Constants.VALUE));
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        LOGGER.info("selectDate completed");
    }

    /**
     * Selects the first available time slot from the Time picker menu.
     */
    public void selectTime() {
        LOGGER.info("selectTime started");
        String ITEM_DISABLED = "item--disabled";
        String ITEM_SELECTED = "item--selected";
        String ITEM_HIGHLIGHTED = "item--highlighted";
        String PRESENTATION = "presentation";

        if (driver.isElementDisplayed(datePickerCloseBtn, Constants.THREE)) {
            datePickerCloseBtn.click();
        }

        driver.jsScrollToElementClick(webDriver.findElement(CommonActions.availableApptTimesBy));

        if (Config.isMobile()) {
            driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        }

        driver.waitForMilliseconds();
        driver.waitForElementVisible(pickTimeContainer);

        List<WebElement> timeList = pickTimeContainer.findElements(By.tagName("li"));

        if (!driver.isElementVisible(walkInsWelcome, Constants.ONE)) {
            for (WebElement timeEle : timeList) {
                if (!timeEle.getAttribute(Constants.CLASS).contains(ITEM_DISABLED)
                        && !timeEle.getAttribute(Constants.CLASS).contains(ITEM_SELECTED)
                        && !timeEle.getAttribute(Constants.CLASS).contains(ITEM_HIGHLIGHTED)
                        && !timeEle.getAttribute(Constants.ROLE).contains(PRESENTATION)) {
                    driver.jsScrollToElement(timeEle);

                    if (Config.isFirefox()) {
                        timeEle.sendKeys(Keys.ENTER);
                    } else {
                        timeEle.click();
                    }

                    driver.waitForMilliseconds(Constants.TWO_THOUSAND);
                    driver.waitForElementClickable(webDriver.findElement(CommonActions.availableApptTimesBy));
                    setPassableTime(appointmentTime.getAttribute(Constants.VALUE));
                    break;
                }
            }
        } else {
            if (appointmentDayStart < APPOINTMENT_DAYS_ATTEMPTED) {
                appointmentDayStart++;
                commonActions.clickButtonByText(CANCEL);
                selectDate();
                selectTime();
            }
        }

        // Verifies that attempts does not equal number of days attempted variable
        if (appointmentDayStart == APPOINTMENT_DAYS_ATTEMPTED) {
            Assert.fail("FAIL: There were no available appointment times for dates leading up until: \""
                    + getPassableDate() + "\"");
        }

        driver.waitForMilliseconds();
        LOGGER.info("selectTime completed");
    }

    /**
     * Enters customer information on the Appointment screen
     *
     * @param customer Customer type to grab dtc.data from
     */
    public void enterCustomerInformation(Customer customer) {
        LOGGER.info("enterCustomerInformation started");
        if (driver.isElementDisplayed(firstNameBoxAppointmentFlow))
            enterCustomerInformationAppointmentFlow(customer);
        else
        	enterCustomerInformationServiceFlow(customer);
        LOGGER.info("enterCustomerInformation completed");
    }

    /**
     * Enters customer information on the Appointment screen landed on via Service Flow
     *
     * @param customer Customer type to grab dtc.data from
     */
    private void enterCustomerInformationServiceFlow(Customer customer) {
        LOGGER.info("enterCustomerInformationServiceFlow started");
        WebElement phoneDropDownEle;
        String phoneTypeIdString;

        if (Config.isMobile()) {
            phoneDropDownEle = phoneDropDownMobile;
            phoneTypeIdString = null;
        } else {
            phoneDropDownEle = phoneDropDownWebServiceFlow;
            phoneTypeIdString = phoneTypeIdWeb;
        }
        driver.waitForElementVisible(phoneNumberBoxServiceFlow, Constants.FIVE);

        try {
            firstNameBoxServiceFlow.sendKeys(customer.firstName);
            lastNameBoxServiceFlow.sendKeys(customer.lastName);
            emailBoxServiceFlow.sendKeys(customer.email);

            if (driver.isElementDisplayed(phoneDropDownWebServiceFlow, Constants.TWO)) {
                if (Config.isMobile()) {
                    driver.selectFromDropdownByVisibleText(phoneDropDownEle, customer.phoneType);
                } else {
                    commonActions.selectDropDownValue(phoneTypeIdString, customer.phoneType);
                }
            }
            phoneNumberBoxServiceFlow.sendKeys(customer.phone);
            phoneNumberBoxAppointmentFlow.sendKeys(Keys.TAB);
        } catch (Exception e) {
            Assert.fail("FAIL: Entering customer info for user " +
                    customer.getCustomerDataString(customer) + "! FAILED with error: " + e);
        }
        LOGGER.info("enterCustomerInformationServiceFlow completed");
    }

    /**
     * Enters customer information on the Appointment screen landed on via Appointment Flow
     *
     * @param customer Customer type to grab dtc.data from
     */
    private void enterCustomerInformationAppointmentFlow(Customer customer) {
        LOGGER.info("enterCustomerInformationAppointmentFlow started");
        // TODO:  sendKeys does not work for these fields with IE and Safari.
        // 1) Talk to developers about what is different about these fields?  Can they be changed?
        // 2) Find a solution other than sendKeys.  Robot and JavascriptExecutor have been tried.
        if (Config.isIe() || Config.isSafari()) {
            LOGGER.info("There is a known issue for automation setting the Customer Info "
                    + "fields in IE and Safari which causes failure creating appointments");
        }

        firstNameBoxAppointmentFlow.sendKeys(customer.firstName);
        lastNameBoxAppointmentFlow.sendKeys(customer.lastName);
        emailBoxAppointmentFlow.sendKeys(customer.email);
        commonActions.formListSelectItem(ConstantsDtc.PHONE_TYPE, customer.phoneType);
        phoneNumberBoxAppointmentFlow.sendKeys(customer.phone);
        phoneNumberBoxAppointmentFlow.sendKeys(Keys.TAB);
        CommonActions.address1.sendKeys(customer.address1);
        CommonActions.orderSummaryZipCode.sendKeys(customer.zip);
        CommonActions.orderSummaryZipCode.click();
        commonActions.clickBrowserBody();
        driver.waitForMilliseconds();
        LOGGER.info("enterCustomerInformationAppointmentFlow completed");
    }

    /**
     * Enters customer phone number in either the Appointment flow or Service flow
     *
     * @param phoneNumber the phone number to be entered
     */
    public void enterCustomerPhoneNumber(String phoneNumber) {
        LOGGER.info("enterCustomerPhoneNumber started");
        int min3digit = 100;
        int range3digit = 899;
        int min4digit = 1000;
        int range4digit = 8999;

        WebElement phoneNumberField = null;
        if (driver.isElementDisplayed(phoneNumberBoxAppointmentFlow)) {
            phoneNumberField = phoneNumberBoxAppointmentFlow;
        } else {
            phoneNumberField = phoneNumberBoxServiceFlow;
        }

        if (phoneNumber.equals(Constants.RANDOM)) {
            String areaCode = String.valueOf(CommonUtils.getRandomNumber(min3digit, range3digit));
            String prefix = String.valueOf(CommonUtils.getRandomNumber(min3digit, range3digit));
            String lastFour = String.valueOf(CommonUtils.getRandomNumber(min4digit, range4digit));
            phoneNumber = areaCode + prefix + lastFour;
        }

        try {
            Double.parseDouble(phoneNumber);
            if (phoneNumber.length() != 10) {
                Assert.fail("FAIL: Invalid phone number specified. Expected 10-digit numeric value. Actual: " + phoneNumber);
            }
        } catch (Exception e) {
            Assert.fail("FAIL: Invalid phone number specified. Expected 10-digit numeric value. Actual: " +
                    phoneNumber + ". Exception thrown: " + e.getMessage());
        }

        java.text.MessageFormat phoneMessageFormat = new java.text.MessageFormat("{0}-{1}-{2}");

        String[] phoneNumberArray = {phoneNumber.substring(0, 3),
                phoneNumber.substring(3, 6),
                phoneNumber.substring(6)};

        phoneNumber = phoneMessageFormat.format(phoneNumberArray);

        while (phoneNumberField.getAttribute(Constants.VALUE).length() > 0) {
            phoneNumberField.sendKeys(Keys.BACK_SPACE);
        }

        phoneNumberField.sendKeys(phoneNumber);

        LOGGER.info("enterCustomerPhoneNumber completed");
    }

    /**
     * Enter the information for the other person that will be picking up the products
     *
     * @param customer           - Customer object containing demographic data
     * @param deliveryType       - The type of desired communication: 'phone' or 'email'
     * @param receiveTextUpdates - Whether or not to receive text updates
     */
    public void enterSomeoneElseInformation(Customer customer, String deliveryType, boolean receiveTextUpdates) {
        LOGGER.info("enterSomeoneElseInformation started");
        commonActions.clearAndPopulateEditField(recipientFirstName, customer.firstName);
        commonActions.clearAndPopulateEditField(recipientLastName, customer.lastName);

        if (!deliveryType.equalsIgnoreCase(Constants.PHONE)) {
            if (receiveTextUpdates) {
                Assert.fail("FAIL: Cannot receive text updates option unless phone delivery method is selected");
            }
            webDriver.findElement(recipientEmailAddressRadioLabel).click();
            commonActions.clearAndPopulateEditField(recipientEmail, customer.email);
        } else {
            webDriver.findElement(recipientPhoneNumberRadioLabel).click();
            recipientPhone.clear();
            recipientPhone.sendKeys(customer.phone);
            if (receiveTextUpdates) {
                webDriver.findElement(recipientReceiveTextMessagesLabel).click();
            } else {
                webDriver.findElement(recipientDoNotReceiveTextMessagesLabel).click();
            }
        }
        LOGGER.info("enterSomeoneElseInformation completed");
    }

    /**
     * Clicks 'Edit' next to Appointment Details
     * Allows you to edit those details
     */
    public void clickAppointmentEditLink() {
        LOGGER.info("clickAppointmentEditLink started");
        driver.waitForElementVisible(appointmentEditLink);
        driver.jsScrollToElement(appointmentEditLink);
        appointmentEditLink.click();
        LOGGER.info("clickAppointmentEditLink completed");
    }

    /**
     * Clicks 'Change Store' on Appointment Details page
     */
    public void clickChangeStore() {
        LOGGER.info("clickChangeStore started");
        driver.waitForElementVisible(changeStoreLink);
        driver.jsScrollToElement(changeStoreLink);
        changeStoreLink.click();
        LOGGER.info("clickChangeStore completed");
    }

    /**
     * Returns the Date for day verification *
     *
     * @return String
     */
    public String extractDate() {
        LOGGER.info("extractDate started");
        String date = null;
        String year = null;

        if (driver.isElementDisplayed(CommonActions.apptSelectedMsg)) {
            // For appointment flow, get date from the message bar displayed on Checkout page.
            // Format it so it will match the appointment page.  i.e. Wednesday, January 24, 2018.
            date = CommonActions.apptSelectedMsg.getText().split("-")[0].replaceAll(ConstantsDtc.APPOINTMENT_MESSAGE_BAR_LABEL +
                    "\n", "").trim();
            if (!Config.isMobile()) {
                driver.waitForMilliseconds();
                String monthYr = apptMonthAndYear.getText();
                year = monthYr.substring(monthYr.length() - 4, monthYr.length());
                date = date + ", " + year;
            }
        } else {
            // Service flow
            date = appointmentDate.getText();
        }

        setPassableDate(date);
        LOGGER.info("extractDate completed");
        return getPassableDate();
    }

    /**
     * Returns the Time for time verification
     *
     * @return String
     */
    public String extractTime() {
        LOGGER.info("extractTime started");
        String time = null;

        if (driver.isElementDisplayed(CommonActions.apptSelectedMsg)) {
            // For appointment flow, get time from the message bar displayed on Checkout page.
            String msgBarDT = CommonActions.apptSelectedMsg.getText();
            time = msgBarDT.split("-")[1].trim();

            if (time.substring(0, 1).equals("0")) {
                String hh = time.split(":")[0];
                String mm = time.split(":")[1];
                hh = hh.substring(1, 2);
                time = hh + ":" + mm;
            }
        } else {
            // Service flow
            time = appointmentTime.getText();
        }
        setPassableTime(time);
        LOGGER.info("extractTime completed");
        return getPassableTime();
    }

    /**
     * Validates current date and time listed on the Appointment page
     *
     * @param date Appointment Date to verify.  Example format:  Wednesday, January 24, 2018
     * @param time Appointment Time to verify.  Example format:  11:00 AM
     */
    public void verifyDateAndTime(String date, String time) {
        LOGGER.info("verifyDateAndTime started");
        driver.waitForElementVisible(appointmentDateTime);

        String apptDT = appointmentDateTime.getText().replace(ConstantsDtc.TIME_LABEL + " ", "");
        String apptDate = apptDT.split("\n")[1].trim();
        String apptTime = apptDT.split("\n")[2].trim();

        if (!CommonUtils.containsIgnoreCase(date, Constants.JUNE) && !CommonUtils.containsIgnoreCase(date, Constants.JULY)) {
            if (CommonUtils.containsIgnoreCase(date, Constants.SEPTEMBER)) {
                date = date.replace(Constants.SEPTEMBER, Constants.SEPT);
            } else {
                date = CommonUtils.replaceShortMonthWithLongMonth(date);
            }
        }

        Assert.assertTrue("FAIL: Did NOT see expected date \"" + date + "\" in appointment info: \""
                + apptDate + "\"!", apptDate.equals(date));
        LOGGER.info("Confirmed that expected date \"" + date + "\" was listed in appointment info");

        Assert.assertTrue("FAIL: Did NOT see expected time \"" + time + "\" in appointment info: \""
                + apptTime + "\"!", apptTime.contains(time));
        LOGGER.info("Confirmed that expected time (" + time + ") was listed in appointment info");

        LOGGER.info("verifyDateAndTime completed");
    }

    /**
     * Verifies the store on screen is the current default store
     *
     * @param store the default store
     */
    public void verifyStore(String store) {
        driver.waitForTextPresent(CommonActions.checkoutSummaryStoreAddressBy, store, Constants.THREE);
        String actualStoreInfo = webDriver.findElement(CommonActions.checkoutSummaryStoreAddressBy).getText().toLowerCase();
        Assert.assertTrue("FAIL: Did NOT see expected store \"" + store + "\" in appointment info: \"" +
                actualStoreInfo + "\"!", actualStoreInfo.contains(store.toLowerCase()));
        LOGGER.info("Confirmed that expected store (" + store + ") was listed in appointment info");
    }

    /**
     * Overloading selectDate method to pick the Last, First date available based on the example
     *
     * @param day       The desired date to select
     * @param availDays The number of available days to validate against
     */
    public void selectDateAndVerifyAvailableDays(String day, String availDays) {
        LOGGER.info("selectDateAndVerifyAvailableDays started");
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        driver.jsScrollToElementClick(webDriver.findElement(CommonActions.apptDatesDayBy));

        driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        driver.waitForElementVisible(CheckoutPage.apptDateContainer);
        driver.waitForMilliseconds();

        String valFirstMonth = currentMonth.getText();
        List<WebElement> firstMonthList = CheckoutPage.apptDateContainer.findElements(By.className(pickDayInfocus));
        for (int firstmonthdays = 0; firstmonthdays < firstMonthList.size(); firstmonthdays++) {
            if (firstMonthList.get(firstmonthdays).getAttribute(Constants.CLASS).contains(pickDayDisabled)) {
                firstMonthList.remove(firstmonthdays);
                firstmonthdays = firstmonthdays - 1;
                LOGGER.info("First month days : " + firstmonthdays + " and the size: " + firstMonthList.size());
            }
        }
        int totalSize = (firstMonthList.size());

        if (day.equalsIgnoreCase(Constants.FIRST) && firstMonthList.size() > 1) {
            firstMonthList.get(1).click();
        } else {
            nextMonthArrow.click();
            String valSecondMonth = currentMonth.getText();
            List<WebElement> secondMonthList = CheckoutPage.apptDateContainer.findElements(By.className(pickDayInfocus));

            for (int secondmonthdays = 0; secondmonthdays < secondMonthList.size(); secondmonthdays++) {
                if (secondMonthList.get(secondmonthdays).getAttribute(Constants.CLASS).contains(pickDayDisabled)) {
                    secondMonthList.remove(secondmonthdays);
                    secondmonthdays = secondmonthdays - 1;
                    LOGGER.info("Second month days: " + secondmonthdays + " and the size: " + secondMonthList.size());
                }
            }
            if (!valSecondMonth.equals(valFirstMonth)) {
                totalSize = totalSize + (secondMonthList.size());
            }
            if (day.equalsIgnoreCase(Constants.FIRST)) {
                secondMonthList.get(0).click();
            } else {
                nextMonthArrow.click();
                String valThirdMonth = currentMonth.getText();
                List<WebElement> thirdMonthList = CheckoutPage.apptDateContainer.findElements(By.className(pickDayInfocus));
                for (int thirdmonthdays = 0; thirdmonthdays < thirdMonthList.size(); thirdmonthdays++) {
                    if (thirdMonthList.get(thirdmonthdays).getAttribute(Constants.CLASS).contains(pickDayDisabled)) {
                        thirdMonthList.remove(thirdmonthdays);
                        thirdmonthdays = thirdmonthdays - 1;
                        LOGGER.info("Third month days: " + thirdmonthdays + " and the size: " + thirdMonthList.size());
                    }
                }
                if (!valSecondMonth.equals(valThirdMonth)) {
                    totalSize = totalSize + (thirdMonthList.size());
                }

                int availDateIndex = thirdMonthList.size() - 1;
                thirdMonthList.get(availDateIndex).click();
            }
        }
        setPassableDate(appointmentDate.getAttribute(Constants.VALUE));
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        //TODO - due to a defect following code will be commented out, we will continue finalizing the appointment
        String totalDays = Integer.toString(totalSize);
        //Assert.assertTrue("FAIL: \"Total number of available appointment days " + availDays + "\" doesn't match with actual appointment days. " + totalSize + "\""
        //         ,availDays.equals(totalDays));*/
        LOGGER.info("Total number of available appointment days" + availDays + "Actual appointment days" + totalDays);
        LOGGER.info("selectDateAndVerifyAvailableDays completed");

    }

    /**
     * Clicks the "I want to enter my address..." checkbox and enters customer address information on the
     * Appointment screen
     *
     * @param apptCustomer Customer type to grab dtc.data from
     */
    public void enterCustomerAddressInformation(Customer apptCustomer) {
        LOGGER.info("enterCustomerAddressInformation started");
        try {
            driver.waitForElementClickable(enterAddressCheckbox);
            driver.jsScrollToElement(enterAddressCheckbox);
            enterAddressCheckbox.click();

            ArrayList<WebElement> addressFields = new ArrayList<>(
                    Arrays.asList(address1, address2, city, zip));

            ArrayList<String> customerInfo = new ArrayList<>(Arrays.asList
                    (apptCustomer.address1, apptCustomer.address2, apptCustomer.city, apptCustomer.zip));

            for (int i = 0; i < addressFields.size(); i++) {
                if (Config.isAndroidTablet() || Config.isAndroidPhone()) {
                    driver.waitForMilliseconds();
                }
                addressFields.get(i).sendKeys(customerInfo.get(i));
            }

            if (apptCustomer.country.contains(Constants.CANADA)) {
                commonActions.selectDropDownValue(ConstantsDtc.countryDropDownString, apptCustomer.country);
                driver.waitForMilliseconds();
            }

            if (Config.isMobile()) {
                driver.selectFromDropdownByVisibleText(stateDropDownMobile, apptCustomer.state);
            } else {
                commonActions.selectDropDownValue(ConstantsDtc.stateDropDownString, apptCustomer.state);
            }
        } catch (Exception e) {
            Assert.fail("FAIL: Entering address for user \"" +
                    apptCustomer.getCustomerDataString(apptCustomer) + "\"! FAILED with error: " + e);
        }
        LOGGER.info("enterCustomerAddressInformation completed");
    }


    /**
     * Verifies that the range of available appointment times
     */
    public void verifyAppointmentTimeRange() {
        LOGGER.info("verifyAppointmentTimeRange started");
        String range1 = "8:00 AM";
        String range2 = "5:45 AM";

        if (CommonUtils.containsIgnoreCase(apptTimeListHeaderDate.getText(), Constants.SATURDAY)) {
            range2 = "4:45 AM";
        }

        String actualRange1, actualRange2;

        driver.jsScrollToElementClick(webDriver.findElement(CommonActions.availableApptTimesBy));

        driver.waitForMilliseconds();
        List<WebElement> timeList = webDriver.findElements(pickTimeContainerBy);

        if (Config.isIphone()) {
            actualRange1 = getAppointmentTimeValueJs(true);
            actualRange2 = getAppointmentTimeValueJs(false);
        } else {
            actualRange1 = timeList.get(0).getText();
            actualRange2 = timeList.get(timeList.size() - 1).getText();
        }

        Assert.assertTrue("FAIL: The first time in the appointment range was NOT \"" + range1 + "\", " +
                "but was rather \"" + actualRange1 + "\"!", range1.contains(actualRange1));

        LOGGER.info("Confirmed the first time in the range, \"" + range1 + "\".");

        Assert.assertTrue("FAIL: The last time in the appointment range was NOT \"" + range2 + "\" " +
                "but was rather \"" + actualRange2 + "\"!", range2.contains(actualRange2));

        LOGGER.info("Confirmed the last time in the range, \"" + range2 + "\".");

        LOGGER.info("verifyAppointmentTimeRange completed");
    }

    /**
     * Builds javascript string for validation of time range for iPhone Simulator
     *
     * @param first Whether it is the first or last time in the range
     * @return String  The appointment time value
     */
    public String getAppointmentTimeValueJs(boolean first) {
        LOGGER.info("getAppointmentTimeValueJs started");
        String appointmentTimeValue = null;
        String verifyRangeScript = "times = document.getElementsByClassName('picker__list-item');";

        if (first) {
            verifyRangeScript = verifyRangeScript + "return times[0].textContent";
        } else {
            verifyRangeScript = verifyRangeScript + "return times[times.length-1].textContent";
        }

        JavascriptExecutor jse = (JavascriptExecutor) webDriver;
        appointmentTimeValue = (String) jse.executeScript(verifyRangeScript);

        LOGGER.info("getAppointmentTimeValueJs completed");
        return appointmentTimeValue;
    }

    /**
     * Click the Edit Appointment link
     */
    public void clickEditAppointmentLink() {
        LOGGER.info("clickEditAppointmentLink started");
        editApptLink.click();
        driver.waitForPageToLoad();
        LOGGER.info("clickEditAppointmentLink completed");
    }

    /**
     * Click the 'Select date' or 'Select time' box on the schedule appointment page
     */
    public void clickSelectDateOrTime(String selection) {
        LOGGER.info("clickSelectDateOrTime started");
        driver.waitForPageToLoad();
        WebElement elementToSelect = null;

        if (selection.equalsIgnoreCase(Constants.DATE)) {
            elementToSelect = webDriver.findElement(CommonActions.apptDatesDayBy);
        } else {
            elementToSelect = webDriver.findElement(CommonActions.availableApptTimesBy);
        }

        driver.waitForElementClickable(elementToSelect);
        driver.jsScrollToElement(elementToSelect);
        elementToSelect.click();
        LOGGER.info("clickSelectDateOrTime completed");
    }

    /**
     * Verify the DatePicker message is correct
     */
    public void verifyDatePickerMessage() {
        LOGGER.info("verifyDatePickerMessage started");
        WebElement messageDivParagraph = datepickerMessage.findElement(CommonActions.ptagNameBy);
        String displayedMessage = messageDivParagraph.getText().trim();
        Assert.assertTrue("FAIL:  The date picker Message at the top of the select "
                        + "date dialog was not correct.  Expected:  "
                        + ConstantsDtc.DATEPICKER_MESSAGE + ".  Actual:  " + displayedMessage + "!",
                displayedMessage.equals(ConstantsDtc.DATEPICKER_MESSAGE));
        LOGGER.info("verifyDatePickerMessage completed");
    }

    /**
     * Close the Appointment Selected message bar by clicking the small x on the right side
     */
    public void closeAppointmentSelectedMessageBar() {
        LOGGER.info("closeAppointmentSelectedMessageBar started");
        CommonActions.apptSelectedMsg.findElement(CommonActions.closeButtonXBy).click();
        LOGGER.info("closeAppointmentSelectedMessageBar completed");
    }

    /**
     * Verify the Appointment Selected message bar is closed
     */
    public void assertAppointmentSelectedMessageIsNotDisplayed() {
        LOGGER.info("assertAppointmentSelectedMessageIsNotDisplayed started");
        Assert.assertTrue("FAIL:  Unable to close the Appointment Selected message bar!",
                !driver.isElementDisplayed(CommonActions.apptSelectedMsg));
        LOGGER.info("assertAppointmentSelectedMessageIsNotDisplayed completed");
    }

    /**
     * Verify the Appointment Details indicate the appointment placed was for peak hours
     * and the completion time is 45 minutes after the start time.
     */
    public void verifyPeakHoursInAppointmentDetails() {
        LOGGER.info("verifyPeakHoursAppointmentInAppointmentDetails started");
        driver.waitForElementVisible(appointmentDateTime);
        String appointmentDetails = appointmentDateTime.getText();

        DateFormat formatter = new SimpleDateFormat("HH:mm");
        String startTime = appointmentDetails.split(PEAK)[0].trim().split(TIME)[1].replace("(", "").trim();

        Assert.assertTrue("FAIL:  '(Peak)' does not appear to the right of start time in the Appointment Details section!",
                appointmentDetails.contains(startTime + " (" + PEAK + ")"));

        String estCompletionTime = appointmentDetails.split(EST_COMPLETION_TIME)[1].replace("i", "").trim();
        Time startTimeValue = null;
        Time completionTimeValue = null;

        try {
            startTimeValue = new java.sql.Time(formatter.parse(startTime.replaceAll("AM|PM", "").trim()).getTime());
            completionTimeValue = new java.sql.Time(formatter.parse(estCompletionTime.replaceAll("AM|PM", "").trim()).getTime());
        } catch (ParseException e) {
            e.getMessage();
            Assert.fail("FAIL:  Unable to convert " + startTime + " to Time object!");
        }

        long milliseconds = completionTimeValue.getTime() - startTimeValue.getTime();
        int seconds = (int) milliseconds / 1000;
        int hours = seconds / 3600;
        int minutes = ((seconds % 3600) / 60 + (hours * 60));

        Assert.assertTrue("FAIL:  The estimated completion time (" + estCompletionTime + ") "
                + "in Appointment Details is not " + Constants.PEAK_DELAY_MINUTES
                + " after the start time (" + startTime + ")!", minutes == Constants.PEAK_DELAY_MINUTES);
        LOGGER.info("verifyPeakHoursAppointmentInAppointmentDetails completed");
    }

    /**
     *
     * @param serviceOption Selected service option text to verify
     */
    public void assertSelectedServiceOptionText(String serviceOption) {
        LOGGER.info("assertSelectedServiceOptionText started");
        Assert.assertTrue("FAIL: Selected Service Option : \"" + serviceOption + "\" was not displayed",
                driver.isElementDisplayed(driver.getElementWithText(serviceOptionMessageBy, serviceOption)));
        LOGGER.info("assertSelectedServiceOptionText completed");
    }

    /**
     * Verify section title is displayed on Service Appointment Page
     *
     * @param sectionTitle section title text to verify
     */
    public void assertActiveSectionTitleMessageIsDisplayed(String sectionTitle) {
        LOGGER.info("assertActiveSectionTitleMessageIsDisplayed started");
        driver.waitForElementVisible(activeSectionTitleElement);
        String activeSectionTitleMessage = activeSectionTitleElement.getText();
        Assert.assertTrue("FAIL: Section title : \"" + sectionTitle + "\" does NOT match with displayed title " + "\""
                + activeSectionTitleMessage + "\"!", activeSectionTitleMessage.equalsIgnoreCase(sectionTitle));
        LOGGER.info("assertActiveSectionTitleMessageIsDisplayed completed");
    }

    /**
     * Verify Action Needed Modal appointment action button is displayed
     *
     * @param actionNeededApptButton CHECKOUT WITH APPOINTMENT|APPOINTMENT ONLY
     */
    public void assertActionNeededButtonIsDisplayed(String actionNeededApptButton) {
        LOGGER.info("assertActionNeededButtonIsDisplayed started");
        driver.waitForMilliseconds();

        if (Config.isSafari() || Config.isIe() || Config.isFirefox())
            driver.waitForPageToLoad();
        WebElement actionButton = driver.getElementWithText(CommonActions.dtButtonBy, actionNeededApptButton);
        if (!driver.isElementDisplayed(actionButton))
            Assert.fail("FAIL: Appointment action button " + actionButton + " did NOT display on Action Needed Modal");
        LOGGER.info("assertActionNeededButtonIsDisplayed completed");
    }

    /**
     * Click CONFIRM APPOINTMENT button on Installation Appointment page
     */
    public void clickConfirmAppointment() {
        LOGGER.info("clickConfirmAppointment started");
        commonActions.clickFormSubmitButtonByText(ConstantsDtc.CONFIRM_APPOINTMENT);
        driver.waitForPageToLoad();
        LOGGER.info("clickConfirmAppointment completed");
    }

    /**
     * This method saves the first ten appointment dates on the appointment details page
     */
    public void saveListOfAllAppointmentDates() {
        LOGGER.info("saveListOfAllAppointmentDates started");
        List<WebElement> appointmentDays = webDriver.findElements(CommonActions.apptDatesDayBy);
        for (int i = 0; i <= appointmentDays.size() - 1; i++) {
            listOfAppointmentDates.add(i, appointmentDays.get(i).getText());
        }
        LOGGER.info("saveListOfAllAppointmentDates completed");
    }

    /**
     * This method selects appointment on specified number of days from the current day
     *
     * @param numberOfDays specifies number of days for selecting the appointment from current day.
     */
    public void selectAppointmentFutureDays(int numberOfDays) {
        LOGGER.info("selectAppointmentFutureDays started");
        String storeStatus = webDriver.findElements(CommonActions.apptDetailsSelectDateActiveBy).get(numberOfDays).getText().toLowerCase();
        if (!storeStatus.contains((ConstantsDtc.STORE_CLOSED).toLowerCase())) {
            driver.waitForElementClickable(webDriver.findElements(CommonActions.apptDetailsSelectDateActiveBy).get(numberOfDays));
            webDriver.findElements(CommonActions.apptDetailsSelectDateActiveBy).get(numberOfDays).click();
            driver.jsScrollToElementClick(webDriver.findElement(CommonActions.availableApptTimesBy));
        } else {
            numberOfDays++;
            selectAppointmentFutureDays(numberOfDays);
        }
        LOGGER.info("selectAppointmentFutureDays completed");
    }

    /**
     * This methods verifies the appointment dates after clicking on edit appointment on the checkout page
     * with the appointments on the appointment details page
     */
    public void assertListOfAllAppointmentDates() {
        LOGGER.info("assertListOfAllAppointmentDates started");
        List<WebElement> appointmentDays = webDriver.findElements(CommonActions.apptDatesDayBy);
        for (int i = 0; i <= appointmentDays.size() - 1; i++) {
            Assert.assertTrue("FAIL: The appointment dates listed under edit appointment details are not the " +
                    "same as Appointment dates Listed on the Appointment page", listOfAppointmentDates.get(i).toLowerCase().
                    equalsIgnoreCase(appointmentDays.get(i).getText().toLowerCase()));
        }
        LOGGER.info("assertListOfAllAppointmentDates completed");
    }

    /**
     * Verifies the presence of date in format "day date displayText" on page.
     * For e.g "Thu 6 New Year" or "Sun 28 Store Closed"
     *
     * @param dateToVerify Date to be verified
     */
    public void assertStoreHolidayDates(String dateToVerify) {
        LOGGER.info("assertStoreHolidayDates started");
        List<WebElement> allElement = webDriver.findElements(CommonActions.apptDetailsSelectDateActiveBy);
        ArrayList<String> UIDates = new ArrayList<String>();
        for (WebElement ell : allElement) {
            String date = ell.getText().replace("\n", " ");
            UIDates.add(date);
        }
        Assert.assertTrue("FAIL: The expected holiday date" + dateToVerify +
                " did not match with the actual holiday dates observed on page.", UIDates.contains(dateToVerify));
        LOGGER.info("assertStoreHolidayDates completed");
    }

    /**
     * Performs  a selection of date using month and date.
     *
     * @param month Month of the year
     * @param day   Day of the month
     */
    public void selectDayInMonth(String month, String day) {
        LOGGER.info("selectDayInMonth started");
        int dayInt = Integer.parseInt(day);
        WebElement dayElement = webDriver.findElement(By.xpath("//div[contains(text(),'" + month + "')]" +
                "//following-sibling::*//strong[contains(text(),'" + dayInt + "')]"));
        dayElement.click();
        driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
        LOGGER.info("selectDayInMonth completed");
    }

    /**
     * Verifies the next day store opening timings as per the type of holiday on page.
     *
     * @param holidayType              can be of any type from
     *                                 "(Full Day Holiday|Sunday Holiday|Partial Day Holiday|Partial Day Local Outage)"
     * @param expectedStoreOpenTimings Expected opening message for next day.
     */
    public void assertNextDayStoreTimings(String holidayType, String expectedStoreOpenTimings) {
        LOGGER.info("assertNextDayStoreTimings started");
        String actualStoreOpenDate = "";
        if (holidayType.contains(FULL_DAY_HOLIDAY)) {
            actualStoreOpenDate = storeFullHolidayMessage.getText();
            actualStoreOpenDate = actualStoreOpenDate.split("reopen")[1]
                    .replace(".", "").trim();
        } else if (holidayType.contains(PARTIAL_DAY_HOLIDAY)
                || holidayType.contains(PARTIAL_DAY_LOCAL_OUTAGE)) {
            driver.jsScrollToElement(storePartialDayHolidayMessage);
            actualStoreOpenDate = storePartialDayHolidayMessage.getText();
            actualStoreOpenDate = actualStoreOpenDate.split("reopen")[1]
                    .replace(".", "").trim();
        }
        Assert.assertTrue(
                "FAIL:Next Store Open Timings Displayed on the Page is -> "
                        + actualStoreOpenDate
                        + ". Expected Store Open Timings Should be -> "
                        + expectedStoreOpenTimings, actualStoreOpenDate
                        .equalsIgnoreCase(expectedStoreOpenTimings));
        LOGGER.info("assertNextDayStoreTimings completed");
    }

    /**
     * Verifies the store holiday message as per the type of holiday on page.
     *
     * @param holidayType          can be of any type from
     *                             "(Full Day Holiday|Sunday Holiday|Partial Day Holiday|Partial Day Local Outage)"
     * @param expectedStoreMessage Expected message to be verified for the holiday.
     */
    public void assertStoreHolidayMessage(String holidayType, String expectedStoreMessage) {
        LOGGER.info("assertStoreHolidayMessage started");
        String actualStoreMessage = null;
        if (holidayType.contains(FULL_DAY_HOLIDAY)) {
            actualStoreMessage = storeFullHolidayMessage.getText();
        } else if (holidayType.contains(PARTIAL_DAY_HOLIDAY)) {
            actualStoreMessage = storePartialDayHolidayMessage.getText();
        } else if (holidayType.contains(PARTIAL_DAY_LOCAL_OUTAGE)) {
            actualStoreMessage = storePartialDayHolidayMessage.getText();
        } else if (holidayType.contains(SUNDAY_HOLIDAY)) {
            actualStoreMessage = sundayHolidayMessage.getText();
        }
        Assert.assertTrue("FAIL:Store Message Displayed on the page is -> "
                        + actualStoreMessage + ". Expected Store Message Should be -> "
                        + expectedStoreMessage,
                actualStoreMessage.equals(expectedStoreMessage));
        LOGGER.info("assertStoreHolidayMessage completed");
    }

    /**
     * Verifies the Holiday message on Appointment page
     *
     * @param expectedStoreReason Expected holiday reason to be displayed on page.
     */
    public void assertStoreHolidayComment(String expectedStoreReason) {
        LOGGER.info("assertStoreHolidayComment started");
        String actualReason = storeReasonCode.getText();
        Assert.assertTrue("FAIL:Store  Reason Comment Displayed on the page is -> "
                        + actualReason + ". Expected Reason Comment Should be"
                        + expectedStoreReason,
                actualReason.equalsIgnoreCase(expectedStoreReason));
        LOGGER.info("assertStoreHolidayComment completed");
    }

    /**
     * Verifies the Appointment Header as per the month and day selected.
     *
     * @param expectedMonth Expected month on header.
     * @param expectedDay   Expected day on header.
     */
    public void assertAppointmentHeader(String expectedMonth, String expectedDay) {
        LOGGER.info("assertAppointmentHeader started");
        String actualDateOnHeader = apptTimeListHeaderDate.getText().replace("\n", "");
        String actualDate = actualDateOnHeader.split(" ")[1] + " " + actualDateOnHeader.split(" ")[2];
        String expectedDate1 = CommonUtils.replaceLongMonthWithShortMonth(expectedMonth) + " " + expectedDay;
        String expectedDate2 = expectedMonth + " " + expectedDay;
        Assert.assertTrue("FAIL:Date Displayed on the header is " + actualDate + ". " +
                        "Expected Date on the Header Should be " + expectedDate1 + " or " + expectedDate2,
                (actualDate.equalsIgnoreCase(expectedDate1) || actualDate.equalsIgnoreCase(expectedDate2)));
        LOGGER.info("assertAppointmentHeader completed");
    }

    /**
     * Verifies if the element is present in Appointment page
     *
     * @param elementName Name of the element
     * @param status   displayed or not displayed.
     */
    public void assertElementDisplayedNotDisplayed(String elementName, String status) {
        LOGGER.info("assertElementDisplayedNotDisplayed started for " + elementName);
        if (status.equalsIgnoreCase(Constants.DISPLAYED)) {
            Assert.assertTrue("FAIL: Element is not displayed", driver.isElementDisplayed(returnElement(elementName)));
        } else if (status.equalsIgnoreCase(Constants.NOT_DISPLAYED)) {
            Assert.assertFalse("FAIL: Element is displayed", driver.isElementDisplayed(returnElement(elementName)));
        }
        LOGGER.info("assertElementDisplayedNotDisplayed completed for " + elementName);
    }

    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("assertElementIsDisplayed started for " + elementName);
        switch (elementName) {
            case SHOP_TIRES:
                return shopTires;
            case SHOP_WHEELS:
                return shopWheels;
            default:
                Assert.fail("FAIL: Could not find element name = " + elementName + " that matched string passed from step");
                return null;
        }
    }

    /**
     * Verifies "My Information" text displayed on the appointment details or appointment confirmation page
     *
     * @param customer customer type
     * @param page     appointment confirmation or appointment details
     */
    public void assertMyInformationDisplayed(Customer customer, String page) {
        LOGGER.info("assertMyInformationDisplayed started on " + page + " for " + customer);
        By myInfo;
        String customerInfo;
        if (page.equalsIgnoreCase(ConstantsDtc.APPOINTMENT_CONFIRMATION))
            myInfo = myInformationBy;
        else {
            driver.waitForElementVisible(myInformationDetailsPageBy);
            myInfo = myInformationDetailsPageBy;
        }

        String myInformationText = webDriver.findElement(myInfo).getText();
        List<String> myInformationData = new ArrayList<>();
        myInformationData.add(customer.firstName + " " + customer.lastName);
        myInformationData.add(customer.email.toLowerCase());
        myInformationData.add(customer.phone.replace("-", ""));
        if (page.equalsIgnoreCase("appointment confirmation"))
            customerInfo = "[" + myInformationText.substring(15).replace("\n", ", ").
                    replace("-", "").toLowerCase() + "]";
        else
            customerInfo = "[" + myInformationText.replace("\n", ", ").replace("-", "") + "]";
        String expectedInformation = myInformationData.toString().toLowerCase();
        boolean informationMatch = customerInfo.toLowerCase().contains(expectedInformation);
        Assert.assertTrue("FAIL: Incorrect customer information is displayed.Expected information: '"
                + expectedInformation + "' Actual information: '" + customerInfo + "'.", informationMatch);
        myInformationData.clear();
        LOGGER.info("assertMyInformationDisplayed completed on " + page + " for " + customer);
    }

    /**
     * Verify whether the 'No vehicle selected' message is displayed on the Service Appointment page
     */
    public void assertNoVehicleSelectedMessageDisplayed() {
        LOGGER.info("assertNoVehicleSelectedMessageDisplayed started");
        commonActions.assertElementWithTextIsVisible(serviceOptionMessageBy, ConstantsDtc.NO_VEHICLE_SELECTED);
        LOGGER.info("assertNoVehicleSelectedMessageDisplayed completed");
    }

    /**
     * Verify whether the specified link is displayed on the Service Appointment page
     *
     * @param linkText - The text of the link to verify
     */
    public void assertAddVehicleLinkDisplayed(String linkText) {
        LOGGER.info("assertAddVehicleLinkDisplayed started");
        By byElement;
        if (linkText.equalsIgnoreCase("add vehicle")) {
            byElement = appointmentSummaryActionsBy;
        }
        else {
            byElement = vehicleInfoFormHeaderContainerBy;
        }
        commonActions.assertElementWithTextIsVisible(byElement, linkText);
        LOGGER.info("assertAddVehicleLinkDisplayed completed");
    }

    /**
     * Click on add vehicle either by 'add vehicle' link or 'add new vehicle' link
     *
     * @param linkText 'add vehicle' or 'add new vehicle'
     */
    public void clickAddVehicleOnServiceAppointmentPage(String linkText) {
        LOGGER.info("clickAddVehicleOnServiceAppointmentPage started");
        if (linkText.equalsIgnoreCase(ConstantsDtc.ADD_VEHICLE))
            driver.jsScrollToElementClick(addVehicleLink, false);
        else
            driver.jsScrollToElementClick(driver.getElementWithText(addNewVehicleBy, linkText), false);
        LOGGER.info("clickAddVehicleOnServiceAppointmentPage completed");
    }

    /**
     * Verify the selected vehicle on the Service Appointment page is as expected.
     *
     * @param expectedVehicle Concatenated string composed of "Year make model trim" of vehicle
     */
    public void assertSelectedVehicleOnServiceAppointmentPage(String expectedVehicle) {
        LOGGER.info("assertSelectedVehicleOnServiceAppointmentPage started");
        String selectedVehicle = selectedVehicleContainer.findElement(CommonActions.vehicleDescriptionBy).getText();
        Assert.assertTrue("FAIL: The expected vehicle was not selected on the Service Appointment page. " +
                        "Expected: " + expectedVehicle + ". Actual: " + selectedVehicle,
                selectedVehicle.replace("\n", "").replace(" ","").toLowerCase().
                        startsWith(expectedVehicle.replace(" ", "").toLowerCase()));
        LOGGER.info("assertSelectedVehicleOnServiceAppointmentPage completed");
    }

    /**
     * Verify the edit vehicle link exists on the selected vehicle and no other vehicle on the Service Appointments page
     */
    public void assertEditVehicleLinkOnServiceAppointmentPage() {
        LOGGER.info("assertEditVehicleLinkOnServiceAppointmentPage started");
        List<WebElement> vehicleContainers = webDriver.findElements(serviceAppointmentVehicleContainerBy);
        for (WebElement vehicleContainer : vehicleContainers) {
            boolean editVehicleLinkExists = driver.elementExists(vehicleContainer, editVehicleServiceAppointmentBy);
            String vehicleDescription = vehicleContainer.findElement(CommonActions.vehicleDescriptionBy).getText();
            if (vehicleContainer.getAttribute(Constants.CLASS).
                    equals(selectedVehicleContainer.getAttribute(Constants.CLASS))) {
                Assert.assertTrue("FAIL: The selected vehicle " + vehicleDescription +
                                " did not contain the edit vehicle link", editVehicleLinkExists);
            } else {
                Assert.assertFalse("FAIL: The unselected vehicle " + vehicleDescription +
                                " contains the edit vehicle link", editVehicleLinkExists);
            }
        }
        LOGGER.info("assertEditVehicleLinkOnServiceAppointmentPage completed");
    }

    /**
     * Select the specified vehicle on the Service Appointments page
     *
     * @param vehicle Concatenated string composed of "Year make model trim" of vehicle
     */
    public void selectVehicleOnServiceAppointmentPage(String vehicle) {
        LOGGER.info("assertSelectedVehicleOnServiceAppointmentPage started");
        List<WebElement> vehicleContainers = webDriver.findElements(serviceAppointmentVehicleContainerBy);
        boolean found = false;
        for (WebElement vehicleContainer : vehicleContainers) {
            String vehicleDescription = vehicleContainer.findElement(CommonActions.vehicleDescriptionBy).getText();
            if (vehicleDescription.replace("\n", "").replace(" ","").toLowerCase().
                    startsWith(vehicle.replace(" ", "").toLowerCase())) {
                if (vehicleContainer.getAttribute(Constants.CLASS).
                        equals(selectedVehicleContainer.getAttribute(Constants.CLASS))) {
                    LOGGER.info(vehicle + " is already selected on Service Appointment page. No action taken.");
                } else {
                    driver.jsScrollToElementClick(vehicleContainer, false);
                    driver.waitOneSecond();
                }
                found = true;
                break;
            }
        }
        Assert.assertTrue("FAIL: Could not find " + vehicle + " on the Service Appointment page", found);
        LOGGER.info("assertSelectedVehicleOnServiceAppointmentPage completed. " + vehicle + " selected.");
    }

    /**
     * Verify the tire size badge and label are as expected for specified vehicle
     *
     * @param vehicle Concatenated string composed of "Year make model trim" of vehicle
     * @param expectedBadgeText Expected text on the badge, such as "O.E.", "+1", "+2", "-1", etc.
     */
    public void assertTireSizeBadgeAndLabelOnServiceAppointmentPage(String vehicle, String expectedBadgeText) {
        LOGGER.info("assertTireSizeBadgeAndLabelOnServiceAppointmentPage started for " + vehicle +
                " with tire size badge " + expectedBadgeText);
        driver.waitOneSecond();
        String expectedSizeLabel = "Optional tire size";
        if (expectedBadgeText.equalsIgnoreCase(ConstantsDtc.O_E))
            expectedSizeLabel = "Factory tire size";

        if (driver.isElementDisplayed(vehicleSummaryContainer, Constants.ONE)) {
            String actualBadgeText = vehicleSummaryContainer.findElement(CommonActions.fitmentSizeBadgeBy).getText();
            Assert.assertTrue("FAIL: Incorrect tire size badge for selected vehicle. Expected: " + expectedBadgeText +
                    ". Actual: " + actualBadgeText, actualBadgeText.startsWith(expectedBadgeText));
            String actualSizeLabel = vehicleSummaryContainer.findElement(CommonActions.fitmentSizeLabelBy).getText();
            Assert.assertTrue("FAIL: Incorrect tire size label for selected vehicle. Expected: " + expectedSizeLabel +
                    ". Actual: " + actualSizeLabel, actualSizeLabel.startsWith(expectedSizeLabel));
        } else {
            List<WebElement> vehicleContainers = webDriver.findElements(serviceAppointmentVehicleContainerBy);
            boolean found = false;
            for (WebElement vehicleContainer : vehicleContainers) {
                String vehicleDescription = vehicleContainer.findElement(CommonActions.vehicleDescriptionBy).getText();
                if (vehicleDescription.replace("\n", "").replace(" ","").toLowerCase().
                        startsWith(vehicle.replace(" ", "").toLowerCase())) {
                    String actualBadgeText = vehicleContainer.findElement(CommonActions.fitmentSizeBadgeBy).getText();
                    Assert.assertTrue("FAIL: Incorrect tire size badge for selected vehicle. Expected: " +
                            expectedBadgeText + ". Actual: " + actualBadgeText,
                            actualBadgeText.startsWith(expectedBadgeText));
                    String actualSizeLabel = vehicleContainer.findElement(CommonActions.fitmentSizeLabelBy).getText();
                    Assert.assertTrue("FAIL: Incorrect tire size label for selected vehicle. Expected: " +
                            expectedSizeLabel + ". Actual: " + actualSizeLabel,
                            actualSizeLabel.startsWith(expectedSizeLabel));
                    found = true;
                    break;
                }
            }
            Assert.assertTrue("FAIL: Could not find " + vehicle + " on the Service Appointment page", found);
        }
        LOGGER.info("assertTireSizeBadgeAndLabelOnServiceAppointmentPage completed for " + vehicle +
                " with tire size badge " + expectedBadgeText);
    }

    /**
     * Extract Date and time and set Scenario data
     */
    public void extractAppointmentDateTimeAndSetScenarioData() {
        LOGGER.info("extractAppointmentDateTimeAndSetScenarioData started");
        String appointmentDate = extractDate();
        String appointmentTime = extractTime();
        driver.scenarioData.genericData.put(OrderXmls.APPOINTMENT_DATE,
                commonActions.convertDateToYearMonthDateFormat(appointmentDate));
        driver.scenarioData.genericData.put(OrderXmls.APPOINTMENT_START_TIME, commonActions.convertToMilitaryTime(appointmentTime));
        driver.scenarioData.genericData.put(ConstantsDtc.APPOINTMENT_DATE, appointmentDate);
        driver.scenarioData.genericData.put(ConstantsDtc.APPOINTMENT_TIME, appointmentTime);
        LOGGER.info("extractAppointmentDateTimeAndSetScenarioData completed");
    }
}