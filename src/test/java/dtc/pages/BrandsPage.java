package dtc.pages;


import common.Config;
import common.Constants;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created by mnabizadeh on 10/20/16.
 */

public class BrandsPage {

    private Driver driver;
    private WebDriver webDriver;
    private final Logger LOGGER = Logger.getLogger(BrandsPage.class.getName());

    public BrandsPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        PageFactory.initElements(webDriver, this);
    }

    private static String brandName = "page-BRANDtire-";

    private static final By brandRefinementBy = By.className("refinements__selected");

    private static final By fitmentComponentBy = By.className("fitment-component__toggle-button");

    private static final By dropDownOptionBy = By.cssSelector("[class*='dropdown-menu']");

    @FindBy(className = "auto-shop-btn-all-season-tires")
    public static WebElement shopAllSeasonButton;

    @FindBy(className = "auto-shop-btn-all-terrain-tires")
    public static WebElement shopAllTerrainButton;

    @FindBy(className = "auto-shop-btn-performance-tires")
    public static WebElement shopPerformanceButton;

    @FindBy(className = "auto-shop-btn-summer-tires")
    public static WebElement shopSummerButton;

    @FindBy(className = "auto-shop-btn-touring-tires")
    public static WebElement shopTouringButton;

    @FindBy(className = "auto-shop-btn-truck-suv-tires")
    public static WebElement shopTruckSuvButton;

    @FindBy(className = "auto-shop-btn-competition-tires")
    public static WebElement shopCompetitionTires;

    @FindBy(className = "auto-shop-btn-winter-tires")
    public static WebElement shopWinterTires;

    @FindBy(css = "[class*='brand-category-page__content']")
    public static WebElement brandCategoryPageContent;

    /**
     * Selects category of tire option to select based on input
     *
     * @param subCategory category of tire to select
     */
    public void clickShopBrandCategoryTires(String subCategory) {
        LOGGER.info("clickShopBrandCategoryTires started");
        driver.jsScrollToElement(shopAllSeasonButton);
        driver.waitForElementClickable(shopAllSeasonButton);

        if (subCategory.equals(shopAllSeasonButton.getText()))
            shopAllSeasonButton.click();
        else if (subCategory.equals(shopAllTerrainButton.getText()))
            shopAllTerrainButton.click();
        else if (subCategory.equals(shopPerformanceButton.getText()))
            shopPerformanceButton.click();
        else if (subCategory.equals(shopSummerButton.getText()))
            shopSummerButton.click();
        else if (subCategory.equals(shopTouringButton.getText()))
            shopTouringButton.click();
        else if (subCategory.equals(shopTruckSuvButton.getText()))
            shopTruckSuvButton.click();
        else if (subCategory.equals(shopCompetitionTires.getText()))
            shopCompetitionTires.click();
        else if (subCategory.equals(shopWinterTires.getText()))
            shopWinterTires.click();
        LOGGER.info("clickShopBrandCategoryTires completed");
    }

    /**
     * Verifies the brand that was selected appears on the page
     *
     * @param brand the brand of tire that has been selected
     */
    public void verifyBrandSelected(String brand) {
        LOGGER.info("verifyBrandSelected started");
        driver.jsScrollToElement(shopAllSeasonButton);
        driver.waitForElementVisible(shopAllSeasonButton);
        final By brandElement = By.className(brandName.concat(brand.split(" ")[0].toLowerCase()));
        Assert.assertTrue("FAIL: The brand: \"" + brand + "\" was NOT found on the page!",
                webDriver.findElement(brandElement).getText().toUpperCase().trim()
                        .contains(brand.toUpperCase()));
        LOGGER.info("verifyBrandSelected completed");
    }

    /**
     * Verifies the selected brand category appears in the refinements area
     *
     * @param refinement selected category / refinement to appear under refinements
     *                   section
     */
    public void verifyBrandRefinementSelected(String refinement) {
        if (Config.isMobile()) {

            LOGGER.info("Skipped the refinement validation on Mobile");
        } else {

            LOGGER.info("verifyBrandRefinementSelected started");
            boolean refinementFound = false;
            // selecting brand refinement if refinement is contained row text
            List<WebElement> rows = webDriver.findElements(brandRefinementBy);

            if (Config.isIe())
                driver.waitForMilliseconds(Constants.TWO);
            //TODO: added a counter for future reference,  Not required at this moment
            //but for later if we have a specific req w.r.t positioning and/or ordering
            int i = 0;
            for (WebElement row : rows) {

                if (Config.isIe())
                    driver.waitForMilliseconds();

                if (row.getText().trim().contains(refinement)) {
                    refinementFound = true;
                    LOGGER.info(
                            "Confirmed that Refinement '" + refinement + "' was listed in the fitment search results.");
                    break;
                } else {
                    i++;
                }
            }

            Assert.assertTrue("FAIL: Refinement '" + refinement + "' was not listed in the fitment search results.",
                    refinementFound);
            LOGGER.info("verifyBrandRefinementSelected completed");
        }
    }

    /**
     * Selects the given brand based on image 'alt' tag text on the Brands page
     *
     * @param brand selected company brand to appear on the 'alt' tag text
     */
    public void selectBrandImage(String brand) {
        LOGGER.info("selectBrandImage started for '" + brand + "'");
        WebElement brandImage = null;
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        if (brand.equalsIgnoreCase(Constants.FIRST)) {
            try {
                brandImage = webDriver.findElement(ProductBrandsPage.imageBy);
            } catch (Exception e) {
                brandImage = webDriver.findElement(ProductBrandsPage.imageAltTextBy);
            }
        } else {
            try {
                brandImage = driver.getElementWithAttribute(ProductBrandsPage.imageBy,
                        Constants.ATTRIBUTE_ALT, brand);
                if (brandImage == null)
                    brandImage = driver.getElementWithText(ProductBrandsPage.imageAltTextBy, brand);
            } catch (Exception e) {
                brandImage = driver.getElementWithText(ProductBrandsPage.imageAltTextBy, brand);
            }
        }
        Assert.assertNotNull("FAIL: Could not find brand image on Brands page", brandImage);
        driver.webElementClick(brandImage);
        driver.resetImplicitWaitToDefault();
        LOGGER.info("selectBrandImage completed for '" + brand + "'");
    }

    /**
     * Select category for Brand via fitment
     *
     * @param category Wheels or Tires
     */
    public void selectBrand(String category) {
        LOGGER.info("selectBrand started for " + category);
        driver.jsScrollToElementClick(driver.getElementWithText(fitmentComponentBy, category));
        LOGGER.info("selectBrand completed for " + category);
    }

    /**
     * Selects the 'view brands that don’t fit your vehicle' drop down on All brands page
     */
    public void selectViewBrandsThatDontFit() {
        LOGGER.info("selectViewBrandsThatDontFit started");
        WebElement dropDownOption = webDriver.findElement(dropDownOptionBy);
        dropDownOption.click();
        LOGGER.info("selectViewBrandsThatDontFit complete");
    }

    /**
     * Verify the Brand Category page displayed for the selected Brand
     *
     * @param brand  Brand Name
     */
    public void assertBrandCategoryPage(String brand) {
        LOGGER.info("assertBrandCategoryPage started for " + brand);
        driver.waitForElementVisible(brandCategoryPageContent);
        Assert.assertTrue("FAIL: The brand: \"" + brand + "\" was NOT found on the Brand Category Page!",
                brandCategoryPageContent.getText().toLowerCase().contains(brand.toLowerCase()));
        LOGGER.info("assertBrandCategoryPage completed for " + brand);
    }
}