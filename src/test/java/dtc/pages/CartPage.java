package dtc.pages;

import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import dtc.data.Customer;
import dtc.steps.CommonActionsSteps;
import orderxmls.pages.OrderXmls;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.CommonUtils;
import utilities.Driver;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Time;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static dtc.pages.TireAndWheelPackagesPage.tireWheelPackageDetails;
import static dtc.pages.TireAndWheelPackagesPage.wheelSelectionBy;
import static dtc.steps.CommonActionsSteps.cartProductPrice;

/**
 * Created by aaronbriel on 9/22/16.
 */
public class CartPage {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private Customer customer;
    private final Logger LOGGER = Logger.getLogger(CartPage.class.getName());

    public CartPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        customer = new Customer();
        PageFactory.initElements(webDriver, this);
    }

    public static HashMap<String, String> productPriceOnCart = new HashMap<>();
    public static HashMap<String, WebElement> productParent = new HashMap<>();
    public static HashMap<String, List<WebElement>> feeParents = new HashMap<>();
    public static HashMap<String, String> itemCodeQuantityMap = new HashMap<>();
    public static HashMap<String, String> hashMap = new HashMap<>();

    private static final String SUBTOTAL = "Cart Subtotal";
    private static final String ITEM_REMOVED_MESSAGE = "Your Cart Is Empty";
    private static final String QUANTITY_UPDATED_MESSAGE = "Product quantity has been updated.";
    private static final String CERTIFICATE_PROTECT_YOUR_PURCHASE = "Certificates - Protect Your Purchase";
    public static final String CERTIFICATE_REPAIR_REFUND_REPLACEMENT_MESSAGE = "Protect your purchase. Cover your " +
            "tires up to 3 years, regardless of road hazard or wear, down to legal tread depth (3/32\")";

    private static final String REMOVED = "Removed";
    public static final String TPMS_REBUILD_KIT = "TPMS Rebuild Kits";
    private static final String TPMS_SENSOR = "TPMS Sensor";
    private static final String CLEAR_CART = "Clear my cart and Continue";
    private static final String CONTINUE_AND_CLEAR_CURRENT_CART = "Continue & Clear Current Cart";
    public static final String VIEW_CART = "View cart";
    private static final String NOT_ELIGIBLE = "Not Eligible";
    private static final String EXPIRED = "Expired";
    public static String environmentFee;
    private static String actualCertificateLabelText = "";
    private static String TIRE_WHEEL_PACKAGES = "TIRE & WHEEL PACKAGE";
    private static final String REMOVE_WHEELS_LINK = "Remove Wheels link";
    private static final String REMOVE_TIRES_LINK = "Remove Tires link";
    private static final String CHANGE_WHEELS_LINK = "Change Wheels link";
    private static final String CHANGE_TIRES_LINK = "Change Tires link";
    private static final String DELETE_PACKAGE_LINK = "Delete package link";
    private static final String PRESENT = "present";
    private static final String NOT_PRESENT = "not present";
    private static final String XPATH_CONTAINS_TEXT = "//*[contains(text(),'";

    public static final By cartItemQuantityBy = By.className("cart-item__item-quantity");

    public static final By childTip = By.cssSelector("span.display-inline-block-sm");

    private static final By orderListPartsInfo = By.className("order-list__parts-info");

    public static final By cartItemCodeNonStaggeredBy = By.cssSelector("[class*='product-details__item-code']");

    public static final By cartItemCodeStaggeredBy = By.className("cart-item__product-code");

    private static final By productSpec = By.className("cart-item__product-specs");

    private static final By switchOptions = By.cssSelector("[class*='fitment-warning__button']");

    private static final By orderFeeServiceItemLabelBy = By.className("cart-item__fee-details");

    private static final By feeServiceItemLabelBy = By.className("cart-item__option-name");

    public static final By cartItemsBy = By.className("cart-item");

    private static final By productDetailsOrdersBy = By.cssSelector("[class*='product-details__container']");

    private static final By certForRRPopUpBy = By.className("checkout-form__second-effort-action");

    public static final By feeDetailsItemsRowParentBy = By.className("cart-item__row");

    public static final By feeDetailsItemsRowLabelBy = By.className("cart-item__column--info");

    public static final By feeDetailsItemsRowPriceBy = By.className("cart-item__column--price");

    public static final By staggeredCertificatePriceBy =
            By.cssSelector("[class*='product-certificates-staggered__col-price']");

    private static final By serviceFeeItemRowPriceBy = By.className("cart-item__column--each");

    public static final By itemRowQuantityBy = By.className("cart-item__column--qty");

    public static final By cartItemDetailsParentBy = By.className("cart-item__details");

    public static final By cartSummaryBreakDownNameBy = By.className("cart-summary__breakdown-name");

    public static final By cartSummaryBreakDownPriceBy = By.className("cart-summary__breakdown-price");

    private static final By itemPriceEachBy = By.className("cart-item__item-each");

    private static final By itemPriceNonStaggeredBy = By.className("cart-item__item-price");

    private static final By itemPriceStaggeredBy = By.cssSelector("[class*='staggered-item__price___']");

    private static final By cartItemProductBy = By.className("cart-item__product");

    public static final By removeItemBy = By.className("cart-item__remove-item");

    public static final By qualifyPromotionAmountBy = By.className("cart-item__rebate-amount--instant");

    private static final By cartSummaryHeadingBy = By.className("cart-summary__heading");

    private static final By cartItemTotalPriceBy = By.className("cart-item__total-price");

    private static final By cartItemProductSize = By.className("cart-item__product-size");

    private static final By cartItemSubtotalLabelBy = By.className("cart-item__subtotal-label");

    private static final By cartItemSubtotal = By.className("cart-item__subtotal-amount");

    private static final By viewCartButtonBy = By.className("auto-popover-view-cart-button");

    private static final By continueShoppingButtonBy = By.className("auto-popover-continue-shopping-button");

    private static final By miniCartItemNameBy = By.className("mini-cart__item-name");

    private static final By miniCartItemQuantityBy = By.className("mini-cart__item-quantity");

    private static final By miniCartItemTotalBy = By.className("mini-cart__item-total");

    private static final By miniCartItemInfoBy = By.className("mini-cart__item-info");

    private static final By miniCartTotalBy = By.className("mini-cart__total");

    public static final By modalSwitchCartBy = By.cssSelector(".modal-switch-cart>h3");

    private static final By cartItemProductImageBy = By.className("cart-item__product-image");

    public static final By cartItemRebateNameBy = By.className("cart-item__rebate-name");

    private static final By cartSummaryInstantSavingBy = By.className("cart-summary__instant-savings-amount");

    public static final By cartItemRowBy = By.className("cart-item__row--space-between");

    private static final By cartItemFitBy = By.className("cart-item__fit");

    private static final By miniCartItemPriceBy = By.className("mini-cart__item-price");

    public static final By hideSmBy = By.className("hide-sm");

    public static final By cartItemStoreInventoryCheckBy = By.className("cart-item__store-inventory-check");

    public static final By cartStoreStockCountBy = By.className("cart-item__store-stock-count");

    public static final By cartItemRowHeaderBy = By.className("cart-item__row-header");

    public static final By cartItemStoreStockBy = By.className("cart-item__store-stock");

    public static final By certificatesCartErrorMsgBy =
            By.cssSelector("[class*='certificate-errors__certificate-errors__container']");

    public static final By cartSummaryBy = By.className("cart-summary");

    public static final By tireAndWheelPackagesListBy = By.cssSelector("[class*='cart-item__details cart-products__shopping-cart-wrapper']");

    public static final By removeTireslinkBy = By.xpath("(//*[contains(@class,'cart-item__package-inventory__specs__remove')]) [1]");

    public static final By removeWheelsLinkBy = By.xpath("(//*[contains(@class,'cart-item__package-inventory__specs__remove')]) [2]");

    public static final By changeTiresLinkBy = By.xpath("(//*[contains(@class,'cart-item__package-inventory__specs__change')])[1]");

    public static final By changeWheelsLinkBy = By.xpath("(//*[contains(@class,'cart-item__package-inventory__specs__change')])[2]");

    public static final By deleteLinkBy = By.xpath("//*[contains(@class,'cart-item__package__header__action')]//*[contains(text(),'delete')]");

    public static final By viewTireOrWheelsButtonBy = By.className("cart-empty__cta-link");

    public static final By feeContentRowBy = By.className("fee-content-row");

    public static final By addOnContentRowBy = By.className("add-on-content-row");

    public static final By addOnPriceEachBy = By.className("cart-item__column--each");

    public static final By addOnQuantityBy = By.className("cart-item__column--qty");

    public static final By addOnPriceBy = By.className("cart-item__column--price");

    public static final By staggeredDetailsFrontContainer =
            By.cssSelector("[class*='staggered-details__front-container'");

    public static final By staggeredDetailsRearContainer =
            By.cssSelector("[class*='staggered-details__rear-container'");

    public static final By staggeredDetailsContainerBy = By.cssSelector("[class*='staggered-details__container']");

    public static final By staggeredItemContainerBy = By.cssSelector("[class*='staggered-item__container']");

    public static final By staggeredCertificatesContainerBy =
            By.cssSelector("[class*='product-certificates-staggered__container']");

    public static final By tpmsPriceBy =
            By.cssSelector("[class*='cart-item__column cart-item__column--price'][class*='product-tpms__row-item']");

    public static final By productPromotionsContainerStandardBy = By.cssSelector("[class*='product-promotions__container']");

    public static final By productPromotionsContainerStaggeredBy =
            By.cssSelector("[class*='product-promotions-staggered__container']");

    public static final By itemInstantRebateBy = By.cssSelector("[class*='cart-item__rebate-amount--instant']");

    public static final By cartCertificateColumnPriceEachStandardBy =
            By.cssSelector("[class*='product-certificates__col-each']");

    public static final By cartCertificateColumnQuantityStandardBy =
            By.cssSelector("[class*='product-certificates__col-qty']");

    public static final By cartCertificateColumnPriceStandardBy =
            By.cssSelector("[class*='product-certificates__col-price']");

    public static final By cartCertificateRowStandardBy = By.cssSelector("[class*='product-certificates__grid-row']");

    public static final By cartCertificateColumnPriceEachStaggeredBy =
            By.cssSelector("[class*='product-certificates-staggered__col-each']");

    public static final By cartCertificateColumnQuantityStaggeredBy =
            By.cssSelector("[class*='product-certificates-staggered__col-qty']");

    public static final By cartCertificateColumnPriceStaggeredBy =
            By.cssSelector("[class*='product-certificates-staggered__col-price']");

    public static final By cartCertificateRowStaggeredBy =
            By.cssSelector("[class*='product-certificates-staggered__grid-row']");

    public static final By cartItemsListPackageBy = By.xpath("//*[@class='cart-item__product'] | " +
            "//*[contains(@class,'cart-details-wrapper cart-products__cart-details-wrapperPackage')]");

    public static final By zipcodeFieldCheckoutBy = By.cssSelector("[class*='form-group__label']");

    @FindBy(css = "[class*='product-details__hide-small-viewports']")
    public static WebElement webView;

    @FindBy(css = "[class*='product-details__hide-big-viewports']")
    public static WebElement mobileView;

    @FindBy(css = "#certificateEntryAddForm > button")
    public static WebElement certificateToolTip;

    public static final By myStoreCityHeaderBy = By.className("header__my-store-city");

    @FindBy(className = "cart-item__item-each")
    public static WebElement priceBox;

    @FindBy(className = "cart-item__store-address-city-state-zip")
    public static WebElement cartPageStoreState;

    @FindBy(className = "order-list__total")
    public static WebElement preTotal;

    @FindBy(css = ".cart-item__item-price")
    public static WebElement itemTotal;

    @FindBy(css = "[class*='mini-cart--empty']")
    public static WebElement miniCartClear;

    //TODO Refactor Needed For Xpath(s) When Auto-Class Attribute Available

    @FindBy(className = "cart-item__option-name")
    public static WebElement certificateForRRRLabel;

    @FindBy(xpath = "//div[@class='order-summary__tax']/span")
    public static WebElement salesTax;

    @FindBy(xpath = "//div[@class='order-list__warranty']/strong")
    public static WebElement totalMilesWarranty;

    @FindBy(className = "tooltip-content--open")
    public static WebElement toolTipContent;

    public static WebElement getCertificateToolTip() {
        return certificateToolTip;
    }

    @FindBy(xpath = "//div[@class='order-list__pressure']/strong")
    public static WebElement tireMaxPressure;

    @FindBy(className = "order-summary__price")
    public static WebElement totalPriceInclTax;

    @FindBy(xpath = "//a[contains(@href, 'alltires')]")
    public static WebElement continueShoppingForTires;

    @FindBy(xpath = "//a[contains(@href, 'allwheels')]")
    public static WebElement continueShoppingForWheels;

    @FindBy(xpath = "//a[contains(@class, 'staticAccessoryMoreOptions')]")
    public static WebElement moreOptionsLink;

    @FindBy(className = "shopping-cart")
    public static WebElement shoppingCart;

    @FindBy(className = "auto-results-row-cartbutton")
    public static WebElement addToCartButton;

    @FindBy(className = "results-row__compare")
    public static WebElement resultRowCompare;

    @FindBy(css = ".header__cart-quick-total.display-lg")
    public static WebElement miniCartPrice;

    @FindBy(xpath = "//span[contains(text(), " +
            "'Certificate')]/following-sibling::span[@class='mini-cart__item-quantity']")
    public static WebElement miniCartRRACertItemQuantity;

    @FindBy(xpath = "//span[contains(text(), " +
            "'Certificate')]/parent::div/following-sibling::div[@class='mini-cart__item-price']")
    public static WebElement miniCartRRACertBasePrice;

    @FindBy(xpath = "//span[contains(text(), " +
            "'Certificate')]/parent::div/following-sibling::div[@class='mini-cart__item-total']")
    public static WebElement miniCartRRACertTotalPrice;

    @FindBy(xpath = "//button[contains(text(), '" + CONTINUE_AND_CLEAR_CURRENT_CART + "')]")
    public static WebElement continueAndClearCurrentCartButton;

    @FindBy(xpath = "//button[contains(text(), '" + ConstantsDtc.VIEW_SHOPPING_CART + "')]")
    public static WebElement viewShoppingCart;

    @FindBy(css = ".modal-switch-cart>a")
    public static WebElement modalSwitchCancel;

    @FindBy(className = "cart-item__subtotal-amount")
    public static WebElement subTotal;

    @FindBy(className = "order-summary__tax")
    public static WebElement tax;

    @FindBy(className = "order-summary__savings")
    public static WebElement savings;

    @FindBy(id = "looseMoreOptions")
    public static WebElement tpmsMoreOpts;

    @FindBy(xpath = "//strong[contains(.,'TPMS Sensor')]")
    public static WebElement tpmsSensor;

    @FindBy(css = "input[data-accessorytype='SENSORS']~span")
    public static WebElement tpmsSensorRadioBtn;

    @FindBy(xpath = "//div[@class='tip-container' and contains(.,'TPMS Sensor')]")
    public static WebElement tpmsSensorLabel;

    @FindBy(className = "cart-summary__checkout")
    public static WebElement checkoutButton;

    @FindBy(css = "[class*='paypal-button']")
    public static WebElement paypalCheckout;

    @FindBy(className = "cart-wrapper")
    public static WebElement orderList;

    @FindBy(id = "js-site-search-input")
    public static WebElement searchBox;

    @FindBy(css = "a[class*='certificate-errors__learn-more']")
    public static WebElement learnMoreLink;

    @FindBy(className = "modal-switch-cart")
    public static WebElement switchModal;

    @FindBy(css = "[class*='dt-button-lg dt-button-lg--primary']")
    public static WebElement clearCartConfirmation;

    @FindBy(css = "[class*='mini-cart__decline-button']")
    public static WebElement keepCart;

    @FindBy(css = "[class*='cart-item__buy-certificates-link']")
    public static WebElement buyCertificatesLink;

    @FindBy(css = "[class*='checkout-form__dtcc-maxymiser-modal-position'] [class*='dt-modal__title']")
    public static WebElement certificatesNewModal;

    @FindBy(className = "mini-cart")
    public static WebElement miniCart;

    private static final String CLEAR_CART_CONFIRMATION = "Clear cart confirmation";
    private static final String KEEP_CART = "Keep cart";
    private static final String WHY_BUY_CERTIFICATES = "Why buy certificates";
    private static final String CERTIFICATES_LINK_NEW_MODAL = "Certificates new modal";

    /**
     * Calculate Certificate Fee for DT
     *
     * @param item product code
     */
    public double getCalculatedCertFeeDt(String item) {
        LOGGER.info("getCalculatedCertFeeDt started");
        double itemPrice;
        if (item.contains(Constants.NONE)) {
            itemPrice = commonActions.cleanMonetaryStringToDouble(webDriver.findElement(itemPriceEachBy).getText());
        } else {
            itemPrice = commonActions.getItemPrice(item);
        }
        double certFee = 0.00;
        if (itemPrice < 0.00) {
            Assert.fail("FAIL: Item Price can't be negative, ItemPrice :" + itemPrice);
        } else if (itemPrice <= 15.99) {
            certFee = 4.50;
        } else if (itemPrice <= 30.99) {
            certFee = 6.00;
        } else if (itemPrice <= 35.99) {
            certFee = 7.50;
        } else if (itemPrice <= 40.99) {
            certFee = 8.00;
        } else if (itemPrice <= 50.99) {
            certFee = 10.50;
        } else if (itemPrice <= 65.99) {
            certFee = 11.00;
        } else if (itemPrice <= 75.99) {
            certFee = 13.50;
        } else if (itemPrice <= 90.99) {
            certFee = 14.00;
        } else if (itemPrice <= 100.99) {
            certFee = 16.00;
        } else if (itemPrice <= 115.99) {
            certFee = 18.50;
        } else if (itemPrice <= 125.99) {
            certFee = 20.00;
        } else if (itemPrice <= 135.99) {
            certFee = 21.50;
        } else if (itemPrice <= 145.99) {
            certFee = 23.00;
        } else if (itemPrice <= 155.99) {
            certFee = 24.00;
        } else if (itemPrice <= 165.99) {
            certFee = 26.00;
        } else if (itemPrice >= 166) {
            certFee = Double.parseDouble(new DecimalFormat("#.##").format(itemPrice * 0.17));
            int certFeeInteger = (int) certFee;
            double diff = certFee - certFeeInteger;
            if (diff == 0.00) {
                return certFee;
            } else if (diff <= 0.25) {
                return certFeeInteger + 0.25;
            } else if (diff <= 0.50) {
                return certFeeInteger + 0.50;
            } else if (diff <= 0.75) {
                return certFeeInteger + 0.75;
            } else {
                return certFeeInteger + 1;
            }
        }
        LOGGER.info("getCalculatedCertFeeDt completed");
        return certFee;
    }

    /**
     * Calculate Certificate Fee for DTD
     *
     * @param item product code
     */
    public double getCalculatedCertFeeDtd(String item) {
        LOGGER.info("getCalculatedCertFeeDtd started");
        double itemPrice;
        if (item.contains(Constants.NONE)) {
            itemPrice = commonActions.cleanMonetaryStringToDouble(webDriver.findElement(itemPriceEachBy).getText());
        } else {
            itemPrice = commonActions.getItemPrice(item);
        }
        double certFee = Double.parseDouble(new DecimalFormat("#.##").format(itemPrice * 0.12));
        if (itemPrice < 0.00) {
            Assert.fail("FAIL: Item Price can't be negative, ItemPrice :" + itemPrice);
        } else if (itemPrice <= 0.01) {
            certFee = 0.01;
        } else {
            int certFeeInteger = (int) certFee;
            double diff = certFee - certFeeInteger;
            if (diff == 0.00) {
                return certFee;
            } else if (diff <= 0.25) {
                return certFeeInteger + 0.25;
            } else if (diff <= 0.50) {
                return certFeeInteger + 0.50;
            } else if (diff <= 0.75) {
                return certFeeInteger + 0.75;
            } else {
                return certFeeInteger + 1;
            }
        }
        LOGGER.info("getCalculatedCertFeeDtd completed");
        return certFee;
    }

    /**
     * Calculate Certificate Fee for AT
     *
     * @param item product code
     */
    public double getCalculatedCertFeeAt(String item) {
        LOGGER.info("getCalculatedCertFeeAt started");
        double itemPrice;
        if (item.contains(Constants.NONE)) {
            itemPrice = commonActions.cleanMonetaryStringToDouble(webDriver.findElement(itemPriceEachBy).getText());
        } else {
            itemPrice = commonActions.getItemPrice(item);
        }
        double certFee = Constants.ZERO;
        if (itemPrice < Constants.ZERO) {
            Assert.fail("FAIL: Item Price can't be negative, ItemPrice :" + itemPrice);
        } else if (itemPrice <= 35.00) {
            certFee = 4.50;
        } else if (itemPrice <= 50.00) {
            certFee = 6.50;
        } else if (itemPrice <= 75.00) {
            certFee = 9.50;
        } else if (itemPrice <= 90.00) {
            certFee = 13.00;
        } else if (itemPrice <= 100.00) {
            certFee = 13.50;
        } else if (itemPrice <= 115.00) {
            certFee = 15.50;
        } else if (itemPrice >= 115.01) {
            certFee = Double.parseDouble(new DecimalFormat("#.##").format(itemPrice * 0.14));
            int certFeeInteger = (int) certFee;
            double diff = certFee - certFeeInteger;
            if (diff == 0.00) {
                return certFee;
            } else if (diff <= 0.25) {
                return certFeeInteger + 0.25;
            } else if (diff <= 0.50) {
                return certFeeInteger + 0.50;
            } else if (diff <= 0.75) {
                return certFeeInteger + 0.75;
            } else {
                return certFeeInteger + 1;
            }
        }
        LOGGER.info("getCalculatedCertFeeAt completed");
        return certFee;
    }

    /**
     * Calculate FET Fee
     *
     * @param itemCode The int item to check
     */
    public double getCalculatedFETFee(int itemCode) {
        LOGGER.info("getCalculatedFETFee started");
        int itemQuantity = Integer.parseInt(webDriver.findElement(CommonActions.cartItemQuantityBy).getAttribute(Constants.VALUE));
        double fetFee = Constants.ZERO;
        if (itemCode == 10001) {
            fetFee = 8.00 * itemQuantity;
        } else if (itemCode == 10082 || itemCode == 10083 || itemCode == 10090 || itemCode == 10097) {
            fetFee = 1.00 * itemQuantity;
        } else if (itemCode == 10204) {
            fetFee = 10.01 * itemQuantity;
        } else if (itemCode == 47359) {
            fetFee = 5.48 * itemQuantity;
        } else if (itemCode == 47360) {
            fetFee = 0.19 * itemQuantity;
        }
        LOGGER.info("getCalculatedFETFee completed");
        return fetFee;
    }

    /**
     * Calculate Disposal Fee for DT & AT
     *
     * @param item product code
     */
    public double getCalculatedDisposalFee(String item) {
        LOGGER.info("getCalculatedDisposalFee started");
        double itemPrice = 0.00;
        double disposalFee = 0.00;
        if (item.contains(Constants.NONE)) {
            itemPrice = commonActions.cleanMonetaryStringToDouble(webDriver.findElement(itemPriceEachBy).getText());
        } else {
            itemPrice = commonActions.getItemPrice(item);
        }
        if (commonActions.getCurrentUrl().contains(ConstantsDtc.AT) ||
                commonActions.getCurrentUrl().contains(ConstantsDtc.AMERICAS_TIRE)) {
            if (Config.getDataSet().equalsIgnoreCase(Constants.QA))
                disposalFee = ConstantsDtc.DISPOSAL_FEE_QA_AT_CAL_01;
            else
                disposalFee = ConstantsDtc.DISPOSAL_FEE_STG_AT_CAN_19;
        } else if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT) && itemPrice <= 9999.99) {
            if (Config.getDataSet().equalsIgnoreCase(Constants.QA))
                disposalFee = ConstantsDtc.DISPOSAL_FEE_QA_DT_AZF_01;
            else
                disposalFee = ConstantsDtc.DISPOSAL_FEE_STG_DT_AZP_20;
        } else if (itemPrice < Constants.ZERO) {
            Assert.fail("FAIL: Item Price can't be negative, ItemPrice :" + itemPrice);
        }
        LOGGER.info("getCalculatedDisposalFee completed");
        return disposalFee;
    }

    /**
     * Calculate Environment Fee for DT, AT and DTD
     * Florida charges $1.00 Environment Fee
     *
     * @param item product code
     */
    public double getCalculatedEnvironmentFee(String item) {
        LOGGER.info("getCalculatedEnvironmentFee started");
        double environmentFee = 0.00;
        if (commonActions.getCurrentUrl().contains(ConstantsDtc.AT) ||
                commonActions.getCurrentUrl().contains(ConstantsDtc.AMERICAS_TIRE)) {
            environmentFee = 1.75;
        } else if (Config.getDataSet().equalsIgnoreCase(Constants.STG)
                && !commonActions.getCurrentUrl().contains(ConstantsDtc.AT) ||
                Config.getDataSet().equalsIgnoreCase(Constants.STG)
                        && !commonActions.getCurrentUrl().contains(ConstantsDtc.AMERICAS_TIRE)) {
            environmentFee = 2.00;
        } else {
            double itemPrice;
            if (item.contains(Constants.NONE)) {
                itemPrice = commonActions.cleanMonetaryStringToDouble(webDriver.findElement(itemPriceEachBy).getText());
            } else {
                itemPrice = commonActions.getItemPrice(item);
            }
            if (itemPrice < 100.00) {
                if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD) ||
                        Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT) &&
                                !webDriver.findElement(myStoreCityHeaderBy).getText().
                                        contains(ConstantsDtc.MELBOURNE)) {
                    environmentFee = Double.parseDouble(new DecimalFormat("#.##").format(itemPrice * 0.02));
                } else {
                    environmentFee = 1.00;
                }
            } else if ((Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT) ||
                    Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD))) {
                environmentFee = 2.00;
                if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)
                        && webDriver.findElement(myStoreCityHeaderBy).getText().contains(ConstantsDtc.MELBOURNE)) {
                    environmentFee = 1.00;
                }
            } else if (itemPrice < Constants.ZERO) {
                Assert.fail("FAIL: Item Price can't be negative, ItemPrice :" + itemPrice);
            }
        }
        LOGGER.info("getCalculatedEnvironmentFee completed");
        return environmentFee;
    }

    /**
     * Asserts item passed in is either present or not present on the shopping cart page
     *
     * @param item                  Selected item/product to verify appears on page
     * @param expectedDisplayStatus Sets the verification expectation as to whether the item should or should NOT be displayed
     *                              on the Cart page
     * @param page                  Cart or Order History Detail page
     */
    public void assertItemOnPage(String item, String expectedDisplayStatus, String page) {
        LOGGER.info("assertItemOnPage started for " + page);
        List<WebElement> elements;

        driver.waitForPageToLoad();

        //TODO: retest when new safaridriver is stable
        if (Config.isSafari() || Config.isMobile() || Config.isFirefox()) {
            driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        }
        if (page.equalsIgnoreCase(Constants.CART)) {
            elements = webDriver.findElements(cartItemsBy);
        } else {
            elements = webDriver.findElements(productDetailsOrdersBy);
        }

        String actualDisplayStatus = "";
        for (WebElement element : elements) {
            if (expectedDisplayStatus.equalsIgnoreCase(Constants.DISPLAYED)) {
                actualDisplayStatus = Constants.NOT_DISPLAYED;
                if (driver.checkIfElementContainsText(element, item, true)) {
                    actualDisplayStatus = Constants.DISPLAYED;
                    LOGGER.info("Confirmed \"" + item + "\" was visible on cart page.");
                    break;
                }
            } else {
                actualDisplayStatus = Constants.DISPLAYED;
                if (!driver.checkIfElementContainsText(element, item, true)) {
                    actualDisplayStatus = Constants.NOT_DISPLAYED;
                    LOGGER.info("Confirmed \"" + item + "\" was NOT visible on cart page.");
                    break;
                }
            }
        }

        Assert.assertTrue("FAIL: Item '" + item + "' was " + actualDisplayStatus + " on the cart page when it was expected"
                + " NOT to be displayed!", expectedDisplayStatus.equalsIgnoreCase(actualDisplayStatus));

        LOGGER.info("assertItemOnPage completed for " + page);
    }

    /**
     * Asserts that the current product quantity matches desired/default quantity in Product Page
     *
     * @param value The quantity value to validate
     */
    public void assertProductQuantityOnCartPage(String value) {
        LOGGER.info("assertProductQuantityOnCartPage started");
        int index = 0;
        if (Config.isMobile()) {
            index = 1;
        }
        WebElement cartItemQuantity = webDriver.findElements(cartItemQuantityBy).get(index);
        driver.waitForElementVisible(cartItemQuantity);
        if (cartItemQuantity.getTagName().equalsIgnoreCase(Constants.INPUT)) {
            Assert.assertTrue("FAIL: The actual product quantity: \"" + cartItemQuantity.getAttribute(Constants.VALUE)
                            + " does NOT match expected: \"" + value + "\"!",
                    cartItemQuantity.getAttribute(Constants.VALUE).equalsIgnoreCase(value));
            LOGGER.info("Confirmed that default product quantity \"" + value +
                    "\" matches with rendered quantity ==> " + cartItemQuantity.getAttribute(Constants.VALUE));
        } else {
            Assert.assertTrue("FAIL: The actual certificate quantity: \"" + cartItemQuantity.getText()
                            + " does NOT match expected: \"" + value + "\"!",
                    cartItemQuantity.getText().equalsIgnoreCase(value));
            LOGGER.info("Confirmed that default certificate quantity \"" + value +
                    "\" matches with rendered quantity ==> " + cartItemQuantity.getText());
        }
        LOGGER.info("assertProductQuantityOnCartPage completed");
    }

    /**
     * Clicks 'delete' on shopping cart page
     */
    public void clickDeleteIconForItem() {
        LOGGER.info("clickDeleteIconForItem started");

        //TODO: retest when new safaridriver is stable
        if (Config.isSafari())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        List<WebElement> deleteItems = webDriver.findElements(removeItemBy);
        WebElement deleteIcon = deleteItems.get(0);
        if (Config.isMobile())
            deleteIcon = deleteItems.get(1);

        WebElement productContainer = deleteIcon;
        do {
            productContainer = driver.getParentElement(productContainer);
        } while (!CommonUtils.containsIgnoreCase(productContainer.getText(), ConstantsDtc.ITEM));
        String[] productInfoArray = productContainer.getText().split(ConstantsDtc.ITEM_NUMBER_PREFIX);
        for (int i = 1; i < productInfoArray.length; i++) {
            String itemNumber = productInfoArray[i].substring(Constants.ZERO, Constants.FIVE);
            commonActions.removeProductInfoListItem(itemNumber);
        }
        driver.jsScrollToElementClick(deleteIcon);
        LOGGER.info("clickDeleteIconForItem completed");
    }

    /**
     * Clicks the "REMOVE TIRE", "REMOVE TIRE SETS" or "REMOVE WHEEL" button on popup modal
     */
    public void clickRemoveTireOrWheelOnModal() {
        String buttonText = Constants.REMOVE + " " + Constants.WHEEL;
        if (driver.isElementDisplayed(driver.getElementWithText(CommonActions.buttonBy, Constants.TIRE)))
            buttonText = Constants.REMOVE + " " + Constants.TIRE;
        commonActions.clickButtonByText(buttonText);
    }

    /**
     * Remove either the front or rear tire from a staggered tire set
     *
     * @param frontOrRear - "front" or "rear"
     */
    public void removeFrontOrRearTires(String frontOrRear) {
        LOGGER.info("removeFrontOrRearTires started removing '" + frontOrRear + "' tires");
        WebElement removeIcon = driver.getElementWithText(CommonActions.dtLinkBy,
                ConstantsDtc.REMOVE + " " + frontOrRear);
        WebElement productContainer = removeIcon;
        do {
            productContainer = driver.getParentElement(productContainer);
        } while (!CommonUtils.containsIgnoreCase(productContainer.getText(), ConstantsDtc.ITEM));
        String itemNumber = productContainer.getText().
                split(Constants.PRODUCT_ARTICLE_NUMBER_LABEL)[1].substring(Constants.ZERO, Constants.FIVE);
        commonActions.removeProductInfoListItem(itemNumber);
        driver.jsScrollToElementClick(removeIcon);
        clickRemoveTireOrWheelOnModal();
        LOGGER.info("removeFrontOrRearTires completed removing '" + frontOrRear + "' tires");
    }

    /**
     * Remove items from cart based on stock status
     *
     * @param stockStatus - 'In Stock' or 'On Order'.  If 'In Stock', remove
     *                    the items that do not have delayed availability.
     *                    if 'On Order', remove the items with delayed availability.
     */
    public void removeItemsByStockStatus(String stockStatus) {
        LOGGER.info("removeItemsByStockStatus started");
        List<WebElement> itemElements = webDriver.findElements(cartItemProductBy);

        if (!stockStatus.equalsIgnoreCase(ConstantsDtc.IN_STOCK) &&
                !stockStatus.equalsIgnoreCase(ConstantsDtc.ON_ORDER)) {
            Assert.fail("Invalid parameter for stock status ('" + stockStatus + "') in "
                    + "CartPage.removeItemsByStockStatus.  Valid values are 'In Stock' or 'On Order'");
        }

        // Iterate the rows from bottom to top keeping item position indices intact.
        // Remove the target items based on specified stock status and current availability.
        for (int itemIndex = itemElements.size() - 1; itemIndex >= 0; itemIndex--) {
            WebElement itemElement = itemElements.get(itemIndex);

            boolean availabilityDelayed = itemElement.getText().trim().contains(
                    ConstantsDtc.AVAILABILITY_3_5_DAYS_CALLTOCONFIRM);

            if ((stockStatus.equalsIgnoreCase(ConstantsDtc.IN_STOCK) && !availabilityDelayed)
                    || (stockStatus.equalsIgnoreCase(ConstantsDtc.ON_ORDER) && availabilityDelayed)) {
                driver.jsScrollToElementClick(itemElement.findElement(removeItemBy));
                LOGGER.info("Removed " + itemElement.getText().split("Item")[0] + " from cart.");
                driver.clickElementWithText(CommonActions.buttonBy, ConstantsDtc.REMOVE_ITEM_FROM_CART);
            }
        }
        LOGGER.info("removeItemsByStockStatus completed");
    }

    /**
     * Asserts the different types of messages that appear on page based on different actions
     *
     * @param messageType used to indicate what type of message should appear on the page
     */
    public void assertHeaderMessage(String messageType) {
        LOGGER.info("assertHeaderMessage started");

        if (Config.isMobile())
            driver.waitForMilliseconds();
        driver.waitForPageToLoad();
        if (messageType.equalsIgnoreCase(REMOVED)) {
            Assert.assertTrue("FAIL: Header element did NOT display \"Item Removed\" message!",
                    webDriver.findElement(CommonActions.headerBy).getText().equalsIgnoreCase(ITEM_REMOVED_MESSAGE));
        } else if (messageType.equalsIgnoreCase(Constants.UPDATED)) {
            Assert.assertTrue("FAIL: Header element did not display \"Quantity Updated\" message!",
                    CommonActions.globalMessage.getText().equalsIgnoreCase(QUANTITY_UPDATED_MESSAGE));
        }
        LOGGER.info("Confirmed that product removed/updated message was displayed.");
        LOGGER.info("assertHeaderMessage completed");
    }

    /**
     * Clears and resets the quantity input box with a new value
     *
     * @param quantity Quantity to update to
     */
    public void updateQuantity(String quantity) {
        LOGGER.info("updateQuantity started");
        driver.waitForElementVisible(CommonActions.cartItemQuantityBy);
        webDriver.findElement(CommonActions.cartItemQuantityBy).click();
        commonActions.clearAndPopulateEditField(webDriver.findElement(CommonActions.cartItemQuantityBy), quantity);

        //TODO: retest when new safaridriver is stable
        if (Config.isSafari()) {
            webDriver.findElement(CommonActions.cartItemQuantityBy).sendKeys(Keys.ENTER);
            driver.waitForMilliseconds();
        } else if (Config.isIe()) {
            priceBox.click();
        } else if (Config.isIphone() || Config.isIpad()) {
            //Clicking parent element to trigger event
            shoppingCart.click();

        } else {
            webDriver.findElement(CommonActions.cartItemQuantityBy).sendKeys(Keys.RETURN);
        }

        driver.waitForMilliseconds();

        LOGGER.info("Entered updated quantity: " + quantity);
        LOGGER.info("updateQuantity completed");
    }

    /**
     * Asserts that the quantity input box is the same as the quantity passed in
     *
     * @param quantity The quantity that the input box should abe showing
     */
    public void assertUpdatedProductQty(String quantity) {
        LOGGER.info("assertUpdatedProductQty started");
        String updatedQuantity = webDriver.findElement(CommonActions.cartItemQuantityBy).getAttribute(Constants.VALUE);
        driver.waitForElementVisible(webDriver.findElement(CommonActions.cartItemQuantityBy));
        Assert.assertEquals("FAIL: The expected quantity: \"" + quantity
                        + "\" does NOT match the actual updated quantity: \"" + updatedQuantity + "\"!", quantity,
                updatedQuantity);
        LOGGER.info("Confirmed that updated quantity'" + quantity + "' matches with rendered quantity ==>"
                + updatedQuantity);
        LOGGER.info("assertUpdatedProductQty completed");
    }

    /**
     * Verifies that the tax or fee is a specific percent of the subtotal
     *
     * @param orderSummaryItem The item in the order summary to validate
     * @param percentage       The percentage to validate
     * @throws Exception Exception thrown by method
     */
    public void verifyPercentage(String orderSummaryItem, String percentage) throws Exception {
        LOGGER.info("verifyPercentage started");
        driver.waitForElementVisible(feeDetailsItemsRowParentBy);
        WebElement orderSummaryItemRow = driver.getElementWithText(feeDetailsItemsRowParentBy, orderSummaryItem);
        WebElement orderSummarySubtotalRow = driver.getElementWithText(feeDetailsItemsRowParentBy, SUBTOTAL);
        String itemAmount = driver.getLastSubstring(orderSummaryItemRow, "$");
        String subtotalAmount = driver.getLastSubstring(orderSummarySubtotalRow, "$");
        float percentageActualFloat = Float.parseFloat(itemAmount) / Float.parseFloat(subtotalAmount);
        int percentageActual = (int) (percentageActualFloat * 100);
        Assert.assertEquals("FAIL: Percentage expected: \"" + percentage + "\", Percentage actual: \""
                + percentageActual + "\"!", 1, percentageActual);
        LOGGER.info("Confirmed that the percentage was %" + percentage + " of the total.");
        LOGGER.info("verifyPercentage completed");
    }

    /**
     * Extracts the Total Miles Manufacturer Warranty
     *
     * @return String
     */
    private String getTotalMilesWarrantyText() {
        LOGGER.info("getTotalMilesWarrantyText started");
        driver.waitForElementVisible(totalMilesWarranty);
        LOGGER.info("getTotalMilesWarrantyText completed");
        return totalMilesWarranty.getText();
    }

    /**
     * Assert provided "Tire Miles Warranty" matches with the rendered warranty
     * value on cart's page
     *
     * @param miles The string fee to check
     */
    public void assertMilesWarranty(String miles) {
        LOGGER.info("assertMilesWarranty started");
        String milesWarranty = getTotalMilesWarrantyText();
        Assert.assertEquals(
                "FAIL: Miles didn't match! (displayed miles:-> " + milesWarranty + " expected:->  "
                        + miles + ")", miles, milesWarranty);
        LOGGER.info("Miles matched: (displayed miles:-> " + milesWarranty + " with expected:->  " + miles + ")");
        LOGGER.info("assertMilesWarranty completed");
    }

    /**
     * Extracts the Tire PSI Max Air Pressure
     *
     * @return String
     */
    private String getTireMaxPressureText() {
        LOGGER.info("getTireMaxPressureText started");
        driver.waitForElementVisible(tireMaxPressure);
        LOGGER.info("getTireMaxPressureText completed");
        return tireMaxPressure.getText();
    }

    /**
     * Assert provided "Tire Max PSI Pressure" matches with the rendered psi on
     * cart's page
     *
     * @param psi The string psi to check
     */

    public void assertTireMaxPressure(String psi) {
        LOGGER.info("assertTireMaxPressure started");
        String tireMaxPressure = getTireMaxPressureText();
        Assert.assertEquals("FAIL: Tire Max PSI Pressure didn't match! (displayed:-> " + tireMaxPressure
                + " expected:->  " + psi + ")!", psi, tireMaxPressure);
        LOGGER.info("Tire Max PSI Pressure matched: (displayed:-> " + tireMaxPressure + " with expected:->  "
                + psi + ")");
        LOGGER.info("assertTireMaxPressure completed");
    }

    /**
     * Extracts the Oversized Package Fee
     *
     * @return String
     */
    public String getOversizedPackageFee() {
        LOGGER.info("getOversizedPackageFee started");
        String returnVal = this.getSpecialPricingOnCartPage(feeDetailsItemsRowParentBy, feeDetailsItemsRowLabelBy,
                feeDetailsItemsRowPriceBy, ConstantsDtc.OVERSIZED_PACKAGE_FEE);
        LOGGER.info("getOversizedPackageFee completed");
        return returnVal;
    }

    /**
     * Extracts the Valve Stem Fee (DTD)
     *
     * @return String
     */
    private String getValveStemFee() {
        LOGGER.info("getValveStemFee started");
        String returnVal = this.getSpecialPricingOnCartPage(orderListPartsInfo, feeDetailsItemsRowLabelBy,
                feeDetailsItemsRowPriceBy, ConstantsDtc.VALVE_STEM);
        LOGGER.info("getValveStemFee completed");
        return returnVal;
    }

    /**
     * Extracts the Federal Excise Tax (F.E.T.)
     *
     * @return String
     */
    private String getFETFee() {
        LOGGER.info("getFETFee started");
        String returnVal = this.getSpecialPricingOnCartPage(feeDetailsItemsRowParentBy, feeDetailsItemsRowLabelBy,
                feeDetailsItemsRowPriceBy, ConstantsDtc.FETFEE_LABEL);
        LOGGER.info("getFETFee completed");
        return returnVal;
    }

    /**
     * Assert provided "Environmental Fee" price matches with the rendered fee
     * on cart's page
     *
     * @param fee The string fee to check
     */
    public void assertEnvironmentalFeeAmount(String fee) {
        LOGGER.info("assertEnvironmentalFeeAmount started");
        String environmentalFee = String.valueOf(getFeeTotal(ConstantsDtc.ENVIRONMENTAL_FEE));
        Assert.assertEquals("FAIL: Environmental Fee didn't match! (displayed:-> " + environmentalFee
                + " expected:->  " + fee + ")", fee, environmentalFee);
        LOGGER.info(
                "Environmental Fee matched: (displayed:-> " + environmentalFee + " with expected:->  "
                        + fee + ")");
        LOGGER.info("assertEnvironmentalFeeAmount completed");
    }

    /**
     * Assert provided "Environmental Fee" price matches with the rendered fee (Fixed when Product Price > $100)
     * on cart's page
     */
    public void assertEnvironmentalFeeAmt() {
        LOGGER.info("assertEnvironmentalFeeAmt started");
        double estimatedEnvPrice = getCalculatedEnvironmentFee(Constants.NONE);
        int itemQuantity = Integer.parseInt(webDriver.findElement(CommonActions.cartItemQuantityBy).
                getAttribute(Constants.VALUE));
        double expectedEnvFeeTotal = (estimatedEnvPrice * itemQuantity);
        double actualEnvFee = commonActions.cleanMonetaryStringToDouble(getFeeTotal(ConstantsDtc.ENVIRONMENTAL_FEE));
        Assert.assertTrue("FAIL: Environmental Fee didn't match! (displayed:-> " + actualEnvFee + "expected:-> "
                + expectedEnvFeeTotal + ")", actualEnvFee == expectedEnvFeeTotal);
        LOGGER.info("assertEnvironmentalFeeAmt completed");
    }

    /**
     * Assert FET fee on cart's page
     */
    public void assertFETFee(int item) {
        LOGGER.info("assertFETFee started");
        WebElement elementLabel = driver.getElementWithText(childTip, ConstantsDtc.FETFEE_LABEL);
        if (elementLabel != null && driver.isTextPresentInPageSource(ConstantsDtc.FETFEE_LABEL)) {
            double expectedFetFee = getCalculatedFETFee(item);
            double fetFee = commonActions.cleanMonetaryStringToDouble(getFETFee());
            Assert.assertTrue("FAIL: fet Fee didn't match! (displayed:-> "
                            + fetFee + "expected:->  " + expectedFetFee + ")",
                    expectedFetFee == fetFee);
        }
        LOGGER.info("assertFETFee completed");
    }

    /**
     * Extracts the Tire Disposal Fee
     *
     * @return String
     */
    public String getTireDisposalFee() {
        return this.getSpecialPricingOnCartPage(feeDetailsItemsRowParentBy, feeDetailsItemsRowLabelBy,
                feeDetailsItemsRowPriceBy, ConstantsDtc.DISPOSAL_FEE);
    }

    /**
     * Assert provided "Tire Disposal Fee" price matches with the rendered fee
     * on cart's page
     *
     * @param fee The string fee to check
     */
    public void assertTireDisposalFeeAmount(String fee) {
        LOGGER.info("assertTireDisposalFeeAmount started");
        String tireDisposalFee = String.valueOf(getTireDisposalFee());
        Assert.assertEquals("FAIL: Tire Disposal Fee didn't match! (displayed:-> " + tireDisposalFee
                + " expected:->  " + fee + ")", fee, tireDisposalFee);
        LOGGER.info(
                "Tire Disposal Fee matched: (displayed:-> " + tireDisposalFee + " with expected:->  " + fee + ")");
        LOGGER.info("assertTireDisposalFeeAmount completed");
    }

    /**
     * Assert provided "Tire Disposal Fee" price matches with the expected disposal fee
     * on cart's page
     */
    public void assertTireDisposalFeeAmt() {
        LOGGER.info("assertTireDisposalFeeAmt started");
        double actualDisposalFee = commonActions.cleanMonetaryStringToDouble(getTireDisposalFee());
        int itemQuantity = Integer.parseInt(webDriver.findElement(CommonActions.cartItemQuantityBy).
                getAttribute(Constants.VALUE));
        double estimatedDisposalFee = getCalculatedDisposalFee(Constants.NONE);
        double expectedDisposalFee = (estimatedDisposalFee * itemQuantity);
        Assert.assertTrue("FAIL: Tire Disposal Fee didn't match! (displayed:-> "
                        + actualDisposalFee + "expected:->  " + expectedDisposalFee + ")",
                expectedDisposalFee == actualDisposalFee);
        LOGGER.info("assertTireDisposalFeeAmt completed");
    }

    /**
     * Extracts the Certificate for Repair, Refund or Replacement Service Fee
     *
     * @return String
     */
    private String getCertificateForRRRServiceFee() {
        return this.getSpecialPricingOnCartPage(orderFeeServiceItemLabelBy, feeDetailsItemsRowLabelBy,
                serviceFeeItemRowPriceBy, CERTIFICATE_PROTECT_YOUR_PURCHASE);
    }

    /**
     * Assert provided "Certificate for Repair, Refund or Replacement Service
     * Fee" price matches with the rendered fee on cart's page
     *
     * @param fee The string fee to check
     */
    public void assertCertificateRRRFeeAmount(String fee) {
        LOGGER.info("assertCertificateRRRFeeAmount started");
        String certificateRRRFee = String.valueOf(getCertificateForRRRServiceFee());
        Assert.assertEquals("FAIL: Certificate for Repair, Refund or Replacement Service Fee did NOT match! "
                + "(displayed:-> " + certificateRRRFee + " expected:->  " + fee + ")", fee, certificateRRRFee);
        LOGGER.info("Certificate for Repair, Refund or Replacement Service Fee matched: (displayed:-> "
                + certificateRRRFee + " with expected:->  " + fee + ")");
        LOGGER.info("assertCertificateRRRFeeAmount completed");
    }

    /**
     * Extracts the Installation and Spin Balancing Price
     *
     * @return String
     */
    public String getInstallationPrice() {
        return this.getSpecialPricingOnCartPage(feeDetailsItemsRowParentBy, feeDetailsItemsRowLabelBy,
                feeDetailsItemsRowPriceBy, ConstantsDtc.INSTALLATION);
    }

    /**
     * Extract the web element containing the text for the fee type and the associated fee data on the cart page
     *
     * @param feeType fee type
     * @return element containing the targeted fee and quantity
     */
    public WebElement extractElementContainingFeeData(String feeType) {
        LOGGER.info("extractElementContainingFeeData started");
        WebElement serviceFeeEle = driver.getElementWithText(feeDetailsItemsRowLabelBy, feeType);
        serviceFeeEle = driver.getParentElement(serviceFeeEle);
        LOGGER.info("extractElementContainingFeeData completed");
        return serviceFeeEle;
    }

    /**
     * Assert provided "Installation and Spin Balancing" price matches with the
     * rendered price on cart's page
     *
     * @param fee The string fee to check
     */
    public void assertInstallationBalancingPrice(String fee) {
        LOGGER.info("assertInstallationBalancingPrice started");
        String installationBalancingPrice = getInstallationPrice();
        Assert.assertEquals("FAIL: Installation & Spin Balancing Price didn't match! (displayed:-> "
                + installationBalancingPrice + " expected:->  " + fee + ")", fee, installationBalancingPrice);
        LOGGER.info("Installation & Spin Balancing Price matched: (displayed:-> " + installationBalancingPrice
                + " with expected:->  " + fee + ")");
        LOGGER.info("assertInstallationBalancingPrice completed");
    }

    /**
     * Extracts the TPMS Rebuild Kits Price
     *
     * @return String
     */
    private String getTPMSRebuildPrice() {
        return this.getSpecialPricingOnCartPage(orderListPartsInfo, feeDetailsItemsRowLabelBy,
                feeDetailsItemsRowPriceBy, TPMS_REBUILD_KIT);
    }

    /**
     * Extracts the TPMS Price irrespective of TPMS Rebuild and TPMS Sensor
     *
     * @return String
     */
    private String getTPMSPrice() {
        return this.getSpecialPricingOnCartPage(orderListPartsInfo, feeDetailsItemsRowLabelBy,
                feeDetailsItemsRowPriceBy, ConstantsDtc.TPMS);
    }

    /**
     * Assert provided "TPMS Rebuild Kits" price matches with the rendered price
     * on cart's page
     *
     * @param fee The string fee to check
     */
    public void assertTPMSRebuildPrice(String fee) {
        LOGGER.info("assertTPMSRebuildPrice started");
        String tPMSRebuildPrice = getTPMSRebuildPrice();
        Assert.assertEquals("FAIL: TPMS Rebuild Kits Price didn't match! (displayed:-> " + tPMSRebuildPrice
                + " with expected:->  " + fee + ")", fee, tPMSRebuildPrice);
        LOGGER.info("TPMS Rebuild Kits Price matched: (displayed:-> " + tPMSRebuildPrice + " with expected:->  "
                + fee + ")");
        LOGGER.info("assertTPMSRebuildPrice completed");
    }

    /**
     * Extracts the Sales Tax
     *
     * @return String
     */
    private String getSalesTax() {
        LOGGER.info("getSalesTax started");
        driver.waitForElementVisible(salesTax);
        LOGGER.info("getSalesTax completed");
        return driver.getLastSubstring(salesTax, "$");
    }

    /**
     * Extracts the Total Price Including Tax
     *
     * @return String
     */
    private String getTotalPriceInclTax() {
        LOGGER.info("getTotalPriceInclTax started");
        driver.waitForElementVisible(totalPriceInclTax);
        LOGGER.info("getTotalPriceInclTax completed");
        return driver.getLastSubstring(totalPriceInclTax, "$");
    }

    /**
     * Assert provided "Total Price Incl Tax" price matches with the rendered
     * price on cart's page
     *
     * @param fee The string price to check
     */
    public void assertTotalPriceInclTax(String fee) {
        LOGGER.info("assertTotalPriceInclTax started");
        String totalPriceInclTax = getTotalPriceInclTax();
        Assert.assertEquals("FAIL: Total Price Incl Tax did NOT match! (displayed:-> " + totalPriceInclTax
                + " expected:->  " + fee + ")", fee, totalPriceInclTax);
        LOGGER.info("Total Price Incl Tax matched: (displayed:-> " + totalPriceInclTax + " with expected:->  "
                + fee + ")");
        LOGGER.info("assertTotalPriceInclTax completed");
    }

    /**
     * Click Continue Shopping For Tires Button
     */
    public void clickContinueShoppingForTires() {
        LOGGER.info("clickContinueShoppingForTires started");
        // TODO: retest when new safaridriver is stable
        if (Config.isSafari())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        driver.waitForElementClickable(continueShoppingForTires);
        continueShoppingForTires.click();
        LOGGER.info("clickContinueShoppingForTires completed");
    }

    /**
     * Click Continue Shopping For Wheels Button
     */
    public void clickContinueShoppingForWheels() {
        LOGGER.info("clickContinueShoppingForWheels started");
        // TODO: retest when new safaridriver is stable
        if (Config.isSafari())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        driver.waitForElementClickable(continueShoppingForWheels);
        continueShoppingForWheels.click();
        LOGGER.info("clickContinueShoppingForWheels completed");
    }

    /**
     * Click More Options link
     */
    public void clickMoreOptionsLink() {
        LOGGER.info("clickMoreOptionsLink started");
        // TODO: retest when new safaridriver is stable
        if (Config.isSafari())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        moreOptionsLink.click();
        LOGGER.info("clickMoreOptionsLink completed");
    }

    /**
     * Checks if "Environmental Fee (State Required)" text is displayed
     */
    public void assertEnvironmentalFeeLabelDisplayed() {
        LOGGER.info("assertEnvironmentalFeeLabelDisplayed started");
        String textLookUp = ConstantsDtc.ENVIRONMENTAL_FEE;
        WebElement elementLabel = driver.getElementWithText(childTip, textLookUp);
        if (elementLabel != null && driver.isTextPresentInPageSource(textLookUp)) {
            Assert.assertTrue("FAIL: Environmental Fee expected value: \"" + textLookUp
                            + "\", actual value: \"" + elementLabel.getText() + "\"!",
                    elementLabel.getText().contains(textLookUp));
        } else
            Assert.fail("FAIL: Environmental Fee Text : \"" + textLookUp + "\" - NOT present in page source!");
        LOGGER.info("assertEnvironmentalFeeLabelDisplayed completed");
    }

    /**
     * Checks if "Tire Disposal Fee" text is displayed
     */
    public void assertTireDisposalFeeLabelDisplayed() {
        LOGGER.info("assertTireDisposalFeeLabelDisplayed started");
        String textLookUp = ConstantsDtc.DISPOSAL_FEE;
        WebElement elementLabel = driver.getElementWithText(feeDetailsItemsRowLabelBy, textLookUp);
        if (elementLabel != null && driver.isTextPresentInPageSource(textLookUp)) {
            Assert.assertTrue("FAIL: Text NOT matched; Found this \"" + elementLabel.getText()
                            + "\" but expected this \"" + textLookUp + "\"!",
                    elementLabel.getText().contains(textLookUp));
        } else
            Assert.fail("FAIL: Tire Disposal Fee Text: \"" + textLookUp + "\" - NOT present in page source!");
        LOGGER.info("assertTireDisposalFeeLabelDisplayed completed");
    }

    /**
     * Checks if "Certificate for Repair Refund Replacement" text is displayed
     */
    public void assertCertificateRepairRefundReplacementLabelDisplayed() {
        LOGGER.info("assertCertificateRepairRefundReplacementLabelDisplayed started");
        String actualCertificateLabelText = certificateForRRRLabel.getText();
        Assert.assertTrue("FAIL: Certificate label text is incorrect. Actual: '" + actualCertificateLabelText +
                        "', Expected: '" + CERTIFICATE_PROTECT_YOUR_PURCHASE + "'",
                actualCertificateLabelText.matches(CERTIFICATE_PROTECT_YOUR_PURCHASE));
        driver.jsScrollToElementClick(certificateToolTip, false);
        String actualToolTipText = toolTipContent.getText();
        Assert.assertTrue("FAIL: Certificate tooltip text is incorrect. Actual '" + actualToolTipText +
                        "', Expected: '" + CERTIFICATE_REPAIR_REFUND_REPLACEMENT_MESSAGE + "'",
                actualToolTipText.contentEquals(CERTIFICATE_REPAIR_REFUND_REPLACEMENT_MESSAGE));
        LOGGER.info("assertCertificateRepairRefundReplacementLabelDisplayed completed");
    }

    /**
     * Checks if "TPMS Rebuild Kits" text is displayed
     */
    public void assertTPMSRebuildKitsLabelDisplayed() {
        LOGGER.info("assertTPMSRebuildKitsLabelDisplayed started");
        String textLookUp = TPMS_REBUILD_KIT;
        WebElement elementLabel = driver.getElementWithText(CommonActions.optionNameBy, textLookUp);
        if (elementLabel != null) {
            Assert.assertTrue("FAIL: Text NOT matched; Found this \"" + elementLabel.getText()
                    + "\" but expected this \"" + textLookUp + "\"!", elementLabel.getText().contains(textLookUp));
        } else
            Assert.fail("FAIL: TPMS Rebuild Kits Text: \"" + textLookUp + "\" - NOT present in page source!");
        LOGGER.info("assertTPMSRebuildKitsLabelDisplayed completed");
    }

    /**
     * Extracts the Pricing among multiple that contains text substring
     *
     * @param parentElement The By element to build list with
     * @param refElement    The By element to check the existence of specific element
     * @param targetElement The By element to interact with
     * @param text          The string to lookup the specific element
     * @return String
     */
    public String getSpecialPricingOnCartPage(By parentElement, By refElement, By targetElement, String text) {
        LOGGER.info("getSpecialPricingOnCartPage started");
        boolean stringFound = false;
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        List<WebElement> objects = webDriver.findElements(parentElement);
        String pricing = "";

        for (WebElement object : objects) {
            try {
                if (object.findElement(refElement).getText().contains(text)) {
                    stringFound = true;
                    pricing = object.findElement(targetElement).getText().trim();
                    break;
                }
            } catch (Exception e) {
                //continue to next iteration
            }
        }
        Assert.assertTrue("FAIL: String '" + text + "' NOT found!", stringFound);
        driver.resetImplicitWaitToDefault();
        LOGGER.info("getSpecialPricingOnCartPage completed");
        return pricing.substring(pricing.lastIndexOf("$") + 1);
    }

    /**
     * Asserts that the Add to Cart button is disabled
     *
     * @throws Exception General exception caught to allow for graceful failure
     */
    public void assertAddToCartButtonIsDisabled() throws Exception {
        LOGGER.info("assertAddToCartButtonIsDisabled started");
        driver.waitForElementVisible(resultRowCompare);
        if (addToCartButton.isEnabled()) {
            Assert.fail("Add to Cart Button is not disabled.");
        }
        LOGGER.info("assertAddToCartButtonIsDisabled completed");
    }

    /**
     * Verifies the Switch Vehicle buttons (Clear my cart and Continue | View cart)
     *
     * @throws Exception General exception caught to allow for graceful failure
     */
    public void assertSwitchVehicleButtons() throws Exception {
        LOGGER.info("assertSwitchVehicleOptions started");
        String failure = null;
        List<String> actualButtons = new ArrayList<>();
        String[] expectedButtons = {CONTINUE_AND_CLEAR_CURRENT_CART, ConstantsDtc.VIEW_SHOPPING_CART};
        driver.waitForElementVisible(switchOptions);
        int counter = 0;
        do {
            driver.waitSeconds(Constants.TWO);
            counter++;
        } while (!driver.isElementDisplayed(switchModal) && counter < Constants.FIVE);
        List<WebElement> buttons = webDriver.findElements(switchOptions);
        for (WebElement button : buttons) {
            actualButtons.add(button.getText());
        }

        for (String expectedButton : expectedButtons) {
            if (!CommonUtils.containsIgnoreCase(actualButtons.toString(), expectedButton))
                failure = "'" + expectedButton + "' button not present. ";
        }

        Assert.assertNull("FAIL: " + failure, failure);
        LOGGER.info("assertSwitchVehicleOptions completed");
    }

    /**
     * Selects the Switch Vehicle options (Clear my cart and Continue | View cart)
     *
     * @param option The switch vehicle option to select.
     * @throws Exception General exception caught to allow for graceful failure
     */
    public void selectSwitchVehicleOption(String option) throws Exception {
        LOGGER.info("selectSwitchVehicleOption started");
        if (!driver.isElementDisplayed(switchModal, Constants.TWO))
            return;
        if (option.equalsIgnoreCase(CONTINUE_AND_CLEAR_CURRENT_CART)) {
            driver.jsScrollToElementClick(continueAndClearCurrentCartButton);
        } else if (option.equalsIgnoreCase(ConstantsDtc.VIEW_SHOPPING_CART)) {
            driver.jsScrollToElementClick(viewShoppingCart);
        } else if (option.equalsIgnoreCase(ConstantsDtc.CANCEL)) {
            driver.waitForElementVisible(modalSwitchCancel);
            modalSwitchCancel.click();
        } else {
            Assert.fail("FAIL: Option \"" + option + "\" not available.");
        }

        LOGGER.info("selectSwitchVehicleOption completed");
    }

    /**
     * Asserts that the current product unit price matches desired price
     *
     * @param itemCode      The product to verify unit price
     * @param expectedPrice The unit price to validate - price value or not displayed
     */
    public void assertItemUnitPrice(String itemCode, String expectedPrice) {
        LOGGER.info("assertItemUnitPrice started for item: " + itemCode);
        int index = 0;
        if (Config.isMobile()) {
            index = 1;
        }
        driver.waitForElementVisible(orderList);
        WebElement productParent = driver.getParentElement(commonActions.getProductSectionTopElement(itemCode));
        String itemUnitPrice = productParent.findElements(itemPriceEachBy).get(index).getText();
        if (expectedPrice.equalsIgnoreCase(Constants.NOT_DISPLAYED))
            Assert.assertTrue("FAIL: The item unit price is still displayed as: \"" + itemUnitPrice
                            + "\" while the the expected price is: \"" + expectedPrice + "\"!",
                    itemUnitPrice.length() == 0);
        else
            Assert.assertTrue("FAIL: The actual unit price: \"" + itemUnitPrice
                            + "\" doesn't match the expected price: \"" + expectedPrice + "\"!",
                    itemUnitPrice.contains(expectedPrice));
        LOGGER.info("assertItemUnitPrice completed for item: " + itemCode);
    }

    /**
     * Verifies the itemTotal = product price * quantity
     *
     * @param status   Displayed/Not Eligible/Expired
     * @param itemCode The product to verify line item total
     */
    public void assertItemTotal(String status, String itemCode) {
        LOGGER.info("assertItemTotal started for item: " + itemCode);
        int index = 0;
        if (Config.isMobile()) {
            index = 1;
        }
        driver.waitForElementVisible(orderList);
        WebElement productParent = driver.getParentElement(commonActions.getProductSectionTopElement(itemCode));
        String itemTotal = productParent.findElements(itemPriceNonStaggeredBy).get(index).getText();
        String itemUnitPrice = productParent.findElements(itemPriceEachBy).get(index).getText();
        WebElement itemQuantityField = productParent.findElements(cartItemQuantityBy).get(index);
        if (status.equalsIgnoreCase(Constants.DISPLAYED)) {
            double unitPrice = commonActions.cleanMonetaryStringToDouble(itemUnitPrice);
            int itemQuantity;
            if (itemQuantityField.getTagName().equalsIgnoreCase(Constants.INPUT)) {
                itemQuantity = Integer.parseInt(itemQuantityField.getAttribute(Constants.VALUE));
            } else {
                itemQuantity = Integer.parseInt(itemQuantityField.getText());
            }
            double actualItemTotal = commonActions.cleanMonetaryStringToDouble(itemTotal);
            double expectedItemTotal = unitPrice * itemQuantity;
            Assert.assertTrue("FAIL: The actual item total: \"" + actualItemTotal
                            + "\" doesn't match the expected item total: \"" + expectedItemTotal + "\"",
                    expectedItemTotal == actualItemTotal);
        } else if (status.equalsIgnoreCase(NOT_ELIGIBLE)) {
            Assert.assertTrue("FAIL: The actual item total: \"" + itemTotal
                            + "\" doesn't match the expected item total: \"" + NOT_ELIGIBLE + "\"",
                    itemTotal.equalsIgnoreCase(NOT_ELIGIBLE));
        } else if (status.equalsIgnoreCase(EXPIRED)) {
            Assert.assertTrue("FAIL: The actual item total: \"" + itemTotal
                            + "\" doesn't match the expected item total: \"" + EXPIRED + "\"",
                    itemTotal.equalsIgnoreCase(EXPIRED));
        }
        LOGGER.info("assertItemTotal completed for item: " + itemCode);
    }

    /**
     * Verifies the pre total
     *
     * @param preTotalValue The pre total to verify
     */
    public void assertCartPreTotal(String preTotalValue) {
        LOGGER.info("assertCartPreTotal started");
        driver.waitForElementVisible(preTotal);
        Assert.assertTrue("FAIL: The actual pre-total: \"" + preTotal.getText()
                        + "\" did not contain the expected pre-total: \"" + preTotalValue + "\"!",
                preTotal.getText().contains(preTotalValue));
        LOGGER.info("Confirmed that \"" + preTotalValue + "\" was listed as the pre-total for the product.");
        LOGGER.info("assertCartPreTotal completed");
    }

    /**
     * Verifies the cart subtotal
     *
     * @param cartSubTotalValue The subtotal in cart to verify
     */
    public void assertCartSubtotal(String cartSubTotalValue) {
        LOGGER.info("assertCartSubtotal started");
        WebElement cartSubTotal = webDriver.findElement(cartSummaryBreakDownPriceBy);
        driver.waitForElementVisible(cartSubTotal);
        Assert.assertTrue("FAIL: The actual  subtotal: \"" + cartSubTotal.getText()
                        + "\" did not contain the expected total: \"" + cartSubTotalValue + "\"!",
                cartSubTotal.getText().contains(cartSubTotalValue));
        LOGGER.info("Confirmed that \"" + cartSubTotalValue + "\" was listed as the subtotal for the product.");
        LOGGER.info("assertCartSubtotal completed");
    }

    /**
     * Verifies the tax
     *
     * @param taxValue The tax value to verify
     */
    public void assertCartTax(String taxValue) {
        LOGGER.info("assertCartTax started");
        driver.waitForElementVisible(tax);
        Assert.assertTrue("FAIL: The actual tax: \"" + tax.getText()
                        + "\" did not contain the expected tax: \"" + taxValue + "\"!",
                tax.getText().contains(taxValue));
        LOGGER.info("Confirmed that \"" + taxValue + "\" was listed as the tax for the product.");
        LOGGER.info("assertCartTax completed");
    }

    /**
     * Assert tax calculation is specific % of sub-total with consideration of savings on product base price
     */
    public double getTaxOnCartPage() {
        LOGGER.info("getTaxOnCartPage started");
        String tirePrice = itemTotal.getText();
        String stateOnCart;
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
            stateOnCart = ConstantsDtc.DTD;
        } else {
            stateOnCart = (cartPageStoreState.getText().split(",")[1].split("\\s+")[1]).trim();
        }
        String customerType = "default_customer_".concat(stateOnCart.toLowerCase());
        commonActions.setRegionalTaxesFactor(customer.getCustomer(customerType));

        //Calculate Tax based on the fees and promotions applicable
        double estimatedTaxAmount = commonActions.getCalculatedSalesTaxForDTCRegion(tirePrice);

        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT) ||
                Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
            String dispFee = getTireDisposalFee();
            estimatedTaxAmount = estimatedTaxAmount + commonActions.getCalculatedSalesTaxForDTCRegion(dispFee);
        } else {
            String optionalFee = String.valueOf(extractPriceForOptionalFee(Constants.NONE, ConstantsDtc.VALVE_STEM));
            estimatedTaxAmount = estimatedTaxAmount + commonActions.getCalculatedSalesTaxForDTCRegion(optionalFee);
        }
        String certFee = String.valueOf(extractPriceForOptionalFee(Constants.NONE, ConstantsDtc.CERTIFICATES));
        if (!commonActions.getCurrentUrl().contains(ConstantsDtc.AT) &&
                !commonActions.getCurrentUrl().contains(ConstantsDtc.AMERICAS_TIRE)) {
            estimatedTaxAmount = estimatedTaxAmount + commonActions.getCalculatedSalesTaxForDTCRegion(certFee);
        }
        if (driver.isElementDisplayed(cartSummaryInstantSavingBy, Constants.ONE)) {
            String promotion = webDriver.findElement(cartSummaryInstantSavingBy).getText();
            estimatedTaxAmount = estimatedTaxAmount
                    - commonActions.getCalculatedSalesTaxForDTCRegion(promotion);
        }

        WebElement fetLabel = driver.getElementWithText(feeDetailsItemsRowLabelBy, ConstantsDtc.FETFEE_LABEL);
        if (fetLabel != null && driver.isTextPresentInPageSource(ConstantsDtc.FETFEE_LABEL)) {
            String fetFEE = getFETFee();
            estimatedTaxAmount = estimatedTaxAmount
                    + commonActions.getCalculatedSalesTaxForDTCRegion(fetFEE);
        }
        LOGGER.info("getTaxOnCartPage completed");
        return commonActions.twoDForm(estimatedTaxAmount, 2);
    }

    /**
     * Calculate tax based on item price, fees and promotions applied for all items on Cart Page
     *
     * @param productSection - The product section containing the item(s) to have tax calculated
     */
    public double calculateTaxForItems(WebElement productSection) {
        LOGGER.info("calculateTaxForItems started");
        double taxAmount = 0.00;
        String stateOnCart;
        String prevItem = "";

        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
            stateOnCart = ConstantsDtc.DTD;
        } else {
            stateOnCart = (cartPageStoreState.getText().split(",")[1].split("\\s+")[1]).trim();
        }

        String customerType = "default_customer_".concat(stateOnCart.toLowerCase());
        commonActions.setRegionalTaxesFactor(customer.getCustomer(customerType));
        List<String> productSectionItems = getItemCodesOnCartProductSection(productSection);
        for (String item : productSectionItems) {
            String tirePrice = String.valueOf(extractItemsPriceTotal(item));
            taxAmount += commonActions.getCalculatedSalesTaxForDTCRegion(tirePrice);
            if (!itemsAreStaggeredSet(productSection, prevItem, item)) {
                String dispFee = String.valueOf(extractFeesForItemsOnCart(ConstantsDtc.DISPOSAL_FEE, item));
                taxAmount += commonActions.getCalculatedSalesTaxForDTCRegion(dispFee);
                String optionalFee = String.valueOf(extractPriceForOptionalFee(item, ConstantsDtc.VALVE_STEM));
                taxAmount += commonActions.getCalculatedSalesTaxForDTCRegion(optionalFee);
                String certFee = String.valueOf(extractPriceForOptionalFee(item, ConstantsDtc.CERTIFICATES));
                taxAmount += commonActions.getCalculatedSalesTaxForDTCRegion(certFee);
                String hubCentricRingFee =
                        String.valueOf(extractFeesForItemsOnCart(ConstantsDtc.HUB_CENTRIC_RING, item));
                taxAmount += commonActions.getCalculatedSalesTaxForDTCRegion(hubCentricRingFee);
                String wheelInstallKitFee =
                        String.valueOf(extractFeesForItemsOnCart(ConstantsDtc.WHEEL_INSTALL_KIT, item));
                taxAmount += commonActions.getCalculatedSalesTaxForDTCRegion(wheelInstallKitFee);
                String federalExciseTax =
                        String.valueOf(extractFeesForItemsOnCart(ConstantsDtc.FETFEE_LABEL, item));
                taxAmount += commonActions.getCalculatedSalesTaxForDTCRegion(federalExciseTax);
            }
            //TODO - Tax calculation needs to be further extended for FET, TPMS, etc Fees and Certificate needs to be tested
            prevItem = item;
        }
        LOGGER.info("calculateTaxForItems completed");
        return commonActions.twoDForm(taxAmount, 2);
    }

    /**
     * Determines whether an item is part of a staggered set with an item in the specified list
     *
     * @param productSection - The product section within which to evaluate whether the items are staggered pair
     * @param item1          - item ID of item to see if it is part of a staggered set
     * @param item2          - item ID of item to see if it is part of a staggered set
     * @return - true/false whether the items compose a staggered set
     */
    private boolean itemsAreStaggeredSet(WebElement productSection, String item1, String item2) {
        LOGGER.info("itemsAreStaggeredSet started");

        if (!driver.scenarioData.isStaggeredProduct() || item1.isEmpty() || item2.isEmpty())
            return false;

        boolean staggeredSet = false;

        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        if (driver.elementExists(productSection, CartPage.staggeredDetailsContainerBy) &&
                driver.isElementDisplayed(productSection.findElement(CartPage.staggeredDetailsContainerBy))) {
            String staggeredProductPair = productSection.findElement(CartPage.staggeredDetailsContainerBy).getText();
            if (staggeredProductPair.contains(item1) && staggeredProductPair.contains(item2)) {
                staggeredSet = true;
            }
        }
        LOGGER.info("itemsAreStaggeredSet completed");
        return staggeredSet;
    }

    /**
     * Extract tax calculation on Cart Page
     */
    public double extractTaxOnCart() {
        LOGGER.info("extractTax started");
        WebElement tax = driver.getElementWithText(cartSummaryBreakDownNameBy, ConstantsDtc.TAX);
        WebElement taxParent = driver.getParentElement(driver.getParentElement(tax));
        LOGGER.info("extractTax completed");
        return commonActions.cleanMonetaryStringToDouble(taxParent.findElement(cartSummaryBreakDownPriceBy).getText());
    }

    /**
     * Assert calculated tax matches with the actual extracted tax on cart page
     */
    public void assertTax() {
        LOGGER.info("assertTax started");
        double estimatedTaxAmount = Constants.ZERO;
        List<WebElement> productSections = webDriver.findElements(CartPage.cartItemDetailsParentBy);
        setProductParent(getProductParent());
        setFeeParents(getFeeParents());
        for (WebElement productSection : productSections) {
            estimatedTaxAmount += calculateTaxForItems(productSection);
        }
        if (driver.isElementDisplayed(cartSummaryInstantSavingBy, Constants.ONE)) {
            String promotion = webDriver.findElement(cartSummaryInstantSavingBy).getText();
            estimatedTaxAmount -= commonActions.getCalculatedSalesTaxForDTCRegion(promotion);
        }
        estimatedTaxAmount = commonActions.twoDForm(estimatedTaxAmount, Constants.TWO);
        double actualTaxAmount = commonActions.extractTaxOnCart();
        Assert.assertTrue("FAIL: The actual  tax amount: {" + actualTaxAmount
                        + ") did not match to the expected tax amount: (" + estimatedTaxAmount + ")",
                Math.abs(Double.parseDouble(new DecimalFormat("#.##").
                        format(estimatedTaxAmount - actualTaxAmount))) <= .02);
        LOGGER.info("assertTax completed");
    }

    /**
     * Verifies the dollar discount amount has actually been applied to the total price
     *
     * @param fixedDiscount Amount in Dollars ($) of the item discount
     */
    public String assertFixedDollarDiscountApplied(double fixedDiscount) {
        LOGGER.info("assertFixedDollarDiscountApplied started");
        driver.waitForElementVisible(preTotal);

        // Order summary subtotal
        WebElement subTotal = webDriver.findElement(feeDetailsItemsRowParentBy);
        subTotal = subTotal.findElement(By.cssSelector(Constants.SPAN));
        double orderSubTotal = commonActions.cleanMonetaryStringToDouble(subTotal.getText());

        // Order summary tax
        tax = tax.findElement(By.cssSelector(Constants.SPAN));
        double taxesInt = commonActions.cleanMonetaryStringToDouble(tax.getText());

        // Order summary discount
        String savingsString = savings.findElement(By.cssSelector(Constants.SPAN)).getText();
        double savings = commonActions.cleanMonetaryStringToDouble(savingsString);

        // Order summary total
        double totalInt = commonActions.cleanMonetaryStringToDouble(totalPriceInclTax.getText());
        String orderTotal = totalPriceInclTax.getText();

        Assert.assertTrue("FAIL: Price on PLP page (" + fixedDiscount + ") did not match 'Savings:' price ("
                + savings + ") in order summary.", savings == fixedDiscount);
        Assert.assertTrue("FAIL: Promotional discount (" + savings + ")was not applied correctly.",
                (orderSubTotal + taxesInt - savings) == totalInt);

        LOGGER.info("assertFixedDollarDiscountApplied completed");
        return orderTotal;
    }

    /**
     * Verifies the fixed discount percentage has actually been applied to the total price
     *
     * @param fixedPercentage Amount in Percentage (%) of the item discount
     */
    public String assertFixedPercentageDiscountApplied(int fixedPercentage) {
        LOGGER.info("assertFixedPercentageDiscountApplied started");
        driver.waitForElementVisible(preTotal);
        double taxes = Constants.ZERO;
        if (fixedPercentage == Constants.ZERO) {
            Assert.fail("Fixed Percentage value set to 0, cannot assert the discount applied on the page");
        }

        // Pre-Total Amount
        double preTotalAmount = commonActions.cleanMonetaryStringToDouble(preTotal.getText());
        // Calculated Savings Based On fixedPercentage

        double calculatedSavings = (preTotalAmount * fixedPercentage) / Constants.ONE_HUNDRED;
        // Order summary subtotal

        WebElement subTotal = webDriver.findElement(feeDetailsItemsRowParentBy);
        subTotal = subTotal.findElement(By.cssSelector(Constants.SPAN));
        double orderSubTotal = commonActions.cleanMonetaryStringToDouble(subTotal.getText());

        tax = tax.findElement(By.cssSelector(Constants.SPAN));
        taxes = commonActions.cleanMonetaryStringToDouble(tax.getText());

        // Order summary discount
        //TODO CCL - Fails in STG due to defect #7185 (No savings on cart page)
        String savingsString = savings.findElement(By.cssSelector(Constants.SPAN)).getText();
        double savings = commonActions.cleanMonetaryStringToDouble(savingsString);

        // Order summary total
        double total = commonActions.cleanMonetaryStringToDouble(totalPriceInclTax.getText());
        String orderTotal = totalPriceInclTax.getText();

        Assert.assertTrue("FAIL: Price on PLP page (" + calculatedSavings + ") did not match 'Savings:' price ("
                + savings + ") in order summary.", savings == calculatedSavings);
        Assert.assertTrue("FAIL: Promotional discount (" + savings + ")was not applied correctly.",
                (orderSubTotal + taxes - savings) == total);

        LOGGER.info("assertFixedPercentageDiscountApplied completed");
        return orderTotal;
    }

    /**
     * Calculates the taxes and fees for a customer by selecting the country, entering the zip code and clicking the
     * calculate the button / icon.
     *
     * @param cartCustomer Type of customer to pull from dtc.data and use for country and zip code fields
     */
    public void calculateTaxesFeesForCustomer(Customer cartCustomer) {
        LOGGER.info("calculateTaxesFeesForCustomer started");
        driver.waitForPageToLoad();
        //TODO: For DTD QA env, zipode is pre-populated with AZ zipcode
        //TODOL No input textbox to enter or update zipcode
        if (driver.isElementDisplayed(CommonActions.orderSummaryZipCode)) {
            CommonActions.orderSummaryZipCode.sendKeys(cartCustomer.zip);
            CommonActions.orderSummaryZipCode.sendKeys(Keys.RETURN);
        }
        LOGGER.info("calculateTaxesFeesForCustomer completed");
    }

    /**
     * Verifies the specified label (fee or service) is present on the cart page
     *
     * @param labelToVerify String representing the text of the fee or service to verify
     */
    public void assertFeeServiceLabelPresentOnCartPage(String labelToVerify) {
        LOGGER.info("assertFeeServiceLabelPresentOnCartPage started");
        driver.waitForElementVisible(orderFeeServiceItemLabelBy);
        Assert.assertTrue("FAIL: Expected label - \"" + labelToVerify + "\" was not found on the cart page!",
                driver.checkIfElementContainsText(orderFeeServiceItemLabelBy, labelToVerify));
        LOGGER.info("assertFeeServiceLabelPresentOnCartPage completed");
    }

    /**
     * Verifies the selected products are displayed on the cart page
     */
    public void assertSelectedProductsOnCartPage() {
        LOGGER.info("assertSelectedProductsOnCartPage started");
        driver.waitForPageToLoad();
        String expectedBrand = commonActions.productInfoListGetValue(ConstantsDtc.BRAND);
        String expectedProduct = commonActions.productInfoListGetValue(ConstantsDtc.PRODUCT);
        String displayBrand = webDriver.findElement(CommonActions.brandNameBy).getText();
        String displayProduct = webDriver.findElement(CommonActions.productNameBy).getText();

        Assert.assertTrue("FAIL: Brand incorrect on Cart page.  Expected:  " + expectedBrand + ".  Actual:  " +
                displayBrand, displayBrand.equalsIgnoreCase(expectedBrand));
        Assert.assertTrue("FAIL: Product incorrect on Cart page.  Expected:  " + expectedProduct + ".  Actual:  " +
                displayProduct, displayProduct.equalsIgnoreCase(expectedProduct));

        LOGGER.info("Verified the selected product on the Cart Page is '" + displayBrand + " | " + displayProduct);
        LOGGER.info("assertSelectedProductsOnCartPage completed");
    }

    /**
     * Calculate the subtotal on cart page by sum of product, fees and certificate
     */
    public double getCartSubtotal() {
        LOGGER.info("getCartSubtotal started");
        double subtotal = commonActions.twoDForm(commonActions.cleanMonetaryStringToDouble(itemTotal.getText()), 2);
        if (driver.isElementDisplayed(cartSummaryInstantSavingBy, Constants.ONE)) {
            double savings = commonActions.cleanMonetaryStringToDouble(
                    webDriver.findElement(cartSummaryInstantSavingBy).getText());
            subtotal = commonActions.twoDForm(subtotal - savings, Constants.TWO);
        }
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT) ||
                Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
            subtotal = subtotal +
                    commonActions.cleanMonetaryStringToDouble(getFeeTotal(ConstantsDtc.ENVIRONMENTAL_FEE));

            subtotal = subtotal + commonActions.cleanMonetaryStringToDouble(getTireDisposalFee())
                    + commonActions.cleanMonetaryStringToDouble(getInstallationPrice());
            subtotal = commonActions.twoDForm(subtotal, Constants.TWO);
        }
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
            subtotal = commonActions.twoDForm(subtotal +
                    commonActions.cleanMonetaryStringToDouble(getValveStemFee()), Constants.TWO);
        }
        double certificateTotal = extractPriceForOptionalFee(Constants.NONE, ConstantsDtc.CERTIFICATES);
        if (certificateTotal != Constants.ZERO) {
            subtotal = commonActions.twoDForm(subtotal + certificateTotal, Constants.TWO);
        }
        if (driver.isTextPresentInPageSource(ConstantsDtc.FETFEE_LABEL)) {
            subtotal = commonActions.twoDForm(subtotal +
                    commonActions.cleanMonetaryStringToDouble(getFETFee()), Constants.TWO);
        }
        WebElement elementLabel = driver.getElementWithText(CommonActions.optionNameBy, ConstantsDtc.TPMS);
        if (elementLabel != null && driver.isTextPresentInPageSource(ConstantsDtc.TPMS)) {
            subtotal = commonActions.twoDForm(subtotal +
                    commonActions.cleanMonetaryStringToDouble(getTPMSPrice()), Constants.TWO);
        }
        LOGGER.info("getCartSubtotal completed");
        return subtotal;
    }

    /**
     * Calculate the subtotal on cart page by sum of product, fees and certificate for multiple items
     *
     * @param productSection - The product section containing the item(s) to calculate subtotal
     */
    public double calculateCartSubtotalForItems(WebElement productSection) {
        LOGGER.info("calculateCartSubtotalForItems started");
        double subtotal = 0.00;
        String prevItem = "";
        List<String> productSectionItems = getItemCodesOnCartProductSection(productSection);
        for (String item : productSectionItems) {
            double itemPrice = extractItemsPriceTotal(item);
            subtotal += itemPrice;
            if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT) ||
                    Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
                if (!itemsAreStaggeredSet(productSection, prevItem, item)) {
                    subtotal += extractFeesForItemsOnCart(ConstantsDtc.ENVIRONMENTAL_FEE, item);
                    subtotal += extractFeesForItemsOnCart(ConstantsDtc.DISPOSAL_FEE, item);
                    subtotal += extractFeesForItemsOnCart(ConstantsDtc.HUB_CENTRIC_RING, item);
                    subtotal += extractFeesForItemsOnCart(ConstantsDtc.WHEEL_INSTALL_KIT, item);
                    subtotal += extractFeesForItemsOnCart(ConstantsDtc.INSTALLATION, item);
                    subtotal += extractFeesForItemsOnCart(ConstantsDtc.FETFEE_LABEL, item);
                    subtotal += extractPriceForOptionalFee(item, ConstantsDtc.CERTIFICATES);

                    By productPromotionsContainerBy = null;
                    if (driver.elementExists(productSection, productPromotionsContainerStandardBy))
                        productPromotionsContainerBy = productPromotionsContainerStandardBy;
                    if (driver.elementExists(productSection, productPromotionsContainerStaggeredBy))
                        productPromotionsContainerBy = productPromotionsContainerStaggeredBy;
                    if (productPromotionsContainerBy != null) {
                        String promotionContainerText =
                                productSection.findElement(productPromotionsContainerBy).getText();
                        String rebate = "0.00";
                        if (promotionContainerText.contains("% Instant Savings")) {
                            String percentage = promotionContainerText.split("%")[0];
                            double decimalPercentage = Double.parseDouble(percentage) * .01;
                            rebate = String.valueOf(itemPrice * decimalPercentage);
                        } else {
                            if (!CommonUtils.containsIgnoreCase(promotionContainerText, ConstantsDtc.VIEW_DETAILS)) {
                                rebate = productSection.findElement(productPromotionsContainerBy).
                                        findElement(itemInstantRebateBy).getText();
                            }
                        }
                        subtotal -= commonActions.cleanMonetaryStringToDouble(rebate);
                    }
                }
                prevItem = item;
            }
        }
        LOGGER.info("calculateCartSubtotalForItems completed");
        return subtotal;
    }

    /**
     * Get all the item codes within a specified product section on the cart page.
     * Staggered tire sets will contain two item codes.
     *
     * @param productSection A section on the Cart page
     * @return List of ItemId Strings
     */
    public List<String> getItemCodesOnCartProductSection(WebElement productSection) {
        LOGGER.info("getItemCodesOnCartProductSection started");
        String productSectionText = productSection.getText();
        List<String> cartItems = getAllItemCodesOnCart();
        List<String> productSectionItems = new ArrayList<>();
        for (String item : cartItems) {
            if (productSectionText.contains(item)) {
                productSectionItems.add(item);
            }
        }
        LOGGER.info("getItemCodesOnCartProductSection completed");
        return productSectionItems;
    }

    /**
     * Extract the subtotal on cart page
     */
    public double extractCartSubtotal() {
        LOGGER.info("extractCartSubtotal started");
        WebElement cartSubTotal = driver.getElementWithText(cartSummaryBreakDownNameBy, ConstantsDtc.CART_SUBTOTAL);
        WebElement subTotalParent = driver.getParentElement(driver.getParentElement(cartSubTotal));
        LOGGER.info("extractCartSubtotal completed");
        return commonActions.cleanMonetaryStringToDouble(
                subTotalParent.findElement(cartSummaryBreakDownPriceBy).getText());
    }

    /**
     * Verifies the subtotal on cart page with calculated subtotal
     */
    public void assertExtractedCartSubtotal() {
        LOGGER.info("assertExtractedCartSubtotal started");
        double expectedSubtotal = Constants.ZERO;
        List<WebElement> productSections = webDriver.findElements(CartPage.cartItemDetailsParentBy);
        setProductParent(getProductParent());
        setFeeParents(getFeeParents());
        for (WebElement productSection : productSections) {
            expectedSubtotal += calculateCartSubtotalForItems(productSection);
        }
        expectedSubtotal = commonActions.twoDForm(expectedSubtotal, Constants.TWO);
        double actualSubTotal = extractCartSubtotal();
        Assert.assertTrue("FAIL: The actual  subtotal: \"" + actualSubTotal
                + "\" did not contain the expected total: \""
                + expectedSubtotal + "\"!", actualSubTotal == expectedSubtotal);
        LOGGER.info("assertExtractedCartSubtotal completed");
    }

    /**
     * Verify added Certificate BasePrice, Quantity, or TotalPrice in MiniCart with Shopping Cart
     *
     * @param validationItem - BasePrice, Quantity, or TotalPrice
     */
    public void assertMiniCartCertificateInfo(String validationItem) {
        LOGGER.info("assertMiniCartCertificateInfo started for '" + validationItem + "'");
        driver.waitForPageToLoad(Constants.ONE_THOUSAND);
        driver.jsScrollToElement(miniCartRRACertBasePrice);
        WebElement miniCartElement = null;
        By cartTargetElementBy = null;
        switch (validationItem) {
            case ConstantsDtc.BASE_PRICE:
                miniCartElement = miniCartRRACertBasePrice;
                cartTargetElementBy = addOnPriceEachBy;
                break;
            case ConstantsDtc.QUANTITY:
                miniCartElement = miniCartRRACertItemQuantity;
                cartTargetElementBy = addOnQuantityBy;
                break;
            case ConstantsDtc.TOTAL_PRICE:
                miniCartElement = miniCartRRACertTotalPrice;
                cartTargetElementBy = addOnPriceBy;
                break;
        }
        double miniCartValue = commonActions.cleanMonetaryStringToDouble(miniCartElement.getText());
        WebElement certificateRow = driver.getElementWithText(addOnContentRowBy, CERTIFICATE_PROTECT_YOUR_PURCHASE);
        if (certificateRow == null) {
            certificateRow = driver.getElementWithText(feeContentRowBy, CERTIFICATE_PROTECT_YOUR_PURCHASE);
        }
        double cartValue = commonActions.cleanMonetaryStringToDouble(
                driver.getElementWithText(certificateRow, ConstantsDtc.CERTIFICATES).
                        findElement(cartTargetElementBy).getText());
        Assert.assertTrue("FAIL: MiniCart Certificate " + validationItem + "is incorrect. Actual: '" +
                String.valueOf(miniCartValue) + "'. Expected: " +
                String.valueOf(cartValue), miniCartValue == cartValue);
        LOGGER.info("assertMiniCartCertificateInfo completed for '" + validationItem + "'");
    }

    /**
     * Assert RRR Certificate Total on Shopping Cart Page
     */
    public void assertCertificateTotal() {
        LOGGER.info("assertCertificateTotal started");
        driver.waitForPageToLoad();
        double estimatedCertificatePrice = 0;
        Set<String> itemCodes = cartProductPrice.keySet();
        for (String itemCode : itemCodes) {
            if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)) {
                estimatedCertificatePrice = getCalculatedCertFeeDt(itemCode);
            } else if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
                estimatedCertificatePrice = getCalculatedCertFeeAt(itemCode);
            } else if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
                estimatedCertificatePrice = getCalculatedCertFeeDtd(itemCode);
            }
            int itemQuantity = Integer.parseInt(productParent.get(itemCode).findElement(cartItemQuantityBy).
                    getAttribute(Constants.VALUE));
            double expectedCertificateTotal = (estimatedCertificatePrice * itemQuantity);
            double actualCertificateTotal = extractPriceForOptionalFee(itemCode, ConstantsDtc.CERTIFICATES);
            Assert.assertTrue("FAIL: Certificate Fee didn't match! (displayed:-> " +
                            actualCertificateTotal + "expected:-> " + expectedCertificateTotal + ")",
                    actualCertificateTotal == expectedCertificateTotal);
        }
        LOGGER.info("assertCertificateTotal Completed");
    }

    /**
     * Assert provided "TPMS Rebuild Kits" price calculated based on  item quantity and the rendered price
     * on cart's page
     *
     * @param tpmsRebldFee The string fee to check
     */
    public void assertTPMSRebldPrice(String tpmsRebldFee) {
        LOGGER.info("assertTPMSRebldPrice started");
        int itemQuantity = Integer.parseInt(webDriver.findElement(CommonActions.cartItemQuantityBy).
                getAttribute(Constants.VALUE));
        double fee = Double.parseDouble(tpmsRebldFee);
        double expectedTPMSRebldFee = (fee * itemQuantity);
        double actualTPMSRebuildPrice =
                commonActions.cleanMonetaryStringToDouble(getSpecialPricingOnCartPage(orderListPartsInfo,
                        feeDetailsItemsRowLabelBy, feeDetailsItemsRowPriceBy, TPMS_REBUILD_KIT));
        Assert.assertTrue("FAIL: TPMS Rebuild Kits Price didn't match! (displayed:-> " + actualTPMSRebuildPrice
                + " with expected:->  " + expectedTPMSRebldFee + ")", expectedTPMSRebldFee == actualTPMSRebuildPrice);
        LOGGER.info("TPMS Rebuild Kits Price matched: (displayed:-> " + actualTPMSRebuildPrice + " with expected:->  "
                + expectedTPMSRebldFee + ")");
        LOGGER.info("assertTPMSRebldPrice completed");
    }

    /**
     * Add TPMS Sensor
     */
    public void addTPMSSensor() {
        LOGGER.info("addTPMSSensor started");
        driver.jsScrollToElement(tpmsMoreOpts);
        tpmsMoreOpts.click();
        driver.waitForElementVisible(tpmsSensor);
        String tpmsWindowHandler = webDriver.getWindowHandle();
        webDriver.switchTo().window(tpmsWindowHandler);
        tpmsSensorRadioBtn.click();
        LOGGER.info("addTPMSSensor completed");
    }

    /**
     * Checks if "TPMS Sensor" text is displayed
     */
    public void assertTPMSSensorLabelDisplayed() {
        LOGGER.info("assertTPMSSensorLabelDisplayed started");
        String textLookUp = TPMS_SENSOR;
        driver.waitForElementNotVisible(By.linkText(TPMS_REBUILD_KIT));
        driver.waitForElementVisible(tpmsSensorLabel);
        Assert.assertTrue("FAIL: Text NOT matched; Found this \"" + tpmsSensorLabel.getText()
                + "\" but expected this \"" + textLookUp + "\"!", tpmsSensorLabel.getText().matches(textLookUp));
        LOGGER.info("assertTPMSSensorLabelDisplayed completed");
    }

    /**
     * Extracts the TPMS Sensor Price
     *
     * @return String
     */
    private String getTPMSSensorPrice() {
        return this.getSpecialPricingOnCartPage(orderListPartsInfo, feeDetailsItemsRowLabelBy,
                feeDetailsItemsRowPriceBy, TPMS_SENSOR);
    }

    /**
     * Assert price on mini cart button
     */
    public void assertMiniCartPrice() {
        LOGGER.info("assertMiniCartPrice started");
        driver.waitForElementVisible(miniCartPrice);
        double miniCartPrc = commonActions.cleanMonetaryStringToDouble(miniCartPrice.getText());
        double subtotalPrc = commonActions.cleanMonetaryStringToDouble(CommonActions.cartSubtotal.getText());
        Assert.assertTrue("FAIL: Minicart price : (" + miniCartPrc + ") not matching to subtotal : ("
                        + subtotalPrc + ")",
                miniCartPrc == subtotalPrc);
        LOGGER.info("assertMiniCartPrice completed");
    }

    /**
     * Verifies the order summary total on cart page
     */
    public void assertCartOrderPriceTotal() {
        LOGGER.info("assertCartOrderPriceTotal started");
        driver.waitForElementVisible(CommonActions.totalAmount);
        double expectedOrderTotal = commonActions.twoDForm(extractCartSubtotal() + commonActions.extractTaxOnCart(), 2);
        double actualOrderTotal = commonActions.cleanMonetaryStringToDouble(CommonActions.totalAmount.getText());
        Assert.assertTrue("FAIL: The actual order total: \"" + actualOrderTotal
                        + "\" did not match to expected order total: \"" + expectedOrderTotal + "\"!",
                actualOrderTotal == expectedOrderTotal);
        LOGGER.info("assertCartOrderPriceTotal completed");
    }

    /**
     * Assert provided "Installation and Spin Balancing" price calculated based on item quantity and
     * expected Install price on cart's page
     */
    public void assertInstallationPrice() {
        LOGGER.info("assertInstallationPrice started");
        int itemQuantity = 0;
        double actualInstallFee = commonActions.cleanMonetaryStringToDouble(getInstallationPrice());
        List<WebElement> productBlocks = webDriver.findElements(CommonActions.cartItemProductBy);
        for (WebElement productBlock : productBlocks) {
            List<WebElement> quantityBoxes = productBlock.findElements(CommonActions.cartItemQuantityBy);
            for (WebElement quantityBox : quantityBoxes) {
                if (driver.isElementDisplayed(quantityBox, Constants.ZERO))
                    itemQuantity += Integer.parseInt(quantityBox.getAttribute(Constants.VALUE));
            }
        }
        double fee = 0.00;
        fee = getInstallationFeeForSiteRegionAndDataSet();
        double expectedInstallFee = (fee * itemQuantity);
        Assert.assertEquals("FAIL: Calculated Installation price incorrect. Expected: " + expectedInstallFee +
                ". Displayed: " + actualInstallFee + ".", expectedInstallFee, actualInstallFee, 0.0);
        LOGGER.info("assertInstallationPrice completed");
    }

    /**
     * Assert provided "TPMS Sensor" price matches with the rendered price
     * on cart's page
     *
     * @param tpmsSensorFee The string fee to check
     */
    public void assertTPMSSensorPrice(String tpmsSensorFee) {
        LOGGER.info("assertTPMSSensorPrice started");
        int itemQuantity = Integer.parseInt(webDriver.findElement(CommonActions.cartItemQuantityBy).
                getAttribute(Constants.VALUE));
        double fee = Double.parseDouble(tpmsSensorFee);
        double expectedTPMSSensorFee = (fee * itemQuantity);
        double actualTPMSRebuildPrice = Double.parseDouble(getTPMSSensorPrice());
        Assert.assertTrue("FAIL: TPMS Rebuild Kits Price didn't match! (displayed:-> " + actualTPMSRebuildPrice
                + " with expected:->  " + expectedTPMSSensorFee + ")", expectedTPMSSensorFee == actualTPMSRebuildPrice);
        LOGGER.info("assertTPMSSensorPrice completed");
    }

    /**
     * Verify element exists in shopping cart
     *
     * @param element element to check
     * @param text    expected element text
     */
    public void verifyElementInCart(String element, String text) {
        LOGGER.info("verifyElementInCart started");

        //TODO: retest when new safaridriver is stable
        if (Config.isSafari()) {
            driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        }

        if (!Config.isSafari()) {
            driver.waitForPageToLoad();
        } else {
            driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        }

        driver.waitForElementVisible(productSpec);
        boolean textPresent;
        if (element.equalsIgnoreCase(ConstantsDtc.PRODUCT_NAME)) {
            textPresent = driver.waitForTextPresentIgnoreCase(CommonActions.productNameBy, text, Constants.THREE);
        } else if (element.equalsIgnoreCase(ConstantsDtc.ITEM_CODE)) {
            textPresent = driver.waitForTextPresent(cartItemCodeNonStaggeredBy, text, Constants.THREE);
        } else if (element.equalsIgnoreCase(ConstantsDtc.PRODUCT_BRAND)) {
            textPresent = driver.waitForTextPresent(CommonActions.brandNameBy, text, Constants.THREE);
        } else if (element.equalsIgnoreCase(ConstantsDtc.PRODUCT_SIZE)) {
            textPresent = driver.waitForTextPresent(cartItemProductSize, text, Constants.THREE);
        } else {
            textPresent = driver.waitForTextPresent(cartItemCodeNonStaggeredBy, element, Constants.THREE);
        }
        Assert.assertTrue("'" + text + "' was not present on the cart page", textPresent);
        LOGGER.info("verifyElementInCart completed");
    }

    /**
     * Selects the 'Checkout Now' button on the Cart page
     */
    public void selectCheckoutNow() {
        LOGGER.info("selectCheckoutNow started");
        //NOTE: Firefox flashes the page, then displays blank page for several seconds
        //This caused waitForPageToLoad to only work intermittently. Adding a second call resolves it
        if (Config.isFirefox() || Config.isSafari())
            driver.waitForPageToLoad();

        driver.waitForPageToLoad();
        driver.jsScrollToElementClick(checkoutButton, false);
        LOGGER.info("selectCheckoutNow completed");
    }

    /**
     * Takes the desired action on the Certificates for Repair and Replacement popup
     *
     * @param action Continue to Checkout OR Add the certificates for product
     */
    public void selectActionOnPopUpCertsForRandR(String action) {
        LOGGER.info("selectActionOnPopUpCertsForRandR started");
        driver.waitForPageToLoad(Constants.ZERO);
        if (driver.isElementDisplayed(certForRRPopUpBy)) {
            String buttonText;
            if (action.toLowerCase().contains(ConstantsDtc.CONTINUE_TO_CHECKOUT.toLowerCase())) {
                buttonText = ConstantsDtc.CONTINUE_TO_CHECKOUT;
            } else {
                buttonText = ConstantsDtc.ADD;
            }
            driver.clickElementWithText(CommonActions.buttonBy, buttonText);
        }
        LOGGER.info("selectActionOnPopUpCertsForRandR completed");
    }

    /**
     * Extracts the parent section for selected item
     *
     * @param itemCode product code
     * @return itemParents Web elements list Grand Parent for item Code
     */
    public List<WebElement> getRowParents(String itemCode) {
        LOGGER.info("getRowParents started");
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        WebElement productCode = null;
        if (driver.scenarioData.isStaggeredProduct())
            productCode = driver.getElementWithText(cartItemCodeStaggeredBy, itemCode);
        else
            productCode = driver.getElementWithText(cartItemCodeNonStaggeredBy, itemCode);
        WebElement itemGrandParent = driver.getParentElement(productCode);
        do {
            itemGrandParent = driver.getParentElement(itemGrandParent);
        } while (!itemGrandParent.getText().contains(ConstantsDtc.SUBTOTAL));
        List<WebElement> itemParents = itemGrandParent.findElements(feeDetailsItemsRowParentBy);
        LOGGER.info("getRowParents completed");
        driver.resetImplicitWaitToDefault();
        return itemParents;
    }

    /**
     * Extracts the Pricing among multiple Web elements that contains text substring
     *
     * @param itemParents The Web elements parent list with
     * @param text        The string to lookup the specific element
     * @param text1       Fee per Each item /Total fee for items
     * @return pricing
     */
    public double getPrice(List<WebElement> itemParents, String text, String text1) {
        LOGGER.info("getPrice started for:" + text);
        By feeElement;
        double pricing = Constants.ZERO;
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        for (WebElement itemParent : itemParents) {
            if (CommonUtils.containsIgnoreCase(itemParent.getText(), text)) {
                if (itemParent.findElement(feeDetailsItemsRowLabelBy).getText().contains(text)) {
                    if (text1.toLowerCase().contains(ConstantsDtc.TOTAL.toLowerCase())) {
                        feeElement = feeDetailsItemsRowPriceBy;
                    } else {
                        feeElement = serviceFeeItemRowPriceBy;
                    }
                    pricing = commonActions.cleanMonetaryStringToDouble(itemParent.findElement(feeElement).getText());
                    break;
                }
            }
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("getPrice completed for:" + text);
        return pricing;
    }

    /**
     * Assert provided "Installation and Spin Balancing" price calculated based on item quantity and
     * rendered price on cart's page
     *
     * @param quantity Webelement quantity Parent
     * @param price    The double fee to compare with the calculated fee
     */
    public void assertInstallationPrice(double price, WebElement quantity) {
        LOGGER.info("assertInstallationPrice started");
        int itemQuantity = ConstantsDtc.DEFAULT_QUANTITY;
        List<WebElement> items = driver.getParentElement(quantity).findElements(feeDetailsItemsRowParentBy);
        for (WebElement item : items) {
            if (item.findElement(feeDetailsItemsRowLabelBy).getText().contains(ConstantsDtc.INSTALLATION)) {
                itemQuantity = Integer.parseInt(item.findElement(itemRowQuantityBy).getText());
                break;
            }
        }
        double fee = getInstallationFeeForSiteRegionAndDataSet();
        double InstallationFee = (fee * itemQuantity);
        Assert.assertTrue("FAIL: Installation Price didn't match! (displayed:-> "
                + price + " expected:->  " + InstallationFee + ")", price == InstallationFee);
        LOGGER.info("assertInstallationPrice completed");
    }

    /**
     * Assert provided Item price on pdp page matches with the item price on cart page
     * Assert itemTotal = PDP item price * quantity
     */
    public void assertCartProductPriceTotal() {
        LOGGER.info("assertCartProductPriceTotal started");
        driver.waitForElementVisible(priceBox);
        int itemQuantity = Integer.parseInt(webDriver.findElement(CommonActions.cartItemQuantityBy).
                getAttribute(Constants.VALUE));
        double actualItemTotal = commonActions.cleanMonetaryStringToDouble(itemTotal.getText());
        double actualItemPrice = commonActions.cleanMonetaryStringToDouble(
                webDriver.findElement(itemPriceEachBy).getText());
        double expectedItemPrice = 0.0;
        String savedPrice = commonActions.productInfoListGetValue(Constants.PRICE);
        if (savedPrice.equalsIgnoreCase(ConstantsDtc.SEE_PRICE_IN_CART))
            expectedItemPrice = actualItemPrice;
        else
            expectedItemPrice = commonActions.cleanMonetaryStringToDouble(savedPrice);
        double expectedItemTotal = (expectedItemPrice * itemQuantity);
        Assert.assertEquals("FAIL: Item price PDP Page (" + expectedItemPrice +
                ") not Equal to item price on cart page (" + actualItemPrice,
                expectedItemPrice, actualItemPrice, 0.0);
        Assert.assertEquals("FAIL: The actual item-total (" + actualItemTotal +
                ") not equal to Expected item-Total Price (" + expectedItemTotal + ")",
                expectedItemTotal, actualItemTotal, 0.0);
        LOGGER.info("assertCartProductPriceTotal completed");
    }

    /**
     * Assert provided Item price on pdp page is updated and displayed on cart page
     *
     * @param raise Increase or Decrease
     * @param value Numeric value by which price changed
     */
    public void assertCartProductPriceUpdatedTo(String raise, double value) {
        LOGGER.info("assertCartProductPriceUpdatedTo " + raise + " by value '" + value + "' started.");
        double expectedPdpPrice = commonActions.cleanMonetaryStringToDouble(
                commonActions.productInfoListGetValue(Constants.PRICE));
        driver.waitForElementVisible(priceBox);
        if (raise.equalsIgnoreCase(Constants.INCREASED)) {
            expectedPdpPrice += value;
        } else {
            expectedPdpPrice -= value;
        }
        int itemQuantity = Integer.parseInt(webDriver.findElement(CommonActions.cartItemQuantityBy).
                getAttribute(Constants.VALUE));
        double actualItemTotal = commonActions.cleanMonetaryStringToDouble(itemTotal.getText());
        double actualItemPrice = commonActions.cleanMonetaryStringToDouble(
                webDriver.findElement(itemPriceEachBy).getText());
        double expectedItemTotal = (expectedPdpPrice * itemQuantity);
        Assert.assertTrue("FAIL: Item price on Cart Page expected to be (" + expectedPdpPrice +
                ") is not equal to actual item price on cart page " +
                "(" + actualItemPrice + ")", expectedPdpPrice == actualItemPrice);
        Assert.assertTrue("FAIL: The actual item-total (" + actualItemTotal +
                        ") not equal to expected item-total Price (" + expectedItemTotal + ")",
                expectedItemTotal == actualItemTotal);
        LOGGER.info("assertCartProductPriceUpdatedTo  " + raise + " by value '" + value + "' completed");
    }

    /**
     * Assert provided Item price on pdp page is updated and displayed on cart page
     */
    public void assertCartProductPricesChanged() {
        LOGGER.info("assertCartProductPricesChanged started");
        driver.waitForElementVisible(priceBox);
        int productCount = driver.scenarioData.productInfoList.size();
        List<WebElement> cartRows = webDriver.findElements(cartItemDetailsParentBy);

        for (WebElement cartRow : cartRows) {
            for (int index = 0; index < productCount; index++) {
                String productName = commonActions.productInfoListGetValue(ConstantsDtc.PRODUCT, index);
                if (productName.equalsIgnoreCase(cartRow.findElement(CommonActions.productNameBy).getText())) {
                    double originalPrice = commonActions.cleanMonetaryStringToDouble(commonActions.
                            productInfoListGetValue(Constants.PRICE, index));
                    int itemQuantity = Integer.parseInt(cartRow.findElement(cartItemQuantityBy).
                            getAttribute(Constants.VALUE));
                    double actualItemPrice = commonActions.cleanMonetaryStringToDouble(
                            cartRow.findElement(itemPriceEachBy).getText());
                    double originalItemTotal = (originalPrice * itemQuantity);
                    double actualItemTotal = commonActions.cleanMonetaryStringToDouble(
                            cartRow.findElement(itemPriceNonStaggeredBy).getText());
                    Assert.assertNotEquals("FAIL: Item price on Cart Page (" + actualItemPrice +
                                    ") is equal to the original price (" + originalPrice + "). Expected it to change",
                            originalPrice, actualItemPrice, 0.0);
                    Assert.assertNotEquals("FAIL: Item-total on Cart Page (" + actualItemTotal +
                                    ") is equal to the original item-total (" + originalItemTotal + "). Expected it to change.",
                            originalItemTotal, actualItemTotal, 0.0);
                }
            }
        }
        LOGGER.info("assertCartProductPricesChanged completed");
    }

    /**
     * Extract product details on Cart page having product code and product price
     *
     * @return product details map
     */
    public HashMap<String, String> getProductPriceOnCart() {
        LOGGER.info("getProductPriceOnCart started");
        List<WebElement> itemElements;
        By itemPriceBy = itemPriceNonStaggeredBy;
        By itemCodeBy = cartItemCodeNonStaggeredBy;
        if (driver.isElementDisplayed(staggeredDetailsFrontContainer, Constants.ONE)) {
            WebElement view = webView;
            if (driver.isElementDisplayed(mobileView, Constants.ONE))
                view = mobileView;
            List<WebElement> itemElementsFront = view.findElements(staggeredDetailsFrontContainer);
            List<WebElement> itemElementsRear = view.findElements(staggeredDetailsRearContainer);
            itemElements = itemElementsFront;
            itemElements.addAll(itemElementsRear);
            itemPriceBy = itemPriceStaggeredBy;
            itemCodeBy = cartItemCodeStaggeredBy;
        } else {
            itemElements = webDriver.findElements(cartItemProductBy);
        }
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        for (WebElement itemElement : itemElements) {
            productPriceOnCart.put(itemElement.findElement(itemCodeBy).getText().split("#")[1].trim(),
                    itemElement.findElement(itemPriceBy).getText());
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("getProductPriceOnCart completed");
        return productPriceOnCart;
    }

    /**
     * Get product parent mapping with product code
     *
     * @return productParent map
     */
    public HashMap<String, WebElement> getProductParent() {
        LOGGER.info("getProductParent started");
        List<String> itemCodes = getAllItemCodesOnCart();
        for (String item : itemCodes) {
            productParent.put(item, commonActions.getProductSectionTopElement(item));
        }
        LOGGER.info("getProductParent completed");
        return productParent;
    }

    /**
     * Get all the item codes that are currently on the cart
     *
     * @return List of String item codes
     */
    public List<String> getAllItemCodesOnCart() {
        LOGGER.info("getAllItemCodesOnCart started");
        List<String> itemCodes = new ArrayList<>();
        for (int i = 0; i < driver.scenarioData.productInfoList.size(); i++) {
            if (Boolean.parseBoolean(commonActions.productInfoListGetValue(ConstantsDtc.IN_CART, i))) {
                String item = commonActions.productInfoListGetValue(ConstantsDtc.ITEM, i);
                itemCodes.add(item);
                LOGGER.info(item + " is in cart");
            }
        }
        LOGGER.info("getAllItemCodesOnCart completed");
        return itemCodes;
    }

    /**
     * Set product parent mapping
     *
     * @param productParent map
     */
    public void setProductParent(HashMap<String, WebElement> productParent) {
        CartPage.productParent = productParent;
    }

    /**
     * Get fee parents mapping with product code
     *
     * @return fee parents map
     */
    public HashMap<String, List<WebElement>> getFeeParents() {
        LOGGER.info("getFeeParents started");
        getProductPriceOnCart();
        List<String> items = getAllItemCodesOnCart();
        for (String item : items) {
            feeParents.put(item, getRowParents(item));
        }
        LOGGER.info("getFeeParents completed");
        return feeParents;
    }

    /**
     * Set fee parents mapping
     *
     * @param feeParents map
     */
    public static void setFeeParents(HashMap<String, List<WebElement>> feeParents) {
        CartPage.feeParents = feeParents;
    }

    /**
     * Assert provided for items on Cart page with PLP page item codes and prices
     */
    public void assertProductsAndPricesOnCartPageFromPlp() {
        LOGGER.info("assertProductsAndPricesOnCartPageFromPlp started");
        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);
        getProductPriceOnCart();
        for (int i = 0; i < driver.scenarioData.productInfoList.size(); i++) {
            for (int j = 0; j < productPriceOnCart.size(); j++) {
                String itemCodeText = productPriceOnCart.keySet().toArray()[j].toString();
                if (commonActions.productInfoListGetValue(ConstantsDtc.ITEM, i).equals(itemCodeText)) {
                    String actualPrice = productPriceOnCart.get(itemCodeText);
                    String expectedPrice = commonActions.productInfoListGetValue(Constants.PRICE, i).replace("$", "").trim();
                    if (!expectedPrice.equalsIgnoreCase(ConstantsDtc.SEE_PRICE_IN_CART)) {
                        expectedPrice = String.valueOf(String.valueOf(Double.parseDouble(expectedPrice) *
                                Double.parseDouble(commonActions.productInfoListGetValue(ConstantsDtc.QUANTITY))));
                        BigDecimal formattedPrice = new BigDecimal(expectedPrice).setScale(2, RoundingMode.CEILING);
                        expectedPrice = "$" + formattedPrice;
                        Assert.assertTrue("FAIL: Item Price for Item ID " + itemCodeText + " didn't match! (displayed:-> "
                                        + actualPrice + " expected:->  " + expectedPrice + ")",
                                commonActions.cleanMonetaryStringToDouble(expectedPrice) ==
                                        commonActions.cleanMonetaryStringToDouble(actualPrice));
                    } else {
                        commonActions.productInfoListSetValue(Constants.PRICE.toUpperCase(), i, actualPrice);
                    }
                }
            }
        }
        LOGGER.info("assertProductsAndPricesOnCartPageFromPlp completed");
    }

    /**
     * Extract items Total On cart page
     *
     * @param itemCode product code
     * @return itemsTotal
     */
    public double extractItemsPriceTotal(String itemCode) {
        LOGGER.info("extractItemsPriceTotal started");
        WebElement itemPrice;
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        try {
            itemPrice = productParent.get(itemCode).findElement(itemPriceNonStaggeredBy);
        } catch (Exception e) {
            itemPrice = productParent.get(itemCode).findElement(itemPriceStaggeredBy);
        }
        driver.resetImplicitWaitToDefault();
        double itemTotal = commonActions.cleanMonetaryStringToDouble(itemPrice.getText());
        itemTotal = commonActions.twoDForm(itemTotal, Constants.TWO);
        LOGGER.info("extractItemsPriceTotal completed");
        return itemTotal;
    }

    /**
     * Assert items total On Cart page with item price on Plp page multiplied with quantity
     */
    public void assertItemsPriceTotal() {
        LOGGER.info("assertItemsPriceTotal started");
        List<String> itemCodes = getAllItemCodesOnCart();
        setProductParent(getProductParent());
        for (String itemCode : itemCodes) {
            double iItemTotal = extractItemsPriceTotal(itemCode);
            double expectedTotal = commonActions.getTotalItemPrice(itemCode);
            if (iItemTotal != expectedTotal) {
                Assert.fail("FAIL: Item total Price didn't match! (displayed:-> "
                        + itemTotal + " expected:->  " + expectedTotal + ")");
            }
        }
        LOGGER.info("assertItemsPriceTotal completed");
    }

    /**
     * Calculate total fee on cart page by getting the calculated fee amount multiplied with quantity
     *
     * @param text     fee type
     * @param itemCode product code
     * @return calculated fee
     */
    public double calculateFeesOnCart(String text, String itemCode) {
        LOGGER.info("calculateFeesOnCart started for " + text);
        double fee = 0.00;
        double calculatedFee;
        int itemQuantity;
        setProductParent(getProductParent());
        if (driver.isTextPresentInPageSource(OrderPage.ORDER_CONFIRMATION_MESSAGE)) {
            if (Config.isMobile()) {
                itemQuantity = Integer.parseInt(productParent.get(itemCode).
                        findElements(cartItemQuantityBy).get(1).getText());
            } else {
                itemQuantity = Integer.parseInt(productParent.get(itemCode).findElement(cartItemQuantityBy).getText());
            }
        } else {
            if (driver.scenarioData.isStaggeredProduct()) {
                itemQuantity = Integer.parseInt(extractElementContainingFeeData(text).
                        findElement(itemRowQuantityBy).getText());
            } else {
                itemQuantity = Integer.parseInt(productParent.get(itemCode).findElement(cartItemQuantityBy).
                        getAttribute(Constants.VALUE));
            }
        }
        if (text.equalsIgnoreCase(ConstantsDtc.INSTALLATION)) {
            fee = getInstallationFeeForSiteRegionAndDataSet();
            itemQuantity = Integer.parseInt(extractElementContainingFeeData(text).
                    findElement(itemRowQuantityBy).getText());
        } else {
            if (text.equalsIgnoreCase(ConstantsDtc.DISPOSAL_FEE)) {
                fee = getCalculatedDisposalFee(itemCode);
            } else {
                fee = extractFeeForItemOnCart(text, itemCode);
            }
        }
        calculatedFee = (fee * itemQuantity);
        LOGGER.info("calculateFeesOnCart completed for " + text);
        return calculatedFee;
    }

    /**
     * Extract the total PRICE fee on Cart page based on fee type and product code
     *
     * @param text     fee type
     * @param itemCode product code
     * @return extracted fee
     */
    public double extractFeesForItemsOnCart(String text, String itemCode) {
        LOGGER.info("extractFeesForItemsOnCart started for " + text);
        double extractedFee;
        extractedFee = getPrice(feeParents.get(itemCode), text, ConstantsDtc.TOTAL);
        LOGGER.info("extractFeesForItemsOnCart completed for " + text);
        return extractedFee;
    }

    /**
     * Extract the EACH fee on Cart page based on fee type and product code for
     *
     * @param text     fee type
     * @param itemCode product code
     * @return extracted fee
     */
    public double extractFeeForItemOnCart(String text, String itemCode) {
        LOGGER.info("extractFeeForItemOnCart started for " + text);
        double extractedFee;
        setFeeParents(getFeeParents());
        extractedFee = getPrice(feeParents.get(itemCode), text, ConstantsDtc.EACH);
        LOGGER.info("extractFeeForItemOnCart completed for " + text);
        return extractedFee;
    }

    /**
     * Verify displayed fee for all products on cart page with calculated fee based on fee type
     *
     * @param feeType fee type
     */
    public void assertFeesForItemsOnCart(String feeType) {
        LOGGER.info("assertFeesForItemsOnCart started for '" + feeType + "'");
        List<String> itemCodes = getAllItemCodesOnCart();
        for (String itemCode : itemCodes) {
            assertFeesForItemOnCart(feeType, itemCode);
        }
        LOGGER.info("assertFeesForItemsOnCart started for '" + feeType + "'");
    }

    /**
     * Verify displayed fee for product on cart page with calculated fee based on fee type
     *
     * @param feeType  fee type
     * @param itemCode product code
     */
    public void assertFeesForItemOnCart(String feeType, String itemCode) {
        LOGGER.info("assertFeesForItemOnCart started for '" + feeType + "' fee for item " + itemCode + "'");
        double calculatedFee = calculateFeesOnCart(feeType, itemCode);
        double extractedFee = extractFeesForItemsOnCart(feeType, itemCode);
        Assert.assertTrue("FAIL: Fee Price didn't match for " + feeType + "!, (displayed:-> " + extractedFee +
                " expected:->  " + calculatedFee + ")", extractedFee == calculatedFee && extractedFee > 0);
        LOGGER.info("assertFeesForItemOnCart completed for '" + feeType + "' fee for item " + itemCode + "'");
    }

    /**
     * Verifies the specified item appears on the Cart page with the expected quantity
     *
     * @param itemNumber       The item number expected to be on the Cart page
     * @param expectedQuantity The expected quantity of the specified item number
     */
    public void assertItemQuantityOnCartPage(String itemNumber, String expectedQuantity) {
        LOGGER.info("assertItemQuantityOnCartPage started with item '" + itemNumber
                + "' and expected quantity of '" + expectedQuantity + "'");
        int index = 0;
        if (Config.isMobile()) {
            index = 1;
        }
        driver.waitForElementVisible(orderList);
        WebElement productParent = driver.getParentElement(commonActions.getProductSectionTopElement(itemNumber));
        WebElement itemQuantityField = productParent.findElements(cartItemQuantityBy).get(index);
        String itemQuantity;
        if (itemQuantityField != null) {
            if (itemQuantityField.getTagName().equalsIgnoreCase(Constants.INPUT)) {
                itemQuantity = itemQuantityField.getAttribute(Constants.VALUE);
            } else {
                itemQuantity = itemQuantityField.getText();
            }
            Assert.assertTrue("FAIL: For item '" + itemNumber + "' the expected quantity was '"
                            + expectedQuantity + "' but was actually '" + itemQuantity + "'!",
                    itemQuantity.equalsIgnoreCase(expectedQuantity));
        } else {
            Assert.fail("FAIL: Unable to find the item quantity field for item number '" + itemNumber + "'!");
        }
        LOGGER.info("assertItemQuantityOnCartPage completed with item '" + itemNumber
                + "' and expected quantity of '" + expectedQuantity + "'");
    }

    /**
     * Extracts the Pricing for optional fee
     *
     * @param itemCode The product code
     * @param feeType  The string to lookup the specific optional fee
     * @return pricing
     */
    public double extractPriceForOptionalFee(String itemCode, String feeType) {
        LOGGER.info("getPriceForOptionalFee started for:" + feeType);
        double pricing = Constants.ZERO;
        WebElement itemGrandParent;
        WebElement priceElement;

        if (itemCode.equalsIgnoreCase(Constants.NONE)) {
            itemGrandParent = webDriver.findElement(cartItemDetailsParentBy);
        } else {
            itemGrandParent = driver.getParentElement(commonActions.getProductSectionTopElement(itemCode));
        }
        List<WebElement> feeForSelectionElements = null;
        List<WebElement> productSections = webDriver.findElements(CartPage.cartItemDetailsParentBy);
        WebElement productSectionWithItem = null;
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        for (WebElement productSection : productSections) {
            if (productSection.getText().contains(itemCode) || itemCode.equalsIgnoreCase(Constants.NONE)) {
                if (driver.scenarioData.isStaggeredProduct()) {
                    feeForSelectionElements = productSection.findElement(staggeredCertificatesContainerBy).
                            findElements(CommonActions.optionNameBy);
                } else {
                    feeForSelectionElements = itemGrandParent.findElements(CommonActions.optionNameBy);
                }
                productSectionWithItem = productSection;
                break;
            }
        }
        driver.resetImplicitWaitToDefault();
        Assert.assertTrue("FAIL: Could not find the Optional Fee section on Cart page", productSectionWithItem != null);
        for (WebElement element : feeForSelectionElements) {
            if (element.getText().contains(feeType)) {
                WebElement pricingElement = element;
                while (!pricingElement.getText().contains("$")) {
                    pricingElement = driver.getParentElement(pricingElement);
                }
                if (feeType.equalsIgnoreCase(ConstantsDtc.CERTIFICATES)) {
                    priceElement = pricingElement.findElement(feeDetailsItemsRowPriceBy);
                } else {
                    priceElement = productSectionWithItem.findElement(tpmsPriceBy);
                }
                pricing += commonActions.cleanMonetaryStringToDouble(priceElement.getText());
            }
        }
        LOGGER.info("getPriceForOptionalFee completed for:" + feeType);
        return pricing;
    }

    /**
     * Select the optional fee
     *
     * @param itemCode The product code
     * @param feeToAdd The string to lookup the specific optional fee
     */
    public void addOptionalFeeOnCartPage(String itemCode, String feeToAdd) {
        LOGGER.info("addOptionalFeeOnCartPage started");
        if (itemCode.contains(Constants.NONE)) {
            List<WebElement> serviceOption = null;
            int counter = 0;
            while (serviceOption == null && counter < 10) {
                driver.waitForMilliseconds();
                serviceOption = driver.getElementsWithText(feeServiceItemLabelBy, feeToAdd);
                counter++;
            }
            Assert.assertTrue("FAIL: The " + feeToAdd + " fee could not be selected", serviceOption != null);
            for (WebElement option : serviceOption) {
                WebElement addon = driver.getNthParentElement(option, "div", "3");
                driver.jsScrollToElementClick(option);
                driver.waitSeconds(Constants.TWO);
                if (feeToAdd.equalsIgnoreCase(ConstantsDtc.CERTIFICATES)) {
                    String feeProductCode = returnSubProductCode(ConstantsDtc.CERTIFICATES);
                    String feeQuantity = addon.findElement(itemRowQuantityBy).getText();
                    String feeRetailPrice = Double.valueOf(addon.findElement(serviceFeeItemRowPriceBy).getText().
                            replaceAll("[^0-9.]+", "")).toString();
                    String feeTotalPrice = Double.valueOf(addon.findElement(feeDetailsItemsRowPriceBy).getText().
                            replaceAll("[^0-9.]+", "")).toString();
                    String feeLineItem = OrderXmls.PRODUCTS + ":" + feeProductCode + "," + Constants.JSON_PARAM_QUANTITY
                            + ":" + feeQuantity + "," + OrderXmls.RETAIL_PRICE + ":" + feeRetailPrice + ","
                            + OrderXmls.SALES_EXTENDED + ":" + feeTotalPrice;
                    driver.scenarioData.genericData.put(feeRetailPrice.split("\\.")[0], feeLineItem);
                }
            }
        } else {
            WebElement itemGrandParent = driver.getParentElement(commonActions.getProductSectionTopElement(itemCode));
            List<WebElement> feeForSelectionElements = itemGrandParent.findElements(CommonActions.optionNameBy);
            driver.waitForElementVisible(CommonActions.optionNameBy, Constants.ZERO);
            for (WebElement element : feeForSelectionElements) {
                if (element.getText().contains(feeToAdd)) {
                    driver.jsScrollToElementClick(element);
                    driver.waitForPageToLoad();
                    break;
                }
            }
        }
        LOGGER.info("addOptionalFeeOnCartPage completed");
    }

    /**
     * Calculate optional fee on cart page by getting the calculated fee amount multiplied with quantity
     *
     * @param feeType  Optional fee type
     * @param itemCode product code
     * @return calculated fee
     */
    public double calculateOptionalFeesOnCart(String itemCode, String feeType) {
        LOGGER.info("calculateOptionalFeesOnCart started for " + feeType);
        double fee = 0.00;
        double calculatedFee;
        setProductParent(getProductParent());
        int itemQuantity = Integer.parseInt(productParent.get(itemCode).
                findElement(cartItemQuantityBy).getAttribute(Constants.VALUE));
        List<WebElement> feeForSelectionElements = driver.getParentElement(
                commonActions.getProductSectionTopElement(itemCode)).findElements(CommonActions.optionNameBy);
        driver.waitForElementVisible(CommonActions.optionNameBy, Constants.ZERO);
        for (WebElement element : feeForSelectionElements) {
            if (element.getText().contains(feeType)) {
                fee = commonActions.cleanMonetaryStringToDouble(element.getText());
            }
        }
        calculatedFee = (fee * itemQuantity);
        LOGGER.info("calculateOptionalFeesOnCart completed for " + feeType);
        return calculatedFee;
    }

    /**
     * Verify the optional fee price on cart page with the expected fee price
     *
     * @param text Optional fee type
     */
    public void assertOptionalFeePriceOnCart(String text) {
        LOGGER.info("assertOptionalFeePriceOnCart started for " + text);
        Set<String> itemCodes = cartProductPrice.keySet();
        for (String itemCode : itemCodes) {
            double calculatedFee = calculateOptionalFeesOnCart(itemCode, text);
            double extractedFee = extractPriceForOptionalFee(itemCode, text);
            Assert.assertTrue("FAIL: Installation Price didn't match! (displayed:-> "
                    + extractedFee + " expected:->  " + calculatedFee + ")", extractedFee == calculatedFee);
        }
        LOGGER.info("assertOptionalFeePriceOnCart completed for " + text);
    }

    /**
     * Update quantity for the product on cart page
     *
     * @param quantity Quantity to update
     * @param item     product code
     */
    public void updateQuantityForItem(String item, String quantity) {
        LOGGER.info("updateQuantityForItem started");
        driver.waitForPageToLoad();
        int itemQuantityByIndex = 0;
        if (Config.isMobile()) {
            itemQuantityByIndex = 1;
        }
        WebElement itemQuantity = commonActions.getProductSectionTopElement(item).
                findElements(cartItemQuantityBy).get(itemQuantityByIndex);
        driver.jsScrollToElementClick(itemQuantity);
        commonActions.clearAndPopulateEditField(itemQuantity, quantity);
        if (Config.isSafari()) {
            itemQuantity.sendKeys(Keys.ENTER);
            driver.waitForMilliseconds();
        } else if (Config.isIe()) {
            priceBox.click();
        } else {
            itemQuantity.sendKeys(Keys.RETURN);
        }
        driver.waitForPageToLoad();
        LOGGER.info("updateQuantityForItem completed");
    }

    /**
     * Remove item from the cart page
     *
     * @param itemCode product code
     */
    public void clickDeleteIconForItem(String itemCode) {
        LOGGER.info("clickDeleteIconForItem started");
        WebElement remove = commonActions.getProductSectionTopElement(itemCode).findElement(removeItemBy);
        if (Config.isSafari()) {
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        }
        driver.waitForElementVisible(remove);
        driver.jsScrollToElementClick(remove);
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        driver.clickElementWithText(CommonActions.buttonBy, ConstantsDtc.REMOVE_ITEM_FROM_CART);
        driver.waitForPageToLoad();
        LOGGER.info("clickDeleteIconForItem completed");
    }

    /**
     * Verify removed item not displayed on cart page
     *
     * @param itemCode product code
     */
    public void assertRemovedItemNotDisplayed(String itemCode) {
        LOGGER.info("assertRemovedItemNotDisplayed started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(cartItemCodeNonStaggeredBy);
        Assert.assertTrue("FAIL: Product not removed from the cart page",
                driver.getElementsWithText(cartItemCodeNonStaggeredBy, itemCode).size() < 1);
        LOGGER.info("assertRemovedItemNotDisplayed completed");
    }

    /**
     * Verify fee label is displayed for item
     *
     * @param text     fee type
     * @param itemCode product code
     */
    public void assertFeeLabelDisplayed(String text, String itemCode) {
        LOGGER.info("assertFeeLabelDisplayed started for " + text);
        boolean StringFound = false;

        if (itemCode.equalsIgnoreCase(Constants.NONE)) {
            WebElement elementLabel = driver.getElementWithText(childTip, text);
            if (elementLabel != null && driver.isTextPresentInPageSource(text)) {
                StringFound = true;
                Assert.assertTrue("FAIL: Fee label NOT matched; Found this \"" + elementLabel.getText()
                        + "\" but expected this \"" + text + "\"!", elementLabel.getText().matches(text));
            }
        } else {
            List<WebElement> labels = driver.getParentElement(
                    commonActions.getProductSectionTopElement(itemCode)).findElements(feeDetailsItemsRowLabelBy);
            for (WebElement label : labels) {
                if (label.getText().contains(text)) {
                    StringFound = true;
                    Assert.assertTrue("FAIL:Fee label" + text + "not displayed for " + itemCode,
                            label.getText().contains(text));
                    break;
                }
            }
        }
        if (!StringFound) {
            Assert.fail("FAIL: Fee label \"" + text + "\" NOT displayed !");
        }
        LOGGER.info("assertFeeLabelDisplayed completed for " + text);
    }

    /**
     * Verify the fee quantity displayed for respective product code
     *
     * @param feeType  fee type displayed for product
     * @param itemCode product code
     * @param quantity fee quantity
     */
    public void assertFeeQuantity(String feeType, String itemCode, String quantity) {
        LOGGER.info("assertFeeQuantity started for " + feeType);
        List<WebElement> items = driver.getParentElement(
                commonActions.getProductSectionTopElement(itemCode)).findElements(feeDetailsItemsRowParentBy);
        for (WebElement item : items) {
            if (item.findElement(feeDetailsItemsRowLabelBy).getText().contains(feeType)) {
                String feeQuantity = item.findElement(itemRowQuantityBy).getText();
                Assert.assertTrue("FAIL: Fee quantity didn't match for" + feeType + "(displayed:-> "
                        + feeQuantity + " expected:->  " + quantity + ")", feeQuantity.equals(quantity));
                break;
            }
        }
        LOGGER.info("assertFeeQuantity completed for " + feeType);
    }

    /**
     * Verify Order Summary section verbiage on cart page
     */
    public void assertCartSummaryVerbiagesDisplay() {
        LOGGER.info("assertCartSummaryVerbiagesDisplay started");
        driver.waitForElementVisible(cartSummaryHeadingBy);
        WebElement orderSummaryVerbiage = driver.getElementWithText(cartSummaryHeadingBy, ConstantsDtc.ORDER_SUMMARY);
        WebElement cartSubtotalVerbiage = driver.getElementWithText(cartSummaryBreakDownNameBy,
                ConstantsDtc.CART_SUBTOTAL);
        WebElement estimatedTaxVerbiage = driver.getElementWithText(cartSummaryBreakDownNameBy,
                ConstantsDtc.ESTIMATED_TAXES);
        WebElement totalVerbiage = driver.getElementWithText(cartItemTotalPriceBy, ConstantsDtc.TOTAL);

        Assert.assertTrue("FAIL: Order Summary Verbiage NOT displayed!",
                driver.isElementDisplayed(orderSummaryVerbiage));
        Assert.assertTrue("FAIL: Cart Total Verbiage NOT displayed!", driver.isElementDisplayed(totalVerbiage));
        Assert.assertTrue("FAIL: Cart Subtotal Verbiage NOT displayed!",
                driver.isElementDisplayed(cartSubtotalVerbiage));
        Assert.assertTrue("FAIL: Estimated Taxes Verbiage not displayed!",
                driver.isElementDisplayed(estimatedTaxVerbiage));
        LOGGER.info("assertCartSummaryVerbiagesDisplay completed");
    }

    /**
     * Calculates & Set Environment Fee to a class variable for later use across pages
     */
    public void setEnvironmentFee() {
        LOGGER.info("setEnvironmentFee started");
        if (Config.isSafari()) {
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        }
        environmentFee = String.valueOf(commonActions.cleanMonetaryStringToDouble(
                getFeeTotal(ConstantsDtc.ENVIRONMENTAL_FEE)));
        LOGGER.info("setEnvironmentFee completed");
    }

    /**
     * Calculate item subtotal on cart page by sum of product price and fees applied for item
     *
     * @param itemCode product code
     */
    public double calculateItemSubtotal(String itemCode) {
        LOGGER.info("calculateItemSubtotal started");
        driver.waitForPageToLoad();
        setProductParent(getProductParent());
        setFeeParents(getFeeParents());
        double itemSubtotal = extractItemsPriceTotal(itemCode);

        if (driver.isElementDisplayed(cartSummaryInstantSavingBy)) {
            double savings = commonActions.cleanMonetaryStringToDouble(
                    webDriver.findElement(cartSummaryInstantSavingBy).getText());
            itemSubtotal = commonActions.twoDForm(itemSubtotal - savings, 2);
        }
        String productElement = driver.getParentElement(commonActions.getProductSectionTopElement(itemCode))
                .findElement(cartItemProductSize).getText();
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT) &&
                productElement.contains(ConstantsDtc.WHEEL_ELEMENT)
                || Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT) &&
                productElement.contains(ConstantsDtc.WHEEL_ELEMENT)) {
            if (driver.isElementDisplayed(driver.getElementWithText(feeDetailsItemsRowLabelBy,
                    ConstantsDtc.HUB_CENTRIC_RING))) {
                itemSubtotal = itemSubtotal + extractFeesForItemsOnCart(ConstantsDtc.HUB_CENTRIC_RING, itemCode);
            }
            if (driver.isElementDisplayed(driver.getElementWithText(feeDetailsItemsRowLabelBy,
                    ConstantsDtc.WHEEL_INSTALL_KIT))) {
                itemSubtotal = itemSubtotal + extractFeesForItemsOnCart(ConstantsDtc.WHEEL_INSTALL_KIT, itemCode);
            }
        } else {
            itemSubtotal = itemSubtotal + extractFeesForItemsOnCart(ConstantsDtc.ENVIRONMENTAL_FEE, itemCode)
                    + extractFeesForItemsOnCart(ConstantsDtc.DISPOSAL_FEE, itemCode);
            if (driver.isElementDisplayed(driver.getElementWithText(feeDetailsItemsRowLabelBy,
                    ConstantsDtc.INSTALLATION))) {
                itemSubtotal = itemSubtotal + getPrice(getRowParents(itemCode),
                        ConstantsDtc.INSTALLATION, ConstantsDtc.TOTAL);
            }
        }
        LOGGER.info("calculateItemSubtotal completed");
        return itemSubtotal;
    }

    /**
     * Extract item subtotal on cart page for item
     *
     * @param itemCode product code
     */
    public double extractItemSubtotal(String itemCode) {
        LOGGER.info("extractItemSubtotal started");
        WebElement productParent = driver.getParentElement(commonActions.getProductSectionTopElement(itemCode));
        double itemSubtotal = commonActions.cleanMonetaryStringToDouble(
                productParent.findElement(cartItemSubtotal).getText());
        LOGGER.info("extractItemSubtotal completed");
        return itemSubtotal;
    }

    /**
     * Verify item subtotal on cart page with calculated subtotal for item code
     *
     * @param itemCode product code
     */
    public void assertItemSubtotal(String itemCode) {
        LOGGER.info("assertItemSubtotal started");
        double calculatedItemTotal = calculateItemSubtotal(itemCode);
        double extractedItemTotal = extractItemSubtotal(itemCode);
        Assert.assertTrue("FAIL: Item Subtotal Price didn't match! (displayed:-> "
                        + extractedItemTotal + " expected:->  " + calculatedItemTotal + ")",
                extractedItemTotal == calculatedItemTotal);
        LOGGER.info("assertItemSubtotal completed");
    }

    /**
     * Verify the actual fee with calculated fee amount for wheels
     *
     * @param extractedFee The double fee to compare with the calculated fee
     * @param quantity     Web element quantity
     * @param text         fee type for wheel
     */
    public void assertFeePriceWheel(double extractedFee, WebElement quantity, String text) {
        LOGGER.info("assertFeePriceWheel started for " + text);
        int itemQuantity = 0;
        double fee = 0.00;
        double calculatedFee;
        List<WebElement> items = driver.getParentElement(quantity).findElements(feeDetailsItemsRowParentBy);
        for (WebElement item : items) {
            if (item.findElement(feeDetailsItemsRowLabelBy).getText().contains(text)) {
                itemQuantity = Integer.parseInt(item.findElement(itemRowQuantityBy).getText());
                break;
            }
        }
        if (text.equalsIgnoreCase(ConstantsDtc.WHEEL_INSTALL_KIT)) {
            fee = ConstantsDtc.WHEEL_INSTALL_KIT_FEE;
        } else if (text.equalsIgnoreCase(ConstantsDtc.HUB_CENTRIC_RING)) {
            fee = ConstantsDtc.HUB_CENTRIC_RING_FEE;
        }
        calculatedFee = (fee * itemQuantity);
        Assert.assertTrue("FAIL: Fee Price for" + text + "didn't match! (displayed:-> "
                + extractedFee + " expected:->  " + calculatedFee + ")", extractedFee == calculatedFee);
        LOGGER.info("assertFeePriceWheel completed for " + text);
    }

    /**
     * Calculate the subtotal on cart page for wheels and tires by adding the item subtotal
     */
    public double calculateCartSubtotalForWheelsAndTires() {
        LOGGER.info("calculateCartSubtotalForWheelsAndTires started");
        double CartSubtotal;
        double finalSubtotal = 0.00;
        Set<String> items = cartProductPrice.keySet();
        for (String item : items) {
            CartSubtotal = calculateItemSubtotal(item);
            finalSubtotal = finalSubtotal + CartSubtotal;
        }
        LOGGER.info("calculateCartSubtotalForWheelsAndTires completed");
        return finalSubtotal;
    }

    /**
     * Verify the subtotal on cart page with calculated subtotal for Wheels and Tires
     */
    public void assertCartSubtotalForWheelsAndTires() {
        LOGGER.info("assertCartSubtotalForWheelsAndTires started");
        driver.waitForPageToLoad();
        double expectedSubtotal = calculateCartSubtotalForWheelsAndTires();
        double actualSubTotal = extractCartSubtotal();
        Assert.assertTrue("FAIL: The actual  subtotal for Wheels and Tires on cart page: \"" + actualSubTotal
                + "\" did not contain the expected total: \""
                + expectedSubtotal + "\"!", actualSubTotal == expectedSubtotal);
        LOGGER.info("assertCartSubtotalForWheelsAndTires completed");
    }

    /**
     * Calculate tax for wheels and Tires on Cart Page
     */
    public double calculateTaxForWheelsAndTires() {
        LOGGER.info("calculateTaxForWheelsAndTires started");
        double taxAmount;
        double totalTaxAmount = 0.00;
        Set<String> items = cartProductPrice.keySet();
        for (String item : items) {
            String stateOnCart = (cartPageStoreState.getText().split(",")[1].split("\\s+")[1]).trim();
            String customerType = "default_customer_".concat(stateOnCart.toLowerCase());
            commonActions.setRegionalTaxesFactor(customer.getCustomer(customerType));

            String productElement = driver.getParentElement(commonActions.getProductSectionTopElement(item)).
                    findElement(cartItemProductSize).getText();
            taxAmount = commonActions.getCalculatedSalesTaxForDTCRegion(String.valueOf(extractItemsPriceTotal(item)));

            if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT) &&
                    productElement.contains(ConstantsDtc.WHEEL_ELEMENT)
                    || Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT) &&
                    productElement.contains(ConstantsDtc.WHEEL_ELEMENT)) {
                if (driver.isElementDisplayed(driver.getElementWithText(feeDetailsItemsRowLabelBy,
                        ConstantsDtc.HUB_CENTRIC_RING))) {
                    String hubCentricRingFee = String.valueOf(extractFeesForItemsOnCart(
                            ConstantsDtc.HUB_CENTRIC_RING, item));
                    taxAmount = taxAmount + commonActions.getCalculatedSalesTaxForDTCRegion(hubCentricRingFee);
                }
                if (driver.isElementDisplayed(driver.getElementWithText(feeDetailsItemsRowLabelBy,
                        ConstantsDtc.WHEEL_INSTALL_KIT))) {
                    String wheelInstallKit = String.valueOf((extractFeesForItemsOnCart(
                            ConstantsDtc.WHEEL_INSTALL_KIT, item)));
                    taxAmount = taxAmount + commonActions.getCalculatedSalesTaxForDTCRegion(wheelInstallKit);
                }
            } else {
                String envFee = String.valueOf(extractFeesForItemsOnCart(ConstantsDtc.ENVIRONMENTAL_FEE, item));
                taxAmount = taxAmount + commonActions.getCalculatedSalesTaxForDTCRegion(envFee);
                if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT) ||
                        Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.AT)) {
                    String dispFee = String.valueOf(extractFeesForItemsOnCart(ConstantsDtc.DISPOSAL_FEE, item));
                    taxAmount = taxAmount + commonActions.getCalculatedSalesTaxForDTCRegion(dispFee);
                } else {
                    String optionalFee = String.valueOf(extractPriceForOptionalFee(item, ConstantsDtc.VALVE_STEM));
                    taxAmount = taxAmount + commonActions.getCalculatedSalesTaxForDTCRegion(optionalFee);
                }
                String certFee = String.valueOf(extractPriceForOptionalFee(item, ConstantsDtc.CERTIFICATES));
                if (!certFee.isEmpty()) {
                    taxAmount = taxAmount + commonActions.getCalculatedSalesTaxForDTCRegion(certFee);
                }
                if (driver.isElementDisplayed(cartSummaryInstantSavingBy)) {
                    String promotion = driver.getParentElement(commonActions.getProductSectionTopElement(item)).
                            findElement(cartSummaryInstantSavingBy).getText();
                    taxAmount = taxAmount - commonActions.getCalculatedSalesTaxForDTCRegion(promotion);
                }
            }
            //TODO - Tax calculation needs to be further extended for FET, TPMS, etc Fees and Certificate needs to be tested
            totalTaxAmount = totalTaxAmount + taxAmount;
        }
        LOGGER.info("calculateTaxForWheelsAndTires completed");
        return commonActions.twoDForm(totalTaxAmount, 2);
    }

    /**
     * Verify calculated tax matches with the extracted tax for Tires and Wheels on cart page
     */
    public void assertTaxForWheelsAndTires() {
        LOGGER.info("assertTaxForWheelsAndTires started");
        double estimatedTaxAmount;
        estimatedTaxAmount = calculateTaxForWheelsAndTires();
        double actualTaxAmount = commonActions.extractTaxOnCart();
        Assert.assertTrue("FAIL: The actual  tax amount: {" + actualTaxAmount
                        + ") did not match to the expected tax amount: ("
                        + estimatedTaxAmount + ") for wheels and tires displayed on cart page)",
                estimatedTaxAmount == actualTaxAmount);
        LOGGER.info("assertTaxForWheelsAndTires completed");
    }

    /**
     * Verify the optional fee is displayed
     *
     * @param itemCode The product code
     * @param text     The string to lookup the specific optional fee
     */
    public void assertOptionalFeeDisplay(String itemCode, String text) {
        LOGGER.info("assertOptionalFeeDisplay started for " + text);
        driver.waitForElementVisible(orderFeeServiceItemLabelBy);
        if (itemCode.contains(Constants.NONE)) {
            Assert.assertTrue("FAIL: Optional fee not displayed for "
                    + text, driver.isElementDisplayed(driver.getElementWithText(CommonActions.optionNameBy, text)));
        } else {
            WebElement itemGrandParent = driver.getParentElement(commonActions.getProductSectionTopElement(itemCode));
            List<WebElement> feeForSelectionElements = itemGrandParent.findElements(CommonActions.optionNameBy);
            driver.waitForElementVisible(CommonActions.optionNameBy);
            for (WebElement element : feeForSelectionElements) {
                if (element.getText().contains(text)) {
                    Assert.assertTrue("FAIL: Optional fee not displayed for "
                            + text, element.getText().contains(text));
                    break;
                }
            }
        }
        LOGGER.info("assertOptionalFeeDisplay completed for " + text);
    }

    /**
     * Verify mini cart quick total before product added
     */
    public void assertMiniCartQuickTotalBeforeAddingProduct() {
        LOGGER.info("assertMiniCartQuickTotalBeforeAddingProduct started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(miniCartPrice);
        double miniCartQuickTotal = commonActions.cleanMonetaryStringToDouble(miniCartPrice.getText());
        Assert.assertTrue("FAIL: Minicart price displayed: (" + miniCartQuickTotal + ") not matching to "
                        + Constants.ZERO + "before adding product)",
                miniCartQuickTotal == Constants.ZERO);
        LOGGER.info("assertMiniCartQuickTotalBeforeAddingProduct completed");
    }

    /**
     * Verify mini cart quick total after product added
     */
    public void assertMiniCartQuickTotalAfterAddingProduct() {
        LOGGER.info("assertMiniCartQuickTotalAfterAddingProduct started");
        driver.waitForElementVisible(miniCartPrice);
        double miniCartQuickTotal = commonActions.cleanMonetaryStringToDouble(miniCartPrice.getText());
        Assert.assertTrue("FAIL: Mini Cart price changed to "
                        + miniCartQuickTotal + ") since product added, expected : " +
                        CommonActionsSteps.miniCartQuickTotal,
                miniCartQuickTotal == CommonActionsSteps.miniCartQuickTotal);
        LOGGER.info("assertMiniCartQuickTotalAfterAddingProduct completed");
    }

    /**
     * Verify the View Cart option is displayed in Mini Cart
     */
    public void assertMiniCartDisplayViewCart() {
        LOGGER.info("assertMiniCartDisplayViewCart started");
        WebElement miniCartViewCart = driver.getElementWithText(viewCartButtonBy, VIEW_CART);
        Assert.assertTrue("FAIL: View Cart button not displayed in Mini Cart!",
                driver.isElementDisplayed(miniCartViewCart));
        LOGGER.info("assertMiniCartDisplayViewCart completed");
    }

    /**
     * Verify the Continue Shopping option is displayed in Mini Cart
     */
    public void assertMiniCartDisplayContinueShopping() {
        LOGGER.info("assertMiniCartDisplayContinueShopping started");
        WebElement miniCartContinueShopping = driver.getElementWithText(continueShoppingButtonBy,
                ConstantsDtc.CONTINUE_SHOPPING);
        Assert.assertTrue("FAIL: Continue Shopping button not displayed in Mini Cart!",
                driver.isElementDisplayed(miniCartContinueShopping));
        LOGGER.info("assertMiniCartDisplayContinueShopping completed");
    }

    /**
     * Verify added product is displayed in Mini Cart
     *
     * @param productName added product
     */
    public void assertAddedProductInMiniCart(String productName) {
        LOGGER.info("assertAddedProductInMiniCart started");
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        Assert.assertTrue("FAIL: Product \"" + productName + "\" NOT present on Mini Cart!",
                webDriver.findElement(miniCartItemNameBy).getText().toLowerCase().contains(productName.toLowerCase()));
        LOGGER.info("assertAddedProductInMiniCart completed");
    }

    /**
     * Click View Cart on Mini Cart
     */
    public void clickViewCartMiniCart() {
        LOGGER.info("clickViewCartMiniCart started");
        WebElement miniCartViewCart = driver.getElementWithText(viewCartButtonBy, VIEW_CART);
        driver.waitForElementVisible(miniCartViewCart);
        driver.jsScrollToElementClick(miniCartViewCart);
        driver.waitForPageToLoad();
        LOGGER.info("clickViewCartMiniCart completed");
    }

    /**
     * Calculate Mini Cart fee price total by multiplying base price with quantity
     *
     * @param text     fee type
     * @param itemCode product code
     */
    public double calculateMiniCartPrice(String text, String itemCode) {
        LOGGER.info("calculateMiniCartPrice started for " + text);
        double expectedBasePrice = 0.00;
        double itemQuantity = 0.00;
        if (text.equalsIgnoreCase(ConstantsDtc.INSTALLATION)) {
            expectedBasePrice = getInstallationFeeForSiteRegionAndDataSet();
            itemQuantity = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                    driver.getElementWithText(miniCartItemNameBy, ConstantsDtc.INSTALLATION))
                    .findElement(miniCartItemQuantityBy).getText());
        } else if (text.contains(ConstantsDtc.ENVIRONMENTAL_FEE)) {
            expectedBasePrice = getCalculatedEnvironmentFee(itemCode);
            itemQuantity = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                    driver.getElementWithText(miniCartItemNameBy, ConstantsDtc.ENVIRONMENTAL_FEE))
                    .findElement(miniCartItemQuantityBy).getText());
        } else if (text.equalsIgnoreCase(ConstantsDtc.DISPOSAL_FEE)) {
            expectedBasePrice = getCalculatedDisposalFee(itemCode);
            itemQuantity = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                    driver.getElementWithText(miniCartItemNameBy, ConstantsDtc.DISPOSAL_FEE))
                    .findElement(miniCartItemQuantityBy).getText());
        } else if (webDriver.findElement(miniCartItemNameBy).getText().toLowerCase().contains(text.toLowerCase())) {
            expectedBasePrice = Double.parseDouble(driver.getParentElement(
                    commonActions.getProductSectionTopElement(itemCode)).
                    findElement(itemPriceEachBy).getText().split("\\$")[1]);
            itemQuantity = Double.parseDouble(driver.getParentElement(
                    commonActions.getProductSectionTopElement(itemCode)).
                    findElement(cartItemQuantityBy).getAttribute(Constants.VALUE));
        }
        double calculatedTotalPrice = (expectedBasePrice * itemQuantity);
        LOGGER.info("calculateMiniCartPrice completed for " + text);
        return calculatedTotalPrice;
    }

    /**
     * Extract Mini Cart fee price total
     *
     * @param text fee type
     */
    public double extractMiniCartPrice(String text) {
        LOGGER.info("extractMiniCartPrice started for " + text);
        double extractedPriceTotal = 0.00;
        if (text.equalsIgnoreCase(ConstantsDtc.INSTALLATION)) {
            extractedPriceTotal = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                    driver.getElementWithText(miniCartItemInfoBy,
                            ConstantsDtc.INSTALLATION)).findElement(miniCartItemTotalBy).getText());
        } else if (text.contains(ConstantsDtc.ENVIRONMENTAL_FEE)) {
            extractedPriceTotal = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                    driver.getElementWithText(miniCartItemInfoBy,
                            ConstantsDtc.ENVIRONMENTAL_FEE)).findElement(miniCartItemTotalBy).getText());
        } else if (text.equalsIgnoreCase(ConstantsDtc.DISPOSAL_FEE)) {
            extractedPriceTotal = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                    driver.getElementWithText(miniCartItemInfoBy,
                            ConstantsDtc.DISPOSAL_FEE)).findElement(miniCartItemTotalBy).getText());
        } else if (webDriver.findElement(miniCartItemNameBy).getText().toLowerCase().contains(text.toLowerCase())) {
            extractedPriceTotal = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                    driver.getElementWithText(miniCartItemInfoBy, text)).findElement(miniCartItemTotalBy).getText());
        }
        LOGGER.info("extractMiniCartPrice completed for " + text);
        return extractedPriceTotal;
    }

    /**
     * Verify Mini Cart fees total with calculated total
     *
     * @param text     fee type
     * @param itemCode product code
     */
    public void assertMiniCartPrice(String text, String itemCode) {
        LOGGER.info("assertMiniCartPrice started for " + text);
        double expectedTotal = calculateMiniCartPrice(text, itemCode);
        double extractedTotal = extractMiniCartPrice(text);
        Assert.assertTrue("FAIL: Mini Cart price total for (" + text + ") didn't match! (displayed:-> "
                + extractedTotal + " expected:->  " + expectedTotal + ")", extractedTotal == expectedTotal);
        // Verify the product base price in mini cart
        if (webDriver.findElement(miniCartItemNameBy).getText().toLowerCase().contains(text.toLowerCase())) {
            double miniCartItemBasePrice = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                    driver.getElementWithText(miniCartItemInfoBy, text)).findElement(miniCartItemPriceBy).getText());
            double CartItemBasePrice = Double.parseDouble(
                    driver.getParentElement(commonActions.getProductSectionTopElement(itemCode)).
                            findElement(itemPriceEachBy).getText().split("\\$")[1]);
            Assert.assertTrue("FAIL: Mini Cart item base price for (" + text + ") didn't match! (displayed:-> "
                            + miniCartItemBasePrice + " expected:->  " + CartItemBasePrice + ")",
                    miniCartItemBasePrice == CartItemBasePrice);
        }
        LOGGER.info("assertMiniCartPrice completed for " + text);
    }

    /**
     * Calculate Mini Cart price total by adding product price and fees
     *
     * @param text product
     */
    public double calculateMiniCartTotal(String text) {
        LOGGER.info("calculateMiniCartTotal started");
        double miniCartTotal = 0.00;
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)) {
            double itemTotal = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                    driver.getElementWithText(miniCartItemInfoBy, text)).findElement(miniCartItemTotalBy).getText());
            double installFee = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                    driver.getElementWithText(miniCartItemInfoBy, ConstantsDtc.INSTALLATION)).
                    findElement(miniCartItemTotalBy).getText());
            double disposalTotal = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                    driver.getElementWithText(miniCartItemInfoBy, ConstantsDtc.DISPOSAL_FEE)).
                    findElement(miniCartItemTotalBy).getText());
            double environmentalFee = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                    driver.getElementWithText(miniCartItemInfoBy, ConstantsDtc.ENVIRONMENTAL_FEE)).
                    findElement(miniCartItemTotalBy).getText());
            miniCartTotal = itemTotal + installFee + disposalTotal + environmentalFee;
        } else if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
            double itemTotal = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                    driver.getElementWithText(miniCartItemInfoBy, text)).findElement(miniCartItemTotalBy).getText());
            double environmentalFee = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                    driver.getElementWithText(miniCartItemInfoBy, ConstantsDtc.ENVIRONMENTAL_FEE)).
                    findElement(miniCartItemTotalBy).getText());
            miniCartTotal = itemTotal + environmentalFee;
        }
        LOGGER.info("calculateMiniCartTotal completed");
        return miniCartTotal;
    }

    /**
     * Verify Mini Cart price total with calculated price total
     *
     * @param text product name
     */
    public void assertMiniCartTotal(String text) {
        LOGGER.info("assertMiniCartTotal started");
        double expectedTotal = extractCartSubtotal();
        double extractedTotal = commonActions.cleanMonetaryStringToDouble(
                webDriver.findElement(miniCartTotalBy).getText().split("\n")[1]);
        Assert.assertTrue("FAIL: Mini Cart total didn't match! (displayed:-> "
                + extractedTotal + " expected:->  " + expectedTotal + ")", extractedTotal == expectedTotal);
        LOGGER.info("assertMiniCartTotal completed");
    }

    /**
     * Verify Mini Cart item total
     *
     * @param text     product name
     * @param itemCode product code
     */
    public void assertMiniCartItemTotal(String text, String itemCode) {
        LOGGER.info("assertMiniCartItemTotal started");
        double expectedTotal = commonActions.getTotalItemPrice(itemCode);
        double extractedTotal = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                driver.getElementWithText(miniCartItemInfoBy, text)).findElement(miniCartItemTotalBy).getText());
        Assert.assertTrue("FAIL: Mini Cart total is incorrect. Mini Cart: " + extractedTotal +
                " Shopping Cart Page:  " + expectedTotal, extractedTotal == expectedTotal);
        LOGGER.info("assertMiniCartItemTotal completed");
    }

    /**
     * Verify the 'Checkout Now' button is enabled on the Cart page
     *
     * @param text
     */
    public void assertButtonEnabled(String text) {
        LOGGER.info("assertButtonEnabled started for " + text);
        WebElement element = null;
        driver.waitForPageToLoad();
        if (text.equalsIgnoreCase(ConstantsDtc.CHECKOUT_NOW))
            element = checkoutButton;
        else if (text.contains(ConstantsDtc.PAYPAL)) {
            element = paypalCheckout;
        } else if (text.equalsIgnoreCase(ConstantsDtc.DONE_EDITING))
            element = CommonActions.submitButton;

        driver.waitForElementVisible(element);
        Assert.assertTrue("FAIL: Checkout Now was not enabled on shopping cart page!",
                element.isEnabled());
        LOGGER.info("assertButtonEnabled completed for " + text);
    }

    /**
     * Verify the popup message display (Switching to another store will clear your cart) when Switch Store on Cart Page
     */
    public void assertSwitchStoreOnCartPopupMessage() {
        LOGGER.info("assertSwitchStoreOnCartPopupMessage started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(modalSwitchCartBy);
        Assert.assertTrue("FAIL: Switching store on Cart Page, popup not displaying message! : "
                        + ConstantsDtc.SWITCH_STORE_MESSAGE_ON_CART,
                webDriver.findElement(modalSwitchCartBy).getText().contains(ConstantsDtc.SWITCH_STORE_MESSAGE_ON_CART));
        LOGGER.info("assertSwitchStoreOnCartPopupMessage completed");
    }

    /**
     * Verify the Switch Store On cart page display options (Clear my cart and Continue | View cart)
     */
    public void assertSwitchStoreOptions() {
        LOGGER.info("assertSwitchStoreOptions started");
        String failure = "";
        List<String> optionValues = new ArrayList<String>();

        driver.waitForElementVisible(switchOptions);
        List<WebElement> options = webDriver.findElements(switchOptions);

        for (WebElement option : options) {
            optionValues.add(option.getText());
        }

        if (!optionValues.toString().contains(CLEAR_CART))
            failure = "\"" + CLEAR_CART + "\" option not present. ";

        if (!optionValues.toString().contains(VIEW_CART))
            failure = failure + "\"" + VIEW_CART + "\" option not present.";

        if (failure.length() > 0)
            Assert.fail("FAIL: " + failure);

        LOGGER.info("assertSwitchStoreOptions completed");
    }

    /**
     * Verify the popup message is displayed (Switching to another vehicle will clear your cart)
     */
    public void assertSwitchVehicleOnCartPopupMessage() {
        LOGGER.info("assertSwitchVehicleOnCartPopupMessage started");
        driver.waitForElementVisible(CommonActions.dtModalTitleBy);
        Assert.assertTrue("FAIL: Switching vehicle on Cart Page, popup not displaying message! : " +
                        ConstantsDtc.SWITCH_VEHICLE_POPUP_MESSAGE,
                webDriver.findElement(CommonActions.dtModalTitleBy).getText().contains(ConstantsDtc.SWITCH_VEHICLE_POPUP_MESSAGE));
        LOGGER.info("assertSwitchVehicleOnCartPopupMessage completed");
    }

    /**
     * Verify applicable fee display on Cart page with vehicle
     */
    public void assertFeeDisplayWithVehicle(String itemCode) {
        LOGGER.info("assertFeeDisplayWithVehicle started for " + itemCode);
        if (!Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
            if (commonActions.getProductSectionTopElement(itemCode).findElement(cartItemProductImageBy).
                    getAttribute(ConstantsDtc.ATTR_STYLE).
                    contains(Constants.WHEEL.toLowerCase())) {
                assertFeeLabelDisplayed(ConstantsDtc.WHEEL_INSTALL_KIT, itemCode);
                assertFeeLabelDisplayed(ConstantsDtc.HUB_CENTRIC_RING, itemCode);
            } else {
                assertFeeLabelDisplayed(ConstantsDtc.INSTALLATION, itemCode);
                assertFeeLabelDisplayed(ConstantsDtc.ENVIRONMENTAL_FEE, itemCode);
                assertFeeLabelDisplayed(ConstantsDtc.DISPOSAL_FEE, itemCode);
            }
        } else {
            if (commonActions.getProductSectionTopElement(itemCode).findElement(cartItemProductImageBy).
                    getAttribute(ConstantsDtc.ATTR_STYLE).
                    contains(Constants.WHEEL.toLowerCase())) {
                assertFeeLabelDisplayed(ConstantsDtc.WHEEL_INSTALL_KIT, itemCode);
            } else {
                assertFeeLabelDisplayed(ConstantsDtc.ENVIRONMENTAL_FEE, itemCode);
            }
        }
        LOGGER.info("assertFeeDisplayWithVehicle completed for " + itemCode);
    }

    /**
     * Verify the Order Summary Instant Savings box on the cart page display applied instant discounts
     *
     * @param page Web Page Header
     */
    public void assertInstantSavingOrderSummary(String page) {
        LOGGER.info("assertInstantSavingOrderSummary started for " + page);
        driver.waitForPageToLoad();
        double expectedDiscount = 0.00;
        double instantDiscount;
        List<WebElement> instantSavings = webDriver.findElements(qualifyPromotionAmountBy);
        for (WebElement instantSaving : instantSavings) {
            instantDiscount = Double.parseDouble(instantSaving.getText().split("\\$")[1].trim());
            expectedDiscount = expectedDiscount + instantDiscount;
        }
        double appliedInstantSaving = Double.parseDouble(webDriver.findElement(
                cartSummaryInstantSavingBy).getText().split("\\$")[1].trim());
        Assert.assertTrue("FAIL: 'Instant online Saving applied to Cart' is not accurate ! expected : "
                        + expectedDiscount + "actual : " + appliedInstantSaving,
                appliedInstantSaving == expectedDiscount);
        LOGGER.info("assertInstantSavingOrderSummary completed for " + page);
    }

    /**
     * Verify Instant Savings
     *
     * @param item product code
     * @param page Web Page Header
     */
    public void assertInstantSavingsDisplayedForItem(String page, String item) {
        LOGGER.info("assertInstantSavingsDisplayedForItem started for " + page);
        driver.waitForPageToLoad();
        int itemQuantityByIndex = 0;
        if (Config.isMobile()) {
            itemQuantityByIndex = 1;
        }
        List<WebElement> promotions = driver.getParentElement(
                commonActions.getProductSectionTopElement(item)).findElements(cartItemRebateNameBy);

        Assert.assertTrue("FAIL: Instant Savings not Displayed!",
                !(driver.getElementsWithText(cartItemRebateNameBy, ConstantsDtc.INSTANT_SAVINGS).size() == 0));

        for (WebElement promotion : promotions) {
            driver.jsScrollToElement(promotion);
            if (promotion.getText().contains(ConstantsDtc.INSTANT_SAVINGS)) {
                int quantity;
                double expectedPromotion = 0.00;
                double displayedPromotion = Double.parseDouble(
                        driver.getParentElement(promotion).findElement(
                                qualifyPromotionAmountBy).getText().split("\\$")[1].trim());
                if (promotion.getText().contains(ConstantsDtc.PERCENTAGE)) {
                    double fixedPercentage = Double.parseDouble(promotion.getText().split("\\%")[0].trim());
                    double price = commonActions.cleanMonetaryStringToDouble(driver.getParentElement(
                            commonActions.getProductSectionTopElement(item)).
                            findElement(itemPriceNonStaggeredBy).getText());
                    expectedPromotion = (price * fixedPercentage) / 100;
                } else {
                    if (page.contains(ConstantsDtc.ORDER_CONFIRMATION)) {
                        quantity = Integer.parseInt(driver.getParentElement(
                                commonActions.getProductSectionTopElement(item)).
                                findElements(cartItemQuantityBy).get(itemQuantityByIndex).getText());
                    } else {
                        quantity = Integer.parseInt(driver.getParentElement(
                                commonActions.getProductSectionTopElement(item)).
                                findElements(cartItemQuantityBy).get(itemQuantityByIndex).
                                getAttribute(Constants.VALUE));
                    }
                    double fixedPromotion = Double.parseDouble(promotion.getText().
                            split("\\$")[1].split(" ")[0].trim());
                    expectedPromotion = (fixedPromotion / ConstantsDtc.DEFAULT_QUANTITY) * quantity;
                }
                Assert.assertTrue("FAIL: Instant Savings: " + promotion.getText()
                                + "for item " + item + " not calculated correctly. Displayed : " + displayedPromotion
                                + ". Expected Promotion : " + expectedPromotion,
                        expectedPromotion == displayedPromotion);
            }
        }
        LOGGER.info("assertInstantSavingsDisplayedForItem completed for " + page);
    }

    /**
     * Verify Mail In Rebate displayed
     *
     * @param mailInRebate text on Mail-In-Rebate
     * @param page         Web Page Header
     * @param item         Product code
     */
    public void assertMailInRebateDisplayed(String mailInRebate, String page, String item) {
        LOGGER.info("assertMailInRebateDisplayed started for " + page);
        List<WebElement> promotions = driver.getParentElement(commonActions.getProductSectionTopElement(item)).
                findElements(cartItemRebateNameBy);
        for (int i = 0; i <= promotions.size() - 1; i++) {
            String promotionMessage = promotions.get(i).getText();
            if (promotionMessage.contains(ConstantsDtc.MAIL_IN_REBATE)) {
                Assert.assertTrue("FAIL: Expected Mail-In-Rebate: " + mailInRebate + " not Displayed!, Actual: "
                        + promotionMessage, promotionMessage.contains(mailInRebate));
            }
        }
        if (!page.equalsIgnoreCase(ConstantsDtc.CHECKOUT)) {
            WebElement linkElement = webDriver.findElement(By.partialLinkText(ConstantsDtc.VIEW_DETAILS));
            driver.isElementDisplayed(linkElement);
            Assert.assertTrue("FAIL: Mail In Rebate not displayed with 'view details' Link ! ",
                    linkElement.isDisplayed());
        }
        if (Config.isMobile()) {
            Assert.assertTrue("FAIL: Mail-in rebates not Displayed ! ",
                    !(driver.getElementsWithText(cartItemRebateNameBy, ConstantsDtc.MAIL_IN_REBATE).size() == 0));
        } else {
            Assert.assertTrue("FAIL: Mail-in rebates not Displayed ! ",
                    !(driver.getElementsWithText(cartItemRebateNameBy, ConstantsDtc.MAIL_IN_REBATE,
                            Constants.VALID).size() == 0));
        }
        LOGGER.info("assertMailInRebateDisplayed completed for " + page);
    }

    /**
     * Extract Instant Savings on Cart page
     *
     * @return applied promotions on cart
     */
    public void getInstantSavingsOnCart() {
        LOGGER.info("getInstantSavingsOnCart started");
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        List<WebElement> promotionElements = webDriver.findElements(itemInstantRebateBy);
        for (WebElement promotion : promotionElements) {
            driver.scenarioData.cartInstantPromotionPrice.add(promotion.getText().split("-")[0].trim().concat("   ").
                    concat(promotion.getText().split("\\-\\$")[1].trim()));
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("getInstantSavingsOnCart completed");
    }

    /**
     * Verify provided vehicle name is present on cart page
     *
     * @param vehicle Vehicle title
     */
    public void assertVehicleOnPage(String vehicle, String page) {
        LOGGER.info("assertVehicleOnPage started on " + page);
        driver.waitForPageToLoad();
        WebElement cartFit = webDriver.findElement(cartItemFitBy);
        Assert.assertEquals("FAILED: vehicle fit not displayed on shopping Cart Page!",
                vehicle.toLowerCase(), cartFit.getText().toLowerCase());
        LOGGER.info("assertVehicleOnPage completed on " + page);
    }

    /**
     * Extract Mail In Rebate on Cart page
     *
     * @return applied promotions on cart
     */
    public void getMailInRebateOnCart() {
        LOGGER.info("getMailInRebateOnCart started");
        List<WebElement> promotionElements = driver.getElementsWithText(cartItemRowBy, ConstantsDtc.MAIL_IN_REBATE);
        for (WebElement promotion : promotionElements) {
            driver.scenarioData.cartMailInPromotion.add(promotion.getText().split("-")[0].trim());
        }
        LOGGER.info("getMailInRebateOnCart completed");
    }

    /**
     * Verify MY STORE INVENTORY display for item code on cart page
     *
     * @param item Product Code
     */
    public void assertMyStoreInventoryIsDisplayed(String item) {
        LOGGER.info("assertMyStoreInventoryIsDisplayed started for item code " + item);
        driver.waitForPageToLoad();
        int elementIndex = 0;
        if (Config.isMobile())
            elementIndex = 4;

        WebElement myStoreInventory = commonActions.getProductSectionTopElement(item).findElements(
                CommonActions.headerSixthBy).get(elementIndex);
        driver.jsScrollToElement(myStoreInventory);
        Assert.assertTrue("FAIL: MY STORE INVENTORY section not displayed with expected Text: '"
                + ConstantsDtc.MY_STORE_INVENTORY + "', displayed Text: '" + myStoreInventory
                + "' , for product " + item, myStoreInventory.getText().equals(ConstantsDtc.MY_STORE_INVENTORY));
        LOGGER.info("assertMyStoreInventoryIsDisplayed completed for item code " + item);
    }

    /**
     * Verify check nearby stores link for item code on cart page
     *
     * @param item Product Code
     */
    public void assertCheckNearbyStoreIsDisplayed(String item) {
        LOGGER.info("assertCheckNearbyStoreIsDisplayed started for item code " + item);
        driver.waitForPageToLoad();
        WebElement grandParent = commonActions.getProductSectionTopElement(item);
        driver.jsScrollToElement(grandParent);
        WebElement checkNearbyStoreLink = driver.getDisplayedElement(cartItemStoreInventoryCheckBy, Constants.ZERO);
        Assert.assertTrue("FAIL: check nearby stores link not displayed for product " + item,
                driver.isElementDisplayed(checkNearbyStoreLink));
        LOGGER.info("assertCheckNearbyStoreIsDisplayed completed for item code " + item);
    }

    /**
     * Creates a HashMap containing the item/product code and the current quantity on the Cart page
     *
     * @return HashMap with the item/product code and the current item quantity
     */
    public HashMap<String, String> getCartQuantityByItemCodes() {
        LOGGER.info("getCartQuantityByItemCodes started");
        List<WebElement> itemList = webDriver.findElements(cartItemProductBy);
        for (WebElement item : itemList) {
            itemCodeQuantityMap.put(item.findElement(cartItemCodeNonStaggeredBy).getText().split("#")[1].trim(),
                    item.findElement(cartItemQuantityBy).getAttribute(Constants.VALUE));
        }
        LOGGER.info("getCartQuantityByItemCodes completed");
        return itemCodeQuantityMap;
    }

    /**
     * Performs a search for the specified product
     *
     * @param productCode code to search on
     */
    public void searchForAProduct(String productCode) {
        LOGGER.info("searchForAProduct started");
        driver.clearInputAndSendKeys(searchBox, productCode);
        searchBox.submit();
        LOGGER.info("searchForAProduct completed");
    }

    /**
     * This method will return the string value from the shopping cart page for the matched elementName
     *
     * @param elementName - Name of element on shopping cart page
     * @return - String value from the shopping cart page for the matched element
     */
    private String returnValueFromShoppingCart(String elementName) {
        LOGGER.info("returnValueFromShoppingCart started");
        String returnValue = null;
        switch (elementName) {
            case ConstantsDtc.QUANTITY:
                if (webDriver.findElement(cartItemQuantityBy).getTagName().equalsIgnoreCase(Constants.INPUT))
                    returnValue = webDriver.findElement(cartItemQuantityBy).getAttribute(Constants.VALUE);
                else
                    returnValue = webDriver.findElement(cartItemQuantityBy).getText();
                break;
            case ConstantsDtc.ITEM_PRICE:
                returnValue = webDriver.findElement(itemPriceNonStaggeredBy).getText();
                break;
            case ConstantsDtc.SUBTOTAL:
                returnValue = webDriver.findElement(cartSummaryBreakDownPriceBy).getText();
                break;
            case ConstantsDtc.TAXES:
                returnValue = String.valueOf(commonActions.extractTaxOnCart());
                break;
            case ConstantsDtc.TOTAL:
                returnValue = CommonActions.totalAmount.getText();
                break;
            case ConstantsDtc.INSTALLATION_FEE:
                returnValue = getInstallationPrice();
                break;
            case ConstantsDtc.DISPOSAL_FEE:
                returnValue = getTireDisposalFee();
                break;
            case ConstantsDtc.ENVIRONMENTAL_FEE:
                returnValue = getFeeTotal(ConstantsDtc.ENVIRONMENTAL_FEE);
                break;
            case ConstantsDtc.ENVIRONMENTAL_FEE_REAR:
                returnValue = getFeeTotal(ConstantsDtc.ENVIRONMENTAL_FEE_REAR);
                break;
            case Constants.ADDRESS:
                returnValue = cartPageStoreState.getText();
                break;
            case ConstantsDtc.PRODUCT_BRAND:
                returnValue = webDriver.findElement(CommonActions.brandNameBy).getText();
                break;
            case ConstantsDtc.PRODUCT_NAME:
                returnValue = webDriver.findElement(CommonActions.productNameBy).getText();
                break;
            case ConstantsDtc.PRODUCT_SIZE:
                returnValue = webDriver.findElement(cartItemProductSize).getText();
                break;
            case ConstantsDtc.PRODUCT_NAME_CART:
                if (Config.getSiteRegion().equalsIgnoreCase(Constants.DTD)) {
                    returnValue = webDriver.findElement(CommonActions.itemAddedToCartBy).getText();
                } else {
                    returnValue = webDriver.findElement(CommonActions.modalProductInfoNameBy).getText();
                }
                break;
            default:
                Assert.fail("FAIL: Could not find the matched string value on shopping cart Page!");
        }
        driver.waitForMilliseconds();
        LOGGER.info("returnValueFromShoppingCart completed");
        return returnValue;
    }

    /**
     * Saving the values from shopping cart page in a hash map
     *
     * @param key         - value name from shopping cart page
     * @param elementName - case value name to get the string value from returnValueFromShoppingCart method
     */
    public void saveDataFromShoppingCartPage(String elementName, String key) {
        LOGGER.info("saveDataFromShoppingCartPage started");
        hashMap.put(key, returnValueFromShoppingCart(elementName));
        LOGGER.info("Values Stored are -->  " + key + " = " + hashMap.get(key));
        LOGGER.info("saveDataFromShoppingCartPage completed");
    }

    /**
     * Verify Environment fee and Tax are charged or not charged
     *
     * @param text       Environment Fee/Tax
     * @param applicable charged/Not-Charged
     * @param itemCode   specific Product Code/All products
     * @param page       Shopping Cart/Checkout/Order Confirmation Page
     */
    public void assertIfApplicable(String text, String applicable, String itemCode, String page) {
        LOGGER.info("assertIfApplicable started for " + text + " as " + applicable + " on page " + page);
        double taxValue = 0.00;
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        if (applicable.equalsIgnoreCase(ConstantsDtc.CHARGED) && text.contains(ConstantsDtc.TAX)) {
            if (page.equalsIgnoreCase(ConstantsDtc.CHECKOUT)) {
                taxValue = commonActions.cleanMonetaryStringToDouble
                        (CheckoutPage.checkoutAndOrderCartSummarySalesTax.getText());
            } else {
                taxValue = commonActions.extractTaxOnCart();
            }
            Assert.assertTrue("FAIL: Expected " + text + " displayed zero, on " + page, !(taxValue == 0.00));
        } else if (applicable.equalsIgnoreCase(ConstantsDtc.CHARGED) && !text.contains(ConstantsDtc.TAX)) {
            if (itemCode.equalsIgnoreCase(ConstantsDtc.ALL)) {
                Assert.assertTrue("FAIL: Expected " + text + "not displayed!, on " + page,
                        driver.isTextPresentInPageSource(getFeeTotal(text)));
            } else {
                assertFeesForItemOnCart(text, itemCode);
            }
        } else if (applicable.equalsIgnoreCase(ConstantsDtc.NOT_CHARGED) && !text.contains(ConstantsDtc.TAX)) {
            if (itemCode.equalsIgnoreCase(ConstantsDtc.ALL)) {
                Assert.assertTrue("FAIL: Expected " + text + " is displayed!, on " + page,
                        driver.getElementsWithText(CartPage.childTip, text).size() == 0);
            } else {
                Assert.assertTrue("FAIL: Expected " + text + " is displayed for item " + itemCode + ", on "
                        + page, !commonActions.getProductSectionTopElement(itemCode).getText()
                        .contains(text));
            }
        } else if (applicable.equalsIgnoreCase(ConstantsDtc.NOT_CHARGED) && text.contains(ConstantsDtc.TAX)) {
            Assert.assertTrue("FAIL: Expected " + text + "not displayed zero, on " + page,
                    taxValue == 0.00);
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("assertIfApplicable completed for " + text + " as " + applicable + " on page " + page);
    }

    /**
     * Extracts the mandatory applied Fee Total(State Required)
     *
     * @param feeText fee type text displayed on web page
     * @return String
     */
    public String getFeeTotal(String feeText) {
        return this.getSpecialPricingOnCartPage(feeDetailsItemsRowParentBy, feeDetailsItemsRowLabelBy,
                feeDetailsItemsRowPriceBy, feeText);
    }

    /**
     * Extracts the mandatory applied Fee for each item(State Required)
     *
     * @param feeText fee type text displayed on web page
     * @return String
     */
    public String getFeePerItem(String feeText) {
        return this.getSpecialPricingOnCartPage(feeDetailsItemsRowParentBy, feeDetailsItemsRowLabelBy,
                serviceFeeItemRowPriceBy, feeText);
    }

    /**
     * Verify the displayed Oversized Package Fee is correct
     */
    public void assertOversizedFee() {
        LOGGER.info("assertOversizedFee started");
        double actualOvrFee = Double.parseDouble(getOversizedPackageFee());
        int quantityValue = Integer.parseInt(webDriver.findElement(cartItemQuantityBy).getAttribute(Constants.VALUE));
        double expectedOvrFee = customer.default_OverSizeFee * quantityValue;
        Assert.assertTrue("FAIL: OverSized Fee Didn't Match! (displayed:-> " + actualOvrFee + " expected:-> "
                + expectedOvrFee + ")", actualOvrFee == expectedOvrFee);
        LOGGER.info("assertOversizedFee completed");
    }

    /**
     * Selects the 'Paypal checkout' button on the Cart page
     */
    public void selectPaypalCheckout() {
        LOGGER.info("selectPaypalCheckout started");
        driver.waitForPageToLoad();
        driver.waitForElementClickable(paypalCheckout);
        paypalCheckout.click();
        LOGGER.info("selectPaypalCheckout completed");
    }

    /**
     * Verify the quantity in certificate cart is read only
     */
    public void assertReadOnlyQty() {
        LOGGER.info("assertReadOnlyQty started");
        String actualQuantityTagName = webDriver.findElement(cartItemQuantityBy).getTagName();
        Assert.assertNotEquals("FAIL: The actual quantity tag name is '" + actualQuantityTagName + "'",
                Constants.INPUT, actualQuantityTagName);
        LOGGER.info("assertReadOnlyQty completed");
    }

    /**
     * Verifies the itemSubtotal = itemTotal in certificates only cart
     *
     * @param status   Displayed or not displayed
     * @param itemCode The product to verify subtotal
     */
    public void assertItemSubtotalOnCertificatesCartPage(String status, String itemCode) {
        LOGGER.info("assertItemSubtotalOnCertificatesCartPage started for product: " + itemCode);
        int index = 0;
        if (Config.isMobile()) {
            index = 1;
        }
        driver.waitForElementVisible(orderList);
        WebElement productParent = driver.getParentElement(commonActions.getProductSectionTopElement(itemCode));
        if (status.equalsIgnoreCase(Constants.DISPLAYED)) {
            String itemTotal = productParent.findElements(itemPriceNonStaggeredBy).get(index).getText();
            String itemSubtotal = productParent.findElement(cartItemSubtotal).getText();
            Assert.assertTrue("FAIL: The actual item subtotal: \"" + itemSubtotal
                            + "\" doesn't match the expected item subtotal: \"" + itemTotal + "\"",
                    itemSubtotal.equalsIgnoreCase(itemTotal));
        } else {
            boolean labelFound;
            boolean valueFound;
            try {
                productParent.findElement(cartItemSubtotalLabelBy);
                labelFound = true;
            } catch (NoSuchElementException e) {
                labelFound = false;
            }
            try {
                productParent.findElement(cartItemSubtotal);
                valueFound = true;
            } catch (NoSuchElementException e) {
                valueFound = false;
            }
            Assert.assertFalse("FAIL: Item subtotal label is displayed. Expected not to be displayed here.",
                    labelFound);
            Assert.assertFalse("FAIL: Item subtotal value is displayed. Expected not to be displayed here.",
                    valueFound);
        }
        LOGGER.info("assertItemSubtotalOnCertificatesCartPage completed for product: " + itemCode);
    }

    /**
     * Click the learn more link after certificates cart error message
     */
    public void clickLearnMoreLink() {
        LOGGER.info("clickLearnMoreLink started");
        driver.waitForElementClickable(learnMoreLink);
        learnMoreLink.click();
        driver.waitForPageToLoad();
        LOGGER.info("clickLearnMoreLink completed");
    }

    /**
     * Get the Installation Fee based on the site region and the environment (data set)
     *
     * @return the Installation fee
     */
    private double getInstallationFeeForSiteRegionAndDataSet() {
        LOGGER.info("getInstallationFeeForSiteRegionAndDataSet started");
        double fee;
        if (Config.getSiteRegion().equalsIgnoreCase(Constants.AT) ||
                commonActions.getCurrentUrl().contains(ConstantsDtc.AMERICAS_TIRE)) {
            if (Config.getDataSet().equalsIgnoreCase(Constants.QA))
                fee = ConstantsDtc.INSTALLATION_FEE_QA_AT_CA_CAL_01;
            else
                fee = ConstantsDtc.INSTALLATION_FEE_STG_AT_CAN_19;
        } else {
            if (Config.getDataSet().equalsIgnoreCase(Constants.QA))
                fee = ConstantsDtc.INSTALLATION_FEE_QA_DT_AZF_01;
            else
                fee = ConstantsDtc.INSTALLATION_FEE_STG_DT_AZP_20;
        }
        LOGGER.info("getInstallationFeeForSiteRegionAndDataSet completed");
        return fee;
    }

    /**
     * Method click on element
     *
     * @param elementName - Name of the Web Element to be clicked
     */
    public void clickOnElementInCartPage(String elementName) {
        LOGGER.info("clickOnElementInCartPage started for " + elementName);
        returnElement(elementName).click();
        LOGGER.info("clickOnElementInCartPage completed for " + elementName);
    }

    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started for " + elementName);
        switch (elementName) {
            case CLEAR_CART_CONFIRMATION:
                return clearCartConfirmation;
            case KEEP_CART:
                return keepCart;
            case WHY_BUY_CERTIFICATES:
                return buyCertificatesLink;
            case CERTIFICATES_LINK_NEW_MODAL:
                return certificatesNewModal;
            default:
                Assert.fail("FAIL: Could not find element that matched string passed from step");
                return null;
        }
    }

    /**
     * Verifies given text with element text
     *
     * @param elementName - Name of the Web Element
     * @param text        - text which should be validated with element text
     */
    public void verifyElementTextInCartPage(String elementName, String text) {
        LOGGER.info("verifyElementTextInCartPage started for " + elementName);
        Assert.assertTrue("FAIL: Given text is not to " + returnElement(elementName).getText(), returnElement(elementName).getText().equalsIgnoreCase(text));
        LOGGER.info("verifyElementTextInCartPage completed for " + elementName);
    }

    /**
     * Verifies the size specifies on the view tire or view wheel button on empty cart page
     *
     * @param sizeOption size specified
     * @param type       Tire or Wheel
     */
    public void assertViewButtonsOnEmptyCartPage(String sizeOption, String type) {
        LOGGER.info("assertViewButtonsOnEmptyCartPage started");
        boolean buttonFound = false;
        List<WebElement> buttonsOnEmptyCartPage = webDriver.findElements(viewTireOrWheelsButtonBy);
        for (int i = 0; i < buttonsOnEmptyCartPage.size(); i++) {
            String buttonText = buttonsOnEmptyCartPage.get(i).getText();
            buttonFound = buttonText.equalsIgnoreCase("View - " + sizeOption + " " + type);
            if (buttonFound) {
                break;
            }
        }
        if (!buttonFound)
            Assert.fail("FAIL: The button with text " + sizeOption + " was not found was found");
        LOGGER.info("assertViewButtonsOnEmptyCartPage completed");
    }

    /**
     * Verify the 'required fees' and 'add-ons' sections are expanded on Shopping Cart page
     */
    public void assertRequiredFeesAndAddonsSectionsAreExpandedOnShoppingCart() {
        LOGGER.info("assertRequiredFeesAndAddonsSectionsAreExpandedOnShoppingCart started");
        driver.waitForPageToLoad();
        // TODO: retest when new safaridriver is stable
        if (Config.isSafari())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        List<WebElement> links = webDriver.findElements(CommonActions.dtLinkBy);
        for (WebElement link : links) {
            Assert.assertFalse("FAIL: All " + ConstantsDtc.SHOW_REQUIRED_FEES +
                            " link should be expanded when landing on Shopping Cart page",
                    CommonUtils.containsIgnoreCase(link.getText(), ConstantsDtc.SHOW_REQUIRED_FEES));
            Assert.assertFalse("FAIL: All " + ConstantsDtc.SHOW_ADD_ONS +
                            " link should be expanded when landing on Shopping Cart page",
                    CommonUtils.containsIgnoreCase(link.getText(), ConstantsDtc.SHOW_ADD_ONS));
        }
        LOGGER.info("assertRequiredFeesAndAddonsSectionsAreExpandedOnShoppingCart completed");
    }

    /**
     * Verify the font color of the dollar value for Instant Savings if it is displayed
     */
    public void assertInstantSavingsColor() {
        LOGGER.info("assertInstantSavingsColor started");
        if (driver.isElementDisplayed(qualifyPromotionAmountBy, Constants.ONE)) {
            List<WebElement> promotionalLineItems = webDriver.findElements(qualifyPromotionAmountBy);

            for (WebElement promotionalLineItem : promotionalLineItems) {
                String actualColor = promotionalLineItem.getCssValue(Constants.COLOR);

                Assert.assertTrue("FAIL: The Instant Savings was not green color",
                        (actualColor.equalsIgnoreCase(Constants.GREEN_BANNER_RGB) ||
                                actualColor.equalsIgnoreCase(Constants.GREEN_BANNER_RGBA)));
            }
        }
        if (driver.isElementDisplayed(cartSummaryInstantSavingBy, Constants.ONE)) {
            String actualColor = webDriver.findElement(cartSummaryInstantSavingBy).getCssValue(Constants.COLOR);

            Assert.assertTrue("FAIL: The Instant Savings was not green color",
                    (actualColor.equalsIgnoreCase(Constants.GREEN_BANNER_RGB) ||
                            actualColor.equalsIgnoreCase(Constants.GREEN_BANNER_RGBA)));
        }
        LOGGER.info("assertInstantSavingsColor completed");
    }

    /**
     * Write Fee data on shopping cart page to scenario data
     */
    public void setFeeScenarioData() {
        LOGGER.info("setFeeScenarioData started");
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        List<WebElement> collapsibleSections = webDriver.findElements(CommonActions.openCollapsibleSectionBy);
        for (WebElement collapsibleSection : collapsibleSections) {
            List<WebElement> feeItems = collapsibleSection.findElements(feeDetailsItemsRowParentBy);
            for (int i = 1; i < feeItems.size(); i++) {
                String feeLabel = feeItems.get(i).findElement(feeDetailsItemsRowLabelBy).getText().split(" ")[0].trim();
                String feeProductCode = returnSubProductCode(feeLabel);
                // Defect OEES-400 has the column headers for ADD-ONS is doubled. So this will fail unless we account
                // for an empty product code. When the issue is resolved, we can remove this if statement
                if (!feeProductCode.isEmpty()) {
                    String feeQuantity = feeItems.get(i).findElement(itemRowQuantityBy).getText();
                    String feeRetailPrice = Double.valueOf(feeItems.get(i).findElement(serviceFeeItemRowPriceBy).getText().
                            replaceAll("[^0-9.]+", "")).toString();
                    String feeTotalPrice = Double.valueOf(feeItems.get(i).findElement(feeDetailsItemsRowPriceBy).getText().
                            replaceAll("[^0-9.]+", "")).toString();
                    String feeLineItem = OrderXmls.PRODUCTS + ":" + feeProductCode + "," + Constants.JSON_PARAM_QUANTITY
                            + ":" + feeQuantity + "," + OrderXmls.RETAIL_PRICE + ":" + feeRetailPrice + ","
                            + OrderXmls.SALES_EXTENDED + ":" + feeTotalPrice;
                    driver.scenarioData.genericData.put(feeProductCode, feeLineItem);
                }
            }
            List<WebElement> feeLabels = webDriver.findElements(CommonActions.optionNameBy);
            for (int i = 0; i < feeLabels.size(); i++) {
                String feeLabel = feeLabels.get(i).getText().split(" ")[0].trim();
                String feeProductCode = returnSubProductCode(feeLabel);
                if (driver.scenarioData.genericData.get(feeProductCode) != null) {
                    continue;
                }
                // Defect OEES-400 has the column headers for ADD-ONS is doubled. So this will fail unless we account
                // for an empty product code. When the issue is resolved, we can remove this if statement
                if (!feeProductCode.isEmpty()) {
                    String feeQuantity = "";
                    int counter = 0;
                    WebElement feeQuantityElement = null;
                    WebElement parentElement = feeLabels.get(i);
                    do {
                        try {
                            driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
                            feeQuantityElement = parentElement.findElement(itemRowQuantityBy);
                            feeQuantity = feeQuantityElement.getText();
                            driver.resetImplicitWaitToDefault();
                        } catch (Exception e) {
                            parentElement = driver.getParentElement(parentElement);
                            counter++;
                        }
                    } while (feeQuantityElement == null && counter < Constants.TEN);
                    Assert.assertNotNull("FAIL: Could not find the row data for '" + feeLabels.get(i) +
                            "' on cart page", feeQuantityElement);
                    String feeRetailPrice = Double.valueOf(parentElement.findElement(serviceFeeItemRowPriceBy).getText().
                            replaceAll("[^0-9.]+", "")).toString();
                    String feeTotalPrice = Double.valueOf(parentElement.findElement(feeDetailsItemsRowPriceBy).getText().
                            replaceAll("[^0-9.]+", "")).toString();
                    String feeLineItem = OrderXmls.PRODUCTS + ":" + feeProductCode + "," + Constants.JSON_PARAM_QUANTITY
                            + ":" + feeQuantity + "," + OrderXmls.RETAIL_PRICE + ":" + feeRetailPrice + ","
                            + OrderXmls.SALES_EXTENDED + ":" + feeTotalPrice;
                    driver.scenarioData.genericData.put(feeProductCode, feeLineItem);
                }
            }
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("setFeeScenarioData completed");
    }

    /**
     * Return sub item Product Code for product label
     *
     * @param feeLabel product label
     */
    public String returnSubProductCode(String feeLabel) {
        LOGGER.info("returnSubProductCode started for " + feeLabel);
        String productCode = null;
        switch (feeLabel) {
            case ConstantsDtc.INSTALLATION:
                productCode = Constants.INSTALLATION_PRODUCT_CODE;
                break;
            case ConstantsDtc.ENVIRONMENTAL:
                productCode = Constants.ENVIRONMENTAL_FEE_PRODUCT_CODE;
                break;
            case ConstantsDtc.DISPOSAL:
                productCode = Constants.DISPOSAL_FEE_PRODUCT_CODE;
                break;
            case ConstantsDtc.CERTIFICATES:
                productCode = Constants.CERTIFICATES_PRODUCT_CODE;
                break;
            case ConstantsDtc.HUB:
                productCode = ConstantsDtc.HUB_CENTRIC_RING;
                break;
            case Constants.WHEEL:
                productCode = ConstantsDtc.WHEEL_INSTALL_KIT;
                break;
            default:
                productCode = "";
        }
        LOGGER.info("returnSubProductCode completed for " + feeLabel + ". Product code = " + productCode);
        return productCode;
    }

    /**
     * Verifies Tire and wheel packages in cart block
     *
     * @param text : Text which has to verified in cart block
     */
    public void verifyTireWheelPackageTextInCartPage(String text) {
        LOGGER.info("verifyTireWheelPackageTextInCartPage started");
        boolean packageBlock = false;
        List<WebElement> cartBlocks = webDriver.findElements(cartItemDetailsParentBy);
        for (WebElement cartBlock : cartBlocks) {
            if (cartBlock.getText().contains(TIRE_WHEEL_PACKAGES)) {
                packageBlock = true;
                Assert.assertTrue("FAIL: Value is not present in cart page", cartBlock.getText().contains(text));
                break;
            }
        }
        if (!packageBlock) {
            Assert.fail("FAIL: Wheel & Tire package block in not present in cart page");
        }
        LOGGER.info("verifyTireWheelPackageTextInCartPage completed");
    }

    /**
     * Method returns element from cart page
     *
     * @param elementName        : Name of the element in package block
     * @param packageOrderNumber : Package number in shopping cart page.
     */
    public WebElement returnElementFromCartPage(String elementName, int packageOrderNumber) {
        LOGGER.info("returnElementFromCartPage started");
        WebElement element = webDriver.findElements(tireAndWheelPackagesListBy).get(packageOrderNumber - 1);
        switch (elementName) {
            case REMOVE_TIRES_LINK:
                return element.findElement(removeTireslinkBy);
            case REMOVE_WHEELS_LINK:
                return element.findElement(removeWheelsLinkBy);
            case CHANGE_TIRES_LINK:
                return element.findElement(changeTiresLinkBy);
            case CHANGE_WHEELS_LINK:
                return element.findElement(changeWheelsLinkBy);
            case DELETE_PACKAGE_LINK:
                return element.findElement(deleteLinkBy);
            default:
                return webDriver.findElement(By.xpath(XPATH_CONTAINS_TEXT + elementName + "')]"));
        }
    }

    /**
     * Clicks on the wheels and packages elements in shopping cart page based on package number
     *
     * @param elementName        : Name of the element in package block
     * @param packageOrderNumber : Package number in shopping cart page.
     */
    public void clickOnTireAndWheelPackagesByOrderNumber(String elementName, int packageOrderNumber) {
        LOGGER.info("clickOnTireAndWheelPackagesByOrderNumber started");
        WebElement webElement = returnElementFromCartPage(elementName, packageOrderNumber);
        Assert.assertNotNull("FAIL: The '" + elementName + "' web element is not valid or displayed", webElement);
        driver.webElementClick(webElement);
        LOGGER.info("clickOnTireAndWheelPackagesByOrderNumber completed");
    }

    /**
     * Verifies package item details based on row number in cart page
     *
     * @param keyName            : Name of the element in package block
     * @param choice             : Can be "present" or "not present"
     * @param packageOrderNumber : Package number in shopping cart page.
     */
    public void verifyPackageDetailsBasedOnOrderPosition(String keyName, String choice, int packageOrderNumber) {
        LOGGER.info("verifyPackageDetailsBasedOnOrderPosition started");
        WebElement element;
        try {
            element = webDriver.findElements(cartItemsListPackageBy).get(packageOrderNumber - 1);
        } catch (StaleElementReferenceException e) {
            element = webDriver.findElements(cartItemsListPackageBy).get(packageOrderNumber - 1);
        }
        String values[] = tireWheelPackageDetails.get(keyName);
        for (String value : values) {
            if (choice.equalsIgnoreCase(PRESENT)) {
                driver.verifyTextDisplayed(element, value);
            } else if (choice.equalsIgnoreCase(NOT_PRESENT)) {
                driver.verifyTextNotDisplayed(element, value);
            }
        }
        LOGGER.info("verifyPackageDetailsBasedOnOrderPosition completed");
    }
}