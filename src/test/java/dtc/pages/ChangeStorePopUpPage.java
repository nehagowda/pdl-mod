package dtc.pages;

import common.Constants;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.logging.Logger;

/**
 * Created by nkumar on 7/10/19.
 */

public class ChangeStorePopUpPage {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(ChangeStorePopUpPage.class.getName());

    public ChangeStorePopUpPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(css = "[class*='store-change-modal'] [class*='dt-button-lg']")
    public static WebElement continueButton;

    private static By makeMyStoreBy = By.cssSelector("[class*='nearby-stores__make-my-store-button']");

    private static By storeAddressFromPopupBy = By.cssSelector("[class*='nearby-stores__store-data']");

    private static final String MAKE_MY_STORE = "Make my store";
    private static final String NEW_STORE_ADDRESS = "New store address";
    private static final String CURRENT_STORE_ADDRESS = "current store address";
    private static final String CONTINUE_BUTTON = "Continue button";

    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnByElement started");
        switch (elementName) {
            case MAKE_MY_STORE:
                return webDriver.findElements(makeMyStoreBy).get(0);
            case NEW_STORE_ADDRESS:
                return webDriver.findElements(storeAddressFromPopupBy).get(1);
            case CURRENT_STORE_ADDRESS:
                return webDriver.findElements(storeAddressFromPopupBy).get(0);
            case CONTINUE_BUTTON:
                return continueButton;
            default:
                Assert.fail("FAIL: Could not find By objects that matched string passed from step");
                return null;
        }
    }

    /**
     * Method clicks on element in Change store pop up
     *
     * @param elementName - Name of the Web Element
     */
    public void clickOnElement(String elementName) {
        LOGGER.info("clickOnElement started for " + elementName);
        returnElement(elementName).click();
        LOGGER.info("clickOnElement completed for " + elementName);
    }

    /**
     * Method saves new store address
     */
    public void saveStoreAddress() {
        LOGGER.info("saveStoreAddress started");
        String newAddress = returnElement(NEW_STORE_ADDRESS).getText().split("\n")[0].trim()
                + "\n" +  returnElement(NEW_STORE_ADDRESS).getText().split("\n")[1];
        driver.scenarioData.genericData.put(NEW_STORE_ADDRESS, newAddress);
        LOGGER.info("saveStoreAddress completed");
    }

    /**
     * Verify current store details in store popup
     */
    public void verifyStoreDetails() {
        LOGGER.info("verifyStoreDetails started");
        String currentAddress = returnElement(CURRENT_STORE_ADDRESS).getText().split("\n")[0].trim()
                + "\n" +  returnElement(CURRENT_STORE_ADDRESS).getText().split("\n")[1];
        Assert.assertTrue("Fail: Address is not equal",
                driver.scenarioData.genericData.get(Constants.STORE_DETAILS).contains(currentAddress));
        LOGGER.info("verifyStoreDetails completed");
    }

    /**
     * Saves new store address data and clicks on continue.
     */
    public void saveStoreDetailsAndClickContinue() {
        LOGGER.info("saveStoreDetailsAndClickContinue started");
        driver.scenarioData.genericData.put(Constants.STORE_DETAILS, driver.scenarioData.genericData.get(NEW_STORE_ADDRESS));
        returnElement(CONTINUE_BUTTON).click();
        LOGGER.info("saveStoreDetailsAndClickContinue started");
    }
}
