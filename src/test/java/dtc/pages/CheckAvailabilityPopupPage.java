package dtc.pages;

import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import dtc.steps.CommonActionsSteps;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.CommonUtils;
import utilities.Driver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by aaronbriel on 1/11/17.
 */
public class CheckAvailabilityPopupPage {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(FitmentPopupPage.class.getName());

    public CheckAvailabilityPopupPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(css = "[class*='store-quick-view__product-availability']")
    public static WebElement productAvailability;

    @FindBy(id = "productAddToCartDiv")
    public static WebElement productAddToCart;

    @FindBy(className = "message-stock")
    public static WebElement stockInfo;

    @FindBy(className = "icon-close")
    public static WebElement iconClose;

    @FindBy(className = "check-inventory__header-close")
    public static WebElement checkInventoryHeaderClose;

    @FindBy(className = "product-availability__headline")
    public static WebElement productAvailabilityHeadline;

    @FindBy(id = "checkinventory_locationForSearch")
    public static WebElement zipWeb;

    @FindBy(className = "form-group__text")
    public static WebElement zipInventory;

    @FindBy(id = "search-zip-input")
    public static WebElement zipMobile;

    @FindBy(className = "product-availability__search-go")
    public static WebElement zipGoButton;

    @FindBy(className = "product-availability__zip-button")
    public static WebElement zipButton;

    @FindBy(className = "product-availability__store-city")
    public static WebElement storeCity;

    @FindBy(className = "jsInStock")
    public static WebElement inStockFilter;

    @FindBy(className = "jsOrMore")
    public static WebElement orMoreFilter;

    @FindBy(className = "check-inventory__show-stock-label")
    public static WebElement onlyShowInStocklabel;

    @FindBy(id = "object-object")
    public static WebElement showStoresNearSearchBar;

    @FindBy(css = "[class*='pagination-message__sort'] [class*='form-group'] [class*='simple-select form-group__select']")
    public static WebElement checkAvailabilityPopup;

    @FindBy(className = "check-inventory__search-button")
    public static WebElement showStoresNearGoBtn;

    private static final By storeAddressBy = By.cssSelector("[class*='store-quick-view-list__list-item'] [class*='store-quick-view__address']");

    private static final By myStoreAddressBy = By.cssSelector("[class*='selected-store__store-info'] [class*='store-quick-view__address']");

    private static final By checkInventoryLocationAddressBy = By.cssSelector("[class*='selected-store__store-info'] " +
            "[class*='store-quick-view__address']");

    private static final By nearestStoreBy = By.cssSelector("[class*='store-locator-page__nearby-stores'] " +
            "[class*='quick-view__store-info']");

    private static final By myStoreEle = By.cssSelector("[class*='selected-store'] [class*='store-quick-view']");

    private static final By quantityErrorBy = By.className("product-availability__error--qty");

    private static final By productInfoBy = By.className("product-availability__product-info");

    public static final By productCheckFitInfoBy = By.xpath("//*[contains(@class, 'modal-product-info__body')]");

    public static final By productImageBy = By.className("product-availability__product-image");

    private static final By addToCartBy = By.className("product-availability__product-add-cart");

    private static final By filterQuantityBy = By.className("js-qtyCount");

    private static final By zipcodeErrorBy = By.className("product-availability__error--zip");

    private static final By priceBy = By.className("product-availability__product-price");

    private static final By loadingOverlayBy = By.className("product-availability__loading-overlay");

    private static final By stockQuantityBy = By.className("auto-message-stock");

    public static final By store = By.className("product-availability__store");

    public static final By storeMakeThisMyStoreButtonBy = By.className("js-make-my-store");

    private static final By currentStoreBy = By.className("product-availability__store--selected");

    private static final By makeMyStoreBy = By.xpath("//button[normalize-space()='Make My Store' and contains(@style,'block;')]");

    private static final By checkInventoryContentBy = By.className("check-inventory__content");

    private static final By stockCountTextBy = By.className("cart-item__store-stock-count-standalone");

    private static final By itemSpecsBy = By.className("check-inventory__item-specs");

    private static final By itemQuantityAmountBy = By.className("check-inventory__item-qty-amt");

    private static final By inventoryLocationsRowBy = By.className("check-inventory__row--locations");

    private static final By inventoryStoreCellsBy = By.className("check-inventory__cell--store");

    private static final By inventoryStoreMakeMyStoreBtnBy = By.className("check-inventory__make-my-store");

    private static final By inventoryStorePhoneNumberBy =
            By.xpath("//span[@class='check-inventory__location-phone display-sm']");

    private static final By storeInventoryStockBy = By.cssSelector("[class*='list'] [class*='store-quick-view__stock']");

    private static final By storeDistanceBy = By.cssSelector("[class*='list'] [class*='store-quick-view__distance']");

    private static final String DISPLAY_STATUS = "display: block;";
    private static final String MESSAGE_STOCK = "message-stock";
    private static final String QUANTITY = "quantity";
    private static final String VALIDITY = "validity";
    private static final String QUANTITY_ERROR = "Please enter a qty under 100";
    private static final String VALIDITY_ERROR = "Please enter a number";
    private static final String ZIPCODE_ERROR = "Enter a 5 digit postal code";
    private static final String IN_STOCK = "In stock";
    private static final String DATA_FRONT_PRODUCT_CODE = "data-front-product";
    private static final String DATA_REAR_PRODUCT_CODE = "data-rear-product";
    private static final String CELL_SELECTED = "cell--selected";

    public static String makeMyStorePhoneNumber;

    /**
     * Confirms the display of the Success pop-up message after sending a store location to a phone via the
     * 'Send to Phone' pop-up
     */
    public void assertCheckAvailabilityPopupLoaded() {
        LOGGER.info("assertCheckAvailabilityPopupLoaded started");
        driver.waitForPageToLoad();
        Assert.assertTrue("FAIL: Check Availability popup did not load.",
                driver.isElementDisplayed(CommonActions.dtModalContainer));
        LOGGER.info("assertCheckAvailabilityPopupLoaded completed");
    }

    /**
     * Confirms the InStock Label is not present on Check Availability page
     */
    public void assertInStockLabelNotPresent() {
        LOGGER.info("assertInStockLabelNotPresent started");
        driver.waitForElementVisible(stockInfo);
        if (stockInfo.getText().equals(ConstantsDtc.IN_STOCK)) {
            Assert.fail("In Stock label is present on Check Availability popup ");
        }
        LOGGER.info("assertInStockLabelNotPresent completed");
    }

    /**
     * Close Check Availability Page Popup
     */
    public void closeCheckAvailabilityPopupModal() {
        LOGGER.info("closeCheckAvailabilityPopupModal started");
        driver.waitForElementVisible(iconClose);
        iconClose.click();
        driver.waitForMilliseconds(Constants.TWO);
        driver.waitForElementNotVisible(CommonActions.closeButtonBy);
        LOGGER.info("closeCheckAvailabilityPopupModal completed");
    }

    /**
     * Confirms the Default Store My Store label is positioned at the top
     */
    public void assertDefaultStoreMyStoreLabelIsTopInTheList() {
        LOGGER.info("assertDefaultStoreMyStoreLabelIsTopInTheList started");
        driver.waitForElementVisible(myStoreEle);

        List<WebElement> myStoreRows = webDriver.findElements(myStoreEle);

        if (webDriver.findElements(storeAddressBy).get(0).getText().toLowerCase().contains(Config.getDefaultStoreCity())) {
            Assert.assertTrue("FAIL: MY STORE Label is not positioned at the top in listed stores list",
                    myStoreRows.get(0).getAttribute(ConstantsDtc.ATTR_STYLE).equalsIgnoreCase(DISPLAY_STATUS));
        } else
            Assert.fail("Selected Default Store " + Config.getDefaultStoreCity() + " is not the top store in the list ");
        LOGGER.info("assertDefaultStoreMyStoreLabelIsTopInTheList completed");
    }

    /**
     * Confirms the Special Order Label is present on Check Availability page
     *
     * @param expectedHeadline Expected headline
     */
    public void assertCheckAvailabilityHeadline(String expectedHeadline) {
        LOGGER.info("assertCheckAvailabilityHeadline started");
        String actualHeadline;
        driver.waitForElementVisible(productAvailabilityHeadline);
        actualHeadline = productAvailabilityHeadline.getText();
        Assert.assertEquals("FAIL: Expected headline: \"" + expectedHeadline + "\", Actual title: \"" + actualHeadline + "\"",
                expectedHeadline.toLowerCase(), actualHeadline.toLowerCase());
        LOGGER.info("Confirmed that the page title is \"" + expectedHeadline + "\".");
        LOGGER.info("assertCheckAvailabilityHeadline completed");
    }

    /**
     * Enters a quantity value on the Product Availability popup
     *
     * @param quantity The quanitity to enter
     */
    public void enterQuantity(String quantity) {
        LOGGER.info("enterQuantity started");
        driver.waitForClassPresent(driver.getByValue(CommonActions.quantityBy), Constants.THIRTY);
        WebElement quantityEl = driver.getDisplayedElement(CommonActions.quantityBy, Constants.ZERO);
        driver.jsScrollToElementClick(quantityEl);
        commonActions.clearAndPopulateEditField(quantityEl, quantity);
        LOGGER.info("enterQuantity completed");
    }

    /**
     * Verifies that an invalid quantity error message is displayed
     *
     * @param errorType The type of error to expect
     */
    public void assertQuanityErrorMessage(String errorType) {
        LOGGER.info("assertQuanityErrorMessage started");
        String actualError;
        String errorMessage = "";

        if (errorType.equalsIgnoreCase(QUANTITY)) {
            errorMessage = QUANTITY_ERROR;
        } else if (errorType.equalsIgnoreCase(VALIDITY)) {
            errorMessage = VALIDITY_ERROR;
        }

        driver.waitForClassPresent(driver.getByValue(quantityErrorBy), Constants.THIRTY);
        WebElement quantityErrorEl = driver.getDisplayedElement(quantityErrorBy, Constants.TWO);
        actualError = quantityErrorEl.getText();

        Assert.assertTrue("FAIL: Expected error message: \"" + errorMessage + "\", Actual message: \"" + actualError + "\"",
                actualError.contains(errorMessage));

        LOGGER.info("Confirmed error message \"" + errorMessage + "\" was displayed.");

        LOGGER.info("assertQuanityErrorMessage completed");
    }

    /**
     * Verifies that the Add To Cart button is disabled
     */
    public void assertAddToCartDisabled() {
        LOGGER.info("assertAddToCartDisabled started");
        WebElement addToCartButton = driver.getDisplayedElement(addToCartBy, Constants.ZERO);
        Assert.assertTrue("FAIL: Add to cart button was not disabled.", !addToCartButton.isEnabled());
        LOGGER.info("Confirmed Add To Cart button was disabled");
        LOGGER.info("assertAddToCartDisabled completed");
    }

    /**
     * Verifies product info on the Check Availability popup
     *
     * @param text The text to verify
     * @param modal The modal that will be active
     */
    public void assertProductInfo(String text, String modal) {
        LOGGER.info("assertProductInfo started");
        WebElement productInfoElement = null;
        if (modal.equals(ConstantsDtc.CHECKFIT)) {
            driver.waitForElementVisible(productCheckFitInfoBy, Constants.FIVE);
            productInfoElement = driver.getDisplayedElement(productCheckFitInfoBy, Constants.ZERO);
        } else if (modal.equals(ConstantsDtc.CHECK_AVAILABILITY)) {
            driver.waitForClassPresent(driver.getByValue(productInfoBy), Constants.THIRTY);
            productInfoElement = driver.getDisplayedElement(productInfoBy, Constants.ZERO);
        }

        Assert.assertTrue("FAIL: Product info did not contain expected text,  \"" + text + "\".",
                productInfoElement.getText().toLowerCase().contains(text.toLowerCase()));

        LOGGER.info("Expected text \"" + text + "\" was listed in product info.");

        LOGGER.info("assertProductInfo completed");
    }

    /**
     * Verifies price is populated
     */
    public void assertPricePopulated() {
        LOGGER.info("assertPricePopulated started");
        driver.waitForClassPresent(driver.getByValue(priceBy), Constants.THIRTY);
        WebElement priceEl = driver.getDisplayedElement(priceBy, Constants.ZERO);
        commonActions.assertElementTextPopulated(priceEl);
        LOGGER.info("assertPricePopulated completed");
    }


    /**
     * Verifies the quantity value in the show quantity filter
     *
     * @param quantity The quantity to verify
     */
    public void assertShowQuantityFilter(String quantity) {
        LOGGER.info("assertShowQuantityFilter started");

        driver.waitForClassPresent(driver.getByValue(filterQuantityBy), Constants.THIRTY);
        WebElement showQuantityFilterEl = driver.getDisplayedElement(filterQuantityBy, Constants.ZERO);

        Assert.assertTrue("FAIL: The show quantity filter did not show expected quantity,  \"" + quantity + "\".",
                showQuantityFilterEl.getText().contains(quantity));

        LOGGER.info("Expected quantity \"" + quantity + "\" was listed in the show quantity filter.");

        LOGGER.info("assertShowQuantityFilter completed");
    }

    /**
     * Enters a quantity value on the Product Availability popup
     *
     * @param zipcode The zipcode to enter
     */
    public void enterZipcode(String zipcode) {
        LOGGER.info("enterZipcode started");
        WebElement zipCodeEl = null;
        if (Config.isMobile()) {
            zipCodeEl = zipMobile;
        } else if (driver.isElementDisplayed(zipWeb) && !driver.isElementDisplayed(zipInventory)) {
            zipCodeEl = zipWeb;
        } else if (!driver.isElementDisplayed(zipWeb) && driver.isElementDisplayed(zipInventory)) {
            zipCodeEl = zipInventory;
        }
        driver.waitForElementVisible(zipCodeEl);
        driver.jsScrollToElementClick(zipCodeEl);
        commonActions.clearAndPopulateEditField(zipCodeEl, zipcode);
        zipCodeEl.click();
        LOGGER.info("enterZipcode completed");
    }

    /**
     * Clicks Go and waits for results to load
     *
     * @throws Exception Descriptive exception message
     */
    public void clickGoAndWaitForResults() {
        LOGGER.info("clickGoAndWaitForResults started");

        try {

            if (driver.isElementDisplayed(zipGoButton)) {
                zipGoButton.click();
            } else {
                zipButton.click();
            }


            if (driver.isElementDisplayed(loadingOverlayBy, Constants.TWO)) {
                driver.waitForElementNotVisible(loadingOverlayBy);
            }
        } catch (Exception e) {
            Assert.fail("FAIL: There was an issue with clicking Go, most likely a zip with no results.");
        }

        LOGGER.info("clickGoAndWaitForResults completed");
    }

    /**
     * Verifies that an invalid zipcode error message is displayed
     */
    public void assertZipcodeErrorMessage() {
        LOGGER.info("assertZipcodeErrorMessage started");
        String actualError;

        driver.waitForClassPresent(driver.getByValue(zipcodeErrorBy), Constants.THIRTY);
        WebElement zipcodeErrorEl = driver.getDisplayedElement(zipcodeErrorBy, Constants.TWO);
        actualError = zipcodeErrorEl.getText();

        Assert.assertTrue("FAIL: Expected error message: \"" + ZIPCODE_ERROR + "\", Actual message: \"" + actualError + "\"",
                actualError.contains(ZIPCODE_ERROR));

        LOGGER.info("Confirmed error message \"" + ZIPCODE_ERROR + "\" was displayed.");

        LOGGER.info("assertZipcodeErrorMessage completed");
    }

    /**
     * Verifies that the first store displayed is within a certain distance in miles
     *
     * @param milesString The miles to verify against
     */
    public void assertFirstStoreWithinDistance(String milesString) {
        LOGGER.info("assertFirstStoreWithinDistance started");

        String actualMilesString;
        int actualMiles;
        int miles = Integer.parseInt(milesString);
        driver.waitForElementVisible(storeCity);
        actualMilesString = storeCity.getText().split(" ")[0];

        try {
            actualMiles = Integer.parseInt(actualMilesString);
        } catch (NumberFormatException e) {
            actualMiles = (int) Double.parseDouble(actualMilesString);
        }

        Assert.assertTrue("FAIL: Expected store city to be within \"" + miles + "\" miles.",
                miles - actualMiles > 0);

        LOGGER.info("Confirmed that first store was within \"" + miles + "\".");

        LOGGER.info("assertFirstStoreWithinDistance completed");
    }

    /**
     * Clicks specified filter
     *
     * @param filter The filter to click
     */
    public void clickFilter(String filter) {
        LOGGER.info("clickFilter started");

        WebElement filterEl;

        if (filter.equalsIgnoreCase(IN_STOCK)) {
            filterEl = inStockFilter;
        } else {
            filterEl = orMoreFilter;
        }

        //filter checkboxes cannot be found by webdriver waits
        driver.waitForElementVisible(zipGoButton);
        driver.jsClick(filterEl);

        if (driver.isElementDisplayed(loadingOverlayBy, Constants.TWO)) {
            driver.waitForElementNotVisible(loadingOverlayBy);
        }


        LOGGER.info("clickFilter completed");
    }

    /**
     * Verifies that the first store displayed is within a certain distance in miles
     *
     * @param quantity The quantity to verify the value is above
     */
    public void assertFirstStoreQuantityAboveSpecifiedValue(String quantity) {
        LOGGER.info("assertFirstStoreQuantityAboveSpecifiedValue started");

        String stockQuantityValue;

        driver.waitForClassPresent(driver.getByValue(stockQuantityBy), Constants.THIRTY);
        WebElement stockQuantityEl = driver.getDisplayedElement(stockQuantityBy, Constants.ZERO);
        stockQuantityValue = stockQuantityEl.getText();

        Assert.assertTrue("FAIL: Stock quantity was not above \"" + quantity + "\", but was \"" +
                stockQuantityValue + "\".", Integer.parseInt(stockQuantityValue) > 0);

        LOGGER.info("Confirmed that first store quantity was above \"" + quantity + "\".");

        LOGGER.info("assertFirstStoreQuantityAboveSpecifiedValue completed");
    }

    /**
     * Clicks specified store and mark make my store
     *
     * @param storeNum The store to click
     */
    public void selectStoreAndMakeMyStore(int storeNum) {
        LOGGER.info("selectStoreAndMakeMyStore started");

        driver.waitForElementVisible(StoreLocatorPage.storeRowBy);
        List<WebElement> stores = webDriver.findElements(StoreLocatorPage.storeRowBy);
        stores.get(storeNum).findElement(storeMakeThisMyStoreButtonBy).click();

        if (driver.isElementDisplayed(loadingOverlayBy, Constants.TWO)) {
            driver.waitForElementNotVisible(loadingOverlayBy);
        }

        LOGGER.info("selectStoreAndMakeMyStore completed");
    }

    /**
     * Verifies that the specified store is the current store
     *
     * @param storeNum The store number to verify
     */
    public void assertStoreIsCurrentStore(int storeNum) {
        LOGGER.info("assertStoreIsCurrentStore started");

        WebElement currentStore;

        driver.waitForElementVisible(store);
        List<WebElement> stores = webDriver.findElements(store);
        currentStore = stores.get(storeNum);

        Assert.assertTrue("FAIL: Store # \"" + storeNum + "\" was not current store.",
                currentStore.getAttribute(Constants.CLASS).contains(driver.getByValue(currentStoreBy)));

        LOGGER.info("Confirmed that store # \"" + storeNum + "\" was the current store.");

        LOGGER.info("assertStoreIsCurrentStore completed");
    }

    /**
     * Verifies that Make My Store button is present on the page
     */
    public void assertMakeMyStoreButtonDisplayed() {
        LOGGER.info("assertMakeMyStoreButtonDisplayed started");
        driver.waitForElementVisible(makeMyStoreBy);

        int storeListEntryForMakeMyStoreCount = 0;

        List<WebElement> rows = webDriver.findElements(makeMyStoreBy);

        for (WebElement row : rows) {
            if (!row.getAttribute(ConstantsDtc.ATTR_STYLE).contains(Constants.NONE)) {
                Assert.assertTrue("FAIL: MAKE MY STORE button is missing on the page for row number "
                                + storeListEntryForMakeMyStoreCount, row.getText().contains(ConstantsDtc.MAKE));

                storeListEntryForMakeMyStoreCount++;
            }
        }
        LOGGER.info("assertMakeMyStoreButtonDisplayed completed");
    }

    /**
     * Confirms the expected near by store displayed for default my store
     * This method is just covered for DT now, and will be extended to have AT when we have the near by
     * store details for AT Store
     *
     * @param page web page
     */
    public void assertNearbyStoreForDefaultDtStoreQA(String page) {
        LOGGER.info("assertNearbyStoreForDefaultDtStoreQA started from " + page);
        driver.waitForElementVisible(myStoreEle);
        List<WebElement> myStoreRows = webDriver.findElements(storeAddressBy);
        Assert.assertTrue("FAIL: No nearby store is displayed when directed from " + page + " page!",
                myStoreRows.size() > 0);
        Assert.assertTrue("FAIL: Nearby Store displayed: '" + myStoreRows.get(0).getText() + "', expected : '"
                        + ConstantsDtc.DT_QA_DEFAULT_NEARBY_STORE_ADDRESS + "' , when directed from " + page,
                myStoreRows.get(0).getText().toLowerCase().contains(ConstantsDtc.DT_QA_DEFAULT_NEARBY_STORE_ADDRESS.
                        toLowerCase()));
        LOGGER.info("assertNearbyStoreForDefaultDtStoreQA completed from " + page);
    }

    /**
     * Confirms my store is displayed at the top
     *
     * @param store store address
     */
    public void assertDefaultMyStoreDisplayedOnTop(String store) {
        LOGGER.info("assertDefaultMyStoreDisplayedOnTop started");
        driver.waitForElementVisible(myStoreEle);
        String mystore = webDriver.findElement(myStoreAddressBy).getText();
        if (store.toLowerCase().contains(ConstantsDtc.DEFAULT)) {
            store = ConstantsDtc.DT_QA_DEFAULT_STORE_PARTIAL_ADDRESS;
        }
        Assert.assertTrue("FAIL: MY STORE is not displayed at the top, displayed: '" + mystore
                + "' ,does not contain expected address: " + store
                + "'.", mystore.toLowerCase().contains(store.toLowerCase()));
        LOGGER.info("assertDefaultMyStoreDisplayedOnTop completed");
    }

    /**
     * Confirms my store is displayed on check inventory modal
     */
    public void assertDefaultMyStoreDisplayedOnCheckInventoryModal() {
        LOGGER.info("assertDefaultMyStoreDisplayedOnCheckInventoryModal started");
        WebElement element;
        List<WebElement> myStoreInfo = webDriver.findElements(checkInventoryLocationAddressBy);
        if (Config.isMobile()){
            element =  myStoreInfo.get(0);
        } else {
            element =  myStoreInfo.get(1);
        }
        driver.waitForElementVisible(element);
        String mystore = element.getText();
        Assert.assertTrue("FAIL: MY STORE is not displayed on check inventory, displayed: '" + mystore
                + "' ,does not contain expected address: " + ConstantsDtc.DT_QA_DEFAULT_STORE_PARTIAL_ADDRESS
                + "'.", mystore.toLowerCase().contains(ConstantsDtc.DT_QA_DEFAULT_STORE_PARTIAL_ADDRESS.toLowerCase()));
        LOGGER.info("assertDefaultMyStoreDisplayedOnCheckInventoryModal completed");
    }

    /**
     * Confirms the expected nearby store displayed for default my store on check inventory modal
     */
    public void assertNearbyStoreCheckInventory() {
        LOGGER.info("assertNearbyStoreCheckInventory started");
        driver.waitForElementVisible(nearestStoreBy);

        //TODO CCL - will this work on AT???
        WebElement myStoreRows = webDriver.findElement(nearestStoreBy);
        Assert.assertTrue("FAIL: No nearby store is displayed on check inventory modal!",
                driver.isElementDisplayed(nearestStoreBy));
        Assert.assertTrue("FAIL: Nearby Store displayed on check inventory modal: '" + myStoreRows.getText()
                        + "', expected : '" + ConstantsDtc.DT_QA_DEFAULT_NEARBY_STORE_ADDRESS,
                myStoreRows.getText().toLowerCase().contains(ConstantsDtc.DT_QA_DEFAULT_NEARBY_STORE_ADDRESS.
                        toLowerCase()));
        LOGGER.info("assertNearbyStoreCheckInventory completed");
    }

    /**
     * Close Check Inventory Popup page
     */
    public void closeCheckInventoryPopupModal() {
        LOGGER.info("closeCheckInventoryPopupModal started");
        driver.waitForElementVisible(checkInventoryHeaderClose);
        checkInventoryHeaderClose.click();
        LOGGER.info("closeCheckInventoryPopupModal completed");
    }

    /**
     * Verify Inventory Message for set product on Check Availability Popup Modal
     *
     * @param message  the expected Inventory Message
     * @param itemType Front/Rear
     * @param item     product code
     * @param page     web page
     */
    public void assertInventoryMessageForSetOnPopupModal(String message, String itemType, String item, String page) {
        LOGGER.info("assertInventoryMessageForSetOnPopupModal started for " + item + " on " + page + " page");
        driver.waitForPageToLoad();
        String inventoryMessageOnModal;
        List<String> inventoryMessageList;
        List<WebElement> inventoryMessage = webDriver.findElements(CommonActions.stockInventoryMessageBy);
        if (webDriver.findElement(CommonActions.headerSecondBy).getText().toLowerCase().contains(CommonActions.CART.toLowerCase())) {
            if (itemType.equalsIgnoreCase(ConstantsDtc.FRONT)) {
                inventoryMessageOnModal = inventoryMessage.get(2).getText();
            } else {
                inventoryMessageOnModal = inventoryMessage.get(3).getText();
            }
        } else {
            if (itemType.equalsIgnoreCase(ConstantsDtc.FRONT)) {
                inventoryMessageOnModal = inventoryMessage.get(0).getText();
            } else {
                inventoryMessageOnModal = inventoryMessage.get(1).getText();
            }
        }
        inventoryMessageList = Arrays.asList(message.split(":"));

        for (int i = 0; i < inventoryMessageList.size(); i++) {
            Assert.assertTrue("FAIL: The inventory message " + inventoryMessageOnModal + " did not contain the expected" +
                    " inventory message " + message, inventoryMessageOnModal.toLowerCase().contains
                    (inventoryMessageList.get(i).toLowerCase()));
        }
        LOGGER.info("assertInventoryMessageForSetOnPopupModal completed on " + item + " on " + page + " page");
    }

    /**
     * Verify stock count display for front/Rear item on check inventory popup
     *
     * @param itemType Front/Rear
     * @param itemCode Product Code
     */
    public void assertStockCountTextForSetOnPopupModal(String itemType, String itemCode) {
        LOGGER.info("assertStockCountTextForSetOnPopupModal started for " + itemType + " product " + itemCode);
        driver.waitForPageToLoad();
        int elementIndex = 0;
        List<WebElement> stockMessages = webDriver.findElements(stockCountTextBy);
        if (itemType.equalsIgnoreCase(ConstantsDtc.REAR)) {
            elementIndex++;
        }
        driver.jsScrollToElement(stockMessages.get(elementIndex));
        Assert.assertTrue("FAIL: The Cart inventory message stock count for " + itemType + " product "
                        + itemCode + " displayed: '" + stockMessages.get(elementIndex).getText()
                        + "' did NOT contain the expected text '" + ConstantsDtc.IN_STOCK + "' .",
                stockMessages.get(elementIndex).getText().contains(ConstantsDtc.IN_STOCK));
        LOGGER.info("assertStockCountTextForSetOnPopupModal completed for " + itemType + " product " + itemCode);
    }

    /**
     * Verifies the item quantity on the 'Check Inventory' popup matches the previously saved 'Cart' page quantity
     * ****Currently only works with item codes and tire sets as that is the only way the item codes are present in the
     * DOM****
     *
     * @param itemCode product/item code on both 'Cart' and 'Check Inventory' used in the quantity comparison
     */
    public void verifyCheckInventoryQuantityForItemMatchesCartQuantity(String itemCode) {
        LOGGER.info("verifyCheckInventoryQuantityForItemMatchesCartQuantity started with item code: '" + itemCode
                + "'");
        driver.waitForElementVisible(checkInventoryHeaderClose);
        WebElement checkInventoryItem = driver.getElementWithAttribute(itemSpecsBy, DATA_FRONT_PRODUCT_CODE,
                itemCode);

        if (checkInventoryItem == null)
            checkInventoryItem = driver.getElementWithAttribute(itemSpecsBy, DATA_REAR_PRODUCT_CODE, itemCode);

        if (checkInventoryItem == null)
            checkInventoryItem = webDriver.findElement(itemSpecsBy);

        Assert.assertNotNull("FAIL: Could NOT find item code: '" + itemCode + "' on  'Check Inventory'! Unable" +
                " to validate 'Check Inventory' quantity against 'Cart' item quantity!", checkInventoryItem);

        String checkInventoryItemQuantity = checkInventoryItem.findElement(itemQuantityAmountBy).getText();

        Assert.assertEquals("FAIL: The 'Check Inventory' and 'Cart' quantities are different for item code '"
                        + itemCode + "'!", Integer.parseInt(CartPage.itemCodeQuantityMap.get(itemCode)),
                Integer.parseInt(checkInventoryItemQuantity));

        LOGGER.info("verifyCheckInventoryQuantityForItemMatchesCartQuantity completed with item code: '" + itemCode
                + "'");
    }

    /**
     * Verifies the "Only Show In Stock" option is displayed on the "Check Inventory" popup
     */
    public void verifyOnlyShowInStockOptionIsDisplayed() {
        LOGGER.info("verifyOnlyShowInStockOptionIsDisplayed started");
        driver.waitForElementVisible(checkInventoryHeaderClose);
        Assert.assertTrue("FAIL: The 'Only Show In Stock' option was NOT displayed on the 'Check Inventory' popup!",
                driver.isElementDisplayed(onlyShowInStocklabel));
        LOGGER.info("verifyOnlyShowInStockOptionIsDisplayed completed");
    }

    /**
     * Conducts a search via the "Show Stores Near" search bar with the user specified search term
     *
     * @param searchTerm search term (zipcode, city name, etc.) used in "Show Stores Near" search
     */
    public void showStoresNearSearch(String searchTerm) {
        LOGGER.info("showStoresNearSearch started with search term: " + searchTerm);
        commonActions.clearAndPopulateEditField(CommonActions.cityStateZipSearchField, searchTerm);
        commonActions.clickButtonByText(Constants.SEARCH);
        driver.waitForMilliseconds(Constants.THREE_THOUSAND);
        LOGGER.info("showStoresNearSearch completed with search term: " + searchTerm);
    }

    /**
     * Verifies that after a "Show Stores Near" search, that at least 1 additional store (not user's current) is
     * displayed
     */
    public void verifyNearbyStoresAreDisplayed() {
        LOGGER.info("verifyNearbyStoresAreDisplayed started");
        driver.waitForElementVisible(inventoryLocationsRowBy);
        List<WebElement> storeLocationsList = webDriver.findElements(inventoryStoreCellsBy);
        Assert.assertTrue("FAIL: No nearby stores were displayed!", storeLocationsList.size() > 0);
        if (storeLocationsList.size() == 1 && storeLocationsList.get(0).getAttribute(Constants.CLASS)
                .contains(CELL_SELECTED))
            Assert.fail("FAIL: Only the user's current store was displayed!");
        LOGGER.info("verifyNearbyStoresAreDisplayed completed");
    }

    /**
     * Clicks the "MAKE MY STORE" button for the first available store in the nearby stores results on the
     * "Check Inventory" popup
     */
    public void clickMakeMyStoreForFirstStore() {
        LOGGER.info("clickMakeMyStoreForFirstStore started");
        driver.waitForMilliseconds();
        driver.waitForElementVisible(inventoryLocationsRowBy);
        List<WebElement> storeLocationsList = webDriver.findElements(inventoryStoreCellsBy);
        for (WebElement storeLocation : storeLocationsList){
            if (!storeLocation.getAttribute(Constants.CLASS).contains(CELL_SELECTED)) {
                makeMyStorePhoneNumber = storeLocation.findElement(inventoryStorePhoneNumberBy).getText();
                driver.webElementClick(storeLocation.findElement(inventoryStoreMakeMyStoreBtnBy));
                driver.waitForPageToLoad();
                break;
            }
        }
        LOGGER.info("clickMakeMyStoreForFirstStore completed");
    }

    /**
     * Clicks the "MAKE MY STORE" button for the specified available store in the nearby stores results on the
     * "Check Inventory" popup
     *
     * @param storeName store Name in row
     */
    public void clickMakeMyStoreForStore(String storeName) {
        LOGGER.info("clickMakeMyStoreForStore started for '" + storeName + "' store in the list.");
        driver.waitForElementVisible(inventoryLocationsRowBy, Constants.THIRTY);
        List<WebElement> storeLocationsList = webDriver.findElements(inventoryStoreCellsBy);
        for(WebElement storeLocation : storeLocationsList){
            if(storeLocation.getText().toLowerCase().contains(storeName.toLowerCase()) &&
                    !storeLocation.getAttribute(Constants.CLASS).contains(CELL_SELECTED)){
                driver.webElementClick(storeLocation.findElement(inventoryStoreMakeMyStoreBtnBy));
                driver.waitForMilliseconds();
                break;
            }
        }
        LOGGER.info("clickMakeMyStoreForStore completed for '" + storeName + "' store in the list.");
    }

    /**
     * This method verifies the store name and address on the Check Availability Popup
     */
    public void verifyStoreName() {
        LOGGER.info("verifyStoreName started");
        if (Config.isMobile()) {
            LOGGER.info("verifyStoreName skipped on mobile");
        }else {
            driver.waitForElementVisible(storeAddressBy);
            String plpStoreName = webDriver.findElement(storeAddressBy).getText().replaceAll("\n", " - ");
            Assert.assertTrue("FAIL: The store on the Check Availability Popup is not correct. Expected '" +
                            CommonActionsSteps.storeName + "' to contain " + plpStoreName,
                    CommonUtils.containsIgnoreCase(plpStoreName, CommonActionsSteps.storeName));
        }
        LOGGER.info("verifyStoreName completed");
    }

    /**
     * Verifies if the sort by option is set to the specified option
     *
     * @param filter availability or Nearest Store
     * @param option stock count or distance
     * @param order  descending or ascending
     */
    public void assertSortingOnCheckAvailabilityPopup(String filter, String option, String order) {
        LOGGER.info("assertSortingOnCheckAvailabilityPopup started");
        List<WebElement> optionList;
        ArrayList<Float> stock = new ArrayList<>();
        String sortByOption = checkAvailabilityPopup.getText();
        Assert.assertTrue("FAIL: The sort by option is not set to " + option + " instead to set to " + sortByOption,
                sortByOption.toLowerCase().contains(filter.toLowerCase()));
        if (filter.equalsIgnoreCase(ConstantsDtc.AVAILABILITY)) {
            optionList = webDriver.findElements(storeInventoryStockBy);
            for (int i = 0; i < optionList.size(); i++) {
                stock.add(Float.parseFloat(optionList.get(i).getText().split(" ")[0]));
            }
        } else {
            optionList = webDriver.findElements(storeDistanceBy);
            for (int i = 0; i < optionList.size(); i++) {
                stock.add(Float.parseFloat(optionList.get(i).getText().split(" ")[0]));
            }
        }
        if (order.equalsIgnoreCase(ProductListPage.ASCENDING)) {
            Assert.assertTrue("FAIL: The " + option + " is not in " + order + " order.",
                    commonActions.verifyAscendingOrder(stock));
        } else {
            Assert.assertTrue("FAIL: The " + option + " is not in " + order + " order.",
                    commonActions.verifyDescendingOrder(stock));
        }
        LOGGER.info("assertSortingOnCheckAvailabilityPopup completed");
    }
}