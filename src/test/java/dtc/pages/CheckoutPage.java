package dtc.pages;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import dtc.data.Customer;
import dtc.steps.CommonActionsSteps;
import common.CommonExcel;
import orderxmls.pages.OrderXmls;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.CommonUtils;
import utilities.Driver;

import static java.util.Calendar.MINUTE;

import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.HashMap;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created by aaronbriel on 9/27/16.
 */
public class CheckoutPage {

    private Driver driver;
    private WebDriver webDriver;
    private CommonExcel commonExcel;
    private final CommonUtils commonUtils;
    private final CommonActions commonActions;
    private final CartPage cartPage;
    private final Customer customer;
    private final AppointmentPage appointmentPage;
    private final OrderPage orderPage;
    private final Calendar calendar = Calendar.getInstance();
    private static final Logger LOGGER = Logger.getLogger(CheckoutPage.class.getName());

    public CheckoutPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonUtils = new CommonUtils();
        commonActions = new CommonActions(driver);
        cartPage = new CartPage(driver);
        customer = new Customer();
        appointmentPage = new AppointmentPage(driver);
        orderPage = new OrderPage(driver);
        PageFactory.initElements(webDriver, this);
        commonExcel = new CommonExcel();
    }

    public static HashMap<String, String> checkoutOrderSummaryValues = new HashMap<>();

    public static final String GROUND_FREE = "Ground - Free Shipping";
    private static final String SHIPPING_ADDRESS = "Shipping Address";
    private static final String DELIVERY_METHOD = "Delivery Method";
    private static final String PAYMENT_BILLING_ADDRESS = "Payment & Billing Address";
    private static final String AMERICAN_EXPRESS = "American Express";
    private static final String DISCOVER = "Discover";
    public static final String VISA = "Visa";
    public static final String AMEX = "amex";
    private static final String VISA_BOPIS = "Visa Bopis";
    private static final String AMERICAN_EXPRESS_BOPIS = "American Express Bopis";
    private static final String DISCOVER_BOPIS = "Discover Bopis";
    private static final String MASTERCARD_BOPIS = "MasterCard Bopis";
    private static final String CAR_CARE_ONE_BOPIS = "CarCareOne Bopis";
    private static final String CAR_CARE_ONE_2_BOPIS = "CarCareOne_2 Bopis";
    private static final String WALKINS_WELCOME = "Walk-Ins Welcome";
    private static final String SHIPPING_FREE = "FREE";
    private static final String VIEW_TIMES = "View Times";
    private static final String SPECIAL_HOURS = "Special Hours";
    private static final String STORE_CLOSED = "Store Closed";
    private static final String PEAK_TIMES_MESSAGE = "Peak Times: Expect a longer wait for service";
    private static final String SERVICE_TIME_LONGER_THAN_USUAL =
            "Service time for this appointment may be longer than usual.";
    private static final String EST_TIME_OF_COMPLETION = "Est. time of completion:";
    private static final String ACCEPT = "accept";
    private static final String DECLINE = "decline";
    private static final String SHIPPING_RESTRICTION = "Shipping Restriction";
    private static final String EDIT_CART = "Edit Cart";
    private static final String REMOVE_ITEMS = "Remove Items";
    private static final String SHIPPING_RESTRICTION_MESSAGING =
            "The following brands may not be shipped to the location you specified:";
    private static final String CHANGE_ADDRESS = "Change Address";
    private static final String FULLDAY = "Fullday";
    public static final String UNABLE_TO_PROCESS_CARD = "We're sorry, we were unable to process the card";

    private static final String VERIFY_ADDRESS_POPUP_CLOSED_JS =
            "return document.getElementById('popup_suggested_delivery_addresses_form')===null;";
    private static final String[] CART_SUMMARY_PROMOTIONS_DISPLAY_CHECKOUT_DTAT = {"summary-product__header",
            "checkout-summary-cart__fees", "cart-item__section summary-product__promotions", "summary-product__header",
            "checkout-summary-cart__fees", "collapsible-section checkout-summary-cart__fees",
            "cart-item__section summary-product__promotions", "summary-product__subtotal"};
    private static final String[] CART_SUMMARY_PROMOTIONS_DISPLAY_CHECKOUT_DTD = {"summary-product__header",
            "checkout-summary-cart__fees", "cart-item__section summary-product__promotions", "summary-product__header",
            "checkout-summary-cart__fees", "cart-item__section summary-product__promotions",
            "summary-product__subtotal"};

    public static final String SHIPPING_DETAILS = "SHIPPING DETAILS";
    private static final String DISPLAY_REASON_FIELD_LABEL = "Reason:";
    private static final String AVAILABLE_TIMES_CLASS_TEXT = "appointment-details__select-time-row-time--available";
    private static final String ACTIVE_APPOINTMENT_DATE_CLASS_TEXT = "appointment-details__select-date--active";
    private static final String THREE_TO_FIVE = "3 - 5";
    private static final String IN_TWO = "in 2";
    private static final String AVAILABLE_TOMORROW = "available tomorrow";
    public static final String ZIP_CODE_IS_INVALID = "Zip Code is invalid";
    private static final int REGULAR_APPOINTMENT_DATE_RANGE = 10;
    private static final int SERVICE_APPOINTMENT_DATE_RANGE = 39;

    private static final By summaryProductProductTotalBy = By.className("summary-product__product-total");

    public static final By salesTaxRowBy = By.className("checkout-summary-cart__tax-row");

    public static final By feeDetailsBy = By.className("summary-product__fees-container");

    private static final By sectionHeadersBy = By.className("checkout-steps__header");

    private static final By paymentTypesContainerBy = By.className("payment-details__checkout-types");

    private static final By allAvailableApptDatesBy = By.className("appointment-details__select-date");

    private static final By selectDateMessageBy = By.className("appointment-details__select-date-message");

    private static final By allApptTimesBy = By.className("appointment-details__select-time-row-time");

    private static final By tooltipBtnBy =
            By.xpath("//BUTTON[@class=\"tooltip-toggle icon-info undefined\" and text() = \"i\"]");

    private static final By summaryProductHeaderBy = By.className("summary-product__header");

    private static final By summaryProductFeesAndPromotionsBy =
            By.cssSelector("[class*='order-summary-cart-product__feesAndPromotions']");

    private static final By checkoutSummaryCertificateBy = By.className("checkout-summary-cart__fees-certificates");

    private static final By checkoutSummaryFeeTpmsBy = By.className("checkout-summary-cart__fees-tpms");

    private static final By peakHoursRowContainerBy =
            By.className("appointment-details__select-time-row-container--peak");

    private static final By appointmentRowLabelBy = By.className("appointment-details__select-time-row-label");

    private static final By checkoutOrderSummaryBy = By.className("checkout-summary-cart__products");

    private static final By productQuantityBy = By.className("summary-product__product-quantity");

    private static final By ropisBy = By.cssSelector(".form-group__radio-label[for='retailStrategy-ropis']");

    public static final By bopisBy = By.cssSelector(".form-group__radio-label[for='retailStrategy-bopis']");

    public static final By someoneElsePickUpElementBy =
            By.cssSelector(".checkout-form__recipient-customer > .collapsible-section__toggle");

    private static final By ccNumberBy = By.xpath("//input[@name='cardNumber']");

    private static final By ccNameBy = By.xpath("//input[@name='nameOnCard']");

    private static final By ccCvnBy = By.xpath("//input[@name='cardCVN']");

    public static final By addressLine1By = By.name("billingAddress1");

    public static final By billingZipCodeBy = By.name("billingZipCode");

    private static final By amountBy = By.name("amount");

    private static final By summaryProductDisplayBy = By.cssSelector(".summary-product > div");

    private static final By editCardDetailsLinkBy = By.linkText("Edit card details");

    public static final By checkoutErrorBy = By.className("checkout-general-error");

    public static final By nextDayArrowControlBy = By.className("appointment-details__select-time-header-next-date");

    private static final By paymentDetailsFormTotalPriceBy = By.className("payment-details-form__amount-total-price");

    public static final By cartFeeStaggeredRearBy = By.cssSelector(".summary-product :nth-child(5) .cart-item__row");

    public static final By submitAddressVerification = By.cssSelector("[id='addressForm'] .dt-form__submit");

    public static final By taxLabelOnCheckoutBy = By.cssSelector(".checkout-summary-cart__line-item-label-name");

    public static final By deliveryEmailAddressBy = By.className("checkout-summary__customer-alternate-details");

    public static final By checkoutSummaryCustomerDetailsBy = By.className("checkout-summary__customer-details");

    public static final By paymentTabBy = By.cssSelector("[class*='payment-tabs__tab']");

    public static final By cartSummaryListBy = By.xpath("//*[@class='summary-product']");

    public static final By checkoutSalesTaxBy = By.cssSelector("[class*='checkout-summary-cart__line-item']");

    @FindBy(css = ".checkout-avs__address-container--last")
    public static WebElement uspsCorrectedAddress;

    @FindBy(className = "react-selectize-toggle-button")
    public static WebElement withoutApptReasonToggleBtn;

    @FindBy(id = "placeOrder")
    public static WebElement placeOrderButton;

    @FindBy(xpath = "//input[@id='scheduledAppointment']/following-sibling::span[contains(@class,'dt-radio__display')]")
    public static WebElement scheduleAppointmentRadio;

    @FindBy(xpath = "//input[@id='reserveItemsOnly']/following-sibling::span[contains(@class,'dt-radio__display')]")
    public static WebElement reserveItemsRadio;

    @FindBy(xpath = "//input[@id='retailStrategy-bopis']/following-sibling::span[contains(@class,'dt-radio__display')]")
    public static WebElement bopisRadio;

    @FindBy(xpath = "//input[@id='retailStrategy-ropis']/following-sibling::span[contains(@class,'dt-radio__display')]")
    public static WebElement ropisRadio;

    @FindBy(className = "delivery-method__radio")
    public static WebElement deliverySection;

    @FindBy(className = "modal-box")
    public static WebElement creditCardConsentModal;

    @FindBy(css = "[aria-label='I Consent']")
    public static WebElement acceptConsentButton;

    @FindBy(css = "[aria-label='Decline']")
    public static WebElement declineConsentButton;

    @FindBy(className = "termsOfAgreement")
    public static WebElement creditCardToaModal;

    @FindBy(css = "[aria-label='Accept & Submit']")
    public static WebElement toaAcceptAndSubmitButton;

    @FindBy(className = "declineTerms")
    public static WebElement toaDeclineButton;

    @FindBy(className = "checkout-summary-cart__cart-items-price")
    public static WebElement checkoutSubtotal;

    @FindBy(className = "auto-cart-summary-fee-detail")
    public static WebElement feeCartSummaryItem;

    @FindBy(className = "cart-summary__opt-fee-detail")
    public static WebElement serviceCartSummaryItem;

    @FindBy(className = "checkout-steps__edit")
    public static WebElement edit;

    @FindBy(className = "checkout-summary__edit-link")
    public static WebElement checkoutSummaryEditLink;

    @FindBy(className = "delivery-method__fetch")
    public static WebElement seeMoreOptionsLink;

    @FindBy(className = "payment-details-form__use-shipping-address-label")
    public static WebElement billingSameAsShippingOption;

    @FindBy(xpath = "//label[@for='reserveItemsOnly']")
    public static WebElement installWithoutApptBtn;

    @FindBy(xpath = "//label[@for='scheduledAppointment']")
    public static WebElement installWithApptBtn;

    @FindBy(className = "appointment-details__select-appointment-date-container")
    public static WebElement apptDateContainer;

    @FindBy(className = "appointment-details__select-time")
    public static WebElement apptTimeContainer;

    @FindBy(className = "checkout-summary-cart__fees-header-label")
    public static WebElement expandFeeDetailsLabel;

    @FindBy(className = "checkout-summary-cart__line-item-price")
    public static WebElement checkoutAndOrderCartSummarySalesTax;

    @FindBy(xpath = "//span[contains(.,'Shipping')]/following-sibling::span")
    public static WebElement shipping;

    @FindBy(id = "customerInformationForm")
    public static WebElement customerInfoField;

    @FindBy(css = "[class*='appointment-details__select-time-no-availability']")
    public static WebElement noAppointmentTimesAvailable;

    @FindBy(css = "[class*='checkout-summary-store__address']")
    public static WebElement checkoutCartSummaryStoreName;

    @FindBy(className = "appiontment-summary__reason")
    public static WebElement checkoutApptDetailsReason;

    @FindBy(className = "checkout-summary__customer-details")
    public static WebElement checkoutSummaryCustomerDetails;

    @FindBy(className = "react-selectize-placeholder")
    private static WebElement installWithoutAppointmentSelectedReason;

    @FindBy(className = "form-group__select")
    public static WebElement formGroupSelect;

    @FindBy(className = "appointment-details__reserve-message")
    private static WebElement reserveAppointmentMessage;

    @FindBy(xpath = "//a[contains(@href, '/checkout/appointment-info')]")
    private static WebElement editAppointmentLink;

    @FindBy(className = "appointment-details__select-time-row-peak-message")
    private static WebElement peakTimesMessage;

    @FindBy(className = "appointment-details__select-time-row-peak-selected-message-body")
    private static WebElement peakTimeSelectedMessage;

    @FindBy(className = "appointment-details__select-time-row-time--selected")
    private static WebElement selectedAppointmentTime;

    @FindBy(className = "checkout-form__shipping-restriction")
    public static WebElement shippingRestrictionMessagingContainer;

    @FindBy(className = "js-nav-checkout-need-help-button")
    public static WebElement needHelpElement;

    @FindBy(css = "div.popover--checkout-need-help-block")
    public static WebElement needHelpPhoneElement;

    @FindBy(className = "min-header__nav-button--my-account")
    public static WebElement userSignedInLabel;

    @FindBy(className = "appointment-details__select-time-header-previous-date")
    public static WebElement prevDayArrowControl;

    @FindBy(className = "appointment-details__select-time-header-next-date")
    public static WebElement nextDayArrowControl;

    @FindBy(className = "bar-chart__values")
    public static WebElement barChartGraph;

    @FindBy(className = "appointment-details__select-time-first-available")
    public static WebElement firstAvailableAppointmentTimeMessage;

    @FindBy(className = "appointment-details__select-time-row-partial-closed-message")
    public static WebElement partialHolidayClosedMessage;

    @FindBy(className = "payment-details__primary-card")
    public static WebElement paymentDetailsPrimaryCard;

    @FindBy(className = "payment-details__secondary-card")
    public static WebElement paymentDetailsSecondaryCard;

    @FindBy(className = "summary-product__subtotal-amount")
    private static WebElement subTotal;

    @FindBy(className = "summary-product__product-name")
    private static WebElement productName;

    @FindBy(className = "summary-product__product-brand")
    private static WebElement productBrand;

    @FindBy(className = "summary-product__product-size")
    private static WebElement productSize;

    @FindBy(css = "div[class*= 'paypal-button']")
    public static WebElement payPalButton;

    @FindBy(name = "cert_email")
    public static WebElement certificatesEmail;

    @FindBy(className = "checkout-order-summary")
    public static WebElement checkoutOrderSummary;

    @FindBy(className = "checkout-form__billing-address")
    public static WebElement paymentDetailsBillingAddress;

    private class PaymentFields {
        private WebElement ccName;
        private WebElement ccNumber;
        private WebElement ccCvn;
        private WebElement addressLine1;
        private WebElement billingZipCode;

        public WebElement getCcName() {
            return ccName;
        }

        public WebElement getCcNumber() {
            return ccNumber;
        }

        public WebElement getCcCvn() {
            return ccCvn;
        }

        public WebElement getAddressLine1() {
            return addressLine1;
        }

        public WebElement getBillingZipCode() {
            return billingZipCode;
        }

        /**
         * Initializes the PaymentFields private class, and instantiates the class objects. Object instantiation
         *
         * @return PaymentFields class with instantiated objects (payment field WebElements)
         */
        public PaymentFields invokePaymentFields() {
            LOGGER.info("invokePaymentFields started");
            int formIndex = 0;
            if (!Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
                if (driver.isElementDisplayed(amountBy, Constants.ZERO))
                    formIndex = 1;
            }

            if (Config.isSafari())
                driver.waitForMilliseconds(Constants.FIVE_THOUSAND);

            ccName = webDriver.findElements(ccNameBy).get(formIndex);
            ccNumber = webDriver.findElements(ccNumberBy).get(formIndex);
            ccCvn = webDriver.findElements(ccCvnBy).get(formIndex);
            boolean useExistingAddress = false;
            WebElement checkBox = null;
            if (driver.isElementDisplayed(paymentDetailsBillingAddress)) {
                checkBox = paymentDetailsBillingAddress.findElement(CommonActions.checkboxInputBy);
                if (checkBox.getAttribute(ConstantsDtc.SELECTED) != null &&
                        checkBox.getAttribute(ConstantsDtc.SELECTED).equalsIgnoreCase(String.valueOf(true))) {
                    driver.jsScrollToElementClick(checkBox, false);
                    driver.waitForMilliseconds();
                    useExistingAddress = true;
                }
                addressLine1 = webDriver.findElements(addressLine1By).get(formIndex);
                billingZipCode = webDriver.findElements(billingZipCodeBy).get(formIndex);
            }
            if (useExistingAddress) {
                driver.jsScrollToElementClick(checkBox, false);
                driver.waitForMilliseconds();
            }
            LOGGER.info("invokePaymentFields completed");
            return this;
        }

        /**
         * Initializes the PaymentFields private class, and instantiates the class objects. Object instantiation
         *
         * @param paymentForm - 'primary' or 'secondary'. Determines which of the two forms to get field info from.
         * @return PaymentFields class with instantiated objects (payment field WebElements)
         */
        private PaymentFields invokePaymentFields(String paymentForm) {
            LOGGER.info("invokePaymentFields started for " + paymentForm + " card");
            int formIndex = commonActions.getPaymentFormIndex(paymentForm);
            if (Config.isSafari())
                driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
            ccName = webDriver.findElements(ccNameBy).get(formIndex);
            ccNumber = webDriver.findElements(ccNumberBy).get(formIndex);
            ccCvn = webDriver.findElements(ccCvnBy).get(formIndex);
            boolean useExistingAddress = false;
            WebElement checkBox = null;
            if (driver.isElementDisplayed(paymentDetailsBillingAddress)) {
                checkBox = paymentDetailsBillingAddress.findElement(CommonActions.checkboxInputBy);
                if (checkBox.getAttribute(ConstantsDtc.SELECTED) != null &&
                        checkBox.getAttribute(ConstantsDtc.SELECTED).equalsIgnoreCase(String.valueOf(true))) {
                    driver.jsScrollToElementClick(checkBox, false);
                    driver.waitForMilliseconds();
                    useExistingAddress = true;
                }
                addressLine1 = webDriver.findElements(addressLine1By).get(formIndex);
                billingZipCode = webDriver.findElements(billingZipCodeBy).get(formIndex);
            }
            if (useExistingAddress) {
                driver.jsScrollToElementClick(checkBox, false);
                driver.waitForMilliseconds();
            }
            LOGGER.info("invokePaymentFields completed for " + paymentForm + " card");
            return this;
        }
    }

    /**
     * Clicks one of the reservation radio button options on the checkout page
     *
     * @param checkoutType With appointment or without appointment
     */
    public void clickReservationRadioButton(String checkoutType) {
        LOGGER.info("clickReservationRadioButton started");
        if (checkoutType.equalsIgnoreCase(ConstantsDtc.INSTALL_WITHOUT_APPOINTMENT)) {
            driver.waitForElementClickable(reserveItemsRadio, Constants.FIVE);
            reserveItemsRadio.click();
        } else if (checkoutType.equalsIgnoreCase(ConstantsDtc.INSTALL_WITH_APPOINTMENT)) {
            driver.waitForElementClickable(scheduleAppointmentRadio, Constants.FIVE);
            scheduleAppointmentRadio.click();
        } else {
            Assert.fail("FAIL: Did not indicate which of the reservation radio buttons should be selected!");
        }
        LOGGER.info("clickReservationRadioButton completed");
    }

    /**
     * Validates the expected reservation radio button was selected on the checkout page
     *
     * @param checkoutType With appointment or without appointment
     */
    public void assertAppointmentOption(String checkoutType) {
        LOGGER.info("assertAppointmentOption started");
        //TODO: retest when new safaridriver is stable
        //Oddly SafariDriver as of 12/5/16 ends up with schedule appointment checked no matter the prior click.
        // This is not reproducible manually, and various pauses did not resolve it
        if (Config.isSafari()) {
            clickReservationRadioButton(checkoutType);
        }

        driver.waitForElementVisible(CommonActions.radioButtonBy);

        if (checkoutType.equalsIgnoreCase(ConstantsDtc.INSTALL_WITHOUT_APPOINTMENT)) {
            Assert.assertTrue("FAIL: \"" + ConstantsDtc.INSTALL_WITHOUT_APPOINTMENT + "\" option was NOT enabled!",
                    reserveItemsRadio.isEnabled());
            LOGGER.info("Confirmed that \"" + ConstantsDtc.INSTALL_WITH_APPOINTMENT + "\" option was enabled.");
        } else if (checkoutType.equalsIgnoreCase(ConstantsDtc.INSTALL_WITH_APPOINTMENT)) {
            Assert.assertTrue("FAIL: \"" + ConstantsDtc.INSTALL_WITH_APPOINTMENT + "\" option was NOT enabled!",
                    scheduleAppointmentRadio.isEnabled());
            LOGGER.info("Confirmed that \"" + ConstantsDtc.INSTALL_WITH_APPOINTMENT + "\" option was enabled.");
        }
        LOGGER.info("assertAppointmentOption completed");
    }

    /**
     * Clicks the submit button on the edit shipping address page
     */
    public void submitUpdatedAddress(boolean handleVerifyAddressPopup) {
        LOGGER.info("submitUpdatedAddress started");
        commonActions.clickFormSubmitButtonByText(ConstantsDtc.CONTINUE_TO_SHIPPING);
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        if (handleVerifyAddressPopup)
            checkForVerifyAddressPopup();
        LOGGER.info("submitUpdatedAddress completed");
    }

    /**
     * Clicks the edit link for shipping address page
     */
    public void clickEditShippingDetails() {
        LOGGER.info("clickEditShippingDetails started");
        driver.clickElementWithLinkText(ConstantsDtc.EDIT_SHIPPING_DETAILS);
        LOGGER.info("clickEditShippingDetails completed");
    }

    /**
     * Selects the deliver method for shipping products
     *
     * @param deliveryMethod Type of delivery used in failure message
     * @param customer       Type of customer to pull dtc.data from and put into failure message
     */
    public void selectDeliveryMethod(String deliveryMethod, Customer customer) {
        LOGGER.info("selectDeliveryMethod started");
        if (driver.isElementDisplayed(CommonActions.addressVerificationBy, Constants.TWO)) {
            commonActions.clickFormSubmitButtonByText(ConstantsDtc.USE_USPS_CORRECTED_ADDRESS);
        }
        selectSeeMoreOptionsLink();
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        WebElement radioGroup = driver.getElementWithText(CommonActions.formGroupRadioBy,
                deliveryMethod, Constants.TEN);
        driver.setImplicitWait(Constants.FIVE, TimeUnit.SECONDS);
        WebElement deliveryMethodRadioButton = radioGroup.findElement(CommonActions.radioLabelBy);
        driver.jsScrollToElementClick(deliveryMethodRadioButton, false);
        commonActions.clickFormSubmitButtonByText(ConstantsDtc.CONTINUE_TO_PAYMENT);
        LOGGER.info("selectDeliveryMethod completed");
    }

    /**
     * Selects the default delivery method for shipping products
     *
     * @param customer Type of customer to pull dtc.data from and put into failure message
     */
    public void selectDefaultDeliveryMethod(Customer customer) {
        LOGGER.info("selectDefaultDeliveryMethod started");
        selectDeliveryMethod(GROUND_FREE, customer);
        LOGGER.info("selectDefaultDeliveryMethod completed");
    }

    /**
     * Enters all payment info based on customer type for cards other than CC1
     *
     * @param creditCard         Name of the credit card to use for payment
     * @param customer           Type of customer to pull data from and pass to payment fields
     * @param useShippingAddress Whether to use the current shipping address
     */
    public void enterPaymentInfo(String creditCard, Customer customer, boolean useShippingAddress) {
        enterPaymentInfo(creditCard, customer, useShippingAddress, true, true);
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD))
            driver.scenarioData.genericData.put(Constants.ORDER_TYPE, ConstantsDtc.MAIL_ORDER);
        else
            driver.scenarioData.genericData.put(Constants.ORDER_TYPE, ConstantsDtc.BOPIS);
    }

    /**
     * Enters all payment info based on customer type
     *
     * @param creditCard                Name of the credit card to use for payment
     * @param customer                  Type of customer to pull data from and pass to payment fields
     * @param useShippingAddress        Whether to use the current shipping address
     * @param consentToDisclosure       Whether or not to click 'I CONSENT' on the 'CONSENT TO DISCLOSURE' popup.
     *                                  Applies only to CC1 customer
     * @param agreeToTermsAndConditions Whether or not to click 'ACCEPT & AGREE' on the 'TERMS OF AGREEMENT' popup.
     *                                  Applies only to CC1 customer
     */
    public void enterPaymentInfo(String creditCard, Customer customer, boolean useShippingAddress,
                                 boolean consentToDisclosure, boolean agreeToTermsAndConditions) {
        LOGGER.info("enterPaymentInfo started for '" + customer.getCustomerDataString(customer) + "'" + customer);
        driver.waitForElementVisible(paymentTypesContainerBy);
        PaymentFields paymentFields = new PaymentFields().invokePaymentFields();
        WebElement ccName = paymentFields.getCcName();
        String expYear = setCustomerCreditCardExpirationYear(customer);
        String expMonth = getCustomerCreditCardExpirationMonth(customer);

        // Backspace over previous value. When using clear() function, it is retained and gets appended to new value.
        while (!ccName.getAttribute(Constants.VALUE).equals("")) {
            ccName.sendKeys(Keys.BACK_SPACE);
        }
        ccName.sendKeys(customer.firstName + " " + customer.lastName);
        if (Config.isAndroidPhone() || Config.isAndroidTablet()) {
            driver.waitForMilliseconds();
        }

        enterCreditCardNumberCvvCvnForCustomer(creditCard, customer, paymentFields);
        setPaymentType(creditCard);
        if (Config.isAndroidPhone() || Config.isAndroidTablet()) {
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        }

        selectCreditCardExpirationMonthYear(expMonth, expYear);
        driver.scenarioData.genericData.put(OrderXmls.EXPIRATION_DATE, expMonth + expYear);
        if (Config.isAndroidPhone() || Config.isAndroidTablet()) {
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        }

        enterBillingAddressForCustomer(customer, useShippingAddress, paymentFields);

        if (CommonUtils.containsIgnoreCase(creditCard, ConstantsDtc.CAR_CARE_ONE) &&
                CommonUtils.containsIgnoreCase(creditCard, ConstantsDtc.BOPIS)) {
            reviewConsentToDisclosureAndTermsOfAgreement(consentToDisclosure, agreeToTermsAndConditions);
        }
        if (!useShippingAddress) {
            setPaymentScenarioData(customer);
        }
        LOGGER.info("enterPaymentInfo completed for '" + customer.getCustomerDataString(customer) + "'" + customer);
    }

    /**
     * Gets the credit card expiration year for a specified customer
     *
     * @param customer Type of customer to pull dtc.data from and use as the credit card expiration year
     * @return String containing the customer's credit card expiration year
     */
    private String getCustomerCreditCardExpirationYear(Customer customer) {
        LOGGER.info("getCustomerCreditCardExpirationYear started");
        String expYear = customer.expDateYY;

        // Default if not defined for customer - Get year ahead of current year
        if (expYear == null) {
            expYear = Integer.toString(commonUtils.getExpYear(1));
            LOGGER.info("Customer was not found! Default value for expiration year = \"" + expYear + "\"");
        }

        LOGGER.info("getCustomerCreditCardExpirationYear completed");
        return expYear;
    }

    /**
     * Sets the credit card expiration year for a specified customer. If the customer is not found, returns a default
     * value of the current year + 1
     *
     * @param customer Type of customer from which to pull credit card data
     * @return Credit Card expiration year (2 digit format)
     */
    private String setCustomerCreditCardExpirationYear(Customer customer) {
        LOGGER.info("setCustomerCreditCardExpirationYear started");
        String expYear = getCustomerCreditCardExpirationYear(customer);

        if (expYear.length() == 4)
            expYear = expYear.substring(2);
        LOGGER.info("setCustomerCreditCardExpirationYear completed. Trimmed expiration year = \"" + expYear
                + "\"");
        return expYear;
    }

    /**
     * Gets the credit card expiration month for a specified customer
     *
     * @param customer Type of customer to pull dtc.data from and use as the credit card expiration month
     * @return String containing the customer's credit card expiration month
     */
    private String getCustomerCreditCardExpirationMonth(Customer customer) {
        String expMonth = customer.expDateMM;

        // Default if not defined for customer - Set month to 1
        if (expMonth == null) {
            expMonth = "1";
        }
        return expMonth;
    }

    /**
     * Sets the credit card info (# & CVN / CVV) for a specified customer
     *
     * @param creditCard Credit card with values used to update customer credit card fields
     * @param customer   Customer object with the credit card fields to be set
     */
    private void setCreditCardForCustomer(String creditCard, Customer customer) {
        LOGGER.info("setCreditCardForCustomer started");
        switch (creditCard) {
            case AMERICAN_EXPRESS:
                customer.ccNum = customer.ccNumAmEx;
                break;
            case DISCOVER:
                customer.ccNum = customer.ccNumDiscover;
                break;
            case VISA:
                customer.ccNum = customer.ccNumVisa;
                break;
            case VISA_BOPIS:
                customer.ccNum = customer.ccNumVisa2;
                customer.cvn = customer.cvn2;
                break;
            case ConstantsDtc.CAR_CARE_ONE:
                customer.ccNum = customer.ccNumCCO;
                break;
            case AMERICAN_EXPRESS_BOPIS:
                customer.ccNum = customer.ccNumAmEx_bopis;
                break;
            case MASTERCARD_BOPIS:
                customer.ccNum = customer.ccNumMaster_bopis;
                customer.cvn = customer.cvn2;
                break;
            case DISCOVER_BOPIS:
                customer.ccNum = customer.ccNumDiscover_bopis;
                customer.cvn = customer.cvn3;
                break;
            case CAR_CARE_ONE_BOPIS:
                customer.ccNum = customer.ccNumCCO_bopis;
                break;
            case CAR_CARE_ONE_2_BOPIS:
                customer.ccNum = customer.ccNumCCO_2_bopis;
                break;
            default:
                customer.ccNum = customer.ccNumVisa;
                customer.cvv = customer.cvv2;
                break;
        }
        driver.scenarioData.genericData.put(OrderXmls.CARD_TOKEN, customer.ccNum.substring(0, 6));
        LOGGER.info("setCreditCardForCustomer completed");
    }

    /**
     * Enters the credit card number and CVV / CVN for a specified customer
     *
     * @param creditCard    Credit card with the values to be used by the customer
     * @param customer      Customer using the values provided by the credit card
     * @param paymentFields The set of currently visible payment fields
     */
    private void enterCreditCardNumberCvvCvnForCustomer(String creditCard, Customer customer,
                                                        PaymentFields paymentFields) {
        LOGGER.info("enterCreditCardNumberCvvCvnForCustomer started");

        WebElement ccNumber = paymentFields.getCcNumber();
        WebElement ccCvn = paymentFields.getCcCvn();

        setCreditCardForCustomer(creditCard, customer);

        // Backspace over previous value. When using clear() function, it is retained and gets appended to new value.
        while (!ccNumber.getAttribute(Constants.VALUE).equals("")) {
            ccNumber.sendKeys(Keys.BACK_SPACE);
        }
        ccNumber.sendKeys(customer.ccNum);
        if (Config.isFirefox()) {
            driver.waitForMilliseconds();
            ccNumber.sendKeys(Keys.ENTER);
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        }

        driver.jsScrollToElement(ccCvn);
        // Backspace over previous value. When using clear() function, it is retained and gets appended to new value.
        while (!ccCvn.getAttribute(Constants.VALUE).equals("")) {
            ccCvn.sendKeys(Keys.BACK_SPACE);
        }
        if (CommonUtils.containsIgnoreCase(creditCard, ConstantsDtc.CAR_CARE_ONE)) {
            ccCvn.sendKeys(customer.cvv);
        } else {
            ccCvn.sendKeys(customer.cvn);
        }
        LOGGER.info("enterCreditCardNumberCvvCvnForCustomer completed");
    }

    /**
     * Selects the expiration month and year from the corresponding 'Payment Details' controls
     *
     * @param expMonth Credit card expiration month
     * @param expYear  Credit card expiration year
     */
    private void selectCreditCardExpirationMonthYear(String expMonth, String expYear) {
        LOGGER.info("selectCreditCardExpirationMonthYear started");
        showExpDateMenuItems(ConstantsDtc.CC_EXPIRE_MONTH);
        driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
        driver.clickElementWithExactText(CommonActions.spanTagNameBy, expMonth);
        driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
        showExpDateMenuItems(ConstantsDtc.CC_EXPIRE_YEAR);
        driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
        driver.clickElementWithExactText(CommonActions.spanTagNameBy, expYear);
        driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
        LOGGER.info("selectCreditCardExpirationMonthYear completed");
    }

    /**
     * Ensure the date menu pulldown menu items are showing
     *
     * @param dropDownLabel - The dropdown menu element label: "Exp. Month" or "Exp. Year".
     */
    private void showExpDateMenuItems(String dropDownLabel) {
        LOGGER.info("showExpDateMenuItems started for " + dropDownLabel);
        WebElement ccExpDateDropdown = driver.getElementWithText(CommonActions.formGroupBy, dropDownLabel);
        int counter1 = 0;
        boolean done = false;
        do {
            int counter2 = 0;
            driver.jsScrollToElementClick(ccExpDateDropdown, false);
            do {
                driver.waitForMilliseconds(Constants.ONE_HUNDRED);
                counter2++;
                if (driver.getDisplayedElement(CommonActions.reactDropdownMenuBy, Constants.ZERO) != null) {
                    done = true;
                }
            } while (!done && counter2 < Constants.TEN);
            counter1++;
        } while (!done && counter1 < Constants.FIVE);
        Assert.assertTrue("FAIL: The date pulldown menu items did not display for " + dropDownLabel, done);
        LOGGER.info("showExpDateMenuItems completed for " + dropDownLabel);
    }

    /**
     * Enters the billing address for a specified customer. Optionally, can opt to use the shipping address as the
     * billing address
     *
     * @param customer           Type of customer to pull data from and pass to address fields
     * @param useShippingAddress True if using the shipping address as the billing address, else false to enter a
     * @param paymentFields      The set of currently visible payment fields
     */
    private void enterBillingAddressForCustomer(Customer customer, boolean useShippingAddress,
                                                PaymentFields paymentFields) {
        LOGGER.info("enterBillingAddressForCustomer started");
        if (!useShippingAddress) {
            if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)) {
                driver.jsScrollToElement(billingSameAsShippingOption);
                driver.waitForMilliseconds();
                billingSameAsShippingOption.click();
                commonActions.enterAddressForCustomer(customer, false, false);
            } else {
                selectDeselectUseExistingAddressForBilling(Constants.DESELECT);
                paymentFields.invokePaymentFields();
                WebElement addressLine1 = paymentFields.getAddressLine1();
                WebElement billingZipCode = paymentFields.getBillingZipCode();
                if (driver.isElementDisplayed(addressLine1, Constants.ONE)) {
                    // Backspace over previous value. When using clear() function,
                    // it is retained and gets appended to new value.
                    while (!addressLine1.getAttribute(Constants.VALUE).equals("")) {
                        addressLine1.sendKeys(Keys.BACK_SPACE);
                    }
                    addressLine1.sendKeys(customer.address1);
                    WebElement countryDropdown = driver.getElementWithText(CommonActions.formGroupBy,
                            Constants.UNITED_STATES);
                    driver.jsScrollToElementClick(countryDropdown, false);
                    driver.clickElementWithText(CommonActions.spanTagNameBy, customer.country);
                    driver.jsScrollToElementClick(billingZipCode, false);
                    commonActions.clearAndPopulateEditField(billingZipCode, customer.zip);
                    do {
                        driver.jsScrollToElementClick(billingZipCode, false);
                        checkoutOrderSummary.click();
                        driver.waitForMilliseconds();
                    } while (commonActions.formGroupErrorMessageDisplayedNotDisplayed(ZIP_CODE_IS_INVALID, true));
                }
            }
        }
        LOGGER.info("enterBillingAddressForCustomer completed");
    }

    /**
     * Clicks the 'Place Order' button
     *
     * @param customer Type of customer to pull dtc.data from and use in failure message
     */
    public void placeOrder(Customer customer) {
        LOGGER.info("placeOrder started");
        driver.waitOneSecond();
        commonActions.clickFormSubmitButtonByText(ConstantsDtc.PLACE_ORDER);
        // This wait time is necessary because navigation may return to payment page for negative testing
        driver.waitSeconds(Constants.FIVE);
        LOGGER.info("placeOrder completed for '" + customer.getCustomerDataString(customer) + "'");
    }

    /***
     * Clicks the 'See more options' link in the Delivery Methods section if available
     */
    public void selectSeeMoreOptionsLink() {
        LOGGER.info("selectSeeMoreOptionsLink started");
        if (driver.isElementDisplayed(seeMoreOptionsLink, Constants.TWO)) {
            driver.jsScrollToElementClick(seeMoreOptionsLink, false);
            driver.waitForPageToLoad();
        }
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        LOGGER.info("selectSeeMoreOptionsLink completed");
    }

    /***
     * Confirms the available shipping options in the Delivery Methods section match the expected values
     * @param expectedOptions String comprised of expected shipping options separated by a comma
     */
    public void confirmAvailableShippingOptions(String expectedOptions) {
        LOGGER.info("confirmAvailableShippingOptions started with options: \"" + expectedOptions + "\"");
        selectSeeMoreOptionsLink();
        driver.waitForMilliseconds();
        List<String> optionsToConfirm = Lists.newArrayList(Splitter.on(",").trimResults().split(expectedOptions));
        for (String option : optionsToConfirm) {
            Assert.assertTrue("FAIL: Could not find Delivery Method: \"" + option + "\"!",
                    driver.isElementDisplayed(driver.getElementWithText(CommonActions.radioLabelBy, option)));
        }
        LOGGER.info("confirmAvailableShippingOptions completed with options: \"" + expectedOptions + "\"");
    }

    /***
     * Verifies a specified delivery option is not present for selection
     *
     * @param deliveryOption String - delivery option that should not be present
     */
    public void assertOptionNotInDeliveryMethod(String deliveryOption) {
        LOGGER.info("assertOptionNotInDeliveryMethod started");
        selectSeeMoreOptionsLink();
        Assert.assertFalse("FAIL: Delivery Method section DID contain option: " + deliveryOption,
                deliverySection.getText().contains(deliveryOption));
        LOGGER.info("assertOptionNotInDeliveryMethod completed");
    }

    /***
     * Verifies the "Shipping Address" and "Payment &#38; Billing Address" header sections are unable to be edited when
     * their dtc.data comes from Paypal. Verification is based on a check for the presence of an "Edit" link in the
     * headers specified.
     */
    public void verifySectionsWithPaypalInfoNotEditable() {
        LOGGER.info("verifySectionsWithPaypalInfoNotEditable started");
        driver.waitForElementClickable(placeOrderButton);
        driver.setImplicitWait(Constants.FIVE, TimeUnit.SECONDS);
        List<WebElement> checkoutSectionsHeaderList = webDriver.findElements(sectionHeadersBy);
        if (checkoutSectionsHeaderList.size() != 0) {
            for (WebElement checkoutSection : checkoutSectionsHeaderList) {
                if (checkoutSection.getText().toLowerCase().contains(SHIPPING_ADDRESS.toLowerCase()) ||
                        checkoutSection.getText().toLowerCase().contains(DELIVERY_METHOD.toLowerCase()) ||
                        checkoutSection.getText().toLowerCase().contains(PAYMENT_BILLING_ADDRESS.toLowerCase())) {
                    Assert.assertTrue("FAIL: One of the Checkout headers for a section containing Paypal " +
                                    "info also contained an \"Edit\" link!",
                            checkoutSection.findElements(CommonActions.anchorTagBy).size() == 0);
                }
            }
        } else {
            Assert.fail("FAIL: No Checkout section headers were found!");
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("verifySectionsWithPaypalInfoNotEditable completed");
    }

    /***
     * Verifies the specified Checkout section can be edited. Verification based on the presence of an "Edit" link in
     * the section header
     * @param editableSection Section header that should contain a link
     */
    public void verifySectionIsEditable(String editableSection) {
        LOGGER.info("verifySectionIsEditable started");
        driver.waitForElementClickable(placeOrderButton);
        List<WebElement> checkoutSectionsHeaderList = webDriver.findElements(sectionHeadersBy);
        if (checkoutSectionsHeaderList.size() != 0) {
            for (WebElement checkoutSection : checkoutSectionsHeaderList) {
                if (checkoutSection.getText().toLowerCase().contains(editableSection.toLowerCase())) {
                    Assert.assertTrue("FAIL: Checkout section \"" + editableSection + "\" did not contain an " +
                            "\"Edit\" link", checkoutSection.findElement(CommonActions.anchorTagBy).isDisplayed());
                }
            }
        } else {
            Assert.fail("FAIL: Section headers were NOT found. Unable to verify \"" + editableSection
                    + "\" section is editable!");
        }
        LOGGER.info("verifySectionIsEditable completed");
    }

    /***
     * Clicks the "Edit" link for a specified Checkout section
     * @param sectionToEdit Section of the Checkout page to be edited/modified
     */
    public void clickEditLinkForSection(String sectionToEdit) {
        LOGGER.info("clickEditLinkForSection started");
        driver.waitForElementClickable(placeOrderButton);
        List<WebElement> checkoutSectionsHeaderList = webDriver.findElements(sectionHeadersBy);
        if (checkoutSectionsHeaderList.size() != 0) {
            for (WebElement checkoutSection : checkoutSectionsHeaderList) {
                if (checkoutSection.getText().toLowerCase().contains(sectionToEdit.toLowerCase())) {
                    WebElement sectionEditLinkEle = checkoutSection.findElement(CommonActions.anchorTagBy);
                    //TODO: Clicking "Edit" link doesn't register on mobile; Troubleshoot when possible
                    driver.jsScrollToElement(sectionEditLinkEle);
                    sectionEditLinkEle.click();
                    break;
                }
            }
        } else {
            Assert.fail("FAIL: Section headers were NOT found. Unable to click \"Edit\" link for \""
                    + sectionToEdit + "\"!");
        }
        LOGGER.info("clickEditLinkForSection completed");
    }

    /***
     * Checks for and handles the "Verify your Address" pop-up when attempting to move from the shipping
     * address to the delivery method section OR when entering a billing address different from the shipping address.
     */
    private void checkForVerifyAddressPopup() {
        LOGGER.info("checkForVerifyAddressPopup started");
        driver.waitForPageToLoad();
        if (driver.isElementDisplayed(CommonActions.addressVerificationBy, Constants.FIVE)) {
            commonActions.clickFormSubmitButtonByText(ConstantsDtc.USE_USPS_CORRECTED_ADDRESS);
            driver.pollUntil(VERIFY_ADDRESS_POPUP_CLOSED_JS, Constants.FIVE);
        } else {
            LOGGER.info("\"Verify your address\" popup was NOT displayed");
        }
        LOGGER.info("checkForVerifyAddressPopup completed");
    }

    /***
     * Performs the action to either accept or decline the credit card disclosure consent
     * @param action String Action to be taken (Accept/Decline)
     */
    public void creditCardDisclosureModalAction(String action) {
        LOGGER.info("creditCardDisclosureModalAction started ");
        driver.waitForMilliseconds();
        driver.clickElementWithText(CommonActions.careCareOneReviewLabel,
                Constants.REVIEW_CONSENT_DISCLOSURE_AND_TERMS_OF_AGREEMENT);
        driver.waitForElementVisible(CommonActions.dtModalContainerBy);
        if (action.contains(ACCEPT)) {
            acceptConsentButton.click();
        } else if (action.contains(DECLINE)) {
            declineConsentButton.click();
        }
        LOGGER.info("Credit Card Disclosure Consent " + action + " successfully ");
        LOGGER.info("creditCardDisclosureModalAction completed ");
    }

    /***
     * Performs the action to either accept or decline the credit card terms of agreement
     * @param action String Action to be taken (Accept/Decline)
     */
    public void ccTermsOfAgreementAction(String action) {
        LOGGER.info("ccTermsOfAgreementAction started ");
        driver.waitForElementVisible(CommonActions.dtModalContainerBy, Constants.TWO);
        if (action.contains(ACCEPT)) {
            toaAcceptAndSubmitButton.click();
        } else if (action.contains(DECLINE)) {
            declineConsentButton.click();
        }
        LOGGER.info("Credit Card Terms of Agreement " + action + " successfully ");
        LOGGER.info("ccTermsOfAgreementAction completed ");
    }

    /**
     * Verifies the Checkout and Cart subtotal amounts match
     */
    public void assertCheckoutAndCartSubtotalMatch() {
        LOGGER.info("assertCheckoutAndCartSubtotalMatch started");
        driver.waitForElementVisible(checkoutSubtotal);
        Assert.assertTrue("FAIL: Expected checkout subtotal of:\""
                        + CommonActionsSteps.checkoutSubtotal + "\" to match the cart subtotal:\""
                        + CommonActionsSteps.cartSubtotal + "\"!",
                CommonActionsSteps.checkoutSubtotal == CommonActionsSteps.cartSubtotal);
        LOGGER.info("assertCheckoutAndCartSubtotalMatch completed");
    }

    /**
     * Verifies the specified fee or service appears as a line item in the cart summary of the Checkout page
     *
     * @param itemType String representing the itemType of either "fee" or "service"
     * @param itemName String representing text value expected to be present in the cart summary line item
     */
    public void assertItemPresentInCartSummary(String itemType, String itemName) {
        LOGGER.info("assertItemPresentInCartSummary started");
        WebElement itemEle = null;

        if (itemType.equalsIgnoreCase(Constants.FEE)) {
            itemEle = feeCartSummaryItem;
        } else if (itemType.equalsIgnoreCase(Constants.SERVICE)) {
            itemEle = serviceCartSummaryItem;
        } else {
            Assert.fail("FAIL: itemType of:\"" + itemType + "\" was NOT recognized!");
        }

        Assert.assertTrue("FAIL: Did NOT find a \"" + itemType
                        + "\" line item containing the expected text:\"" + itemName + "\"!",
                driver.checkIfElementContainsText(itemEle, itemName));
        LOGGER.info("assertItemPresentInCartSummary completed");
    }

    /**
     * Select the install without appointment option based on the type of reason passed in
     *
     * @param reasonText type of installment reason according to scenario
     */
    public void selectCheckoutWithoutInstallReason(String reasonText) {
        LOGGER.info("selectCheckoutWithoutInstallReason started");
        driver.waitForMilliseconds();
        driver.jsScrollToElementClick(installWithoutApptBtn);

        driver.waitForElementClickable(withoutApptReasonToggleBtn);
        driver.jsScrollToElement(withoutApptReasonToggleBtn);
        withoutApptReasonToggleBtn.click();

        driver.waitForMilliseconds();
        if (reasonText.equalsIgnoreCase(ConstantsDtc.DEFAULT))
            driver.clickElementWithText(CommonActions.spanTagNameBy, ConstantsDtc.APPT_NOT_SURE_OF_AVAILABILITY);
        else {
            driver.clickElementWithText(CommonActions.spanTagNameBy, reasonText);
            while (!webDriver.findElement(CommonActions.sortByOptionBy).getText().equals(reasonText)) {
                driver.waitForMilliseconds();
            }
        }
        LOGGER.info("selectCheckoutWithoutInstallReason ended");
    }

    /**
     * Select the install with appointment option
     */
    private void selectInstallWithAppointment() {
        LOGGER.info("selectInstallWithAppointment started");
        driver.waitForElementClickable(installWithApptBtn);
        installWithApptBtn.click();
        driver.waitForPageToLoad();
        LOGGER.info("selectInstallWithAppointment completed");
    }

    /**
     * Selects the Checkout install type (Appointment Details section) either with or without an appointment
     * NOTE: Default assumes checkout w/o appointment and passes in the default option to be selected
     *
     * @param installType type of checkout install i.e. with appointment OR without appointment
     */
    public void selectCheckoutInstallType(String installType) {
        if (installType.equalsIgnoreCase(ConstantsDtc.DEFAULT) ||
                installType.equalsIgnoreCase(ConstantsDtc.WITHOUT_APPOINTMENT)) {
            selectCheckoutWithoutInstallReason(ConstantsDtc.APPT_NOT_SURE_OF_AVAILABILITY);
        } else if ((installType.equalsIgnoreCase(ConstantsDtc.WITH_APPOINTMENT))) {
            selectInstallWithAppointment();
        }
        LOGGER.info("selectCheckoutInstallType ended");
    }

    /**
     * Clicks the payment radio button specified (credit or paypal)
     *
     * @param paymentType Type of payment (credit or paypal)
     */
    public void clickPaymentType(String paymentType) {
        LOGGER.info("clickPaymentType started");
        WebElement paymentTypeRadioBtn;

        if (!Config.isSafari()) {
            driver.waitForPageToLoad();
        } else {
            driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        }
        driver.waitForElementVisible(paymentTypesContainerBy);

        paymentTypeRadioBtn = driver.getElementWithText(CommonActions.radioLabelBy, paymentType);
        driver.jsScrollToElement(paymentTypeRadioBtn);
        paymentTypeRadioBtn.click();
        LOGGER.info("clickPaymentType ended");
    }

    /**
     * Waits for and selects the "Continue to Paypal" button
     */
    public void continueToPayPalCheckout() {
        LOGGER.info("continueToPayPalCheckout started");
        driver.waitForElementVisible(CommonActions.radioLabelBy);
        commonActions.clickFormSubmitButtonByText(ConstantsDtc.CONTINUE_TO_PAYPAL);
        LOGGER.info("continueToPayPalCheckout ended");
    }

    /**
     * Selects the first available date and then time in order to create an appointment from the Checkout page
     */
    public void selectFirstAvailableApptDateTime() {
        LOGGER.info("selectFirstAvailableApptDateTime started");
        driver.waitForElementVisible(apptDateContainer);
        List<WebElement> availableApptDateList = getAvailableApptDatesForCheckout();
        Assert.assertTrue("FAIL: None of the listed dates were active / had available appointment time slots!",
                availableApptDateList.size() > 0);
        boolean success = false;
        for (WebElement apptDate : availableApptDateList) {
            driver.jsScrollToElementClick(apptDate);
            if (selectFirstAvailableApptTime()) {
                success = true;
                break;
            }
        }
        driver.waitForMilliseconds();
        Assert.assertTrue("FAIL: Unable to select appointment time", success);
        LOGGER.info("selectFirstAvailableApptDateTime completed");
    }

    /**
     * Selects the last available date and then time in order to create an appointment from the Checkout page
     */
    public void selectLastAvailableApptDateTime() {
        List<WebElement> availableApptDateList = getAvailableApptDatesForCheckout();
        Assert.assertTrue("FAIL: None of the listed dates were active / had available appointment time slots!",
                availableApptDateList.size() > 0);
        driver.jsScrollToElementClick(availableApptDateList.get(availableApptDateList.size() - 1));
        selectFirstAvailableApptTime();
        Assert.assertTrue("FAIL: Unable to select a time slot for the first or last appointment day!",
                selectLastAvailableApptTime());
    }

    /**
     * Enters (non-DTD) customer information on the Checkout page
     *
     * @param customer Customer type to grab dtc.data from
     */
    public void enterCustomerInformationForCheckout(Customer customer) {
        LOGGER.info("enterCustomerInformationForCheckout started");
        By phoneDropdownFieldBy = CommonActions.dropdownBy;
        WebElement phoneDropdown;

        driver.waitForElementClickable(CommonActions.firstName, Constants.FIVE);
        try {
            CommonActions.firstName.sendKeys(customer.firstName);
            CommonActions.lastName.sendKeys(customer.lastName);
            CommonActions.email.sendKeys(customer.email);

            phoneDropdown = driver.getElementWithText(CommonActions.formGroupBy, ConstantsDtc.PHONE_TYPE);

            if (Config.isMobile()) {
                phoneDropdown = driver.getParentElement(phoneDropdown).findElement(phoneDropdownFieldBy);
            }

            driver.jsScrollToElement(phoneDropdown);
            phoneDropdown.click();
            driver.clickElementWithText(CommonActions.spanTagNameBy, customer.phoneType);
            driver.jsScrollToElement(CommonActions.phone);
            CommonActions.phone.sendKeys(customer.phone.replaceAll("[^\\d.]", ""));
            commonActions.clickBrowserBody();
            driver.waitForMilliseconds();
        } catch (Exception e) {
            Assert.fail("FAIL: Entering customer info for user " +
                    customer.getCustomerDataString(customer) + "! FAILED with error: " + e);
        }
        LOGGER.info("enterCustomerInformationForCheckout completed");
    }

    /**
     * Selects / expands the Cart items section in the Order Summary on the Checkout page
     */
    public void expandCartSummaryCartItems() {
        LOGGER.info("expandCartSummaryCartItems started");
        List<WebElement> toggleIcons;
        if (CommonActions.packageFlow)
            toggleIcons = webDriver.findElements(CommonActions.cartTireAndWheelPackageHeaderBy);
        else
            toggleIcons = webDriver.findElements(CommonActions.cartItemsHeaderIconBy);

        for (WebElement toggleIcon : toggleIcons) {
            if (toggleIcon.getText().contains(Constants.PLUS_SIGN)) {
                driver.webElementClick(toggleIcon);
            }
        }
        LOGGER.info("expandCartSummaryCartItems completed");
    }

    /**
     * Selects / expands the "show fee details" section for an item in the Order Summary on the Checkout page
     * Could be improved to select the section based on a specified item name, id, position, etc.
     */
    public void expandFeeDetailsForCartItem() {
        LOGGER.info("expandFeeDetailsForCartItem started");
        WebElement showRequiredFeesToggleIcon = CommonActions.showRequiredFeesToggleIconStandard;
        WebElement showAddOnsToggleIcon = CommonActions.showAddOnsToggleIconStandard;
        if (driver.scenarioData.isStaggeredProduct()) {
            showRequiredFeesToggleIcon = CommonActions.showRequiredFeesToggleIconStaggered;
            showAddOnsToggleIcon = CommonActions.showAddOnsToggleIconStaggered;
        }
        if (driver.isElementDisplayed(showRequiredFeesToggleIcon, Constants.ZERO))
            driver.jsScrollToElementClick(showRequiredFeesToggleIcon);
        if (driver.isElementDisplayed(showAddOnsToggleIcon, Constants.ZERO))
            driver.jsScrollToElementClick(showAddOnsToggleIcon);
        LOGGER.info("expandFeeDetailsForCartItem completed");
    }

    /**
     * Selects the first or last available date and then the first available time on those dates in order to create an
     * appointment from the Checkout page. Will fail if there are no time slots available on the first or last available
     * date
     *
     * @param selectFirstDate True for the first available day, False for the last available day
     */
    public void selectFirstOrLastAvailableApptDateTime(boolean selectFirstDate) {
        LOGGER.info("selectFirstOrLastAvailableApptDateTime started");
        driver.waitOneSecond();
        driver.waitForElementVisible(apptDateContainer);
        boolean dateTimeSelected;
        do {
            dateTimeSelected = false;
            if (selectFirstDate) {
                selectFirstAvailableApptDateTime();
            } else {
                selectLastAvailableApptDateTime();
            }
            if (driver.isElementDisplayed(CommonActions.apptSelectedMsg)) {
                if (CommonActions.apptSelectedMsg.getText().toLowerCase().contains(Constants.NO_LONGER.toLowerCase())) {
                    webDriver.navigate().refresh();
                    driver.waitForPageToLoad();
                } else {
                    dateTimeSelected = true;
                }
            }
        } while (!dateTimeSelected);
        // give time for screen to update so data can be pulled
        driver.waitForMilliseconds();
        LOGGER.info("selectFirstOrLastAvailableApptDateTime completed");
    }

    /**
     * On mobile screen size, selects the first or last available date and then the first available time on those dates
     * in order to create an appointment from the Checkout page. Will fail if there are no time slots available on the
     * first or last available date
     *
     * @param selectFirstDate True for the first available day, False for the last available day
     */
    public void selectFirstOrLastAvailableApptDateTimeMobile(boolean selectFirstDate) {
        LOGGER.info("selectFirstOrLastAvailableApptDateTimeMobile started");
        boolean found = true;
        WebElement baseArrowControl = prevDayArrowControl;
        WebElement iteratorArrowControl = nextDayArrowControl;

        if (!selectFirstDate) {
            baseArrowControl = nextDayArrowControl;
            iteratorArrowControl = prevDayArrowControl;
        }

        while (driver.isElementDisplayed(baseArrowControl))
            driver.moveToElementClick(baseArrowControl);
        while (!driver.isElementDisplayed(firstAvailableAppointmentTimeMessage)) {
            if (driver.isElementDisplayed(iteratorArrowControl))
                driver.moveToElementClick(iteratorArrowControl);
            else
                found = false;
        }

        Assert.assertTrue("FAIL: There are no available appointments", found);

        selectFirstAvailableApptTime();

        // give time for screen to update so data can be pulled
        driver.waitForMilliseconds();
        LOGGER.info("selectFirstOrLastAvailableApptDateTimeMobile completed");
    }

    /**
     * Selects the first available appointment time slot
     *
     * @return True if a time was selected, False if no time was selected
     */
    private boolean selectFirstAvailableApptTime() {
        LOGGER.info("selectFirstAvailableApptTime started");
        boolean returnVal = false;
        if (driver.isElementDisplayed(CommonActions.availableApptTimesBy, Constants.THREE)) {
            driver.jsScrollToElementClick(webDriver.findElement(CommonActions.availableApptTimesBy));
            returnVal = true;
        }
        LOGGER.info("selectFirstAvailableApptTime completed w/ " + String.valueOf(returnVal));
        return returnVal;
    }

    /**
     * Selects the last available appointment time slot
     *
     * @return True if a time was selected, False if no time was selected
     */
    private boolean selectLastAvailableApptTime() {
        LOGGER.info("selectFirstAvailableApptTime started");
        boolean returnVal = false;
        if (driver.isElementDisplayed(CommonActions.availableApptTimesBy, Constants.ONE)) {
            List<WebElement> appointmentTimes = webDriver.findElements(CommonActions.availableApptTimesBy);
            driver.jsScrollToElementClick(appointmentTimes.get(appointmentTimes.size() - 1));
            returnVal = true;
        }
        LOGGER.info("selectFirstAvailableApptTime completed w/ " + String.valueOf(returnVal));
        return returnVal;
    }

    /**
     * Retrieves all the available appointment times for a given date on the Checkout page
     *
     * @return Group of elements representing the available appointment times
     */
    private List<WebElement> getAvailableApptTimesForDateCheckout() {
        LOGGER.info("getAvailableApptTimesForDateCheckout started");
        return webDriver.findElements(CommonActions.availableApptTimesBy);
    }

    /**
     * Retrieves all appointment times for a given date on the Checkout page
     *
     * @return Group of elements representing the appointment times
     */
    private List<WebElement> getAllApptTimesForDateCheckout() {
        LOGGER.info("getAllApptTimesForDateCheckout started");
        List<WebElement> appointmentTimeFields = webDriver.findElements(allApptTimesBy);
        List<WebElement> appointmentTimes = new ArrayList<>();
        for (WebElement appointmentTimeField : appointmentTimeFields) {
            if (!appointmentTimeField.getText().isEmpty()) {
                appointmentTimes.add(appointmentTimeField);
            }
        }
        LOGGER.info("getAllApptTimesForDateCheckout completed");
        return appointmentTimes;
    }

    /**
     * Retrieves all the available appointment dates
     *
     * @return Group of elements representing the available appointment dates
     */
    private List<WebElement> getAvailableApptDatesForCheckout() {
        LOGGER.info("getAvailableApptDatesForCheckout started");
        List<WebElement> appointmentDays = webDriver.findElements(CommonActions.apptDetailsSelectDateActiveBy);
        List<WebElement> availableAppointmentDays = new ArrayList<WebElement>();

        for (WebElement day : appointmentDays) {
            String dateText = day.getText();
            if (CommonUtils.containsIgnoreCase(dateText, VIEW_TIMES) ||
                    CommonUtils.containsIgnoreCase(dateText, SPECIAL_HOURS)) {
                availableAppointmentDays.add(day);
            }
        }
        LOGGER.info("getAvailableApptDatesForCheckout completed");
        return availableAppointmentDays;
    }

    /**
     * Retrieves all the unAvailable appointment dates
     *
     * @return Group of elements representing the unAvailable appointment dates
     */
    private List<WebElement> getUnavailableApptDatesForCheckout() {
        LOGGER.info("getUnavailableApptDatesForCheckout started");
        List<WebElement> allApptDatesList = webDriver.findElements(allAvailableApptDatesBy);
        List<WebElement> unavailableApptDatesList = new ArrayList<>();

        for (WebElement apptDate : allApptDatesList) {
            if (!apptDate.getAttribute(Constants.CLASS).contains(ACTIVE_APPOINTMENT_DATE_CLASS_TEXT))
                unavailableApptDatesList.add(apptDate);
        }
        LOGGER.info("getUnavailableApptDatesForCheckout completed");
        return unavailableApptDatesList;
    }

    /**
     * Verifies that the total number of appointment days displayed to the user for a checkout with an item in the cart
     * equals 10 business days
     */
    public void verifyTotalNumberOfApptDaysForCheckoutWithItem() {
        LOGGER.info("verifyTotalNumberOfApptDaysForCheckoutWithItem started");
        int expectedNumberOfDays = REGULAR_APPOINTMENT_DATE_RANGE;
        if (webDriver.findElement(CommonActions.headerBy).getText().equalsIgnoreCase(ConstantsDtc.SERVICE_APPOINTMENT)) {
            expectedNumberOfDays = SERVICE_APPOINTMENT_DATE_RANGE;
        }
        driver.waitForElementVisible(apptDateContainer);
        List<WebElement> apptDateDayList = webDriver.findElements(CommonActions.apptDatesDayBy);
        Assert.assertTrue("FAIL: The total number of business days displayed was '"
                + apptDateDayList.size() + "' but the expectation was '" + expectedNumberOfDays + "'!",
                apptDateDayList.size() == expectedNumberOfDays);
        LOGGER.info("verifyTotalNumberOfApptDaysForCheckoutWithItem completed");
    }

    /**
     * Waits for 'Customer Information' section to load on page
     */
    public void assertCustomerInfoPageLoaded() {
        LOGGER.info("assertCustomerInfoPageLoaded started");
        driver.waitForElementVisible(customerInfoField);
        LOGGER.info("assertCustomerInfoPageLoaded completed - Customer Information Form Displayed!");
    }

    /**
     * Verifies the tooltip is displayed for Install without appointment
     */
    public void assertInstallWithoutApptToolTip() {
        LOGGER.info("assertInstallWithoutApptToolTip started");
        driver.waitForPageToLoad();
        Assert.assertTrue("The Install without appointment tooltip was not displayed",
                driver.isElementDisplayed(tooltipBtnBy));
        LOGGER.info("assertInstallWithoutApptToolTip completed");
    }

    /**
     * Verifies valid tooltip message displays when clicked on
     */
    public void assertInstallWithoutApptToolTipMessage() {
        LOGGER.info("assertInstallWithoutApptToolTipMessage started");
        driver.waitForPageToLoad();
        driver.jsScrollToElementClick(webDriver.findElement(tooltipBtnBy), false);
        driver.waitForMilliseconds();
        String contentText = CommonActions.tooltipContent.getText();
        Assert.assertTrue("The tooltip message did not display with the text:"
                        + "  'If you don't want these items installed, contact your selected store.'",
                contentText.matches("If you don.*t want these items installed, contact your selected store."));
        LOGGER.info("assertInstallWithoutApptToolTipMessage completed");
    }

    /**
     * Retrieves all the unavailable appointment times for a given date on the Checkout page
     *
     * @return Group of elements representing the unavailable appointment times
     */
    private List<WebElement> getUnavailableApptTimesForDateCheckout() {
        LOGGER.info("getUnavailableApptTimesForDateCheckout started");
        List<WebElement> apptTimeList = getAllApptTimesForDateCheckout();
        List<WebElement> unAvailTimeList = new ArrayList<>();

        for (WebElement apptTime : apptTimeList) {
            if (!apptTime.getText().isEmpty() && !apptTime.getAttribute(Constants.CLASS).
                    contains(AVAILABLE_TIMES_CLASS_TEXT))
                unAvailTimeList.add(apptTime);
        }
        LOGGER.info("getUnavailableApptTimesForDateCheckout completed");
        return unAvailTimeList;
    }

    /**
     * Verifies that unavailable time slots are disabled
     */
    public void assertUnavailableTimeSlotsAreDisabled() {
        LOGGER.info("assertUnavailableTimeSlotsAreDisabled started");
        List<WebElement> availableApptDateList = getAvailableApptDatesForCheckout();
        driver.waitForElementVisible(apptDateContainer);

        if (availableApptDateList.size() > 0) {
            for (WebElement apptDate : availableApptDateList) {
                driver.jsScrollToElement(apptDate);
                apptDate.click();
                String date = apptDate.getText().replace("\n", " ").replace(VIEW_TIMES, "").trim();
                LOGGER.info("Selected Appointment date:  " + date);

                List<WebElement> unAvailApptTimeList = getUnavailableApptTimesForDateCheckout();
                if (unAvailApptTimeList.size() == 0) {
                    LOGGER.info("No appointment dates found with unavailable time slots");
                    break;
                }

                int counter = 0;
                int maxTimeSlotChecks = 5;
                for (WebElement apptTime : unAvailApptTimeList) {
                    if ((apptTime.getText().equals("8:00") || apptTime.getText().equals("8:30")) &&
                            (driver.isElementDisplayed(CommonActions.apptSelectedMsg, Constants.ZERO))) {
                        appointmentPage.closeAppointmentSelectedMessageBar();
                    }
                    apptTime.click();
                    // It takes some time for button to become enabled when available time slots are clicked.
                    // For test validity, include wait time even when clicking on unavailable time slots.
                    driver.waitSeconds(Constants.TWO);
                    WebElement continueToCustDetailsButton = driver.getElementWithText(
                            CommonActions.formSubmitButtonBy, ConstantsDtc.CONTINUE_TO_CUSTOMER_DETAILS);
                    String time = apptTime.getText();
                    boolean enabled = continueToCustDetailsButton.isEnabled();
                    Assert.assertTrue("The " + ConstantsDtc.CONTINUE_TO_CUSTOMER_DETAILS + " button was not enabled " +
                            "when clicking on unavailable time slot:  " + time, !enabled);
                    LOGGER.info("Appt time:  " + time + ". " + ConstantsDtc.CONTINUE_TO_CUSTOMER_DETAILS +
                            " button enabled = " + enabled);
                    counter++;
                    if (counter == maxTimeSlotChecks) {
                        break;
                    }
                }
            }
        } else {
            Assert.fail("FAIL: None of the listed dates were active");
        }
        LOGGER.info("assertUnavailableTimeSlotsAreDisabled completed");
    }

    /**
     * Verifies that available time slots are enabled
     */
    public void assertAvailableTimeSlotsAreEnabled() {
        LOGGER.info("assertAvailableTimeSlotsAreEnabled started");
        List<WebElement> availableApptDateList = getAvailableApptDatesForCheckout();
        driver.waitForElementVisible(apptDateContainer);
        boolean found = false;

        if (availableApptDateList.size() > 0) {
            for (WebElement apptDate : availableApptDateList) {
                apptDate.click();
                String date = apptDate.getText().replace("\n", " ").replace(VIEW_TIMES, "").trim();
                LOGGER.info("Selected Appointment date:  " + date);

                List<WebElement> availApptTimeList = getAvailableApptTimesForDateCheckout();
                if (availApptTimeList.size() > 0) {
                    for (WebElement apptTime : availApptTimeList) {
                        if ((apptTime.getText().equals("8:00") || apptTime.getText().equals("8:30")) &&
                                (driver.isElementDisplayed(CommonActions.apptSelectedMsg))) {
                            appointmentPage.closeAppointmentSelectedMessageBar();
                        }
                        int counter = 0;
                        boolean enabled = false;
                        String time = "";
                        do {
                            driver.jsScrollToElementClick(apptTime);
                            driver.waitForMilliseconds();
                            enabled = driver.getElementWithText(CommonActions.formSubmitButtonBy,
                                    ConstantsDtc.CONTINUE_TO_CUSTOMER_DETAILS).isEnabled();
                            time = apptTime.getText();
                            counter++;
                        } while (!enabled && counter < Constants.FIVE);


                        Assert.assertTrue("The 'CONTINUE TO CUSTOMER DETAILS' button not enabled when "
                                + "clicking on available time slot:  " + time, enabled);

                        LOGGER.info("Appt time:  " + apptTime.getText() + ".  Button enabled = " + enabled);
                    }

                    found = true;
                    break;
                }
            }

            if (!found) {
                Assert.fail("FAIL: No appointment dates found with available time slots");
            }
        } else {
            Assert.fail("FAIL: None of the listed dates were active");
        }
        LOGGER.info("assertAvailableTimeSlotsAreEnabled completed");
    }

    /**
     * Verifies the time slots are ordered in ascending order from top to bottom
     */
    public void assertTimeSlotsAscendingOrderTopToBottom() {
        LOGGER.info("assertTimeSlotsAscendingOrderTopToBottom started");
        SimpleDateFormat stdTime = new SimpleDateFormat("hh:mm:ss aa");
        List<WebElement> availableApptDateList = getAvailableApptDatesForCheckout();
        driver.waitForElementVisible(apptDateContainer);

        if (availableApptDateList.size() == 0) {
            Assert.fail("FAIL: None of the listed dates were active");
        }

        WebElement apptDate = availableApptDateList.get(0);
        apptDate.click();

        List<WebElement> apptTimeList = getAllApptTimesForDateCheckout();
        String curTime = null;
        String prevTime = null;
        Date curDateTime = null;
        Date prevDateTime = null;

        for (WebElement apptTime : apptTimeList) {
            String AMPM = Constants.DAY_AM;

            curTime = apptTime.getText();
            String hh = curTime.split(":")[0];
            String mm = curTime.split(":")[1];

            if (Integer.parseInt(hh) <= 5 || Integer.parseInt(hh) == 12)
                AMPM = Constants.DAY_PM;

            curTime = hh + ":" + mm + ":00 " + AMPM;

            if (prevTime != null) {
                try {
                    curDateTime = stdTime.parse(curTime);
                    prevDateTime = stdTime.parse(prevTime);
                } catch (ParseException e) {
                    e.printStackTrace();
                }

                Assert.assertTrue("The time " + curDateTime +
                                " is not greater than the previous time " + prevDateTime,
                        curDateTime.after(prevDateTime));
            }

            prevTime = curTime;
        }
        LOGGER.info("assertTimeSlotsAscendingOrderTopToBottom completed");
    }

    /**
     * Verifies user can scroll from top to bottom and from bottom to top of appointment list
     */
    public void assertScrollableTimeSlotList() {
        LOGGER.info("assertScrollableTimeSlotList started");
        LOGGER.info("assertAppointmentMessageBar started");
        List<WebElement> availableApptDateList = getAvailableApptDatesForCheckout();

        if (availableApptDateList.size() == 0) {
            Assert.fail("FAIL: None of the listed dates were active");
        }
        for (int i = 0; i < availableApptDateList.size() - 1; i++) {
            String message = availableApptDateList.get(i).getText();
            if (message.contains(VIEW_TIMES) &&
                    !driver.isElementDisplayed(AppointmentPage.storePartialDayHolidayMessage)) {
                WebElement apptDate = availableApptDateList.get(i);
                driver.jsScrollToElementClick(apptDate);
                break;
            }
        }
        List<WebElement> apptTimeList = this.getAllApptTimesForDateCheckout();
        WebElement firstApptTime = apptTimeList.get(0);
        WebElement lastApptTime = apptTimeList.get(apptTimeList.size() - 1);
        String time1 = firstApptTime.getText();
        String time2 = lastApptTime.getText();
        try {
            driver.jsScrollToElement(driver.getElementWithText(allApptTimesBy, time2));
        } catch (Exception e) {
            Assert.fail("Could not scroll from top to bottom of appointment list");
        }
        try {
            driver.jsScrollToElement(driver.getElementWithText(allApptTimesBy, time1));
        } catch (Exception e) {
            Assert.fail("Could not scroll from bottom to top of appointment list");
        }
        LOGGER.info("assertScrollableTimeSlotList completed");
    }

    /**
     * Verifies the appointment message bar appears with the expected text when an
     * available appointment is clicked on
     */
    public void assertAppointmentMessageBarWeb() {
        LOGGER.info("assertAppointmentMessageBar started");
        WebElement apptDate = AppointmentPage.apptSelectedDate;
        String date = apptDate.getText().replace("\n", " ").replace(VIEW_TIMES, "").trim();
        String day = CommonUtils.replaceShortDayWithLongDay(date.split(" ")[0]);
        String dayOfMonth = date.split(" ")[1];
        String month = apptDateContainer.getText().split(" ")[0];
        assertAppointmentMessageBar(day, month, dayOfMonth);
        driver.waitForElementVisible(CommonActions.apptSelectedMsg);
        LOGGER.info("assertAppointmentMessageBar completed");
    }

    /**
     * Verifies the appointment message bar appears with the expected text when an
     * available appointment is clicked on
     */
    public void assertAppointmentMessageBarMobile() {
        LOGGER.info("assertAppointmentMessageBar started");
        boolean found = false;
        while (driver.getDisplayedElementsList(nextDayArrowControlBy).size() > 0) {
            String date = AppointmentPage.apptTimeListHeaderDate.getText();
            String day = CommonUtils.replaceShortDayWithLongDay(date.split(", ")[0]);
            String month = CommonUtils.replaceShortMonthWithLongMonth(date.split(" ")[1]);
            String dayOfMonth = date.split(" ")[2];
            found = assertAppointmentMessageBar(day, month, dayOfMonth);
            if (found) {
                break;
            }
            nextDayArrowControl.click();
        }
        if (!found) {
            Assert.fail("FAIL: None of the listed dates were active");
        }
        LOGGER.info("assertAppointmentMessageBar completed");
    }

    /**
     * Verifies the appointment message bar appears with the expected text when an
     * available appointment is clicked on
     */
    private boolean assertAppointmentMessageBar(String day, String month, String dayOfMonth) {
        String displayedMsg;
        String expectedMsg;
        boolean found = false;
        List<WebElement> availableApptTimeList = getAvailableApptTimesForDateCheckout();
        if (availableApptTimeList.size() > 0) {
            List<WebElement> apptTimeList = this.getAllApptTimesForDateCheckout();

            for (WebElement apptTime : apptTimeList) {
                String AMPM = Constants.DAY_AM;

                if (apptTime.getAttribute(Constants.CLASS).contains(AVAILABLE_TIMES_CLASS_TEXT)) {
                    driver.moveToElementClick(apptTime);
                    // give time for message to appear
                    driver.waitForMilliseconds(Constants.TWO_THOUSAND);
                    driver.waitForElementVisible(CommonActions.apptSelectedMsg);
                    if (CommonUtils.containsIgnoreCase(CommonActions.apptSelectedMsg.getText(), Constants.NO_LONGER))
                        continue;
                    String time = apptTime.getText();
                    String hh = time.split(":")[0];
                    String mm = time.split(":")[1];

                    if (Integer.parseInt(hh) <= 5 || Integer.parseInt(hh) == 12)
                        AMPM = Constants.DAY_PM;

                    time = hh + ":" + mm + " " + AMPM;

                    Assert.assertTrue("The appointment message bar did not appear when selecting"
                            + " an available appointment", driver.isElementDisplayed(CommonActions.apptSelectedMsg, Constants.THIRTY));

                    expectedMsg = ConstantsDtc.APPOINTMENT_MESSAGE_BAR_LABEL + "\n"
                            + day + ", " + month + " " + dayOfMonth + " - " + time;
                    displayedMsg = CommonActions.apptSelectedMsg.getText();

                    if (!month.equalsIgnoreCase(Constants.JUNE) && !(month.equalsIgnoreCase(Constants.JULY))) {
                        if (month.equalsIgnoreCase(Constants.SEPTEMBER)) {
                            expectedMsg = expectedMsg.replace(Constants.SEPTEMBER, Constants.SEPT);
                        } else {
                            expectedMsg = CommonUtils.replaceLongMonthWithShortMonth(expectedMsg);
                        }
                    }

                    Assert.assertTrue("The message bar text is incorrect.  Expected:  " + expectedMsg
                            + ".  Actual:  " + displayedMsg, expectedMsg.equals(displayedMsg));

                    found = true;
                    break;
                }
            }
        }
        return found;
    }

    /**
     * Verifies the order total amounts on checkout page matches with Shopping cart order total
     */
    public void assertCheckoutAndCartOrderTotal() {
        LOGGER.info("assertCheckoutAndCartOrderTotal started");
        driver.waitForElementVisible(CommonActions.orderTotal);
        double checkoutOrderTotal = commonActions.cleanMonetaryStringToDouble(CommonActions.orderTotal.getText());
        Assert.assertTrue("FAIL: Expected order total on cart page: ("
                + CommonActionsSteps.orderTotal + ") did not match to the actual order total on checkout: ("
                + checkoutOrderTotal + ")", CommonActionsSteps.orderTotal == checkoutOrderTotal);
        LOGGER.info("assertCheckoutAndCartOrderTotal completed");
    }

    /**
     * Verifies the sales tax amount on checkout page matches with sales tax on Shopping cart
     */
    public void assertCheckoutAndCartSalesTax() {
        LOGGER.info("assertCheckoutAndCartSalesTax started");
        driver.waitForElementVisible(checkoutAndOrderCartSummarySalesTax);
        double checkoutSalesTax =
                commonActions.cleanMonetaryStringToDouble(checkoutAndOrderCartSummarySalesTax.getText());
        Assert.assertTrue("FAIL: Expected sales tax on checkout page: ("
                + CommonActionsSteps.salesTax + ") did not match to the actual sales tax: ("
                + checkoutSalesTax + ")", CommonActionsSteps.salesTax == checkoutSalesTax);
        LOGGER.info("assertCheckoutAndCartSalesTax completed");
    }

    /**
     * Verifies the store name on checkout page matches the store name on Shopping cart
     */
    public void assertCheckoutAndCartStoreName() {
        LOGGER.info("assertCheckoutAndCartStoreName started");
        if (Config.isMobile()) {
            LOGGER.info("assertCheckoutAndCartStoreName skipped on mobile");
        } else {
            driver.waitForElementVisible(checkoutCartSummaryStoreName);
            String checkoutStoreName = checkoutCartSummaryStoreName.getText().replaceAll("\n", " - ");
            Assert.assertTrue("FAIL: Expected store name on checkout page: (" + checkoutStoreName +
                            ") did not match to the actual store name: (" + CommonActionsSteps.storeName + ")",
                    checkoutStoreName.contains(CommonActionsSteps.storeName));
            LOGGER.info("assertCheckoutAndCartStoreName completed");
        }
    }

    /**
     * Verifies the reason displayed on the Appointment Details section of the Checkout page is correct
     *
     * @param reason - The expected reason to be validated on the checkout page
     */
    public void assertCheckoutApptDetailsReason(String reason) {
        LOGGER.info("assertCheckoutApptDetailsReason started");
        driver.waitForElementVisible(checkoutApptDetailsReason, Constants.TEN);
        String displayReason = checkoutApptDetailsReason.getText().split("\n")[1].
                replace(DISPLAY_REASON_FIELD_LABEL, "").trim();
        Assert.assertTrue("The expected reason was not displayed on the Appointment Details section of the Checkout " +
                        "page! Expected: " + reason + ".  Actual: " + displayReason + ".",
                displayReason.equals(reason));
        LOGGER.info("assertCheckoutApptDetailsReason completed");
    }

    /**
     * Verify Walkins Welcome message is displayed
     */
    public void assertWalkInsWelcomeMessage() {
        LOGGER.info("assertWalkInsWelcomeMessage started");
        if (AppointmentPage.apptSelectedDate.getText().contains(Constants.SUN)) {
            LOGGER.info("Unable to test Walkins Welcome because selected date is Sunday and store is closed");
        } else {
            Assert.assertTrue("The '" + WALKINS_WELCOME + "' message did not appear!",
                    CommonUtils.containsIgnoreCase(noAppointmentTimesAvailable.getText(), WALKINS_WELCOME));
        }
        LOGGER.info("assertWalkInsWelcomeMessage completed");
    }

    /**
     * Verify the appointment dates and appointment times sections displayed
     */
    public void assertAppointmentSelectionDisplayed() {
        LOGGER.info("assertAppointmentSelectionDisplayed started");
        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);
        Assert.assertTrue("The appointment dates and appointment times sections did not display",
                driver.isElementDisplayed(apptDateContainer) && driver.isElementDisplayed(apptTimeContainer));
        LOGGER.info("assertAppointmentSelectionDisplayed completed");
    }

    /**
     * Verify the first appointment date listed is correct
     *
     * @param stockStatus - 'In Stock' or 'On Order'.  If 'In Stock', the
     *                    expected date is current day.  If 'On Order',
     *                    expected date is a number of days in the
     *                    future determined by current day of week.
     */
    public void verifyFirstAppointmentDate(String stockStatus) {
        LOGGER.info("verifyFirstAppointmentDate started with stock status '" + stockStatus + "'");

        driver.waitForElementVisible(apptDateContainer);
        List<WebElement> apptDateDayList = webDriver.findElements(CommonActions.apptDatesDayBy);
        int dateListSize = apptDateDayList.size();

        if (dateListSize == 0) {
            Assert.fail("There were no appointment dates listed on the checkout page");
        }

        Date currentDay = new Date();

        // Add an extra day if it is 5:00 PM or later
        if (Integer.parseInt(currentDay.toString().split(" ")[3].substring(0, 2)) >= 17) {
            calendar.add(Calendar.DATE, 1);
            currentDay = calendar.getTime();
        }

        String dayOfWeek = currentDay.toString().substring(0, 3);
        String[] dateText = null;

        //If Available today, then appointment date is current date,
        // Otherwise it is a future date based on current day of week.
        if (stockStatus.equalsIgnoreCase(ConstantsDtc.AVAILABLE_NOW)) {
            dateText = currentDay.toString().split(" ");
        } else {
            switch (dayOfWeek) {
                case "Sun":
                case "Mon":
                case "Tue":
                    calendar.add(Calendar.DATE, setNumberOfDaysForAppointment(stockStatus));
                    break;

                case "Wed":
                case "Thu":
                case "Fri":
                    calendar.add(Calendar.DATE, setNumberOfDaysForAppointment(stockStatus));
                    break;
                case "Sat":
                    calendar.add(Calendar.DATE, setNumberOfDaysForAppointment(stockStatus));
                    break;
            }
            Date future = calendar.getTime();
            dateText = future.toString().split(" ");
        }

        if (Integer.parseInt(dateText[2]) < 10) {
            dateText[2] = dateText[2].substring(1, 2);
        }

        String expectedDate = dateText[0] + " " + dateText[2];
        String displayedDate = apptDateDayList.get(0).getText().replace("\n", " ");

        Assert.assertTrue("The first appointment date listed (" + displayedDate + ") did not match the expected date "
                + "(" + expectedDate + ")", expectedDate.equals(displayedDate));

        LOGGER.info("verifyFirstAppointmentDate completed");
    }

    /**
     * This method sets the number of days for appointment based on the stockStatus.
     *
     * @param stockStatus The availability message of the article on PLP Page.
     * @return The number of days that are to be added to the current day based on the availablity message.
     */

    private int setNumberOfDaysForAppointment(String stockStatus) {
        LOGGER.info("setNumberOfDaysForAppointment started");
        int returnValue = 0;
        if (stockStatus.toLowerCase().contains(THREE_TO_FIVE)) {
            returnValue = 6;
        } else if (stockStatus.toLowerCase().contains(IN_TWO)) {
            returnValue = 3;
        } else if (stockStatus.toLowerCase().contains(AVAILABLE_TOMORROW)) {
            returnValue = 2;
        }
        LOGGER.info("setNumberOfDaysForAppointment completed");
        return returnValue;
    }

    /**
     * Verify the Appointment Date list header contains the current month and year
     */
    public void assertMonthYearAppointmentDateHeader() {
        LOGGER.info("assertMonthYearAppointmentDateHeader started");
        Date curDay = new Date();
        String curDate = CommonUtils.replaceShortMonthWithLongMonth(String.valueOf(curDay));
        String[] dateText = curDate.split(" ");
        String expectedMonth = CommonUtils.replaceShortMonthWithLongMonth(dateText[1]);
        String expectedYear = dateText[dateText.length - 1];
        String expectedMonthYear = expectedMonth + " " + expectedYear;

        String displayMonthYear = AppointmentPage.apptMonthAndYear.getText();

        Assert.assertTrue("The Appointment Date Header did not contain the correct Month and Year.  Expected: "
                + expectedMonthYear + ".  Displayed: " + displayMonthYear, displayMonthYear.equals(expectedMonthYear));
        LOGGER.info("assertMonthYearAppointmentDateHeader completed");
    }

    /**
     * Verify the Appointment Date list divider contains the next month and year
     */
    public void assertMonthYearAppointmentDateDivider() {
        LOGGER.info("assertMonthYearAppointmentDateDivider started");

        // Verify that there is a divider only when appointments fall across two months
        Date curDay = new Date();
        Date futureDay = new Date();
        int dayIncrement = 9;
        futureDay.setTime(curDay.getTime() + dayIncrement * 1000 * 60 * 60 * 24);

        List<WebElement> headers = webDriver.findElements(AppointmentPage.apptMonthAndYearBy);

        boolean sameMonth = String.valueOf(curDay).split(" ")[1].
                equals(String.valueOf(futureDay).split(" ")[1]);

        if (sameMonth && headers.size() > 1) {
            Assert.fail("A month and year divider for next month is displayed "
                    + "when 10 days in the future is in the current month");
        }

        if (!sameMonth && headers.size() == 1) {
            Assert.fail("10 days in the future is in the next month but "
                    + "no month divider is displayed.");
        }

        if (sameMonth && headers.size() == 1) {
            LOGGER.info("10 days in the future is in the current month.  "
                    + "Month and Year divider can only be tested when the "
                    + "appointment dates fall in different months.");
            return;
        }

        // Get the expected next month and year
        calendar.add(Calendar.MONTH, 1);
        Date futureDate = new Date();
        futureDate = calendar.getTime();
        String futureDateText = CommonUtils.replaceShortMonthWithLongMonth(futureDate.toString());
        String[] futureTextArray = futureDateText.split(" ");
        String expectedMonth = futureTextArray[1];
        String expectedYear = futureTextArray[futureTextArray.length - 1];
        String expectedMonthYear = expectedMonth + " " + expectedYear;

        // Get the displayed divider text
        String displayDividerMonthYear = headers.get(1).getText();

        // Verify the divider text shows the next month and year
        Assert.assertTrue("The Appointment Date divider did not contain the correct Month"
                + " and Year.  Expected:  " + expectedMonthYear + ".  Displayed:  "
                + displayDividerMonthYear, displayDividerMonthYear.equals(expectedMonthYear));

        LOGGER.info("assertMonthYearAppointmentDateDivider completed");
    }

    /**
     * Verify the the appointment time list is displayed
     */
    public void assertAppointmentTimeListDisplayed() {
        LOGGER.info("assertAppointmentTimeListDisplayed started");
        Assert.assertTrue("The appointment time list was not displayed",
                getAllApptTimesForDateCheckout().size() > 0);
        LOGGER.info("assertAppointmentTimeListDisplayed completed");
    }

    /**
     * Verify the date listed in the appointment time list header is displayed and correct
     */
    public void assertAppointmentTimeListHeaderDate() {
        LOGGER.info("assertAppointmentTimeListHeaderDate started");

        // Verify a date is present
        Assert.assertTrue("The appointment time list header does not contain a date",
                driver.isElementDisplayed(AppointmentPage.apptTimeListHeaderDate));

        // Get the day of week and the day of month from the header date
        String headerDate = AppointmentPage.apptTimeListHeaderDate.getText();
        String[] headerDateText = headerDate.split(" ");
        String headerDayOfWeek = headerDateText[0].replace(",", "").trim();
        String headerMonthDay = headerDateText[headerDateText.length - 1].trim();
        String headerDayOfMonth = headerDayOfWeek + " " + headerMonthDay;

        // Get the day of week and day of month and date displayed text from the selected date
        String selectedDate = AppointmentPage.apptSelectedDate.getText().
                replace("\n", " ").replace(VIEW_TIMES, "").trim();
        String expectedDayOfMonth = CommonUtils.replaceShortDayWithLongDay(selectedDate);

        //Extract the value of day of week and day of month from the selected date text
        String[] expectedDateText = expectedDayOfMonth.split(" ");
        String expectedDate = expectedDateText[0] + " " + expectedDateText[1];

        // Verify the header date is correct
        Assert.assertTrue("The appointment time list header date is incorrect.  "
                        + "Selected date is '" + selectedDate + "'.  "
                        + "Time list header date is '" + headerDate + "'",
                headerDayOfMonth.startsWith(expectedDate));

        LOGGER.info("assertAppointmentTimeListHeaderDate completed");
    }

    /**
     * Calculate Tax on checkout page after Shipping details and Shipping Method provided
     */
    public double calculateTaxOnCheckout() {
        LOGGER.info("calculateTaxOnCheckout started");
        double taxOnCheckout;
        double totalTaxOnCheckout = 0.00;
        List<WebElement> items = webDriver.findElements(summaryProductHeaderBy);
        String stateOnCart = (checkoutSummaryCustomerDetails.getText().split(",")[1].split("\\s+")[1]).trim();
        if (stateOnCart.equalsIgnoreCase(Constants.STATE_AZ) || stateOnCart.equalsIgnoreCase(Constants.STATE_TX)
                || stateOnCart.equalsIgnoreCase(Constants.STATE_GA)
                || stateOnCart.equalsIgnoreCase(Constants.STATE_OH)) {
            for (WebElement item : items) {
                String customerType;
                if (stateOnCart.equalsIgnoreCase(Constants.STATE_TX)
                        || stateOnCart.equalsIgnoreCase(Constants.STATE_OH)
                        || stateOnCart.equalsIgnoreCase(Constants.STATE_GA)) {
                    customerType = "default_customer_".concat(stateOnCart.toLowerCase());
                } else {
                    customerType = "default_customer_dtd";
                }
                commonActions.setRegionalTaxesFactor(customer.getCustomer(customerType));

                driver.waitForElementVisible(summaryProductProductTotalBy);
                String itemPriceTotal = item.findElement(summaryProductProductTotalBy).getText();
                taxOnCheckout = commonActions.getCalculatedSalesTaxForDTCRegion(itemPriceTotal);
                if (!stateOnCart.equalsIgnoreCase(Constants.STATE_TX) &&
                        !stateOnCart.equalsIgnoreCase(Constants.STATE_GA)) {
                    String envFee = item.findElement(CartPage.feeDetailsItemsRowParentBy).
                            findElement(CartPage.feeDetailsItemsRowPriceBy).getText();
                    taxOnCheckout = taxOnCheckout + commonActions.getCalculatedSalesTaxForDTCRegion(envFee);
                }
                if (driver.isElementDisplayed(checkoutSummaryCertificateBy)) {
                    String certFee = item.findElement(checkoutSummaryCertificateBy).
                            findElement(CartPage.feeDetailsItemsRowPriceBy).getText();
                    taxOnCheckout = taxOnCheckout + commonActions.getCalculatedSalesTaxForDTCRegion(certFee);
                }
                if (driver.isElementDisplayed(checkoutSummaryFeeTpmsBy)) {
                    String optionalFee = item.findElement(checkoutSummaryFeeTpmsBy).
                            findElement(CartPage.feeDetailsItemsRowPriceBy).getText();
                    taxOnCheckout = taxOnCheckout + commonActions.getCalculatedSalesTaxForDTCRegion(optionalFee);
                }
                if (driver.isElementDisplayed(CartPage.qualifyPromotionAmountBy)) {
                    String promotion = item.findElement(CartPage.qualifyPromotionAmountBy).getText();
                    taxOnCheckout = taxOnCheckout
                            - commonActions.getCalculatedSalesTaxForDTCRegion(promotion);
                }
                totalTaxOnCheckout = totalTaxOnCheckout + taxOnCheckout;
                if (!shipping.getText().equalsIgnoreCase(SHIPPING_FREE)) {
                    totalTaxOnCheckout = totalTaxOnCheckout +
                            commonActions.getCalculatedSalesTaxForDTCRegion(shipping.getText());
                }
            }
        }
        LOGGER.info("calculateTaxOnCheckout completed");
        return commonActions.twoDForm(totalTaxOnCheckout, 2);
    }

    /**
     * Verifies the Tax on checkout page with calculated tax based on shipping address and fees applicable
     */
    public void assertTaxOnCheckout() {
        LOGGER.info("assertTaxOnCheckout started");
        driver.waitForElementVisible(checkoutAndOrderCartSummarySalesTax);
        double checkoutSalesTax =
                commonActions.cleanMonetaryStringToDouble(checkoutAndOrderCartSummarySalesTax.getText());
        double expectedSalesTax = calculateTaxOnCheckout();
        Assert.assertTrue("FAIL: Expected sales tax on checkout page: (" + expectedSalesTax +
                        ") did not match to the actual sales tax: (" + checkoutSalesTax + ")",
                expectedSalesTax == checkoutSalesTax);
        LOGGER.info("assertTaxOnCheckout completed");
    }

    /**
     * Verifies the order total on checkout page matches to sum of item total, calculated sales tax and
     * shipping fee applied
     */
    public void assertCheckoutOrderPriceTotal() {
        LOGGER.info("assertCheckoutOrderPriceTotal started");
        driver.waitForElementVisible(CommonActions.orderTotal);
        double expectedOrderTotal =
                commonActions.cleanMonetaryStringToDouble(checkoutAndOrderCartSummarySalesTax.getText())
                        + commonActions.cleanMonetaryStringToDouble(checkoutSubtotal.getText());
        if (!shipping.getText().equalsIgnoreCase(SHIPPING_FREE)) {
            expectedOrderTotal = expectedOrderTotal + commonActions.cleanMonetaryStringToDouble(shipping.getText());
        }
        double actualOrderTotal = commonActions.cleanMonetaryStringToDouble(CommonActions.orderTotal.getText());
        Assert.assertTrue("FAIL: The actual order total: '" + actualOrderTotal +
                        "' did not match to expected order total: '" +
                        commonActions.twoDForm(expectedOrderTotal, 2) + "'!",
                actualOrderTotal == commonActions.twoDForm(expectedOrderTotal, 2));
        LOGGER.info("assertCheckoutOrderPriceTotal completed");
    }

    /**
     * Verify that the specified with or without appointment option is selected on the Checkout page
     *
     * @param appointmentOption - 'Install with appointment' or 'Install without appointment'
     */
    public void assertAppointmentOptionSelected(String appointmentOption) {
        LOGGER.info("assertAppointmentOptionSelected started");
        if (appointmentOption.equalsIgnoreCase(ConstantsDtc.INSTALL_WITH_APPOINTMENT)) {
            Assert.assertTrue("FAIL:  The 'install with appointment' option was not selected on the "
                    + "Appointment page!", driver.isElementDisplayed(apptDateContainer));
        } else {
            Assert.assertTrue("FAIL:  The 'install without appointment' option was not selected on the "
                    + "Appointment page!", !driver.isElementDisplayed(apptDateContainer));
        }
        LOGGER.info("assertAppointmentOptionSelected completed");
    }

    /**
     * Verify the selected 'Install without appointment' reason is correct
     *
     * @param expectedReason - the expected selected reason
     */
    public void assertInstallWithoutAppointmentSelectedReason(String expectedReason) {
        LOGGER.info("assertInstallWithoutAppointmentSelectedReason started");
        String selectedReason = installWithoutAppointmentSelectedReason.getText();
        Assert.assertTrue("FAIL:  The selected reason was not correct.  "
                        + "Expected:  " + expectedReason + ".  Actual:  " + selectedReason + "!",
                selectedReason.equals(expectedReason));
        LOGGER.info("assertInstallWithoutAppointmentSelectedReason completed");
    }

    /**
     * Verify the listed 'Install without appointment' reasons are correct
     */
    public void assertInstallWithoutAppointmentReasonOptions() {
        LOGGER.info("assertInstallWithoutAppointmentReasonOptions started");
        driver.waitForPageToLoad();
        withoutApptReasonToggleBtn.click();
        driver.waitForMilliseconds();
        List<WebElement> listedReasons = formGroupSelect.
                findElements(CommonActions.dropdownOptionBy);
        String[] expectedReasons = {ConstantsDtc.APPT_NOT_SURE_OF_AVAILABILITY,
                ConstantsDtc.APPT_MAKE_AN_APPOINTMENT_AT_A_LATER_TIME,
                ConstantsDtc.APPT_THESE_ITEMS_ARE_FOR_MULTIPLE_VEHICLES,
                ConstantsDtc.APPT_MY_PREFERRED_DATE_TIME_IS_NOT_AVAILABLE};

        // Validate the reasons that are listed are correct
        for (WebElement listedReason : listedReasons) {
            String reasonText = listedReason.findElement(CommonActions.spanTagNameBy).getText();
            boolean found = false;
            for (int i = 0; i < expectedReasons.length; i++) {
                if (reasonText.equals(expectedReasons[i])) {
                    found = true;
                    break;
                }
            }
            Assert.assertTrue("FAIL:   Unexpected reason (" + reasonText + ") listed "
                    + "in the Install Without Appointment Reason dropdown!", found);
        }

        // Validate all of the expected reasons are listed
        for (String expectedReason : expectedReasons) {
            boolean found = false;
            for (int i = 0; i < listedReasons.size(); i++) {
                if (expectedReason.equals(listedReasons.get(i).findElement(CommonActions.spanTagNameBy).getText())) {
                    found = true;
                    break;
                }
            }
            Assert.assertTrue("FAIL:  Expected reason (" + expectedReason + ") not listed "
                    + "in the Install Without Appointment Reason dropdown!", found);
        }
        withoutApptReasonToggleBtn.click();
        LOGGER.info("assertInstallWithoutAppointmentReasonOptions completed");
    }

    /**
     * Verify the expected Reserve Without Appointment message displayed
     */
    public void assertReserveWithoutAppointmentMessage(String installWithoutAppointmentReason) {
        LOGGER.info("assertReserveAppointmentMessage started");
        String expectedMessage = null;

        switch (installWithoutAppointmentReason) {
            case ConstantsDtc.APPT_NOT_SURE_OF_AVAILABILITY:
                expectedMessage = ConstantsDtc.RESERVE_WITHOUT_APPOINTMENT_MESSAGE;
                break;
            case ConstantsDtc.APPT_MAKE_AN_APPOINTMENT_AT_A_LATER_TIME:
                expectedMessage = ConstantsDtc.RESERVE_WITHOUT_APPOINTMENT_MESSAGE;
                break;
            case ConstantsDtc.APPT_THESE_ITEMS_ARE_FOR_MULTIPLE_VEHICLES:
                expectedMessage = ConstantsDtc.RESERVE_WITHOUT_APPOINTMENT_MULTIPLE_VEHICLES_MESSAGE;
                break;
            case ConstantsDtc.APPT_MY_PREFERRED_DATE_TIME_IS_NOT_AVAILABLE:
                expectedMessage = ConstantsDtc.RESERVE_WITHOUT_APPOINTMENT_WALK_IN_MESSAGE;
                break;
        }

        Assert.assertTrue("FAIL:  The expected reserve appointment message (" + expectedMessage + ") did not display!",
                reserveAppointmentMessage.getText().equals(expectedMessage));
        LOGGER.info("assertReserveAppointmentMessage completed");
    }

    /**
     * Click the 'edit' link for Appointment Details
     */
    public void clickEditAppointmentLink() {
        LOGGER.info("clickEditAppointmentLink started");
        driver.waitForPageToLoad();
        editAppointmentLink.click();
        LOGGER.info("clickEditAppointmentLink completed");
    }

    /**
     * Verify the 'Continue to Customer Details' button is enabled or disabled
     *
     * @param enabledDisabled - expected status:  'enabled' or 'disabled'
     */
    public void verifyContinueToCustomerDetailsButtonStatus(String enabledDisabled) {
        LOGGER.info("verifyContinueToCustomerDetailsButtonStatus started");
        WebElement continueToCustomerDetailsButton = driver.getElementWithText(CommonActions.formSubmitButtonBy,
                ConstantsDtc.CONTINUE_TO_CUSTOMER_DETAILS);
        if (enabledDisabled.equalsIgnoreCase(Constants.ENABLED)) {
            Assert.assertTrue("FAIL: The 'Continue to Customer Details' button was disabled. "
                    + "Expected it to be enabled!", continueToCustomerDetailsButton.isEnabled());
        } else {
            Assert.assertTrue("FAIL: The 'Continue to Customer Details' button was enabled. "
                    + "Expected it to be disabled!", !continueToCustomerDetailsButton.isEnabled());
        }
        LOGGER.info("verifyContinueToCustomerDetailsButtonStatus completed");
    }

    /**
     * Verify 'View Times' did not display on any unavailable appointment dates
     */
    public void verifyViewTimesNotDisplayedForUnavailableAppointmentDate() {
        LOGGER.info("verifyViewTimesDisplayedNotDisplayed started");
        List<WebElement> unavailableApptDateList = getUnavailableApptDatesForCheckout();
        for (WebElement unavailableApptDate : unavailableApptDateList) {
            Assert.assertTrue("'View Times' displayed on an unavailable appointment date:  " +
                            unavailableApptDate.getText(),
                    !unavailableApptDate.getText().contains(VIEW_TIMES));
        }
        LOGGER.info("verifyViewTimesDisplayedNotDisplayed completed");
    }

    /**
     * Verify 'Store Closed' is displayed on Sunday appointment date
     */
    public void verifyStoreClosedDisplayedOnSundayAppointmentDate() {
        LOGGER.info("verifyStoreClosedDisplayedOnSundayAppointmentDate started");
        List<WebElement> appointmentDays = webDriver.findElements(allAvailableApptDatesBy);
        boolean found = false;
        for (WebElement day : appointmentDays) {
            if (day.getText().contains(Constants.SUN)) {
                String sunStoreClosed = day.findElement(selectDateMessageBy).getText();
                Assert.assertTrue("FAIL: Sunday appointment date not displaying the store closed text, displayed: '"
                                + sunStoreClosed + "' , expected: '" + STORE_CLOSED + "'.",
                        sunStoreClosed.equalsIgnoreCase(STORE_CLOSED));
                found = true;
            }
        }
        if (!found) {
            Assert.fail("Unable to find Sunday appointment date");
        }
        LOGGER.info("verifyStoreClosedDisplayedOnSundayAppointmentDate completed");
    }

    /**
     * Select the first appointment date found for the specified day of the week
     *
     * @param dayOfWeek - Full name for day of the week
     */
    public void selectAppointmentDate(String dayOfWeek) {
        LOGGER.info("selectAppointmentDate started");
        driver.waitForElementVisible(apptDateContainer);
        boolean found = false;
        List<WebElement> availableApptDateList = getAvailableApptDatesForCheckout();
        for (WebElement apptDate : availableApptDateList) {
            if (apptDate.getText().contains(CommonUtils.replaceLongDayWithShortDay(dayOfWeek))) {
                apptDate.click();
                driver.waitForPageToLoad();
                found = true;
                break;
            }
        }
        if (!found) {
            Assert.fail("FAIL: None of the active dates listed were on '" + dayOfWeek + "'!");
        }
        LOGGER.info("selectAppointmentDate completed");
    }

    /**
     * Verify the appropriate peak times message is displayed for specified day of the week
     *
     * @param dayOfWeek - Full name for day of the week
     */
    public void verifyPeakTimesMessage(String dayOfWeek) {
        LOGGER.info("verifyPeakTimesMessage started");
        if (dayOfWeek.equalsIgnoreCase(Constants.SATURDAY)) {
            Assert.assertTrue("FAIL:  The message, 'Our busiest day of the week expect a longer wait for service', " +
                            "did not display for Saturday!",
                    peakTimesMessage.getText().trim().equals(Constants.BUSIEST_DAY_OF_WEEK_MESSAGE));
        } else {
            if (dayOfWeek.toUpperCase().equalsIgnoreCase(Constants.MONDAY) ||
                    dayOfWeek.toUpperCase().equalsIgnoreCase(Constants.FRIDAY) ||
                    dayOfWeek.toUpperCase().equalsIgnoreCase(Constants.TUESDAY) ||
                    dayOfWeek.toUpperCase().equalsIgnoreCase(Constants.WEDNESDAY) ||
                    dayOfWeek.toUpperCase().equalsIgnoreCase(Constants.THURSDAY)) {
                Assert.assertTrue("The 'Peak Times: Expect a longer wait for service' message did not display",
                        driver.isElementDisplayed(peakTimesMessage) &&
                                peakTimesMessage.getText().equals(PEAK_TIMES_MESSAGE));
            } else {
                Assert.fail("FAIL:  The input date is not valid.  Expected:  "
                        + "Any day Monday through Saturday.  Actual:  " + dayOfWeek + "!");
            }
        }
        LOGGER.info("verifyPeakTimesMessage completed");
    }

    /**
     * Verify the peak hours are defined correctly for specified day of the week
     *
     * @param dayOfWeek - Full name for day of the week
     */
    public void verifyPeakHours(String dayOfWeek) {
        LOGGER.info("verifyPeakHours started");
        String expectedPeakHoursStartTime = null;
        String expectedPeakHoursEndTime = Constants.ONE_PM;

        List<WebElement> peakHoursRows = webDriver.findElements(peakHoursRowContainerBy);

        if (dayOfWeek.equalsIgnoreCase(Constants.SATURDAY)) {
            Assert.assertTrue("FAIL:  Unexpected peak hours are defined when a Saturday date is selected!",
                    peakHoursRows.size() == 0);
            return;
        } else {
            if (dayOfWeek.toUpperCase().equalsIgnoreCase(Constants.MONDAY) ||
                    dayOfWeek.toUpperCase().equalsIgnoreCase(Constants.FRIDAY)) {
                expectedPeakHoursStartTime = Constants.EIGHT_AM;
            } else {
                if (dayOfWeek.toUpperCase().equalsIgnoreCase(Constants.TUESDAY) ||
                        dayOfWeek.toUpperCase().equalsIgnoreCase(Constants.WEDNESDAY) ||
                        dayOfWeek.toUpperCase().equalsIgnoreCase(Constants.THURSDAY)) {
                    expectedPeakHoursStartTime = Constants.ELEVEN_AM;
                } else {
                    Assert.fail("FAIL:  The input date is not valid.  Expected:  "
                            + "Any day Monday through Saturday.  Actual:  " + dayOfWeek + "!");
                }
            }
        }

        String displayedPeakHoursStartTime = peakHoursRows.get(0).findElement(appointmentRowLabelBy).getText().trim();
        String displayedPeakHoursEndTime = peakHoursRows.get(peakHoursRows.size() - 1).
                findElement(appointmentRowLabelBy).getText().trim();
        Assert.assertTrue("FAIL:  The peak hours block is incorrect for " + dayOfWeek + "!"
                        + "  Expected: " + expectedPeakHoursStartTime + " - " + expectedPeakHoursEndTime
                        + ".  Actual: " + displayedPeakHoursStartTime + " - " + displayedPeakHoursEndTime + "!",
                displayedPeakHoursStartTime.equals(expectedPeakHoursStartTime) &&
                        displayedPeakHoursEndTime.equals(expectedPeakHoursEndTime));
        LOGGER.info("verifyPeakHours completed");
    }

    /**
     * Select the first available appointment time within the peak hours for the selected date
     */
    public void selectFirstAvailablePeakHoursAppointmentTimeForSelectedDate() {
        LOGGER.info("selectFirstAvailablePeakHoursAppointmentTimeForSelectedDate started");
        List<WebElement> peakHoursRows = webDriver.findElements(peakHoursRowContainerBy);
        List<WebElement> apptTimes = null;

        for (WebElement peakHourRow : peakHoursRows) {
            try {
                apptTimes = peakHourRow.findElements(CommonActions.availableApptTimesBy);
                break;
            } catch (NoSuchElementException noElement) {
                LOGGER.info("No available appointments found on peak hour row "
                        + peakHourRow.getText().replace("Peak Times: Expect a longer wait for service", "").
                        replace("\n", " "));
            }
        }

        Assert.assertTrue("FAIL:  No available appointments were found within peak hours for selected date!",
                apptTimes != null);
        apptTimes.get(0).click();
        LOGGER.info("selectFirstAvailablePeakHoursAppointmentTimeForSelectedDate completed.  Peak time " +
                apptTimes.get(0).getText() + " selected.");
    }

    /**
     * Verify message containing 'Service time for this appointment may be longer than usual'
     * and estimated time of completion displayed in the appointment time list
     */
    public void verifyPeakTimeSelectedMessage() {
        LOGGER.info("verifyPeakTimeSelectedMessage started");
        DateFormat formatter = new SimpleDateFormat("HH:mm");
        String selectedTime = selectedAppointmentTime.getText();
        String AMPM = Constants.DAY_PM;
        Time timeValue = null;

        try {
            timeValue = new java.sql.Time(formatter.parse(selectedTime).getTime());
        } catch (ParseException e) {
            e.getMessage();
            Assert.fail("FAIL:  Unable to convert " + selectedTime + " to Time object!");
        }

        calendar.setTime(timeValue);
        calendar.add(MINUTE, 45);
        timeValue.setTime(calendar.getTimeInMillis());
        String timeString = timeValue.toString();
        timeString = timeString.substring(0, timeString.length() - 3);

        if (timeString.charAt(0) == '0') {
            timeString = timeString.substring(1, timeString.length());
            AMPM = Constants.DAY_AM;
        }

        String displayedMessage = peakTimeSelectedMessage.getText().replace("\n", "");
        String expectedMessage = SERVICE_TIME_LONGER_THAN_USUAL + EST_TIME_OF_COMPLETION + timeString + " " + AMPM;
        Assert.assertTrue("FAIL:  The expected message containing " +
                        "'Service time for this appointment may be longer than usual. Est. time of completion: " +
                        timeString + " " + AMPM + "' did not display!",
                displayedMessage.equals(expectedMessage));
        LOGGER.info("verifyPeakTimeSelectedMessage completed");
    }

    /**
     * IF displayStatus == Displayed, the method verifies the current modal is 'Shipping Restriction', that the
     * messaging matches expected, as well as the controls / buttons (EDIT CART & REMOVE ITEMS) being active / enabled.
     * ELSE verifies that the modal is NOT displayed
     *
     * @param displayStatus Determines if the 'Shipping Restrictions' modal (and messaging) should or should not be
     *                      displayed after entering a customer's shipping information
     */
    public void verifyShippingRestrictionModalMessagingControls(String displayStatus) {
        LOGGER.info("verifyShippingRestrictionModalMessagingControls started");
        driver.waitForPageToLoad();

        if (displayStatus.equalsIgnoreCase(Constants.DISPLAYED)) {
            driver.waitForElementVisible(CommonActions.dtModalContainer);
            Assert.assertTrue("FAIL: The currently displayed modal is not titled '" + SHIPPING_RESTRICTION + "'!",
                    CommonActions.dtModalContainer.findElement(CommonActions.dtModalTitleBy).
                            getText().equalsIgnoreCase(SHIPPING_RESTRICTION));

            String modalText = shippingRestrictionMessagingContainer.getText();
            Assert.assertTrue("FAIL: The 'Shipping Restriction' modal did NOT contain the expected messaging: '"
                            + SHIPPING_RESTRICTION_MESSAGING + "' in its displayed text of: '" + modalText + "'!",
                    modalText.contains(SHIPPING_RESTRICTION_MESSAGING));

            String[] controlNames = {EDIT_CART, REMOVE_ITEMS};
            for (String name : controlNames) {
                WebElement controlEle = driver.getElementWithText(CommonActions.dtButtonBy, name);

                if (controlEle != null) {
                    Assert.assertTrue("FAIL: The modal control: '" + name + "' was NOT enabled!",
                            controlEle.isEnabled());
                } else {
                    Assert.fail("FAIL: Could NOT find modal control: '" + name + "'!");
                }
            }
        } else {
            Assert.assertTrue("FAIL: The 'Shipping Restriction modal was displayed when it was expected"
                    + " NOT to be!", !driver.isElementDisplayed(CommonActions.dtModalContainer, Constants.TWO));
        }
        LOGGER.info("verifyShippingRestrictionModalMessagingControls completed");
    }

    /**
     * Selects / performs the desired action on the 'Shipping Restriction' modal window
     */
    public void selectActionFromShippingRestrictionModal(String action) {
        LOGGER.info("selectActionFromShippingRestrictionModal started with action: '" + action + "'");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(CommonActions.dtModalContainer);

        switch (action) {
            case CHANGE_ADDRESS:
                if (!Config.isSafari()) {
                    driver.clickElementWithLinkText(CHANGE_ADDRESS);
                } else {
                    CommonActions.dtModalContainer.findElement(CommonActions.anchorTagBy).click();
                }
                break;
            case Constants.CLOSE:
                commonActions.closeModalWindow();
                break;
            case REMOVE_ITEMS:
                driver.clickElementWithText(CommonActions.dtButtonBy, REMOVE_ITEMS);
                break;
            default:
                Assert.fail("FAIL: The specified action: '" + action + "' is NOT currently a valid selection on the"
                        + " 'Shipping Restriction' modal!");
                break;
        }
        LOGGER.info("selectActionFromShippingRestrictionModal completed with action: '" + action + "'");
    }

    /**
     * Verifies the specified product is either displayed or not displayed in the 'Order Summary' of the Checkout page
     *
     * @param productName   Name of the product to search for in the 'Order Summary'
     * @param displayStatus Display expectation for the product - displayed or not displayed
     */
    public void verifyProductDisplayInOrderSummary(String productName, String displayStatus) {
        LOGGER.info("verifyProductDisplayInOrderSummary started with product: '" + productName
                + "', verifying that it is '" + displayStatus + "'");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(checkoutOrderSummaryBy);
        if (displayStatus.equalsIgnoreCase(Constants.DISPLAYED)) {
            Assert.assertTrue("FAIL: Product \"" + productName + "\" was NOT displayed in the"
                    + " 'Order Summary' of the Checkout page!", driver.checkIfElementContainsText(
                    checkoutOrderSummaryBy, productName));
        } else {
            Assert.assertTrue("FAIL: Product \"" + productName + "\" was expected to NOT be displayed but"
                            + " was present in the 'Order Summary' of the Checkout page!",
                    !driver.checkIfElementContainsText(checkoutOrderSummaryBy, productName));
        }
        LOGGER.info("verifyProductDisplayInOrderSummary completed with product: '" + productName
                + "', verified that it was '" + displayStatus + "'");
    }

    /**
     * Verify Need Help link is displayed
     */
    public void assertNeedHelpLinkDisplayed() {
        LOGGER.info("assertNeedHelpLinkDisplayed started");
        driver.waitForPageToLoad();
        Assert.assertTrue("FAIL: Need Help link did NOT display", driver.isElementDisplayed(needHelpElement));
        LOGGER.info("assertNeedHelpLinkDisplayed completed");
    }

    /**
     * Verify Need Help popup values are displayed
     */
    public void assertNeedHelpPopUpValuesDisplayed() {
        LOGGER.info("assertNeedHelpPopUpValuesDisplayed started");
        driver.waitForPageToLoad();
        if (!(driver.getParentElement(CommonActions.emailEnvelopeIcon).getText().contains(CommonActions.EMAIL)
                && needHelpPhoneElement.getText().contains(Config.getDefaultStorePhoneNumber()))) {
            Assert.fail("FAIL: Need Help PopUp values did NOT match");
        }
        LOGGER.info("assertNeedHelpPopUpValuesDisplayed completed");
    }

    /**
     * Verify display of "Sign In to Skip this step" in Checkout Page
     *
     * @param text displayed or not displayed
     */
    public void assertSignInToSkipThisStepDisplay(String text) {
        LOGGER.info("assertSignInToSkipThisStepDisplay started");
        driver.waitForPageToLoad();
        if (text.equalsIgnoreCase(Constants.DISPLAYED)) {
            WebElement signInToSkipThisStep =
                    driver.getElementWithText(CommonActions.dtLinkBy, ConstantsDtc.SIGN_IN_TO_SKIP_THIS_STEP);
            Assert.assertTrue("FAIL: 'sign in to skip this step' NOT displayed in Customer Details Checkout !",
                    driver.isElementDisplayed(signInToSkipThisStep));
        } else {
            Assert.assertTrue("FAIL: 'sign in to skip this step' is displayed in Customer Details Checkout"
                            + " Page for already logged in customer!",
                    !driver.isTextPresentInPageSource(ConstantsDtc.SIGN_IN_TO_SKIP_THIS_STEP));
        }
        LOGGER.info("assertSignInToSkipThisStepDisplayed completed");
    }

    /**
     * Verify pre-populated shipping user details on checkout page
     *
     * @param userDetails  Comma separated string of inputFields to be verified
     * @param customerType customer user profile
     */
    public void assertPrePopulatedShippingDetails(String userDetails, String customerType) {
        LOGGER.info("assertPrePopulatedShippingDetails started");
        driver.waitForPageToLoad();
        String inputFieldLabel = "";
        String inputFieldValue = "";
        List<String> inputValues = Arrays.asList(userDetails.split("\\s*,\\s*"));

        Customer expectedUser = customer.getCustomer(customerType);

        for (String inputValue : inputValues) {
            switch (inputValue) {
                case CommonActions.FIRST_NAME:
                    inputFieldLabel = CommonActions.FIRST_NAME;
                    inputFieldValue = expectedUser.firstName;
                    break;
                case CommonActions.LAST_NAME:
                    inputFieldLabel = CommonActions.LAST_NAME;
                    inputFieldValue = expectedUser.lastName;
                    break;
                case CommonActions.ADDRESS_LINE_1:
                    inputFieldLabel = CommonActions.ADDRESS_LINE_1;
                    inputFieldValue = expectedUser.address1;
                    break;
                case CommonActions.ADDRESS_LINE_2:
                    inputFieldLabel = CommonActions.ADDRESS_LINE_2;
                    inputFieldValue = expectedUser.address2;
                    break;
                case ConstantsDtc.ZIP:
                    inputFieldLabel = ConstantsDtc.ZIP;
                    inputFieldValue = expectedUser.zip;
                    break;
                case ConstantsDtc.ZIP_CODE:
                    inputFieldLabel = ConstantsDtc.ZIP_CODE;
                    inputFieldValue = expectedUser.zipPlus4;
                    break;
                case CommonActions.EMAIL:
                    inputFieldLabel = CommonActions.EMAIL;
                    inputFieldValue = expectedUser.email;
                    break;
                case ConstantsDtc.PHONE_TYPE:
                    inputFieldLabel = ConstantsDtc.PHONE_TYPE;
                    inputFieldValue = ConstantsDtc.MOBILE.toUpperCase();
                    break;
                case Constants.PHONE_NUMBER:
                    inputFieldLabel = Constants.PHONE_NUMBER;
                    inputFieldValue = expectedUser.phone;
                    inputFieldValue = inputFieldValue.replaceAll("\\D", "");
                    break;
                case Constants.CITY:
                    inputFieldLabel = Constants.CITY;
                    inputFieldValue = expectedUser.city;
                    break;
                case ConstantsDtc.STATE:
                    inputFieldLabel = ConstantsDtc.STATE;
                    inputFieldValue = expectedUser.state;
                    break;
                case ConstantsDtc.COUNTRY:
                    inputFieldLabel = ConstantsDtc.COUNTRY;
                    inputFieldValue = expectedUser.country;
                    break;
            }
            assertPopulatedCustomerInfo(inputFieldLabel, inputFieldValue);
        }
        LOGGER.info("assertPrePopulatedShippingDetails completed");
    }

    /**
     * Verify populated customer details
     *
     * @param inputFieldLabel displayed form label
     * @param inputFieldValue displayed form value
     */
    public void assertPopulatedCustomerInfo(String inputFieldLabel, String inputFieldValue) {
        LOGGER.info("assertPopulatedCustomerInfo started");
        String actualValue = returnCheckoutCustomerValues(inputFieldLabel);
        Assert.assertTrue("FAIL: Input field \"" + inputFieldLabel + "\" did not contain value \""
                        + inputFieldValue + "\", but instead contained \"" + actualValue + "\"",
                actualValue.toLowerCase().contains(inputFieldValue.toLowerCase()));
        LOGGER.info("assertPopulatedCustomerInfo completed");
    }

    /**
     * Return populated customer details for input field label
     *
     * @param inputFieldLabel displayed form label
     */
    public String returnCheckoutCustomerValues(String inputFieldLabel) {
        LOGGER.info("returnCheckoutCustomerValues started");
        List<WebElement> formValues = webDriver.findElements(CommonActions.formGroupBy);
        String actualValue = "";
        for (WebElement formValue : formValues) {
            String valueText = formValue.getText();
            if (valueText.contains("\n")) {
                valueText = valueText.split("\n")[0].trim();
            }
            if (valueText.contains(ConstantsDtc.STATE) && valueText.contains(inputFieldLabel) ||
                    valueText.contains(ConstantsDtc.COUNTRY) && valueText.contains(inputFieldLabel)) {
                actualValue = formValue.findElement(CommonActions.dropdownBy).getText();
                break;
            } else if (valueText.contains(inputFieldLabel)) {
                actualValue = formValue.findElement(CommonActions.inputBy).getAttribute(Constants.VALUE);
                break;
            }
        }
        LOGGER.info("returnCheckoutCustomerValues completed");
        return actualValue;
    }

    /**
     * Select either 'ROPIS' or 'BOPIS' option
     *
     * @param selection - The option to choose: 'ROPIS' or 'BOPIS'
     */
    public void selectPickUpInStoreOption(String selection) {
        LOGGER.info("selectPickUpInStoreOption started selecting '" + selection + "'");
        driver.waitForPageToLoad(Constants.THREE_THOUSAND);
        if (selection.equalsIgnoreCase(ConstantsDtc.BOPIS)) {
            driver.moveToElementClick(bopisRadio);
        } else {
            driver.moveToElementClick(ropisRadio);
        }
        LOGGER.info("selectPickUpInStoreOption completed selecting '" + selection + "'");
    }

    /**
     * Verify the product name and quantity are correct on the Checkout page
     *
     * @param productName - name of the product
     */
    public void verifyProductQuantity(String productName) {
        LOGGER.info("verifyProductQuantity started for product '" + productName + "'");
        List<WebElement> productHeaders = webDriver.findElements(summaryProductHeaderBy);
        String quantity = "";
        boolean found = false;

        String productQuantity = commonActions.productInfoListGetValue(ConstantsDtc.QUANTITY);

        int i = 0;
        for (WebElement productHeader : productHeaders) {
            if (CommonUtils.containsIgnoreCase(productHeader.getText(), productName)) {
                WebElement productQuantityElement =
                        webDriver.findElements(summaryProductFeesAndPromotionsBy).get(i).findElement(productQuantityBy);
                quantity = productQuantityElement.getText().split(":")[1].trim();
                found = true;
                break;
            }
            i++;
        }

        if (!found) {
            Assert.fail("FAIL: The product summary did not contain the expected product: " + productName);
        }

        Assert.assertEquals("FAIL: The quantity displayed was not correct for '" + productName +
                "'. Expected: '" + productQuantity + "'. Actual: '" + quantity + "'", Integer.parseInt(quantity),
                Integer.parseInt(productQuantity));
        LOGGER.info("verifyProductQuantity completed for product '" + productName + "'");
    }

    /**
     * Verify Instant Savings with Cart page Instant Savings
     *
     * @param page Web Page Header
     */
    public void assertInstantSavings(String page) {
        LOGGER.info("assertInstantSavings started for " + page);
        driver.waitForPageToLoad();
        int i = 0;
        List<WebElement> Savings = driver.getElementsWithText(CartPage.cartItemRowBy, ConstantsDtc.FIXED_DISCOUNT);
        for (WebElement saving : Savings) {
            String expectedPromotion = driver.scenarioData.cartInstantPromotionPrice.get(i);
            String instantSavingText = saving.getText().split("-")[0].trim();
            String instantSavingPrice = saving.getText().split("\\-\\$")[1].trim();
            Assert.assertTrue(
                    "FAIL: Instant Savings displayed not matching with Instant Savings applied from Cart Page!",
                    expectedPromotion.contains(instantSavingText) &&
                            expectedPromotion.contains(instantSavingPrice));
            i++;
        }
        Assert.assertNotEquals(0, driver.getElementsWithText(CartPage.cartItemRebateNameBy,
                ConstantsDtc.FIXED_DISCOUNT).size());
        LOGGER.info("assertInstantSavings completed for " + page);
    }

    /**
     * Verify the display of 'pay online or pay in store' on Checkout Page
     *
     * @param ropisBopis    pay online or pay in store
     * @param displayStatus displayed or not displayed
     */
    public void assertRopisBopisOptionDisplayStatus(String ropisBopis, String displayStatus) {
        LOGGER.info("assertRopisBopisOptionDisplayStatus started verifying '" + ropisBopis + "' is " + displayStatus);
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        WebElement optionBy = driver.getElementWithText(paymentTabBy, ConstantsDtc.PAY_IN_STORE);
        if (ropisBopis.equalsIgnoreCase(ConstantsDtc.PAY_ONLINE))
            optionBy = driver.getElementWithText(paymentTabBy, ConstantsDtc.PAY_ONLINE);
        if (displayStatus.equalsIgnoreCase(Constants.DISPLAYED)) {
            Assert.assertTrue("FAIL:  '" + ropisBopis + "' not displayed!",
                    driver.isElementDisplayed(optionBy, Constants.ONE));
        } else {
            Assert.assertTrue("FAIL:  '" + ropisBopis + "' is displayed!",
                    !driver.isElementDisplayed(optionBy, Constants.ONE));
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("assertRopisBopisOptionDisplayStatus completed verifying '" + ropisBopis + "' is " + displayStatus);
    }

    /**
     * Set the amount to pay on the split payment
     *
     * @param amount      - Either the word 'half' or a numeric value that can be converted to a Double
     * @param paymentForm - 'primary' or 'secondary'. Determines which of the two forms to enter data into.
     */
    public void setSplitPayment(String amount, String paymentForm) {
        LOGGER.info("setSplitPayment started");
        Double newAmount = 0.00;
        List<WebElement> amountFields = driver.getDisplayedElementsList(amountBy);
        String amountValue = amountFields.get(0).getAttribute(Constants.VALUE);
        Double dollarAmount = Double.parseDouble(webDriver.findElements(paymentDetailsFormTotalPriceBy).
                get(0).getText().replace("$", "").trim());
        int formIndex = commonActions.getPaymentFormIndex(paymentForm);

        if (amount.equalsIgnoreCase(Constants.HALF)) {
            newAmount = dollarAmount / 2;
        } else {
            try {
                newAmount = Double.parseDouble(amount);
            } catch (NumberFormatException e) {
                e.printStackTrace();
                Assert.fail("FAIL: Invalid amount: '" + amount +
                        "'. The amount specified must be either a decimal or the word 'half'");
            }
        }

        String newAmountText = String.format("%,.2f", newAmount);
        driver.waitForMilliseconds();
        amountFields.get(formIndex).click();
        // Backspace over previous value. When using clear() function, it is retained and gets appended to new value.
        while (!amountFields.get(formIndex).getAttribute(Constants.VALUE).equals("")) {
            amountFields.get(formIndex).sendKeys(Keys.BACK_SPACE);
        }
        amountFields.get(formIndex).sendKeys(newAmountText);
        LOGGER.info("setSplitPayment completed");
    }

    /**
     * Click the 'SAVE CARD DETAILS' button at the bottom of the credit card form
     */
    public void clickSaveCardDetails() {
        LOGGER.info("clickSaveCardDetails started");
        commonActions.clickFormSubmitButtonByText(ConstantsDtc.SAVE_CARD_DETAILS);
        driver.waitSeconds(Constants.THREE);
        LOGGER.info("clickSaveCardDetails completed");
    }

    /**
     * Scrolls to and selects the date slot (row) for the specified holiday type (fullday or partial)
     *
     * @param holidayType fullday (store closed for full day) or partial (store closed for only part of the day)
     */
    public void scrollTimeSlotToHoliday(String holidayType) {
        LOGGER.info("scrollTimeSlotToHoliday started with holiday type: \"" + holidayType + "\"");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(apptDateContainer);

        if (holidayType.equalsIgnoreCase(FULLDAY)) {
            selectFulldayHolidayDate();
        } else {
            selectPartialHolidayDate();
        }

        LOGGER.info("scrollTimeSlotToHoliday completed with holiday type: \"" + holidayType + "\"");
    }

    /**
     * Iterates over the currently displayed date rows, looking for the store closed due to partial holiday message
     * container
     */
    private void selectPartialHolidayDate() {
        LOGGER.info("selectPartialHolidayDate started");
        boolean partialHolidayFound = false;

        List<WebElement> apptDatesList = webDriver.findElements(CommonActions.apptDetailsSelectDateActiveBy);
        for (WebElement apptDate : apptDatesList) {
            driver.jsScrollToElement(apptDate);
            apptDate.click();

            if (driver.isElementDisplayed(partialHolidayClosedMessage, Constants.ONE)) {
                partialHolidayFound = true;
                break;
            }
        }

        Assert.assertTrue("FAIL: Could not find any dates considered partial holidays!", partialHolidayFound);
        LOGGER.info("selectPartialHolidayDate completed");
    }

    /**
     * Iterates over the currently displayed date rows, looking for the text of the row to contain either "Store Closed"
     * or "Thanksgiving Day" (should always be present in QA environments). Once the fullday holiday is found, method
     * looks for the first day with available appointment times after the holiday. If none are found, method attempts
     * to look for days before the holiday.
     */
    private void selectFulldayHolidayDate() {
        LOGGER.info("selectFulldayHolidayDate started");
        List<WebElement> apptDatesList = webDriver.findElements(allAvailableApptDatesBy);

        for (WebElement apptDate : apptDatesList) {
            if (apptDate.getText().contains(ConstantsDtc.THANKSGIVING_DAY)
                    || apptDate.getText().contains(ConstantsDtc.STORE_CLOSED)) {
                int fullHolidayDateIndex = apptDatesList.indexOf(apptDate);
                int nextPrevControlClicks = 0;
                boolean dateAfterHolidayFound = false;
                WebElement closestAvailableApptDate = null;

                for (int i = fullHolidayDateIndex; i < apptDatesList.size(); i++) {
                    if (apptDatesList.get(i).getText().contains(VIEW_TIMES)) {
                        closestAvailableApptDate = apptDatesList.get(i);
                        dateAfterHolidayFound = true;
                        break;
                    }
                    nextPrevControlClicks++;
                }

                if (!dateAfterHolidayFound) {
                    for (int i = fullHolidayDateIndex; i >= 0; i--) {
                        if (apptDatesList.get(i).getText().contains(VIEW_TIMES)) {
                            closestAvailableApptDate = apptDatesList.get(i);
                            break;
                        }
                        nextPrevControlClicks++;
                    }
                }

                if (closestAvailableApptDate == null)
                    Assert.fail("FAIL: Could not find any dates before OR after the holiday with available appointment"
                            + " times!");

                driver.jsScrollToElement(closestAvailableApptDate);
                closestAvailableApptDate.click();
                driver.waitForMilliseconds();
                navToFulldayHolidayDate(nextPrevControlClicks, dateAfterHolidayFound);
            }
        }
        LOGGER.info("selectFulldayHolidayDate completed");
    }

    /**
     * Uses the 'Popular Times' control in the 'Appointment Details' section to navigate to the fullday holiday
     * date. If a date is found after the fullday holiday, the number of clicks from that date back (or forward) to the
     * holiday are then used with the prev (or next) control. End result of this method is date focus has been moved to
     * the fullday holiday (since user cannot click to select that day)
     *
     * @param nextPrevControlClicks number of clicks from the first available date with appointment times back to the
     *                              fullday holiday
     * @param dateAfterHolidayFound True if a date with available appointment times was found after the holiday
     */
    private void navToFulldayHolidayDate(int nextPrevControlClicks, boolean dateAfterHolidayFound) {
        LOGGER.info("navToFulldayHolidayDate started with # of clicks set to: \"" + nextPrevControlClicks
                + "\" and dateAfterHolidayFound: \"" + dateAfterHolidayFound + "\"");

        WebElement dateControl;
        if (dateAfterHolidayFound) {
            dateControl = prevDayArrowControl;
        } else {
            dateControl = nextDayArrowControl;
        }

        do {
            driver.jsScrollToElement(dateControl);
            dateControl.click();
            driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
            if (AppointmentPage.apptSelectedDate.getText().contains(ConstantsDtc.STORE_CLOSED)) {
                List<WebElement> allApptDatesList = webDriver.findElements(allAvailableApptDatesBy);
                int listSize = allApptDatesList.size();
                for (int index = 0; index < listSize; index++) {
                    if (allApptDatesList.get(index).getAttribute(Constants.CLASSNAME).
                            equals(AppointmentPage.apptSelectedDate.getAttribute(Constants.CLASSNAME))) {
                        if (dateControl == prevDayArrowControl)
                            allApptDatesList.get(index - 1).click();
                        else
                            allApptDatesList.get(index + 1).click();
                        driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
                        break;
                    }
                }
                nextPrevControlClicks--;
            }
            nextPrevControlClicks--;
        } while (nextPrevControlClicks > 0);

        LOGGER.info("navToFulldayHolidayDate completed with # of clicks set to: \"" + nextPrevControlClicks
                + "\" and dateAfterHolidayFound: \"" + dateAfterHolidayFound + "\"");
    }

    /**
     * Verifies the 'Store Schedule' bar graph is either displayed or not displayed
     *
     * @param displayExpectation True if the bar graph is expected to be displayed, otherwise False
     */
    public void verifyGraphShowingStoreScheduleDisplay(String displayExpectation) {
        LOGGER.info("verifyGraphShowingStoreScheduleDisplay started with displayedExpectation: \""
                + displayExpectation + "\"");
        driver.waitForElementVisible(apptDateContainer);
        boolean storeScheduleBarGraphDisplayed = driver.isElementDisplayed(barChartGraph, Constants.ONE);

        if (displayExpectation.equalsIgnoreCase(Constants.DISPLAYED)) {
            Assert.assertTrue("FAIL: The bar chart graph for the current store's schedule was NOT displayed!",
                    storeScheduleBarGraphDisplayed);
        } else {
            Assert.assertFalse("FAIL: The bar chart graph for the current store's schedule WAS displayed!",
                    storeScheduleBarGraphDisplayed);
        }
        LOGGER.info("verifyGraphShowingStoreScheduleDisplay completed with displayedExpectation: \""
                + displayExpectation + "\"");
    }

    /**
     * Verifies the 'First Available Appointment Time' message is either displayed or not displayed
     *
     * @param displayExpectation True if the messaging is expected to be displayed, otherwise False
     */
    public void verifyFirstAvailableAppointmentTimeMessageDisplay(String displayExpectation) {
        LOGGER.info("verifyFirstAvailableAppointmentTimeMessageDisplay started with displayedExpectation: \""
                + displayExpectation + "\"");
        driver.waitForElementVisible(apptDateContainer);
        boolean messageDisplayed = driver.isElementDisplayed(firstAvailableAppointmentTimeMessage,
                Constants.ONE);

        if (displayExpectation.equalsIgnoreCase(Constants.DISPLAYED)) {
            Assert.assertTrue("FAIL: The \"First Available Appointment Time\" message was NOT displayed!",
                    messageDisplayed);
        } else {
            Assert.assertFalse("FAIL: The \"First Available Appointment Time\" message WAS displayed!",
                    messageDisplayed);
        }
        LOGGER.info("verifyFirstAvailableAppointmentTimeMessageDisplay completed with displayedExpectation: \""
                + displayExpectation + "\"");
    }

    /**
     * Verify promotions display order for staggered/two products on Checkout Page
     */
    public void assertPromotionDisplayOrder() {
        LOGGER.info("assertPromotionDisplayOrder started");
        driver.waitForPageToLoad();
        List<WebElement> displayedSummary = webDriver.findElements(summaryProductDisplayBy);
        for (int i = 0; i < displayedSummary.size(); i++) {
            String summaryItem = displayedSummary.get(i).getAttribute(Constants.CLASS);
            if (!(Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD))) {
                Assert.assertTrue("FAIL: Displayed class on order summary: '" + summaryItem + "' expected: '"
                                + CART_SUMMARY_PROMOTIONS_DISPLAY_CHECKOUT_DTAT[i],
                        CART_SUMMARY_PROMOTIONS_DISPLAY_CHECKOUT_DTAT[i].equalsIgnoreCase(summaryItem));
            } else {
                Assert.assertTrue("FAIL: Displayed class on order summary: '" + summaryItem + "' expected: '"
                                + CART_SUMMARY_PROMOTIONS_DISPLAY_CHECKOUT_DTD[i],
                        CART_SUMMARY_PROMOTIONS_DISPLAY_CHECKOUT_DTD[i].equalsIgnoreCase(summaryItem));
            }
        }
        LOGGER.info("assertPromotionDisplayOrder completed");
    }

    /**
     * Verify Mail In rebate on checkout page
     *
     * @param page Web Page Header
     */
    public void assertMailInRebate(String page) {
        LOGGER.info("assertMailInRebate started for " + page);
        driver.waitForPageToLoad();
        int i = 0;
        List<WebElement> items = driver.getElementsWithText(CartPage.cartItemRebateNameBy, ConstantsDtc.MAIL_IN_REBATE);
        for (WebElement item : items) {
            String expectedPromotion = driver.scenarioData.cartMailInPromotion.get(i);
            String mailInRebate = item.getText().split("-")[0].trim();
            Assert.assertTrue("FAIL: Mail In Rebates displayed: " + mailInRebate
                            + " .not matching with Mail In Rebates applied from Cart Page!: " + expectedPromotion,
                    expectedPromotion.contains(mailInRebate));
            i++;
        }
        Assert.assertTrue("FAIL: Instant Savings not displayed on page " + page,
                !(items.size() == 0));
        LOGGER.info("assertMailInRebate completed for " + page);
    }

    /**
     * Select 'Review the Consent Disclosure & Terms of Agreement' checkbox and select options for consent and
     * agreement based on the passed parameters.
     *
     * @param consentToDisclosure       Whether or not to consent to the disclosure
     * @param agreeToTermsAndConditions Whether or not to agree to the terms of agreement
     */
    private void reviewConsentToDisclosureAndTermsOfAgreement(boolean consentToDisclosure,
                                                              boolean agreeToTermsAndConditions) {
        LOGGER.info("reviewConsentToDisclosureAndTermsOfAgreement started");
        driver.waitForMilliseconds();
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        int index = 0;
        int consentLabelCount = webDriver.findElements(CommonActions.careCareOneReviewLabel).size();
        Assert.assertTrue("There were no 'Review the Consent Disclosure & Terms of Agreement' links on " +
                "checkout payment page. Ensure the customer is a CC1 customer.", consentLabelCount > 0);
        WebElement consentLabel =
                webDriver.findElements(CommonActions.careCareOneReviewLabel).get(consentLabelCount - 1);
        Assert.assertTrue("The 'Review the Consent Disclosure & Terms of Agreement' link was not present on " +
                "checkout payment page for CC1 customer", driver.isElementDisplayed(consentLabel));
        driver.clickElementWithText(CommonActions.careCareOneReviewLabel,
                Constants.REVIEW_CONSENT_DISCLOSURE_AND_TERMS_OF_AGREEMENT);
        while (!driver.isElementDisplayed(webDriver.findElements(CommonActions.dtModalContainerBy).get(index))) {
            index++;
        }

        Assert.assertTrue("The '" + Constants.CONSENT_TO_DISCLOSURE.toUpperCase() + "' popup dialog did not display",
                webDriver.findElements(CommonActions.dtModalContainerBy).get(index).
                        findElement(CommonActions.dtModalHeaderBy).
                        findElement(CommonActions.dtModalTitleBy).getText().
                        equals(Constants.CONSENT_TO_DISCLOSURE.toUpperCase()));

        Assert.assertTrue("The '" + Constants.I_CONSENT.toUpperCase() + "' button was not displayed on the '" +
                        Constants.CONSENT_TO_DISCLOSURE.toUpperCase() + "' popup dialog",
                webDriver.findElements(CommonActions.dtModalContainerBy).get(index).
                        findElement(CommonActions.buttonLinkPrimaryBy).getText().
                        equals(Constants.I_CONSENT.toUpperCase()));

        Assert.assertTrue("The '" + Constants.DECLINE.toUpperCase() + "' button was not displayed on the '" +
                        Constants.CONSENT_TO_DISCLOSURE.toUpperCase() + "' popup dialog",
                webDriver.findElements(CommonActions.dtModalContainerBy).get(index).
                        findElement(CommonActions.buttonLinkDeclineBy).getText().
                        equals(Constants.DECLINE.toUpperCase()));

        if (!consentToDisclosure)
            webDriver.findElements(CommonActions.dtModalContainerBy).get(index).
                    findElement(CommonActions.buttonLinkDeclineBy).click();
        else {
            webDriver.findElements(CommonActions.dtModalContainerBy).get(index).
                    findElement(CommonActions.buttonLinkPrimaryBy).click();
            index++;

            driver.waitForPageToLoad();
            Assert.assertTrue("The '" + Constants.TERMS_OF_AGREEMENT.toUpperCase() + "' popup dialog did not display",
                    webDriver.findElements(CommonActions.dtModalContainerBy).get(index).
                            findElement(CommonActions.dtModalHeaderBy).
                            findElement(CommonActions.dtModalTitleBy).getText().
                            equals(Constants.TERMS_OF_AGREEMENT.toUpperCase()));

            Assert.assertTrue("The '" + Constants.ACCEPT_AMP_SUBMIT.toUpperCase() +
                            "' button was not displayed on the '" + Constants.TERMS_OF_AGREEMENT.toUpperCase() +
                            "' popup dialog",
                    webDriver.findElements(CommonActions.dtModalContainerBy).get(index).
                            findElement(CommonActions.buttonLinkPrimaryBy).getText().
                            equals(Constants.ACCEPT_AMP_SUBMIT.toUpperCase()));

            Assert.assertTrue("The '" + Constants.DECLINE.toUpperCase() + "' button was not displayed on the '" +
                            Constants.TERMS_OF_AGREEMENT.toUpperCase() + "' popup dialog",
                    webDriver.findElements(CommonActions.dtModalContainerBy).get(index).
                            findElement(CommonActions.buttonLinkDeclineBy).getText().
                            equals(Constants.DECLINE.toUpperCase()));

            if (agreeToTermsAndConditions)
                webDriver.findElements(CommonActions.dtModalContainerBy).get(index).
                        findElement(CommonActions.buttonLinkPrimaryBy).click();
            else
                webDriver.findElements(CommonActions.dtModalContainerBy).get(index).
                        findElement(CommonActions.buttonLinkDeclineBy).click();
        }
        LOGGER.info("reviewConsentToDisclosureAndTermsOfAgreement completed");
    }

    /**
     * Enters a specified value into the specified field of a specified form.
     * Useful for entering invalid values for negative testing.
     *
     * @param fieldName   - The name of the field to be populated.
     * @param value       - Nullable, the value to be entered.
     * @param paymentForm - 'primary' or 'secondary'. Determines which of the two forms to enter data into.
     */
    public void enterSpecifiedPaymentFieldValue(String fieldName, String value, String paymentForm) {
        enterPaymentFormFieldValue(fieldName, value, paymentForm, null);
    }

    /**
     * Enters the value for a specified credit card into the specified field of a specified form.
     *
     * @param fieldName   - The name of the field to be populated.
     * @param paymentForm - 'primary' or 'secondary'. Determines which of the two forms to enter data into.
     * @param cardType    - Nullable, the Credit Card type. Note: Use Card Type, such as "Visa Bopis", for
     *                    credit card number and cvv/cvn fields. Use Customer Type, such as DEFAULT_CUSTOMER_BOPIS_VISA,
     *                    for all other fields.
     */
    public void enterPaymentFieldValueForCreditCardType(String fieldName, String paymentForm, String cardType) {
        enterPaymentFormFieldValue(fieldName, null, paymentForm, cardType);
    }

    /**
     * Enters either a specified value or the value for a specific credit card into a specified field of a specified form.
     *
     * @param fieldName   - The name of the field to be populated.
     * @param value       - Nullable, the value to be entered.
     * @param paymentForm - 'primary' or 'secondary'. Determines which of the two forms to enter data into.
     * @param cardType    - Nullable, the Credit Card type. Note: Use Card Type, such as "Visa Bopis", for
     *                    credit card number and cvv/cvn fields. Use Customer Type, such as DEFAULT_CUSTOMER_BOPIS_VISA,
     *                    for all other fields.
     */
    private void enterPaymentFormFieldValue(String fieldName, String value, String paymentForm, String cardType) {
        LOGGER.info("enterPaymentFormFieldValue started for '" + fieldName + "' field in the '" +
                paymentForm + "' form");
        driver.waitForElementVisible(paymentTypesContainerBy);
        Customer cust = null;
        String expMonth = null;
        String expYear = null;

        if (fieldName.equalsIgnoreCase(CommonActions.ADDRESS_LINE_1) ||
                fieldName.equalsIgnoreCase(Constants.ZIP_CODE)) {
            selectDeselectUseExistingAddressForBilling(Constants.DESELECT);
        }

        WebElement formField = getFormFieldWebElement(fieldName, paymentForm);
        driver.jsScrollToElement(formField);
        if (value == null) {
            if (cardType != null) {
                switch (fieldName) {
                    case Constants.CREDIT_CARD_NUMBER:
                        setCreditCardForCustomer(cardType, customer);
                        value = customer.ccNum;
                        break;
                    case Constants.NAME_ON_CARD:
                        cust = customer.getCustomer(cardType);
                        value = cust.firstName + " " + cust.lastName;
                        break;
                    case Constants.EXP_DATE:
                        cust = customer.getCustomer(cardType);
                        expMonth = getCustomerCreditCardExpirationMonth(cust);
                        expYear = setCustomerCreditCardExpirationYear(cust);
                        selectCreditCardExpirationMonthYear(expMonth, expYear);
                        break;
                    case Constants.SECURITY_CODE:
                        setCreditCardForCustomer(cardType, customer);
                        if (CommonUtils.containsIgnoreCase(cardType, ConstantsDtc.CAR_CARE_ONE)) {
                            value = customer.cvv;
                        } else {
                            value = customer.cvn;
                        }
                        break;
                    case CommonActions.ADDRESS_LINE_1:
                        cust = customer.getCustomer(cardType);
                        value = cust.address1;
                        break;
                    case Constants.ZIP_CODE:
                        cust = customer.getCustomer(cardType);
                        value = cust.zip;
                        break;
                }
            } else {
                Assert.fail("FAIL: specified value and card type cannot both be null in enterPaymentFormFieldValue " +
                        "function");
            }
        } else {
            if (fieldName.equals(Constants.EXP_DATE)) {
                expMonth = value.substring(0, 2);
                expYear = value.substring(3, 5);
                selectCreditCardExpirationMonthYear(expMonth, expYear);
            }
        }

        if (!fieldName.equals(Constants.EXP_DATE)) {
            boolean done = false;
            while (!done) {
                try {
                    driver.jsScrollToElement(formField);
                    int counter = 0;
                    int maxAttempts = 20;
                    while (!formField.getAttribute(Constants.VALUE).trim().equals(value) && counter < maxAttempts) {
                        commonActions.clearEditField(formField);
                        driver.waitForMilliseconds(Constants.ONE_HUNDRED);
                        formField.sendKeys(value);
                        formField.click();
                        if (fieldName.equalsIgnoreCase(Constants.ZIP_CODE)) {
                            driver.waitOneSecond();
                            getFormFieldWebElement(CommonActions.ADDRESS_LINE_1, paymentForm).click();
                        }
                        counter++;
                    }
                    String actualValue = formField.getAttribute(Constants.VALUE).trim();
                    Assert.assertTrue("FAIL: Unable to set '" + value + "' in " + fieldName + " field. " +
                            "Actual value is: " + actualValue, actualValue.equals(value));
                    done = true;
                } catch (Exception e) {
                    driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
                    formField = getFormFieldWebElement(fieldName, paymentForm);
                }
            }
            // Click space and backspace to trigger the field when entering empty string in order to see error message
            if (value.equals("")) {
                formField.sendKeys(Keys.SPACE);
                formField.sendKeys(Keys.BACK_SPACE);
            }
        }

        LOGGER.info("enterPaymentFormFieldValue completed. '" + value + "' entered into '" + fieldName +
                "' field in the '" + paymentForm + "' form.");
    }

    /**
     * Get the WebElement for the specified form field name
     *
     * @param fieldName     Form field name
     * @param paymentForm   'primary' or 'secondary'. Determines which of the two forms to enter data into.
     * @return              WebElement representing the field
     */
    private WebElement getFormFieldWebElement(String fieldName, String paymentForm) {
        LOGGER.info("getFormFieldWebElement for '" + fieldName + "' field started");
        PaymentFields paymentFields = new PaymentFields().invokePaymentFields(paymentForm);
        switch (fieldName) {
            case Constants.CREDIT_CARD_NUMBER:
                return paymentFields.getCcNumber();
            case Constants.NAME_ON_CARD:
                return paymentFields.getCcName();
            case Constants.SECURITY_CODE:
                return paymentFields.getCcCvn();
            case CommonActions.ADDRESS_LINE_1:
                return paymentFields.getAddressLine1();
            case Constants.ZIP_CODE:
                return paymentFields.getBillingZipCode();
            default:
                return null;
        }
    }

    /**
     * Selects the "Edit card details" link for either the primary or secondary credit card on an order
     *
     * @param cardRole Primary or Secondary which decides which "Edit card details" link to select
     */
    public void editCardDetailsForCreditCard(String cardRole) {
        LOGGER.info("editCardDetailsForCreditCard started for card in the \"" + cardRole + "\" role");
        driver.waitForPageToLoad();
        WebElement paymentDetailsEditLink;
        WebElement paymentDetailsCardContainer = paymentDetailsPrimaryCard;
        driver.waitForElementVisible(paymentDetailsCardContainer);

        if (cardRole.equalsIgnoreCase(Constants.SECONDARY))
            paymentDetailsCardContainer = paymentDetailsSecondaryCard;

        paymentDetailsEditLink = paymentDetailsCardContainer.findElement(editCardDetailsLinkBy);
        driver.jsScrollToElement(paymentDetailsEditLink);
        paymentDetailsEditLink.click();
        LOGGER.info("editCardDetailsForCreditCard completed for card in the \"" + cardRole + "\" role");
    }

    /**
     * Verify the display of 'Someone else will pick up my order' on Checkout Page
     *
     * @param display displayed or not displayed
     */
    public void assertSomeoneElsePickUpMyOrderDisplay(String display) {
        LOGGER.info("assertSomeoneElsePickUpMyOrderDisplay started");
        if (display.equalsIgnoreCase(Constants.DISPLAYED)) {
            Assert.assertTrue("FAIL: 'Someone else will pick up my order' not displayed!",
                    driver.isElementDisplayed(someoneElsePickUpElementBy));
        } else {
            Assert.assertTrue("FAIL: 'Someone else will pick up my order' is displayed!",
                    !driver.isElementDisplayed(someoneElsePickUpElementBy));
        }
        LOGGER.info("assertSomeoneElsePickUpMyOrderDisplay completed");
    }

    /**
     * Enter an Expiration Date that is the month preceding the current month
     */
    public void enterExpirationDateFromThePast() {
        LOGGER.info("enterExpirationDateFromThePast started");
        calendar.add(Calendar.MONTH, -1);
        int month = calendar.get(Calendar.MONTH) + 1;
        int year = calendar.get(Calendar.YEAR);
        String expMonth = String.valueOf(month);
        String expYear = String.valueOf(year).substring(2, 4);
        selectCreditCardExpirationMonthYear(expMonth, expYear);
        LOGGER.info("enterExpirationDateFromThePast completed");
    }

    /**
     * Return 'true' or 'false' whether an error message that appears directly above the payment form when
     * invalid data is entered displayed
     *
     * @param errorMessage - The expected error message
     * @param paymentForm  - 'primary' or 'secondary'. Determines which of the two forms to enter data into.
     * @return 'true' or 'false' whether the error message was displayed
     */
    public boolean checkoutErrorMessageDisplayed(String errorMessage, String paymentForm) {
        LOGGER.info("checkoutErrorMessageDisplayed started for error message: '" + errorMessage + "' on " +
                paymentForm + " payment form");

        int waitCounter = 0;
        boolean errorDisplayed = false;
        int formIndex = commonActions.getPaymentFormIndex(paymentForm);

        if (driver.isElementDisplayed(checkoutErrorBy, Constants.ONE)) {
            List<WebElement> checkoutErrorMessages = webDriver.findElements(checkoutErrorBy);
            WebElement checkoutError = checkoutErrorMessages.get(formIndex);

            do {
                if (!CommonUtils.containsIgnoreCase(checkoutError.getText(), errorMessage)) {
                    driver.waitForMilliseconds();
                    waitCounter++;
                } else {
                    errorDisplayed = true;
                }
            } while (!errorDisplayed && waitCounter < 3);
        }

        LOGGER.info("checkoutErrorMessageDisplayed completed for error message: '" + errorMessage + "' on" +
                paymentForm + " payment form");
        return errorDisplayed;
    }

    /**
     * Verify the error message that is displayed when invalid data is entered into the payment form
     *
     * @param errorMessage      - The expected error message
     * @param paymentForm       - 'primary' or 'secondary'. Determines which of the two forms to enter data into.
     * @param expectedDisplayed - true or false whether to expect the error message to be displayed.
     */
    public void assertPaymentFormInvalidDataErrorMessage(String errorMessage, String paymentForm,
                                                         boolean expectedDisplayed) {
        LOGGER.info("assertPaymentFormInvalidDataErrorMessage started for error message: '" + errorMessage + "' on " +
                paymentForm + " payment form");

        driver.waitForMilliseconds();
        boolean displayed = checkoutErrorMessageDisplayed(errorMessage, paymentForm);

        if (!displayed) {
            displayed = commonActions.formGroupErrorMessageDisplayed(errorMessage, paymentForm);
        }

        if (displayed && !expectedDisplayed) {
            Assert.fail("FAIL: Unexpected error message '" + errorMessage + "' was displayed");
        }

        if (!displayed && expectedDisplayed) {
            Assert.fail("FAIL: Expected error message '" + errorMessage + "' was not displayed");
        }

        LOGGER.info("assertPaymentFormInvalidDataErrorMessage completed for error message: '" + errorMessage + "' on " +
                paymentForm + " payment form");
    }

    /**
     * Enter a past expiration date into the payment form unless it is currently January. Click Place Order, and verify
     * "Future date must supersede the current date" error message is displayed.
     *
     * @param form - "primary" (first payment form) or "secondary" (second payment form).
     */
    public void enterPastExpirationDateAndVerifyErrorMessage(String form) {
        LOGGER.info("enterPastExpirationDateAndVerifyErrorMessage started");
        if (calendar.get(Calendar.MONTH) == Calendar.JANUARY) {
            LOGGER.info("Don't test previous expiration date if current month is January because previous year is " +
                    "not an available selection");
        } else {
            enterExpirationDateFromThePast();
            commonActions.clickFormSubmitButtonByText(ConstantsDtc.PLACE_ORDER);
            assertPaymentFormInvalidDataErrorMessage(ConstantsDtc.FUTURE_DATE_MUST_SUPERCEDE_CURRENT_DATE, form, true);
        }
        LOGGER.info("enterPastExpirationDateAndVerifyErrorMessage completed");
    }

    /**
     * Verify the amount displayed in the specified payment form
     *
     * @param paymentForm - 'primary' or 'secondary'. Determines which of the two forms to enter data into.
     * @param amount
     */
    public void verifyPaymentFormAmount(String paymentForm, String amount) {
        LOGGER.info("verifyPaymentFormAmount started for '" + amount + "' in the '" + paymentForm + "' payment form");

        int formIndex = commonActions.getPaymentFormIndex(paymentForm);
        String displayedAmount = driver.getDisplayedElementsList(amountBy).get(formIndex).getAttribute(Constants.VALUE);

        if (amount.equalsIgnoreCase(Constants.ORDER_TOTAL)) {
            amount = String.valueOf(webDriver.findElements(paymentDetailsFormTotalPriceBy).
                    get(formIndex).getText().split("\\$")[1].trim());
        }

        Assert.assertTrue("FAIL: The expected amount '" + amount + "' was not displayed in the Ammount Summary " +
                "field in the '" + paymentForm + "'payment form", amount.equals(displayedAmount));

        LOGGER.info("verifyPaymentFormAmount completed for '" + amount + "' in the '" + paymentForm + "' payment form");
    }


    /**
     * Verify the two payment amounts from the primary and secondary payment forms equal the order total
     */
    public void verifySplitPaymentAmountsEqualOrderTotal() {
        LOGGER.info("verifySplitPaymentAmountsEqualOrderTotal started");

        String primaryFormAmount = driver.getDisplayedElementsList(amountBy).
                get(CommonActions.PRIMARY_FORM_INDEX).getAttribute(Constants.VALUE);

        String secondaryFormAmount = driver.getDisplayedElementsList(amountBy).
                get(CommonActions.SECONDARY_FORM_INDEX).getAttribute(Constants.VALUE);

        String orderTotal = String.valueOf(commonActions.cleanMonetaryStringToDouble(
                webDriver.findElements(paymentDetailsFormTotalPriceBy).get(0).getText()));

        Assert.assertTrue("FAIL: The sum of the primary and secondary payment amounts do not equal the order total!",
                Double.parseDouble(primaryFormAmount) + Double.parseDouble(secondaryFormAmount) ==
                        Double.parseDouble(orderTotal));

        LOGGER.info("verifySplitPaymentAmountsEqualOrderTotal completed");
    }

    /**
     * Save the Web Order Number to excel file
     *
     * @param file             File name with full file path
     * @param tabName          Excel sheet tab name
     * @param columnHeaderName The name of the column in which to store the data
     * @param append           'true' or 'false' whether to append to end of the data in the file
     */
    public void storeDtcOrderNumberToExcel(String file, String tabName, String columnHeaderName, boolean append) {
        LOGGER.info("storeDtcOrderNumberToExcel started for file: '" + file + "', tab: '" + tabName +
                "', column header: '" + columnHeaderName + "'");
        int columnIndex = -1;

        try {
            columnIndex = commonExcel.excelGetColumnIndex(file, tabName, columnHeaderName);
        } catch (InterruptedException e) {
            LOGGER.info(e.getMessage());
        }

        if (columnIndex < 0) {
            Assert.fail("The column index could not be retrieved for column header name: " + columnHeaderName);
        }

        storeDtcOrderNumberToExcel(file, tabName, columnIndex, append);
        LOGGER.info("storeDtcOrderNumberToExcel completed for file: '" + file + "', tab: '" + tabName +
                "', column header: '" + columnHeaderName + "'");
    }

    /**
     * Save the Web Order Number to excel file
     *
     * @param file        File name with full file path
     * @param tabName     Excel sheet tab name
     * @param columnIndex Zero-based index for column
     * @param append      'true' or 'false' whether to append to end of the data in the file
     */
    public void storeDtcOrderNumberToExcel(String file, String tabName, int columnIndex, boolean append) {
        LOGGER.info("storeDtcOrderNumberToExcel started");
        String orderNumber = OrderPage.orderNumber.getText().replaceAll("[^\\d]", "");
        try {
            commonExcel.saveDataToExcel(file, orderNumber, tabName, columnIndex, append);
        } catch (Exception e) {
            Assert.fail("FAIL: Exception occurred in writing '" + orderNumber + "' to the excel file: " + file + "'");
        }
        LOGGER.info("storeDtcOrderNumberToExcel completed for Order # " + orderNumber);
    }

    /**
     * This method will return the string value from the checkout page for the matched elementName
     *
     * @param elementName - Name of element on shopping cart page
     * @return - String value from the shopping cart page for the matched element
     */
    private String returnValueFromOrderSummary(String elementName) {
        LOGGER.info("returnValueFromOrderSummary started");
        String returnValue = null;
        switch (elementName) {
            case ConstantsDtc.SHOW_FEES:
                returnValue = expandFeeDetailsLabel.getText();
                break;
            case ConstantsDtc.CART_ITEMS:
                returnValue = CommonActions.expandCartItemsLabel.getText();
                break;
            case ConstantsDtc.QUANTITY:
                returnValue = webDriver.findElement(productQuantityBy).getText();
                break;
            case ConstantsDtc.ITEM_PRICE:
                returnValue = webDriver.findElement(summaryProductProductTotalBy).getText();
                break;
            case ConstantsDtc.TOTAL:
                returnValue = CommonActions.orderTotal.getText();
                break;
            case ConstantsDtc.TAXES:
                returnValue = checkoutAndOrderCartSummarySalesTax.getText();
                break;
            case ConstantsDtc.INSTALLATION_FEE:
                returnValue = cartPage.getInstallationPrice();
                break;
            case ConstantsDtc.ENVIRONMENTAL_FEE:
                returnValue = cartPage.getSpecialPricingOnCartPage(CartPage.feeDetailsItemsRowParentBy,
                        CartPage.feeDetailsItemsRowLabelBy, CartPage.feeDetailsItemsRowPriceBy,
                        ConstantsDtc.ENVIRONMENTAL_FEE);
                break;
            case ConstantsDtc.ENVIRONMENTAL_FEE + " " + ConstantsDtc.REAR:
                returnValue = cartPage.getSpecialPricingOnCartPage(cartFeeStaggeredRearBy,
                        CartPage.feeDetailsItemsRowLabelBy, CartPage.feeDetailsItemsRowPriceBy,
                        ConstantsDtc.ENVIRONMENTAL_FEE);
                break;
            case ConstantsDtc.DISPOSAL_FEE:
                returnValue = cartPage.getTireDisposalFee();
                break;
            case ConstantsDtc.SUBTOTAL:
                returnValue = subTotal.getText();
                break;
            case Constants.ADDRESS:
                returnValue = checkoutCartSummaryStoreName.getText();
                break;
            case ConstantsDtc.PRODUCT_BRAND:
                returnValue = productBrand.getText();
                break;
            case ConstantsDtc.PRODUCT_NAME:
                returnValue = productName.getText();
                break;
            case ConstantsDtc.PRODUCT_SIZE:
                returnValue = productSize.getText();
                break;
            default:
                Assert.fail("FAIL: Could not find By matched element name on the checkout page");
        }
        LOGGER.info("returnValueFromOrderSummary completed");
        return returnValue;
    }

    /**
     * This method will compare the order summary values from Cart and Checkout page  with the value on the Checkout
     * and Order Confirmation page
     *
     * @param elementName - Name of element on shopping cart/checkout page
     * @param key         - pass the key for the hash map
     */
    public void assertOrderSummaryDetails(String elementName, String key, String page) {
        LOGGER.info("assertOrderSummaryDetails started for " + page);
        String actualValue;
        if (driver.isTextPresentInPageSource(OrderPage.ORDER_CONFIRMATION_MESSAGE)) {
            actualValue = orderPage.returnValueFromOrderConfirmation(elementName);
        } else {
            actualValue = returnValueFromOrderSummary(elementName);
        }
        String expectedValue;
//      For DTD on checkout page all the applicable fees and taxes are recalculated based on the shipping address
        if (page.equalsIgnoreCase(ConstantsDtc.CHECKOUT) || page.equalsIgnoreCase(ConstantsDtc.ORDER_CONFIRMATION)) {
            expectedValue = checkoutOrderSummaryValues.get(key);
        } else {
            expectedValue = CartPage.hashMap.get(key);
        }
        Assert.assertTrue("FAIL: The value of field " + elementName + " = " + actualValue +
                " does not contain the value of  " + key + " = " + expectedValue,
                commonActions.cleanMonetaryStringToDouble(actualValue) ==
                        commonActions.cleanMonetaryStringToDouble(expectedValue));
        LOGGER.info("assertOrderSummaryDetails completed for " + page);
    }

    /**
     * Saving the values from checkout page in a hash map
     *
     * @param key         - value name from checkout page
     * @param elementName - case value name to get the string value from returnValueFromOrderSummary method
     */
    public void saveDataFromCheckoutPage(String elementName, String key) {
        LOGGER.info("saveDataFromShoppingCartPage started");
        checkoutOrderSummaryValues.put(key, returnValueFromOrderSummary(elementName));
        LOGGER.info("Values Stored are -->  " + key + " = " + checkoutOrderSummaryValues.get(key));
        LOGGER.info("saveDataFromShoppingCartPage completed");
    }

    /**
     * Verify 5 digit zip code is auto corrected to zip + 4 digit after doing continue to shipping method
     * Applicable only for DTD and US customers
     */
    public void assertAutoCorrectedZip(Customer customer) {
        LOGGER.info("validateAutoCorrectedZip started for " + customer);
        String expectedZip = customer.zipPlus4;
        String displayedZip = checkoutSummaryCustomerDetails.getText().split(",")[1].substring(4, 14);
        Assert.assertTrue("FAIL: The auto corrected zip: " + displayedZip + ", on shipping method page does " +
                "not match to expected zip: " + expectedZip, displayedZip.equals(expectedZip));
        LOGGER.info("validateAutoCorrectedZip completed for " + customer);
    }

    /**
     * Verify zip + 4 is displayed on shipping address page
     * Applicable only for DTD and US customers
     */
    public void assertZipPlus4(Customer customer) {
        LOGGER.info("assertZipPlus4 started for " + customer);
        String expectedZip = customer.zipPlus4;
        String displayedZip = CommonActions.orderSummaryZipCode.getAttribute(Constants.VALUE);
        Assert.assertTrue("FAIL: The displayed zip: " + displayedZip + ", on shipping details page does not " +
                "match to expected zip: " + expectedZip, displayedZip.equals(expectedZip));
        LOGGER.info("assertZipPlus4 completed for " + customer);
    }

    /**
     * Verify USPS Corrected Address display expected text on ADDRESS VERIFICATION modal
     * Applicable only for DTD and US customers
     *
     * @param zip         expected zip + 4
     * @param ContactType primary, secondary or default
     * @param page        My Account or Checkout
     */
    public void assertUspsCorrectedAddress(String zip, String page, String ContactType) {
        LOGGER.info("assertUspsCorrectedAddress started for " + zip);
        driver.waitForMilliseconds();
        WebElement contactType = null;
        if (page.equalsIgnoreCase(ConstantsDtc.MY_ACCOUNT)) {
            if (ContactType.equalsIgnoreCase(MyAccountPage.PRIMARY))
                contactType = webDriver.findElement(MyAccountPage.primaryContactInfoBy);
            else if (ContactType.equalsIgnoreCase(Constants.ALTERNATE))
                if (Config.isMobile())
                    contactType = webDriver.findElements(MyAccountPage.alternateContactAddressBy).get(1);
                else
                    contactType = webDriver.findElement(MyAccountPage.alternateContactAddressBy);
        } else if (page.equalsIgnoreCase(ConstantsDtc.CHECKOUT))
            contactType = uspsCorrectedAddress;
        String displayedUSPSZip = contactType.getText();
        Assert.assertTrue("FAIL: USPS Corrected Address display: " + displayedUSPSZip +
                " does not match to expected zip: " + zip, displayedUSPSZip.contains(zip));
        LOGGER.info("assertUspsCorrectedAddress completed for " + zip);
    }

    /**
     * Verify Edit Entered Address/Use USPS Corrected Address displayed on ADDRESS VERIFICATION modal
     * Applicable only for DTD and US customers
     *
     * @param inputSelection Edit Entered Address/Use USPS Corrected Address
     */
    public void assertAddressVerificationModal(String inputSelection) {
        LOGGER.info("assertAddressVerificationModal started for " + inputSelection);
        WebElement addressVerificationSelection = null;
        if (inputSelection.equalsIgnoreCase(ConstantsDtc.EDIT_ENTERED_ADDRESS))
            addressVerificationSelection = driver.getElementWithText(CommonActions.editEnteredAddressBy, inputSelection);
        else if (inputSelection.equalsIgnoreCase(ConstantsDtc.USE_USPS_CORRECTED_ADDRESS)) {
            addressVerificationSelection = driver.getElementWithText(CommonActions.useUspsCorrectedAddressBy, inputSelection);
        }
        Assert.assertTrue("FAIL: ADDRESS VERIFICATION modal not displayed for " + inputSelection,
                driver.isElementDisplayed(addressVerificationSelection));
        LOGGER.info("assertAddressVerificationModal completed for " + inputSelection);
    }

    /**
     * Select Edit Entered Address/Use USPS Corrected Address displayed on ADDRSS VERIFICATION modal
     * Applicable only for DTD and US customers
     *
     * @param inputSelection Edit Entered Address/Use USPS Corrected Address
     */
    public void selectOptionFromAddressVerificationModal(String inputSelection) {
        LOGGER.info("selectOptionFromAddressVerificationModal started for " + inputSelection);
        driver.waitForElementClickable(submitAddressVerification);
        if (Config.isMobile()) {
            commonActions.clickFormSubmitButtonByText(inputSelection);
        } else {
            driver.jsScrollToElementClick(driver.getElementWithText(submitAddressVerification, inputSelection));
        }
        LOGGER.info("selectOptionFromAddressVerificationModal completed for " + inputSelection);
    }

    /**
     * Assert that shipping address are pre-populated from the original order
     *
     * @param customerType customer user profile
     */
    public void assertPrePopulatedReadOnlyShippingAddress(String customerType) {
        LOGGER.info("assertPrePopulatedReadOnlyShippingAddress started");
        Customer expectedUser = customer.getCustomer(customerType);
        driver.waitForElementVisible(checkoutSummaryCustomerDetails);
        String shippingDetails = checkoutSummaryCustomerDetails.getText();
        Assert.assertTrue("FAIL: Pre-populated shipping address did not contain first name '"
                + expectedUser.firstName + "'", shippingDetails.contains(expectedUser.firstName));
        Assert.assertTrue("FAIL: Pre-populated shipping address did not contain last name '"
                + expectedUser.lastName + "'", shippingDetails.contains(expectedUser.lastName));
        Assert.assertTrue("FAIL: Pre-populated shipping address did not contain address line 1 '"
                + expectedUser.address1 + "'", shippingDetails.contains(expectedUser.address1));
        if (!expectedUser.address2.isEmpty())
            Assert.assertTrue("FAIL: Pre-populated shipping address did not contain address line 2 '"
                    + expectedUser.address2 + "'", shippingDetails.contains(expectedUser.address2));
        Assert.assertTrue("FAIL: Pre-populated shipping address did not contain city '"
                + expectedUser.city + "'", shippingDetails.contains(expectedUser.city));
        Assert.assertTrue("FAIL: Pre-populated shipping address did not contain state '"
                + expectedUser.state + "'", shippingDetails.contains(expectedUser.state));
        Assert.assertTrue("FAIL: Pre-populated shipping address did not contain zip code '"
                + expectedUser.zip + "'", shippingDetails.contains(expectedUser.zip));
        Assert.assertTrue("FAIL: Pre-populated shipping address did not contain email '"
                + expectedUser.email + "'", shippingDetails.contains(expectedUser.email));
        Assert.assertTrue("FAIL: Pre-populated shipping address did not contain phone number '"
                + expectedUser.phone + "'", shippingDetails.contains(expectedUser.phone));
        LOGGER.info("assertPrePopulatedReadOnlyShippingAddress completed");
    }

    /**
     * Click the edit delivery email address link
     */
    public void clickEditDeliveryEmailAddress() {
        LOGGER.info("clickEditDeliveryEmailAddress started");
        driver.waitForElementClickable(checkoutSummaryEditLink);
        checkoutSummaryEditLink.click();
        LOGGER.info("clickEditDeliveryEmailAddress completed");
    }

    /**
     * Get the Geo customer
     */
    public Customer getGeoCustomer() {
        LOGGER.info("getGeoCustomer started");
        driver.waitForPageToLoad();
        String state = Config.getStateCode().toLowerCase();
        String geoCust = Constants.geoCustomer.get(state);
        Customer cust = customer.getCustomer(geoCust);
        LOGGER.info("getGeoCustomer completed");
        return cust;
    }

    /**
     * Click 'Pay Online' or 'Pay in Store' tab and set scenario data order type to BOPIS or ROPIS
     *
     * @param tabLabel - Text label of the tab: 'Pay Online' or 'Pay in Store'
     */
    public void selectPaymentTab(String tabLabel) {
        LOGGER.info("selectPaymentTab started for '" + tabLabel + "' tab");
        Assert.assertTrue("FAIL. The payment tabs were not displayed", driver.isElementDisplayed(paymentTabBy,
                Constants.TEN));
        if (tabLabel.equalsIgnoreCase(ConstantsDtc.PAY_ONLINE)) {
            driver.clickElementWithText(paymentTabBy, ConstantsDtc.PAY_ONLINE);
        } else {
            driver.clickElementWithText(paymentTabBy, ConstantsDtc.PAY_IN_STORE);
        }
        LOGGER.info("selectPaymentTab completed for '" + tabLabel + "' tab");
    }

    /**
     * Select or Deselect the Use Existing Address as Billing Address checkbox
     *
     * @param selectDeselect 'Select' or 'Deselect'. Case insensitive.
     */
    private void selectDeselectUseExistingAddressForBilling(String selectDeselect) {
        LOGGER.info("selectDeselectUseExistingAddressForBilling started");
        Assert.assertTrue("FAIL: Must pass 'Select' or 'Deselect' to selectDeselectUseExistingAddressForBilling",
                selectDeselect.equalsIgnoreCase(Constants.SELECT) ||
                        selectDeselect.equalsIgnoreCase(Constants.DESELECT));
        if (driver.isElementDisplayed(paymentDetailsBillingAddress)) {
            WebElement checkBox = paymentDetailsBillingAddress.findElement(CommonActions.checkboxInputBy);
            boolean clickCheckBox = false;
            if (selectDeselect.equalsIgnoreCase(Constants.SELECT)) {
                if (checkBox.getAttribute(ConstantsDtc.SELECTED) == null) {
                    clickCheckBox = true;
                }
            } else {
                if (checkBox.getAttribute(ConstantsDtc.SELECTED) != null &&
                        checkBox.getAttribute(ConstantsDtc.SELECTED).equals(String.valueOf(true))) {
                    clickCheckBox = true;
                }
            }
            if (clickCheckBox) {
                driver.jsScrollToElementClick(checkBox, false);
                driver.waitForMilliseconds();
            }
        }
        LOGGER.info("selectDeselectUseExistingAddressForBilling completed");
    }

    /**
     * set payment type to scenario data
     *
     * @param creditCard Name of the credit card to use for payment
     */
    public void setPaymentType(String creditCard) {
        LOGGER.info("setPaymentType started");
        String paymentType = creditCard;
        if (creditCard.toLowerCase().contains(ConstantsDtc.MASTER)) {
            paymentType = ConstantsDtc.MASTER;
        } else if (creditCard.toLowerCase().contains(ConstantsDtc.CAR_CARE_ONE.toLowerCase())) {
            paymentType = ConstantsDtc.CAR_CARE_ONE.toLowerCase();
        } else if (creditCard.toLowerCase().contains(CheckoutPage.VISA.toLowerCase())) {
            paymentType = CheckoutPage.VISA.toLowerCase();
        } else if (creditCard.toLowerCase().contains(CheckoutPage.AMERICAN_EXPRESS.toLowerCase())) {
            paymentType = CheckoutPage.AMEX.toLowerCase();
        }
        driver.scenarioData.genericData.put(OrderXmls.PAYMENT_TYPE, paymentType);
        LOGGER.info("setPaymentType completed");
    }

    /**
     * Write checkout customer populated data to scenario data
     *
     * @param customer Type of customer to pull data from and pass to payment fields
     */
    public void setPaymentScenarioData(Customer customer) {
        LOGGER.info("setPaymentScenarioData started");
        driver.scenarioData.genericData.put(Constants.Billing + " " + CommonActions.ADDRESS_LINE_1, customer.address1);
        driver.scenarioData.genericData.put(Constants.Billing + " " + ConstantsDtc.ZIP_CODE, customer.zip);
        driver.scenarioData.genericData.put(Constants.Billing + " " + Constants.CITY, customer.city);
        driver.scenarioData.genericData.put(Constants.Billing + " " + Constants.STATE, customer.state);
        if (!Config.getDataSet().equalsIgnoreCase(Constants.DTD)) {
            driver.scenarioData.genericData.put(Constants.Billing + " " + Constants.CITY, customer.city);
            driver.scenarioData.genericData.put(Constants.Billing + " " + Constants.STATE, customer.state);
        }
        driver.scenarioData.genericData.put(Constants.Billing + " " + Constants.TYPE, Constants.Billing);
        driver.scenarioData.genericData.put(Constants.Billing + " " + ConstantsDtc.COUNTRY,
                commonUtils.replaceStateCountryNameWithCode(customer.country));
        LOGGER.info("setPaymentScenarioData completed");
    }

    /**
     * Verify the summary cart data with saved data
     *
     * @param keyName : primary key value of saved data
     * @param number  : summary page product row number
     */
    public void assertSummaryCartDataWithSavedData(String keyName, int number) {
        LOGGER.info("assertSummaryCartDataWithSavedData started");
        String values[] = TireAndWheelPackagesPage.tireWheelPackageDetails.get(keyName);
        WebElement cartRow = webDriver.findElements(cartSummaryListBy).get(number - 1);
        for (String value : values) {
            driver.verifyTextDisplayed(cartRow, value);
        }
        LOGGER.info("assertSummaryCartDataWithSavedData completed");
    }
}