package dtc.pages;

import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.CommonUtils;
import utilities.Driver;

import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

/**
 * Created by aaronbriel on 9/22/16.
 */
public class CompareProductsPage {

    private final Driver driver;
    private final WebDriver webDriver;
    private final CommonActions commonActions;
    private final ItemAddedToYourCartPopupPage itemAddedToYourCartPopupPage;
    private final Logger LOGGER = Logger.getLogger(CompareProductsPage.class.getName());

    public CompareProductsPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        itemAddedToYourCartPopupPage = new ItemAddedToYourCartPopupPage(driver);
        PageFactory.initElements(webDriver, this);
    }

    private static final String BADGE = "BADGE";
    private static final String VALUE = "VALUE";
    private static final String GOOD = "GOOD";
    private static final String BETTER = "BETTER";
    private static final String BEST = "BEST";
    private static final String SIZE_SPECIFICATIONS = "Size specifications";
    private static final String TREAD_TRACTION_SPECIFICATIONS = "Tread and traction specifications";
    private static final String SAFETY_PERFORMANCE_SPECIFICATIONS = "Safety and performance specifications";
    private static final String SHIPPING_RESTRICTIONS = "Shipping Restrictions";
    private static final String EMPTY_STRING = "";
    private static final String PRODUCT_TITLE_BY_ITEM_NUMBER = "//a[contains(@href, \"%s\")]//h5";

    private static String[] compareDtValues = {
            "Wheel Diameter", "Aspect Ratio", "Section Width",
            "Overall Diameter", "Rim Width Range", "Weight", "Tread Depth", "Tread Grade", "Traction Grade",
            "Max Load", "Max PSI", "Speed Rating", "Temperature Grade", "Vendor Product Number / MPN"
    };

    private static String[] compareH3Values = {
            "customer reviews", "Size specifications", "Tread and traction specifications",
            "Safety and performance specifications"
    };

    private static String[] compareH3ValuesMobile = {
            "customer reviews", "Size specifications"
    };

    private static String[] compareH5Values = {
            "Overall Rating", "Ride Comfort", "Cornering/Steering", "Ride Noise", "Tread Life", "Dry Traction",
            "Wet Traction", "Winter Traction", "Buy tire again"
    };

    private static String[] compareH5ValuesMobile = {
            "Overall Rating", "Ride Comfort", "Cornering / Steering", "Ride Noise", "Tread Life", "Dry Traction",
            "Wet Traction", "Winter Traction", "Buy tire again"
    };

    private static String[] compareDivValues = {
            "Warranty", "Tread Grade", "Traction Grade", "Max Load", "Speed Rating", "Temperature Grade"
    };

    private static List<String> sizeSectionAttributes = new ArrayList<>(Arrays.asList("Wheel Diameter", "Aspect Ratio",
            "Section Width (Cross Section)", "Overall Diameter", "Rim Width Range", "Weight"));

    private static List<String> treadSectionAttributes = new ArrayList<>(Arrays.asList("Tread Depth",
            "Tread Grade", "Traction Grade"));

    private static List<String> safetyPerformanceSectionAttributes = new ArrayList<>(Arrays.asList("Max Load",
            "Max PSI", "Speed Rating", "Temperature Grade", "Vendor Product Number / MPN"));

    private static final By compareProductHeadersBy = By.cssSelector("[class*='compare-main-section__columns']");

    private static final By addToCartBy = By.cssSelector("[class*='compare container-box auto-compare'] [class*='add-to-cart__row'] button");

    private static final By compareMainSectionActionBy = By.className("compare-main-section__action");

    private static final By mobileColumnsBy = By.className("compare-column");

    private static final By mobileProductBy = By.className("compare-pdp__img-info");

    public static final By iconCloseBy = By.cssSelector("[class*='icon-close']");

    private static final By compareSectionHeaderBy = By.className("specWrap");

    private static final By compareDescriptionListTagBy = By.tagName("dl");

    private static final By compareDescriptionDataTagBy = By.tagName("dd");

    private static final By compareAddItemBy = By.className("js-compare-add");

    public static final By compareMainSectionBy = By.cssSelector("[class*='compare-main-section']");

    public static final By compareMobileProductsBy = By.cssSelector("[class*='compare-mobile-products']");

    private static final By addToPackageComparePageBy = By.xpath("//*[contains(@class,'compare-desktop in-package-flow')]//*[contains(text(),'Add to Package')]");

    public static final By goodBetterBestBadgeBy = By.className(BADGE);

    public static final By goodBetterBestBadgeOnTireImageBy = By.cssSelector("[class*='plp_image_content']");

    @FindBy(className = "compare")
    public static WebElement compareClass;

    @FindBy(className = "compare-mobile")
    public static WebElement compareMobile;

    @FindBy(id = "productgbbDlId")
    public static WebElement gBBContainer;

    @FindBy(linkText = "Remove All")
    public static WebElement removeAll;

    @FindBy(linkText = "Undo Remove")
    public static WebElement undoRemove;

    @FindBy(className = "icon-close")
    public static WebElement closeButton;

    /**
     * Select first item that has the Add To Cart button enabled on the Compare Products page to add to the shopping cart
     *
     * @param action View shopping cart or shop for wheels
     */
    public void addFirstAvailableItemToCart(String action) {
        LOGGER.info("addFirstAvailableItemToCart started");
        boolean itemFound = false;
        List<WebElement> addToCartButtons = webDriver.findElements(addToCartBy);
        for (WebElement addToCartButton : addToCartButtons) {
            try {
                driver.moveToElementClick(addToCartButton);
                if (action.equalsIgnoreCase(ConstantsDtc.VIEW_SHOPPING_CART))
                    itemAddedToYourCartPopupPage.clickViewShoppingCart();
                else if (action.equalsIgnoreCase(ConstantsDtc.CONTINUE_SHOPPING)) {
                    commonActions.closeModalWindow();
                } else {
                    itemAddedToYourCartPopupPage.clickContinueShopping();
                }
                itemFound = true;
                break;
            } catch (Exception ex) {
                // continue to next iteration
            }
        }
        Assert.assertTrue("FAIL: There were no enabled Add To Cart buttons to be clicked", itemFound);
        LOGGER.info("addFirstAvailableItemToCart completed");
    }

    /**
     * Returns the VALUE of the element associated with the passed in tag attribute
     *
     * @param row    Parent webelement
     * @param itemID The ID of the item you'd like returned
     * @return String
     */
    public String returnTagValue(WebElement row, String itemID) {
        LOGGER.info("returnTagValue started");
        WebElement addSection = row.findElement(compareMainSectionActionBy);
        List<WebElement> inputs = addSection.findElements(By.tagName("input"));
        String inputValue = null;

        for (WebElement input : inputs) {
            inputValue = input.getAttribute(VALUE);
            if (inputValue.equalsIgnoreCase(itemID)) {
                LOGGER.info("returnTagValue completed");
                return inputValue;
            }
        }
        LOGGER.info("returnTagValue completed");
        return inputValue;
    }


    /**
     * Verifies all the values in associated strings
     * compareDTValues, compareH3Values, compareH5Values and compareDivValues (above)
     * are present on the page
     */
    public void verifyCompareProductsPageElements() {
        LOGGER.info("verifyCompareProductsPageElements started");
        WebElement compareElement;

        if (Config.isMobile()) {
            compareElement = compareMobile;
        } else {
            compareElement = compareClass;
        }

        verifyTableValues(compareElement, compareDtValues);
        verifyTableValues(compareElement, compareDivValues);

        if (Config.isMobile()) {
            verifyTableValues(compareElement, compareH5ValuesMobile);
            verifyTableValues(compareElement, compareH3ValuesMobile);
        } else {
            verifyTableValues(compareElement, compareH5Values);
            verifyTableValues(compareElement, compareH3Values);
        }
        LOGGER.info("verifyCompareProductsPageElements ended");
    }


    /**
     * Method takes in String[] and verifies all values are present on page
     *
     * @param compareElement WebElement to use for mobile vs. web page
     * @param values         String of values that should be present on page
     */
    private void verifyTableValues(WebElement compareElement, String[] values) {
        LOGGER.info("verifyCompareElements started");
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        String compareElementText = compareElement.getText();
        driver.resetImplicitWaitToDefault();
        for (String value : values) {
            boolean isPresent;
            isPresent = compareElementText.toLowerCase().contains(value.toLowerCase());
            if (!value.equalsIgnoreCase(ConstantsDtc.WARRANTY)) {
                Assert.assertTrue("\"" + value + "\" was not found on Compare Products page!", isPresent);
            } else {
                if (isPresent) {
                    LOGGER.info("Warranty found on Compare Products page!");
                } else {
                    LOGGER.info("Warranty was not found on Compare Products page!");
                }
            }
        }
        LOGGER.info("verifyCompareElements ended");
    }


    /**
     * Verifies values being passed in are present on the compare products screen
     *
     * @param quantity Number of products being compared (2 or 3)
     */
    public void verifyTopProductDetails(int quantity) {
        LOGGER.info("verifyTopProductDetails started");
        String brand = "";
        String product = "";
        String price = "";
        String inventoryMessage = "";
        driver.waitForElementClickable(removeAll);

        if (Config.isMobile()) {
            List<WebElement> details = webDriver.findElements(mobileColumnsBy);
            int end = details.size() - 1;
            int index = 0;

            for (WebElement detail : details.subList(0, end)) {
                WebElement productElement = detail.findElement(mobileProductBy);
                WebElement inventoryMessageElement = detail.findElement(CommonActions.inventoryMessagesBy);
                product = commonActions.productInfoListGetValue(Constants.PRICE, index);
                inventoryMessage = commonActions.productInfoListGetValue(ConstantsDtc.INVENTORY_MESSAGE, index);
                Assert.assertTrue("FAIL: Expected PRODUCT: " + product + ", but actual PRODUCT was: " + productElement,
                        product.contains(productElement.getText()));
                Assert.assertTrue("FAIL: Expected inventory message mystore VALUE: " + inventoryMessage
                        + ", but actual VALUE was: " + inventoryMessageElement.getText(),
                        inventoryMessageElement.getText().contains(inventoryMessage));
                index++;
            }
        } else {
            List<WebElement> details = webDriver.findElements(compareProductHeadersBy);
            int productListSize = driver.scenarioData.productInfoList.size();
            int arrayStart = 1;
            int arrayEnd = quantity + 1;
            By headerElementBy = CommonActions.headerFifthBy;

            // TODO: Staggered fitment needs to be handled for mobile as well
            int staggeredCount = 1;
            if (CommonUtils.containsIgnoreCase(details.get(1).getText(), ConstantsDtc.REAR)) {
                headerElementBy = CommonActions.headerThirdBy;
                staggeredCount = 2;
            }

            for (WebElement detail : details.subList(arrayStart, arrayEnd)) {
                String actualValue = detail.getText();
                String actualBrandAndProduct = detail.findElement(headerElementBy).getText();
                String inventoryMessageMystore = "";
                String actualPrice = "";
                boolean found = false;
                int foundCount = 0;

                for (int index = 0; index < productListSize; index++) {
                    brand = commonActions.productInfoListGetValue(ConstantsDtc.BRAND, index);
                    product = commonActions.productInfoListGetValue(ConstantsDtc.PRODUCT, index);
                    price = commonActions.productInfoListGetValue(Constants.PRICE, index).replace("$", "");
                    inventoryMessage = commonActions.productInfoListGetValue(ConstantsDtc.INVENTORY_MESSAGE, index);
                    if (CommonUtils.containsIgnoreCase(CommonUtils.removeSpaces(actualBrandAndProduct),
                            CommonUtils.removeSpaces(product))) {
                        inventoryMessageMystore = detail.findElements(
                                CommonActions.inventoryMessagesBy).get(0).getText().replace(", ", " ");
                        if (driver.scenarioData.productInfoList.keySet().toArray()[index].toString().
                                contains(ConstantsDtc.REAR)) {
                            if (actualValue.split(ConstantsDtc.REAR)[1].contains(ConstantsDtc.SEE_PRICE_IN_CART)) {
                                actualPrice = ConstantsDtc.SEE_PRICE_IN_CART;
                            }
                            else {
                                int amountCount = actualValue.split(ConstantsDtc.REAR)[1].split("\\$").length - 1;
                                actualPrice = actualValue.split(ConstantsDtc.REAR)[1].split("\\$")[amountCount].
                                        split("\\s")[0].replace(".ea", "").replace("each", "").
                                        replace("/ea", "").trim();
                            }
                        } else {
                            if (actualValue.split(ConstantsDtc.REAR)[0].contains(ConstantsDtc.SEE_PRICE_IN_CART)) {
                                actualPrice = ConstantsDtc.SEE_PRICE_IN_CART;
                            }
                            else {
                                int amountCount = actualValue.split(ConstantsDtc.REAR)[0].split("\\$").length - 1;
                                actualPrice = actualValue.split(ConstantsDtc.REAR)[0].split("\\$")[amountCount].
                                        split("\\s")[0].replace("ea.", "").replace("each", "").
                                        replace("/ea", "").trim();
                            }
                        }

                        if (inventoryMessageMystore.replace("\n", " ").replace(" ", "").
                                contains(inventoryMessage.replace("\n", " ").replace(" ", "")) &
                                price.equals(actualPrice)) {
                            foundCount++;
                        }

                        if (foundCount == staggeredCount) {
                            found = true;
                            break;
                        }
                    }
                }

                Assert.assertTrue("FAIL: '" + brand + " " + product + "' was not found with inventory message '" +
                        inventoryMessage + "' and price = " + price + ". For staggered fitment, both FRONT and REAR " +
                        "tires need to be present.", found);
            }
        }
        LOGGER.info("verifyTopProductDetails completed");
    }

    /**
     * Verifies values Good, Better and Best appear on the page
     * as well as the associated color of each tag
     */
    public void assertGoodBetterBestBackgroundColor() {
        LOGGER.info("assertGoodBetterBestBackgroundColor started");
        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);
        List<WebElement> gbbBadges = null;
        String colorValueFourDigitBetter = "";
        String colorValueThreeDigitBetter = "";
        String colorValueFourDigitBest = "";
        String colorValueThreeDigitBest = "";
        List<WebElement> productColumns = webDriver.findElements(compareProductHeadersBy);
        if (CommonUtils.containsIgnoreCase(productColumns.get(1).getText(), ConstantsDtc.REAR)) {
            // Staggered fitment
            gbbBadges = webDriver.findElement(compareMainSectionBy).
                    findElements(goodBetterBestBadgeOnTireImageBy);
            colorValueFourDigitBetter = ConstantsDtc.BLUE_COLOR_FOUR_DIGIT_2;
            colorValueThreeDigitBetter = ConstantsDtc.BLUE_COLOR_THREE_DIGIT_2;
            colorValueFourDigitBest = ConstantsDtc.GREEN_COLOR_FOUR_DIGIT_2;
            colorValueThreeDigitBest = ConstantsDtc.GREEN_COLOR_THREE_DIGIT_2;
        } else {
            // Non-staggered fitment
            gbbBadges = gBBContainer.findElements(goodBetterBestBadgeBy);
            colorValueFourDigitBetter = ConstantsDtc.BLUE_COLOR_FOUR_DIGIT;
            colorValueThreeDigitBetter = ConstantsDtc.BLUE_COLOR_THREE_DIGIT;
            colorValueFourDigitBest = ConstantsDtc.GREEN_COLOR_FOUR_DIGIT;
            colorValueThreeDigitBest = ConstantsDtc.GREEN_COLOR_THREE_DIGIT;
        }

        for (WebElement gbbBadge : gbbBadges) {
            String text = gbbBadge.getText();
            String color = CommonUtils.removeSpaces(gbbBadge.getCssValue(Constants.BACKGROUND_COLOR));
            String colorValueFourDigit = "";
            String colorValueThreeDigit = "";
            boolean colorMatchFound = false;

            switch (text.toUpperCase()) {
                case GOOD:
                    colorValueFourDigit = ConstantsDtc.GREY_COLOR_FOUR_DIGIT;
                    colorValueThreeDigit = ConstantsDtc.GREY_COLOR_THREE_DIGIT;
                    break;
                case BETTER:
                    colorValueFourDigit = colorValueFourDigitBetter;
                    colorValueThreeDigit = colorValueThreeDigitBetter;
                    break;
                case BEST:
                    colorValueFourDigit = colorValueFourDigitBest;
                    colorValueThreeDigit = colorValueThreeDigitBest;
                    break;
            }

            if (color.equalsIgnoreCase(CommonUtils.removeSpaces(colorValueFourDigit)) ||
                    color.equalsIgnoreCase(CommonUtils.removeSpaces(colorValueThreeDigit)))
                colorMatchFound = true;

            Assert.assertTrue("FAIL: The expected color values (4 digit: \""
                            + colorValueFourDigit + "\" OR 3 digit: \"" + colorValueThreeDigit
                            + "\") did NOT match the actual color: \"" + color + "\"!",
                    colorMatchFound);
        }
        LOGGER.info("assertGoodBetterBestBackgroundColor Completed");
    }


    /**
     * Performs remove all actions
     */
    public void clickRemoveAll() {
        LOGGER.info("clickRemoveAll started");
        driver.waitForPageToLoad();
        driver.waitForElementVisible(removeAll);
        driver.jsScrollToElementClick(removeAll);
        if (Config.getBrowser().equalsIgnoreCase(Constants.SAFARI_BROWSER))
            driver.waitForMilliseconds();
        LOGGER.info("clickRemoveAll completed");
    }


    /**
     * Clicks X button next to the first item being compared
     */
    public void removeFirstProductOnCompareProducts() {
        LOGGER.info("removeFirstProductOnCompareProducts started");
        String brand = "";
        String product = "";
        String inventoryMessage = "";

        if (Config.isMobile()) {
            List<WebElement> columns = webDriver.findElements(mobileColumnsBy);
            for (WebElement column : columns) {
                if (column.isDisplayed()) {
                    product = column.findElement(mobileProductBy).getText();
                    inventoryMessage = column.findElement(
                            CommonActions.inventoryMessagesBy).getText().replace(", ", " ");
                    column.findElement(iconCloseBy).click();
                    break;
                }
            }
//            TODO: Revisit this for mobile
//            for (int index = 0; index < driver.scenarioData.productInfoList.size(); index++) {
//                productData = driver.scenarioData.productInfoList.get(index);
//                if (CommonUtils.containsIgnoreCase(productData, product) &&
//                        CommonUtils.containsIgnoreCase(productData, inventoryMessage)) {
//                    driver.scenarioData.productInfoList.remove(index);
//                    break;
//                }
//            }
        } else {
            List<WebElement> detail = webDriver.findElements(compareProductHeadersBy);
            String actualBrandAndProduct = "";
            String inventoryMessageMystore = "";
            driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
            try {
                actualBrandAndProduct = detail.get(0).findElement(CommonActions.headerFifthBy).getText();
                inventoryMessageMystore = detail.get(0).findElement(CommonActions.inventoryMessagesBy).
                        getText().replace(", ", " ");
            } catch (Exception e) {
                actualBrandAndProduct = detail.get(1).findElement(CommonActions.headerFifthBy).getText();
                inventoryMessageMystore = detail.get(1).findElement(CommonActions.inventoryMessagesBy).
                        getText().replace(", ", " ");
            }
            driver.resetImplicitWaitToDefault();
            int productListSize = driver.scenarioData.productInfoList.size();
            for (int index = 0; index < productListSize; index++) {
                brand = commonActions.productInfoListGetValue(ConstantsDtc.BRAND, index);
                product = commonActions.productInfoListGetValue(ConstantsDtc.PRODUCT, index);
                inventoryMessage = commonActions.productInfoListGetValue(ConstantsDtc.INVENTORY_MESSAGE, index);
                if (CommonUtils.containsIgnoreCase(
                        CommonUtils.removeSpaces(actualBrandAndProduct + inventoryMessageMystore),
                        CommonUtils.removeSpaces(brand + product + inventoryMessage))) {
                    commonActions.removeProductInfoListItem(brand, product);
                    break;
                }
            }
            driver.waitForElementVisible(closeButton);
            closeButton.click();
        }
        LOGGER.info("removeFirstProductOnCompareProducts completed");
    }

    /**
     * Performs undo-remove all actions
     */
    public void clickUndoRemove() {
        LOGGER.info("clickUndoRemove started");
        driver.jsScrollToElementClick(undoRemove);
        //TODO: IE change - may make this a conditional
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        LOGGER.info("clickUndoRemove completed");
    }

    /**
     * Verifies the expected attributes for a given section of the comparison page are displayed
     *
     * @param compareSection The section of the compare page containing the attributes to be validated
     */
    public void verifyExpectedAttributesForCompareSection(String compareSection) {
        LOGGER.info("verifyExpectedAttributesForCompareSection started for compare page section: '"
                + compareSection + "'");
        driver.waitForPageToLoad();
        driver.waitForElementClickable(removeAll);
        List<String> expectedAttributesList = null;

        switch (compareSection) {
            case SIZE_SPECIFICATIONS:
                expectedAttributesList = sizeSectionAttributes;
                break;
            case TREAD_TRACTION_SPECIFICATIONS:
                expectedAttributesList = treadSectionAttributes;
                break;
            case SAFETY_PERFORMANCE_SPECIFICATIONS:
                expectedAttributesList = safetyPerformanceSectionAttributes;
                break;
            default:
                Assert.fail("FAIL - Did not recognize the compare section to validate! " +
                        "Please verify section arg matches the displayed case and/or wording.");
        }

        commonActions.verifyPageSectionContainsAttributes(compareSection, compareSectionHeaderBy,
                expectedAttributesList);
        LOGGER.info("verifyExpectedAttributesForCompareSection completed for compare page section: '"
                + compareSection + "'");
    }

    /**
     * Verifies the 'Add to cart' buttons on the Compare Products page are all enabled
     */
    public void verifyAddToCartEnabledForProducts() {
        LOGGER.info("verifyAddToCartEnabledForProducts started");
        driver.waitForPageToLoad();
        driver.waitForElementClickable(removeAll);

        List<WebElement> addToCartButtonsList = webDriver.findElements(addToCartBy);
        for (WebElement addToCartButton : addToCartButtonsList) {
            Assert.assertTrue("FAIL: One of the 'Add to cart' buttons was NOT enabled!",
                    addToCartButton.isEnabled());
        }
        LOGGER.info("verifyAddToCartEnabledForProducts completed");
    }

    /**
     * Verifies that all the products on the compare products page contain the '...cannot ship ____ items...' messaging
     * and listed locations. IF displayStatus == Display, the method allows for up to one cell to contain empty string,
     * accounting for an instance where only two products are compared. ELSE method validates the
     * 'Shipping Restrictions' row is not displayed on the Compare Products page.
     *
     * @param displayStatus Determines if the 'Shipping Restrictions' row (and messaging) should or should not be
     *                      displayed for the currently compared products
     */
    public void verifyCannotShipMessageForAllProducts(String displayStatus) {
        LOGGER.info("verifyCannotShipMessageForAllProducts started");
        driver.waitForPageToLoad();
        driver.waitForElementClickable(removeAll);
        String locationValidationMessaging = ConstantsDtc.CANNOT_SHIP_LOCATIONS;

        WebElement shippingRestrictionRowEle = driver.getElementWithText(compareDescriptionListTagBy,
                SHIPPING_RESTRICTIONS);

        if (displayStatus.equalsIgnoreCase(Constants.DISPLAYED)) {
            List<WebElement> shippingRestrictionCellList = shippingRestrictionRowEle.findElements
                    (compareDescriptionDataTagBy);

            int cellsMissingWarningMessage = 0;
            for (WebElement cell : shippingRestrictionCellList) {
                String cellText = cell.getText();

                if (cellText.equalsIgnoreCase(EMPTY_STRING)) {
                    cellsMissingWarningMessage++;
                } else {
                    Assert.assertTrue("FAIL: Shipping Restrictions cell text did not contain expected warning"
                                    + " message! \n\t\tExpected string: '" + ConstantsDtc.CANNOT_SHIP_ITEMS_WARNING
                                    + "' \n\t\tActual string: '" + cellText + "'",
                            cellText.contains(ConstantsDtc.CANNOT_SHIP_ITEMS_WARNING));

                    if (Config.isSafari()) {
                        locationValidationMessaging = ConstantsDtc.CANNOT_SHIP_LOCATIONS_SAFARI;
                        cellText = cellText.replaceAll("[\\r\\n\\t]+", "");
                    }

                    Assert.assertTrue("FAIL: Shipping Restrictions cell text did not contain expected "
                                    + "locations! \n\t\tExpected string: '" + locationValidationMessaging
                                    + "' \n\t\tActual string: '" + cellText + "'",
                            cellText.contains(locationValidationMessaging));
                }

                Assert.assertTrue("FAIL: More than one of the compared products is missing the"
                        + " '...cannot ship items...' messaging! Please verify items used are "
                        + "shipping restricted!", cellsMissingWarningMessage <= 1);
            }
        } else {
            Assert.assertTrue("FAIL: The 'Shipping Restrictions' row was displayed for compared products!",
                    shippingRestrictionRowEle == null);
        }
        LOGGER.info("verifyCannotShipMessageForAllProducts completed");
    }

    /**
     * Selects the item / product title by the specified item number
     *
     * @param itemNumber Item number of the product to select in order to view its product details
     */
    public void viewItemDetailsForProductByItemNumber(String itemNumber) {
        LOGGER.info("viewItemDetailsForProductByItemNumber started with item number: '" + itemNumber + "'");
        driver.waitForPageToLoad();
        driver.waitForElementClickable(removeAll);

        webDriver.findElement(By.xpath
                (String.format(PRODUCT_TITLE_BY_ITEM_NUMBER, itemNumber))).click();
        LOGGER.info("viewItemDetailsForProductByItemNumber completed with item number: '" + itemNumber + "'");
    }

    /**
     * Selects "Add an item" to compare products
     */
    public void selectAddAnItemToCompare() {
        LOGGER.info("selectAddAnItemToCompare started");
        if (Config.isMobile()) {
            driver.waitForElementClickable(driver.getElementWithText(CommonActions.btnDefaultBy, ConstantsDtc.ADD_AN_ITEM));
            driver.getElementWithText(CommonActions.btnDefaultBy, ConstantsDtc.ADD_AN_ITEM).click();
        } else {
            driver.waitForElementClickable(compareAddItemBy);
            webDriver.findElement(compareAddItemBy).click();
        }
        driver.waitForPageToLoad();
        LOGGER.info("selectAddAnItemToCompare completed");
    }

    /**
     * Verify Inventory Message for set product on Compare page
     *
     * @param message  the expected Inventory Message
     * @param position Front/Rear
     * @param product  set Product Name
     */
    public void assertInventoryMessageForSetOnComparePage(String message, String position, String product) {
        LOGGER.info("assertInventoryMessageForSetOnComparePage started for " + position + "Tire");
        driver.waitForPageToLoad();
        WebElement parent = driver.getParentElement(
                driver.getParentElement(
                        driver.getElementWithText(CommonActions.headerFifthBy, product)));
        List<WebElement> frontRear = parent.findElements(CommonActions.headerSixthBy);
        for (WebElement name : frontRear) {
            if (name.getText().equalsIgnoreCase(position)) {
                String inventoryMessage = driver.getParentElement(name).findElement(
                        CommonActions.inventoryMessagesBy).getText();
                Assert.assertTrue("FAIL: The inventory message didn't match for product :" + product
                        + "with tire position" + position + " ! (displayed:-> '" + inventoryMessage
                        + "'  expected:-> '" + message + "'!", inventoryMessage.equals(message));
                break;
            }
            LOGGER.info("assertInventoryMessageForSetOnComparePage completed for " + position + "Tire");
        }
    }

    /**
     * Add's product to add to package based on item choice
     *
     * @param choice: Choice of product from compare page
     */
    public void addProductToPageInComparePage(String choice) {
        LOGGER.info("addProductToPageInComparePage started");
        choice = choice.replaceAll("[^\\d]", "");
        int index = Integer.valueOf(choice);
        driver.waitForElementVisible(addToPackageComparePageBy);
        List<WebElement> elements = webDriver.findElements(addToPackageComparePageBy);
        elements.get(index - 1).click();
        LOGGER.info("addProductToPageInComparePage completed");
    }
}