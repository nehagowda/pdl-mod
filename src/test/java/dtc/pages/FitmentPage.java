package dtc.pages;

import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.CommonUtils;
import utilities.Driver;

import java.sql.Time;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by aaronbriel on 1/30/17.
 */
public class FitmentPage {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private HomePage homePage;
    private MyVehiclesPopupPage myVehiclePopupPage;
    private final Logger LOGGER = Logger.getLogger(FitmentPage.class.getName());

    public FitmentPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        homePage = new HomePage(driver);
        myVehiclePopupPage = new MyVehiclesPopupPage(driver);
        PageFactory.initElements(webDriver, this);
    }


    private static final String[] FITMENT_TIRE_OPTIONS = {
            "TREADWELL\nTIRE GUIDE", "ON PROMOTION", "BEST SELLER", "HIGHEST RATED"
    };

    private static final String YEAR = "Year";
    private static final String MAKE = "Make";
    private static final String MODEL = "Model";
    private static final String TRIM = "Trim";
    public static final String ASSEMBLY = "Assembly";
    private static final String TIRE_WIDTH = "Tire Width";
    private static final String TIRE_DIAMETER = "Tire Diameter";
    private static final String WHEEL_WIDTH = "Wheel Width";
    private static final String BOLT_PATTERN = "Bolt Pattern";
    private static final String BRAND = "brand";
    public static final String SHOP_BY_VEHICLE = "Vehicle";
    private static final String SHOP_BY_SIZE = "Size";
    private static final String SHOP_BY_BRAND = "Brand";
    private static final String SHOP_BY_TIRE_SIZE = "Tire Size";
    private static final String VEHICLE_SEARCH = "Vehicle Search";
    private static final String TIRE_SIZE_SEARCH = "Tire Size Search";
    private static final String WHEEL_SIZE_SEARCH = "Wheel Size Search";
    private static final String PERIOD_DECIMAL = ".";
    private static final String ARROW_UP = "upwards";
    private static final String ARROW_DOWN = "downwards";
    private static final String NO_RESULTS_MESSAGE = " did not match any products that fit your current vehicle.";
    private static final String GREY_COLOR = "rgb(102, 102, 102)";
    private static final String ATTR_ARIA_DISABLED = "aria-disabled";
    private static final String FITMENT_GRID_OPTION_LIST = "fitment-options__option-list";
    private static final String FITMENT_GRID_SELECTIONS = "fitment-crumbs__selections";
    private static final String FITMENT_GRID_SELECTIONS_MESSAGE = "fitment-crumbs__selections-message";
    private static final String FITMENT_GRID_HELP_SECTION_TEXT = "fitment-help-section__help-text";
    private static final String FITMENT_GRID_TIRE_SIDEWALL_IMAGE = "fitment-help-section__help-image";
    private static final String FITMENT_GRID_ACTIVE_TAB = "fitment-entry-tabs__active-button";
    private static final String FITMENT_GRID_VEHICLES_MESSAGE = "fitment-recent-vehicles__recent-vehicles-message";
    private static final String FITMENT_LINK_ORANGE_COLOR_RGB = "rgb(255, 161, 13)";
    private static final String FITMENT_LINK_ORANGE_COLOR_RGBA = "rgba(255, 161, 13, 1)";
    private static final String FITMENT_GRID_RECENT_SELECTIONS_TITLE = "recent-selections-title";
    private static final String FITMENT_RECENT_SELECTION_LIST_ITEM = "selection-list-item";
    private static final String FITMENT_OPTIONS_DIVIDER = "fitment-options__divider";
    private static final String FITMENT_CONTAINER = "fitment-entry__container";

    private static final String MULTIPLE_SIZE = "multiple size options";
    private static final String TIRE_SIDEWALL = "tire sidewall";
    private static final String VEHICLE_MULTIPLE_SIZE_OPTIONS_MESSAGE = "*This vehicle has multiple size options";
    public static final String TIRE_SIDEWALL_HELP_MESSAGE = "Your tire sidewall has a series of numbers that show" +
            " your specific tire and wheel size. Match the numbers from your tire to one of the size options below.";
    public static final String TIRE_SIDEWALL_IMAGE_ALT_TEXT = "helpful tire size sidewall";
    private static final String ADD_VEHICLES_MESSAGE = "You may add up to 3 vehicles";
    private static final String RECENT_TIRE_SIZES = "recent tire sizes";
    private static final String RECENT_WHEEL_SIZES = "recent wheel sizes";
    private static final String VIEW = "View";
    private static final String ADVANCED_SEARCH = "Advanced Search";
    private static final String FITMENT_LABEL = "div[class*='image-";
    public static final String BACK_TO_MY_VEHICLES_BUTTON_TEXT = "Back to my vehicles";
    public static final String SUB_HEADER = "Sub Header";

    private String dropDownArrowUpXpath = "//div[contains(@class, \"%s\")]//div/i[contains(@class,'fa-angle-up')]";

    private String dropDownArrowDownXpath = "//div[contains(@class, \"%s\")]//div/i[contains(@class,'fa-angle-down')]";

    private String recentSelectionRemoveButtonXpath = "(//button[contains(@class, 'fitment-recent-selections__" +
            "remove-button')])[%s]";

    private static final String[] SHOP_BY_VEHICLE_DEFAULT_DROPDOWN_VALUES =
            {"Year", "Make", "Model", "Trim"};
    private static final String[] SHOP_BY_TIRE_SIZE_DEFAULT_DROPDOWN_VALUES =
            {"Width", "Ratio", "Diameter"};
    private static final String[] SHOP_BY_WHEEL_SIZE_DEFAULT_DROPDOWN_VALUES =
            {"Diameter", "Wheel Width", "Bolt Pattern"};
    private static final String[] SHOP_BY_BRAND_DEFAULT_DROPDOWN_VALUES =
            {"Search for a partial or full brand name..."};

    private static final String radioButtonActive = "fitment-component__radio-select--active";

    private static final String searchButtonDisabled = "fitment-component__find-button--disabled";

    private static final String searchButtonClassName = ".fitment-component__find-button";

    private static final String viewRecommendationClassName = ".dt-button-lg--primary";

    private static final int MAX_DEFAULT_OPTION_COUNT = 12;

    private static final By dropDownMenuBy = By.className("dropdown-menu");

    private static final By radioButtonBy = By.className("fitment-component__toggle-button");

    private static final By fitmentComponentOptionBy = By.className("fitment-component__toggle-label");

    private static final By reactSelectizeInputBy = By.className("resizable-input");

    private static final By selectOptionBy = By.className("option-wrapper");

    private static final By shopTabBy = By.className("fitment-component__shop-by-button");

    private static final By noResultsDescriptionBy = By.className("no-results__description");

    private static final By fitmentSubheaderBy = By.className("dt-sub-menu__button--selected");

    private static final By highFlotationTireSizeSectionBy = By.xpath("//hr[contains(@class,'fitment-options__divider" +
            "')]/following-sibling::ul");

    private static final By addNewVehicleBy = By.cssSelector("[class*='fitment-recent-vehicles__recent-vehicles-add']");

    private static final By removeRecentVehicleBy = By.cssSelector("[class*='fitment-recent-vehicles__remove-vehicle']");

    private static final By fitmentGridVisibleItemsBy = By.cssSelector("[class*='" + FITMENT_GRID_OPTION_LIST + "']");

    private static final By tiresLabelBy = By.cssSelector("[class*='fitment-vehicle-results__cta-label']");

    private static final By yearMakeBy = By.cssSelector("[class*='year-make']");

    private static final By modalTrimBy = By.cssSelector("[class*='model-trim']");

    public static final By fitmentOptionButtonBy = By.cssSelector("button[class*='fitment-options__option']");

    private static final By fitmentExistingVehicleBy =
            By.cssSelector("[class*='fitment-recent-vehicles__recent-vehicles-year-make']");

    public static final By noMatchFoundErrorMessageBy = By.cssSelector("[class*='fitment-by-vehicle__no-match']");

    public static final By tireOptionsBy = By.cssSelector("[class*='shop-tires-ctas__tire-cta-label']");

    public static final By recentVehicleFitmentBy = By.cssSelector("[class*='recent-vehicles-list-item']");


    @FindBy(className = "fitment-component__find-button")
    public static WebElement searchButton;

    @FindBy(className = "react-selectize-control")
    public static WebElement selectControl;

    @FindBy(className = "auto-fitment-year")
    public static WebElement dropDownYear;

    @FindBy(className = "auto-fitment-make")
    public static WebElement dropDownMake;

    @FindBy(className = "auto-fitment-model")
    public static WebElement dropDownModel;

    @FindBy(className = "auto-fitment-trim")
    public static WebElement dropDownTrim;

    @FindBy(className = "auto-fitment-assembly")
    public static WebElement dropDownAssembly;

    @FindBy(className = "auto-fitment-tire-width")
    public static WebElement dropDownTireWidth;

    @FindBy(className = "auto-fitment-aspect-ratio")
    public static WebElement dropDownAspectRatio;

    @FindBy(className = "auto-fitment-tire-diameter")
    public static WebElement dropDownTireDiameter;

    @FindBy(className = "auto-fitment-wheel-diameter")
    public static WebElement dropDownWheelDiameter;

    @FindBy(className = "auto-fitment-wheel-width")
    public static WebElement dropDownWheelWidth;

    @FindBy(className = "auto-fitment-wheel-bolt-pattern")
    public static WebElement dropDownWheelBoltPattern;

    @FindBy(className = "fitment-component__select-brand")
    public static WebElement dropDownBrand;

    @FindBy(className = "fitment-component__find-button")
    public static WebElement fitmentComponentFindButton;

    @FindBy(css = ".fitment-component__select-brand > div > div")
    public static WebElement dropDownBrandTextPlaceholderElement;

    @FindBy(css = (".react-selectize.fitment-block__select--year"))
    public static WebElement dropDownYearWheel;

    @FindBy(className = "fitment-block__select--make")
    public static WebElement dropDownMakeWheel;

    @FindBy(className = "fitment-block__select--model")
    public static WebElement dropDownModelWheel;

    @FindBy(className = "fitment-block__select--trim")
    public static WebElement dropDownTrimWheel;

    @FindBy(className = "fitment-block__select--assembly")
    public static WebElement dropDownAssemblyWheel;

    @FindBy(css = "[class*=fitment-block__button]")
    public static WebElement seeWheelsButton;

    @FindBy(css = "[class*='fitment-options__show-more']")
    public static WebElement fitmentOptionShowMore;

    @FindBy(css = "[class*='fitment-vehicle-results__vehicle-details']")
    public static WebElement fitmentVehicleResults;

    @FindBy(className = "fitment-block__select--assembly")
    public static WebElement selectAssembly;

    /**
     * Selects dropdown value from specified fitment dropdown
     *
     * @param dropDown      The dropDown By (Year, Make, etc)
     * @param dropDownValue The value to select
     * @throws Exception General exception caught to allow for graceful failure
     */
    public void selectDropDownValue(WebElement dropDown, String dropDownValue) throws Exception {
        LOGGER.info("selectDropDownValue started");
        boolean optionsDisplayed = false;
        int maxAttempts = Constants.TEN;
        int attempt = Constants.ZERO;
        if (dropDownValue.equalsIgnoreCase(Constants.NONE)) {
            return;
        }
        do {
            driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
            //Expanding dropdown if it is not already
            if (!dropDown.getAttribute(Constants.CLASS).contains(Constants.OPEN)) {
                //Note: Menu only expands in ie/safari/mobile safari when clicking input element
                if (Config.isSafari() || Config.isIphone() || Config.isIpad() || Config.isIe()) {
                    dropDown = dropDown.findElement(reactSelectizeInputBy);
                } else if (Config.isFirefox()) {
                    dropDown = dropDown.findElement(CommonActions.reactSelectizePlaceholderBy);
                }
                driver.jsScrollToElementClick(dropDown, false);
            }
            if (CommonUtils.containsIgnoreCase(dropDown.getAttribute(Constants.CLASS), Constants.OPEN)) {
                optionsDisplayed = true;
            } else {
                if (Config.isSafari() || Config.isIphone() || Config.isIpad() || Config.isIe()) {
                    dropDown = dropDown.findElement(reactSelectizeInputBy);
                } else if (Config.isFirefox()) {
                    dropDown = dropDown.findElement(CommonActions.reactSelectizePlaceholderBy);
                }
                driver.waitForMilliseconds(Constants.ONE_HUNDRED);
            }
            attempt++;
        } while (!optionsDisplayed && attempt < maxAttempts);

        Assert.assertTrue("FAIL: Not able to expand drop down list", optionsDisplayed);
        if (Config.isFirefox()) {
            dropDown.findElement(reactSelectizeInputBy).sendKeys(dropDownValue);
        }
        WebElement dropDownOption = getDropDownOptionElement(dropDownValue);
        driver.jsScrollToElementClick(dropDownOption, false);
        Assert.assertTrue("FAIL: The dropdown element values: '" + dropDown.getText().trim()
                        + " did NOT contain the expected value: '" + dropDownValue.trim() + "'!",
                dropDown.getText().trim().toLowerCase().contains(dropDownValue.trim().toLowerCase()));
        LOGGER.info("selectDropDownValue completed");
    }

    /**
     * Returns dropDown WebElement based on type passed in
     *
     * @param dropDownName The type of dropdown to return
     * @return WebElement   The dropDown to return
     */
    public WebElement getDropDown(String dropDownName) {
        WebElement dropDown = null;
        if (dropDownName.equalsIgnoreCase(YEAR)) {
            dropDown = dropDownYear;
        } else if (dropDownName.equalsIgnoreCase(MAKE)) {
            dropDown = dropDownMake;
        } else if (dropDownName.equalsIgnoreCase(MODEL)) {
            dropDown = dropDownModel;
        } else if (dropDownName.equalsIgnoreCase(TRIM)) {
            dropDown = dropDownTrim;
        } else if (dropDownName.equalsIgnoreCase(ASSEMBLY)) {
            dropDown = dropDownAssembly;
        } else if (dropDownName.equalsIgnoreCase(TIRE_WIDTH)) {
            dropDown = dropDownTireWidth;
        } else if (dropDownName.equalsIgnoreCase(TIRE_DIAMETER)) {
            dropDown = dropDownTireDiameter;
        } else if (dropDownName.equalsIgnoreCase(ConstantsDtc.ASPECT_RATIO)) {
            dropDown = dropDownAspectRatio;
        } else if (dropDownName.equalsIgnoreCase(WHEEL_WIDTH)) {
            dropDown = dropDownWheelWidth;
        } else if (dropDownName.equalsIgnoreCase(ConstantsDtc.WHEEL_DIAMETER)) {
            dropDown = dropDownWheelDiameter;
        } else if (dropDownName.equalsIgnoreCase(BOLT_PATTERN)) {
            dropDown = dropDownWheelBoltPattern;
        } else if (dropDownName.equalsIgnoreCase(BRAND)) {
            dropDown = dropDownBrand;
        }
        return dropDown;
    }

    /**
     * Returns dropDown WebElement based on type passed in
     *
     * @param dropDownValue The value of the dropdown option
     * @return WebElement   The dropDown option element to return
     */
    private WebElement getDropDownOptionElement(String dropDownValue) {

        LOGGER.info("getDropDownOptionElement started");

        List<WebElement> elements = webDriver.findElements(CommonActions.dropdownOptionBy);

        for (WebElement element : elements) {
            String elementText = element.getText();
            LOGGER.info("text:" + elementText);
            if (elementText.trim().contains(dropDownValue.trim()))
                return element;
        }
        return null;
    }

    /**
     * Conducts a vehicle search by selecting specified values from dropdown controls
     *
     * @param year     The year value to select
     * @param make     The make value to select
     * @param model    The model value to select
     * @param trim     The trim value to select
     * @param assembly The assembly value to select
     * @throws Exception Exception
     */
    public void vehicleSearch(String year, String make, String model,
                              String trim, String assembly) throws Exception {
        LOGGER.info("vehicleSearch started");
        driver.waitForPageToLoad(Constants.ONE_THOUSAND);
        selectDropDownValue(dropDownYearWheel, year);
        selectDropDownValue(dropDownMakeWheel, make);
        selectDropDownValue(dropDownModelWheel, model);
        selectDropDownValue(dropDownTrimWheel, trim);
        if (!assembly.equalsIgnoreCase(Constants.NONE)) {
            selectDropDownValue(dropDownAssemblyWheel, assembly);
        }
        driver.waitForElementClickable(seeWheelsButton);
        seeWheelsButton.click();
        driver.waitSeconds(Constants.FIVE);
        LOGGER.info("vehicleSearch completed");
    }

    /**
     * Conducts a search on the 'Size' tab based on the product type (TIRES or WHEELS). Method handles the selection
     * of the 'Size' tab, and the required sub-heading based on the product type. If the product type is 'WHEELS', the
     * size of the values list will be checked to see if it is an 'Advanced' (3 values) or 'Quick' (1 value) search
     *
     * @param searchType  Which fitment search control to use: homepage or via 'My Vehicles' modal
     * @param productType TIRES or WHEELS
     * @param values      String values representing the dimensions of the tire or wheel to be entered as search criteria
     */
    public void fitmentSizeSearch(String searchType, String productType, String values) {
        LOGGER.info("fitmentSizeSearch started with '" + searchType + "' search, for '" + productType
                + "' products, and values: '" + values + "'");
        List<String> valuesToSelectList = new ArrayList<>(Arrays.asList(values.split(",")));

        selectFitmentSizeOrBrandSearchType(searchType, SHOP_BY_SIZE.toUpperCase(), productType);

        for (String value : valuesToSelectList) {
            selectFitmentGridOption(true, value);
            driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
        }

        WebElement viewButton = driver.getDisplayedElement(driver.getElementsWithText(
                CommonActions.buttonBy, VIEW + " " + productType), Constants.THREE);
        viewButton.click();
        driver.waitForPageToLoad();
        LOGGER.info("fitmentSizeSearch completed with '" + searchType + "' search, for '" + productType
                + "' products, and values: '" + values + "'");
    }

    /**
     * Selects the 'Size' or 'Brand' fitment search tab and the desired sub-heading based on product type (TIRES or
     * WHEELS)
     *
     * @param searchType  Which fitment search control to use: homepage or via 'My Vehicles' modal
     * @param fitmentTab  Size or Brand
     * @param productType TIRES or WHEELS
     * @throws Exception Exception error
     */
    private void selectFitmentSizeOrBrandSearchType(String searchType, String fitmentTab, String productType) {
        LOGGER.info("selectFitmentSizeOrBrandSearchType started from '" + searchType + "' for '" + fitmentTab
                + "' with '" + productType + "' products");
        if (searchType.equalsIgnoreCase(ConstantsDtc.MY_VEHICLES)) {
            homePage.openMyVehiclesPopup();
            myVehiclePopupPage.clickAddVehicle();
            driver.waitForElementClickable(CommonActions.dtModalContainerBy);
        }
        commonActions.clickButtonByText(fitmentTab);
        commonActions.clickButtonByText(productType);
        LOGGER.info("selectFitmentSizeOrBrandSearchType completed from '" + searchType + "' for '" + fitmentTab
                + "' with '" + productType + "' products");
    }

    /**
     * Gets the correct search tab based on search type
     *
     * @param searchType Type of search to perform on page
     * @return Search type WebElement
     */
    //TODO: This could be refactored to use auto class names, might even look into methods that leverage this
    private WebElement getSearchTab(String searchType, String tab) {
        LOGGER.info("getSearchTab started");
        List<WebElement> tabList;

        if (searchType.equalsIgnoreCase(ConstantsDtc.MY_VEHICLES)
                && tab.equalsIgnoreCase(TIRE_SIZE_SEARCH)) {
            tabList = driver.getElementsWithText(shopTabBy, SHOP_BY_SIZE);
            LOGGER.info("getSearchTab completed");
            return driver.getDisplayedElement(tabList, Constants.TWO);
        } else if (searchType.equalsIgnoreCase(ConstantsDtc.MY_VEHICLES)
                && tab.equalsIgnoreCase(WHEEL_SIZE_SEARCH)) {
            tabList = driver.getElementsWithText(shopTabBy, SHOP_BY_SIZE);
            LOGGER.info("getSearchTab completed");
            return driver.getDisplayedElement(tabList, Constants.TWO);
        } else if (searchType.equalsIgnoreCase(ConstantsDtc.MY_VEHICLES)
                && tab.equalsIgnoreCase(VEHICLE_SEARCH)) {
            tabList = driver.getElementsWithText(shopTabBy, VEHICLE_SEARCH);
            LOGGER.info("getSearchTab completed");
            return driver.getDisplayedElement(tabList, Constants.TWO);
        } else {
            LOGGER.info("getSearchTab completed");
            return driver.getElementWithText(shopTabBy, SHOP_BY_SIZE);
        }
    }

    /**
     * Selects brand from brand drop down
     *
     * @param brandName The brand to select
     * @throws Exception Exception error
     */
    public void enterBrandName(String brandName) throws Exception {
        LOGGER.info("enterBrandName started");
        selectDropDownValue(dropDownBrand, brandName);
        LOGGER.info("enterBrandName completed");
    }

    /**
     * Expands specified fitment dropdown
     *
     * @param dropDownName The value in the dropDown (Year, Make, etc)
     * @throws Exception General exception caught to allow for graceful failure
     */
    public void expandFitmentDropdown(String dropDownName) throws Exception {
        LOGGER.info("expandFitmentDropdown started");

        try {

            //TODO: Needed for IE popup window due to on screen movement that cannot be manually replicated
            if (Config.isIe())
                driver.waitForMilliseconds(Constants.THREE);

            driver.waitForElementClickable(selectControl);

            WebElement dropDown = getDropDown(dropDownName);
            driver.waitForElementClickable(dropDown);

            driver.jsScrollToElement(dropDown);

            //Note: Menu only expands in ie/safari/mobile safari when clicking input element
            if (Config.isIe() || Config.isSafari() || Config.isIphone() || Config.isIpad()) {
                dropDown.findElement(reactSelectizeInputBy).click();
            } else {
                dropDown.click();
            }

        } catch (Exception e) {
            Assert.fail("FAIL: Expanding fitment dropdown menu \"" + dropDownName
                    + "\" FAILED with error: " + e);
        }
        LOGGER.info("expandFitmentDropdown completed");
    }

    /**
     * Clicks specified disabled fitment dropdown
     *
     * @param dropDownName The value in the dropDown (Year, Make, etc)
     * @throws Exception General exception caught to allow for graceful failure
     */
    public void clickDisabledFitmentDropdown(String dropDownName) throws Exception {
        LOGGER.info("clickDisabledFitmentDropdown started");
        try {
            //TODO: Needed for IE popup window due to on screen movement that cannot be manually replicated
            if (Config.isIe())
                driver.waitForMilliseconds(Constants.THREE);

            driver.waitForElementClickable(selectControl);

            WebElement dropDown = getDropDown(dropDownName);
            driver.waitForElementClickable(dropDown);

            driver.jsScrollToElement(dropDown);

            //Expanding dropdown if it is not already expanded
            if (!dropDown.getAttribute(Constants.CLASS).contains(Constants.OPEN)) {
                //Note: Menu only expands in ie/safari/mobile safari when clicking input element
                if (Config.isIe() || Config.isSafari() || Config.isIphone() || Config.isIpad()) {
                    dropDown.findElement(reactSelectizeInputBy).click();
                } else {
                    dropDown.click();
                }
            }

        } catch (Exception e) {
            LOGGER.info("Clicking disabled dropdown correctly failed.");
        }
        LOGGER.info("clickDisabledFitmentDropdown completed");
    }

    /**
     * Types the specified value in the given dropdown
     *
     * @param dropDownName The dropdown to type in
     * @param value        The value to type
     */
    public void typeValueInDropdown(String dropDownName, String value) {
        LOGGER.info("typeValueInDropdown started");

        try {
            WebElement dropDown = getDropDown(dropDownName);

            //TODO: Needed for IE popup window due to on screen movement that cannot be manually replicated
            if (Config.isIe())
                driver.waitForMilliseconds(Constants.THREE);

            driver.waitForElementClickable(selectControl);
            driver.waitForElementClickable(dropDown);

            //TODO: retest when geckodriver is updated & stabilized
            if (Config.isFirefox())
                driver.waitForMilliseconds(Constants.TWO_THOUSAND);

            driver.jsScrollToElement(dropDown);
            //Expanding dropdown if it is not already expanded
            if (!dropDown.getAttribute(Constants.CLASS).contains(Constants.OPEN)) {
                //Note: Menu only expands in ie/safari/mobile safari when clicking input element
                if (Config.isIe() || Config.isSafari() || Config.isIphone() || Config.isIpad()) {
                    dropDown.findElement(reactSelectizeInputBy).click();
                } else {
                    dropDown.click();
                }
            }

            //TODO: This fails on iPhone 10 simulator, possibly due to existing appium bug where
            //TODO (cont) keyboard not expanding. It does however work with iPhone 9.3.
            //TODO (cont) See https://github.com/appium/appium/issues/7868
            new Actions(webDriver).moveToElement(dropDown).sendKeys(value).perform();

            driver.waitForMilliseconds();
        } catch (Exception e) {
            Assert.fail("FAIL: Typing value \"" + value + "\" into dropdown menu \"" + dropDownName
                    + "\" FAILED with error (NOTE: This fails in iOS 10 due to Appium bug 7868, test manually.): " + e);
        }

        LOGGER.info("typeValueInDropdown completed");
    }

    /**
     * Selects a dropdown value from an already expanded menu
     *
     * @param dropDownValue The value in the dropDown to verify
     * @throws Exception General exception caught to allow for graceful failure
     */
    public void selectDropDownValueFromExpandedMenu(String dropDownValue) throws Exception {
        LOGGER.info("selectDropDownValueFromExpandedMenu started");
        try {
            List<WebElement> dropDownOptions = webDriver.findElements(selectOptionBy);

            for (WebElement dropDownOption : dropDownOptions) {
                if (dropDownOption.getText().trim().toLowerCase().contains(dropDownValue.toLowerCase())) {
                    driver.jsScrollToElement(dropDownOption);
                    dropDownOption.click();
                    break;
                }
            }

        } catch (Exception e) {
            Assert.fail("FAIL: Clicking dropdown value \"" + dropDownValue + "\" FAILED with error: " + e);
        }
        LOGGER.info("selectDropDownValueFromExpandedMenu completed");
    }

    /**
     * Clicks the radio button of the fitment window based on button type
     *
     * @param buttonType Wheels or Tires
     */
    public void clickFitmentRadioButton(String buttonType) {
        LOGGER.info("clickFitmentRadioButton started");
        WebElement fitmentRadioButton = driver.getElementWithText(fitmentComponentOptionBy, buttonType);
        driver.waitForElementClickable(fitmentRadioButton);
        driver.jsScrollToElementClick(fitmentRadioButton);
        driver.waitForPageToLoad();
        LOGGER.info("clickFitmentRadioButton completed");
    }

    /**
     * Clicks the Find Tires or Find Wheels button in fitment search modal
     */
    public void clickFindTiresOrWheels() {
        LOGGER.info("clickFindTires started");
        driver.waitForElementClickable(fitmentComponentFindButton);
        driver.jsScrollToElement(fitmentComponentFindButton);
        fitmentComponentFindButton.click();
        LOGGER.info("clickFindTires completed");
    }

    /**
     * Verifies that all fitment menus (tabs) are displayed
     */
    public void verifyFitmentMenusDisplayed() {
        LOGGER.info("verifyFitmentMenusDisplayed started");
        List<String> fitmentTabsList = new ArrayList<>(Arrays.asList(SHOP_BY_VEHICLE, SHOP_BY_SIZE, SHOP_BY_BRAND));

        for (String fitmentTab : fitmentTabsList) {
            WebElement tabElement = driver.getDisplayedElement(driver.getElementsWithText(CommonActions.buttonBy,
                    fitmentTab), Constants.FIVE);
            Assert.assertNotNull("FAIL: The 'Shop By' fitment grid tab: '" + fitmentTab + "' was NOT displayed!",
                    tabElement);
        }
        LOGGER.info("verifyFitmentMenusDisplayed completed");
    }

    /**
     * Verifies that the specified radio button is enabled or disabled
     *
     * @param buttonType The type of radio button to validate (Tires / Wheels)
     * @param enabled    Whether to check if enabled (true) or disabled (false)
     */
    public void verifyRadioButtonEnabledOrDisabled(String buttonType, boolean enabled) {
        LOGGER.info("verifyRadioButtonEnabledOrDisabled started");

        try {
            WebElement radioButton;
            if (!buttonType.equalsIgnoreCase(ConstantsDtc.BOPIS_LABEL) &&
                    !buttonType.equalsIgnoreCase(ConstantsDtc.ROPIS_LABEL)) {
                radioButton = driver.getElementWithText(radioButtonBy, buttonType);
            } else {
                radioButton = driver.getElementWithText(CommonActions.radioLabelBy, buttonType);
            }
            if (enabled) {
                Assert.assertTrue("FAIL: Radio button \"" + radioButton + "\" was disabled.",
                        radioButton.getAttribute(Constants.CLASS).contains(radioButtonActive));
            } else {
                Assert.assertTrue("FAIL: Radio button \"" + radioButton + "\" was enabled.",
                        !radioButton.getAttribute(Constants.CLASS).contains(radioButtonActive));
            }


        } catch (Exception e) {
            Assert.fail("FAIL: Verifying that radio button \"" + buttonType
                    + "\" was enabled or disabled FAILED with error: " + e);
        }

        LOGGER.info("verifyRadioButtonEnabledOrDisabled completed");
    }

    /**
     * Verifies that the dropdown values of an expanded fitment menu are sorted
     */
    public void verifyDropdownValuesSorted() {
        LOGGER.info("verifyDropdownValuesSorted started");

        List<WebElement> dropDownOptions = webDriver.findElements(selectOptionBy);
        ArrayList dropDownValues = new ArrayList();

        for (WebElement dropDownOption : dropDownOptions) {
            dropDownValues.add(dropDownOption.getText().trim().toLowerCase());
        }

        Assert.assertTrue("FAIL: Dropdown list was not alphabetically sorted. Values: " +
                dropDownValues, CommonUtils.isListSorted(dropDownValues));

        LOGGER.info("Verified that dropDown values were sorted:" + dropDownValues);
        LOGGER.info("verifyDropdownValuesSorted completed");
    }

    /**
     * Verifies that the dropdown values of an expanded dropdown are limited to those
     * that start with the specified character
     *
     * @param character The character to validate
     */
    public void verifyDropdownValuesLimited(String character) {
        LOGGER.info("verifyDropdownValuesLimited started");

        List<WebElement> dropDownOptions = webDriver.findElements(selectOptionBy);

        for (WebElement dropDownOption : dropDownOptions) {
            String dropDownText = dropDownOption.getText().trim();
            Assert.assertTrue("FAIL: First character of dropdown value did not start with " +
                            "\"" + character + "\" (dropdown value: " + dropDownText,
                    String.valueOf(dropDownText.charAt(0)).equalsIgnoreCase(character));
        }

        LOGGER.info("Verified that dropDown values were limited to those that start with \"" +
                character + "\"");
        LOGGER.info("verifyDropdownValuesLimited completed");
    }

    /**
     * Verifies that a dropdown with the specified value set is visible
     *
     * @param dropDownValue The value in the dropDown
     * @throws Exception General exception caught to allow for graceful failure
     */
    public void verifyDropdownWithValueIsDisplayed(String dropDownValue) throws Exception {
        LOGGER.info("verifyDropdownWithValueIsDisplayed started");
        int size = 0;

        try {

            //TODO: Needed for IE popup window due to on screen movement that cannot be manually replicated
            if (Config.isIe())
                driver.waitForMilliseconds(Constants.THREE);

            driver.waitForElementClickable(selectControl);

            List<WebElement> dropDownLists = webDriver.findElements(CommonActions.selectBy);

            for (WebElement dropDownList : dropDownLists) {
                if (dropDownList.getText().trim().toLowerCase().contains(dropDownValue.toLowerCase())) {
                    LOGGER.info("Confirmed that the dropdown with value \"" + dropDownValue + "\" exists.");
                    break;
                }
                size++;
            }

            if (size == dropDownLists.size()) {
                Assert.fail("FAIL: Dropdown with value set to \"" + dropDownValue + "\" was not found.");
            }

        } catch (Exception e) {
            Assert.fail("FAIL: Verifying dropdown with value \"" + dropDownValue
                    + "\" FAILED with error: " + e);
        }
        LOGGER.info("verifyDropdownWithValueIsDisplayed completed");
    }

    /**
     * Verifies that a specified drop down value is selected (workaround to validate that it is blue)
     *
     * @param dropDownValue The value in the dropDown to verify
     * @throws Exception General exception caught to allow for graceful failure
     */
    public void verifyDropdownValueSelected(String dropDownValue) throws Exception {
        LOGGER.info("verifyDropdownValueSelected started");
        try {
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
            List<WebElement> dropDownOptions = webDriver.findElements(selectOptionBy);

            for (WebElement dropDownOption : dropDownOptions) {
                if (dropDownOption.getText().trim().toLowerCase().contains(dropDownValue.toLowerCase())) {
                    Assert.assertTrue("Dropdown value \"" + dropDownValue + "\" was not selected.",
                            dropDownOption.getAttribute(Constants.CLASS).contains(Constants.HIGHLIGHT));
                    break;
                }
            }

        } catch (Exception e) {
            Assert.fail("FAIL: Verifying that dropdown value \"" + dropDownValue
                    + "\" was selected FAILED with error: " + e);
        }
        LOGGER.info("verifyDropdownValueSelected completed");
    }

    /**
     * Verifies that the dropdown menu has expanded (workaround for
     * "the dropdown arrow position should change from downward to upward")
     *
     * @param hasExpanded Whether to verify the menu has expanded or not
     * @throws Exception General exception caught to allow for graceful failure
     */
    public void verifyDropdownMenuExpanded(boolean hasExpanded) throws Exception {
        LOGGER.info("verifyDropdownMenuExpanded started");
        driver.waitForMilliseconds();

        if (hasExpanded) {
            Assert.assertTrue("Dropdown menu was not expanded.",
                    driver.isElementDisplayed(dropDownMenuBy, Constants.TWO));
        } else {
            Assert.assertFalse("Dropdown menu was expanded when it should not have been.",
                    driver.isElementDisplayed(dropDownMenuBy, Constants.TWO));
        }
        LOGGER.info("verifyDropdownMenuExpanded completed");
    }

    /**
     * Asserts that the fitment search button is disabled
     *
     * @throws Exception General exception caught to allow for graceful failure
     */
    public void assertSearchButtonIsDisabled(String button) throws Exception {
        LOGGER.info("assertSearchButtonIsDisabled started");
        if (button.equalsIgnoreCase(ConstantsDtc.VIEW_RECOMMEDED_TIRES)) {
            Boolean buttonEnabled = CommonActions.dtButtonLgPrimary.isEnabled();
            Assert.assertFalse("FAIL: The view recommended tires button was not disabled."
                    , buttonEnabled);
        } else if (button.equalsIgnoreCase(Constants.SEARCH)) {
            Assert.assertTrue("FAIL: The Search button was not disabled.",
                    searchButton.getAttribute(Constants.CLASS).contains(searchButtonDisabled));
        }
        LOGGER.info("assertSearchButtonIsDisabled completed");
    }

    /**
     * Asserts that the fitment search button is enabled
     *
     * @throws Exception General exception caught to allow for graceful failure
     */
    public void assertSearchButtonIsEnabled() throws Exception {
        LOGGER.info("assertSearchButtonIsEnabled started");
        Assert.assertTrue("FAIL: The Search button was not enabled.",
                !searchButton.getAttribute(Constants.CLASS).contains(searchButtonDisabled));
        LOGGER.info("assertSearchButtonIsEnabled completed");
    }

    /**
     * Asserts the color of the fitment specified button
     *
     * @param expectedColorString The color to verify
     */
    public void assertSearchButtonColor(String button, String expectedColorString) {
        LOGGER.info("assertSearchButtonColor started");
        String expectedColor = getSearchButtonColorHexCodeFromString(expectedColorString);
        String actualColor;
        boolean colorChanged;
        if (button.equalsIgnoreCase(ConstantsDtc.VIEW_RECOMMEDED_TIRES)) {
            String colorScript = "return $('" + viewRecommendationClassName +
                    "').css('background-color') === '" + expectedColor + "'";
            colorChanged = driver.pollUntil(colorScript, Constants.FIVE);
            actualColor = CommonActions.dtButtonLgPrimary.getCssValue(Constants.BACKGROUND_COLOR);
        } else {
            String colorScript = "return $('" + searchButtonClassName +
                    "').css('background-color') === '" + expectedColor + "'";
            colorChanged = driver.pollUntil(colorScript, Constants.FIVE);
            actualColor = searchButton.getCssValue(Constants.BACKGROUND_COLOR);
        }
        Assert.assertTrue("FAIL: Search button did not change to color \"" + expectedColor +
                "\", actual color was: \"" + actualColor + "\"!", colorChanged);
        LOGGER.info("assertSearchButtonColor completed");
    }

    /**
     * Gets the specified color hex code
     *
     * @param color The color to check
     * @return String   The hex code of the color
     */
    private static String getSearchButtonColorHexCodeFromString(String color) {
        String colorHexCode = null;
        if (color.equalsIgnoreCase(Constants.RED)) {
            colorHexCode = Constants.RED_COLOR_SEARCH_BUTTON;
        } else if (color.equalsIgnoreCase(Constants.GREY)) {
            colorHexCode = GREY_COLOR;
        }
        return colorHexCode;
    }

    /**
     * Checks for default values of dropdowns
     *
     * @param dropDownString String representing the default values of the dropdowns being checked
     */
    public void assertDropdownsAreVisible(String dropDownString) {
        LOGGER.info("assertDropdownsAreVisible started");
        driver.waitForElementVisible(CommonActions.selectBy);
        int count = 0;
        String dropDownValues[] = dropDownString.split(",");
        List<WebElement> dropDowns = webDriver.findElements(CommonActions.selectBy);
        for (int i = 0; i < dropDownValues.length; i++) {
            Assert.assertTrue("FAIL: " + dropDownValues[count] + " was not found in any dropdown.",
                    dropDowns.get(count).getText().trim().contains(dropDownValues[count].trim()));
            count++;
        }
        LOGGER.info("assertDropdownsAreVisible completed");
    }

    /**
     * Verifies that the given My Vehicle dropdown is not empty and contains no decimals
     *
     * @param tab          Tab that the dropdown is contained in
     * @param dropDownName Name of the dropdown to check values of
     */
    //TODO: Refactor based on existing dropdown methods
    public void assertNoDecimalInMyVehicleDropdown(String tab, String dropDownName) {
        LOGGER.info("assertNoDecimalInDropdown started");

        WebElement tabToClick = null;

        if (tab.equalsIgnoreCase(VEHICLE_SEARCH)) {
            tabToClick = getSearchTab(ConstantsDtc.MY_VEHICLES, VEHICLE_SEARCH);
        } else if (tab.equalsIgnoreCase(TIRE_SIZE_SEARCH)) {
            tabToClick = getSearchTab(ConstantsDtc.MY_VEHICLES, TIRE_SIZE_SEARCH);
        } else if (tab.equalsIgnoreCase(WHEEL_SIZE_SEARCH)) {
            tabToClick = getSearchTab(ConstantsDtc.MY_VEHICLES, WHEEL_SIZE_SEARCH);
        } else {
            Assert.fail("Search tab " + tab + " was not found on My Vehicles popup page.");
        }

        driver.waitForElementClickable(tabToClick);
        tabToClick.click();

        //TODO: Needed for IE popup window due to on screen movement that cannot be manually replicated
        if (Config.isIe() || Config.isFirefox())
            driver.waitForMilliseconds(Constants.THREE);

        if (tab.contains(Constants.WHEEL)) {
            HomePage.wheelsRadioLink.click();
        } else if (tab.contains(Constants.TIRE)) {
            HomePage.tiresRadioLink.click();
        }
        driver.waitForElementClickable(selectControl);

        List<WebElement> dropDownLists = webDriver.findElements(CommonActions.selectBy);

        for (WebElement dropDownList : dropDownLists) {
            if (dropDownList.getText().trim().toLowerCase().contains(dropDownName.toLowerCase())) {
                driver.waitForElementClickable(dropDownList);
                //TODO: replace with pollUntil call
                driver.waitForMilliseconds();

                //TODO: retest when geckodriver is updated & stabilized
                if (Config.isFirefox()) {
                    driver.waitForMilliseconds(Constants.TWO_THOUSAND);
                }

                dropDownList.click();
                driver.waitForMilliseconds();

                List<WebElement> dropDownOptions = webDriver.findElements(selectOptionBy);
                Assert.assertTrue(dropDownOptions + " list is empty.", dropDownOptions.size() != 0);
                for (WebElement dropDownOption : dropDownOptions) {
                    Assert.assertFalse(dropDownName + " dropdown DOES contain a decimal: " + dropDownOption.getText(),
                            dropDownOption.getText().contains(PERIOD_DECIMAL));
                }
                break;
            }
        }

        driver.waitForMilliseconds();
        LOGGER.info("assertNoDecimalInDropdown completed");
    }

    /**
     * Returns the default dropdown values for specific tabs
     *
     * @param shopBy The tab to get default dropdown values for
     * @return String[] An array of default dropdown values
     */
    private String[] getDefaultDropDownValues(String shopBy) {
        if (shopBy.equalsIgnoreCase(SHOP_BY_VEHICLE)) {
            return SHOP_BY_VEHICLE_DEFAULT_DROPDOWN_VALUES;
        } else if (shopBy.equalsIgnoreCase(SHOP_BY_BRAND)) {
            return SHOP_BY_BRAND_DEFAULT_DROPDOWN_VALUES;
        } else if (shopBy.equalsIgnoreCase(SHOP_BY_TIRE_SIZE)) {
            return SHOP_BY_TIRE_SIZE_DEFAULT_DROPDOWN_VALUES;
        } else if (shopBy.equalsIgnoreCase(ConstantsDtc.WHEEL_SIZE)) {
            return SHOP_BY_WHEEL_SIZE_DEFAULT_DROPDOWN_VALUES;
        } else {
            return null;
        }
    }

    /**
     * Asserts the position of the displayed arrow on a selected drop-down
     */
    public void assertSelectedDropDownArrowPosition(String dropdown, String position) throws Exception {
        LOGGER.info("assertSelectedDropDownArrowPosition started");
        String dropDownFitmentAutoString = "auto-fitment-".concat(dropdown.toLowerCase());
        if (position.toLowerCase().equals(ARROW_DOWN)) {
            Assert.assertTrue("FAIL: Drop Down arrow was not pointing downwards.",
                    driver.isElementDisplayed(By.xpath(String.format(dropDownArrowDownXpath,
                            dropDownFitmentAutoString))));
        } else if (position.toLowerCase().equals(ARROW_UP)) {
            Assert.assertTrue("FAIL: Expanded Drop Down arrow was not pointing upwards.",
                    driver.isElementDisplayed(By.xpath(String.format(dropDownArrowUpXpath,
                            dropDownFitmentAutoString))));
        }
        LOGGER.info("assertSelectedDropDownArrowPosition completed");
    }

    /**
     * Verify that the Shop By Vehicle is the default selection in the Fitment Component on the Homepage
     */
    public void verifyShopByVehicleIsDefault() throws Exception {
        LOGGER.info("assertShopByVehicle started");
        Assert.assertTrue("FAIL: The Shop By Vehicle Tab is not selected by default.",
                driver.isElementDisplayed(dropDownYear));
        LOGGER.info("assertShopByVehicle completed");
    }

    /**
     * Verify Shop By Brand default placeholder text
     *
     * @throws Exception General exception caught to allow for graceful failure
     */
    public void assertShopByBrandDefaultPlaceholderText() throws Exception {
        LOGGER.info("assertShopByBrandDefaultPlaceholderText started");
        Assert.assertTrue("FAIL: Shop By Brand placeholder text: " + dropDownBrandTextPlaceholderElement.getText()
                        + " did NOT match with default expected value : "
                        + ConstantsDtc.BRAND_SEARCH_FIELD_PLACEHOLDER_TEXT,
                dropDownBrandTextPlaceholderElement.getText()
                        .equalsIgnoreCase(ConstantsDtc.BRAND_SEARCH_FIELD_PLACEHOLDER_TEXT));
        LOGGER.info("assertShopByBrandDefaultPlaceholderText completed");
    }

    /**
     * Verify Wheels Shop By Size fields are in specific order left to right
     *
     * @throws Exception General exception caught to allow for graceful failure
     */
    public void assertWheelsShopBySizeFieldsAreInSpecificOrderLeftToRight() throws Exception {
        LOGGER.info("assertWheelsShopBySizeFieldsAreInSpecificOrderLeftToRight started");
        int rimDiameterXCoordinateValue = commonActions.getXCoordinateValueOfAnElement(dropDownWheelDiameter);
        int rimWidthXCoordinateValue = commonActions.getXCoordinateValueOfAnElement(dropDownWheelWidth);
        int boltPatternXCoordinateValue = commonActions.getXCoordinateValueOfAnElement(dropDownWheelBoltPattern);

        if (!((rimDiameterXCoordinateValue < rimWidthXCoordinateValue)
                && (rimWidthXCoordinateValue < boltPatternXCoordinateValue))) {
            Assert.fail("FAIL: Wheels Shop By Size fields were NOT in specific order left to right");
        }

        LOGGER.info("assertWheelsShopBySizeFieldsAreInSpecificOrderLeftToRight completed");
    }

    /**
     * Verifies the specified fitment grid breadcrumb is active
     *
     * @param breadcrumb Text of the fitment grid breadcrumb (Year, Make, Model, Trim, etc.)
     */
    public void verifyBreadcrumbActiveOnFitmentGrid(String breadcrumb) {
        LOGGER.info("verifyBreadcrumbActiveOnFitmentGrid started for breadcrumb link: '" + breadcrumb + "'");
        WebElement breadCrumb = driver.getElementWithText(CommonActions.buttonBy, breadcrumb);
        Assert.assertTrue("FAIL: The fitment breadcrumb link: '" + breadcrumb + "' is NOT active!",
                breadCrumb.getAttribute(ATTR_ARIA_DISABLED).equalsIgnoreCase(CommonActions.FALSE));
        LOGGER.info("verifyBreadcrumbActiveOnFitmentGrid completed for breadcrumb link: '" + breadcrumb + "'");
    }

    /**
     * Verifies the number of currently displayed fitment grid options matches expectation
     *
     * @param expectedCount Expected number of fitment grid options to be displayed
     */
    public void verifyNumberOfOptionsDisplayed(int expectedCount) {
        LOGGER.info("verifyNumberOfOptionsDisplayed started with count expectation: '" + expectedCount + "'");
        List<WebElement> fitmentGridOptionsList = getCurrentFitmentGridOptions();
        Assert.assertEquals("FAIL: The number of displayed fitment grid options for 'Year' does NOT"
                + " match the expectation!", expectedCount, fitmentGridOptionsList.size());
        LOGGER.info("verifyNumberOfOptionsDisplayed completed with count expectation: '" + expectedCount + "'");
    }

    /**
     * Verifies the currently displayed fitment grid options matches the sort order expectation
     *
     * @param order           Expected sort order of the fitment grid options (ascending or descending)
     * @param standardOptions True for standard options / False for non-standard options (e.g. High Flotation tires)
     */
    public void verifySortOrderOfOptionsDisplayed(String order, boolean standardOptions) {
        LOGGER.info("verifySortOrderOfOptionsDisplayed started with order expectation: '" + order + "'");
        List<WebElement> fitmentGridOptionsList;
        List<String> optionTextList = new ArrayList<>();

        if (standardOptions) {
            fitmentGridOptionsList = getCurrentFitmentGridOptions();
        } else {
            fitmentGridOptionsList = getCurrentFitmentGridOptions(false);
        }

        for (WebElement option : fitmentGridOptionsList) {
            optionTextList.add(option.getText());
        }

        List<String> defaultOptionOrderList = new ArrayList<>(optionTextList);
        Collections.sort(optionTextList);

        if (order.equalsIgnoreCase(ConstantsDtc.DESCENDING))
            Collections.reverse(optionTextList);

        Assert.assertEquals("FAIL: The fitment grid options are NOT in '" + order + "' order!",
                optionTextList, defaultOptionOrderList);
        LOGGER.info("verifySortOrderOfOptionsDisplayed completed with order expectation: '" + order + "'");
    }

    /**
     * Verifies the currently displayed fitment grid options matches the sort order expectation
     *
     * @param order Expected sort order of the fitment grid options (ascending or descending)
     */
    public void verifySortOrderOfOptionsDisplayed(String order) {
        verifySortOrderOfOptionsDisplayed(order, true);
    }

    /**
     * Verifies the option with specified text is (not) displayed on the fitment grid
     *
     * @param optionText   Text of the option to check display status
     * @param displayCheck Displayed or not displayed
     */
    public void verifyOptionDisplayOnFitmentGrid(String optionText, String displayCheck) {
        LOGGER.info("verifyOptionDisplayOnFitmentGrid started");
        WebElement optionToVerify = driver.getElementWithText(CommonActions.buttonBy, optionText,
                Constants.FIVE);

        if (displayCheck.equalsIgnoreCase(Constants.DISPLAYED)) {
            if (Config.getDataSet().equalsIgnoreCase(Constants.QA)
                    && optionText.equalsIgnoreCase(ConstantsDtc.SHOW_ALL)) {
                LOGGER.info("Skipping validation for 'Show all' in QA as there is not enough data to get the" +
                        " option to appear! Remove when data situation is improved. (ALM #15587)");
            } else {
                Assert.assertTrue("FAIL: The fitment grid option: '" + optionText + "' was NOT displayed!",
                        driver.isElementDisplayed(optionToVerify));
            }
        } else {
            Assert.assertNull("FAIL: The fitment grid option: '" + optionText + "' was displayed!", optionToVerify);
        }
        LOGGER.info("verifyOptionDisplayOnFitmentGrid completed");
    }

    /**
     * Selects the specified fitment grid option for a vehicle, size, or brand search
     *
     * @param standardOptions True for standard options / False for non-standard options (e.g. High Flotation tires)
     * @param gridOption      The value to select from the fitment search grid
     */
    public void selectFitmentGridOption(boolean standardOptions, String gridOption) {
        LOGGER.info("selectFitmentGridOption started with gridOption: '" + gridOption + "'");
        showAllItemsOnFitmentGridContainer();
        WebElement optionToSelect = null;
        int counter = 0;
        boolean success = true;
        do {
            success = true;
            try {
                Matcher regexMatcher = Pattern.compile("[F]\\s[0-9]{3}\\s[\\\\/][0-9]{2}\\s[R][0-9]{2}\\s[A-Z]{0,2}\\s-")
                        .matcher(gridOption);
                regexMatcher.reset();

                if (!(regexMatcher.find())) {
                    boolean done = false;
                    do {
                        List<WebElement> gridOptions = getCurrentFitmentGridOptions(standardOptions);
                        try {
                            optionToSelect = getElementWithEqualsOrContainsText(gridOption.trim(), gridOptions);
                            done = true;
                        } catch (StaleElementReferenceException se) {
                            //continue
                        }
                    } while (!done);
                } else {
                    driver.waitForPageToLoad();
                    WebElement fitmentType;
                    if (driver.isElementDisplayed(CommonActions.dtModalContainerBy, Constants.ONE)) {
                        fitmentType = driver.getDisplayedElement(CommonActions.dtModalContainerBy, Constants.ONE);
                    } else {
                        fitmentType = CommonActions.fitmentContainer;
                    }

                    driver.waitForElementClickable(fitmentType, Constants.TEN);
                    optionToSelect = driver.getElementWithAttribute(CommonActions.buttonBy, Constants.TITLE,
                            gridOption.trim());
                }
                driver.jsScrollToElementClick(optionToSelect, false);
            } catch (NullPointerException npe) {
                success = false;
                driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
                counter++;
            }
        } while (!success && counter < Constants.THIRTY);
        LOGGER.info("selectFitmentGridOption completed with gridOption: '" + gridOption + "'");
    }

    /**
     * Conducts a vehicle search by selecting specified values from the vehicle search fitment grid
     *
     * @param searchType The type of vehicle search (homepage or my vehicle)
     * @param year       The year value to select
     * @param make       The make value to select
     * @param model      The model value to select
     * @param trim       The trim value to select
     * @param assembly   The assembly value to select
     */
    public void fitmentGridVehicleSearch(String searchType, String year, String make, String model, String trim,
                                         String assembly) {
        LOGGER.info("fitmentGridVehicleSearch started from '" + searchType + "' with values: '" + year + "', '"
                + make + "', '" + model + "', '" + trim + "', and '" + assembly + "'");
        if (searchType.equalsIgnoreCase(ConstantsDtc.MY_VEHICLES)) {
            homePage.openMyVehiclesPopup();
            myVehiclePopupPage.clickAddVehicle();
        } else if (searchType.equalsIgnoreCase(Constants.HOMEPAGE)) {
            try {
                // It is preferred to go to homepage via the site logo in case we have switched from DT to AT or vice
                // versa. That way driver.getUrl(baseUrl) is not called which would switch us back.
                commonActions.clickSiteLogo();
            } catch (Exception e) {
                homePage.goToHome();
            }
            driver.waitForPageToLoad(Constants.ONE_THOUSAND);
        } else if (searchType.equalsIgnoreCase(CommonActions.CHECKFIT_EDIT)) {
            driver.clickElementWithText(CommonActions.buttonBy, YEAR);
        }
        driver.setImplicitWait(Constants.TWO, TimeUnit.SECONDS);
        if (driver.isElementDisplayed(driver.getElementWithText(CommonActions.dtButtonLgBy,
                ConstantsDtc.ADD_NEW_VEHICLE), Constants.ONE))
            commonActions.clickButtonByText(ConstantsDtc.ADD_NEW_VEHICLE);
        driver.resetImplicitWaitToDefault();
        selectVehicle(year, make, model, trim, assembly);
        LOGGER.info("fitmentGridVehicleSearch completed from '" + searchType + "' with values: '" + year + "', '"
                + make + "', '" + model + "', '" + trim + "', and '" + assembly + "'");
    }

    /**
     * Select Vehicle
     *
     * @param year     The year value to select
     * @param make     The make value to select
     * @param model    The model value to select
     * @param trim     The trim value to select
     * @param assembly The assembly value to select
     */
    public void selectVehicle(String year, String make, String model, String trim, String assembly) {
        LOGGER.info("selectVehicle started");
        ArrayList<String> vehicleValuesList = new ArrayList<>(Arrays.asList(year, make, model, trim, assembly));
        for (String vehicleValue : vehicleValuesList) {
            if (!vehicleValue.equalsIgnoreCase(Constants.NONE)) {
                selectFitmentGridOption(true, vehicleValue);
            }
        }
        LOGGER.info("selectVehicle completed");
    }

    /**
     * Select a specified vehicle from the fitment panel
     *
     * @param year  Vehicle year
     * @param make  Vehicle make
     * @param model Vehicle model
     * @param trim  Vehicle trim
     */
    public void fitmentSelectExistingVehicle(String year, String make, String model, String trim) {
        LOGGER.info("fitmentSelectExistingVehicle started for: '" + year + " " + make + " " + model + " " + trim + "'");
        List<WebElement> existingVehicles = webDriver.findElements(CommonActions.myVehiclesButtonBy);
        boolean found = false;
        for (WebElement vehicle : existingVehicles) {
            String text = vehicle.getText();
            if (CommonUtils.containsIgnoreCase(text, year) && CommonUtils.containsIgnoreCase(text, make) &&
                    CommonUtils.containsIgnoreCase(text, model) && CommonUtils.containsIgnoreCase(text, trim)) {
                driver.jsScrollToElementClick(vehicle);
                found = true;
                break;
            }
        }
        Assert.assertTrue("FAIL: Vehicle '" + year + " " + make + " " + model + " " + trim + "' was not found", found);
        LOGGER.info("fitmentSelectExistingVehicle completed for: '" + year + " " + make + " " + model + " " + trim + "'");
    }

    /**
     * Gets and Returns a list of WebElements for the currently displayed vehicle fitment grid options
     *
     * @param standardOptions True for standard options / False for non-standard options (e.g. High Flotation tires)
     * @return List of WebElements for the currently displayed vehicle fitment grid options
     * e.g. Year options would be 2016, 2015, 2014, etc.
     */
    private List<WebElement> getCurrentFitmentGridOptions(boolean standardOptions) {
        LOGGER.info("getCurrentFitmentGridOptions started");
        List<WebElement> currentGridOptionsList = new ArrayList<>();

        try {
            if (standardOptions) {
                List<WebElement> currentFitmentGrids = driver.getDisplayedElementsList(fitmentGridVisibleItemsBy);
                for (WebElement currentFitmentGrid : currentFitmentGrids) {
                    List<WebElement> itemList = currentFitmentGrid.findElements(CommonActions.buttonBy);
                    currentGridOptionsList.addAll(itemList);
                }
                LOGGER.info("getCurrentFitmentGridOptions completed, returning '" + currentGridOptionsList.size()
                        + "' elements");
            } else {
                selectShowMoreShowAllOnVehicleGrid(ConstantsDtc.SHOW_ALL);
                WebElement highFlotationTiresSection = driver.getDisplayedElement(highFlotationTireSizeSectionBy,
                        Constants.THREE);
                currentGridOptionsList = highFlotationTiresSection.findElements(CommonActions.buttonBy);
            }
        } catch (Exception e) {
            currentGridOptionsList = null;
        }
        return currentGridOptionsList;
    }

    /**
     * Gets and Returns a list of WebElements for the currently displayed vehicle fitment grid options
     *
     * @return List of WebElements for the currently displayed vehicle fitment grid options
     * e.g. Year options would be 2016, 2015, 2014, etc.
     */
    private List<WebElement> getCurrentFitmentGridOptions() {
        return getCurrentFitmentGridOptions(true);
    }

    /**
     * Gets the count / size of the currently displayed vehicle fitment grid options
     *
     * @return Int for the count / size of the displayed vehicle fitment grid options
     */
    public int getCurrentFitmentGridOptionCount() {
        return getCurrentFitmentGridOptions().size();
    }

    /**
     * Verifies that given an initial or previous count of vehicle fitment grid options that additional options have
     * been added / displayed
     *
     * @param initialOptionCount The initial count of the vehicle fitment grid options
     * @param optionLimit        Max number of additional options that can be added via "Show more" (12 as of 6/28/2018)
     */
    public void verifyAdditionalNumberOfFitmentGridOptionsAreDisplayed(int initialOptionCount, int optionLimit) {
        LOGGER.info("verifyAdditionalNumberOfFitmentGridOptionsAreDisplayed started with initial option" +
                " count of: '" + initialOptionCount + "' and expecting another '" + optionLimit
                + "' to have been added");
        int currentOptionCount = getCurrentFitmentGridOptionCount();

        Assert.assertTrue("FAIL: No additional fitment grid options were displayed! Initial count: '"
                        + initialOptionCount + "' Current count: '" + currentOptionCount + "'",
                currentOptionCount > initialOptionCount && currentOptionCount <= initialOptionCount
                        + optionLimit);
        LOGGER.info("verifyAdditionalNumberOfFitmentGridOptionsAreDisplayed completed successfully");
    }

    /**
     * Verifies that all possible fitment grid options for the current context (Year, Make, Model, Trim, etc.) are
     * being displayed to the user. If less than the default initial option are displayed, method validates that there
     * is at least 1 option displayed (options > 0). Method additionally validates the "Show more" and "Show all" links
     * are not available
     */
    public void verifyAllFitmentGridOptionsForCurrentContextAreDisplayed() {
        LOGGER.info("verifyAllFitmentGridOptionsForCurrentContextAreDisplayed started");
        driver.waitForPageToLoad();
        WebElement showMoreLink = driver.getElementWithText(CommonActions.buttonBy, ConstantsDtc.SHOW_MORE);
        WebElement showAllLink = driver.getElementWithText(CommonActions.buttonBy, ConstantsDtc.SHOW_ALL);
        int optionCount = getCurrentFitmentGridOptionCount();

        if (optionCount < MAX_DEFAULT_OPTION_COUNT)
            Assert.assertTrue("FAIL: No fitment grid options are displayed!", optionCount > 0);

        Assert.assertTrue("FAIL: The option to 'Show more' or 'Show all' is still available to user!" +
                " All options for 'Year' NOT displayed!", showMoreLink == null && showAllLink == null);
        LOGGER.info("verifyAllFitmentGridOptionsForCurrentContextAreDisplayed completed");
    }

    /**
     * Verifies the breadcrumb text for a section of the fitment grid where a selection has been made then
     * appears in orange (e.g. after a Year selection of 2016, Year breadcrumb link should appear in orange)
     *
     * @param breadcrumbLinkText Text of the breadcrumb that should be colored orange
     */
    public void verifyFitmentBreadcrumbHighlightOrange(String breadcrumbLinkText) {
        LOGGER.info("verifyFitmentBreadcrumbHighlightOrange started with breadcrumb: '" + breadcrumbLinkText + "'");
        driver.waitForPageToLoad();
        WebElement breadcrumbLink = driver.getElementWithText(CommonActions.buttonBy, breadcrumbLinkText);
        String linkColor = breadcrumbLink.getCssValue(Constants.COLOR);

        Assert.assertTrue("FAIL: '" + breadcrumbLinkText + "' breadcrumb was NOT highlighted in Orange!" +
                        " Actual color value: '" + linkColor + "' Expected color values: '" + FITMENT_LINK_ORANGE_COLOR_RGBA
                        + "' OR '" + FITMENT_LINK_ORANGE_COLOR_RGB + "'",
                linkColor.equalsIgnoreCase(FITMENT_LINK_ORANGE_COLOR_RGBA)
                        || linkColor.equalsIgnoreCase(FITMENT_LINK_ORANGE_COLOR_RGB));
        LOGGER.info("verifyFitmentBreadcrumbHighlightOrange completed with breadcrumb: '" + breadcrumbLinkText + "'");
    }

    /**
     * Verifies that the selections made on the fitment grid for Year, Make, Model, etc. are displayed to
     * the user
     *
     * @param selectionToVerify Text of the selection that should be displayed
     */
    public void verifySelectionDisplayedBelowFitmentBreadcrumbMenuLinks(String selectionToVerify) {
        LOGGER.info("verifySelectionDisplayedBelowFitmentBreadcrumbMenuLinks started with: '" + selectionToVerify
                + "'");
        driver.waitForPageToLoad();
        WebElement fitmentSelections = driver.getElementWithClassContainingText(FITMENT_GRID_SELECTIONS);

        Assert.assertTrue("FAIL: '" + selectionToVerify + "' was NOT found in the breadcrumb selections" +
                " container!", fitmentSelections.getText().toLowerCase().replace("\n", " ").contains(selectionToVerify.toLowerCase()));
        LOGGER.info("verifySelectionDisplayedBelowFitmentBreadcrumbMenuLinks completed with: '"
                + selectionToVerify + "'");
    }

    /**
     * Verifies verbiage / messaging displayed to the user for a specified section (vehicle with multiple sizes, tire
     * sidewall help text, adding a vehicle, or recent tire sizes)
     *
     * @param section Section verbiage / messaging to be validated
     */
    public void verifyVerbiageForSectionIsDisplayedInFitmentGrid(String section) {
        LOGGER.info("verifyVerbiageForSectionIsDisplayedInFitmentGrid started for section: '" + section + "'");
        driver.waitForPageToLoad();
        String expectedText = null;
        WebElement fitmentSection = null;

        switch (section) {
            case MULTIPLE_SIZE:
                expectedText = VEHICLE_MULTIPLE_SIZE_OPTIONS_MESSAGE;
                fitmentSection = driver.getElementWithClassContainingText(FITMENT_GRID_SELECTIONS_MESSAGE);
                break;
            case TIRE_SIDEWALL:
                expectedText = TIRE_SIDEWALL_HELP_MESSAGE;
                fitmentSection = driver.getElementWithClassContainingText(FITMENT_GRID_HELP_SECTION_TEXT);
                break;
            case ConstantsDtc.ADD_VEHICLE:
                expectedText = ADD_VEHICLES_MESSAGE;
                fitmentSection = driver.getElementWithClassContainingText(FITMENT_GRID_VEHICLES_MESSAGE);
                break;
            case RECENT_TIRE_SIZES:
                expectedText = RECENT_TIRE_SIZES;
                fitmentSection = driver.getElementWithClassContainingText(FITMENT_GRID_RECENT_SELECTIONS_TITLE);
                break;
            case RECENT_WHEEL_SIZES:
                expectedText = RECENT_WHEEL_SIZES;
                fitmentSection = driver.getElementWithClassContainingText(FITMENT_GRID_RECENT_SELECTIONS_TITLE);
                break;
            case ConstantsDtc.ADD_NEW_VEHICLE:
                expectedText = ConstantsDtc.ADD_NEW_VEHICLE;
                fitmentSection = driver.getElementWithText(addNewVehicleBy, ConstantsDtc.ADD_NEW_VEHICLE);
                break;
            default:
                Assert.fail("FAIL: '" + section + "' is NOT recognized as a valid fitment grid section!");
                break;
        }

        Assert.assertNotNull("FAIL: Could not find the '" + section + "' message element!", fitmentSection);
        Assert.assertTrue("FAIL: The messaging did not match expected! \n\tActual: '"
                        + fitmentSection.getText() + "' \n\tExpected: '" + expectedText + "'",
                fitmentSection.getText().equalsIgnoreCase(expectedText));
        LOGGER.info("verifyVerbiageForSectionIsDisplayedInFitmentGrid completed for section: '" + section + "'");
    }

    /**
     * Verifies the image for the tire sidewall help (shows user how to find tire/wheel size on their current tires)
     * section is displayed
     */
    public void verifyTireSidewallHelpImageDisplayed() {
        LOGGER.info("verifyTireSidewallHelpImageDisplayed started");
        driver.waitForPageToLoad();
        WebElement tireSidewallImage = driver.getElementWithClassContainingText(FITMENT_GRID_TIRE_SIDEWALL_IMAGE);

        Assert.assertNotNull("FAIL: Could not find the tire sidewall element!", tireSidewallImage);
        Assert.assertTrue("FAIL: Tire sidewall image 'alt text' attribute did NOT match expected!" +
                        " \n\tActual: '" + tireSidewallImage.getAttribute(Constants.ATTRIBUTE_ALT) + "' \n\tExpected: '"
                        + TIRE_SIDEWALL_IMAGE_ALT_TEXT + "'",
                tireSidewallImage.getAttribute(Constants.ATTRIBUTE_ALT).equalsIgnoreCase(TIRE_SIDEWALL_IMAGE_ALT_TEXT));
        LOGGER.info("verifyTireSidewallHelpImageDisplayed completed");
    }

    /**
     * Verifies that a specified vehicle with details is either displayed or NOT displayed in the "My Vehicles" section
     * of the fitment grid
     *
     * @param vehicleDetails String containing the vehicle details to be verified as either displayed
     * @param displayCheck   Whether the vehicle details should or should not be displayed
     */
    public void verifyMyVehicleDetailsDisplayInMyVehiclesSectionOfFitmentGrid(String vehicleDetails,
                                                                              String displayCheck) {
        LOGGER.info("verifyMyVehicleDetailsDisplayInMyVehiclesSectionOfFitmentGrid started with vehicle details: '"
                + vehicleDetails + "' and expectation: '" + displayCheck + "'");
        boolean matchFound = false;
        String[] vehicleDetailsArray = vehicleDetails.split(",");
        List<WebElement> myVehiclesList;

        driver.waitForPageToLoad();
        driver.waitForElementClickable(CommonActions.myVehiclesButtonBy, Constants.FIVE);
        WebElement fitmentParent = CommonActions.fitmentContainer;
        myVehiclesList = fitmentParent.findElements(CommonActions.myVehiclesButtonBy);

        for (WebElement myVehicle : myVehiclesList) {
            String myVehicleText = myVehicle.getText();

            if (matchFound)
                break;

            for (String vehicleDetail : vehicleDetailsArray) {
                if (!myVehicleText.toLowerCase().contains(vehicleDetail.trim().toLowerCase())) {
                    matchFound = false;
                    break;
                }
                matchFound = true;
            }
        }

        if (displayCheck.contains(Constants.NOT_DISPLAYED)) {
            Assert.assertFalse("FAIL: Found 'My Vehicles' entry with details: '" + vehicleDetails + "'!", matchFound);
        } else {
            Assert.assertTrue("FAIL: Could not find 'My Vehicles' entry with details: '" + vehicleDetails + "'!",
                    matchFound);
        }
        LOGGER.info("verifyMyVehicleDetailsDisplayInMyVehiclesSectionOfFitmentGrid completed with vehicle details:" +
                " '" + vehicleDetails + "' and expectation: '" + displayCheck + "'");
    }

    /**
     * Verifies that the specified tab is the "active" tab in the fitment grid
     *
     * @param tabName Name of the tab to be validated as being active
     */
    public void verifyActiveTabOnFitmentGrid(String tabName) {
        LOGGER.info("verifyActiveTabOnFitmentGrid started for tab: '" + tabName + "'");
        driver.waitForPageToLoad();
        WebElement activeTab = driver.getElementWithClassContainingText(FITMENT_GRID_ACTIVE_TAB);

        Assert.assertNotNull("FAIL: Could not find the active tab element on the vehicle fitment grid!",
                activeTab);
        Assert.assertTrue("FAIL: '" + tabName + "' was NOT the active tab on the vehicle fitment grid! " +
                "\n\tActual: '" + activeTab.getText() + "'", activeTab.getText().equalsIgnoreCase(tabName));
        LOGGER.info("verifyActiveTabOnFitmentGrid completed for tab: '" + tabName + "'");
    }

    /**
     * Verifies the button with specified text is displayed with a red background color
     *
     * @param buttonText Text of the button that should have a red background
     */
    public void verifyButtonDisplaysWithRedBackground(String buttonText) {
        LOGGER.info("verifyButtonDisplaysWithRedBackground started for button: '" + buttonText + "'");
        driver.waitForPageToLoad();
        WebElement addNewVehicleBtn = driver.getElementWithText(CommonActions.buttonBy, buttonText);
        String buttonColor = addNewVehicleBtn.getCssValue(Constants.BACKGROUND_COLOR);

        //If any more alternates are added, move colors into a list and iterate over
        Assert.assertTrue("FAIL: '" + buttonText + "' button did NOT have a red background!" +
                        " Actual color value was: '" + buttonColor + "', Expected one of the following: '"
                        + Constants.RED_COLOR_RGBA + "', '" + Constants.RED_COLOR_RGB + "', '"
                        + Constants.ALT_RED_COLOR_RGB + "', or '" + Constants.ALT_RED_COLOR_RGBA + "'",
                buttonColor.equalsIgnoreCase(Constants.RED_COLOR_RGBA)
                        || buttonColor.equalsIgnoreCase(Constants.RED_COLOR_RGB)
                        || buttonColor.equalsIgnoreCase(Constants.ALT_RED_COLOR_RGB)
                        || buttonColor.equalsIgnoreCase(Constants.ALT_RED_COLOR_RGBA));
        LOGGER.info("verifyButtonDisplaysWithRedBackground completed for button: '" + buttonText + "'");
    }

    /**
     * Verifies the "My Vehicles" section displays the current max (3) number of permitted vehicles
     *
     * @param currentMax The max number of vehicles that can be displayed in the "My Vehicles" section. As of 6/28/2018
     *                   the max is 5 vehicles
     * @param page       Homepage or My vehicles modal
     */
    public void verifyMyVehiclesSectionDisplaysMaxNumberOfVehicles(int currentMax, String page) {
        LOGGER.info("verifyMyVehiclesSectionDisplaysMaxNumberOfVehicles started with expected max number of" +
                " vehicles: '" + currentMax + "'");
        List<WebElement> myVehiclesList = null;
        driver.waitForPageToLoad();

        if (page.equalsIgnoreCase(Constants.HOMEPAGE)) {
            myVehiclesList = webDriver.findElements(CommonActions.myVehiclesButtonBy);
        } else {
            int counter = 0;
            int listSize = -1;
            driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
            do {
                try {
                    myVehiclesList = MyVehiclesPopupPage.searchedVehicleList.findElements(CommonActions.vehicleInfoSectionBy);
                    listSize = myVehiclesList.size();
                } catch (Exception e) {
                    driver.waitForMilliseconds();
                    counter++;
                }
            } while (counter < Constants.FIVE && listSize == -1);
            driver.resetImplicitWaitToDefault();
        }
        Assert.assertEquals("FAIL: 'My Vehicles' section is NOT displaying the expected max number of vehicles!",
                currentMax, myVehiclesList.size());
        LOGGER.info("verifyMyVehiclesSectionDisplaysMaxNumberOfVehicles completed with expected max number of" +
                " vehicles: '" + currentMax + "'");
    }

    /**
     * Verifies the expected subheader (as of now either Tires or Wheels) is active
     *
     * @param subheader Text of the subheader expected to be active
     */
    public void verifySubheaderActiveOnFitmentGrid(String subheader) {
        LOGGER.info("verifySubheaderActiveOnFitmentGrid started w/ subheader: '" + subheader + "'");
        WebElement activeSubheader = driver.getElementWithText(fitmentSubheaderBy, subheader);
        Assert.assertNotNull("FAIL: Could not find the active tab element on the vehicle fitment grid!",
                activeSubheader);
        Assert.assertTrue("FAIL: '" + subheader + "' was NOT the active subheader on the vehicle fitment grid! " +
                "\n\tActual: '" + activeSubheader.getText() + "'", activeSubheader.getText()
                .equalsIgnoreCase(subheader));
        LOGGER.info("verifySubheaderActiveOnFitmentGrid completed w/ subheader: '" + subheader + "'");
    }

    /**
     * Removes a recently selected option (Tire/Wheels size searches, as well as Brands) from the fitment grid
     *
     * @param option Text of the option to be removed from the fitment grid's recently selected section
     */
    public void removeRecentlySelectedOptionFromFitmentGrid(String option) {
        LOGGER.info("removeRecentlySelectedOptionFromFitmentGrid started w/ option: '" + option + "'");
        driver.waitForPageToLoad();
        boolean optionRemoved = false;
        List<WebElement> recentOptionsList = driver.getElementsWithClassContainingText
                (FITMENT_RECENT_SELECTION_LIST_ITEM);

        for (int i = 0; i < recentOptionsList.size(); i++) {
            WebElement recentOption = recentOptionsList.get(i);
            if (recentOption.getText().toLowerCase().contains(option.toLowerCase())) {
                LOGGER.info("Found recently selected grid option with text: '" + option + "'");
                WebElement removeRecentOption = webDriver.findElement(By.xpath(String.format(
                        recentSelectionRemoveButtonXpath, Integer.toString(i + 1))));
                driver.jsScrollToElement(removeRecentOption);
                removeRecentOption.click();
                optionRemoved = true;
                break;
            }
        }

        Assert.assertTrue("FAIL: Unable to find and remove option: '" + option + "'!", optionRemoved);
        LOGGER.info("removeRecentlySelectedOptionFromFitmentGrid completed w/ option: '" + option + "'");
    }

    /**
     * Selects / clicks either the "Show more" or "Show all" links for the current context of the fitment grid
     * if they are available. If not present, method will log the status and continue on.
     *
     * @param selection "Show more" or "Show all"
     */
    public void selectShowMoreShowAllOnVehicleGrid(String selection) {
        LOGGER.info("selectShowMoreShowAllOnVehicleGrid started for: '" + selection + "'");
        WebElement selectionElement = driver.getDisplayedElement(driver.getElementsWithText(
                CommonActions.buttonBy, selection), Constants.ONE);

        if (selectionElement != null) {
            selectionElement.click();
        } else {
            LOGGER.info("The '" + selection + "' was NOT displayed for current context of the"
                    + " fitment grid. Selection skipped!");
        }
        LOGGER.info("selectShowMoreShowAllOnVehicleGrid completed for: '" + selection + "'");
    }

    /**
     * Returns the WebElement that either matches the specified text exactly OR not finding a match, returns
     * element containing the text
     *
     * @param text        Text expected to be present in one of the WebElements
     * @param elementList Pre-built list of WebElements to search for an element with exact or containing the specified
     *                    text
     * @return WebElement that either equaled (ignore case) OR contained the specified text
     */
    private WebElement getElementWithEqualsOrContainsText(String text, List<WebElement> elementList) {
        LOGGER.info("getElementWithEqualsOrContainsText started with text: '" + text + "'");
        WebElement returnElement = null;

        for (WebElement element : elementList) {
            if (element.getText().equalsIgnoreCase(text)) {
                returnElement = element;
                LOGGER.info("Element with exact text match found for: '" + text + "'!");
                break;
            }
        }

        if (returnElement == null) {
            LOGGER.info("Unable to find element with an exact text match! Attempting to find element containing: '"
                    + text + "'");
            for (WebElement element : elementList) {
                if (element.getText().toLowerCase().contains(text.toLowerCase())) {
                    returnElement = element;
                    LOGGER.info("Element containing text substring found for: '" + text + "'!");
                    break;
                }
            }
        }
        LOGGER.info("getElementWithEqualsOrContainsText completed");
        return returnElement;
    }

    /**
     * Verifies "High Flotation" tire sizes are displayed below the "Metric" sizes section border and that the number
     * of high flotation options is at least one
     */
    public void verifyHighFlotationTiresDisplayBelowMetricTires() {
        LOGGER.info("verifyHighFlotationTiresDisplayBelowMetricTires started");
        WebElement tireSizeDivider = driver.getElementWithClassContainingText(FITMENT_OPTIONS_DIVIDER);

        Assert.assertNotNull("FAIL: The tire size divider was NOT displayed between the metric and high flotation" +
                " tire sizes!", tireSizeDivider);

        List<WebElement> highFlotationTireOptionsList = getCurrentFitmentGridOptions(false);

        Assert.assertTrue("FAIL: No high flotation tire sizes were displayed below the metric sizes section!",
                highFlotationTireOptionsList.size() > 0);
        LOGGER.info("verifyHighFlotationTiresDisplayBelowMetricTires completed");
    }

    /**
     * Conducts a search on the 'Brand' tab based on the product type (TIRES or WHEELS). Method handles the selection
     * of the 'Brand' tab, and the required sub-heading based on the product type.
     *
     * @param searchType  Which fitment search control to use: homepage or via 'My Vehicles' modal
     * @param productType TIRES or WHEELS
     * @param brandOption Name of the brand to select e.g. Michelin Tires, BFGoodrich, etc.
     */
    public void fitmentBrandSearch(String searchType, String productType, String brandOption) {
        LOGGER.info("fitmentBrandSearch started from '" + searchType + "' for '" + productType + "' products by '"
                + brandOption + "'");
        selectFitmentSizeOrBrandSearchType(searchType, SHOP_BY_BRAND.toUpperCase(), productType);
        selectFitmentGridOption(true, brandOption);
        commonActions.waitForSpinner();
        driver.waitForPageToLoad(Constants.ONE_HUNDRED);
        LOGGER.info("fitmentBrandSearch completed from '" + searchType + "' for '" + productType + "' products by '"
                + brandOption + "'");
    }

    /**
     * Verifies that the specified list of breadcrumbs are displayed on the fitment grid
     *
     * @param breadcrumbs Text of the breadcrumbs to be validated in a comma separated string
     */
    public void verifyBreadcrumbsDisplayOnFitmentGrid(String breadcrumbs) {
        LOGGER.info("verifyBreadcrumbsDisplayOnFitmentGrid started with breadcrumbs: '" + breadcrumbs + "'");
        List<String> breadcrumbList = new ArrayList<>(Arrays.asList(breadcrumbs.split(",")));
        for (String breadcrumbText : breadcrumbList) {
            WebElement breadCrumb = driver.getDisplayedElement(driver.getElementsWithText(CommonActions.buttonBy,
                    breadcrumbText.trim()), Constants.THREE);
            Assert.assertNotNull("FAIL: Could not find the '" + breadcrumbText + "' breadcrumb!", breadCrumb);
        }
        LOGGER.info("verifyBreadcrumbsDisplayOnFitmentGrid completed with breadcrumbs: '" + breadcrumbs + "'");
    }

    /**
     * Selects the specified fitment grid subheader (TIRES, WHEELS, Quick Search, and Advanced Search)
     *
     * @param subheader The text of the fitment grid subheader to select
     */
    public void selectSubheaderOnFitmentGrid(String subheader) {
        LOGGER.info("selectSubheaderOnFitmentGrid started with '" + subheader + "' subheader");
        WebElement subheaderElement = driver.getDisplayedElement(driver.getElementsWithText(CommonActions.buttonBy,
                subheader), Constants.THREE);
        Assert.assertNotNull("The '" + subheader + "' was NOT displayed for current context of the"
                + " fitment grid. Selection skipped!", subheaderElement);
        driver.jsScrollToElement(subheaderElement);
        subheaderElement.click();
        LOGGER.info("selectSubheaderOnFitmentGrid completed with '" + subheader + "' subheader");
    }

    /**
     * Verifies a previously selected item appears in the 'Recently Selected' section of the fitment grid
     *
     * @param option       Text of the option to be validated as either displayed or not displayed in the fitment grid's
     *                     'Recent Selections' section
     * @param displayCheck Displayed or not displayed
     */
    public void verifyRecentlySelectedOptionDisplayOnFitmentGrid(String option, String displayCheck) {
        LOGGER.info("verifyRecentlySelectedOptionDisplayOnFitmentGrid started for grid option: '" + option
                + "' and expectation: '" + displayCheck + "'");
        boolean displayed = false;

        driver.setImplicitWait(Constants.FIVE, TimeUnit.SECONDS);
        List<WebElement> recentOptionsList = driver.getElementsWithClassContainingText
                (FITMENT_RECENT_SELECTION_LIST_ITEM);
        driver.resetImplicitWaitToDefault();

        for (WebElement recentOption : recentOptionsList) {
            if (recentOption.getText().toLowerCase().trim().contains(option.toLowerCase().trim())) {
                displayed = true;
                break;
            }
        }

        if (displayCheck.contains(Constants.NOT_DISPLAYED)) {
            Assert.assertFalse("FAIL: Found option '" + option + "' in Recent Selection section of fitment grid!",
                    displayed);
        } else {
            Assert.assertTrue("FAIL: Could not the option '" + option + "' in the Recent Selection section"
                    + " of the fitment grid!", displayed);
        }
        LOGGER.info("verifyRecentlySelectedOptionDisplayOnFitmentGrid completed for grid option: '" + option
                + "' and expectation: '" + displayCheck + "'");
    }

    /**
     * Asserts that none of the fitment grid options contain a specified value
     *
     * @param value The string to check each result for
     */
    public void verifyNoFitmentGridResultsContainValue(String value) {
        LOGGER.info("verifyNoFitmentGridResultsContainValue started");
        for (WebElement option : getCurrentFitmentGridOptions()) {
            Assert.assertFalse(
                    "FAIL: Fitment Grid result " + option.getText() + " unexpectedly contains value " + value,
                    option.getText().contains(value));
        }
        LOGGER.info("verifyNoFitmentGridResultsContainValue completed");
    }

    /**
     * Click "Show More" until either there is an exception error or the grid height no longer increases.
     * This is necessary because we can't use driver.isDisplayed() to indicate we can click on "Show More"
     * because it always returns true even when it isn't displayed.
     */
    public void showAllItemsOnFitmentGridContainer() {
        LOGGER.info("showAllItemsOnFitmentGridContainer started");
        int numberOfVisibleItems = 0;
        while (getFitmentGridVisibleItemsCount() != numberOfVisibleItems &&
                driver.isElementDisplayed(fitmentOptionShowMore, Constants.ZERO)) {
            numberOfVisibleItems = getFitmentGridVisibleItemsCount();
            driver.jsScrollToElementClick(fitmentOptionShowMore, false);
        }
        LOGGER.info("showAllItemsOnFitmentGridContainer completed");
    }

    /**
     * Get the number of displayed items from the fitment grid
     *
     * @return number of displayed items
     */
    private int getFitmentGridVisibleItemsCount() {
        LOGGER.info("getFitmentGridVisibleItemsCount started");
        int returnVal = webDriver.findElement(fitmentGridVisibleItemsBy).getText().split("\n").length;
        LOGGER.info("getFitmentGridVisibleItemsCount completed");
        return returnVal;
    }

    /**
     * Clicks on close button to remove all vehicles on session or Vehicles added
     */
    public void clickRemoveRecentVehicleButton() {
        LOGGER.info("clickRemoveRecentVehicleButton started");
        List<WebElement> removeVehicles = webDriver.findElements(removeRecentVehicleBy);
        for (WebElement removeVehicle : removeVehicles) {
            removeVehicle.click();
        }
        LOGGER.info("clickRemoveRecentVehicleButton completed");
    }

    /**
     * Selects the specified fitment label on the fitment modal on the homepage
     *
     * @param inputLabelText Must contain Tire, Wheel, Package, or Accessories
     */
    public void selectFitmentLabel(String inputLabelText) {
        LOGGER.info("selectFitmentLabel started");
        String label = "";
        if (CommonUtils.containsIgnoreCase(inputLabelText, Constants.TIRE)) {
            label = Constants.TIRE.toLowerCase();
        }
        if (CommonUtils.containsIgnoreCase(inputLabelText, Constants.WHEEL)) {
            label = Constants.WHEEL.toLowerCase();
        }
        if (CommonUtils.containsIgnoreCase(inputLabelText, Constants.PACKAGE)) {
            label = Constants.PACKAGE.toLowerCase();
        }
        if (CommonUtils.containsIgnoreCase(inputLabelText, Constants.ACCESSORIES)) {
            label = Constants.ACCESSORIES.toLowerCase();
        }
        Assert.assertFalse("FAIL: The input label '" + inputLabelText + "' is not valid for selectFitmentLabel. " +
                "Must contain Tire, Wheel, Package, or Accessories", label.isEmpty());
        WebElement vehicleFitmentLabel = webDriver.findElement(By.cssSelector(FITMENT_LABEL + label + "']"));
        driver.jsScrollToElementClick(vehicleFitmentLabel);
        LOGGER.info("selectFitmentLabel completed");
    }

    /**
     * Selects the vehicle with fitment details
     *
     * @param year  Year
     * @param make  Make
     * @param modal Modal
     * @param trim  Trim
     */
    public void selectVehicleWithFitmentDetails(String year, String make, String modal, String trim) {
        LOGGER.info("selectVehicleWithFitmentDetails started");
        List<WebElement> yearMake = webDriver.findElements(yearMakeBy);
        List<WebElement> modalTrim = webDriver.findElements(modalTrimBy);
        for (int i = 0; i < yearMake.size(); i++) {
            if (driver.isElementDisplayed(yearMake.get(i)) && yearMake.get(i).getText().replace("\n", " ").equalsIgnoreCase(year + " " + make)) {
                for (int j = 0; j < modalTrim.size(); j++) {
                    if (driver.isElementDisplayed(modalTrim.get(j)) && modalTrim.get(j).getText().replace("\n", " ").equalsIgnoreCase(modal + " " + trim)) {
                        modalTrim.get(j).click();
                        break;
                    }
                }
            }
        }
        LOGGER.info("selectVehicleWithFitmentDetails completed");
    }

    /**
     * Verifies the year's starting for any specified year are displayed
     *
     * @param searchYear specified number for year
     */
    public void assertYearDisplayed(String searchYear) {
        LOGGER.info("assertYearDisplayed started");
        List<WebElement> searchYearButton = webDriver.findElements(fitmentOptionButtonBy);
        for (WebElement searchByYear : searchYearButton) {
            Assert.assertTrue("FAIL: Fitment option for year doesn't contain " + searchYear + ".", searchByYear.
                    getText().contains(searchYear));
        }
        LOGGER.info("assertYearDisplayed completed");
    }

    /**
     * Verifies if the error message "No Match Found" is displayed
     *
     * @param errorMessage "No Match Found" message
     */
    public void assertErrorMessageDisplayedOnFitmentModal(String errorMessage) {
        LOGGER.info("assertErrorMessageDisplayedOnFitmentModal started for error message '" + errorMessage + "'");
        WebElement errorMessageElement = returnWebElement(errorMessage);
        String displayedErrorMessage = errorMessageElement.getText();
        if (errorMessageElement.isDisplayed())
            Assert.assertTrue("FAIL: The error message '" + displayedErrorMessage + "' does not match '" + errorMessage + "'",
                    displayedErrorMessage.equalsIgnoreCase(errorMessage));
        else
            Assert.fail("FAIL: The error message '" + errorMessage + "'is not displayed");
        LOGGER.info("assertErrorMessageDisplayedOnFitmentModal completed for error message '" + errorMessage + "'");
    }

    /**
     * This method will return the web element matching the input String.
     *
     * @param elementName - Name of the By element
     * @return - Web element
     */
    public WebElement returnWebElement(String elementName) {
        LOGGER.info("returnWebElement started");
        switch (elementName) {
            case ConstantsDtc.NO_MATCH_FOUND:
                return webDriver.findElement(FitmentPage.noMatchFoundErrorMessageBy);
            case ConstantsDtc.VIEW_TIRES:
                return webDriver.findElements(CartPage.viewTireOrWheelsButtonBy).get(0);
            case ConstantsDtc.VIEW_WHEELS:
                return webDriver.findElements(CartPage.viewTireOrWheelsButtonBy).get(1);
            default:
                Assert.fail("FAIL: Could not find By objects that matched string passed from step");
                return null;
        }
    }

    /**
     * Clicks on the specified view tires or view wheels button
     *
     * @param text View tires/View Wheels
     */
    public void clickOnButtonOnEmptyCartPage(String text) {
        LOGGER.info("clickOnButtonOnEmptyCartPage started");
        WebElement buttonElement = returnWebElement(text);
        buttonElement.click();
        LOGGER.info("clickOnButtonOnEmptyCartPage completed");
    }

    /**
     * Verifies if all the fitment options are specified
     */
    public void assertAllTireOptions() {
        LOGGER.info("assertAllTireOptions started");
        List<String> tireOptionsText = new ArrayList<>();
        int counter = 0;
        List<WebElement> tireOptions = webDriver.findElements(tireOptionsBy);
        driver.waitSeconds(Constants.TWO);
        for (WebElement tireOption : tireOptions) {
            tireOptionsText.add(tireOption.getText());
        }
        Assert.assertFalse("FAIL: There are duplicate fitment tire options displayed",
                driver.arrayListHasDuplicates(tireOptionsText));
        for (String tireOptionText : tireOptionsText) {
            Assert.assertTrue("FAIL: Unexpected tire fitment option displayed: " + tireOptionText,
                    Arrays.asList(FITMENT_TIRE_OPTIONS).contains(tireOptionText));
            LOGGER.info("PASS: Tire fitment option found : " + tireOptionText);
        }
        LOGGER.info("assertAllTireOptions completed");
    }

    /**
     * Select on the specified fitment by position
     *
     * @param position of the fitment
     */
    public void selectVehicleAtPosition(int position) {
        LOGGER.info("selectVehicleAtPosition started");
        List<WebElement> recentVehicles = webDriver.findElements(recentVehicleFitmentBy);
        recentVehicles.get(position - 1).click();
        LOGGER.info("selectVehicleAtPosition completed");
    }

    /**
     * Select Treadwell on the Fitment page
     */
    public void selectTreadwell() {
        LOGGER.info("selectTreadwell started");
        if (driver.isElementDisplayed(FitmentPopupPage.treadwellLogo, Constants.ONE)) {
            driver.jsScrollToElementClick(FitmentPopupPage.treadwellLogo);
        } else {
            driver.clickElementWithText(FitmentPage.tireOptionsBy, ConstantsDtc.TREADWELL);
        }
        LOGGER.info("selectTreadwell completed");
    }

    /**
     * Verify the fitment panel is displayed or not displayed
     *
     * @param displayStatus "displayed" or "not displayed"
     */
    public void assertFitmentPanelDisplayedNotDisplayed(String displayStatus) {
        LOGGER.info("assertFitmentPanelDisplayedNotDisplayed started verifying fitment panel is " + displayStatus);
        if (displayStatus.equalsIgnoreCase(Constants.DISPLAYED))
            Assert.assertTrue("FAIL: The fitment panel was not displayed",
                    driver.isElementDisplayed(CommonActions.fitmentContainer));
        else if (displayStatus.equalsIgnoreCase(Constants.NOT_DISPLAYED))
            Assert.assertTrue("FAIL: The fitment panel was displayed",
                    !driver.isElementDisplayed(CommonActions.fitmentContainer));
        LOGGER.info("assertFitmentPanelDisplayedNotDisplayed completed. Fitment panel is " + displayStatus);
    }

    /**
     * Select product category from fitment page or popup
     *
     * @param productCategory Tire, Wheel, Package, or Accessories
     */
    public void selectFitmentShoppingCategory(String productCategory) {
        LOGGER.info("selectFitmentShoppingCategory started for " + productCategory);
        WebElement shopForProductsLink = driver.getElementWithText(FitmentPopupPage.fitmentBannerDtLinkBy,
                ConstantsDtc.SHOP_PRODUCTS);
        if (driver.isElementDisplayed(shopForProductsLink, Constants.ONE)) {
            try {
                driver.webElementClick(shopForProductsLink);
            } catch (Exception e) {
                shopForProductsLink = driver.getElementWithText(FitmentPopupPage.fitmentBannerDtLinkBy,
                        ConstantsDtc.SHOP_PRODUCTS);
                if (driver.isElementDisplayed(shopForProductsLink, Constants.ONE)) {
                    driver.webElementClick(shopForProductsLink);
                }
            }
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        }
        selectFitmentLabel(productCategory);
        commonActions.clickContinueWithThisVehicleButton();
        LOGGER.info("selectFitmentShoppingCategory completed for " + productCategory);
    }
}