package dtc.pages;

/**
 * Created by aaronbriel on 9/16/16.
 */

import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import dtc.data.Vehicle;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

public class FitmentPopupPage {

    private Driver driver;
    private WebDriver webDriver;
    private final ProductListPage productListPage;
    private final CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(FitmentPopupPage.class.getName());
    private Vehicle vehicle;

    public FitmentPopupPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        productListPage = new ProductListPage(driver);
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
        vehicle = new Vehicle();
    }

    private static final String[] NON_STAGGERED_FITMENT_LINKS = {
            "All tires", "Best selling", "On promotion", "Optional Tire Sizes", "All wheels", "Optional Wheel Sizes",
            "Highest rated"
    };
    private static final String[] STAGGERED_FITMENT_LINKS = {
            "All tire sets", "Optional Tire Sizes", "All Front Tires", "Best selling", "All Rear Tires",
            "All Front Wheels", "All Rear Wheels", "Optional Wheel Sizes", "Highest rated"
    };
    private static final String[] BASIC_NON_STAGGERED_FITMENT_LINKS = {
            "All tires", "Best selling", "On promotion", "Optional Tire Sizes", "All wheels",
            "Optional Wheel Sizes"
    };

    private static final String STAGGERED_VEHICLE_MESSAGE = "Ok, now that we know your vehicle uses different front " +
            "and rear tire sizes we can help you choose from options that fit.";
    private static final String NON_STAGGERED_VEHICLE_MESSAGE = "Great, now that we know about your vehicle, we can " +
            "help you stay safe on the road. We'll narrow the product options to those that will fit.";
    private static final String BASICNONSTAGGERED = "Basic Non Staggered";
    private static final String BEST_SELLING = "Best selling";
    private static final String SCHEDULE_SERVICE = "Schedule a Service";
    private static final String OPTIONAL_TIRE_SIZES = "Optional Tire Sizes";
    private static final String OPTIONAL_WHEEL_SIZES = "Optional Wheel Sizes";
    private static final String[] EXP_FITMENT_OPTS = {"TIRES", "WHEELS"};
    private static final String TIRES_HEADER = "Results for Tires";
    private static final String WHEELS_HEADER = "Results for Wheels";
    private static final String POPUP_PAGE = "popup page";
    public static final String REVERT_BACK_TO_OE = "Revert to O.E. size";
    public static final String OE_SIZE_MESSAGE = "These tire sizes fit the original wheel diameter of your vehicle and generally do not require purchasing new wheels.";
    public static final String OPTIONAL_SIZE_MESSAGE = "These tire sizes do not fit the original wheel diameter of your vehicle and require a custom wheel size.";

    private static final By closeButtonBy = By.className("fitment-box__close");

    private static final By fitmentVehicleYearMakeBy = By.cssSelector("[class*='fitment-banner__year-make-container']");

    private static final By vehicleMessageBy = By.className("fitment-box__result-desc");

    private static final By fitmentTabName = By.className("dt-tabs__tab");

    private static final By fitmentRangeBy =
            By.cssSelector("[class*='fitment-vehicle-results__optional-size-toggle-label']");

    private static final By staggeredMenuOptionBy = By.className("dt-sub-menu__button");

    private static final By treadwellFitmentBoxBy = By.cssSelector(".fitment-box__pdl-cta");

    private static final By treadwellGetStartedBy = By.className("fitment-box__pdl-cta-button");

    public static final By revertBackToOEBy = By.cssSelector("[class*='revert-to-oe']");

    public static final By optionalSizesHeaderIconBy =
            By.cssSelector("[class*='fitment-vehicle-results__optional-sizes-header-icon']");

    public static final By backToMyVehiclesBy = By.cssSelector("[class*='back-to-vehicles']");

    public static final By StaggeredVehicleSubHeaderBy = By.cssSelector("[class*='dt-sub-menu ']");

    public static final By fitmentBannerDtLinkBy = By.cssSelector("[class*='fitment-banner__dt-link']");

    public static final By staggeredTreadwellFitmentBoxBy = By.cssSelector("[class*='treadwell-container']");

    public static final By sizeMessageBy = By.cssSelector("[class*='fitment-box__optional-sizes-message']");

    public static final By optionalSizeLabelBy = By.cssSelector("[class*='size-label-optional']");

    @FindBy(className = "fitment-box__set-flex")
    public static WebElement fitmentBoxFlex;

    @FindBy(css = "[class*='fitment-vehicle-results__optional-size-toggle-label']")
    public static WebElement fitmentRange;

    @FindBy(css = "[class*='active-optional-size-button']")
    public static WebElement fitmentRangeSelected;

    @FindBy(css = "[class*='fitment-navigation__close']")
    public static WebElement fitmentPopupCloseIcon;

    @FindBy(className = "fitment-vehicle-display__change")
    public static WebElement changeVehicle;

    @FindBy(className = "fitment-box__pdl-cta-button")
    public static WebElement pdlxDrivingDetails;

    @FindBy(css = "[class*='optional-size-toggle-label']")
    public static WebElement fitmentBoxOptionalSizeList;

    @FindBy(css = ".fitment-box__pdl-cta")
    public static WebElement treadwellFitmentBox;

    @FindBy(css = ".fitment-box__pdl-cta-logo")
    public static WebElement treadwellLogo;

    @FindBy(css = ".fitment-box__pdl-cta-slogan")
    public static WebElement treadwellSlogan;

    @FindBy(css = "[class*='fitment-box__optional-sizes-links']")
    public static WebElement fitmentOptionalSizesLinks;

    @FindBy(css = "[class*='optional-size-button']")
    public static WebElement optionalSizeButton;

    @FindBy(css = "[class*='revert-to-oe--staggered']")
    public static WebElement revertToOESize;

    /**
     * Finds an element on the Fitment popup screen and selects it
     *
     * @param fitmentText Fitment option to be selected
     */
    public void selectFitment(String fitmentText) {
        LOGGER.info("selectFitment started for '" + fitmentText + "' selection");
        WebElement fitmentElement = null;
        fitmentElement = driver.getElementWithText(FitmentPage.fitmentOptionButtonBy, fitmentText, Constants.THREE);
        if (fitmentElement == null) {
            //TODO: retest when new safaridriver is stable
            if (Config.isSafari() || Config.isMobile())
                driver.waitForMilliseconds(Constants.TWO_THOUSAND);
            if (fitmentText.matches("\\d+"))
                fitmentElement = driver.getElementWithText(fitmentRangeBy, fitmentText, 10);
            else if (driver.isElementDisplayed(getFitmentElement(fitmentText))) {
                fitmentElement = driver.getElementWithText(getFitmentElement(fitmentText), fitmentText, 10);
                driver.waitForElementVisible(fitmentElement);
            } else {
                try {
                    fitmentElement = driver.getElementWithTextNumericOnly(fitmentRangeBy, fitmentText, 10);
                } catch (Exception e) {
                    try {
                        fitmentElement = driver.getElementWithTextNumericOnly(getFitmentElement(fitmentText), fitmentText, 10);
                        driver.waitForElementVisible(fitmentElement);
                    } catch (Exception ex) {
                        fitmentElement = driver.getElementWithTextNumericOnly(CommonActions.dtLinkBy, fitmentText, 10);
                    }
                }
            }
        }

        //TODO:  may fail to click on element in Safari
        try {
            driver.webElementClick(fitmentElement);
        } catch (NoSuchElementException e) {
            Assert.fail("FAIL: Fitment link '" + fitmentText + "' NOT found. (Full Stack Trace: " + e.toString() + ")");
        } catch (WebDriverException we) {
            //Deals with intermittent issue where other element receives click due to popup animation
            driver.waitForMilliseconds();
            fitmentElement.click();
        }
        driver.waitForPageToLoad(Constants.ZERO);
        LOGGER.info("selectFitment completed for '" + fitmentText + "' selection");
    }

    /**
     * Finds an element on the Fitment popup screen and selects it
     *
     * @param vehicle Fitment vehicle to be selected
     */
    public void selectFitmentVehicle(String vehicle) {
        LOGGER.info("selectFitmentVehicle started");

        //TODO: retest when new safaridriver is stable
        if (Config.isSafari() || Config.isMobile())
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);

        WebElement vehicleButton = driver.getElementWithText(CommonActions.dtButtonBy, vehicle);
        driver.waitForElementVisible(vehicleButton);

        try {
            driver.jsScrollToElement(vehicleButton);
            vehicleButton.click();
        } catch (NoSuchElementException e) {
            Assert.fail("FAIL: Vehicle button \"" + vehicle + "\" NOT found. (Full Stack Trace: " + e.toString() + ")");
        } catch (WebDriverException we) {
            //Deals with intermittent issue where other element receives click due to popup animation
            driver.waitForMilliseconds();
            vehicleButton.click();
        }

        driver.waitForPageToLoad();

        LOGGER.info("selectFitmentVehicle completed");
    }

    /**
     * Clicks 'Change' link on vehicle fitment popup
     */
    public void clickChangeVehicle() {
        LOGGER.info("clickChangeVehicle started");
        driver.waitForElementClickable(changeVehicle);
        changeVehicle.click();
        driver.waitForMilliseconds();
        LOGGER.info("clickChangeVehicle completed");
    }

    /**
     * Returns partial link text of specified fitment
     *
     * @param fitment Fitment to get link for
     * @return By       Partial link text By element
     */
    private By getFitmentElement(String fitment) {
        return By.partialLinkText(fitment);
    }

    /**
     * Verifies passed in vehicle name is present and visible
     *
     * @param vehicle Vehicle title to assert is present on the screen
     */
    public void assertVehicleInPanel(String vehicle) {
        LOGGER.info("assertVehicleInPanel started with vehicle '" + vehicle + "'");
        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);
        WebElement fitmentElement = null;
        int counter = 0;
        do {
            try {
                fitmentElement = webDriver.findElement(fitmentVehicleYearMakeBy);
            } catch (Exception e) {
                driver.waitForMilliseconds();
            }
            counter++;
        } while (fitmentElement == null && counter <= Constants.THIRTY);
        Assert.assertNotNull("FAIL: Vehicle fitment panel was not displayed", fitmentElement);
        Assert.assertTrue("FAILED: Vehicle was not displayed in fitment panel", fitmentElement.getText().toLowerCase()
                .contains(vehicle.toLowerCase()));
        LOGGER.info("assertVehicleInPanel completed with vehicle '" + vehicle + "'");
    }

    /**
     * Verifies the detail message on screen based on the passed in option type
     *
     * @param optionType Staggered or Non-staggered vehicle
     */
    public void assertVehicleDetailsMessage(String optionType) {
        LOGGER.info("assertVehicleDetailsMessage started");
        if (optionType.equalsIgnoreCase(Constants.NONSTAGGERED)) {
            Assert.assertTrue("FAIL: The option type: \"" + optionType + "\" was NOT shown on vehicle details page!",
                    driver.waitForTextPresent(vehicleMessageBy, STAGGERED_VEHICLE_MESSAGE,
                            Constants.THIRTY));
            LOGGER.info("Confirmed that '" + STAGGERED_VEHICLE_MESSAGE + "' shown on vehicle details page.");
        } else if (optionType.equalsIgnoreCase(Constants.STAGGERED)) {
            Assert.assertTrue("FAIL: The option type: \"" + optionType + "\" was NOT shown on vehicle details page!",
                    driver.waitForTextPresent(vehicleMessageBy, NON_STAGGERED_VEHICLE_MESSAGE, Constants.THIRTY));
            LOGGER.info("Confirmed that '" + NON_STAGGERED_VEHICLE_MESSAGE + "' shown on vehicle details page.");
        }
        LOGGER.info("assertVehicleDetailsMessage completed");
    }

    /**
     * Verifies all the Fitment popup links common to the option type
     *
     * @param optionType String staggered or non-staggered
     */
    public void verifyAllFitmentLinksWork(String optionType) {
        LOGGER.info("verifyAllFitmentLinksWork started");
        //TODO: Recheck once new safaridriver is stable. Navigation does NOT work with Version 2.48:
        //org.openqa.selenium.WebDriverException: Yikes! Safari history navigation does not work.
        // We can go forward or back, but once we do, we can no longer communicate with the page...

        //NOTE: Some of the header validations are not visible in mobile with changes as of 3/9/17,
        //so this validation is being skipped for mobile altogether

        List<String> failedLinkList = new ArrayList<>();

        if (!Config.isSafari() && !Config.isMobile()) {
            String[] links = new String[0];
            //TODO: Text values for Best Selling Font + Rear still work as expected but may need to be refactored in the future.
            if (optionType.equalsIgnoreCase(Constants.NONSTAGGERED)) {
                links = STAGGERED_FITMENT_LINKS;
            } else if (optionType.equalsIgnoreCase(ConstantsDtc.STAGGERED)) {
                links = NON_STAGGERED_FITMENT_LINKS;
            } else if (optionType.equalsIgnoreCase(BASICNONSTAGGERED)) {
                links = BASIC_NON_STAGGERED_FITMENT_LINKS;
            }
            for (String link : links) {
                boolean linkVerified;
                WebElement element = null;
                if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DTD)
                        && link.equalsIgnoreCase(SCHEDULE_SERVICE)) {
                    //DTD does not display the "Schedule a Service" link
                    driver.waitForPageToLoad();
                    linkVerified = !driver.isTextPresentInPageSource(link);
                } else {
                    element = webDriver.findElement(By.partialLinkText(link));
                    driver.waitForElementClickable(element);
                    element.click();
                    linkVerified = assertFitmenLinkHeaderResults(link);
                }

                if (!linkVerified) {
                    failedLinkList.add("\"" + link + "\" FAILED Expected Page Header Not Found!\n\t");
                }

                webDriver.navigate().back();
                driver.waitForPageToLoad();
                LOGGER.info(element + " element visible on screen");
            }

            Assert.assertTrue("FAIL: Issues found for one or more \"" + optionType + "\" link(s): "
                    + failedLinkList + "\"!", failedLinkList.isEmpty());
        } else {
            LOGGER.info("SKIPPING TEST FOR SAFARIDRIVER, NAVIGATION NOT SUPPORTED IN VERSION 2.48.");
        }
        LOGGER.info("verifyAllFitmentLinksWork completed");
    }

    /**
     * Verifies the selected class value of the box via provided text value
     *
     * @param value Text value of the fitment box that is selected
     */
    public void assertFitmentBoxIsSelected(String value) {
        LOGGER.info("assertFitmentBoxIsSelected started");
        driver.waitForPageToLoad(Constants.ONE_HUNDRED);
        Assert.assertTrue("Value of the selected element was not " + value, webDriver.findElement
                (CommonActions.fitmentBannerTireBy).getText().contains(value));
        LOGGER.info("assertFitmentBoxIsSelected completed");
    }

    /**
     * Verifies the fitment box contains the provided text value
     *
     * @param value Text value of the fitment box
     */
    public void assertFitmentBoxValue(String value) {
        LOGGER.info("assertFitmentBoxIsSelected started");
        driver.waitForElementVisible(fitmentRange);
        WebElement getFitmentRangeElement = driver.getElementWithText(fitmentRangeBy, value);
        Assert.assertTrue("Value of the selected element was not " + value, getFitmentRangeElement.getText().contains(value));
        LOGGER.info("assertFitmentBoxIsSelected completed");
    }

    /**
     * Verifies the color of the selected fitment box
     */
    public void assertSelectedFitmentBoxColorIsRed() {
        LOGGER.info("assertGoodBetterBestBackgroundColor started");
        driver.waitForElementVisible(fitmentRange);
        String redColor = Constants.RED_COLOR_RGBA;
        String color = fitmentRangeSelected.getCssValue(Constants.BACKGROUND_COLOR);

        if (Config.isFirefox() || (Config.isSafari()))
            redColor = Constants.RED_COLOR_RGB;

        Assert.assertTrue("FAIL: Expected color: " + redColor + " but the actual color was: "
                + color + "!", color.equalsIgnoreCase(redColor));
        LOGGER.info("assertSelectedFitmentBoxColor completed");
    }

    /**
     * Clicks on the fitment selection range box via provided text value
     *
     * @param value Text value of the box to select
     */
    public void selectFitmentBoxOption(String value) {
        LOGGER.info("selectFitmentBoxOption started");
        WebElement tireSizeBox = driver.getElementWithText(fitmentRangeBy, value);
        driver.jsScrollToElementClick(tireSizeBox);
        driver.waitOneSecond();
        LOGGER.info("selectFitmentBoxOption completed");
    }

    /**
     * Closes the Fitment popup
     **/
    public void closeFitmentPopUp() {
        LOGGER.info("closeFitmentPopUp started");
        webDriver.findElement(CommonActions.dtModalCloseBy).click();
        LOGGER.info("closeFitmentPopUp completed");
    }

    /***
     * Returns a boolean based on the success or failure of the header verification for the link coming from the
     * Fitment popup.
     * @param link Link of the fitment option on Fitment results page
     * @return True for successful verification OR False for a failure
     */
    public boolean assertFitmenLinkHeaderResults(String link) {
        LOGGER.info("assertFitmenLinkHeaderResults started");
        if (link.equals(OPTIONAL_TIRE_SIZES) || link.equals(OPTIONAL_WHEEL_SIZES)) {
            return FitmentPopupPage.fitmentBoxFlex.isDisplayed();
        } else if (link.contains(Constants.WHEEL)) {
            try {
                commonActions.assertPageHeader(WHEELS_HEADER);
                return true;
            } catch (Exception e) {
                return false;
            }
        } else if (link.contains(Constants.TIRE)) {
            try {
                commonActions.assertPageHeader(TIRES_HEADER);
                if (link.equals(ConstantsDtc.TIRES_ON_PROMOTION)) {
                    commonActions.assertPageHeader(TIRES_HEADER);
                }
                return true;
            } catch (Exception e) {
                return false;
            }
        } else if (link.equals(BEST_SELLING)) {
            try {
                productListPage.verifySortByValue(ConstantsDtc.BEST_SELLER);
                return true;
            } catch (Exception e) {
                return false;
            }
        } else if (link.equals(SCHEDULE_SERVICE)) {
            try {
                commonActions.assertPageHeader(ConstantsDtc.SCHEDULE_APPOINTMENT);
                return true;
            } catch (Exception e) {
                return false;
            }
        } else {
            LOGGER.info("assertFitmenLinkHeaderResults completed - \"" + link + "\" not recognized by method!");
            return false;
        }
    }

    /**
     * Verifies all fitment options on fitment result window
     */
    public void assertAllFitmentOpts() {
        LOGGER.info("assertAllFitmentOpts started");
        driver.waitForPageToLoad();
        List<WebElement> fitmentOpts = webDriver.findElements(fitmentTabName);

        //verify if all three fitment options found
        if (EXP_FITMENT_OPTS.length != fitmentOpts.size()) {
            Assert.fail("FAIL: All three fitment options not displayed");
        }
        //verify that the value of every fitment option equal to the expected value
        for (int i = Constants.ZERO; i < EXP_FITMENT_OPTS.length; i++) {
            String fitmentValue = fitmentOpts.get(i).getText().toUpperCase();
            if (Arrays.asList(EXP_FITMENT_OPTS).contains(fitmentValue)) {
                LOGGER.info("PASS: Fitment option found : " + fitmentValue);
            } else {
                Assert.fail("FAIL: actual fitment option: '" + fitmentValue + "' expected: '"
                        + EXP_FITMENT_OPTS[i] + "'!");
            }
        }
        LOGGER.info("assertAllFitmentOpts completed");
    }

    /**
     * Clicks PDLX Driving Details Recommended Tires Link
     */
    public void selectPdlxDrivingDetailsRecommendedTiresLink() {
        LOGGER.info("selectPdlxDrivingDetailsRecommendedTiresLink started");
        driver.waitForElementClickable(pdlxDrivingDetails);
        pdlxDrivingDetails.click();
        driver.waitForMilliseconds();
        LOGGER.info("selectPdlxDrivingDetailsRecommendedTiresLink completed");
    }

    /**
     * Clicks the menu buttons on the Fitment Modal popup
     *
     * @param value Text value of the box to select
     */
    public void clickMenuOption(String value, String menuOptionType) {
        LOGGER.info("clickMenuOption started with menuOptionType = '" + menuOptionType + "' and value = '"
                + value + "'");
        By fitmentMenuBy;

        if (menuOptionType.contains(ConstantsDtc.STAGGERED.toLowerCase())) {
            fitmentMenuBy = staggeredMenuOptionBy;
        } else {
            fitmentMenuBy = fitmentTabName;
        }

        driver.waitForElementVisible(fitmentMenuBy, Constants.ONE);

        try {
            List<WebElement> menuOptions = driver.getElementsWithText(fitmentMenuBy, value);
            for (WebElement menuOption : menuOptions) {
                if (menuOption.getText().contains(value) || menuOption.getText().trim().toLowerCase().contains
                        (value.toLowerCase())) {
                    driver.jsScrollToElementClick(menuOption);
                    break;
                }
            }
        } catch (Exception e) {
            Assert.fail("FAIL: did not click '" + value + "' menu option.");
        }
        LOGGER.info("clickMenuOption completed with menuOptionType = '" + menuOptionType + "' and value = '"
                + value + "'");
    }

    /**
     * Validates that there are no duplicate values in the Optional Tire Size list
     */
    public void assertNoDuplicateOptionalTireSizes() {
        LOGGER.info("assertNoDuplicateOptionalTireSizes started");
        List<WebElement> optionalTireSizes = webDriver.findElements(fitmentRangeBy);
        List<String> uniqueOptionalTireSizes = new ArrayList<>();
        List<String> optionalTireSizeList = new ArrayList<>();

        for (WebElement optionalTireSizeListOrder : optionalTireSizes) {
            String optionalTireSize = optionalTireSizeListOrder.getText();
            uniqueOptionalTireSizes.add(optionalTireSize);
        }
        for (String optionalSize : uniqueOptionalTireSizes) {
            if (!optionalTireSizeList.contains(optionalSize)) {
                optionalTireSizeList.add(optionalSize);
            } else {
                Assert.fail("FAIL: Optional Tire size " + optionalSize + " was listed multiple times");
            }
        }
        LOGGER.info("assertNoDuplicateOptionalTireSizes completed");
    }

    /**
     * checks treadwell fitment box contains treadwell logo, slogan and button to go to Treadwell Guide
     *
     * @param page   pop up page or home page
     * @param option displayed or not displayed
     */
    public void assertTreadwellFitmentBox(String page, String option) {
        LOGGER.info("assertTreadwellFitmentBox started");
        if (option.equals(Constants.DISPLAYED)) {
            if (treadwellFitmentBox.isDisplayed()) {
                if (page.equalsIgnoreCase(POPUP_PAGE)) {
                    Assert.assertTrue("FAIL: Treadwell button not visible", driver.isElementDisplayed(
                            webDriver.findElement(treadwellFitmentBoxBy).findElement(treadwellGetStartedBy)));
                }
                Assert.assertTrue("FAIL: Treadwell icon not visible", treadwellLogo.isDisplayed());
                Assert.assertTrue("FAIL: Treadwell slogan not visible", treadwellSlogan.isDisplayed());
            }
        } else {
            Assert.assertTrue("FAIL: Treadwell button is visible",
                    driver.isElementNotVisible(treadwellFitmentBoxBy, Constants.FIVE));
        }
        LOGGER.info("assertTreadwellFitmentBox completed");
    }

    /**
     * Click the button to go to Treadwell Guide
     */
    public void clickTreadwellButton() {
        LOGGER.info("clickTreadwellButton started");
        WebElement findYourTires = webDriver.findElement(treadwellFitmentBoxBy).findElement(treadwellGetStartedBy);
        driver.jsScrollToElementClick(findYourTires);
        driver.waitForMilliseconds();
        LOGGER.info("clickTreadwellButton completed");
    }

    /**
     * This method will return the web element matching the input String.
     *
     * @param elementName - Name of the By element
     * @return - Web element
     */
    public WebElement returnWebElement(String elementName) {
        LOGGER.info("returnWebElement started");
        switch (elementName) {
            case CommonActions.OPTIONAL_TIRE_AND_WHEEL_SIZE:
                return webDriver.findElement(CommonActions.collapsibleToggleBy);
            case CommonActions.WHAT_ARE_YOU_SHOPPING_FOR:
                return CommonActions.whatAreYouShoppingForHeadingButton;
            case CommonActions.REVERT_TO_OE_SIZE:
                return revertToOESize;
            default:
                Assert.fail("FAIL: Could not find By objects that matched string passed from step");
                return null;
        }
    }

    /**
     * Verifies the fitment optional size are unique
     */
    public void assertFitmentOptionalSizes() {
        LOGGER.info("assertFitmentOptionalSizes started");
        String optionalFitmentSizes = fitmentOptionalSizesLinks.getText();
        List<String> optionalTireSizeList = Arrays.asList(optionalFitmentSizes.split("\\n\"\\n"));
        List<String> uniqueOptionalFitmentSizes = new ArrayList<>();
        for (String optionalSize : optionalTireSizeList) {
            optionalSize = optionalSize.substring(0, 9);
            if (!uniqueOptionalFitmentSizes.contains(optionalSize)) {
                uniqueOptionalFitmentSizes.add(optionalSize);
            } else {
                Assert.fail("FAIL: Optional fitment size " + optionalSize + " was listed multiple times");
            }
        }
        LOGGER.info("assertFitmentOptionalSizes completed");
    }

    /**
     * Verifies the badge displayed for the selected optional size
     *
     * @param badge Text on the badge
     * @param size  Optional size
     */
    public void assertBadgeDisplayedForSize(String badge, String size) {
        LOGGER.info("assertBadgeDisplayedForSize started");
        Assert.assertTrue("FAIL: The fitment sizes did not match", fitmentRangeSelected.getText().contains(size)
                && fitmentRangeSelected.getText().contains(badge));
        LOGGER.info("assertBadgeDisplayedForSize completed");
    }

    /**
     * Verifies if the optional tire and size header is expanded or collapsed
     *
     * @param state Collapsed or Expanded
     */
    public void assertOptionalSizeHeader(String state) {
        LOGGER.info("assertOptionalSizeHeader started");
        String optionalSizeHeaderStatus = webDriver.findElement(CommonActions.collapsibleToggleBy).getAttribute(Constants.CLASS);
        if (state.equalsIgnoreCase(ConstantsDtc.COLLAPSED)) {
            Assert.assertFalse("FAIL: The Optional size header is expanded", optionalSizeHeaderStatus.contains(Constants.OPEN));
        } else {
            Assert.assertTrue("FAIL: The Optional size header is collapsed", optionalSizeHeaderStatus.contains(Constants.OPEN));
        }
        LOGGER.info("assertOptionalSizeHeader completed");
    }

    /**
     * Verify the message on the homepage fitment modal for standard and staggered fitment sizes
     *
     * @param size OE size or Optional size
     */
    public void assertOptionalSizeMessage(String size) {
        LOGGER.info("assertOptionalSizeMessage started");
        WebElement fitmentBoxMessage = webDriver.findElement(sizeMessageBy);
        if (size.equalsIgnoreCase(ConstantsDtc.OE_SIZE)) {
            Assert.assertTrue("FAIL: The expected message: " + OE_SIZE_MESSAGE + " did not match the actual message: " +
                    "" + fitmentBoxMessage.getText() + ".", OE_SIZE_MESSAGE.equalsIgnoreCase(fitmentBoxMessage.getText()));
        } else {
            Assert.assertTrue("FAIL: The expected message: " + OPTIONAL_SIZE_MESSAGE + " did not match the actual message: " +
                    "" + fitmentBoxMessage.getText() + ".", OPTIONAL_SIZE_MESSAGE.equalsIgnoreCase(fitmentBoxMessage.getText()));
        }
        LOGGER.info("assertOptionalSizeMessage completed");
    }
}