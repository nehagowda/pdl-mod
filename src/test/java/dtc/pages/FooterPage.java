package dtc.pages;

import common.Constants;
import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.CommonUtils;
import utilities.Driver;

import java.util.Iterator;
import java.util.Set;
import java.util.List;
import java.util.Arrays;
import java.util.logging.Logger;

/**
 * Created by aaronbriel on 9/22/16.
 */
public class FooterPage {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(FooterPage.class.getName());

    public FooterPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(className = "business-locator")
    public static WebElement pleaseRead;

    @FindBy(className = "body-container")
    public static WebElement welcomeMessage;

    @FindBy(css = "h3.footer__quote-headline-header")
    public static WebElement footerTextElement;

    public static final By payYourBillHeaderSubtextBy = By.className("synchrony-hero-heading-subtext");
    public static final By footerCopyrightTextBy = By.className("footer__copyright");
    public static final By footerSectionLinkBy = By.className("footer__section-list-item");
    public static final String COPYRIGHT_TEXT = "© 2017 The Reinalt-Thomas Corporation. All rights reserved.";
    public static final String FOOTER_HEADLINE_TEXT = "Fast & Friendly service";
    public static final String CONTACT_US = "Contact Us";

    // Footer links ConstantsDtc.TIRE_SAFETY, ConstantsDtc.RETURN_POLICY - Issue in QA1
    public static List<String> footerLinks = Arrays.asList(
            ConstantsDtc.TIRE_SEARCH, ConstantsDtc.WHEEL_SEARCH,
            Constants.SERVICES, ConstantsDtc.DEALS_AND_REBATES, ConstantsDtc.CREDIT, ConstantsDtc.APPLY_NOW,
            ConstantsDtc.COMMERCIAL_PAYMENTS, ConstantsDtc.ABOUT_US, ConstantsDtc.OUR_STORY, ConstantsDtc.MOTORSPORTS,
            ConstantsDtc.CAREERS, ConstantsDtc.TIRE_SIZE_CALCULATOR, ConstantsDtc.CHECK_TIRE_PRESSURE,
            ConstantsDtc.MORE_TOPICS, ConstantsDtc.CUSTOMER_CARE, ConstantsDtc.STORE_LOCATOR, ConstantsDtc.APPOINTMENTS,
            ConstantsDtc.REGIONAL_OFFICES, CONTACT_US, ConstantsDtc.YOUTUBE, ConstantsDtc.FACEBOOK,
            ConstantsDtc.TWITTER, ConstantsDtc.PINTEREST, ConstantsDtc.INSTAGRAM, ConstantsDtc.GOOGLE);

    /**
     * Helper method for constructing and clicking an autoClass named element
     * as opposed to straight linkText
     *
     * @param type     Type of link: header, footer...etc
     * @param linkText Text of the link to be clicked
     */
    // TODO: Remove after page validation completion
    public void clickAutoClassLink(String type, String linkText) {
        LOGGER.info("clickAutoClassLink started");
        driver.waitForPageToLoad();
        if (linkText.equalsIgnoreCase(CONTACT_US)) {
            driver.clickElementWithLinkText(CONTACT_US);
        } else {
            driver.clickElementByAutoClassName(type, linkText);
        }
        LOGGER.info("clickAutoClassLink completed");
    }

    /**
     * Clicks customer care links based on the input text
     *
     * @param text Text to assert on page
     */
    public void assertApplyNowPageText(String text) {
        LOGGER.info("assertApplyNowPageText started");
        driver.waitForElementVisible(pleaseRead);
        Assert.assertTrue("FAIL: terms_heading header on \"Apply Now\" page did NOT display!",
                pleaseRead.isDisplayed());
        Assert.assertTrue("FAIL: terms_heading header did NOT contain \"" + text + "\"!",
                pleaseRead.getText().contains(text));
        LOGGER.info("assertApplyNowPageText completed");
    }

    /**
     * Verifies the 'text' header is displayed on teh page
     *
     * @param text String text of the displayed header
     */
    public void assertCommercialPaymentsText(String text) {
        LOGGER.info("assertCommercialPaymentsText started");
        Assert.assertTrue("FAIL: welcome-message header on \"Commercial Payment\" page did NOT display!",
                welcomeMessage.isDisplayed());
        Assert.assertTrue("FAIL: welcome-message header did NOT contain \"" + text + "\"!",
                welcomeMessage.getText().contains(text));
        LOGGER.info("assertCommercialPaymentsText completed");
    }

    /**
     * Helper method to click a hyperlink via linkText
     *
     * @param linkText text of the element to click
     */
    public void clickHyperLink(String linkText) {
        LOGGER.info("clickHyperLink started");
        //TODO: These links need to be updated with autoClass names
        driver.clickElementWithLinkText(linkText);
        LOGGER.info("clickHyperLink completed");
    }

    /**
     * Verifies the URL of the social media site clicked on in the footer
     *
     * @param urlText Partial text to be found inside the page URL
     */
    public void verifySocialMediaUrl(String urlText) {
        LOGGER.info("verifySocialMediaUrl started for " + urlText);
        String mainHandle = webDriver.getWindowHandle();
        Set allHandles = webDriver.getWindowHandles();
        Iterator iter = allHandles.iterator();
        while (iter.hasNext()) {
            String popupHandle = iter.next().toString();
            if (!popupHandle.contains(mainHandle)) {
                webDriver.switchTo().window(popupHandle);
                commonActions.waitForUrl(urlText, Constants.TEN);
            }
        }
        LOGGER.info("verifySocialMediaUrl completed for " + urlText);
    }

    /**
     * Verifies the Footer has a Copyright Text
     */
    public void verifyCopyrightText() {
        LOGGER.info("verifyCopyright Text started");
        driver.waitForElementVisible(footerCopyrightTextBy);
        String copyrightText = webDriver.findElement(footerCopyrightTextBy).getText();
        Assert.assertTrue("FAIL: \"" + copyrightText + "\" does NOT contain " + "\"" + COPYRIGHT_TEXT + "\"!", copyrightText.contains(COPYRIGHT_TEXT));
        LOGGER.info("verifyCopyright Text completed");
    }

    /**
     * Verify that footer link is displayed
     *
     * @param linkText Text in the link to validate
     */
    public void assertFooterLinkIsDisplayed(String linkText) {
        LOGGER.info("assertFooterLinkIsDisplayed started with linkText \"" + linkText + "\"");

        WebElement linkElement = driver.getElementWithText(footerSectionLinkBy, linkText);
        Assert.assertTrue("FAIL: Link " + linkText + " did NOT display ", driver.isElementDisplayed(linkElement));
        LOGGER.info("assertFooterLinkIsDisplayed completed with linkText \"" + linkText + "\"");
    }

    /**
     * Verify the Footer Headline Text
     */
    public void verifyFooterHeadlineText() {
        LOGGER.info("verifyFooterHeadlineText Text started");
        driver.waitForElementVisible(footerTextElement);
        String footerHeadlineText = footerTextElement.getText();
        Assert.assertTrue("FAIL: \"" + footerHeadlineText + "\" does NOT contain " + "\"" + FOOTER_HEADLINE_TEXT
                + "\"!", footerHeadlineText.contains(FOOTER_HEADLINE_TEXT));
        LOGGER.info("verifyFooterHeadlineText Text completed");
    }

    /**
     * Verify Footer Links
     */
    public void verifyFooterLinks() {
        LOGGER.info("verifyFooterLinks started");
        for (String footer : FooterPage.footerLinks) {
            LOGGER.info("verifyFooterLinks started for " + footer);
            String urlText = footer;
            if (driver.isElementDisplayed(driver.getElementWithText(commonActions.footerSectionLinkBy, footer))) {
                driver.clickElementWithLinkText(footer);
            } else {
                driver.getElementWithClassContainingText(footer).click();
            }
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
            String footerHeaderBreadCrumbUrl = returnHeaderBreadCrumbText(footer);
            String footerHeader = footerHeaderBreadCrumbUrl.split("%")[0].trim();
            String footerBreadCrumb = footerHeaderBreadCrumbUrl.split("%")[1].split("#")[0].trim();
            String footerUrl = footerHeaderBreadCrumbUrl.split("#")[1].trim();
            driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
            if (!(footerBreadCrumb.equalsIgnoreCase("null"))) {
                commonActions.verifyBreadcrumbLinks(footerBreadCrumb);
            }
            if (!(footerUrl.equalsIgnoreCase("null"))) {
                verifySocialMediaUrl(urlText);
            }
            if (!(footerHeader.equalsIgnoreCase("null"))) {
                commonActions.assertPageHeader(footerHeader);
            }
            commonActions.switchToOriginatedTabOrPage();
        }
        LOGGER.info("verifyFooterLinks completed");
    }

    /**
     * Return header text for footer link urls
     *
     * @param link Footer links
     * @Return header text, breadcrumb and socialMediaUrl
     */
    public String returnHeaderBreadCrumbText(String link) {
        LOGGER.info("returnHeaderBreadCrumbText started for " + link);
        String headerText = null;
        String breadCrumb = null;
        String socialMediaUrl = null;
        switch (link) {
            case ConstantsDtc.TIRE_SEARCH:
                headerText = SiteMapPage.TIRES_MIXED_CASE;
                break;
            case ConstantsDtc.WHEEL_SEARCH:
                headerText = ConstantsDtc.WHEELS_NAME;
                break;
            case Constants.SERVICES:
                headerText = ConstantsDtc.WHEEL_AND_TIRE_SERVICES;
                breadCrumb = Constants.HOME + ", " + Constants.SERVICES;
                break;
            case ConstantsDtc.DEALS_AND_REBATES:
                headerText = ConstantsDtc.DEALS_AND_REBATES;
                breadCrumb = Constants.HOME + ", " + ConstantsDtc.DEALS_AND_REBATES;
                break;
            case ConstantsDtc.CREDIT:
                headerText = ConstantsDtc.DISCOUNT_TIRE_CREDIT_CARD;
                breadCrumb = Constants.HOME + ", " + ConstantsDtc.FINANCE;
                break;
            case ConstantsDtc.APPLY_NOW:
                headerText = ConstantsDtc.YOUR_APPLICATION;
                break;
            case ConstantsDtc.COMMERCIAL_PAYMENTS:
                headerText = ConstantsDtc.LOGIN_SCREEN;
                break;
            case ConstantsDtc.ABOUT_US:
                headerText = ConstantsDtc.ADDITIONAL_LINKS;
                breadCrumb = Constants.HOME + ", " + ConstantsDtc.ABOUT_US;
                break;
            case ConstantsDtc.OUR_STORY:
                headerText = ConstantsDtc.ADDITIONAL_LINKS;
                breadCrumb = ConstantsDtc.ABOUT_US + ", " + ConstantsDtc.OUR_STORY;
                break;
            case ConstantsDtc.MOTORSPORTS:
                headerText = ConstantsDtc.DISCOUNT_TIRE_SITE + " " + ConstantsDtc.MOTORSPORTS;
                breadCrumb = ConstantsDtc.ABOUT_US + ", " + ConstantsDtc.MOTORSPORTS;
                break;
            case ConstantsDtc.CAREERS:
                headerText = ConstantsDtc.JOIN_OUR_TALENT_NETWORK;
                break;
            case ConstantsDtc.TIRE_SAFETY:
                headerText = ConstantsDtc.TIRE_SAFETY;
                breadCrumb = ConstantsDtc.TIPS_AND_GUIDES + ", " + ConstantsDtc.TIRE_SAFETY;
                break;
            case ConstantsDtc.TIRE_SIZE_CALCULATOR:
                headerText = ConstantsDtc.TIRE_SIZE_AND_CONVERSION_CALCULATOR;
                breadCrumb = ConstantsDtc.TIPS_AND_GUIDES + ", " + ConstantsDtc.TIRE_SIZE_CALCULATOR;
                break;
            case ConstantsDtc.CHECK_TIRE_PRESSURE:
                headerText = ConstantsDtc.CHECKING_TIRE_AIR_PRESSURE;
                breadCrumb = ConstantsDtc.TIPS_AND_GUIDES + ", " + ConstantsDtc.CHECKING_AIR_PRESSURE;
                break;
            case ConstantsDtc.MORE_TOPICS:
                headerText = ConstantsDtc.TIPS_AND_GUIDES;
                breadCrumb = Constants.HOME + ", " + ConstantsDtc.TIPS_AND_GUIDES;
                break;
            case ConstantsDtc.CUSTOMER_CARE:
                headerText = ConstantsDtc.CUSTOMER_SERVICE;
                breadCrumb = Constants.HOME + ", " + ConstantsDtc.CUSTOMER_CARE;
                break;
            case ConstantsDtc.STORE_LOCATOR:
                headerText = ConstantsDtc.STORE_LOCATOR;
                break;
            case ConstantsDtc.APPOINTMENTS:
                headerText = ConstantsDtc.SERVICE_APPOINTMENT;
                if (driver.isTextPresentInPageSource(ConstantsDtc.ACTION_NEEDED)) {
                    headerText = ConstantsDtc.ACTION_NEEDED;
                }
                break;
            case ConstantsDtc.RETURN_POLICY:
                headerText = ConstantsDtc.OUR + " " + ConstantsDtc.RETURN_POLICY;
                breadCrumb = ConstantsDtc.CUSTOMER_CARE + ", " + ConstantsDtc.RETURN_POLICY;
                break;
            case ConstantsDtc.REGIONAL_OFFICES:
                headerText = ConstantsDtc.REGIONAL_OFFICES;
                breadCrumb = ConstantsDtc.ABOUT_US + ", " + ConstantsDtc.REGIONAL_OFFICES;
                break;
            case CONTACT_US:
                headerText = ConstantsDtc.ADDITIONAL_LINKS;
                breadCrumb = ConstantsDtc.ABOUT_US + ", " + CONTACT_US;
                break;
            case ConstantsDtc.YOUTUBE:
                socialMediaUrl = ConstantsDtc.YOUTUBE + ConstantsDtc.DOT_COM + ConstantsDtc.DISCOUNT_TIRE;
                break;
            case ConstantsDtc.FACEBOOK:
                socialMediaUrl = ConstantsDtc.FACEBOOK + ConstantsDtc.DOT_COM + ConstantsDtc.DISCOUNT_TIRE;
                break;
            case ConstantsDtc.TWITTER:
                socialMediaUrl = ConstantsDtc.TWITTER + ConstantsDtc.DOT_COM + ConstantsDtc.DISCOUNT_TIRE;
                break;
            case ConstantsDtc.PINTEREST:
                socialMediaUrl = ConstantsDtc.PINTEREST + ConstantsDtc.DOT_COM + ConstantsDtc.TIRES_DOT_COM;
                break;
            case ConstantsDtc.INSTAGRAM:
                socialMediaUrl = ConstantsDtc.INSTAGRAM + ConstantsDtc.DOT_COM + ConstantsDtc.DISCOUNT_UNDER_TIRE;
                break;
            case ConstantsDtc.GOOGLE:
                socialMediaUrl = ConstantsDtc.PLUS_GOOGLE + ConstantsDtc.DOT_COM + ConstantsDtc.DISCOUNT_TIRE;
                break;
            default:
                Assert.fail("FAIL: Could not find footer link name " + link
                        + " that matched inside footerLinks array list");
        }
        String headerBreadCrumbUrl = headerText + "%" + breadCrumb + "#" + socialMediaUrl;
        LOGGER.info("returnHeaderBreadCrumbText completed for " + link);
        return headerBreadCrumbUrl;
    }
}