package dtc.pages;

import java.util.logging.Logger;

import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

import dtc.data.ConstantsDtc;
import utilities.Driver;

/**
 * Created by collinreed on 7/14/17.
 */
public class ItemAddedToYourCartPopupPage {

    private final Driver driver;
    private final WebDriver webDriver;
    private final Logger LOGGER = Logger.getLogger(ItemAddedToYourCartPopupPage.class.getName());
    private final CommonActions commonActions;
    
    public ItemAddedToYourCartPopupPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
    }
    
    private static final By itemAddedToYourCartHeader = By.className("order");
    private static final By addToCartBrandNonReactBy = By.className("order-list__brandname");
    private static final By addToCartNameNonReactBy = By.className("order-list__productname");
    private static final By addToCartBrandBy = By.cssSelector("[class*='add-to-cart__brand']");
    private static final By addToCartNameBy = By.cssSelector("[class*='add-to-cart__name']");

    /**
	 * Verify the 'Item added to your cart!" popup displayed 
	 */
    public void assertItemAddedToYourCartPopUp() {
        LOGGER.info("assertItemAddedToYourCartPopUp started");
        String header = "Item added to your cart!";

        if (!driver.isElementDisplayed(CommonActions.popUpDialog, 5)) {
            Assert.assertTrue("FAIL: Page header " + header + "\"!",
                    webDriver.findElement(itemAddedToYourCartHeader).getText().contains(header));
        }

        LOGGER.info("assertItemAddedToYourCartPopUp completed");
    }

    /**
     * Verify the 'Item added to your cart!' popup contains the selected tires
     */
    public void assertItemAddedToYourCartSelectedTires() {
        LOGGER.info("assertItemAddedToYourCartSelectedTires started");
        driver.waitForPageToLoad();
        String expectedBrand = commonActions.productInfoListGetValue(ConstantsDtc.BRAND);
        String expectedProduct = commonActions.productInfoListGetValue(ConstantsDtc.PRODUCT);
        String displayBrand = "";
        String displayProduct = "";

        if (driver.isElementDisplayed(addToCartBrandBy, 1)) {
            displayBrand = webDriver.findElement(addToCartBrandBy).getText();
            displayProduct = webDriver.findElement(addToCartNameBy).getText();
        }
        else {
            displayBrand = webDriver.findElement(addToCartBrandNonReactBy).getText();
            displayProduct = webDriver.findElement(addToCartNameNonReactBy).getText();
        }

		Assert.assertTrue("FAIL: Popup Brand incorrect.  Expected:  " + expectedBrand +
						".  Actual:  " + displayBrand, displayBrand.equalsIgnoreCase(expectedBrand));

		Assert.assertTrue("FAIL: Popup Product incorrect.  Expected:  " + expectedProduct +
						".  Actual:  " + displayProduct, displayProduct.equalsIgnoreCase(expectedProduct));

        LOGGER.info("Verified the selected tire on the Added To Your Cart Popup page is '" + displayBrand + " | " + displayProduct);
        LOGGER.info("assertItemAddedToYourCartSelectedTires completed");
    }

    /**
     * Clicks the "View shopping cart" button on the "Item added to your cart" popup displayed when clicking
     * "Add to cart" on the Compare Products page
     */
    public void clickViewShoppingCart() {
        LOGGER.info("clickViewShoppingCart started");
        assertItemAddedToYourCartPopUp();
        driver.clickElementWithText(CommonActions.btnDefaultBy, ConstantsDtc.VIEW_SHOPPING_CART);
        LOGGER.info("clickViewShoppingCart completed");
    }

    /**
     * Clicks the "Continue Shopping" button on the "Item added to your cart" popup displayed when clicking
     * "Add to cart" on the Compare Products page
     */
    public void clickContinueShopping() {
        LOGGER.info("clickViewShoppingCart started");
        assertItemAddedToYourCartPopUp();
        driver.clickElementWithText(CommonActions.btnDefaultBy, ConstantsDtc.CONTINUE_SHOPPING);
        LOGGER.info("clickViewShoppingCart completed");
    }
}
