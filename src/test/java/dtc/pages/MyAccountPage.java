package dtc.pages;

import common.Config;
import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import common.Constants;
import utilities.Driver;
import dtc.data.Customer;
import webservices.pages.WebServices;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import static webservices.pages.WebServices.TOKEN;

/**
 * Created by @ankitarora on 02/14/18.
 */
public class MyAccountPage {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private Customer customer;
    private HomePage hompage;
    private final Logger LOGGER = Logger.getLogger(MyAccountPage.class.getName());
    private List<String> orderNumberListFromHistoryTab = new ArrayList<>();
    private List<String> orderNumberAppointmentListFromHistoryTab = new ArrayList<>();

    public MyAccountPage(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        hompage = new HomePage(driver);
        customer = new Customer();
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(css = "[id ='e-mail-address']")
    public static WebElement myAccountEmailAddress;

    @FindBy(id = "reset-password-email")
    public static WebElement myAccountResetPasswordEmail;

    @FindBy(css = "[id = 'password']")
    public static WebElement myAccountPassword;

    @FindBy(className = "form-group__input-toggle")
    public static WebElement myAccountForgotPassword;

    @FindBy(className = "dt-checkbox__display")
    public static WebElement keepSignedInCheckbox;

    @FindBy(linkText = "Sign-up Now")
    public static WebElement myAccountSignUpNow;

    @FindBy(css = "[class*='my-account-modals__forgot-password-text']")
    public static WebElement forgotPasswordLabelElement;

    @FindBy(className = "my-account-modals__password-reset-confirmation___2dHur")
    public static WebElement resetPasswordInstructionElement;

    @FindBy(id = "create-a-new-password")
    public static WebElement myAccountResetPassword;

    @FindBy(css = "[class*='react-selectize-search-field-and-selected-values']")
    public static WebElement accountInformationDropDown;

    @FindBy(className = "dt-checkbox")
    public static WebElement checkboxInput;

    @FindBy(xpath = "//span[contains(text(),'delete')]")
    public static WebElement deleteLinkForSecondaryContact;

    @FindBy(css = "[for= phone-type")
    public static WebElement phoneType;

    @FindBy(css = "[class*='form-group '] [name*='regionIso']")
    public static WebElement state;

    @FindBy(css = "[for='country']")
    public static WebElement country;

    @FindBy(css = "[class*='dt-form--cover']")
    public static WebElement createAccountModal;

    @FindBy(xpath = "//button[contains(text(),'Use USPS Corrected Address')]")
    public static WebElement uspsButton;

    @FindBy(id = "create-account")
    public static WebElement createAccountCheckBox;

    @FindBy(id = "current-password")
    public static WebElement currentPasswordField;

    @FindBy(id = "new-password")
    public static WebElement newPasswordField;

    @FindBy(id = "re-type-new-password")
    public static WebElement reTypeNewPassword;

    @FindBy(css = "[class*='my-orders-list__order']")
    public static WebElement ordersTabTextBody;

    @FindBy(css = "[class='my-appointments']")
    public static WebElement appointmentTabTextBody;

    @FindBy(css = "[class*='appointment-status__appointment-info__my-order']")
    public static WebElement appointmentInfoMyOrder;

    public static By myAccountErrorValidationBy = By.className("dt-error-message");
    private static By myProfileHeadingBy = By.cssSelector("[class*='index__heading']");
    private static By myAccountAddContactInformationBy = By.className("my-profile__action");
    private static By myProfileContactActionBy = By.cssSelector("[class*=' my-profile__contact-action dt-link']");
    private static By contactInformationBy = By.cssSelector(".simple-option > span");
    public static By checkoutFormCreateAccountBy = By.cssSelector("label[class*='checkout-form__create-account']");
    public static By checkoutFormAttentionMessageBy = By.className("checkout-form__attention-message");
    private static final By myAccountTabBy = By.cssSelector("span[class='my-account__tab-label']");
    public static final By myAccountSelectedTabBy = By.className("dt-tabs__tab--selected");
    public static final By getSignInBy = By.cssSelector("[class*='header__nav-button-label']");
    public static final By signInIconBy = By.cssSelector("[class*='myaccount-button']");
    public static final By contactInfoBy = By.cssSelector("[class*='my-profile__contact-action']");
    public static final By primaryAndSecondaryContactInfoBy = By.cssSelector("[class*='my-profile__contact-item']");
    public static final By alternateContactAddressBy =
            By.cssSelector("[class*='my-profile__contact-item--alternate-contact']");
    public static final By primaryContactInfoBy = By.cssSelector("div[class='my-profile__addresses']");
    private static final By orderDetailsInAppointmentHistoryTabListBy =
            By.cssSelector("[class*='appointment-status__appointment-info__my-order'] p>span:nth-child(2)");
    public static final By orderDateInHistoryTabListBy =
            By.cssSelector("[class*='my-orders__order-info'] p:nth-of-type(1)");
    public static final By orderDetailsInOrderHistoryTabListBy = By.cssSelector("[class*='my-orders__order-list-item'] " +
            "[class*='my-orders__order-info'] div:nth-child(1) > p:nth-child(2) >span");
    public static final By viewDetailsBy = By.xpath("//*[contains(text(),'view details')]");
    public static final By viewOrderBy = By.xpath("//*[contains(text(),'view order')]");
    public static final By deleteSavedVehicleListBy = By.cssSelector("[class*='vehicles-list__vehicle-list--saved'] " +
            "[class*= 'vehicle-detail__content'] a[class*='delete-link']");
    public static final By deleteSavedVehicleListMobileBy =
            By.cssSelector("[class*='vehicle-detail__mobile-vehicle-actions']" +
                    " [class*= 'dt-link vehicle-detail__delete-link']");
    public static final By saveVehiclesLinkBy = By.cssSelector("[class*='dt-link  vehicle-detail__save-link___']" +
            "[class*='display-sm'][style*='visible']");
    public static final By saveVehiclesLinkMobileBy =
            By.cssSelector("[class*='vehicle-detail__mobile-vehicle-actions'] [style*='visible']");
    public static final By paginationNumberBy = By.cssSelector("[class*='pagination__page-number']");
    public static final By profileEditPrimaryLinkBy =
            By.cssSelector("[class='my-profile__contact-item--primary-contact'] [class*='dt-link']");
    public static final By profileEditAlternateLinkBy = By.cssSelector("[class = 'my-profile__contact'] " +
            "[class='my-profile__contact-item'] [class='my-profile__contact-actions'] a");
    public static final By emailAndPasswordEditLinkBy = By.cssSelector("[class*='my-profile__details-edit dt-link']");
    public static final By appointmentNumbers =
            By.cssSelector("[class*='appointment-status__appointment-info__my-order'] [class*='common__p']");
    public static final By sortOptionOrderSummaryPageBy = By.cssSelector("[class*='my-orders-list__sort-container']");
    public static final By sortOptionAppointmentSummaryPageBy =
            By.cssSelector("[class*='my-appointment-list__sort-container']");
    public static final By myOrdersInfo = By.cssSelector("[class*='my-orders__order-info']");
    public static final By myOrderCancelled = By.cssSelector("[class*='my-orders__ordercancelled']");

    public static final String RESET_PASSWORD = "Reset Password";
    private static final String SIGN_IN = "Sign-in";
    private static final String EMAIL_VALIDATION_MESSAGE = "A valid e-mail address is required";
    private static final String PASSWORD_PROGRESS_STEPS_LABELS = "Passwords must include at least seven characters and at least one letter and one number";
    private static final String FAKE_EMAIL = "@DTATDTD.com";
    private static final String FORGOT_PASSWORD_TEXT = "If you have an account, we'll send you instructions on how to reset your password:";
    private static final String RESET_PASSWORD_INSTRUCTIONS = "An e-mail was sent to the address you've provided. Please follow the instructions to reset your password.";
    public static final String PRIMARY = "Primary";
    private static final String CONTACT_INFO = "Contact Info";
    public static final String EMAIL_VERIFICATION_LINK_MESSAGE = "Once you've placed your order, a verification e-mail is sent to the address you've provided. In that e-mail is a link to authenticate your account.";
    public static final String ACCOUNT_CONFIRMATION_MODAL = "account confirmation modal";
    public static final String INVALID_ADDRESS = "250242 N Scottsdale road";
    public static final String EXISTING_EMAIL_ERROR_MESSAGE = "An account already exists for this email address.";
    public static final String CREATE_AN_ACCOUNT_USING_MY_INFORMATION = "Create an account using my information";
    public static final String CURRENT_PASSWORD = "current password";
    public static final String NEW_PASSWORD = "new password";
    public static final String RE_TYPE_NEW_PASSWORD = "re type new password";
    public static final String NO_ONLINE_ORDERS_MESSAGE = "You have no online orders on file";
    public static final String FETCHING_ORDERS_FAILED = "Fetching orders failed.";
    public static final String CURRENTLY_HAVE_NO_APPOINTMENTS_MESSAGE = "You currently have no appointments scheduled.";
    private ArrayList<String> invalidEmailAddress = new ArrayList<>(Arrays.asList("testdiscounttire.com",
            "testAtTheRatediscounttire.com", "test@discounttirecom", "test@discounttirecom--", "test@test@gmail.com"));
    private ArrayList<String> invalidPassword = new ArrayList<>(Arrays.asList("testing", "Testing", "123"));
    private int attemptsCounter = 0;

    /*
     * Input text value in MyAccount Sign-in field
     *
     * @param element The web element to select
     * @param text    String contains text
     */
    public void inputTextToMyAccountField(WebElement element, String text) {
        LOGGER.info("inputTextToMyAccountField started");
        driver.waitForPageToLoad();
        driver.waitForElementClickable(element);
        driver.clearInputAndSendKeys(element, text);
        LOGGER.info("inputTextToMyAccountField completed");
    }

    /**
     * Click forgot password link
     */
    public void clickForgotPasswordLink() {
        LOGGER.info("clickForgotPasswordLink started");
        driver.waitForPageToLoad();
        driver.waitForElementClickable(myAccountForgotPassword);
        myAccountForgotPassword.click();
        LOGGER.info("clickForgotPasswordLink completed");
    }

    /**
     * Click Reset Password button
     */
    public void clickResetPasswordButton() {
        LOGGER.info("clickResetPasswordButton started");
        driver.waitForMilliseconds();
        commonActions.clickFormSubmitButtonByText(RESET_PASSWORD);
        driver.waitForPageToLoad();
        LOGGER.info("clickResetPasswordButton completed");
    }

    /**
     * Click Sign-up Now link
     */
    public void clickSignUpNowLink() {
        LOGGER.info("clickSignUpNowLink started");
        driver.waitForElementClickable(myAccountSignUpNow);
        myAccountSignUpNow.click();
        driver.waitForPageToLoad();
        LOGGER.info("clickSignUpNowLink completed");
    }

    /**
     * Set Keep me signed in checkbox to true
     */
    public void setKeepMeSignedInCheckbox() {
        LOGGER.info("setKeepMeSignedInCheckbox started");
        driver.waitForElementClickable(keepSignedInCheckbox);
        if (webDriver.findElement(CommonActions.checkboxInputBy).getAttribute(Constants.VALUE)
                .equalsIgnoreCase(CommonActions.FALSE))
            checkboxInput.click();
        if (!Config.getSiteRegion().equalsIgnoreCase(Constants.DTD)) {
            driver.scenarioData.genericData.put(Constants.CUSTOMER_TYPE, ConstantsDtc.REGISTERED_CUSTOMER);
            LOGGER.info("Customer Type set to '" + ConstantsDtc.REGISTERED_CUSTOMER + "' in setKeepMeSignedInCheckbox");
        }
        driver.scenarioData.createAccount(true);
        LOGGER.info("setKeepMeSignedInCheckbox completed");
    }

    /**
     * UnSet / Deselect Keep me signed in checkbox
     */
    public void unSetKeepMeSignedInCheckbox() {
        LOGGER.info("unSetKeepMeSignedInCheckbox started");
        driver.waitForElementClickable(keepSignedInCheckbox);
        if (keepSignedInCheckbox.isSelected())
            keepSignedInCheckbox.click();
        LOGGER.info("unSetKeepMeSignedInCheckbox completed");
    }

    /**
     * Verify My Account modal input field value with provided text value
     *
     * @param InputFieldName Element name that contains the text to verify
     * @param text           String to validate against element text
     */
    public void verifyMyAccountInputFieldValue(String InputFieldName, String text) {
        LOGGER.info("verifyMyAccountInputFieldValue started");
        String actualText = null;
        driver.waitForElementClickable(myAccountEmailAddress);

        if (InputFieldName.equalsIgnoreCase(ConstantsDtc.EMAIL)) {
            actualText = myAccountEmailAddress.getAttribute(Constants.VALUE);
        } else if (InputFieldName.equalsIgnoreCase(ConstantsDtc.PASSWORD)) {
            actualText = myAccountPassword.getAttribute(Constants.VALUE);
        } else
            actualText = myAccountResetPasswordEmail.getAttribute(Constants.VALUE);
        Assert.assertTrue("FAIL: " + InputFieldName + " field,  Actual value :" + actualText
                + " did NOT match with expected text: \"" + text + "\"!", actualText.contains(text));

        LOGGER.info("verifyMyAccountInputFieldValue completed");
    }

    /**
     * Verify My Account modal input field value with provided text value
     *
     * @param buttonName Name of the button to verify
     * @param status     Desired status of the button
     */
    public void verifyMyAccountButtonState(String buttonName, String status) {
        LOGGER.info("verifyMyAccountButtonState started");
        driver.waitForPageToLoad();
        driver.setImplicitWait(Constants.ZERO, TimeUnit.SECONDS);
        String buttonStatus = Constants.ENABLED;
        if (!driver.getElementWithText(CommonActions.formSubmitButtonBy, buttonName).isEnabled())
            buttonStatus = Constants.DISABLED;
        Assert.assertTrue("FAIL : " + buttonName + " button expected status (" + status +
                ") didn't match, Actual status (" + buttonStatus + ")", status.equalsIgnoreCase(buttonStatus));
        driver.resetImplicitWaitToDefault();
        LOGGER.info("verifyMyAccountButtonState completed. " + buttonName + " is " + buttonStatus);
    }

    /**
     * Verify keep me signed in option is displayed on MyAccount modal
     */
    public void assertKeepMeSignedInIsDisplayed() {
        LOGGER.info("assertKeepMeSignedInIsDisplayed started");
        driver.waitForPageToLoad();
        if (!driver.isElementDisplayed(keepSignedInCheckbox)) {
            Assert.fail("FAIL : Keep me signed in option did NOT displayed on My Account modal");
        }
        LOGGER.info("assertKeepMeSignedInIsDisplayed completed");
    }

    /**
     * Verify Signed in user first name is displayed on home-page header
     *
     * @param firstName type of first name (Updated or default)
     */
    public void assertSignedInUserFirstNameIsDisplayed(String firstName) {
        LOGGER.info("assertSignedInUserFirstNameIsDisplayed started");
        if (firstName.equalsIgnoreCase(Constants.FIRST)) {
            firstName = customer.getCustomer(Customer.CustomerType.MY_ACCOUNT_USER_A.toString()).firstName;
        }
        String actualText = null;
        if (Config.isMobile())
            actualText = CommonActions.getMyAccountLoggedInLabelMobile.getText();
        else
            actualText = CommonActions.myAccountLoggedInLabel.getText();
        Assert.assertTrue("FAIL: " + firstName + " name was NOT present,  Actual value :" + actualText
                + " did NOT match with expected name: \"" + firstName + "\"!", actualText.contains(firstName));
        LOGGER.info("assertSignedInUserFirstNameIsDisplayed completed");
    }

    /**
     * Verify the error message for invalid email addresses
     */
    public void assertEmailFieldErrorMessageValidation() {
        LOGGER.info("assertEmailFieldErrorMessageValidation started");
        driver.waitForElementClickable(myAccountEmailAddress);
        myAccountEmailAddress.click();
        for (String invalidEmailAddressMyAccount : invalidEmailAddress) {
            myAccountEmailAddress.sendKeys(invalidEmailAddressMyAccount);
            myAccountEmailAddress.sendKeys(Keys.ENTER);
            driver.waitForMilliseconds();
            Assert.assertTrue("FAIL : Email field error message validations failed",
                    CommonActions.formGroupErrorMessageElement.getText().contains(EMAIL_VALIDATION_MESSAGE));

            while (!myAccountEmailAddress.getAttribute(Constants.VALUE).equals("")) {
                myAccountEmailAddress.sendKeys(Keys.BACK_SPACE);
            }
            driver.waitForMilliseconds();
        }
        LOGGER.info("assertEmailFieldErrorMessageValidation completed");
    }

    /**
     * Generate random email and set email to create new account
     */
    public void generateAndSetFakeRandomEmailForUser(String user) {
        LOGGER.info("generateAndSetFakeRandomEmailForUser started");
        Random rand = new Random();

        user += rand.nextInt(9999) + rand.nextInt(99) + FAKE_EMAIL;
        CommonActions.generatedRandomEmail = user;
        commonActions.enterTextIntoField(myAccountEmailAddress, user);
        LOGGER.info("Generated fake email address : " + CommonActions.generatedRandomEmail);

        LOGGER.info("generateAndSetFakeRandomEmailForUser completed");
    }

    /**
     * Click Continue Shopping button
     */
    public void clickContinueShoppingButton() {
        LOGGER.info("clickContinueShoppingButton started");
        commonActions.clickFormSubmitButtonByText(ConstantsDtc.CONTINUE_SHOPPING);
        driver.waitForPageToLoad();
        LOGGER.info("clickContinueShoppingButton completed");
    }

    /**
     * Verify My Account confirmation page has provided text message present
     *
     * @param element Web Element that contains the text to verify
     * @param text    String to validate against element text
     */
    public void assertAccountModalHasMessagePresent(By element, String text) {
        LOGGER.info("assertAccountModalHasMessagePresent started");
        driver.waitForElementVisible(CommonActions.dtModalTitleBy);
        driver.waitForMilliseconds(Constants.FIVE_HUNDRED);
        Assert.assertTrue("FAIL: Message :" + text + " did NOT present on My Account modal",
                driver.checkIfElementContainsText(element, text));
        LOGGER.info("assertAccountModalHasMessagePresent completed");
    }

    /**
     * Verify email authentication link message present on Account confirmation modal
     */
    public void assertAuthenticationEmailLinkMessagePresent() {
        LOGGER.info("assertAuthenticationEmailLinkMessagePresent started");
        driver.waitForElementVisible(CommonActions.successMessageBy);

        String messageElement = webDriver.findElement(CommonActions.successMessageBy).getText();
        Assert.assertTrue("FAIL: Message :" + ConstantsDtc.EMAIL_AUTHENTICATION_LINK_MESSAGE + " did NOT present on My Account modal",
                messageElement.toLowerCase().contains(ConstantsDtc.EMAIL_AUTHENTICATION_LINK_MESSAGE.toLowerCase()));

        LOGGER.info("assertAuthenticationEmailLinkMessagePresent completed");
    }

    /**
     * Verify the requirements for passed progress steps validation for password field
     */
    public void assertPasswordProgressStepsValidation() {
        LOGGER.info("assertPasswordProgressStepsValidation started");
        boolean validationPassed = true;
        driver.waitForElementClickable(myAccountPassword);
        myAccountPassword.click();
        for (String password : invalidPassword) {
            while (!myAccountPassword.getAttribute(Constants.VALUE).isEmpty()) {
                myAccountPassword.sendKeys(Keys.BACK_SPACE);
            }
            myAccountPassword.sendKeys(password);
            myAccountPassword.sendKeys(Keys.ENTER);
            driver.waitForMilliseconds();
            String errorMessage = CommonActions.formGroupErrorMessageElement.getText();
            if (!(errorMessage.equalsIgnoreCase(PASSWORD_PROGRESS_STEPS_LABELS))) {
                validationPassed = false;
                LOGGER.info("FAIL: Password error message validation failed for case :" + password);
            }
        }
        if (!validationPassed) {
            Assert.fail("FAIL: Password field error message validations failed");
        }
    }

    /**
     * Verify reset your password instruction displayed
     */
    public void assertResetYourPasswordInstructionDisplayed() {
        LOGGER.info("assertResetYourPasswordInstructionDisplayed started");
        driver.waitForElementVisible(myAccountResetPasswordEmail);
        Assert.assertTrue("FAIL: Reset your password instruction :" + FORGOT_PASSWORD_TEXT
                        + " did NOT present on My Account forgot password modal",
                forgotPasswordLabelElement.getText().equalsIgnoreCase(FORGOT_PASSWORD_TEXT));
        LOGGER.info("assertResetYourPasswordInstructionDisplayed completed");
    }

    /**
     * Verify the error messages for invalid email addresses for reset your password email field
     */
    public void assertResetYourPasswordEmailFieldErrorMessageValidation() {
        LOGGER.info("assertResetYourPasswordEmailFieldErrorMessageValidation started");
        boolean validationPassed = true;
        for (String invalidEmailAddressMyAccount : invalidEmailAddress) {
            commonActions.enterTextIntoField(myAccountResetPasswordEmail, invalidEmailAddressMyAccount, true, Constants.ONE);
            driver.waitForMilliseconds();
            myAccountResetPasswordEmail.click();
            if (!CommonActions.formGroupErrorMessageElement.getText().contains(EMAIL_VALIDATION_MESSAGE)) {
                validationPassed = false;
                LOGGER.info("FAIL : Email error message validation failed for case :" + invalidEmailAddressMyAccount);
            }
            while (!myAccountResetPasswordEmail.getAttribute(Constants.VALUE).equals("")) {
                myAccountResetPasswordEmail.sendKeys(Keys.BACK_SPACE);
            }
        }
        if (!validationPassed) {
            Assert.fail("FAIL : Email field error message validations failed");
        }
        LOGGER.info("assertResetYourPasswordEmailFieldErrorMessageValidation completed");
    }

    /**
     * Verify password instruction displayed after reset password
     */
    public void assertPasswordInstructionDisplayedAfterResetPassword() {
        LOGGER.info("assertPasswordInstructionDisplayedAfterResetPassword started");
        driver.waitForElementVisible(myAccountEmailAddress);

        Assert.assertTrue("FAIL: Reset your password instruction :" + RESET_PASSWORD_INSTRUCTIONS
                + " did NOT present on My Account forgot password modal", driver.checkIfElementContainsText(resetPasswordInstructionElement, RESET_PASSWORD_INSTRUCTIONS));
        LOGGER.info("assertPasswordInstructionDisplayedAfterResetPassword completed");
    }

    /**
     * Click My Account popup
     */
    public void clickMyAccountPopup() {
        LOGGER.info("clickMyAccountPopup started");
        driver.waitForMilliseconds();
        WebElement element = driver.getParentElement(
                driver.getElementWithText(CommonActions.headerSecondBy, ConstantsDtc.EDIT_SIGNIN_EMAIL_LABEL));
        element.findElement(CommonActions.dtModalCloseBy).click();
        driver.waitForPageToLoad();
        LOGGER.info("clickMyAccountPopup completed");
    }

    /**
     * Verify updated email authentication link message on Account confirmation modal
     */
    public void assertAuthenticationUpdatedEmailLinkMessagePresent() {
        LOGGER.info("assertAuthenticationEmailLinkMessagePresent started");
        driver.waitForElementVisible(CommonActions.successMessageBy);

        WebElement messageElement = driver.getElementWithText(CommonActions.successMessageBy, ConstantsDtc.UPDATED_EMAIL_AUTHENTICATION_LINK_MESSAGE);
        Assert.assertTrue("FAIL: Message :" + ConstantsDtc.UPDATED_EMAIL_AUTHENTICATION_LINK_MESSAGE + " did NOT present on My Account modal",
                driver.checkIfElementContainsText(messageElement, ConstantsDtc.UPDATED_EMAIL_AUTHENTICATION_LINK_MESSAGE));

        LOGGER.info("assertAuthenticationEmailLinkMessagePresent completed");
    }

    /**
     * Verifies Profile Header with the specified text
     *
     * @param headerText The text on the header in the Profile tab
     */
    public void assertProfileHeader(String headerText) {
        LOGGER.info("assertProfileHeader started");
        driver.waitForElementVisible(myProfileHeadingBy);

        List<WebElement> headers = webDriver.findElements(myProfileHeadingBy);
        WebElement leftPanelHeader = headers.get(0);
        WebElement rightPanelHeader = headers.get(1);
        String leftHeader = leftPanelHeader.getText();
        String rightHeader = rightPanelHeader.getText();

        if (headerText.equalsIgnoreCase(SIGN_IN)) {
            Assert.assertTrue("FAIL: Expected Left Header :" + headerText + " is NOT present on My Account modal ."
                    + " Actual Header displayed " + leftHeader, leftHeader.equalsIgnoreCase(headerText));
        } else if (headerText.equalsIgnoreCase(CONTACT_INFO)) {
            Assert.assertTrue("FAIL: Expected Right Header :" + headerText + " is NOT present on My Account modal ."
                    + " Actual Header displayed " + rightHeader, rightHeader.equalsIgnoreCase(headerText));
        }
        LOGGER.info("assertProfileHeader completed");
    }

    /**
     * Verifies the add contact information link for Primary Contact
     */
    public void assertAddContactInformationLink() {
        LOGGER.info("assertAddContactInformationLink started");
        driver.waitForPageToLoad();
        Assert.assertTrue("FAIL: The add contact information link did not display for CONTACT section ",
                driver.isElementDisplayed(myAccountAddContactInformationBy));
        LOGGER.info("assertAddContactInformationLink completed");
    }

    /**
     * Adds the secondary contact info in the My Profile tab of My Account
     *
     * @param customerContactInfo type of customer email address,password,primary or alternate
     */
    public void clickEditLink(String customerContactInfo) {
        LOGGER.info("clickEditLink started");
        driver.jsScrollToElement(webDriver.findElement(profileEditPrimaryLinkBy));
        WebElement ContactEditLink = null;
        List<WebElement> EditEmailAndPasswordField = webDriver.findElements(emailAndPasswordEditLinkBy);
        if (customerContactInfo.toLowerCase().contains(PRIMARY.toLowerCase()))
            ContactEditLink = webDriver.findElement(profileEditPrimaryLinkBy);
        else if (customerContactInfo.toLowerCase().contains(Constants.ALTERNATE.toLowerCase())) {
            if (Config.isMobile())
                ContactEditLink = (webDriver.findElements(profileEditAlternateLinkBy).get(2));
            else
                ContactEditLink = driver.getElementWithText(webDriver.findElement(profileEditAlternateLinkBy),
                        Constants.EDIT.toLowerCase());
        } else if (customerContactInfo.toLowerCase().contains(ConstantsDtc.EMAIL_ADDRESS_LABEL.toLowerCase()))
            ContactEditLink = EditEmailAndPasswordField.get(0);
        else
            ContactEditLink = EditEmailAndPasswordField.get(1);
        ContactEditLink.click();
        LOGGER.info("clickEditLink completed");
    }

    /**
     * Verifies Profile Header with the specified text is displayed
     *
     * @param category The text on the header in the Profile tab
     */
    public void assertEditDeleteLink(String category) {
        LOGGER.info("assertEditDeleteLink started for " + category);
        driver.waitForPageToLoad();
        driver.waitForElementVisible(myProfileContactActionBy);

        List<WebElement> contactActions = webDriver.findElements(myProfileContactActionBy);
        if (category.equalsIgnoreCase(PRIMARY)) {
            Assert.assertTrue("FAIL: The primary contact section did not display the expected actions ",
                    contactActions.get(0).getText().equalsIgnoreCase(Constants.EDIT));
        } else if (category.equalsIgnoreCase(Constants.ALTERNATE)) {
            Assert.assertTrue("FAIL: The secondary contact section did not display the expected actions ",
                    contactActions.get(1).getText().equalsIgnoreCase(Constants.EDIT)
                            && contactActions.get(2).getText().equalsIgnoreCase(Constants.DELETE));

        }
        LOGGER.info("assertEditDeleteLink completed for " + category);
    }

    /**
     * Verifies the Contact Selection drop down on the Checkout page
     *
     * @param contactType Primary or Secondary
     */
    public void assertContactSelectionDropDown(String contactType) {
        LOGGER.info("assertContactSelectionDropDown started");
        driver.waitForPageToLoad();
        Assert.assertTrue("FAIL: The contact selection drop down did not display on the Checkout page ",
                accountInformationDropDown.getText().toLowerCase().equals(contactType.toLowerCase()));
        LOGGER.info("assertContactSelectionDropDown completed");
    }

    /**
     * Select the contact information option based on the type passed in
     *
     * @param contactType type of contact information according to scenario(Primary Information, Secondary Information)
     */
    public void selectContactType(String contactType) {
        LOGGER.info("selectContactType started");
        driver.jsScrollToElementClick(accountInformationDropDown);
        driver.waitOneSecond();
        WebElement contactInformationEle = driver.getElementWithText(contactInformationBy, contactType);
        driver.jsScrollToElementClick(contactInformationEle, false);
        LOGGER.info("selectContactType completed");
    }

    /**
     * Verify create account label is displayed
     *
     * @param text text of the label
     */
    public void assertCreateAccountLabelDisplayed(String text) {
        LOGGER.info("assertCreateAccountLabelDisplayed started for " + text);
        WebElement createAccountForm = driver.getElementWithText(checkoutFormCreateAccountBy, text);
        Assert.assertTrue("FAIL: Create Account label " + text + " did not display",
                driver.isElementDisplayed(createAccountForm));
        LOGGER.info("assertCreateAccountLabelDisplayed completed for " + text);
    }

    /**
     * Clicks Create an account check box
     */
    public void selectCreateAnAccountCheckBox() {
        LOGGER.info("selectCreateAnAccountCheckBox started");
        driver.jsScrollToElementClick(checkboxInput);
        LOGGER.info("selectCreateAnAccountCheckBox completed");
    }

    public void clickOnSignIn() {
        LOGGER.info("clickOnSignInIcon started");
        if (Config.isMobile()) {
            WebElement signInIconMobile = webDriver.findElement(MyAccountPage.signInIconBy);
            driver.jsScrollToElementClick(signInIconMobile);
        }
        driver.clickElementWithLinkText(ConstantsDtc.JOIN_SIGN_IN);
        LOGGER.info("clickOnSignInIcon completed");
    }

    /**
     * Clicks the Tab on My Account page
     *
     * @param inputSelection Text value of the tab to select
     */
    public void clickTabOnMyAccountPage(String inputSelection) {
        LOGGER.info("clickTabOnMyAccountPage for " + inputSelection + "started");
        driver.waitForMilliseconds();
        if (Config.isMobile()) {
            WebElement myAccountTabMobile = driver.getParentElement(driver.getElementWithText(myAccountTabBy,
                    inputSelection));
            myAccountTabMobile.click();
        }
        driver.getParentElement(driver.getElementWithText(myAccountTabBy, inputSelection)).click();
        LOGGER.info("clickTabOnMyAccountPage for " + inputSelection + "completed");
    }

    /**
     * Verifies the expected tab (Profile, My Vehicles, Orders, Appointments) is active
     *
     * @param selectedTab Text of the tab expected to be active
     */
    public void verifyActiveTab(String selectedTab) {
        LOGGER.info("verifyActiveTab started w/ selectedTab: '" + selectedTab + "'");
        WebElement activeSubheader = driver.getElementWithText(myAccountSelectedTabBy, selectedTab);

        Assert.assertNotNull("FAIL: Could not find the active tab element on the vehicle fitment grid!",
                activeSubheader);
        Assert.assertTrue("FAIL: '" + selectedTab + "' was NOT the active subheader on the vehicle fitment grid! " +
                "\n\tActual: '" + activeSubheader.getText() + "'", activeSubheader.getText()
                .equalsIgnoreCase(selectedTab));
        LOGGER.info("verifyActiveTab completed w/ selectedTab: '" + selectedTab + "'");
    }

    /**
     * Verifies the primary and secondary Address in my profile tab
     */
    public void assertPrimaryAndAlternateAddress() {
        LOGGER.info("assertPrimaryAndAlternateAddress started");
        List<WebElement> contactInfo = webDriver.findElements(primaryAndSecondaryContactInfoBy);
        for (WebElement contactAddress : contactInfo) {
            Assert.assertTrue("The contact info did not match", ((contactAddress.getText().toLowerCase().contains
                    (customer.getCustomer(Customer.CustomerType.MY_ACCOUNT_USER_A.toString()).address1.toLowerCase())) ||
                    (contactAddress.getText().toLowerCase().contains
                            (customer.getCustomer(Customer.CustomerType.MY_ACCOUNT_USER_A.toString()).address2.toLowerCase()))));
        }
        LOGGER.info("assertPrimaryAndAlternateAddress completed");
    }

    /**
     * This method will return the webelement matching the input String.
     *
     * @param elementName - Name of the By element
     * @return - Webelement
     */
    public WebElement returnWebElement(String elementName) {
        LOGGER.info("returnWebElement started");
        List<WebElement> zipCode;
        switch (elementName) {
            case CommonActions.FIRST_NAME:
                return CommonActions.firstName;
            case CommonActions.LAST_NAME:
                return CommonActions.lastName;
            case CommonActions.EMAIL:
                return myAccountEmailAddress;
            case ConstantsDtc.PASSWORD:
                return myAccountPassword;
            case ConstantsDtc.PHONE_TYPE:
                return phoneType;
            case Constants.PHONE_NUMBER:
                return CommonActions.phone;
            case CommonActions.ADDRESS_LINE_1:
                return CommonActions.address1;
            case CommonActions.ADDRESS_LINE_2:
                return CommonActions.address2Optional;
            case CommonActions.COUNTRY:
                return country;
            case Constants.ZIP_CODE:
                zipCode = webDriver.findElements(CommonActions.zipCodeBy);
                if ((Config.isMobile() && driver.isElementDisplayed(CommonActions.secondaryContactMobileBy)))
                    return zipCode.get(1);
                else
                    return CommonActions.orderSummaryZipCode;
            case Constants.CITY:
                return CommonActions.city;
            case ConstantsDtc.STATE:
                return driver.getParentElement(state);
            case ConstantsDtc.CREATE_ACCOUNT:
                return createAccountModal;
            case RESET_PASSWORD:
                return forgotPasswordLabelElement;
            case CURRENT_PASSWORD:
                return currentPasswordField;
            case NEW_PASSWORD:
                return newPasswordField;
            case RE_TYPE_NEW_PASSWORD:
                return reTypeNewPassword;
            default:
                Assert.fail("FAIL: Could not find By objects that matched string passed from step");
                return null;
        }
    }

    /**
     * Populates all the field present for the specified customer on create account modal
     *
     * @param customerType primary or secondary
     */
    public void populateDataForCustomer(String customerType) {
        LOGGER.info("populateDataForCustomer started");
        Customer customerInfo = customer.getCustomer(customerType);
        commonActions.populateAddressFormFieldsForCustomer(customerInfo, true);
        LOGGER.info("populateDataForCustomer completed");
    }

    /**
     * Verifies the specified fields are displayed
     *
     * @param elementName Name of the element to be displayed
     */
    public void assertFieldDisplayed(String elementName) {
        LOGGER.info("assertFieldDisplayed started");
        driver.waitForPageToLoad(Constants.FIVE_HUNDRED);
        List<String> inputValues = Arrays.asList(elementName.split("\\s*,\\s*"));
        for (int i = 0; i < inputValues.size(); i++)
            commonActions.assertWebElementDisplayed(returnWebElement(inputValues.get(i)), elementName);
        LOGGER.info("assertFieldDisplayed completed");
    }

    /**
     * Clears the specified field and verifies the error message
     *
     * @param field Name of the field that has to cleared
     */
    public void assertFieldEmptyErrorMessage(String field) {
        LOGGER.info("assertFieldEmptyErrorMessage started for " + field);
        WebElement fieldName = returnWebElement(field);
        commonActions.clearEditField(fieldName);
        List<WebElement> errorMessage = webDriver.findElements(myAccountErrorValidationBy);
        if (errorMessage.size() < 1)
            Assert.fail("No error message was displayed");
        for (WebElement errorMessageField : errorMessage) {
            String message = errorMessageField.getText().toLowerCase().replace("-", "");
            Assert.assertTrue("FAIL: The error message displayed is incorrect, the expected message is '" + field +
                            " cannot be empty' but actual message is '" + message + "'"
                    , message.equalsIgnoreCase(field + " cannot be empty"));
        }
        LOGGER.info("assertFieldEmptyErrorMessage completed for " + field);
    }

    /**
     * Verifies expected error message is displayed
     *
     * @param errorMessage - Expected error message
     */
    public void assertCreateAccountFieldErrorMessage(String errorMessage) {
        LOGGER.info("assertCreateAccountFieldErrorMessage started for '" + errorMessage + "'");
        boolean messageDisplayed = false;
        driver.waitSeconds(Constants.TWO);
        List<WebElement> errorMessages = webDriver.findElements(myAccountErrorValidationBy);
        for (WebElement errorMessageField : errorMessages) {
            if (errorMessage.equals(errorMessageField.getText())) {
                messageDisplayed = true;
                break;
            }
        }
        Assert.assertTrue("FAIL: The expected error message, " + errorMessage + ",was not displayed", messageDisplayed);
        LOGGER.info("assertCreateAccountFieldErrorMessage completed for '" + errorMessage + "'");
    }

    /**
     * Verifies that the invalid credentials error message is displayed or not displayed
     *
     * @param isDisplayed displayed or not displayed
     */
    public void verifyInvalidCredentials(String isDisplayed) {
        LOGGER.info("verifyInvalidCredentials started");
        if (isDisplayed.equalsIgnoreCase(Constants.DISPLAYED))
            assertAccountModalHasMessagePresent(MyAccountPage.myAccountErrorValidationBy,
                    ConstantsDtc.MY_ACCOUNT_INVALID_CREDS_MESSAGE);
        else
            Assert.assertFalse("FAIL: Invalid credentials error validation message is displayed",
                    driver.isElementDisplayed(MyAccountPage.myAccountErrorValidationBy));
        LOGGER.info("verifyInvalidCredentials completed");
    }

    /**
     * Deletes all the saved vehicles
     *
     * @param numberOfVehicles number of vehicles to be deleted
     */
    public void deleteAllSavedVehicles(String numberOfVehicles) {
        LOGGER.info("deleteAllSavedVehicles started");
        List<WebElement> deleteButton;
        int k;
        int j;
        webDriver.manage().timeouts().implicitlyWait(Constants.ZERO, TimeUnit.SECONDS);
        if (Config.isMobile())
            deleteButton = webDriver.findElements(deleteSavedVehicleListMobileBy);
        else
            deleteButton = webDriver.findElements(deleteSavedVehicleListBy);

        if (deleteButton.size() > 0) {
            if (numberOfVehicles.equalsIgnoreCase(ConstantsDtc.ALL)) {
                if (Config.isMobile()) {
                    k = 0;
                    j = 1;
                } else {
                    k = 1;
                    j = 2;
                }
                for (int i = k; i < deleteButton.size(); i += j) {
                    deleteButton.get(i).click();
                    driver.waitForMilliseconds();
                    commonActions.clickButtonByText(ConstantsDtc.YES_DELETE_THIS_VEHICLE);
                    driver.waitForMilliseconds();
                }
            } else {
                int numberOfVehicleToBeDeleted = Integer.parseInt(numberOfVehicles);
                for (int i = 1; i < numberOfVehicleToBeDeleted; i += 2) {
                    deleteButton.get(i).click();
                    commonActions.clickButtonByText(ConstantsDtc.YES_DELETE_THIS_VEHICLE);
                }
            }
        } else {
            LOGGER.info("No Vehicles in the saved section");
        }
        driver.resetImplicitWaitToDefault();
        LOGGER.info("deleteAllSavedVehicles completed");
    }

    /**
     * Save all the recent vehicles
     */
    public void saveAllRecentSearchVehicles() {
        LOGGER.info("saveAllRecentSearchVehicles started");
        WebElement element;
        int saveVehiclesLinksSize = driver.getElementsWithText(CommonActions.dtLinkBy, ConstantsDtc.SAVE).size();
        for (int i = 0; i < saveVehiclesLinksSize; i++) {
            if (Config.isMobile())
                element = webDriver.findElement(saveVehiclesLinkMobileBy);
            else
                element = webDriver.findElement(saveVehiclesLinkBy);

            element.click();
            driver.waitForMilliseconds();
        }
        LOGGER.info("saveAllRecentSearchVehicles completed");
    }

    /**
     * Verifies specified order number is present on a specified tab of the My Account page
     *
     * @param tabName     - The name of the tab on which to verify the order number
     * @param orderNumber - The order number to verify
     */
    public void assertOrderNumberOnMyAccountPage(String tabName, String orderNumber) {
        LOGGER.info("assertOrderNumberOnMyAccountPage started for " + tabName + " tab");
        WebElement tabTextBody;
        String noListItemsMessage;
        By orderDetailsBy;
        boolean found = false;
        ArrayList<String> orderNumberValues = new ArrayList<String>();
        if (tabName.equalsIgnoreCase(ConstantsDtc.ORDERS)) {
            tabTextBody = ordersTabTextBody;
            noListItemsMessage = NO_ONLINE_ORDERS_MESSAGE;
            orderDetailsBy = orderDetailsInOrderHistoryTabListBy;
        } else {
            tabTextBody = appointmentTabTextBody;
            noListItemsMessage = CURRENTLY_HAVE_NO_APPOINTMENTS_MESSAGE;
            orderDetailsBy = orderDetailsInAppointmentHistoryTabListBy;
        }
        driver.waitForPageToLoad();
        int refreshCounter = Constants.ZERO;
        int maxRefresh = Constants.ONE_HUNDRED;
        driver.waitForElementVisible(tabTextBody, Constants.TEN);
        // Waiting for initial loading of the tab
        while (tabTextBody.getText().isEmpty()) {
            driver.waitOneSecond();
        }
        Assert.assertFalse("FAIL: Failure to fetch order history message displayed",
                tabTextBody.getText().equalsIgnoreCase(FETCHING_ORDERS_FAILED));
        while ((tabTextBody.getText().equalsIgnoreCase(noListItemsMessage)) &&
                refreshCounter < maxRefresh) {
            webDriver.navigate().refresh();
            driver.waitOneSecond();
            // Waiting for the refresh to complete
            while (tabTextBody.getText().isEmpty()) {
                driver.waitOneSecond();
            }
            refreshCounter++;
        }
        Assert.assertFalse("FAIL: No orders were found on " + tabName + " tab.",
                tabTextBody.getText().equalsIgnoreCase(noListItemsMessage));

        refreshCounter = Constants.ZERO;
        maxRefresh = Constants.THIRTY;
        do {
            webDriver.navigate().refresh();
            driver.waitForPageToLoad(Constants.TWO_THOUSAND);
            // Waiting for the refresh to complete
            while (tabTextBody.getText().isEmpty()) {
                driver.waitOneSecond();
            }
            List<WebElement> ordersList = webDriver.findElements(orderDetailsBy);
            refreshCounter++;

            for (WebElement orderNo : ordersList) {
                if (tabName.equalsIgnoreCase(ConstantsDtc.ORDERS)) {
                    orderNumberValues.add(orderNo.getText().split(" ")[2]);
                } else {
                    orderNumberValues.add(orderNo.getText());
                }
            }
        } while (!orderNumberValues.contains(orderNumber) &&
                refreshCounter < maxRefresh);

        if (orderNumberValues.contains(orderNumber) && (refreshCounter < maxRefresh)) {
            found = true;
        }
        Assert.assertTrue("FAIL: The expected order " + orderNumber + " was not found on " + tabName + " tab.",
                found);
        LOGGER.info("assertOrderNumberOnMyAccountPage completed for " + tabName + " tab");
    }

    /**
     * Method click on view details link based on order now number in order history summary page.
     *
     * @param orderNumber Order number
     */
    public void viewDetailsClickByRow(String orderNumber) {
        LOGGER.info("viewDetailsClickByRow started");
        List<WebElement> orderRows = webDriver.findElements(viewDetailsBy);
        int arrayLength = webDriver.findElements(CommonActions.orderListItemBy).size();
        for (int i = 0; i < arrayLength; i++) {
            List<WebElement> orderList = webDriver.findElements(CommonActions.orderListItemBy);
            if (orderList.get(i).getText().contains(orderNumber)) {
                driver.jsScrollToElementClick(orderRows.get(i), false);
                break;
            }
        }
        driver.waitOneSecond();
        LOGGER.info("viewDetailsClickByRow completed");
    }

    /**
     * Method updates My Account first name based on new or default(First name from customer data)
     *
     * @param nameStatusType Name specified to change
     */
    public void updateFirstName(String nameStatusType) {
        LOGGER.info("updateFirstName started");
        driver.jsScrollToElementClick(CommonActions.firstName);
        while (!CommonActions.firstName.getAttribute(Constants.VALUE).equals("")) {
            CommonActions.firstName.sendKeys(Keys.BACK_SPACE);
        }
        commonActions.enterTextIntoField(CommonActions.firstName, nameStatusType);
        LOGGER.info("updateFirstName completed");
    }

    /*
    Verifies the Order History Tab has sufficient number of orders to check for pagination
     */
    public void assertTotalNumberOfOrders() {
        LOGGER.info("assertTotalNumberOfOrders started");
        int minimumNumberOfOrdersRequired = 21;
        driver.waitForElementVisible(CommonActions.orderListItemBy, Constants.THREE);
        int totalNumberOfOrders = 0;
        List<WebElement> numberOfOrders = webDriver.findElements(paginationNumberBy);
        for (int i = 0; i < numberOfOrders.size() - 2; i++) {
            numberOfOrders.get(i).click();
            totalNumberOfOrders += webDriver.findElements(CommonActions.orderListItemBy).size();
        }
        Assert.assertFalse("FAIL: Minimum number of orders are not present on the order history tab",
                totalNumberOfOrders < minimumNumberOfOrdersRequired);
        LOGGER.info("assertTotalNumberOfOrders completed");
    }

    /**
     * Method Navigates to Url to authenticate the emailId
     */
    public void navigateMyAccountEmailUrl() {
        LOGGER.info("navigateMyAccountEmailUrl started");
        String emailAuthUrl;
        try {
            String encodedToken = URLEncoder.encode(driver.scenarioData.genericData.get(TOKEN), StandardCharsets.UTF_8.toString());
            if (Config.getBaseUrl().endsWith("/"))
                emailAuthUrl = Config.getBaseUrl() + "register/validateToken?token=" + encodedToken;
            else
                emailAuthUrl = Config.getBaseUrl() + "/register/validateToken?token=" + encodedToken;
            webDriver.navigate().to(emailAuthUrl);
            Assert.assertFalse("FAIL: The authentication URL contains email ID.", webDriver.getCurrentUrl().contains("@"));
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
        LOGGER.info("navigateMyAccountEmailUrl completed");
    }

    /**
     * Method Navigates to Url to reset the password for My Account customer
     */
    public void navigateMyAccountPasswordResetUrl() {
        LOGGER.info("navigateMyAacoountPasswordResetUrl started");
        try {
            String encodedToken = URLEncoder.encode(driver.scenarioData.genericData.get(TOKEN), StandardCharsets.UTF_8.toString());
            String passwordResetUrl = Config.getBaseUrl() + "/login/pw/change?token=" + encodedToken;
            webDriver.navigate().to(passwordResetUrl);
            Assert.assertFalse("FAIL: The authentication URL contains email ID.", webDriver.getCurrentUrl().contains("@"));
        } catch (UnsupportedEncodingException ex) {
            throw new RuntimeException(ex.getCause());
        }
        LOGGER.info("navigateMyAacoountPasswordResetUrl completed");
    }

    /**
     * Populate Primary and Secondary Contact fields on My account Profile page
     *
     * @param customerType Type of Customer
     * @param Customer     Primary or Alternate Contact
     */
    public void populatePrimaryAndAlternateContact(String customerType, String Customer) {
        LOGGER.info("enterPrimaryAndAlternateContact started");
        Customer contactInfo = customer.getCustomer(customerType);
        ArrayList<WebElement> addressFields = null;
        ArrayList<String> customerInfo = null;
        if (Customer.equalsIgnoreCase(MyAccountPage.PRIMARY)) {
            addressFields = new ArrayList<>(Arrays.asList(CommonActions.firstName, CommonActions.lastName,
                    CommonActions.address1, CommonActions.address2, CommonActions.zip, CommonActions.city, CommonActions.phone));
            customerInfo = new ArrayList<>(Arrays.asList(contactInfo.firstName, contactInfo.lastName,
                    contactInfo.address1, contactInfo.address2, contactInfo.zip, contactInfo.city, contactInfo.phone.replaceAll("-", "")));
        } else if (Customer.equalsIgnoreCase(Constants.ALTERNATE)) {
            if (Config.isMobile()) {
                addressFields = new ArrayList<>(Arrays.asList((webDriver.findElements(CommonActions.addressLine1By).get(1)),
                        (webDriver.findElements(CommonActions.addressLine2By).get(1)),
                        (webDriver.findElements(CommonActions.zipCodeBy).get(1)),
                        (webDriver.findElements(CommonActions.cityBy).get(1)),
                        (webDriver.findElements(CommonActions.phoneNumberBy).get(1))));
            } else {
                addressFields = new ArrayList<>(Arrays.asList(CommonActions.address1, CommonActions.address2,
                        CommonActions.zip, CommonActions.city, CommonActions.phone));
            }
            customerInfo = new ArrayList<>(Arrays.asList(contactInfo.address1, contactInfo.address2, contactInfo.zip,
                    contactInfo.city, contactInfo.phone.replaceAll("-", "")));
        }
        for (int i = 0; i < addressFields.size(); i++) {
            if (!customerInfo.get(i).equalsIgnoreCase("")) {
                driver.jsScrollToElementClick(addressFields.get(i));
                commonActions.clearAndPopulateEditField(addressFields.get(i), customerInfo.get(i));
            }
        }
        CommonActions.webBrowserBody.click();
        LOGGER.info("enterPrimaryAndAlternateContact started");
    }

    /**
     * Enter the email ID on page
     *
     * @param page Homepage or checkout
     */
    public void enterEmailIdOnPage(String page) {
        LOGGER.info("enterEmailIdOnPage started on " + page);
        WebElement emailAddress;
        if (page.equalsIgnoreCase(Constants.HOMEPAGE)) {
            emailAddress = MyAccountPage.myAccountEmailAddress;
        } else {
            emailAddress = CommonActions.email;
        }
        if (!emailAddress.getAttribute(Constants.VALUE).isEmpty())
            emailAddress.sendKeys(Keys.CONTROL, "a", Keys.BACK_SPACE);
        String emailId = customer.getCustomer(Customer.CustomerType.MY_ACCOUNT_NEW_CUSTOMER.toString()).email;
        driver.scenarioData.genericData.put(WebServices.MY_ACCOUNT_EMAIL_ID, emailId);
        commonActions.enterTextIntoField(emailAddress, emailId);
        driver.waitForPageToLoad();
        LOGGER.info("enterEmailIdOnPage completed on " + page);
    }

    /**
     * Select the option for the specified order
     *
     * @param option      view order or view details
     * @param orderNumber order number
     */
    public void selectOptionForOrder(String option, String orderNumber) {
        LOGGER.info("selectOptionForOrder started for option " + option + " on order " + orderNumber);
        boolean found = false;
        boolean loop = true;
        int maxFindorderAttempts = 12;
        List<WebElement> orderRows;
        commonActions.waitForSpinner();
        do {
            if (option.equalsIgnoreCase(ConstantsDtc.VIEW_DETAILS)) {
                orderRows = webDriver.findElements(viewDetailsBy);
            } else {
                orderRows = webDriver.findElements(viewOrderBy);
            }
            commonActions.waitForSpinner();
            List<WebElement> appointmentTabOrderNumber = webDriver.findElements(appointmentNumbers);
            for (int i = 0; i < appointmentTabOrderNumber.size(); i++) {
                String ordernum = appointmentTabOrderNumber.get(i).getText();
                if (ordernum.toLowerCase().contains(orderNumber.toLowerCase())) {
                    found = true;
                    loop = false;
                    driver.jsScrollToElementClick(orderRows.get(i));
                    break;
                }
            }
            if (!found) {
                if (!driver.isElementDisplayed(CommonActions.nextBtnBy)) {
                    driver.waitForPageToLoad(Constants.THREE_THOUSAND);
                    webDriver.navigate().refresh();
                    attemptsCounter++;
                    if (attemptsCounter == maxFindorderAttempts) {
                        loop = false;
                        break;
                    }
                } else {
                    driver.jsScrollToElementClick(webDriver.findElement(CommonActions.nextBtnBy));
                }
            }
        } while (loop);
        Assert.assertTrue("Appointment with order number " + orderNumber + " not found in " +
                attemptsCounter + " attempts", found);
        LOGGER.info("selectOptionForOrder completed for option " + option + " on order " + orderNumber);
    }

    /**
     * Select the reason for cancellation of appointment
     *
     * @param reason reason for cancellation
     */
    public void selectReasonForCancellation(String reason) {
        LOGGER.info("selectReasonForCancellation for reason: '" + reason + "'  started");
        driver.jsScrollToElementClick(CommonActions.sortByDropdown);
        driver.waitForMilliseconds();
        driver.clickElementWithText(CommonActions.spanTagNameBy, reason);
        LOGGER.info("selectReasonForCancellation for reason: '" + reason + "' completed");
    }

    /**
     * Verify if the sorting is displayed on the page
     *
     * @param page appointment summary or order summary page
     */
    public void verifySortingIsDisplayed(String page) {
        LOGGER.info("verifySortingIsDisplayed started on " + page);
        WebElement sortOption;
        if (page.equalsIgnoreCase(Constants.ORDER))
            sortOption = webDriver.findElement(sortOptionOrderSummaryPageBy);
        else
            sortOption = webDriver.findElement(sortOptionAppointmentSummaryPageBy);
        Assert.assertTrue("FAIL: Sort option is not displayed on " + page + " page", driver.isElementDisplayed(sortOption));
        LOGGER.info("verifySortingIsDisplayed completed on " + page);
    }

    /**
     * verifies the sorting order displayed
     *
     * @param order Ascending or Descending
     */
    public void verifyOrderOfSorting(String order) {
        LOGGER.info("verifyOrderOfSorting started for " + order + " order");
        String defaultSortingOrderText = webDriver.findElement(CommonActions.sortByOptionBy).getText();
        Assert.assertTrue("FAIL: Sorting option is not displayed in " + order + " order",
                defaultSortingOrderText.toLowerCase().contains(order.toLowerCase()));
        LOGGER.info("verifyOrderOfSorting completed for " + order + " order");
    }

    /**
     * Assert ascending or descending order of items on Order History page
     *
     * @param order Ascending or Descending
     */
    public void assertSortedOrder(String order) {
        LOGGER.info("assertSortedOrder started for " + order + " order");
        List<WebElement> orderDates = webDriver.findElements(orderDateInHistoryTabListBy);
        ArrayList<String> orderDatesBeforeSorting = new ArrayList<String>();
        ArrayList<String> sortedDate = new ArrayList<String>();
        ArrayList<String> temp2 = new ArrayList<String>();
        for (int i = 0; i < orderDates.size(); i++) {
            String orderDate = orderDates.get(i).getText().substring(5);
            if (!orderDate.isEmpty())
                orderDatesBeforeSorting.add(orderDate);
        }

        for (int k = 0; k < orderDatesBeforeSorting.size(); k++) {
            String date = "";
            date += Constants.month.get(orderDatesBeforeSorting.get(k).substring(1, 4));
            date += "-";

            if (orderDatesBeforeSorting.get(k).length() > 12) {
                date += orderDatesBeforeSorting.get(k).substring(5, 7);
                date += "-";
                date += orderDatesBeforeSorting.get(k).substring(9, 13);
            } else {
                date += "0";
                date += orderDatesBeforeSorting.get(k).substring(5, 6);
                date += "-";
                date += orderDatesBeforeSorting.get(k).substring(8, 12);
            }
            sortedDate.add(date);
            temp2.add(date);
        }
        commonActions.dateBubbleSort(sortedDate, order);
        boolean isEqual = sortedDate.equals(temp2);
        Assert.assertTrue("FAIL: The orders are not arranged in " + order + " order", isEqual);
        LOGGER.info("assertSortedOrder complete for " + order + " order");
    }
}