package dtc.pages;

/**
 * Created by aarora on 10/10/16.
 */

import dtc.data.ConstantsDtc;
import org.junit.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;

import common.Constants;
import utilities.Driver;

import java.util.List;
import java.util.logging.Logger;

public class SearchAutocompleteDropDownPage {

	private Driver driver;
	private WebDriver webDriver;
	private CommonActions commonActions;
	private final Logger LOGGER = Logger.getLogger(SearchAutocompleteDropDownPage.class.getName());

	@FindBy(className = "autocomplete-row__itemid")
	public static WebElement autoCompleteTireNameInfo;

	@FindBy(className = "autocomplete-row__productname")
	public static WebElement autoCompleteProductNameInfo;

	@FindBy(css = "div[class*= 'results-bar__count-line___']")
	public static WebElement searchResultsTitle;

	@FindBy(css = "p[class*='no-results__description']")
	private static WebElement searchResultsIrrelaventTitle;

	@FindBy(css = "a[class*='no-results__shop'][href*='tires']")
	private static WebElement shopTiresLink;

	@FindBy(css = "[class*='results__shop'][href*='wheels']")
	private static WebElement shopWheelsLink;

	@FindBy(xpath = "//a[contains(text(),'All-Season tires')]")
	private static WebElement allSeasonTireLink;

	@FindBy(css = "div[class*='fitment-entry fitment-entry__container']")
	private static WebElement fitmentPopUp;

	@FindBy(css = "span[class*='no-results__shop-by-label']")
	private static WebElement shopByOption;

	@FindBy(xpath = "//h4[contains(text(),'Start A New Search')]")
	private static WebElement startANewSearchText;

	private static By vehicleLinkBy = By.cssSelector("a[class='dt-link'][href*='vehicle']");

	private static By sizeLinkBy = By.cssSelector("a[class='dt-link'][href*='#/fitment/vehicle']");

	private static By brandLinkBy = By.cssSelector("a[class='dt-link'][href*='brands']");

	private static By autoCompleteProductNameInfoBy = By.className("autocomplete-row__productname");

	private static final String SEARCH = "Search";
	private static final String SEARCH_TITLE = "Search title";
	private static final String SEARCH_INVALID_TITLE = "Search invalid title";
	private static final String SHOP_TIRES_THAT_FIT = "Shop tires that fit";
	private static final String SHOP_WHEELS_THAT_FIT = "Shop wheels that fit";
	private static final String SHOP_BY = "Shop By";
	private static final String MY_VEHICLE = "My Vehicle";
	private static final String VEHICLE_OPTION_TIRES = "Vehicle under Tires";
	private static final String VEHICLE_OPTION_WHEELS = "Vehicle under Wheels";
	private static final String SIZE_OPTION_TIRE = "Size under Tires";
	private static final String SIZE_OPTION_WHEELS = "Size under Wheels";
	private static final String BRAND_OPTION_TIRES = "Brand under Tires";
	private static final String BRAND_OPTION_WHEELS = "Brand under Wheels";
	private static final String START_A_NEW_SEARCH = "Start A New Search";


	public SearchAutocompleteDropDownPage(Driver driver) {
		this.driver = driver;
		webDriver = driver.getDriver();
		PageFactory.initElements(webDriver, this);
	}

	/**
	 * Verifies passed in product name is shown in the results panel
	 *
	 * @param productName Expected name of product
	 */
	public void assertFirstProductNameInResultPanel(String productName) {
		LOGGER.info("assertFirstProductNameInResultPanel started");
		driver.waitForMilliseconds(Constants.TWO_THOUSAND);

		List<WebElement> webElements = webDriver.findElements(autoCompleteProductNameInfoBy);
		boolean found = false;
		for (WebElement webElement : webElements)
		{
			if (webElement.getText().equals(productName)) {
				found = true;
				break;
			}
		}
		Assert.assertTrue(
				"FAIL: The expected product name: \"" + productName + "\" did NOT match the actual: \""
						+ autoCompleteProductNameInfo.getText() + "\"!", found);
		LOGGER.info("Confirmed that \"" + productName + "\" was listed in the Search Autocomplete DropDown.");
		LOGGER.info("assertFirstProductNameInResultPanel completed");
	}

	/**
	 * Verifies passed in item ID is shown in the results panel
	 *
	 * @param itemId Expected item ID
	 */
	public void assertFirstItemIdInResultPanel(String itemId) {
		LOGGER.info("assertFirstItemIdInResultPanel started");
		Assert.assertEquals("FAIL: \"" + itemId + "\" was NOT found in dropdown!", itemId,
				autoCompleteTireNameInfo.getText());
		LOGGER.info("Confirmed that \"" + itemId + "\" was listed in the Search Autocomplete DropDown.");
		LOGGER.info("assertFirstItemIdInResultPanel completed");
	}

	/**
	 * Finds product on page by name and clicks on it
	 *
	 * @param productName Name of product to select
	 */
	public void selectProductByName(String productName) {
		LOGGER.info("selectProductByName started");
		boolean found = false;
		try {
			List<WebElement> webElements = webDriver.findElements(autoCompleteProductNameInfoBy);
			for (WebElement webElement : webElements)
			{
				if (webElement.getText().equals(productName)) {
					webElement.click();
					found = true;
					break;
				}
			}
			Assert.assertTrue("FAIL: Unable to click " + productName + " in the autocomplete dropdown box", found);
		} catch (Exception e) {
			HomePage.searchBoxInput.sendKeys(Keys.RETURN);
		}
		LOGGER.info("selectProductByName completed");
	}

	/**
	 * This method will return the webelement matching the input String.
	 *
	 * @param elementName - Name of the Web Element
	 * @return - WebElement
	 */
	public WebElement returnElement(String elementName) {
		LOGGER.info("returnElement started");
		WebElement returnElement = null;
		switch (elementName) {
			case SEARCH:
				returnElement = HomePage.searchBoxInput;
				break;
			case SEARCH_TITLE:
				returnElement = searchResultsTitle;
				break;
			case SEARCH_INVALID_TITLE:
				returnElement = searchResultsIrrelaventTitle;
				break;
			case SHOP_TIRES_THAT_FIT:
				returnElement = shopTiresLink;
				break;
			case SHOP_WHEELS_THAT_FIT:
				returnElement = shopWheelsLink;
				break;
			case SHOP_BY:
				returnElement = shopByOption;
				break;
			case MY_VEHICLE:
				returnElement = CommonActions.myVehiclesHeaderButton;
				break;
			case START_A_NEW_SEARCH:
				returnElement = startANewSearchText;
				break;
			case VEHICLE_OPTION_TIRES:
				returnElement = webDriver.findElements(vehicleLinkBy).get(0);
				break;
			case ConstantsDtc.FITMENT_POPUP:
				returnElement = fitmentPopUp;
				break;
			case VEHICLE_OPTION_WHEELS:
				returnElement = webDriver.findElements(vehicleLinkBy).get(1);
				break;
			case SIZE_OPTION_TIRE:
				returnElement = webDriver.findElements(sizeLinkBy).get(0);
				break;
			case SIZE_OPTION_WHEELS:
				returnElement = webDriver.findElements(sizeLinkBy).get(1);
				break;
			case BRAND_OPTION_TIRES:
				returnElement = webDriver.findElements(brandLinkBy).get(0);
				break;
			case BRAND_OPTION_WHEELS:
				returnElement = webDriver.findElements(brandLinkBy).get(1);
				break;
			default:
				Assert.fail("FAIL: Could not find element that matched string passed from step");
		}
		LOGGER.info("returnElement completed");
		return returnElement;
	}

	/**
	 * This method verifies if the element is displayed
	 *
	 * @param elementName -send in elementName and gets the WebELement from returnElement method.
	 */
	public void verifySearchResultPageElementExists(String elementName) {
		LOGGER.info("verifySearchResultPageElementExists started");
		driver.isElementDisplayed(returnElement(elementName));
		LOGGER.info("verifySearchResultPageElementExists completed");
	}

	/**
	 * Click method for SearchAutocompleteDropDownPage.java class
	 *
	 * @param elementName - send in elementName and gets the WebELement from returnElement method.
	 */
	public void clickInSearchAutocomplete(String elementName) {
		LOGGER.info("clickInSearchAutocomplete started");
		driver.waitForElementClickable(returnElement(elementName));
		WebElement element = returnElement(elementName);
		element.click();
		LOGGER.info("clickInSearchAutocomplete completed");
	}

	/**
	 * verifies the text Attributes of the Web Elements
	 *
	 * @param elementName - send in elementName and gets the WebELement from returnElement method.
	 * @param validation  - String to validate against
	 */
	public void verifyTextOfElementInSearchAutocomplete(String elementName, String validation) {
		LOGGER.info("verifyTextOfElementInSearchAutocomplete started");
		driver.waitForElementVisible(returnElement(elementName));
		WebElement element = returnElement(elementName);
		String text = element.getText().toLowerCase();
		Assert.assertTrue("FAIL: The expected text of - " + validation + " was Not Found, " +
				"This Text was found - " + text, text.contains(validation.toLowerCase()));
		LOGGER.info("verifyTextOfElementInSearchAutocomplete completed");
	}
}