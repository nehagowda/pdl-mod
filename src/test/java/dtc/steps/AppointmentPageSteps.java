package dtc.steps;

import common.Config;
import common.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.ConstantsDtc;
import dtc.data.Customer;
import dtc.pages.AppointmentPage;
import dtc.pages.CommonActions;
import dtc.pages.OrderPage;
import orderxmls.pages.OrderXmls;
import utilities.Driver;

/**
 * Created by aaronbriel on 10/24/16.
 */
public class AppointmentPageSteps {

    private AppointmentPage appointmentPage;
    private OrderPage orderPage;
    private Customer customer;
    private CommonActions commonActions;
    private Scenario scenario;
    private Driver driver;

    public AppointmentPageSteps(Driver driver) {
        this.driver = driver;
        appointmentPage = new AppointmentPage(driver);
        orderPage = new OrderPage(driver);
        commonActions = new CommonActions(driver);
        customer = new Customer();
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @And("^I schedule an appointment for \"([^\"]*)\"$")
    public void i_schedule_an_appointment(String customerType) throws Throwable {
        Customer apptcCust = customer.getCustomer(customerType);
        appointmentPage.makeAppointment(apptcCust);
    }

    @And("^I reserve items for \"([^\"]*)\"$")
    public void i_reserve_items(String customerType) throws Throwable {
        Customer apptcCust = customer.getCustomer(customerType);
        appointmentPage.clickNextStepForCustomerInformation();
        appointmentPage.enterCustomerInformation(apptcCust);
        commonActions.clickDefaultFormSubmitButton();
    }

    @And("^I click next step for customer information$")
    public void i_click_next_for_customer_information() throws Throwable {
        appointmentPage.clickNextStepForCustomerInformation();
    }

    @And("^I click continue for appointment customer details page$")
    public void i_click_continue_for_appointment_customer_details_page() throws Throwable {
        appointmentPage.clickContinueForAppointmentCustomerDetailsPage();
    }

    @When("^I select edit and select change store$")
    public void i_select_edit_and_change_store() throws Throwable {
        appointmentPage.clickAppointmentEditLink();
        appointmentPage.clickChangeStore();
    }

    @When("^I select service option\\(s\\): \"(.*?)\"$")
    public void i_select_service_options(String options) throws Throwable {
        appointmentPage.selectService(options);
    }

    @When("^I select default date and time$")
    public void i_select_default_date_and_time() throws Throwable {
        appointmentPage.clickSetAppointmentDetailsForDateAndTime();
        commonActions.clickButtonByText(Constants.CONTINUE);
        appointmentPage.selectDate();
        appointmentPage.selectTime();
    }

    @And("^I select default date$")
    public void i_select_default_date() throws Throwable {
        appointmentPage.clickSetAppointmentDetailsForDateAndTime();
        commonActions.clickButtonByText(Constants.CONTINUE);
        appointmentPage.selectDate();
    }

    @Then("^I verify the appointment time range$")
    public void i_verify_appointment_time_range() throws Throwable {
        appointmentPage.verifyAppointmentTimeRange();
    }

    @And("^I extract date and time for validation$")
    public void i_extract_date_and_time_for_validation() throws Throwable {
        appointmentPage.extractAppointmentDateTimeAndSetScenarioData();
    }

    @Then("^I verify date and time on the customer details appointment page$")
    public void i_verify_date_and_time_on_the_customer_details_page() throws Throwable {
        String appointmentDate = driver.scenarioData.genericData.get(ConstantsDtc.APPOINTMENT_DATE);
        String appointmentTime = driver.scenarioData.genericData.get(ConstantsDtc.APPOINTMENT_TIME);
        appointmentPage.verifyDateAndTime(appointmentDate, appointmentTime);
    }

    @Then("^I verify date and time on the order confirmation page$")
    public void i_verify_date_and_time_on_the_order_confirmation_page() throws Throwable {
        if (commonActions.paymentSystemDownMessageExists(scenario))
            return;
        String appointmentDate = driver.scenarioData.genericData.get(ConstantsDtc.APPOINTMENT_DATE);
        String appointmentTime = driver.scenarioData.genericData.get(ConstantsDtc.APPOINTMENT_TIME);
        orderPage.verifyDateAndTime(appointmentDate, appointmentTime);
    }

    @And("^I verify \"([^\"]*)\" store on the customer information appointment page$")
    public void i_verify_store_on_the_customer_information_page(String store) throws Throwable {
        //store address not appearing on mobile view, hence verifying with business. This will be updated once after business confirmation.
        if (!Config.isMobile()) {
            appointmentPage.verifyStore(store);
        }
    }

    @And("^I verify default store on the customer details appointment page$")
    public void i_verify_default_store_on_the_customer_details_page() throws Throwable {
        appointmentPage.verifyStore(Config.getDefaultStoreAddress().split(",")[0]);
    }

    @When("^I select \"(Schedule Appointment|Continue To Shipping Method|Continue To Payment|Make Appointment)\" after entering customer information for \"(.*?)\"$")
    public void i_continue_after_entering_customer_information(String text, String customerType) throws Throwable {
        commonActions.setScenarioDataExpediteYourExperience(false);
        Customer apptcCust = customer.getCustomer(customerType);
        scenario.write(apptcCust.getCustomerDataString(apptcCust));
        appointmentPage.enterCustomerInformation(apptcCust);
        commonActions.addScenarioGenericData();
        if (text.equalsIgnoreCase(ConstantsDtc.CONTINUE_TO_PAYMENT)
                || text.equalsIgnoreCase(ConstantsDtc.MAKE_APPOINTMENT)) {
            commonActions.setCustomerScenarioData();
        }
        commonActions.clickButtonByText(text);
    }

    @When("^I enter customer information for \"(.*?)\"$")
    public void i_enter_customer_information(String customerType) throws Throwable {
        Customer apptcCust = customer.getCustomer(customerType);
        appointmentPage.enterCustomerInformation(apptcCust);
    }

    @When("^I enter \"(?:valid|invalid)\" customer phone number: \"(.*?)\"$")
    public void i_enter_phone_number(String phoneNumber) throws Throwable {
        appointmentPage.enterCustomerPhoneNumber(phoneNumber);
    }

    @When("^I enter information for other recipient \"([^\"]*)\" requesting delivery by \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_enter_other_recipient_information(String otherCustomer, String deliveryType, String textUpdates) {
        Customer someoneElse = customer.getCustomer(otherCustomer);
        boolean receiveTextUpdates = false;
        if (textUpdates.equalsIgnoreCase(Constants.TEXT_UPDATES)) {
            receiveTextUpdates = true;
        }
        appointmentPage.enterSomeoneElseInformation(someoneElse, deliveryType, receiveTextUpdates);
    }

    @When("^I select 'Set Appointment Details' for Date and Time$")
    public void i_select_set_appointment_details_for_date_and_time() throws Throwable {
        appointmentPage.clickSetAppointmentDetailsForDateAndTime();
    }

    @When("^I select the \"(First|Last)\" appointment to verify total available appointment days are \"([^\"]*)\"$")
    public void i_select_the_appointment_to_verify_total_available_appointment_days_are(String day, String availDays) throws Throwable {
        appointmentPage.selectDateAndVerifyAvailableDays(day, availDays);
        appointmentPage.selectTime();
    }

    @And("^I click reserve after entering customer information, including the address, for \"([^\"]*)\"$")
    public void i_enter_customer_information_with_address(String customerType) throws Throwable {
        Customer apptcCust = customer.getCustomer(customerType);
        appointmentPage.enterCustomerInformation(apptcCust);
        appointmentPage.enterCustomerAddressInformation(apptcCust);
        commonActions.clickDefaultFormSubmitButton();
    }

    @And("^I click Edit Appointment link$")
    public void i_click_edit_appointment_link() throws Throwable {
        appointmentPage.clickEditAppointmentLink();
    }

    @When("^I click Select \"([^\"]*)\"$")
    public void i_click_select_datetime(String selection) throws Throwable {
        appointmentPage.clickSelectDateOrTime(selection);
    }

    @Then("^I verify the datepicker message is correct$")
    public void i_verify_the_datepicker_message_is_correct() throws Throwable {
        appointmentPage.verifyDatePickerMessage();
    }

    @When("^I close the Appointment Selected message bar$")
    public void i_close_the_appointment_selected_message_bar() throws Throwable {
        appointmentPage.closeAppointmentSelectedMessageBar();
    }

    @Then("^I verify the Appointment Selected message is not displayed$")
    public void i_verify_the_appointment_selected_message_not_displayed() throws Throwable {
        appointmentPage.assertAppointmentSelectedMessageIsNotDisplayed();
    }

    @Then("^I verify peak hours appointment is indicated in appointment details$")
    public void i_verify_peak_hours_appointment_is_indicated_in_appointment_details() throws Throwable {
        appointmentPage.verifyPeakHoursInAppointmentDetails();
    }

    @Then("^I verify selected service option\\(s\\): \"(.*?)\" is displayed on Service Appointment page$")
    public void i_verify_selected_service_option_text_on_service_appointment_page(String serviceOption) throws Throwable {
        appointmentPage.assertSelectedServiceOptionText(serviceOption);
    }

    @Then("^I verify \"(SERVICES|APPOINTMENT DETAILS|CUSTOMER DETAILS)\" section is displayed and active$")
    public void i_verify_section_title_is_displayed_and_active(String sectionTitle) throws Throwable {
        appointmentPage.assertActiveSectionTitleMessageIsDisplayed(sectionTitle);
    }

    @When("^I verify \"(CHECKOUT WITH APPOINTMENT|APPOINTMENT ONLY)\" option is displayed on appointments 'Action Needed' modal")
    public void i_verify_the_appointment_action_needed_modal_button_is_displayed(String actionNeededButtonOption) throws Throwable {
        appointmentPage.assertActionNeededButtonIsDisplayed(actionNeededButtonOption);
    }

    @When("^I select 'CONFIRM APPOINTMENT' on Installation Appointment page")
    public void i_select_confirm_appointment_on_installation_appointment_page() throws Throwable {
        appointmentPage.clickConfirmAppointment();
    }

    @When("^I save all the available appointment dates$")
    public void i_save_all_the_available_appointment_dates() throws Throwable {
        appointmentPage.saveListOfAllAppointmentDates();
    }

    @When("^I select appointment date after \"(.*?)\" days$")
    public void i_select_appointment_date_after_two_days(String numberOfDays) throws Throwable {
        appointmentPage.selectAppointmentFutureDays(Integer.parseInt(numberOfDays));
    }

    @Then("^I verify Previous dates are displayed on the appointment page$")
    public void i_verify_previoes_dates_are_displayed_on_the_appointment_page() throws Throwable {
        appointmentPage.assertListOfAllAppointmentDates();
    }

    @When("^I verify the date \"([^\"]*)\" is displayed on the appointment page$")
    public void i_verify_the_date_is_displayed_on_the_appointment_page(String dateToVerify) throws Throwable {
        appointmentPage.assertStoreHolidayDates(dateToVerify);
    }

    @When("^I select \"([^\"]*)\" and day \"([^\"]*)\" to view its appointment timings$")
    public void i_select_holiday_date(String month, String day) throws Throwable {
        appointmentPage.selectDayInMonth(month, day);
    }

    @When("^I verify the next store open timings for \"(Full Day Holiday|Partial Day Holiday|Partial Day Local Outage)\" as \"([^\"]*)\" displayed$")
    public void i_verify_next_store_open_timings(String holidayType, String timings) throws Throwable {
        appointmentPage.assertNextDayStoreTimings(holidayType, timings);
    }

    @When("^I should see the store holiday message for \"(Full Day Holiday|Partial Day Holiday|Partial Day Local Outage|Sunday Holiday)\" is displayed as \"([^\"]*)\"$")
    public void i_should_see_store_holiday_message(String holidayType, String expectedstoreMessage) throws Throwable {
        appointmentPage.assertStoreHolidayMessage(holidayType, expectedstoreMessage);
    }

    @When("^I should see the store holiday comment \"([^\"]*)\" is displayed$")
    public void i_should_see_store_holiday_comment(String expectedstoreComment) throws Throwable {
        appointmentPage.assertStoreHolidayComment(expectedstoreComment);
    }

    @When("^I should see the \"([^\"]*)\" and \"([^\"]*)\" on the appointment header$")
    public void i_should_see_date_on_header(String month, String day) throws Throwable {
        appointmentPage.assertAppointmentHeader(month, day);
    }

    @When("^I verify element \"([^\"]*)\" is \"(displayed|not displayed)\" in appointment page$")
    public void i_verify_element_is_in_appointment_page(String elementName, String status) throws Throwable {
        appointmentPage.assertElementDisplayedNotDisplayed(elementName, status);
    }

    @Then("^I verify \"(.*?)\" details is displayed on the \"(appointment confirmation|appointment details)\" page$")
    public void i_verify_customer_details_is_displayed_on_the_page(String customerType, String page) throws Throwable {
        Customer customerObject = customer.getCustomer(customerType);
        appointmentPage.assertMyInformationDisplayed(customerObject, page);
    }

    @And("^I verify \"(No vehicle selected|add vehicle|add new vehicle)\" is displayed on Service Appointment page$")
    public void i_verify_no_vehicle_elements_displayed_on_service_appointment_page(String text) throws Throwable {
        if (text.equalsIgnoreCase(ConstantsDtc.NO_VEHICLE_SELECTED))
            appointmentPage.assertNoVehicleSelectedMessageDisplayed();
        else
            appointmentPage.assertAddVehicleLinkDisplayed(text);
    }

    @And("^I click on the \"(add new vehicle|add vehicle)\" link on Service Appointment page$")
    public void i_click_on_the_link_on_service_appointment_page(String text) throws Throwable {
        appointmentPage.clickAddVehicleOnServiceAppointmentPage(text);
    }

    @And("^I verify selected vehicle on Service Appointment page is \"([^\"]*)\"$")
    public void i_verify_selected_vehicle_on_service_appointment_page(String vehicle) throws Throwable {
        appointmentPage.assertSelectedVehicleOnServiceAppointmentPage(vehicle);
    }

    @And("^I verify selected vehicle on Service Appointment page contains edit vehicle link and unselected vehicles do not$")
    public void i_verify_edit_vehicle_link_is_or_is_not_displayed() throws Throwable {
        appointmentPage.assertEditVehicleLinkOnServiceAppointmentPage();
    }

    @And("^I select \"([^\"]*)\" on Service Appointment page$")
    public void i_select_vehicle_on_service_appointment_page(String vehicle) throws Throwable {
        appointmentPage.selectVehicleOnServiceAppointmentPage(vehicle);
    }

    @And("^I verify \"([^\"]*)\" vehicle on Service Appointment page has \"([^\"]*)\" tire size badge and associated label$")
    public void i_verify_selected_vehicle_on_service_appointment_page_has_expected_tire_size_badge_and_text(String vehicle, String badge) throws Throwable {
        appointmentPage.assertTireSizeBadgeAndLabelOnServiceAppointmentPage(vehicle, badge);
    }
}
