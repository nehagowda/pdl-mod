package dtc.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import dtc.pages.ChangeStorePopUpPage;
import utilities.Driver;

public class ChangeStorePopUpPageSteps {

    private ChangeStorePopUpPage changeStorePopUpPage;

    public ChangeStorePopUpPageSteps(Driver driver) {
        changeStorePopUpPage  = new ChangeStorePopUpPage(driver);
    }

    @When("^I click on element name \"(.*?)\" in change store popup$")
    public void i_click_on_element_name_in_change_store_popup(String elementName) throws Throwable {
        changeStorePopUpPage.clickOnElement(elementName);
    }

    @And("^I save new store address in store popup$")
    public void i_save_new_store_address_in_store_pop_up() throws Throwable {
        changeStorePopUpPage.saveStoreAddress();
    }

    @And("^I verify current store details in store popup$")
    public void i_verify_current_store_details_in_store_popup() throws Throwable {
        changeStorePopUpPage.verifyStoreDetails();
    }

    @And("^I click on continue for successful change of store$")
    public void i_click_on_continue_for_successful_change_of_store() throws Throwable {
        changeStorePopUpPage.saveStoreDetailsAndClickContinue();
    }
}
