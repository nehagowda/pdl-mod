package dtc.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.ConstantsDtc;
import dtc.pages.CheckAvailabilityPopupPage;
import dtc.pages.CommonActions;
import utilities.Driver;

/**
 * Created by abriel on 1/11/2017.
 */
public class CheckAvailabilityPopupPageSteps {

    private CheckAvailabilityPopupPage checkAvailabilityPopupPage;
    private CommonActions commonActions;

    public CheckAvailabilityPopupPageSteps(Driver driver) {
        checkAvailabilityPopupPage = new CheckAvailabilityPopupPage(driver);
        commonActions = new CommonActions(driver);
    }

    @Then("^I should verify that the Check Availability popup loaded$")
    public void i_should_verify_that_the_check_availability_popup_loaded() throws Throwable {
        checkAvailabilityPopupPage.assertCheckAvailabilityPopupLoaded();
    }
    
    @Then("^I should verify that In Stock Label is not displayed on Check Inventory popup$")
    public void i_should_verify_that_inStock_label_not_present_on_check_availability_popup() throws Throwable {
        checkAvailabilityPopupPage.assertInStockLabelNotPresent();
    }
    
    @When("^I close the Check Availability popup$")
    public void i_close_check_availability_popup() throws Throwable {
    }
    
    @Then("^I should verify that default store MY STORE label is at top and visible$")
    public void i_should_verify_that_default_store_myStore_label_is_at_top() throws Throwable {
        checkAvailabilityPopupPage.assertDefaultStoreMyStoreLabelIsTopInTheList();
    }

    @And("^I should verify that the Check Inventory headline is \"(.*?)\"$")
    public void i_should_verify_product_availability_headline(String headline) throws Throwable {
        checkAvailabilityPopupPage.assertCheckAvailabilityHeadline(headline);
    }

    @And("^I should verify that the Product Info shows \"(.*?)\", \"(.*?)\", and \"(.*?)\" in \"(Check Availability|CheckFit)\"$")
    public void i_should_verify_product_info(String brand, String productName, String tireSize, String modal)
            throws Throwable {
        checkAvailabilityPopupPage.assertProductInfo(brand, modal);
        checkAvailabilityPopupPage.assertProductInfo(productName, modal);
        checkAvailabilityPopupPage.assertProductInfo(tireSize, modal);
    }

    @And("^I should verify price is not blank$")
    public void i_verify_price_not_blank() throws Throwable {
        checkAvailabilityPopupPage.assertPricePopulated();
    }

    @When("^I enter a quantity of \"(.*?)\"$")
    public void i_enter_quantity(String quantity) throws Throwable {
        checkAvailabilityPopupPage.enterQuantity(quantity);
    }

    @Then("^I should verify that a \"(quantity|validity)\" error message is displayed$")
    public void i_should_verify_quantity_error(String errorType) throws Throwable {
        checkAvailabilityPopupPage.assertQuanityErrorMessage(errorType);
    }

    @And("^I should verify that the Add To Cart button is disabled$")
    public void i_should_verify_add_to_cart_disabled() throws Throwable {
        checkAvailabilityPopupPage.assertAddToCartDisabled();
    }

    @Then("^I should verify that the show quantity filter is displayed with \"(.*?)\"$")
    public void i_verify_show_quantity_filter(String quantity) throws Throwable {
        checkAvailabilityPopupPage.assertShowQuantityFilter(quantity);
    }

    @When("^I enter a zipcode of \"(.*?)\"$")
    public void i_enter_zipcode(String zipcode) throws Throwable {
        //TODO: Zip code will need to be conditionalized based on environment and site (Defect # 6833)
        checkAvailabilityPopupPage.enterZipcode(zipcode);
    }

    @Then("^I should verify that a zip code error message is displayed$")
    public void i_should_verify_zipcode_error() throws Throwable {
        checkAvailabilityPopupPage.assertZipcodeErrorMessage();
    }

    @When("^I click go and wait for results to load$")
    public void i_click_go() throws Throwable {
        checkAvailabilityPopupPage.clickGoAndWaitForResults();
    }

    @Then("^the first store listed should be within \"(.*?)\" miles$")
    public void i_should_verify_first_store(String miles) throws Throwable {
        checkAvailabilityPopupPage.assertFirstStoreWithinDistance(miles);
    }

    @When("^I click the \"(In stock|Show more)\" filter$")
    public void i_click_filter(String filter) throws Throwable {
        checkAvailabilityPopupPage.clickFilter(filter);
    }

    @Then("^I should verify the first store has stock quantity greater than \"(.*?)\"$")
    public void i_should_verify_first_store_quantity_above_specified_value(String quantity) throws Throwable {
        checkAvailabilityPopupPage.assertFirstStoreQuantityAboveSpecifiedValue(quantity);
    }

    @When("^I select store \"(.*?)\" to make my store$")
    public void i_select_store(String storeNum) throws Throwable {
        checkAvailabilityPopupPage.selectStoreAndMakeMyStore(Integer.parseInt(storeNum)-1);
    }

    @Then("^I should verify that store \"(.*?)\" is now the current store$")
    public void i_should_verify_store_is_current_store(String storeNum) throws Throwable {
        checkAvailabilityPopupPage.assertStoreIsCurrentStore(Integer.parseInt(storeNum)-1);
    }

    @And("^I should verify that make my store button is displayed$")
    public void i_should_verify_that_make_my_store_button_is_displayed() throws Throwable {
        checkAvailabilityPopupPage.assertMakeMyStoreButtonDisplayed();
    }

    @Then("^I verify the nearby store is displayed when directed from \"(PLP|PDP|Shopping Cart)\"$")
    public void i_verify_the_nearby_store_for_default_dt_store_is_displayed_directed_from_page(String page) throws Throwable {
        checkAvailabilityPopupPage.assertNearbyStoreForDefaultDtStoreQA(page);
    }

    @Then("^I verify the \"(default|[^\"]*)\" store MY STORE is displayed at top$")
    public void i_verify_the_default_store_myStore_is_displayed_at_top(String store) throws Throwable {
        checkAvailabilityPopupPage.assertDefaultMyStoreDisplayedOnTop(store);
    }

    @Then("^I verify \"(MY STORE|Nearby Stores)\" is displayed on check inventory$")
    public void i_verify_the_expected_store_is_displayed(String store) throws Throwable {
        if (store.equalsIgnoreCase(ConstantsDtc.NEARBY_STORES)) {
            checkAvailabilityPopupPage.assertNearbyStoreCheckInventory();
        } else {
            checkAvailabilityPopupPage.assertDefaultMyStoreDisplayedOnCheckInventoryModal();
        }
    }

    @When("^I close the Check Inventory popup$")
    public void i_close_check_inventory_popup() throws Throwable {
        checkAvailabilityPopupPage.closeCheckInventoryPopupModal();
    }

    @Then("^I verify the \"(.*?)\" for \"(Front|Rear)\" product \"(.*?)\" on \"(Check Availability)\"$")
    public void i_verify_inventory_message_for_set_item_on_popup_modal(String message, String itemType, String item, String page) throws Throwable {
        checkAvailabilityPopupPage.assertInventoryMessageForSetOnPopupModal(message, itemType, item, page);
    }

    @Then("^I verify the stock count message for \"(Front|Rear)\" product \"(.*?)\"$")
    public void i_verify_stock_count_message_for_set_item(String itemType, String item) throws Throwable {
        checkAvailabilityPopupPage.assertStockCountTextForSetOnPopupModal(itemType, item);
    }

    @And("^I verify quantity for item code: \"(.*?)\" on 'Check Inventory' popup matches cart page quantity$")
    public void i_verify_quantity_item_on_check_inventory_popup_matches_cart_page(String itemCode) throws Throwable {
        checkAvailabilityPopupPage.verifyCheckInventoryQuantityForItemMatchesCartQuantity(itemCode);
    }

    @And("^I verify 'Only Show In Stock' option is displayed on the 'Check Inventory' popup$")
    public void i_verify_only_show_in_stock_option_is_displayed() throws Throwable {
        checkAvailabilityPopupPage.verifyOnlyShowInStockOptionIsDisplayed();
    }

    @When("^I use the 'Show Stores Near' search with \"(.*?)\"$")
    public void i_use_show_stores_near_search_with_term(String searchTerm) throws Throwable {
        checkAvailabilityPopupPage.showStoresNearSearch(searchTerm);
    }

    @Then("^I verify nearby stores are displayed in the 'Check Inventory' popup$")
    public void i_verify_nearby_stores_are_displayed_in_the_check_inventory_popup() throws Throwable {
        checkAvailabilityPopupPage.verifyNearbyStoresAreDisplayed();
    }

    @When("^I click \"MAKE MY STORE\" for the first available store$")
    public void i_click_make_my_store_for_first_available_store() throws Throwable {
        checkAvailabilityPopupPage.clickMakeMyStoreForFirstStore();
    }

    @Then("^I verify the address in the check availabilty popup with my store$")
    public void i_verify_the_address_in_the_check_availabilty_popup_with_my_store () throws Throwable {
        checkAvailabilityPopupPage.verifyStoreName();
    }

    @When("^I click store \"(.*?)\" from the check inventory popup$")
    public void i_click_store_from_the_check_inventory_popup(String storeName) throws Throwable {
        checkAvailabilityPopupPage.clickMakeMyStoreForStore(storeName);
    }

    @Then("^I verify that the check availability modal has default sort by option set to \"(Availability|Nearest Store)\" and verify if the \"(stock count|distance)\" is in \"(descending|ascending)\" order$")
    public void i_verify_that_the_check_availability_modal_has_default_sort_by_option_set_to_option(String filter, String option, String order) throws Throwable {
        checkAvailabilityPopupPage.assertSortingOnCheckAvailabilityPopup(filter, option, order);
    }
}