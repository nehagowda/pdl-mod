package dtc.steps;

import common.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.ConstantsDtc;
import dtc.data.Vehicle;
import dtc.pages.CommonActions;
import dtc.pages.FitmentPage;
import dtc.pages.FitmentPopupPage;
import org.openqa.selenium.WebElement;
import utilities.CommonUtils;
import utilities.Driver;

/**
 * Created by aaronbriel on 1/30/17.
 */
public class FitmentPageSteps {

    private CommonActions commonActions;
    private FitmentPage fitmentPage;
    private int initialOptionCount;
    private Vehicle vehicle;
    private Scenario scenario;
    private FitmentPopupPage fitmentPopupPage;
    private Driver driver;

    public FitmentPageSteps(Driver driver) {
        this.driver = driver;
        commonActions = new CommonActions(driver);
        fitmentPage = new FitmentPage(driver);
        fitmentPopupPage = new FitmentPopupPage(driver);
        vehicle = new Vehicle();
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @When("^I click the \"([^\"]*)\" dropdown$")
    public void i_click_the_dropdown(String dropDownType)
            throws Throwable {
        fitmentPage.expandFitmentDropdown(dropDownType);
    }

    @When("^I click the disabled dropdown with value \"([^\"]*)\"$")
    public void i_click_the_disabled_dropdown(String dropDownType)
            throws Throwable {
        fitmentPage.clickDisabledFitmentDropdown(dropDownType);
    }

    @Then("^I verify that a dropdown has the value \"([^\"]*)\"$")
    public void i_verify_the_dropdown_has_value(String value)
            throws Throwable {
        fitmentPage.verifyDropdownWithValueIsDisplayed(value);
    }

    @When("^I select \"([^\"]*)\" from the expanded dropdown$")
    public void i_select_a_dropdown_value_from_an_expanded_menu(String value)
            throws Throwable {
        fitmentPage.selectDropDownValueFromExpandedMenu(value);
    }

    @And("^I verify that the \"([^\"]*)\" value in the dropdown is selected")
    public void i_verify_the_dropdown_value_is_selected(String value)
            throws Throwable {
        fitmentPage.verifyDropdownValueSelected(value);
    }

    @When("^I verify that the \"(Tires|Wheels|Pay now, install at store|Reserve now, install at store)\" radio " +
            "button is \"(enabled|disabled)\"$")
    public void i_verify_the_radio_button_is_enabled_or_disabled(String buttonType, String enabled)
            throws Throwable {
        fitmentPage.verifyRadioButtonEnabledOrDisabled(buttonType, Boolean.parseBoolean(enabled));
    }

    @When("^I type \"(.*?)\" in the \"(.*?)\" dropdown$")
    public void i_type_the_value_in_the_dropdown(String value, String dropDown)
            throws Throwable {
        fitmentPage.typeValueInDropdown(dropDown, value);
    }

    @Then("^I verify that the items in the drop down display in alphabetically order$")
    public void i_verify_the_dropdown_values_are_sorted()
            throws Throwable {
        fitmentPage.verifyDropdownValuesSorted();
    }

    @Then("^I verify that the dropdown is limited to values that start with \"(.*?)\"$")
    public void i_verify_the_dropdown_values_are_limited_to_specific_values(String character)
            throws Throwable {
        fitmentPage.verifyDropdownValuesLimited(character);
    }

    @And("^I click the fitment \"(Wheels|Tires)\" radio button$")
    public void i_click_the_fitment_radio_button(String buttonType) throws Throwable {
        fitmentPage.clickFitmentRadioButton(buttonType);
    }

    @And("^I verify that the dropdown menu \"(has|has not)\" expanded")
    public void i_verify_the_dropdown_menu_has_expanded(String expanded)
            throws Throwable {
        if (expanded.equalsIgnoreCase(Constants.HAS)) {
            fitmentPage.verifyDropdownMenuExpanded(true);
        } else {
            fitmentPage.verifyDropdownMenuExpanded(false);
        }
    }

    @And("^I verify that the \"(Search|view recommended tires)\" button is disabled")
    public void i_verify_the_search_button_is_disabled(String button)
            throws Throwable {
        fitmentPage.assertSearchButtonIsDisabled(button);
    }

    @And("^I verify that the Search button is enabled")
    public void i_verify_the_search_button_is_enabled()
            throws Throwable {
        fitmentPage.assertSearchButtonIsEnabled();
    }

    @And("^I verify that the \"(Search|view recommended tires)\" button color is \"([^\"]*)\"$")
    public void i_verify_the_search_button_color(String button, String color)
            throws Throwable {
        fitmentPage.assertSearchButtonColor(button, color);
    }

    @When("^I select \"(.*?)\" from the \"([^\"]*)\" dropdown$")
    public void i_select_a_dropdown_value(String value, String dropDownType)
            throws Throwable {
        WebElement dropDown = fitmentPage.getDropDown(dropDownType);
        fitmentPage.selectDropDownValue(dropDown, value);
    }

    @When("^I do a \"(homepage|my vehicles|default|CheckFit|CheckFitEdit)\" vehicle search with details \"(.*?)\" \"(.*?)\" \"(.*?)\" \"(.*?)\" \"(.*?)\"$")
    public void i_do_a_type_of_vehicle_search_with_details(String searchType, String year, String make,
                                                           String model, String trim, String assembly) throws Throwable {
        fitmentPage.fitmentGridVehicleSearch(searchType, year, make, model, trim, assembly);
        driver.scenarioData.setVehicleData(year, make, model, trim);
    }

    @And("^I select vehicle with details \"(.*?)\" \"(.*?)\" \"(.*?)\" \"(.*?)\"$")
    public void i_select_vehicle_with_details(String year, String make, String model, String trim) throws Throwable {
        fitmentPage.fitmentSelectExistingVehicle(year, make, model, trim);
    }

    @When("^I do a \"(homepage|my vehicles|default|appointment page|CheckFit|CheckFitEdit)\" vehicle search for \"([^\"]*)\" vehicle type$")
    public void i_do_a_type_of_vehicle_search_for_vehicle_type(String searchType, String vehicleType) throws Throwable {
        Vehicle veh = vehicle.getVehicle(vehicleType);
        scenario.write(veh.toString());
        fitmentPage.fitmentGridVehicleSearch(searchType, veh.year, veh.make, veh.model, veh.trim, veh.assembly);
        driver.scenarioData.setVehicleData(veh.year, veh.make, veh.model, veh.trim);
    }

    @When("^I do a \"(homepage|my vehicles)\" \"(tire|wheel)\" size search with details \"(.*?)\"$")
    public void i_do_a_product_type_size_search_with_details(String searchType, String productType, String sizeValues)
            throws Throwable {
        if (productType.equalsIgnoreCase(Constants.TIRE)) {
            productType = ConstantsDtc.TIRES;
        } else {
            productType = ConstantsDtc.WHEELS;
        }
        fitmentPage.fitmentSizeSearch(searchType, productType, sizeValues);
    }

    @And("^the dropdown \"([^\"]*)\" is visible on the page$")
    public void the_dropdown_is_visible_on_the_page(String dropDown) throws Throwable {
        fitmentPage.assertDropdownsAreVisible(dropDown);
    }

    @And("^I verify the My Vehicle \"(Vehicle Search|Tire Size Search|Wheel Size Search)\" \"([^\"]*)\" " +
            "dropdown has no decimals$")
    public void i_verify_the_my_vehicle_dropdown_has_no_decimals(String tab, String dropdownName)
            throws Throwable {
        fitmentPage.assertNoDecimalInMyVehicleDropdown(tab, dropdownName);
    }

    @And("^I select \"(.*?)\" and find products$")
    public void i_select_and_find_products(String brand) throws Throwable {
        fitmentPage.enterBrandName(brand);
        fitmentPage.clickFindTiresOrWheels();
    }

    @Then("^I verify all of the fitment menus are displayed$")
    public void i_verify_all_fitment_menus_are_displayed() throws Throwable {
        fitmentPage.verifyFitmentMenusDisplayed();
    }

    @When("^I navigate to Shop By \"(Vehicle|Brand|Size)\"$")
    public void i_navigate_to_the_shop_by_tab(String shopBy) throws Throwable {
        commonActions.clickButtonByText(shopBy);
    }

    @Then("^I verify that \"(.*?)\" dropdown arrow is pointing \"(upwards|downwards)\"$")
    public void i_verify_that_selected_dropdown_arrow_position_is(String dropdown, String position)
            throws Throwable {
        fitmentPage.assertSelectedDropDownArrowPosition(dropdown, position);
    }

    @Then("^I verify the default selection in the fitment component$")
    public void i_verify_the_default_selection_in_the_fitment_component() throws Throwable {
        fitmentPage.verifyActiveTabOnFitmentGrid(FitmentPage.SHOP_BY_VEHICLE);
    }

    @Then("^I verify Shop By Brand default placeholder text$")
    public void i_verify_shop_by_brand_placeholder_text() throws Throwable {
        fitmentPage.assertShopByBrandDefaultPlaceholderText();
    }

    @Then("^I verify wheels shop by size fields are in correct order left to right$")
    public void i_verify_wheels_shop_by_size_fields_ordering() throws Throwable {
        fitmentPage.assertWheelsShopBySizeFieldsAreInSpecificOrderLeftToRight();
    }

    @Then("^I verify \"(Year|Make|Model|Trim|Width|Ratio|Diameter|Quick Search|Advanced Search|Wheel Width|Bolt Pattern|Height)\" breadcrumb is active on the fitment grid$")
    public void i_verify_breadcrumb_active_on_fitment_grid(String breadcrumbLink) throws Throwable {
        fitmentPage.verifyBreadcrumbActiveOnFitmentGrid(breadcrumbLink);
    }

    @And("^I verify \"(\\d{1,2})\" years are displaying numerically in \"(ascending|descending)\" order$")
    public void i_verify_number_of_years_display_in_order(int yearCount, String order) throws Throwable {
        fitmentPage.verifyNumberOfOptionsDisplayed(yearCount);
        fitmentPage.verifySortOrderOfOptionsDisplayed(order);
    }

    @And("^I verify option \"(.*?)\" is \"(displayed|not displayed)\" on the fitment grid$")
    public void i_verify_option_display_on_fitment_grid(String optionToVerify, String displayCheck) throws Throwable {
        fitmentPage.verifyOptionDisplayOnFitmentGrid(optionToVerify, displayCheck);
    }

    @When("^I select \"(Show more|Show all)\" on the vehicle grid$")
    public void i_select_show_more_all_on_vehicle_grid(String selection) throws Throwable {
        initialOptionCount = fitmentPage.getCurrentFitmentGridOptionCount();
        fitmentPage.selectShowMoreShowAllOnVehicleGrid(selection);
    }

    @When("^I select the \"(.*?)\" fitment grid option$")
    public void i_select_fitment_grid_option(String gridOption) throws Throwable {
        fitmentPage.selectFitmentGridOption(true, gridOption);
    }

    @When("^I select the \"(standard|non-standard)\" fitment grid option: \"(.*?)\"$")
    public void i_select_type_of_fitment_grid_option(String standardType, String option) throws Throwable {
        if (standardType.equalsIgnoreCase(Constants.STANDARD)) {
            fitmentPage.selectFitmentGridOption(true, option);
        } else {
            fitmentPage.selectFitmentGridOption(false, option);
        }
    }

    @Then("^I verify up to an additional \"(\\d+)\" fitment grid options are displayed$")
    public void i_verify_additional_number_of_fitment_grid_options_are_displayed(int optionLimit) throws Throwable {
        fitmentPage.verifyAdditionalNumberOfFitmentGridOptionsAreDisplayed(initialOptionCount, optionLimit);
    }

    @Then("^I verify all available fitment grid options for the current context are displayed")
    public void i_verify_all_available_fitment_grid_options_for_current_context_are_displayed() throws Throwable {
        fitmentPage.verifyAllFitmentGridOptionsForCurrentContextAreDisplayed();
    }

    @Then("^I verify \"(Year|Make|Model|Trim|Width|Ratio|Diameter|Height|Wheel Width|Bolt Pattern|Advanced Search)\" fitment breadcrumb is highlighted with orange color$")
    public void i_verify_fitment_breadcrumb_highlighted_orange(String breadcrumbLinkText) throws Throwable {
        fitmentPage.verifyFitmentBreadcrumbHighlightOrange(breadcrumbLinkText);
    }

    @And("^I verify \"(.*?)\" is displayed below the fitment breadcrumb menu links$")
    public void i_verify_selection_displayed_below_fitment_breadcrumb_menu_links(String selectionToVerify) throws
            Throwable {
        fitmentPage.verifySelectionDisplayedBelowFitmentBreadcrumbMenuLinks(selectionToVerify);
    }

    @And("^I verify the current fitment grid options are displaying in \"(ascending|descending)\" order$")
    public void i_verify_current_fitment_grid_options_display_in_order(String order) throws Throwable {
        fitmentPage.verifySortOrderOfOptionsDisplayed(order);
    }

    @And("^I verify the verbiage for the \"(multiple size options|tire sidewall|add vehicle|recent tire sizes|recent wheel sizes|Add New Vehicle)\" section is displayed in the fitment grid$")
    public void i_verify_verbiage_for_section_displayed_in_fitment_grid(String section) throws Throwable {
        fitmentPage.verifyVerbiageForSectionIsDisplayedInFitmentGrid(section);
    }

    @And("^I verify the 'tire sidewall help' image is displayed$")
    public void i_verify_tire_sidewall_help_image_displayed() throws Throwable {
        fitmentPage.verifyTireSidewallHelpImageDisplayed();
    }

    @Then("^I verify my vehicle details: \"(.*?)\" are \"(not displayed|displayed)\" in the 'My Vehicles' section of the fitment grid$")
    public void i_verify_my_vehicle_details_display_in_my_vehicles_section_of_fitment_grid
            (String vehicleDetails, String displayCheck) throws Throwable {
        fitmentPage.verifyMyVehicleDetailsDisplayInMyVehiclesSectionOfFitmentGrid(vehicleDetails, displayCheck);
    }

    @Then("^I verify the \"(Vehicle|Size|Brand)\" tab is active on the fitment grid$")
    public void i_verify_tab_active_on_fitment_grid(String tabName) throws Throwable {
        fitmentPage.verifyActiveTabOnFitmentGrid(tabName);
    }

    @Then("^I verify the \"(.*?)\" button displays with a red background$")
    public void i_verify_button_displays_with_red_background(String buttonText) throws Throwable {
        fitmentPage.verifyButtonDisplaysWithRedBackground(buttonText);
    }

    @Then("^I verify 'My Vehicles' section displays only \"(\\d)\" vehicles on \"(My Vehicles modal|Homepage)\"$")
    public void i_verify_my_vehicles_section_displays_max_number_of_vehicles(int currentMax, String page) throws Throwable {
        fitmentPage.verifyMyVehiclesSectionDisplaysMaxNumberOfVehicles(currentMax, page);
    }

    @And("^I verify the \"(TIRES|WHEELS)\" subheader is active on the fitment grid$")
    public void i_verify_subheader_active_on_fitment_grid(String subheader) throws Throwable {
        fitmentPage.verifySubheaderActiveOnFitmentGrid(subheader);
    }

    @And("^I verify \"(\\d{1,2})\" fitment grid options are displayed$")
    public void i_verify_number_of_fitment_grid_options_displayed(int expectedNumber) throws Throwable {
        fitmentPage.verifyNumberOfOptionsDisplayed(expectedNumber);
    }

    @When("^I remove recently selected option \"(.*?)\" from the fitment grid$")
    public void i_remove_recently_selected_option_from_fitment_grid(String option) throws Throwable {
        fitmentPage.removeRecentlySelectedOptionFromFitmentGrid(option);
    }

    @Then("^I verify 'High flotation' tire sizes are displayed below the 'Metric' tire sizes$")
    public void i_verify_high_flotation_tires_display_below_metric_tires() throws Throwable {
        fitmentPage.verifyHighFlotationTiresDisplayBelowMetricTires();
    }

    @And("^I verify 'High flotation' tire sizes are displayed in \"(ascending|descending)\" order$")
    public void i_verify_high_flotation_tire_sizes_display_in_order(String order) throws Throwable {
        fitmentPage.verifySortOrderOfOptionsDisplayed(order, false);
    }

    @When("^I do a \"(homepage|my vehicle)\" \"(?:Brand|brand)\" \"(tire|wheel)\" search with option: \"(.*?)\"$")
    public void i_do_a_brand_product_type_search_with_option(String searchType, String productType, String brandOption)
            throws Throwable {
        fitmentPage.fitmentBrandSearch(searchType, productType, brandOption);
    }

    @Then("^I verify breadcrumbs: \"(.*?)\" are displayed on the fitment grid")
    public void i_verify_breadcrumbs_display_on_fitment_grid(String breadcrumbs) throws Throwable {
        fitmentPage.verifyBreadcrumbsDisplayOnFitmentGrid(breadcrumbs);
    }

    @And("^I select the \"(Tires|Wheels|Quick Search|Advanced Search)\" subheader on the fitment grid$")
    public void i_select_subheader_on_fitment_grid(String subheader) throws Throwable {
        fitmentPage.selectSubheaderOnFitmentGrid(subheader);
    }

    @And("^I verify recently selected option \"(.*?)\" is \"(displayed|not displayed)\" on the fitment grid$")
    public void i_verify_recently_selected_option_on_fitment_grid(String option, String displayCheck) throws Throwable {
        fitmentPage.verifyRecentlySelectedOptionDisplayOnFitmentGrid(option, displayCheck);
    }

    @Then("^I verify that no results have \"([^\"]*)\" on the fitment grid$")
    public void i_verify_that_no_results_have_on_the_fitment_grid(String value) throws Throwable {
        fitmentPage.verifyNoFitmentGridResultsContainValue(value);
    }

    @And("^I remove all vehicles on session from fitment section$")
    public void i_remove_all_vehicles_on_session_from_fitment_section() throws Throwable {
        fitmentPage.clickRemoveRecentVehicleButton();
    }

    @When("^I select a fitment option \"(.*?)\"$")
    public void i_select_a_fitment_option(String fitment) throws Throwable {
        if (CommonUtils.containsIgnoreCase(fitment, Constants.TIRE) ||
                CommonUtils.containsIgnoreCase(fitment, Constants.WHEEL) ||
                CommonUtils.containsIgnoreCase(fitment, Constants.PACKAGE) ||
                CommonUtils.containsIgnoreCase(fitment, Constants.ACCESSORIES)) {
            fitmentPage.selectFitmentShoppingCategory(fitment);
        } else {
            fitmentPopupPage.selectFitment(fitment);
        }
    }

    @When("^I select a vehicle with fitment details \"(.*?)\" \"(.*?)\" \"(.*?)\" \"(.*?)\"$")
    public void i_select_a_vehicle_with_fitment_details(String year, String make, String modal, String trim) throws Throwable {
        fitmentPage.selectVehicleWithFitmentDetails(year, make, modal, trim);
    }

    @Then("^I verify all years starting from \"(.*?)\" is displayed$")
    public void i_verify_all_years_starting_from_specified_year_is_displayed(String searchYear) throws Throwable {
        fitmentPage.assertYearDisplayed(searchYear);
    }

    @Then("^I verify error message \"(.*?)\" is displayed$")
    public void i_verify_error_message_is_displayed(String errorMessage) throws Throwable {
        fitmentPage.assertErrorMessageDisplayedOnFitmentModal(errorMessage);
    }

    @When("^I select back to my vehicles button$")
    public void i_select_back_to_my_vehicles_button() throws Throwable {
        driver.getElementWithText(FitmentPopupPage.backToMyVehiclesBy, FitmentPage.BACK_TO_MY_VEHICLES_BUTTON_TEXT).click();
        driver.waitForPageToLoad();
    }

    @When("^I click on \"(view tires|view wheels)\" button on empty cart page$")
    public void i_click_on_button_on_empty_cart_page(String text) throws Throwable {
        fitmentPage.clickOnButtonOnEmptyCartPage(text);
    }

    @When("^I select edit vehicle link$")
    public void i_select_edit_vehicle_link() throws Throwable {
        commonActions.clickElementWithLinkText(ConstantsDtc.EDIT_VEHICLE.toLowerCase());
    }

    @When("^I select \"(Treadwell|On Promotion|Best Seller|Highest Rated|Original Equipment)\" option$")
    public void i_select_tire_option(String option) throws Throwable {
        if (option.equalsIgnoreCase(ConstantsDtc.TREADWELL)) {
            fitmentPage.selectTreadwell();
        } else {
            commonActions.clickElementWithLinkText(option);
        }
    }

    @When("^I should see the fitment panel page with fitment options$")
    public void i_should_see_the_fitment_panel_page_with_fitment_options() throws Throwable {
        fitmentPage.assertAllTireOptions();
    }

    @When("^I select vehicle at position \"(.*?)\" on fitment modal$")
    public void i_select_vehicle_at_position_on_fitment_modal(int position) throws Throwable {
        fitmentPage.selectVehicleAtPosition(position);
    }

    @When("^I select a fitment option \"(tire|wheel)\" and shop for all tires/wheels$")
    public void i_select_a_fitment_option_and_shop_for_all_tires(String fitment) throws Throwable {
        i_select_a_fitment_option(fitment);
        if (driver.isElementDisplayed(CommonActions.shopAllTiresButton))
            CommonActions.shopAllTiresButton.click();
        commonActions.clickContinueWithThisVehicleButton();
    }

    @And("^I should see the fitment panel is \"(displayed|not displayed)\"$")
    public void i_should_see_the_fitment_panel(String displayStatus) throws Throwable {
        fitmentPage.assertFitmentPanelDisplayedNotDisplayed(displayStatus);
    }
}