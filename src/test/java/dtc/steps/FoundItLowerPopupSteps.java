package dtc.steps;

import common.Constants;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.ConstantsDtc;
import dtc.pages.CommonActions;
import dtc.pages.FoundItLowerPopupPage;
import dtc.pages.MyAccountPage;
import utilities.Driver;

public class FoundItLowerPopupSteps {

    private MyAccountPage myAccountPage;
    private FoundItLowerPopupPage foundItLowerPopupPage;
    private CommonActions commonActions;

    public FoundItLowerPopupSteps(Driver driver) {
        myAccountPage = new MyAccountPage(driver);
        commonActions = new CommonActions(driver);
        foundItLowerPopupPage = new FoundItLowerPopupPage(driver);
    }

    @Then("^I verify \"([^\"]*)\" modal is displayed$")
    public void i_verify_found_it_lower_modal_is_displayed(String modalName) throws Throwable {
        foundItLowerPopupPage.assertModalIsDisplayed(modalName);
    }

    @Then("^I verify the success modal is displayed$")
    public void i_verify_found_it_lower_modal_success_message_is_displayed() throws Throwable {
        foundItLowerPopupPage.verifyWebElementContainsText(FoundItLowerPopupPage.FOUND_IT_LOWER_MODAL_SUCCESS,
                Constants.SUCCESS);
    }

    @When("^I enter \"(.*?)\" into the \"(.*?)\" field$")
    public void i_enter_into_the_field(String value, String field) throws Throwable {
        foundItLowerPopupPage.enterIntoField(value, field);
    }

    @Then("^I verify modal title is \"([^\"]*)\"$")
    public void i_verify_modal_title(String title) throws Throwable {
        foundItLowerPopupPage.verifyWebElementContainsText(FoundItLowerPopupPage.FOUND_IT_LOWER_HEADER, title);
    }

    @Then("^I verify modal form text$")
    public void i_verify_modal_form_text() throws Throwable {
        foundItLowerPopupPage.verifyWebElementContainsText(FoundItLowerPopupPage.FOUND_IT_LOWER_FORM_TEXT,
                FoundItLowerPopupPage.FORM_TEXT);
    }

    @Then("^I verify modal footer text$")
    public void i_verify_modal_footer_text() throws Throwable {
        foundItLowerPopupPage.verifyWebElementContainsText(FoundItLowerPopupPage.FOUND_IT_LOWER_FOOTER,
                FoundItLowerPopupPage.FOOTER_TEXT);
    }

    @Then("^I verify the details displayed on modal for product$")
    public void i_verify_the_details_displayed_on_modal_for_product() throws Throwable {
        foundItLowerPopupPage.verifyWebElementContainsText(FoundItLowerPopupPage.FOUND_IT_LOWER_BRAND,
                commonActions.productInfoListGetValue(ConstantsDtc.BRAND));
        foundItLowerPopupPage.verifyWebElementContainsText(FoundItLowerPopupPage.FOUND_IT_LOWER_PRODUCT_NAME,
                commonActions.productInfoListGetValue(ConstantsDtc.PRODUCT));
        foundItLowerPopupPage.verifyWebElementContainsText(FoundItLowerPopupPage.FOUND_IT_LOWER_PRODUCT_PRICE,
                commonActions.productInfoListGetValue(Constants.PRICE));
    }
}
