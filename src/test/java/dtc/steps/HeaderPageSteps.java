package dtc.steps;

import common.Config;
import common.Constants;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.ConstantsDtc;
import dtc.pages.HeaderPage;
import dtc.pages.MobileHeaderPage;
import utilities.Driver;

/**
 * Created by aaronbriel on 10/26/16.
 */
public class HeaderPageSteps {

    private HeaderPage headerNavigationPage;
    private MobileHeaderPage mobileHeaderPage;

    public HeaderPageSteps(Driver driver) {
        headerNavigationPage = new HeaderPage(driver);
        mobileHeaderPage = new MobileHeaderPage(driver);
    }

    @When("^I open the \"(TIRES|WHEELS|APPOINTMENTS|SERVICES|TIPS & GUIDES|FINANCING|PROMOTIONS|Installers|[^\"]*)\" navigation link")
    public void i_open_the_navigation_link(String navigationLink) throws Throwable {
        if (Config.isMobile()) {
            mobileHeaderPage.openMobileMenu();
            if (navigationLink.toLowerCase().contains(ConstantsDtc.INSTALLERS.toLowerCase())) {
                navigationLink = MobileHeaderPage.FIND_AN_INSTALLER;
            }
            mobileHeaderPage.clickMenuLink(navigationLink);
        } else {
            headerNavigationPage.clickNavigationOption(navigationLink);
        }
    }

    @And("^I click the \"([^\"]*)\" menu option")
    public void i_click_the_menu_option(String menuOption) throws Throwable {
        headerNavigationPage.clickNavigationMenuOption(menuOption);
    }

    @Then("^I click the \"([^\"]*)\" View All link in the header$")
    public void i_click_the_view_all_link_in_the_header(String section) throws Throwable {
        headerNavigationPage.clickViewAllNavigationOption(section);
    }
    
    @When("^I verify the \"(TIRES|WHEELS|APPOINTMENTS|SERVICES|TIPS & GUIDES|FINANCING|PROMOTIONS)\" navigation link is displayed")
    public void i_verify_the_navigation_link_is_displayed(String navigationLink) throws Throwable {
        headerNavigationPage.assertNavigationOptionIsDisplayed(navigationLink);
    }
}