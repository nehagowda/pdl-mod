package dtc.steps;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.pages.MobileHeaderPage;
import utilities.Driver;

/**
 * Created by aaronbriel on 1/20/17.
 */
public class MobileHeaderPageSteps {

    private MobileHeaderPage mobileHeaderPage;

    public MobileHeaderPageSteps(Driver driver) {
        mobileHeaderPage = new MobileHeaderPage(driver);
    }

    @When("^I click on \"([^\"]*)\" menu link$")
    public void i_click_on_link(String link) throws Throwable {
        mobileHeaderPage.clickMenuLink(link);
    }

    @When("^I click on \"([^\"]*)\" header link$")
    public void i_click_on_header_link(String link) throws Throwable {
        mobileHeaderPage.clickHeaderLink(link);
    }

    @Then("^I verify all of the mobile header elements are visible")
    public void i_verify_mobile_header_elements()
            throws Throwable {
        mobileHeaderPage.verifyMobileHeaderElements();
    }

    @When("^I click the mobile homepage menu$")
    public void i_click_the_mobile_homepage_menu() throws Throwable {
        mobileHeaderPage.openMobileMenu();
    }

    @When("^I click the discount tire logo in the mobile header$")
    public void i_click_the_dt_logo_in_mobile_header() throws Throwable {
        mobileHeaderPage.clickMobileSiteLogo();
    }

    @Then("^I am brought to the mobile page with header \"([^\"]*)\"$")
    public void i_am_brought_to_the_mobile_page_with_header(String pageTitle) throws Throwable {
        mobileHeaderPage.assertPageH1Header(pageTitle);
    }

    @Then("^I verify that My Vehicles displays \"([^\"]*)\" as the current vehicle in the mobile menu$")
    public void i_verify_that_my_vehicles_displays_as_the_current_vehicle_in_the_mobile_menu(String vehicle) throws Throwable {
        mobileHeaderPage.assertMyVehicles(vehicle);
    }

    @Then("^I verify My Vehicles displays add vehicle in the mobile menu$")
    public void i_verify_my_vehicles_displays_add_vehicle_in_the_mobile_menu() throws Throwable {
        mobileHeaderPage.assertAddVehicle();
    }

    @When("^I select Add Vehicle in the mobile menu$")
    public void i_select_add_vehicle_in_the_mobile_menu() throws Throwable {
        mobileHeaderPage.clickAddVehicle();
    }
}
