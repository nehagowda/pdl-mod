package dtc.steps;

import common.Constants;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.pages.MyVehiclesPopupPage;
import utilities.Driver;

/**
 * Created by aaronbriel on 4/27/17.
 */
public class MyVehiclesPopupPageSteps {

    private MyVehiclesPopupPage myVehiclesPopupPage;

    public MyVehiclesPopupPageSteps(Driver driver) {
        myVehiclesPopupPage = new MyVehiclesPopupPage(driver);
    }

    @Then("^I verify that My Vehicles displays \"(.*?)\" as the current vehicle$")
    public void i_verify_current_vehicle(String vehicle) throws Throwable {
        myVehiclesPopupPage.assertCurrentVehicleIsSelected(vehicle);
    }

    @Then("^I verify that My Vehicles displays the vehicle limit reached message$")
    public void i_verify_vehicle_limit_message() throws Throwable {
        myVehiclesPopupPage.assertVehicleLimitMessage();
    }

    @Then("^I select recent vehicle \"(.*?)\"$")
    public void i_click_recent_vehicle(String vehicle) throws Throwable {
        myVehiclesPopupPage.clickRecentVehicle(vehicle);
    }

    @Then("^I verify \"(default|treadwell)\" My Vehicles popup displays add vehicle$")
    public void i_verify_my_vehicle_popup_displays_add_vehicle(String option) throws Throwable {
        myVehiclesPopupPage.assertAddVehicle(option);
    }

    @When("^I select current Vehicle$")
    public void i_select_current_vehicle() throws Throwable {
        myVehiclesPopupPage.clickCurrentVehicle();
    }

    @Then("^I verify that My Vehicles displays \"([^\"]*)\" in the header$")
    public void i_verify_my_vehicles_display_in_the_header(String vehicle) throws Throwable {
        myVehiclesPopupPage.assertMyVehicles(vehicle);
    }

    @Then("^I verify recent vehicle \"(.*?)\" \"(displayed|not displayed)\"$")
    public void i_verify_recent_vehicle_display(String vehicle, String displayed) throws Throwable {
        boolean isDisplayed = false;
        if (displayed.equalsIgnoreCase(Constants.DISPLAYED))
            isDisplayed = true;
        myVehiclesPopupPage.assertRecentVehicleDisplay(vehicle, isDisplayed);
    }

    @When("^I select Add Vehicle$")
    public void i_select_add_vehicle() throws Throwable {
        myVehiclesPopupPage.clickAddVehicle();
    }

    @Then("^I verify the add vehicle button is clickable and red$")
    public void i_verify_the_add_vehicle_button_is_clickable_and_red() throws Throwable {
        myVehiclesPopupPage.verifyAddVehicleButtonProperties();
    }

    @And("^I verify \"(Regular|Staggered)\" Vehicle Details on \"My Vehicles\" page contains \"(OE Tire|OE FRONT Tire|OE REAR Tire|OE Wheel|OE FRONT Wheel|OE REAR Wheel|Non-OE Tire|Non-OE FRONT Wheel|Non-OE REAR Wheel|Non-OE Wheel|FRONT Tire|FRONT Wheel)\" fitment banner with \"(.*?)\" and \"(.*?)\"$")
    public void i_verify_the_vehicle_details_on_my_vehicles(String vehicle, String type, String sizeBadge, String text) throws Throwable {
        myVehiclesPopupPage.assertMyVehiclesModalVehicleDetails(vehicle, type, sizeBadge, text);
    }

    @And("^I select Clear Recent Searches and \"(Yes, Clear Recent Searches|No, Keep These Searches|Cancel & Keep Tires|Continue Building)\" option$")
    public void i_select_clear_recent_searches(String option) throws Throwable {
        myVehiclesPopupPage.clickClearRecentSearches(option);
    }

    @Then("^I verify \"([^\"]*)\" element displayed$")
    public void i_verify_is_element_displayed(String elementName) throws Throwable {
        myVehiclesPopupPage.verifyIsElementDisplayed(elementName);
    }

    @Then("^I verify vehicle \"(.*?)\" \"(displayed|not displayed)\" in \"(Saved Vehicles|Recent Searches)\" section$")
    public void i_verify_vehicle_display(String vehicle, String displayed, String savedOrRecent) throws Throwable {
        boolean isDisplayed = false;
        if (displayed.equalsIgnoreCase(Constants.DISPLAYED))
            isDisplayed = true;
        myVehiclesPopupPage.assertRecentOrSavedVehicleDisplay(vehicle, isDisplayed, savedOrRecent);
    }

    @Then("^I verify \"(.*?)\" \"(.*?)\" \"(.*?)\" \"(.*?)\" vehicle is \"(present|absent)\" in the saved vehicles at position \"(.*?)\"$")
    public void i_verify_vehicle_in_the_saved_vehicles_at_position(String year, String make,
                                                                   String model, String trim, String isPresent,
                                                                   String position) throws Throwable {
        myVehiclesPopupPage.verifyVehiclePresentAtPosition(year, make, model, trim, isPresent, position);
    }

    @When("^I select \"(shop tires|shop wheels)\" for \"(.*?)\" \"(.*?)\" vehicle$")
    public void i_select_shop_tires_or_shop_wheels_for_vehicle(String option, String year, String make) throws Throwable {
        myVehiclesPopupPage.selectShopTiresShopWheels(option, year, make);
    }
}