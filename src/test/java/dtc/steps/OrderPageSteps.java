package dtc.steps;

import common.Config;
import common.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import dtc.data.ConstantsDtc;
import dtc.data.Customer;
import dtc.data.Product;
import dtc.pages.CommonActions;
import dtc.pages.HomePage;
import dtc.pages.MyAccountPage;
import dtc.pages.OrderPage;
import common.CommonExcel;
import orderxmls.pages.OrderXmls;
import org.openqa.selenium.WebElement;
import utilities.CommonUtils;
import utilities.Driver;
import webservices.pages.WebServices;

import java.text.DecimalFormat;
import java.util.WeakHashMap;

/**
 * Created by aaronbriel on 10/24/16.
 */
public class OrderPageSteps {

    private OrderPage orderPage;
    private CartPageSteps cartPageSteps;
    private Customer customer;
    private CommonExcel commonExcel;
    private Product product;
    private Scenario scenario;
    private MyAccountPage myAccountPage;
    private CommonActions commonActions;
    private Driver driver;

    public OrderPageSteps(Driver driver) {
        orderPage = new OrderPage(driver);
        cartPageSteps = new CartPageSteps(driver);
        customer = new Customer();
        commonExcel = new CommonExcel();
        myAccountPage = new MyAccountPage(driver);
        commonActions = new CommonActions(driver);
        product = new Product();
        this.driver = driver;
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @Then("^I am brought to the order confirmation page$")
    public void i_am_brought_to_the_order_confirmation_page() throws Throwable {
        if (commonActions.paymentSystemDownMessageExists(scenario))
            return;
        orderPage.assertOrderMessage(ConstantsDtc.ORDER_CONFIRMATION);
        i_store_the_order_number();

        commonActions.addCustomerTypeScenarioGenericData();
        scenario.write("Order Created # " + driver.scenarioData.getCurrentOrderNumber() + " for " +
                driver.scenarioData.genericData.get(Constants.CUSTOMER_TYPE) + " customer type");
    }

    @Then("^I should see reservation confirmation message with details \"(.*?)\" and \"(.*?)\"$")
    public void i_should_see_reservation_confirmation_message(String productName, String itemCode) throws Throwable {
        if (commonActions.paymentSystemDownMessageExists(scenario))
            return;
        orderPage.assertOrderMessage(ConstantsDtc.ORDER_CONFIRMATION);
        if (productName.startsWith(ConstantsDtc.PRODUCT.toUpperCase())) {
            productName = product.getProduct(productName).productName;
            scenario.write("Product: '" + product + "'");
        }
        if (itemCode.startsWith(ConstantsDtc.PRODUCT.toUpperCase())) {
            itemCode = product.getProduct(itemCode).productCode;
            scenario.write("Item Code: '" + itemCode + "'");
        }
        orderPage.assertItemOnOrderConfirmationPage(productName, itemCode);
    }

    @And("^I should see reservation confirmation message with expected product name and item code$")
    public void i_should_see_reservation_confirmation_message_with_product_name_and_item_code() throws Throwable {
        String productName = "";
        String itemCode = "";
        int productCount = driver.scenarioData.productInfoList.size();
        for (int index = 0; index < productCount; index++) {
            if (Boolean.parseBoolean(commonActions.productInfoListGetValue(ConstantsDtc.IN_CART, index))) {
                productName = commonActions.productInfoListGetValue(ConstantsDtc.PRODUCT, index);
                itemCode = commonActions.productInfoListGetValue(ConstantsDtc.ITEM, index);
                i_should_see_reservation_confirmation_message(productName, itemCode);
            }
        }
    }

    @And("^I store the order number$")
    public void i_store_the_order_number() throws Throwable {
        orderPage.storeOrderNumber();
    }

    @And("^I store the total amount$")
    public void i_store_the_total_amount() throws Throwable {
        orderPage.storeTotalAmount();
    }

    @Then("^I verify the Total price on the order confirmation page$")
    public void i_verify_the_total_price_on_the_order_confirmation_page() throws Throwable {
        orderPage.assertOrderTotal(CartPageSteps.orderTotalPrice);
    }

    @And("^I verify \"(.*?)\" is listed on the order confirmation page$")
    public void i_verify_text_on_the_order_confirmation_page(String text) throws Throwable {
        orderPage.assertTextOnOrderConfirmationPage(text);
    }

    @And("^I expand the fee details for the item listed on the order confirmation page$")
    public void i_expand_fee_details_for_item_listed_on_order_confirmation_page() throws Throwable {
        orderPage.expandFeeDetailsForItem();
    }

    @And("^I verify the order total on order confirmation page matches with \"(cart|checkout)\" order total$")
    public void i_verify_the_order_total_on_order_confirmation_page(String text) throws Throwable {
        if (text.equalsIgnoreCase(Constants.CART)) {
            orderPage.assertOrderConfirmationAndCartOrderTotal();
        } else {
            orderPage.assertOrderConfirmationAndCheckoutOrderTotal();
        }
    }

    @And("^I verify the sales tax on order confirmation page matches with \"(cart|checkout)\" sales tax for \"(.*?)\"$")
    public void i_verify_the_sales_tax_on_order_confirmation_page(String text, String customer) throws Throwable {
        if (text.equalsIgnoreCase(Constants.CART)) {
            orderPage.assertOrderConfirmationAndCartSalesTax(customer);
        } else {
            orderPage.assertOrderConfirmationAndCheckoutSalesTax(customer);
        }
    }

    @And("^I verify customer \"(.*?)\" details are listed on the \"(order confirmation|Order History Detail)\" page$")
    public void i_verify_customer_details_are_listed_on_the_order_confirmation_page(String customerType, String page) throws Throwable {
        Customer customerObject = customer.getCustomer(customerType);
        orderPage.verifyCustomerDetailsOnOrderConfirmation(customerObject, page);
    }

    @And("^I verify customer \"(.*?)\" email confirmation message on the order confirmation page$")
    public void i_verify_customer_email_confirmation_message_on_the_order_confirmation_page(String customerType) throws Throwable {
        Customer customerObject = customer.getCustomer(customerType);
        orderPage.verifyCustomerEmailMessageOnOrderConfirmation(customerObject);
    }

    @And("^I select survey feedback on order confirmation page$")
    public void i_select_survey_feedback_link_on_the_order_confirmation_page() throws Throwable {
        orderPage.clickSurveyLink();
    }

    @Then("^I verify the Environment Fee details on the order confirmation page$")
    public void i_verify_the_environment_fee_details_on_the_order_confirmation_page() throws Throwable {
        orderPage.verifyEnvironmentFeeDetailsOnOrderConfirmation();
    }

    @Then("^I save the order number to the \"([^\"]*)\" excel with EA flag \"([^\"]*)\"$")
    public void i_save_the_order_number_to_the_excel_with_ea_flag(String file, String flag) throws Throwable {
        orderPage.storeDtcOrderNumberEAFlagToExcel(file, flag);
    }

    @And("^I verify selected TPMS option \"(TPMS Rebuild Kits|TPMS Sensors|Valve Stems)\" is displayed on order confirmation page$")
    public void i_verify_selected_tpms_option_is_displayed_on_order_confirmation_page(String optionText) throws Throwable {
        orderPage.verifySelectedTPMSOptionDisplayed(optionText);
    }

    @And("^I save order number from order confirmation page in a list$")
    public void i_save_order_number_from_order_confirmation_page_in_a_list() throws Throwable {
        orderPage.saveOrderNumbersFromOrderConfirmationPageInList();
    }

    @And("^I verify order number from order confirmation page with order history detail page$")
    public void i_verify_order_number_from_order_confirmation_page_with_order_history_detail_page() throws Throwable {
        orderPage.assertOrderNumberFromOrderConfirmationWithOrderDetailsPage();
    }

    @Then("^I verify \"(.*?)\" is displayed on order confirmation page$")
    public void i_verify_email_is_displayed_on_order_confirmation_page(String text) throws Throwable {
        if (commonActions.paymentSystemDownMessageExists(scenario))
            return;
        commonActions.assertElementWithTextIsVisible(OrderPage.orderConfirmationMessage, text);
    }

    @Then("^I extract and save the sales tax for \"([^\"]*)\" on order confirmation page$")
    public void i_extract_and_save_sales_tax_on_order_confirmation_page(String customer) throws Throwable {
        driver.scenarioData.genericData.put(ConstantsDtc.TAX, new DecimalFormat(".##").
                format(orderPage.extractSalesTaxOnOrderConfirmation(customer)));
    }

    @Then("^I verify \"([^\"]*)\" email address displayed on order confirmation page$")
    public void i_verify_my_email_address_displayed_on_order_confirmation_page(String emailAddress) throws Throwable {
        if (emailAddress.equalsIgnoreCase(ConstantsDtc.MY_ACCOUNT))
            emailAddress = driver.scenarioData.genericData.get(WebServices.MY_ACCOUNT_EMAIL_ID);
        orderPage.verifyEmailAddressOnOrderConfirmationPage(emailAddress);
    }

    @And("^I am brought to the order \"(cancellation request|cancellation complete)\" page$")
    public void i_am_brought_to_the_order_cancellation_page(String text) throws Throwable {
        if (text.equalsIgnoreCase(OrderPage.CANCELLATION_COMPLETE)) {
            orderPage.assertOrderMessage(OrderPage.CANCELLATION_COMPLETE + " - Order No. "
                    + driver.scenarioData.getCurrentOrderNumber());
        } else {
            orderPage.assertOrderMessage(OrderPage.ORDER_CANCELLATION_REQUEST);
        }
    }

    @And("^I select cancel order$")
    public void i_select_cancel_order() throws Throwable {
        commonActions.clickFormSubmitButtonByText(OrderPage.CANCEL_ORDER);
    }

    @Then("^I am brought to the page \"(Your appointment is confirmed)\"$")
    public void i_am_brought_to_the_page_your_appointment_is_confirmed(String text) throws Throwable {
        orderPage.assertOrderMessage(text);
    }

    @And("^I save paypal order details$")
    public void i_save_order_reference_number() throws Throwable {
        driver.jsScrollToElement(OrderPage.sellersTransaction);
        String strPattern = "^0+";
        String sellersTxnNumber = OrderPage.sellersTransaction.getText().split(": ")[1].trim()
                .replaceAll(strPattern, "");
        driver.scenarioData.genericData.put(OrderXmls.CARD_TOKEN, sellersTxnNumber);
        driver.scenarioData.genericData.put(OrderXmls.REFERENCE_NUMBER, sellersTxnNumber);
        driver.scenarioData.genericData.put(OrderXmls.PAYMENT_TYPE, ConstantsDtc.PAYPAL);
    }
}
