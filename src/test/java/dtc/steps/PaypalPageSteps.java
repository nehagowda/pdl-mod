package dtc.steps;

import common.Config;
import common.Constants;
import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import dtc.data.Customer;
import dtc.pages.PaypalPage;
import utilities.Driver;

/**
 * Created by aaronbriel on 9/17/16.
 */
public class PaypalPageSteps {

    private PaypalPage paypalPage;
    private Customer customer;

    public PaypalPageSteps(Driver driver) {
        paypalPage = new PaypalPage(driver);
        customer = new Customer();
    }

    @When("^I log into paypal as \"(.*?)\"$")
    public void i_enter_paypal_info(String customerType) throws Throwable {
        Customer paypalCust;
        if (Config.getDataSet().equalsIgnoreCase(Constants.STG)
                && Config.getSiteRegion().equalsIgnoreCase(Constants.DTD)) {
            paypalCust = customer.getCustomer(Customer.CustomerType.PAYPAL_CUSTOMER_OH.toString());
            paypalPage.login(paypalCust.nexusEmail, paypalCust.paypalPass);
        } else {
            paypalCust = customer.getCustomer(customerType);
            paypalPage.login(paypalCust.paypalUser, paypalCust.paypalPass);
        }
    }

    @And("^I continue with the paypal payment$")
    public void i_continue_with_paypal_payment() throws Throwable {
        paypalPage.clickContinue();
    }
}
