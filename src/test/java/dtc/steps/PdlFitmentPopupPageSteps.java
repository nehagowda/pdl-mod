package dtc.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.pages.FitmentPopupPage;
import dtc.pages.PdlxFitmentPopupPage;
import utilities.Driver;

/**
 * Created by aarora on 01/17/18.
 */
public class PdlFitmentPopupPageSteps {

    private PdlxFitmentPopupPage pdlFitmentPopupPage;
    private FitmentPopupPage fitmentPopupPage;

    public PdlFitmentPopupPageSteps(Driver driver) {
    	pdlFitmentPopupPage = new PdlxFitmentPopupPage(driver);
    	fitmentPopupPage = new FitmentPopupPage(driver);
    }

    @Then("^I should see the pdlx fitment panel page with vehicle \"(.*?)\"$")
    public void i_should_see_pdlx_fitment_panel_page_with_vehicle(String vehicle) throws Throwable {
    	fitmentPopupPage.assertVehicleInPanel(vehicle);
}

    @When("^I select \"(Everyday|Performance)\" as my driving priority$")
    public void i_select_a_driving_priority(String priority) throws Throwable {
    	pdlFitmentPopupPage.selectDrivingPriority(priority.toUpperCase());
    }
    
    @When("^I select view recommended tires$")
    public void i_select_view_recommended_tires() throws Throwable {
    	pdlFitmentPopupPage.selectViewRecommendedTiresButton();;
    }

    @When("^I set the miles per year to \"(.*?)\"$")
    public void i_set_the_miles_driven_per_year_to(String miles) throws Throwable {
    	pdlFitmentPopupPage.setMiles(miles);
    }

    @Then("^I verify treadwell logo is displayed with title and subtitle on \"(PLP|PDP|Compare Products|treadwell modal|Fitment Modal)\"$")
    public void i_verify_treadwell_logo_with_title_and_subtitle_is_displayed(String page) throws Throwable {
        pdlFitmentPopupPage.assertTreadwellLogoTitleAndSubtitle(page);
    }

    @Then("^I verify the selected treadwell fitment tire size value \"([^\"]*)\" is displayed$")
    public void i_verify_treadwell_fitment_tireSize_has_a_value_of(String value) throws Throwable {
        pdlFitmentPopupPage.assertSelectedPdlFitmentTireSizeValue(value);
    }

    @Then("^I verify the miles driven per year has a value of \"([^\"]*)\" on treadwell customer preference model$")
    public void i_verify_miles_driven_per_year_has_a_value_of(String value) throws Throwable {
        pdlFitmentPopupPage.assertMilesDrivenPerYearValue(value);
    }

    @And("^I select Change Vehicle on pdl fitment page$")
    public void i_select_change_vehicle() throws Throwable {
    	fitmentPopupPage.clickChangeVehicle();
    }
    
    @When("^I move \"(Handling|Stopping Distance|Comfort & Noise|Life of Tire)\" driving priority to position \"(1|2|3|4)\"$")
    public void i_move_driving_priority(String priorityOption, String position) throws Throwable {
    	pdlFitmentPopupPage.moveDrivingPriorityOptionTo(priorityOption, position);
    }

    @Then("^I should verify that error message is displayed for \"(invalid|valid)\" \"(zipcode|Mile Driven Per Year)\"$")
    public void i_should_verify_that_error_message_is_displayed(String option1, String option2 ) throws Throwable {
        pdlFitmentPopupPage.invalidZipcodeErrorMessage(option1, option2);
    }

    @Then("^I verify the edit option is displayed$")
    public void i_verify_the_edit_option_is_displayed() throws Throwable {
        pdlFitmentPopupPage.assertEditOptionDisplayed();
    }

    @Then("^I verify the treadwell location display zipcode \"([^\"]*)\" on \"(treadwell modal|PLP)\"$")
    public void i_verify_the_treadwell_location_zipcode(String zip, String page) throws Throwable {
        pdlFitmentPopupPage.assertPrimaryDrivingLocation(zip, page);
    }

    @When("^I click on get started on the treadwell model on homepage$")
    public void i_click_on_get_started_on_the_treadwell_model_on_homepage() throws Throwable {
        pdlFitmentPopupPage.clickOnGetStarted();
    }

    @When("^I select edit icon on \"(treadwell modal|PLP|brand page|PDP)\"$")
    public void i_select_edit_icon(String page) throws Throwable {
        pdlFitmentPopupPage.clickEditIcon(page);
    }
}
