package dtc.steps;

import common.Constants;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.ConstantsDtc;
import dtc.pages.CommonActions;
import dtc.pages.ProductListPage;
import utilities.Driver;

/**
 * Created by aaronbriel on 9/17/16.
 */
public class ProductListPageSteps {

    private Driver driver;
    private ProductListPage productListPage;
    private final CommonActions commonActions;

    private String currentProductPageUrl;
    public static String productOnPLP;
    public static String productOnHomepage;

    private final String elementNameList = "\"(Compare|Sub Total with Tool Tip|Need It Now with Tool Tip|Tool Tip|" +
            "Check nearby Stores Banner|Found it lower|Best, Better, Good|PromotionsSub total tool tip|Tool Tip Content" +
            "|Tire subtotal|Need it now\\?|Check nearby stores|Wheel subtotal|Promotions|Sub total tool tip|Tire|Wheel|" +
            "add product to cart modal Popup|Item added to cart|Continue Shopping|Continue Shopping|Shopping Cart page|" +
            "Shopping cart|View Cart|view details|Savings available|Mile Warranty|Add To Cart|Free Shipping|Enter vehicle)\"";
    private String tireLifeAndCost;
    public int plpResultsCount = 0;
    private String priceMinimum = null;
    private String priceMaximum = null;

    public ProductListPageSteps(Driver driver) {
        this.driver = driver;
        productListPage = new ProductListPage(driver);
        commonActions = new CommonActions(driver);
    }

    @Then("^The search results show tires/wheels with the specified measurements \"(.*?)\"$")
    public void the_search_results_show_tires_wheels_with_the_specified_measurements(String measurements)
            throws Throwable {
        productListPage.assertSearchResults(measurements);
    }

    @And("^I note the current product list results url$")
    public void i_note_url_of_current_product_list_page() throws Throwable {
        currentProductPageUrl = commonActions.getCurrentUrl();
    }

    //NOTE: It's necessary to keep this step within this class due to the url dependency
    @Then("^I am taken back to the previous product list results url$")
    public void i_am_back_at_previous_product_list_results_url() throws Throwable {
        commonActions.waitForUrl(currentProductPageUrl, Constants.FIVE_THOUSAND);
    }

    @Then("^I verify that each page displays 10 items and total number of pages is equal to total count / 10 on \"(PLP|Orders|Store Locator)\" page$")
    public void i_verify_that_each_page_displays_products_and_total_number_of_pages_is_equal_to_total_count_div_10(String page)
            throws Throwable {
        commonActions.validatePagination(page);
    }

    @And("^I verify that the sort by dropdown value is set to \"(.*?)\"$")
    public void i_verify_that_the_sortby_dropdown_value_is_set_to(String value) throws Throwable {
        productListPage.verifySortByValue(value);
    }

    @And("^I verify that the number of search refinement filters is \"(.*?)\"$")
    public void i_verify_search_refinement_filters_number(String number) throws Throwable {
        productListPage.verifyNumberOfSearchRefinementFilters(number);
    }

    @And("^I verify that the search refinement filters contain the \"(single|multiple)\" value\\(s\\): \"(.*?)\"$")
    public void i_verify_that_search_refinement_filters_contain_values(String multiple, String values) throws Throwable {
        if (priceMinimum != null && priceMaximum != null) {
            values += ",$" + priceMinimum + " - $" + priceMaximum;
        }
        productListPage.verifySearchRefinementFilterValues(multiple, values);
    }

    @And("^I select the \"([^\"]*)\" from the Sort By dropdown box$")
    public void i_select_the_from_the_sort_by_dropdown_box(String value) throws Throwable {
        productListPage.setSortByValue(value);
    }

    @And("^I verify the results list is sorted in \"(.*?)\" order by \"(price|name)\"$")
    public void i_verify_the_results_list_is_sorted_in_order(String order, String orderType) throws Throwable {
        if (orderType.equalsIgnoreCase(Constants.PRICE)) {
            productListPage.verifyPricesInOrder(order);
        } else {
            productListPage.verifyNamesInOrder(order);
        }
    }

    @And("^I add \"(in stock|on order|first available)\" item of type \"(.*?)\" to my cart and \"(View shopping Cart|Continue Shopping)\"$")
    public void i_add_item_in_stock_or_on_order_to_my_cart_and(String itemStockStatus, String option, String action) throws Throwable {
        if (itemStockStatus.equalsIgnoreCase(ConstantsDtc.IN_STOCK)) {
            commonActions.clickContinueWithThisVehicleButton();
            i_select_from_filter_section(ConstantsDtc.QUICK_FILTERS, Constants.SINGLE, ConstantsDtc.IN_STOCK);
            productListPage.addToCart(option, null, "true", action, false);
        } else if (itemStockStatus.equalsIgnoreCase(ConstantsDtc.ON_ORDER)) {
            productListPage.addToCart(option, null, "false", action, false);
        } else {
            productListPage.addToCart(option, null, null, action, false);
        }
    }

    @And("^I add item \"(.*?)\" of type \"(sets|front|rear|none)\" to my cart and \"(View shopping Cart|Continue Shopping|Verify Vehicle Fitment|Close|shop for wheels|none)\"$")
    public void i_add_items_of_type_to_my_cart_and(String itemId, String option, String action) throws Throwable {
        productListPage.addToCart(option, itemId, null, action, false);
    }

    @And("^I set quantity to \"(.*?)\" and add item \"(.*?)\" of type \"(sets|front|rear|none)\" to my cart and \"(View Cart|Continue Shopping)\"$")
    public void i_set_quantity_and_add_items_to_my_cart_and(String quantity, String itemId, String option, String action) throws Throwable {
        CommonActions.productQuantity = quantity;
        productListPage.addToCart(option, itemId, null, action, false);
    }

    @And("^I \"(expand|collapse)\" the \"(.*?)\" filter section$")
    public void i_expand_filter_section(String action, String filterSection) throws Throwable {
        productListPage.expandCollapseFilterSection(action, filterSection);
    }

    @When("^I select from the \"(.*?)\" filter section, \"(single|multiple)\" option\\(s\\): \"(.*?)\"$")
    public void i_select_from_filter_section(String multiple, String filterSection, String options) throws Throwable {
        productListPage.selectFromFilterSection(filterSection, multiple, options);
    }

    @Then("^I verify no search refinement filters are being applied$")
    public void i_verify_no_search_refinement_filters_applied() throws Throwable {
        productListPage.verifyNoSearchRefinementsAreApplied();
    }

    @Then("^I verify the initial page displays products that match my tire size\\(s\\): \"(.*?)\"$")
    public void i_verify_initial_page_displays_products_with_matching_tire_sizes(String tireSizes) throws Throwable {
        productListPage.verifyProductsMatchTireSize(tireSizes);
    }

    @And("^I select the \"([^\"]*)\" product result image on \"(PLP|Compare Products|Compare Sets|Homepage)\" page$")
    public void i_select_the_product_result_image_by_position(String position, String page) throws Throwable {
        commonActions.selectProductImageByPosition(position, page, false);
    }

    @And("^I select the \"([^\"]*)\" available product result image on PLP page$")
    public void i_select_the_available_product_result_image_by_position(String position) throws Throwable {
        commonActions.selectProductImageByPosition(position, ConstantsDtc.PLP, true);
    }

    @And("^I click on the first available product result on PLP page$")
    public void i_select_the_first_available_product_result() throws Throwable {
        productListPage.clickFirstAvailableProductOnPlp();
    }

    @And("I click on the first available product result with view on my vehicle link on PLP page")
    public void i_select_the_first_available_product_result_with_view_on_my_vehicle() throws Throwable {
        productListPage.clickFirstAvailableProductOnPlpWithViewOnMyVehicleLink();
    }

    @When("^I select the Check Inventory for the first item available$")
    public void i_click_the_check_inventory_button_for_first_item_available() throws Throwable {
        productListPage.clickCheckInventoryForFirstAvailableItem();
    }

    @And("^I verify PLP UI \"(basic controls|banner without vehicle|sorting options|pagination)\"$")
    public void i_verify_plp_ui_aspects(String aspectToVerify) throws Throwable {
        productListPage.verifyPlpUiSection(aspectToVerify);
    }

    @Then("^I should see \"(.*?)\" on the product list page$")
    public void i_should_see_specified_info_on_the_product_list_page(String text) throws Throwable {
        productListPage.assertTextInProductListDetails(text);
    }

    @And("^I verify the Add To Cart button is clickable and Red on \"(.*?)\" page$")
    public void i_verify_the_add_to_cart_button_is_clickable_and_red(String page) throws Throwable {
        productListPage.verifyAddToCartButtonProperties(page);
    }

    @When("^I enter \"([^\"]*)\" into the first item quantity text box$")
    public void i_enter_into_the_first_item_quantity_text_box(int quantity) throws Throwable {
        productListPage.updateFirstRowItemQuantity(quantity);
    }

    @Then("^I see the Please Enter a Number error message$")
    public void i_see_the_please_enter_a_number_error_message() throws Throwable {
        productListPage.verifyProductQuantityErrorMessage();
    }

    @And("^I extract the fixed dollar promotion discount of \"([^\"]*)\"$")
    public void i_extract_the_fixed_dollar_promotion_discount_of_item(String itemCode) throws Throwable {
        CartPageSteps.fixedDiscountAmount = productListPage.extractFixedDiscountFromItem(itemCode);
    }

    @And("^I extract the fixed percentage promotion discount of \"([^\"]*)\"$")
    public void i_extract_the_fixed_percentage_promotion_discount_of_item(String itemCode) throws Throwable {
        CartPageSteps.fixedDiscountPercentage = productListPage.extractFixedDiscountPercentageFromItem(itemCode);
    }

    @Then("^I verify the word \"([^\"]*)\" does not appear in the tire set tab titles$")
    public void i_verify_the_word_does_not_appear_in_the_tire_set_tab_titles(String word) throws Throwable {
        productListPage.assertTextNotInTireTabTitles(word);
    }

    @Then("^I see \"([^\"]*)\" on the product list page$")
    public void i_see_item_on_the_product_list_page(String item) throws Throwable {
        productListPage.assertItemOnProductListPage(item);
    }

    @And("^I select the \"(.*?)\" checkbox$")
    public void i_select_the_quick_filter_checkbox(String checkBox) throws Throwable {
        productListPage.clickPlpCheckboxToSelectDeselectFilter(checkBox, false);
    }

    @And("^I verify the \"(.*?)\" checkbox has been checked$")
    public void i_verify_the_checkbox_has_been_checked(String checkBox) throws Throwable {
        productListPage.verifyPlpCheckboxFilterSelected(checkBox);
    }

    @And("^I select the \"([^\"]*)\" filter to uncheck the checkbox$")
    public void i_select_the_filter_to_uncheck_the_checkbox(String checkBox) throws Throwable {
        productListPage.clickPlpCheckboxToSelectDeselectFilter(checkBox, true);
    }

    @And("^I verify the \"(.*?)\" filter checkbox to be \"(deselected|selected)\"$")
    public void i_verify_the_filter_checkbox_is_deselected(String checkBoxLabel, String option) throws Throwable {
        productListPage.assertCheckboxDeselectedOrSelected(checkBoxLabel, option);
    }

    @And("^I verify the \"(.*?)\" checkbox to be \"(deselected|selected)\" by default$")
    public void i_verify_filter_checkbox_is_selected_or_deselected(String checkBoxLabel, String option) throws Throwable {
        productListPage.assertCheckboxDeselectedOrSelected(checkBoxLabel, option);
    }

    @Then("^I should see \"(.*?)\" as my selected miles per year value on pdl driving details section$")
    public void i_verify_pdl_miles_per_year_is_present(String milesPerYear) throws Throwable {
        productListPage.assertMilesPerYearValueOnPdlDrivingDetails(milesPerYear, ConstantsDtc.PLP);
    }

    @Then("^I should see \"(.*?)\" as my selected miles per year value on treadwell driving details section on \"(PLP|PDP)\"$")
    public void i_verify_pdl_miles_per_year_is_present(String milesPerYear, String page) throws Throwable {
        productListPage.assertMilesPerYearValueOnPdlDrivingDetails(milesPerYear, page);
    }

    @Then("^I verify \"(Everyday|Performance)\" default pdl driving priority order on \"(Pdlx fitment popup page|PLP)\"$")
    public void i_verify_pdl_default_driving_priorities_order_on_page(String drivingPriority, String page) throws Throwable {
        if (ConstantsDtc.DRIVING_PRIORITY_EVERYDAY.equalsIgnoreCase(drivingPriority)) {
            productListPage.assertDrivingPrioritiesOrder(ProductListPage.PDL_EVERYDAY_PRIORITY_ORDER, page);
        } else {
            productListPage.assertDrivingPrioritiesOrder(ProductListPage.PDL_PERFORMANCE_PRIORITY_ORDER, page);
        }
    }

    @Then("^I should see \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\" are in order on \"(Pdlx fitment popup page|PLP)\"$")
    public void i_should_see_driving_priorities_order_on_page(String pdlA, String pdlB, String pdlC, String pdlD, String page) throws Throwable {
        String[] PDL_PRIORITY_ORDER = {pdlA, pdlB, pdlC, pdlD};
        productListPage.assertDrivingPrioritiesOrder(PDL_PRIORITY_ORDER, page);
    }

    @And("^I verify driving priority order on \"(Pdlx fitment popup page|PLP)\"$")
    public void i_verify_driving_priority_order_on_page(String page) throws Throwable {
        Object[] array = driver.scenarioData.getDrivingPriorityList().toArray();
        String[] PDL_PRIORITY_ORDER = new String[array.length];
        int i = 0;
        for (Object o : array) {
            String s = (String) o;
            PDL_PRIORITY_ORDER[i] = s;
            i++;
        }
        productListPage.assertDrivingPrioritiesOrder(PDL_PRIORITY_ORDER, page);
    }

    @And("^I select edit treadwell driving details on plp page$")
    public void i_select_edit_pdl_driving_details() throws Throwable {
        productListPage.selectEditPdlDrivingDetails();
    }

    @And("^I select compare tire reviews of the first displayed product$")
    public void i_select_compare_tire_reviews() throws Throwable {
        commonActions.clickElementWithLinkText(ConstantsDtc.COMPARE_ITEM_MESSAGE);
    }

    @Then("^I verify the \"(.*?)\" filter section is displayed$")
    public void i_verify_filter_section_display(String filterSection) throws Throwable {
        productListPage.verifyFilterSectionsDisplay(filterSection);
    }

    @And("^I verify the \"Bolt Pattern\" facet is displayed when no vehicle is selected$")
    public void i_verify_bolt_pattern_facet_is_displayed_when_no_vehicle_is_selected() throws Throwable {
        productListPage.verifyBoltPatternFacetIsDisplayed(true);
    }

    @And("^I verify \"Bolt Pattern\" facet is not displayed when vehicle is selected$")
    public void i_verify_bolt_pattern_facet_is_not_displayed_when_vehicle_is_selected() throws Throwable {
        productListPage.verifyBoltPatternFacetIsDisplayed(false);
    }

    @Then("^I verify the \"(.*?)\" filter section\\(s\\) is/are displayed$")
    public void i_verify_filter_sections_are_displayed(String filterSections) throws Throwable {
        productListPage.verifyFilterSectionsDisplay(filterSections);
    }

    @Then("^I verify \"(SETS|FRONT|REAR|.*?)\" staggered option tab is displayed on PLP result page$")
    public void i_verify_staggered_option_tab_is_displayed_on_plp(String fitmentTab) throws Throwable {
        productListPage.verifyStaggeredOptionTabIsDisplayed(fitmentTab);
    }

    @When("^I extract \"(FRONT|REAR)\" staggered option size from tab$")
    public void i_extract_staggered_option_size_from_tab(String fitmentTab) throws Throwable {
        productListPage.getStaggeredFitmentSizeFromTab(fitmentTab);
    }

    @Then("^I verify \"(FRONT|REAR)\" wheel diameter matches with the size of each product on the results page$")
    public void i_verify_wheel_diameter_matches_with_each_rendered_product_size_on_plp(String fitmentTab) throws Throwable {
        productListPage.verifyListedProductsSizeMatchesWithSelectedStaggeredDiameter(fitmentTab);
    }

    @Then("^I select \"(FRONT|REAR|SETS|.*?)\" staggered tab on PLP result page$")
    public void i_select_staggered_tab(String fitmentTab) throws Throwable {
        commonActions.selectStaggeredTab(fitmentTab);
    }

    @Then("^I verify Customer Rating and reviews are displayed for listed products on PLP result page$")
    public void i_verify_customer_rating_and_reviews_are_displayed_for_listed_products_on_plp_result_page() throws Throwable {
        productListPage.verifyCustomerRatingAndReviewsDisplayedForListedProducts();
    }

    @Then("^I verify compare tire reviews link is displayed for products that have reviews$")
    public void i_verify_compare_tire_reviews_link_is_displayed_for_products_that_have_reviews() throws Throwable {
        productListPage.verifyCompareTireReviewsLinksDisplayed();
    }

    @When("^I extract \"(.*?)\" filter font size$")
    public void i_extract_filter_font_size(String filterLabel) throws Throwable {
        productListPage.getFilterFontSize(filterLabel);
    }

    @When("^I verify \"(.*?)\" filter font size value before and after selection$")
    public void i_verify_filter_font_size_value_before_after_selection(String filterLabel) throws Throwable {
        productListPage.assertFilterFontSizeBeforeAndAfterFilterSelection(filterLabel);
    }

    @And("^I verify expected brand \"(.*?)\" products displayed on PLP$")
    public void i_verify_expected_brand_products_displayed_on_plp(String brand) throws Throwable {
        productListPage.assertProductBrandOnPLP(brand);
    }

    @Then("^I verify Original Equipment tire is displayed on \"(PLP|PDP)\" page$")
    public void i_verify_original_equipment_tire_is_displayed_on_page(String page) throws Throwable {
        productListPage.assertOETireDisplayedOnPage(page);
    }

    @And("^I verify tire quantity is \"(.*?)\" on \"(product list page|Compare tire reviews)\"$")
    public void i_verify_tire_quantity_on_product_list_page(String quantity, String page) throws Throwable {
        commonActions.assertProductQuantity(quantity, page);
    }

    @And("^I verify the order of filter categories for \"(Tires|Wheels)\"$")
    public void i_verify_order_of_filter_categories_for_product_type(String productType) throws Throwable {
        productListPage.verifyOrderOfFilterCategoriesForProductType(productType);
    }

    @Then("^I verify all PLP results are on promotion$")
    public void i_verify_all_plp_results_are_on_promotion() throws Throwable {
        productListPage.assertAllProductsOnPromotion(true);
    }

    @Then("^I verify \"([1|2|3])\" products selected$")
    public void i_verify_products_selected(Integer quantity) throws Throwable {
        productListPage.assertProductQuantitySelected(quantity);
    }

    @Then("^I verify compare button text is displayed as \"([^\"]*)\"$")
    public void i_verify_compare_button_text_is_displayed_as(String text) throws Throwable {
        productListPage.assertCompareButtonText(text);
    }

    @And("^I \"(select|deselect)\" the compare checkbox at position \"(.*?)\" from the products list page$")
    public void i_select_deselect_product_checkbox_at(String selectDeselect, String position) throws Throwable {
        if (selectDeselect.equalsIgnoreCase(Constants.SELECT)) {
            productListPage.selectCompareCheckboxAtPosition(Integer.parseInt(position));
        } else {
            productListPage.deselectCompareCheckboxAtPosition(Integer.parseInt(position));
        }
    }

    @And("^I \"(select|deselect)\" the first eligible checkbox from the products list page$")
    public void i_select_deselect_first_eligible_product_checkbox(String selectDeselect) throws Throwable {
        int position = productListPage.findEligibleCompareCheckBox(selectDeselect);
        if (selectDeselect.equalsIgnoreCase(Constants.SELECT)) {
            productListPage.selectCompareCheckboxAtPosition(position + 1);
        } else {
            productListPage.deselectCompareCheckboxAtPosition(position + 1);
        }
    }

    @Then("^I verify the PLP header message contains \"(.*?)\"$")
    public void i_verify_plp_header_message_contains_text(String textToValidate) throws Throwable {
        productListPage.verifyPlpHeaderMessageContainsText(textToValidate);
    }

    @And("^I verify that the price range is correct$")
    public void i_verify_that_price_range_is_correct() throws Throwable {
        if (priceMinimum != null && priceMaximum != null) {
            productListPage.verifySearchRefinementFilterValues(Constants.SINGLE, priceMinimum);
            productListPage.verifySearchRefinementFilterValues(Constants.SINGLE, priceMaximum);
        }
    }

    @And("^I reduce the \"Price Range\" slider filter$")
    public void i_reduce_price_range_slider_filter() throws Throwable {
        commonActions.reducePriceRangeSliderFilter();
        priceMinimum = commonActions.getCurrentPriceRangePoint(CommonActions.MIN);
        priceMaximum = commonActions.getCurrentPriceRangePoint(CommonActions.MAX);
    }

    @And("^I set the \"Price Range\" slider filter to the range: \\$\"(.*?)\" - \"(.*?)\"$")
    public void i_set_price_range_slider_filter_to_price_range(String priceMin, String priceMax) throws Throwable {
        priceMinimum = priceMin;
        priceMaximum = priceMax;
        commonActions.setPriceRangeSliderFilterToRange(priceMin, priceMax);
    }

    @When("^I clear all the currently active filters on the PLP page$")
    public void i_clear_all_currently_active_filters_on_plp_page() throws Throwable {
        productListPage.clearAllSearchFilters();
    }

    @Then("^I verify the product list page is displayed having \"(staggered|non-staggered|no)\" fitment$")
    public void i_verify_the_product_list_page_is_displayed(String fitmentType) throws Throwable {
        if (fitmentType.equalsIgnoreCase(Constants.STAGGERED)) {
            productListPage.verifyPLPTabDisplayForStaggered();
        }
        productListPage.verifyPlpDisplayedWithResults();
    }

    @Then("^I verify the PLP displays 'Top 3 Tiles' below the PLP header$")
    public void i_verify_the_plp_displays_top_tiles_below_the_plp_header() throws Throwable {
        productListPage.assertTop3TilesComponentIsDisplayed();
    }

    @Then("^I verify the PLP displays \"([^\"]*)\",\"([^\"]*)\",\"([^\"]*)\" in the 'Top 3 Tiles'$")
    public void i_verify_the_plp_displays_in_the_top_tiles(String value1, String value2, String value3) throws Throwable {
        productListPage.assertTop3TileValues(value1, value2, value3);
    }

    @When("^I select product at position \"([^\"]*)\" in 'Top 3 Tiles'$")
    public void i_select_product_at_position_in_top_3_tiles(int position) throws Throwable {
        productListPage.selectProductInTop3Tiles(position);
    }

    @Then("^I am brought to the search results PLP with the product's size as the search criteria \"([^\"]*)\" and the brand \"([^\"]*)\" facet selected$")
    public void i_am_brought_to_the_search_results_page(String sizeCriteria, String targetBrand) throws Throwable {
        productListPage.assertRedirectToTargetSearchResultsPage(sizeCriteria, targetBrand);
    }

    @Then("^I verify treadwell details section \"(.*?)\"$")
    public void i_should_see_treadwell_details_section_with_treadwell_logo(String option) throws Throwable {
        productListPage.assertTreadwellDetailsBox(option);
    }

    @Then("^I verify that the treadwell filter is \"(present|absent)\" in the first position in quick filters$")
    public void i_verify_that_the_treadwell_filter_is_in_the_first_position_in_quick_filters(String option) throws Throwable {
        productListPage.assertTreadwellLogoPosition(option);
    }

    @Then("^I extract the product at position \"([^\"]*)\" from \"(PLP|Homepage)\"$")
    public void i_extract_the_product_at_position_from_page(String position, String page) throws Throwable {
        if (page.equalsIgnoreCase(ConstantsDtc.PLP)) {
            productOnPLP = productListPage.extractProductBrandOnHomepageOrPLP(Integer.parseInt(position), page);
        } else {
            productOnHomepage = productListPage.extractProductBrandOnHomepageOrPLP(Integer.parseInt(position), page);
            String brand = productOnHomepage.split("\n")[0];
            String productName = productOnHomepage.split("\n")[1];
            String price = productOnHomepage.split("\n")[2];
            commonActions.addProductInfoListItem(brand, productName, price, "", "", "", "", false);
        }
    }

    @Then("^I verify the product is displayed at position \"([^\"]*)\" in the 'Top 3 Tiles'$")
    public void i_verify_the_product_is_displayed_at_position_in_the_top_tiles(String position) throws Throwable {
        productListPage.assertProductInTop3Tiles(productOnPLP.split("\n")[0], Integer.parseInt(position));
    }

    @Then("^I verify \"([^\"]*)\" does not display in the filters$")
    public void i_verify_does_not_display_in_the(String checkBoxLabel) throws Throwable {
        productListPage.assertFilterNotDisplayed(checkBoxLabel);
    }

    @Then("^I verify \"([^\"]*)\" is displayed at position \"([^\"]*)\" in the 'Top 3 Tiles'$")
    public void i_verify_is_displayed_at_position_in_the_top_tiles(String brand, String position) throws Throwable {
        productListPage.assertProductInTop3Tiles(brand, Integer.parseInt(position));
    }

    @And("^I select the 'Read Reviews' link on the PLP$")
    public void i_select_the_read_reviews_link_on_the_plp() throws Throwable {
        productListPage.selectReadReviewsLink();
    }

    @Then("^I can see \"(.*?)\" PLP page$")
    public void i_can_see_plp_page(String category) throws Throwable {
        productListPage.verifyCatalogPage(category, false);
    }

    @Then("^I can see \"(.*?)\" Brand PLP page$")
    public void i_can_see_brand_plp_page(String category) throws Throwable {
        productListPage.verifyCatalogPage(category, true);
    }

    @When("^I click on the product \"([^\"]*)\"$")
    public void i_click_on_product(String itemCode) throws Throwable {
        productListPage.clickOnProduct(itemCode);
    }

    @When("^I select \"(.*?)\" on the product list page to compare$")
    public void i_select_item_on_the_product_list_page_to_compare(String item) throws Throwable {
        productListPage.selectProductToCompare(item);
    }

    @And("^I verify the \"([^\"]*)\" link displayed for \"([^\"]*)\" on PLP page$")
    public void i_verify_the_link_displayed_for_item_on_plp_page(String link, String itemCode) throws Throwable {
        productListPage.assertLinkIsDisplayedForItem(itemCode, link);
    }

    @Then("^I verify that all PLP results are In Stock$")
    public void i_verify_that_all_plp_results_are_in_stock() throws Throwable {
        productListPage.assertAllProductsInStock(true);
    }

    @Then("^I verify all results match the search criteria: \"([^\"]*)\"$")
    public void i_verify_all_results_match_the_search_criteria(String value) throws Throwable {
        productListPage.verifyListedProductsMatchesWithSelectedValue(value);
    }

    @When("^I extract the tire results count from the PLP$")
    public void i_extract_tire_results_count_from_plp() throws Throwable {
        plpResultsCount = productListPage.extractResultsCountFromPlp();
    }

    @And("^I verify the tire results count from the PLP without fitment matches the tire results count on PLP with fitment$")
    public void i_verify_tire_results_count_from_plp_without_fitment_matches_tire_results_count_on_plp_with_fitment() throws Throwable {
        productListPage.verifyPlpResultCountMatchesExpectation(plpResultsCount);
    }

    @And("^I verify the 'Brands' search bar contains the \"(placeholder text|text)\": \"(.*?)\"$")
    public void i_verify_brands_filter_search_field_contains_text(String textType, String text) throws Throwable {
        productListPage.verifyBrandsFilterSearchBarContainsText(textType, text);
    }

    @And("^I verify \"(Show More|Show Less)\" is \"(displayed|not displayed)\" for the \"(.*?)\" filter section$")
    public void i_verify_show_option_display_for_filter_section(String showType, String displayCheck,
                                                                String filterSection) throws Throwable {
        productListPage.verifyShowMoreOrLessDisplayForFilterSection(showType, displayCheck, filterSection);
    }

    @And("^I select \"(Show More|Show Less)\" option for the \"(.*?)\" filter section$")
    public void i_select_show_more_less_option_for_filter_section(String showType, String filterSection) throws
            Throwable {
        productListPage.selectShowMoreOrLessForFilterSection(showType, filterSection);
    }

    @When("^I enter \"(.*?)\" into the 'Brands' search bar$")
    public void i_enter_text_into_brands_search_bar(String searchText) throws Throwable {
        productListPage.enterTextIntoBrandsSearchBar(searchText);
    }

    @Then("^I verify up to \"(\\d+)\" characters were entered into the 'Brands' search bar$")
    public void i_verify_up_to_limit_of_chars_were_entered_into_brands_search_bar(int charLimit) throws Throwable {
        productListPage.verifyUpToCharLimitEnteredInBrandsSearchBar(charLimit);
    }

    @When("^I clear the 'Brands' search bar$")
    public void i_clear_brands_search_bar() throws Throwable {
        productListPage.clearBrandsSearchBar();
    }

    @Then("^I verify the only options displayed for the 'Brands' filter section contain: \"(.*?)\"$")
    public void i_verify_only_options_displayed_for_brands_filter_section_contain_text(String searchText)
            throws Throwable {
        productListPage.verifyOptionsContainingTextDisplayForBrandsFilterSection(searchText);
    }

    @And("^I verify the \"(.*?)\" filter section is expanded by default$")
    public void i_verify_filter_section_expanded(String filterSection) throws Throwable {
        productListPage.verifyFilterSectionIsExpanded(filterSection);
    }

    @Then("^I verify 'Price Range' slider filter has two handles$")
    public void i_verify_price_range_slider_filter_has_two_handles() throws Throwable {
        productListPage.verifyPriceRangeSliderFilterHasTwoHandles();
    }

    @Then("^I verify the extracted first product price is displayed as the \"(maximum|minimum)\" price point in the 'Price Range' filter section$")
    public void i_verify_extracted_product_price_displayed_as_price_point_in_price_range_filter_section(String pricePoint) throws Throwable {
        productListPage.verifyFirstListedPriceMatchesMinimumOrMaximum(pricePoint);
    }

    @Then("^I verify " + elementNameList + " exists in PLP$")
    public void i_verify_if_elements_exists_in_plp(String elementName) throws Throwable {
        productListPage.verifyIfEachElementInTheListIsDisplayed(elementName);
    }

    @Then("^I verify " + elementNameList + " has the text of " + elementNameList + " in PLP$")
    public void i_verify_if_all_of_these_elements_has_the_text(String elementName, String validation) throws Throwable {
        productListPage.verifyTextAttributeOfListOfElements(elementName, validation);
    }

    @Then("^I verify check near by stores has the text of " + elementNameList + " in PLP$")
    public void i_verify_if_alternate_elements_has_the_text(String validation) throws Throwable {
        String elementName = ProductListPage.CHECK_NEARBY_STORES_BANNER;
        productListPage.verifyTextAttributeOfListOfElementsAlternateElement(elementName, validation);
    }

    @When("^I click " + elementNameList + " element in PLP$")
    public void i_click_element_in_plp(String elementName) throws Throwable {
        productListPage.clickInPlp(elementName);
    }

    @Then("^I verify " + elementNameList + " Tool tip has the required text in PLP$")
    public void i_verify_if_the_element_has_the_text_in_plp(String type) throws Throwable {
        if (type.equalsIgnoreCase(Constants.TIRE)) {
            productListPage.verifyTextOfElementPlp(ConstantsDtc.TOOL_TIP_CONTENT, ProductListPage.TOOL_TIP_TIRE_MESSAGE);
        } else if (type.equalsIgnoreCase(Constants.WHEEL))
            productListPage.verifyTextOfElementPlp(ConstantsDtc.TOOL_TIP_CONTENT, ProductListPage.TOOL_TIP_WHEEL_MESSAGE);
    }

    @Then("^I verify Sub Total Tool Tip has the required text in PLP$")
    public void i_verify_tire_sub_total_tool_tip_has_the_required_text_in_plp() throws Throwable {
        productListPage.verifyTextOfElementPlp(ConstantsDtc.TOOL_TIP_CONTENT, ConstantsDtc.SUB_TOTAL_TOOL_TIP_MESSAGE);
    }

    @When("^I add item with message \"(.*?)\" to my cart$")
    public void i_add_item_with_message_to_my_cart(String status) throws Throwable {
        productListPage.addElementToCart(status);
    }

    @Then("^I verify the PLP \"(displays|not displays)\" the Driving Details block$")
    public void i_verify_the_plp_displays_the_driving_details_block(String visibility) throws Throwable {
        productListPage.assertDrivingDetailsBlock(visibility);
    }

    @When("^I click cancel on pdl fitment page$")
    public void i_click_cancel_pdl_fitment_page() {
        productListPage.selectCancelPdlFitmentPanel();
    }

    @Then("^I verify the PLP displays \"([^\"]*)\" in the Top 3 Tiles$")
    public void i_verify_the_plp_displays_in_the_top_tiles(String type) throws Throwable {
        productListPage.assertPDLTile(type);
    }

    @Then("^I verify Canonical products are \"(displayed|not displayed)\" on PLP page$")
    public void i_verify_canonical_products_are_displayed_on_plp_page(String displayedNotDisplayed) throws Throwable {
        productListPage.verifyPlpDisplayedWithResults();
        if (displayedNotDisplayed.equalsIgnoreCase(Constants.DISPLAYED))
            productListPage.assertCanonicalProductsOnPlpPage(true);
        else
            productListPage.assertCanonicalProductsOnPlpPage(false);
    }

    @Then("^I verify if the error message \"(.*?)\" is displayed on the header$")
    public void i_verify_if_the_error_message_is_displayed_on_the_header(String message) throws Throwable {
        productListPage.assertTreadwellErrorMessage(message);
    }

    @Then("^I verify treadwell top recommended product is \"(displayed|not displayed)\" in 'Top 3 Tiles'$")
    public void i_verify_treadwell_top_recommended_product_is_displayed_in_top_3_tiles(String option) throws Throwable {
        productListPage.assertTreadwellProductInTop3Tiles(option);
    }

    @Then("^I verify all the products listed on PLP contains product driving details container$")
    public void i_verify_all_the_products_listed_on_plp_contains_product_driving_details_container() throws Throwable {
        productListPage.assertProductDetailsContainer();
    }

    @When("^I extract the \"(.*?)\" product tire life and cost$")
    public void i_extract_the_ranking_product_tire_life_and_cost(int position) throws Throwable {
        tireLifeAndCost = productListPage.extractTireLifeAndCost(position);
    }

    @Then("^I verify tire life and cost values of the selected vehicle$")
    public void i_verify_tire_life_and_cost_values_of_the_selected_vehicle() throws Throwable {
        productListPage.assertTireLifeAndCost(tireLifeAndCost);
    }

    @And("^I verify PLP results are grouped by brand \"(.*?)\"$")
    public void i_verify_plp_results_are_grouped_by_brand(String brand) throws Throwable {
        productListPage.assertPlpResultsByBrand(brand);
    }

    @And("^I verify default \"(.*?)\" sortby option$")
    public void i_verify_default_sortby_option(String sortby) throws Throwable {
        productListPage.verifySortByCurrentValue(sortby);
    }

    @And("^I verify price range displayed on canonical product$")
    public void i_verify_price_range_displayed_on_canonical_product() throws Throwable {
        productListPage.assertPlpProductPrice();
    }

    @And("^I verify non canonical product details contain Item#$")
    public void i_verify_non_canonical_product_details_contain_itemNumber() throws Throwable {
        productListPage.verifyItemNumber();
    }

    @And("^I verify tooltip and tooltip text for each icon$")
    public void i_verify_tooltip_and_tooltip_text_for_each_icon() throws Throwable {
        productListPage.verifyTooltipText();
    }

    @Then("^I verify the thumbnail images are displayed for the listed products$")
    public void i_verify_thumbnail_image_displayed_for_listed_products() throws Throwable {
        productListPage.verifyProductThumbnailImageDisplayedForListedProducts();
    }

    @Then("^I verify the results list is sorted in ascending order by treadwell ranking on PLP$")
    public void i_verify_the_results_list_is_sorted_in_ascending_order_by_treadwell_ranking_on_plp() throws Throwable {
        productListPage.assertTreadwellItemsRanking();
    }

    @Then("^I verify calculation of subtotal on qty and retail price$")
    public void i_verify_calculation_of_subtotal_on_qty_and_retail_price() throws Throwable {
        productListPage.assertSubtotalCalculation();
    }

    @Then("^I verify calculation of subtotal on qty and retail price for 'SETS' staggered tab$")
    public void i_verify_calculation_of_subtotal_on_qty_and_retail_price_for_sets_staggered_tab() throws Throwable {
        productListPage.assertSubtotalCalculationOnSetsTab();
    }

    @And("^I verify quantity for front and rear tabs plp$")
    public void i_verify_quantity_for_front_and_rear_tabs_plp() throws Throwable {
        productListPage.assertFrontRearQty();
    }

    @Then("^I verify Add To Cart button is displayed on PLP page$")
    public void i_verify_Add_To_Cart_button_is_displayed_on_plp_page() throws Throwable {
        productListPage.assertAddToCartDisplay();
    }

    @And("^I verify \"(.*?)\" text not present on PLP page$")
    public void i_verify_text_not_present_on_plp_page(String text) throws Throwable {
        productListPage.assertTextNotPresentOnPlp(text);
    }

    @And("^I verify mile warranty range for PLP products$")
    public void i_verify_mile_warranty_range_for_plp_products() throws Throwable {
        productListPage.verifyMileWarrantyRange();
    }

    @And("^I verify mileage warranty is within filter range \"(.*?)\" \"(.*?)\"$")
    public void i_verify_mile_warranty_is_within_filter_range(int min_Range, int max_Range) throws Throwable {
        productListPage.verifyMileWarrantyWithinExpectedRange(min_Range, max_Range);
    }

    @And("^I verify 'no result' page is not displayed$")
    public void i_verify_no_result_page_is_not_displayed() throws Throwable {
        commonActions.checkForNoResultsMessage();
    }

    @And("^I select the first available View Details from the results page$")
    public void i_select_the_first_available_view_details() throws Throwable {
        commonActions.clickFirstViewDetailsButton(false);
    }


    @And("^I select \"View Details\" for product \"([^\"]*)\" on the PLP page$")
    public void i_select_view_details_for_product_with_on_the_plp_page(String productName) throws Throwable {
        commonActions.clickViewDetailsButtonForProductOnPlpPage(productName);
    }

    @And("^I select the first available view details from the results page for product with image and extract product info$")
    public void i_select_the_first_available_view_details_button_for_product_with_image_and_extract_product_info() throws Throwable {
        commonActions.clickFirstViewDetailsButton(true);
    }

    @Then("^I verify selected items on cart page$")
    public void i_verify_selected_items_on_cart_page() throws Throwable {
        productListPage.verifySelectedItemCodesOnCart();
    }

    @And("^I select the first available Standard product to \"(Add to Cart|Add to Package)\" on PLP page$")
    public void i_select_the_first_available_standard_product_on_plp_page(String buttonType) throws Throwable {
        productListPage.selectFirstStandardProductOnPLP(buttonType);
    }

    @And("^I verify subtotal with tooltip exist for all non canonical products$")
    public void i_verify_subtotal_with_tooltip_exist_for_all_non_canonical_products() throws Throwable {
        productListPage.verifySubtotalExistForStandardProduct();
    }

    @And("^I verify products displayed contain treadwell details on PLP$")
    public void i_verify_products_displayed_contain_treadwell_details_on_plp() throws Throwable {
        productListPage.assertDrivingDetailsOnPlp();
    }

    @When("^I select product \"([^\"]*)\" from PLP page$")
    public void i_select_product_on_plp_page(String productName) throws Throwable {
        productListPage.selectProductByName(productName);
    }

    @And("^I verify \"(.*?)\" and \"(.*?)\" on PLP product details when vehicle on session$")
    public void i_verify_plp_product_details_when_vehicle_on_session(String brand, String size) throws Throwable {
        productListPage.assertPlpDetailsWhenVehicleOnSession(brand, size);
    }

    @And("^I verify price and inventory text for canonical products when \"(VEHICLE_ON_SESSION|VEHICLE_NOT_ON_SESSION)\"$")
    public void i_verify_price_and_inventory_text_for_canonical_products_when(String vehicleSession) throws Throwable {
        productListPage.assertCanonicalForPriceLinkAndInventoryText(vehicleSession);
    }

    @Then("^I verify Size,Item,Price and Inventory displayed for the products in Standard PLP$")
    public void i_verify_size_item_price_and_inventory_displayed_for_the_products_in_standard_plp() throws Throwable {
        productListPage.verifyItemNumberSizePrice();
    }

    @And("^I verify the PLP page is displayed$")
    public void i_verify_the_plp_page_is_displayed() throws Throwable {
        productListPage.verifyPlpDisplayedWithResults();
    }

    @When("^I verify all the products listed on plp are of \"(.*?)\"$")
    public void i_verify_all_the_products_listed_on_plp_are_of_the_specified_size(String size) throws Throwable {
        productListPage.verifyPlpResultsSize(size);
    }

    @And("^I verify message displayed in search no results page with \"(no vehicle in session|vehicle in session|no results)\"$")
    public void i_verify_message_displayed_in_search_no_results_page_with(String vehicleState) throws Throwable {
        productListPage.verifyNoResultsSearchMessage(vehicleState);
    }

    @And("^I verify add to cart button disabled for item code \"(.*?)\" in PLP page$")
    public void i_verify_add_to_cart_button_disabled_for_item_code_in_page(String itemCode) throws Throwable {
        productListPage.assertAddToCartDisabledInPlp(itemCode);
    }

    @And("^I set quantity to \"(.*?)\" and add item \"(.*?)\" of type \"(sets|front|rear|none)\" to my add to package$")
    public void i_set_quantity_to_and_add_item_of_type_to_my_add_to_package(String quantity, String itemId, String option) throws Throwable {
        CommonActions.productQuantity = quantity;
        commonActions.getTargetAddToCartButton(ConstantsDtc.ADD_TO_PACKAGE, option, itemId, null, false).click();
    }

    @And("^I verify package details by key name \"(.*?)\" are present at in PLP page$")
    public void i_verify_package_details_by_key_name_are_present_at_in_plp_page(String keyName) throws Throwable {
        productListPage.assertPackageDetailsInPlpPage(keyName);
    }

    @When("^I reset the minimum and maximum price range$")
    public void i_reset_the_minimum_and_maximum_price_range() throws Throwable {
        priceMinimum = null;
        priceMaximum = null;
    }

    @Then("^I verify the staggered size line displayed the selected size$")
    public void i_verify_the_staggered_size_line_displayed_the_selected_size() throws Throwable {
        productListPage.assertStaggeredSizeLine();
    }

    @And("^I select the product image with \"(.*?)\" on PLP page$")
    public void i_select_the_product_image_with_itemcode_on_plp_page(String itemCode) throws Throwable {
        productListPage.selectProductImageOnPLP(itemCode);
    }

    @Then("^I verify the product is displayed at position \"(.*?)\" in the 'suggested selling carousel'$")
    public void i_verify_the_product_is_displayed_at_position_in_the_suggested_selling_carousel(int position) throws Throwable {
        productListPage.assertProductPositionOnSuggestedSellingCarousel(productOnPLP, position);
    }

    @When("^I select the \"(.*?)\" product result image on PLP page with \"(Add to Cart|View details)\" button$")
    public void i_select_the_product_result_image_on_plp_page_with_button(String position, String buttonType) throws Throwable {
        productListPage.selectProductWithButton(position, buttonType);
    }
}