package dtc.steps;

import common.Config;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.data.ConstantsDtc;
import dtc.data.Product;
import dtc.pages.CommonActions;
import dtc.pages.SearchAutocompleteDropDownPage;
import utilities.Driver;

/**
 * Created by aaronbriel on 10/24/16.
 */
public class SearchAutocompleteDropDownPageSteps {

    private SearchAutocompleteDropDownPage searchAutocompleteDropDownPage;
    private final CommonActions commonActions;
    private final String elementNameList = "\"(Shop tires that fit|Shop wheels that fit|Start A New Search|Brand under Wheels|" +
            "Brand under Tires|Size under Wheels|Size under Tires|Vehicle under Wheels|Fitment popup|Vehicle under Tires|" +
            "My Vehicle|Shop By|Search invalid title|Search title|Search)\"";
    private Product product;
    private Scenario scenario;

    public SearchAutocompleteDropDownPageSteps(Driver driver) {
        searchAutocompleteDropDownPage = new SearchAutocompleteDropDownPage(driver);
        commonActions = new CommonActions(driver);
        product = new Product();
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @When("^I select \"(.*?)\" from the autocomplete dropdown of the search box$")
    public void i_select_text_from_the_search_dropdown(String text) throws Throwable {
        if (text.startsWith(ConstantsDtc.PRODUCT.toUpperCase())) {
            text = product.getProduct(text).productName;
            scenario.write("Product Name: '" + text + "'");
        }

        if (Config.isMobile()) {
            commonActions.clickElementWithLinkText(text);
        } else {
            searchAutocompleteDropDownPage.selectProductByName(text);
        }
    }

    @Then("^I click on " + elementNameList + " in search results Page$")
    public void i_click_on_the_element(String elementName) throws Throwable {
        searchAutocompleteDropDownPage.clickInSearchAutocomplete(elementName);
    }

    @Then("^I verify " + elementNameList + " has the text " + elementNameList + " in search results page$")
    public void i_verify_if_element_has_the_text(String elementName, String validation) throws Throwable {
        searchAutocompleteDropDownPage.verifyTextOfElementInSearchAutocomplete(elementName, validation);
    }

    @Then("^I verify " + elementNameList + " exists in search results page$")
    public void i_verify_if_element_exists_in_searchautocomplete(String elementName) throws Throwable {
        searchAutocompleteDropDownPage.verifySearchResultPageElementExists(elementName);
    }
}