package dtc.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import dtc.data.ConstantsDtc;
import dtc.pages.StoreChangeModalPage;
import dtc.pages.CommonActions;
import utilities.Driver;

public class StoreChangeModalSteps {

    private StoreChangeModalPage storeChangeModalPage;
    private CommonActions commonActions;

    public StoreChangeModalSteps(Driver driver) {
        storeChangeModalPage = new StoreChangeModalPage(driver);
        commonActions = new CommonActions(driver);
    }

    @And("^I verify the modal popup contains item \"(.*?)\" with \"(Price Increase|Price Decrease|Product not available|Available today|Available)\" message$")
    public void i_verify_the_modal_popup_contains_item_with_message(String productName, String price) throws Throwable {
        storeChangeModalPage.verifyModalItemAndMessage(productName, price);
    }

    @And("^I verify the modal popup contains product names$")
    public void i_verify_the_modal_popup_contains_product_names() throws Throwable {
        storeChangeModalPage.verifyModalProductName();
    }

    @Then("^I verify the Store Changed Modal is displayed$")
    public void i_verify_the_store_changed_modal_is_displayed() throws Throwable {
        storeChangeModalPage.verifyStoreChangedModalDisplayed();
    }

    @Then("^I verify the Store Changed Modal contains \"(.*?)\" and success message$")
    public void i_verify_the_store_changed_modal_contains(String text) throws Throwable {
        storeChangeModalPage.verifyStoreChangeModalContains(text);
    }

    @Then("^I verify the 'Confirm Store Changed' Modal is displayed with warning message$")
    public void i_verify_the_confirm_store_changed_modal_is_displayed_with_warning() throws Throwable {
        storeChangeModalPage.verifyConfirmStoreChangeModalWithWarningMessage();
    }

    @And("^I click \"USE THIS STORE\"$")
    public void i_select_use_this_store() throws Throwable {
        storeChangeModalPage.removeUnavailableProductsFromProductInfoList();
        commonActions.clickButtonByText(ConstantsDtc.USE_THIS_STORE);
    }
}
