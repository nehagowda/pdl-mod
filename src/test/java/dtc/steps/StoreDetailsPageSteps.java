package dtc.steps;

import common.Constants;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import dtc.pages.StoreDetailsPage;
import utilities.Driver;

/**
 * Created by aaronbriel on 10/24/16.
 */
public class StoreDetailsPageSteps {

    private StoreDetailsPage storeDetailsPage;
    private Driver driver;

    public StoreDetailsPageSteps(Driver driver) {
        storeDetailsPage = new StoreDetailsPage(driver);
        this.driver = driver;
    }

    @And("^I verify the \"([^\"]*)\", \"([^\"]*)\" of the current store$")
    public void i_verify_details_of_the_current_store(String title, String address) throws Throwable {
        storeDetailsPage.verifyMyStoreDetails(title, address);
    }

    @And("^I click \"Schedule appointment\" on \"(Store Details|Store Locator)\" page$")
    public void i_click_schedule_appointment_on_page(String page) throws Throwable {
        storeDetailsPage.clickScheduleAppointment(page);
    }

    @And("^I select Store Details \"(Next Carousel button|Next Carousel indicator|Store Services)\" on Store Details page$")
    public void i_select_store_image_next_carousel(String text) throws Throwable {
        storeDetailsPage.selectOnStoreDetails(text);
    }

    @And("^I select \"([^\"]*)\" on Share \"(Store|Installer)\" Details popup page$")
    public void i_select_option_on_share_store_details_popup(String option, String text) throws Throwable {
        storeDetailsPage.selectOnShareStoreDetailsPopup(option, text);
    }

    @Then("^I verify the displayed text matches to the clipboard copied text for \"(Store Details Link|Installer Details Link)\"$")
    public void i_verify_the_displayed_text_matches_to_clipboard_copied_text(String textType) throws Throwable {
        storeDetailsPage.assertTextWithClipboardText(textType);
    }

    @Then("^I verify three nearest stores displayed for the current store on store details page$")
    public void i_verify_three_nearest_stores_displayed_for_the_current_store_on_store_details_page() throws Throwable {
        storeDetailsPage.assertNearestStoresDisplayed();
    }

    @And("^I \"(expand|collapse)\" number \"(1|2|3)\" nearest Store on store details page$")
    public void i_expand_collapse_nearest_store_on_store_details_page(String select, Integer option) throws Throwable {
        storeDetailsPage.selectNearestStoreQuickViewToggleButton(select, option);
    }

    @Then("^I verify the nearest store number \"(1|2|3)\" is \"(expanded|collapsed)\"$")
    public void i_verify_the_nearest_store_is_expanded_or_collapsed(Integer storeNumber, String option) throws Throwable {
        storeDetailsPage.assertNearestStoreResultExpansion(storeNumber, option);
    }
}