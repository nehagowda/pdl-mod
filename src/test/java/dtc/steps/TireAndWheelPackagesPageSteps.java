package dtc.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.When;
import dtc.pages.TireAndWheelPackagesPage;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import utilities.Driver;

import static dtc.pages.CommonActions.dtModalCloseBy;
import static dtc.pages.CommonActions.dtModalHeaderBy;

public class TireAndWheelPackagesPageSteps {
    private Driver driver;
    private TireAndWheelPackagesPage tireAndWheelPackagesPage;
    private WebDriver webDriver;

    public TireAndWheelPackagesPageSteps(Driver driver) {
        this.driver = driver;
        tireAndWheelPackagesPage = new TireAndWheelPackagesPage(driver);
        webDriver = driver.getDriver();
    }

    @When("^I click on \"([^\"]*)\" in tire wheel package page$")
    public void i_click_on_element_in_wheel_package_page(String elementName) throws Throwable {
        driver.waitOneSecond();
        tireAndWheelPackagesPage.clickInTireWheelPackagePage(elementName);
    }

    @When("^I select (tire|wheel) of type \"([^\"]*)\" in wheel tire package$")
    public void i_select_of_type_in_wheel_tire_package(String type, String tireName) throws Throwable {
        if (type.equalsIgnoreCase("wheel")) {
            tireAndWheelPackagesPage.clickElementWithTextOnWheelTirePackage(TireAndWheelPackagesPage.wheelSelectionBy, tireName);
        } else if (type.equalsIgnoreCase("tire")) {
            tireAndWheelPackagesPage.clickElementWithTextOnWheelTirePackage(TireAndWheelPackagesPage.tiresSelectionBy, tireName);
        }
    }

    @When("^I verify the tire or wheel option \"([^\"]*)\" is (selected|not-selected)$")
    public void i_verify_the_tire_is_selected(String tireOrWheelName, String action) throws Throwable {
        tireAndWheelPackagesPage.selectedOrNotSelectedElementWithText(tireOrWheelName, action);
    }

    @When("^I close wheel and tire package popup$")
    public void i_close_wheel_and_tire_package_popup() throws Throwable {
        driver.isElementDisplayed(webDriver.findElement(dtModalHeaderBy).findElement(dtModalCloseBy));
    }


    @When("^I verify package filter has \"([^\"]*)\"$")
    public void i_verify_package_filter_has(String filterCategory) throws Throwable {
        driver.waitForElementVisible(TireAndWheelPackagesPage.filterCategory);
        Assert.assertTrue("FAIL: Filter didn't contain " + filterCategory, (TireAndWheelPackagesPage.filterCategory).getText().equalsIgnoreCase(filterCategory));
    }

    @When("^I verify wheel/tire package navigation has \"([^\"]*)\" field is complete$")
    public void i_verify_wheel_tire_package_navigation_has_field_is_complete(String wheelTireIcons) throws Throwable {
        tireAndWheelPackagesPage.wheelTireNavigationIcon(wheelTireIcons);
    }

    @And("^I verify product details of key value \"(.*?)\"$")
    public void i_verify_item_code_present_in_wheel_tire_package(String keyName) throws Throwable {
       tireAndWheelPackagesPage.verifyDataIntoWheelTirePopUp(keyName);
    }

    @When("^I save the product details with key value \"([^\"]*)\"$")
    public void i_save_the_product_details_with_key_value(String keyName) throws Throwable {
        tireAndWheelPackagesPage.saveProductDetailsWithKey(keyName);
    }

    @And("^I select \"(PREBUILT|BUILD YOUR OWN)\" on Select Tire and Wheel Package modal$")
    public void i_select_tire_and_wheel_package_option_from_modal(String option) throws Throwable {
        tireAndWheelPackagesPage.selectOptionFromSelectYourTireWheelPackageModal(option);
    }
}