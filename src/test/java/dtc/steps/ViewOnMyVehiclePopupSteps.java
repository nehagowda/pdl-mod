package dtc.steps;

import cucumber.api.java.en.Then;
import dtc.pages.ViewOnMyVehiclePopupPage;
import utilities.Driver;

public class ViewOnMyVehiclePopupSteps {

    private ViewOnMyVehiclePopupPage ViewOnMyVehiclePopupPage;

    public ViewOnMyVehiclePopupSteps(Driver driver) {
        ViewOnMyVehiclePopupPage = new ViewOnMyVehiclePopupPage(driver);
    }

    @Then("^I verify View On My Vehicle Popup page is displayed$")
    public void i_verify_view_on_my_vehicle_popup_page_is_displayed() throws Throwable {
        ViewOnMyVehiclePopupPage.verifyViewOnMyVehiclePopupPage();
    }

}
