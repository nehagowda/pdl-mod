package dtc.steps;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtc.pages.FitmentPage;
import dtc.pages.FitmentPopupPage;
import dtc.pages.WheelConfiguratorPage;
import utilities.Driver;

public class WheelConfiguratorSteps {

    private WheelConfiguratorPage wheelConfiguratorPage;
    private FitmentPage fitmentPage;
    private Driver driver;

    public WheelConfiguratorSteps(Driver driver) {
        this.driver = driver;
        wheelConfiguratorPage = new WheelConfiguratorPage(driver);
        fitmentPage = new FitmentPage(driver);
    }

    @Then("^I verify the Wheel Banner displayed on the Home page$")
    public void i_verify_wheel_banner() throws Throwable {
        wheelConfiguratorPage.assertWheelBanner();
    }

    @Then("^I should see \"(title|subtitle)\" \"([^\"]*)\" on the Wheel Configurator modal window displayed$")
    public void i_verify_wheel_config_page(String title, String expectedWheelPagelabel)
            throws Throwable {
        wheelConfiguratorPage.assertWheelConfigPageTitles(title, expectedWheelPagelabel);
    }

    @And("^I should see the wheel results displayed on the Wheel Configurator modal window$")
    public void i_verify_Wheel_results() throws Throwable {
        wheelConfiguratorPage.assertWheelResults();
    }

    @When("^I do a vehicle search with details \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" \"([^\"]*)\" on Wheel Configurator modal window$")
    public void i_do_a_vehicle_search_with_details(String year, String make, String model, String trim, String assembly) throws Throwable {
        fitmentPage.vehicleSearch(year, make, model, trim, assembly);
    }

    @And("^I should see vehicle \"([^\"]*)\" on the Wheel Configurator modal window$")
    public void i_verify_vehicle_by_model(String expectedVehicle) throws Throwable {
        wheelConfiguratorPage.assertVehicleByModel(expectedVehicle);
    }

    @And("^I should see the wheel has Image Name Brand Price displayed$")
    public void i_verify_wheel_properties() throws Throwable {
        wheelConfiguratorPage.assertWheelProperties();
    }

    @Then("^I verify the wheel count on the header should match with the displayed wheel results$")
    public void i_verify_the_wheel_count() throws Throwable {
        wheelConfiguratorPage.assertWheelCount();
    }

    @And("^I choose 'Change Vehicle' option on the Wheel Configurator modal window$")
    public void i_choose_change_vehicle() throws Throwable {
        wheelConfiguratorPage.clickEditVehicleIcon();
    }

    @Then("^I should see the default \"(Wheel Color|Wheel Size)\" in the Wheel Configurator modal window$")
    public void i_verify_default_Wheel_color_size(String option) throws Throwable {
        wheelConfiguratorPage.assertDefaultWheelSizeColor(option);
    }

    @And("^I choose \"(Year|Make|Model|Trim|Assembly)\" to view its options and set the value to \"([^\"]*)\"$")
    public void i_choose_wheel_config_options(String option,String value) throws Throwable {
        wheelConfiguratorPage.chooseWheelSize(option,value);
    }

    @And("^I choose FILTER option$")
    public void i_click_on_filters() throws Throwable {
        wheelConfiguratorPage.chooseFilter();
    }

    @Then("^I should see the wheel filter options on Wheel Configurator modal window$")
    public void i_should_see_wheel_filters() throws Throwable {
        wheelConfiguratorPage.assertWheelFilteringOptions();
    }

    @When("^I choose a \"([^\"]*)\" from the below list of brands$")
    public void i_Should_choose_a_Brand_from_the_list(String brandName) throws Throwable {
        wheelConfiguratorPage.selectBrand(brandName);
    }

    @And("^I choose a wheel on Wheel Configurator modal window$")
    public void i_choose_wheel_on_page() throws Throwable {
        wheelConfiguratorPage.selectWheelOnPage();
    }

    @And("^I choose a wheel on Wheel Configurator modal window with \"([^\"]*)\" button$")
    public void i_choose_wheel_with_specified_button_on_page(String buttonName) throws Throwable {
        wheelConfiguratorPage.selectWheelOnPage(buttonName);
    }

    @And("^I choose filter option \"([^\"]*)\" on Wheel Configurator modal window$")
    public void i_choose_filter_options(String option) throws Throwable {
        wheelConfiguratorPage.chooseFilterOptions(option);
    }

    @And("^I should see previously selected wheel should be on focus$")
    public void i_verify_previously_selected_Wheel_focus() throws Throwable {
        wheelConfiguratorPage.assertPreviouslySelectedWheel();
    }

    @And("^I should see previously added filter \"([^\"]*)\" is present$")
    public void i_verify_previously_addedFilter(String filterValue) throws Throwable {
        wheelConfiguratorPage.assertPreviousAppliedFilters(filterValue);
    }

    @Then("^I verify the 'Shopping cart' page has an label displayed for \"(In Store|To Ship)\" item as \"([^\"]*)\"$")
    public void i_verify_the_shopping_Cart_Page_label_for_item(String itemType, String label) throws Throwable {
        wheelConfiguratorPage.assertShoppingCartlabel(itemType, label);
    }

    @And("^I should see the Social Site \"(Email|Facebook|Twitter|Google Plus|Pinterest|Mobile|pinterest)\" option displayed$")
    public void i_verify_social_sites(String option) throws Throwable {
        wheelConfiguratorPage.assertSocialSites(option);
    }

    @When("^I should see the cart quantity is set to \"([^\"]*)\"$")
    public void i_verify_default_quantity(String quantity) throws Throwable {
        wheelConfiguratorPage.assertQuantity(quantity);
    }

    @And("^I select the Social Site \"(Email|Facebook|Twitter|Google Plus|Pinterest|Mobile)\"$")
    public void i_select_social_sites(String option) throws Throwable {
        wheelConfiguratorPage.chooseSocialSites(option);
    }

    @And("^I verify the Wheel Configurator filters are \"(displayed|not displayed)\"$")
    public void i_verify_the_wheel_configurator_filters_are_displayed_or_not_displayed(String displayStatus) {
        wheelConfiguratorPage.verifyWheelConfiguratorFiltersDisplayedNotDisplayed(displayStatus);
    }

    @When("^I close view on my vehicle modal$")
    public void i_close_view_on_my_vehicle_modal() {
        driver.jsScrollToElementClick(FitmentPopupPage.fitmentPopupCloseIcon);
    }

    @And("^I choose a wheel \"([^\"]*)\" \"([^\"]*)\" on Wheel Configurator modal window$")
    public void i_choose_wheel_on_wheel_configurator_modal_window(String wheelBrand, String wheelName) throws Throwable {
        wheelConfiguratorPage.selectWheelOnPage(wheelBrand, wheelName);
    }

    @Then("^I select a wheel size \"([^\"]*)\" on Wheel Configurator modal window$")
    public void i_select_a_wheel_size_on_wheel_configurator_modal_window(String wheelSize) throws Throwable {
        wheelConfiguratorPage.selectWheelSize(wheelSize);
    }

    @And("^I verify \"([^\"]*)\" text is present in the vehicle image on Wheel Configurator modal window$")
    public void i_verify_text_is_present_in_the_vehicle_image_on_wheel_configurator_modal_window(String text) throws Throwable {
        wheelConfiguratorPage.verifyImageText(text);
    }

    @When("^I select \"(share|close|view details)\" option on wheel configurator window$")
    public void i_select_option_on_wheel_configurator_window(String option) throws Throwable {
        wheelConfiguratorPage.chooseWheelConfigOption(option);
    }

    @And("^I enter email address \"([^\"]*)\" on Wheel Configurator page$")
    public void i_enter_email_address_on_wheel_configurator_page(String emailAddress) throws Throwable {
        wheelConfiguratorPage.enterEmailAddress(emailAddress);
    }

    @And("^I enter phone number \"([^\"]*)\" on Wheel Configurator page$")
    public void i_enter_phone_number_on_wheel_configurator_page(String phoneNumber) throws Throwable {
        wheelConfiguratorPage.enterPhoneNumber(phoneNumber);
    }

}
