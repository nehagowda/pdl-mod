package fleet.steps;

import common.Constants;
import cucumber.api.java.en.Then;
import fleet.pages.Fleet;
import cucumber.api.java.en.When;
import org.openqa.selenium.Keys;
import sap.pages.CommonActions;
import utilities.Driver;

/**
 * Created by mnabizadeh on 04/02/19.
 */

public class FleetSteps {

    private Driver driver;
    private Fleet fleet;
    private CommonActions commonActions;

    public FleetSteps(Driver driver) {
        this.driver = driver;
        fleet = new Fleet(driver);
        commonActions = new CommonActions(driver);

    }

    @When("^I enter \"([^\"]*)\" in \"([^\"]*)\" field$")
    public void i_enter_in_field(String value, String element) throws Throwable {
        fleet.sendKeys(fleet.returnElement(element), value);
    }

    @When("^I make a post method call to GRAPHQL and request file with name \"([^\"]*)\" and I save the repair number for store \"([^\"]*)\"$")
    public void i_make_a_post_method_call_to_graphql_and_request_file_with_name_and_I_save_the_repair_number(String fileName, String store) throws Throwable {
        fleet.getFleetRepairNumber(fileName, store);
    }

    @When("^I enter the previously saved repair number in \"([^\"]*)\" field$")
    public void i_enter_the_previously_saved_repair_number_in_field(String labelName) throws Throwable {
        commonActions.sendKeysWithLabelName(labelName, driver.scenarioData.getData(Constants.REPAIR_NUMBER));
    }

    @When("I enter Tread depth value for O - \"([^\"]*)\" M - \"([^\"]*)\" I - \"([^\"]*)\" for Tire at location - \"([^\"]*)\" in Vehicle Details page")
    public void i_enter_tread_depth_for_tire_at_location_in_vehicle_details_page(String oValue, String mValue, String iValue, String tireLocation) throws Throwable {
        fleet.sendTreadDepthValues(tireLocation, Fleet.O, oValue);
        fleet.sendTreadDepthValues(tireLocation, Fleet.M, mValue);
        fleet.sendTreadDepthValues(tireLocation, Fleet.I, iValue);
    }

    @When("I enter random Tread depth values for Tire at location \"([^\"]*)\" in Vehicle Details page")
    public void i_enter_tread_depth_for_tire_at_location_in_vehicle_details_page(String tireLocation) throws Throwable {
        fleet.generateSaveAndEnterRandomValuesForTreadDepth(Fleet.O, tireLocation);
        fleet.generateSaveAndEnterRandomValuesForTreadDepth(Fleet.M, tireLocation);
        fleet.generateSaveAndEnterRandomValuesForTreadDepth(Fleet.I, tireLocation);
    }

    @When("I click on \"([^\"]*)\" on fleet app")
    public void i_click_on_fleet_app(String elementName) throws Throwable {
        fleet.click(fleet.returnElement(elementName));
    }

    @When("I click on replace check box for tire \"([^\"]*)\"")
    public void i_click_on_replace_check_box_for_tire(String tireLocation) throws Throwable {
        fleet.click(fleet.returnReplaceTireElement(tireLocation));
    }

    @Then("I verify the text of \"([^\"]*)\" field is \"([^\"]*)\" in fleet app")
    public void i_verify_the_text_of_field_is_in_fleet_app(String field, String assertValue) throws Throwable {
        driver.waitForPageToLoad();
        driver.verifyTextDisplayedCaseInsensitive(fleet.returnVehicleOrInvoiceDetailsElement(field), assertValue);
    }

    @When("I hit TAB key")
    public void i_hit_tab_key() throws Throwable {
        driver.performKeyAction(Keys.TAB);
    }

    @Then("I verify the text of \"([^\"]*)\" field in fleet app")
    public void i_verify_the_text_of_field_is_in_fleet_app(String field) throws Throwable {
        driver.verifyTextDisplayedCaseInsensitive(fleet.returnVehicleOrInvoiceDetailsElement(field),
                driver.scenarioData.getData(Fleet.FLEET_REPAIR_ID_KEY));
    }

    @When("I save \"([^\"]*)\" to scenario data")
    public void i_save_field_to_scenario_data(String fieldName) throws Throwable {
        fleet.saveFieldDataToScenarioData(fieldName);
    }

    @Then("I verify the text of \"([^\"]*)\" field is \"([^\"]*)\" in summary")
    public void i_verify_the_text_of_field_in_fleet(String elementName, String value) throws Throwable {
        driver.waitForPageToLoad();
        driver.verifyTextDisplayedCaseInsensitive(fleet.returnElement(elementName), value);
    }

    @When("I click on reason drop down for tire at location \"([^\"]*)\" and select \"([^\"]*)\" as reason")
    public void i_click_on_reason_drop_down_for_the_tire_at_location_and_select_as_reason(String tireLocation, String reason) throws Throwable {
        fleet.selectReasonForReplacement(tireLocation, reason);
    }

    @When("I save all the line item details")
    public void i_save_all_the_line_item_details() throws Throwable {
        fleet.determineNumberOfLineItemsAndSaveTheAttributes();
        fleet.saveFinalInvoiceValues();
    }
}