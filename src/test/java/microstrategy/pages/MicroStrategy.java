package microstrategy.pages;

import cucumber.api.Scenario;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;
import java.util.logging.Logger;


/**
 * Created by mnabizadeh on 04/09/19.
 */

public class MicroStrategy {

    private Driver driver;
    private WebDriver webDriver;
    private Scenario scenario;
    private CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(MicroStrategy.class.getName());

    public MicroStrategy(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//input[@value='Login']")
    public static WebElement login;

    @FindBy(xpath="//a[@class='mstrLargeIconViewItemLink']")
    public static WebElement dtStoreOperations;

    @FindBy(xpath="//td[@class='mstrLargeIconViewItemText']//a[@title='Run Document']")
    public static WebElement dtcExecutiveDashboard;

    @FindBy(xpath = "//img[contains(@src,'company')]")
    public static WebElement companyOperations;

    @FindBy(className = "mstrCalendarAndTimePickerCellCalendarButton")
    public static WebElement yourSelection;

    @FindBy(id = "id_mstr191_year")
    public static WebElement year;

    @FindBy(xpath = "//*[@title='AZP:Arizona']")
    public static WebElement region;

    @FindBy(xpath = "//input[@value='Run Document']")
    public static WebElement runDocument;

    @FindBy(id = "id_mstr191_move_prev")
    public static WebElement previousMonth;

    @FindBy(xpath = "//*[text()=2][not (contains(@class,'mstrPromptTOCListItemIndex'))][not (contains(@class,'mstrPromptQuestionTitleBarIndex'))]" )
    public static WebElement secondDay;

    @FindBy(xpath = " //div[contains(text(),'UNITS')]" )
    public static WebElement unit;

    @FindBy(xpath = " //div[contains(text(),'NET SALES')]" )
    public static WebElement netSales;

    @FindBy(xpath = " //div[contains(text(),'GROSS PROFIT')]" )
    public static WebElement grossProfit;

    @FindBy(xpath = " //div[contains(text(),'LABOR EFFICIENCY')]" )
    public static WebElement laborEfficiency;

    @FindBy(xpath = " //div[contains(text(),'RECOMMEND CDI')]" )
    public static WebElement recommendCDI;

    @FindBy(xpath = "//span[contains(text(),'AZP')]" )
    public static WebElement azp;

    @FindBy(xpath = "//div[contains(text(),'Company Operations')]" )
    public static WebElement companyOperationTitle;

    public static final String LOGIN = "Login";
    public static final String DT_STORE_OPERATIONS = "DT Store Operations";
    public static final String DTC_EXECUTIVE_DASHBOARD = "DTC Executive Dashboard";
    public static final String COMPANY_OPERATIONS = "Company Operations";
    public static final String YOUR_SELECTION = "Your selection";
    public static final String YEAR = "Year";
    public static final String REGION = "AZP:Arizona";
    public static final String RUN_DOCUMENT = "Run Document";
    public static final String PREVIOUS_MONTH = "previous month";
    public static final String SECOND_DAY = "2";
    public static final String UNIT = "UNITS";
    public static final String NET_SALES = "NET SALES";
    public static final String GROSS_PROFIT = "GROSS PROFIT";
    public static final String LABOR_EFFICIENCY = "LABOR EFFICIENCY";
    public static final String RECOMMEND_CDI = "RECOMMEND CDI";
    public static final String AZP = "AZP";
    public static final String COMPANY_OPERATION_TITLE = "Company Operations Title";

    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started");
        switch (elementName) {
            case LOGIN:
                return login;
            case DT_STORE_OPERATIONS:
                return dtStoreOperations;
            case DTC_EXECUTIVE_DASHBOARD:
                return dtcExecutiveDashboard;
            case COMPANY_OPERATIONS:
                return companyOperations;
            case YOUR_SELECTION:
                return yourSelection;
            case YEAR:
                return year;
            case REGION:
                return region;
            case RUN_DOCUMENT:
                return runDocument;
            case PREVIOUS_MONTH:
                return previousMonth;
            case SECOND_DAY:
                return secondDay;
            case UNIT:
                return unit;
            case NET_SALES:
                return netSales;
            case GROSS_PROFIT:
                return grossProfit;
            case LABOR_EFFICIENCY:
                return laborEfficiency;
            case RECOMMEND_CDI:
                return recommendCDI;
            case AZP:
                return azp;
            case COMPANY_OPERATION_TITLE:
                return companyOperationTitle;
            default:
                Assert.fail("FAIL: Could not find icon that matched string passed from step");
                return null;
        }
    }

    /**
     * Clicks on text based on an element
     *
     * @param elementName - Name of the Web Element
     *
     */
    public void clickOnText(String elementName) {
        LOGGER.info("clickOnText started");
        commonActions.click(returnElement(elementName));
        LOGGER.info("clickOnText completed");
    }
}