package orderxmls.pages;

import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import dtc.data.Customer;
import dtc.pages.CommonActions;
import org.junit.Assert;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import utilities.Driver;
import webservices.pages.WebServices;
import utilities.CommonUtils;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.logging.Logger;


/**
 * Created by csahoo on 7/15/18.
 */

public class OrderXmls {
    private Driver driver;
    private final Logger LOGGER = Logger.getLogger(WebServices.class.getName());
    private Customer customer;
    private CommonActions commonActions;
    private Config config;

    public OrderXmls(Driver driver) {
        customer = new Customer();
        this.driver = driver;
        config = new Config();
        commonActions = new CommonActions(driver);
    }

    public static ArrayList<String> customerDataList =
            new ArrayList<>(Arrays.asList(OrderXmls.NODE_FIRST_NAME, OrderXmls.NODE_LAST_NAME,
                    OrderXmls.NODE_ADDRESS_LINE1, Constants.CITY, ConstantsDtc.STATE, OrderXmls.NODE_ZIPCODE,
                    OrderXmls.NODE_COUNTRY_CODE, ConstantsDtc.PHONE_TYPE, OrderXmls.NODE_PHONE_NUMBER,
                    OrderXmls.EMAIL_ADDRESS, OrderXmls.NODE_SMS_OPT_IN));
    public static ArrayList<String> bopisCustomerDataList =
            new ArrayList<>(Arrays.asList(OrderXmls.NODE_FIRST_NAME, OrderXmls.NODE_LAST_NAME,
                    ConstantsDtc.PHONE_TYPE, OrderXmls.NODE_PHONE_NUMBER, OrderXmls.EMAIL_ADDRESS,
                    OrderXmls.NODE_SMS_OPT_IN));
    public static ArrayList<String> dtPaymentNodeList =
            new ArrayList<>(Arrays.asList(OrderXmls.CARD_TOKEN, OrderXmls.EXPIRATION_DATE, OrderXmls.PAYMENT_TYPE,
                    OrderXmls.AMOUNT, OrderXmls.NODE_ADDRESS_LINE1, Constants.CITY, Constants.STATE,
                    OrderXmls.NODE_ZIPCODE, Constants.TYPE, OrderXmls.NODE_COUNTRY_CODE, OrderXmls.PAYMENT_TXN_TYPE));
    public static ArrayList<String> dtdPaymentNodeList =
            new ArrayList<>(Arrays.asList(OrderXmls.CARD_TOKEN, OrderXmls.EXPIRATION_DATE, OrderXmls.PAYMENT_TYPE,
                    OrderXmls.AMOUNT, OrderXmls.NODE_ADDRESS_LINE1, Constants.CITY, Constants.STATE,
                    OrderXmls.NODE_ZIPCODE, Constants.TYPE, OrderXmls.NODE_COUNTRY_CODE));
    public static ArrayList<String> paypalPaymentNodeList =
            new ArrayList<>(Arrays.asList(OrderXmls.CARD_TOKEN, OrderXmls.PAYMENT_TYPE,
                    OrderXmls.AMOUNT, OrderXmls.PAYMENT_TXN_TYPE, OrderXmls.REFERENCE_NUMBER));

    public static ArrayList<String> serviceOrdersList =
            new ArrayList<>(Arrays.asList("84802", "98911", "80400", "80225", "90079", "80222", "80487", "80087"));
    public static Element rootElement;
    private static final String WEB_ORDER_ID = "webOrderId";
    private static final String TXN_TIME_STAMP = "transactionTimestamp";
    public static final String EMAIL_ADDRESS = "emailAddress";
    private static final String WEB_ORDER_ORIGIN = "webOrderOrigin";
    private static final String SHIP_METHOD = "shipMethod";
    private static final String SITE_NUMBER = "siteNumber";
    private static final String AMOUNT_OF_SALE = "amountOfSale";
    private static final String AMOUNT = "amount";
    private static final String PROMOTION_ARTICLES = "promotionArticle";
    public static final String PROMOTION_DISCOUNT = "discount";
    private static final String ORDER_TYPE = "orderType";
    private static final String CUSTOMER_TYPE = "customerType";
    public static final String PRODUCTS = "productCode";
    private static final String VEHICLE = "vehicle";
    public static final String YEAR = "year";
    public static final String MAKE = "make";
    public static final String MODEL = "model";
    public static final String TRIM = "trim";
    private static final String TYPE = "type";
    public static final String TAX = "tax";
    public static final String ITEM_TAX = "Item tax";
    public static final String SUBITEM_TAX = "Subitem tax";
    public static final String PROMOTION_TAX = "Promotion tax";
    public static final String ITEM_TAX_TYPE = "Item tax type";
    public static final String SUBITEM_TAX_TYPE = "Subitem tax type";
    public static final String PROMOTION_TAX_TYPE = "Promotion tax type";
    private static final String PRODUCTS_ACCESSORIES = "accessories";
    private static final String SALES_ORDER_HEADER = "/SalesOrder/header/";
    public static final String SALES_ORDER_CUSTOMER = "/SalesOrder/header/customer/";
    private static final String ADDRESS_NODE = "address/";
    public static final String PHONE_NODE = "phone/";
    private static final String SALES_ORDER_PROMOTION = "/SalesOrder/promotion/";
    private static final String SALES_ORDER_ITEM = "/SalesOrder/item/";
    public static final String NODE_PATH_BUNDLE_TYPE = SALES_ORDER_ITEM + "bundleType";
    private static final String SALES_ORDER_SUBITEM = "/SalesOrder/item/subItem/";
    public static final String SALES_ORDER_PAYMENT = "/SalesOrder/payment/";
    public static final String BILLING_ADDRESS = "billingAddress/";
    private static final String SALES_ORDER_VEHICLE = SALES_ORDER_CUSTOMER + VEHICLE + "/";
    private static final String SHIPPING_METHOD = "shipping/shipMethod";
    private static final String TAX_AMOUNT = "tax/amount";
    private static final String TAX_TYPE = "tax/type";
    public static final String NODE_FIRST_NAME = "firstName";
    public static final String NODE_LAST_NAME = "lastName";
    public static final String NODE_ADDRESS_LINE1 = "addressLine1";
    public static final String NODE_ADDRESS_LINE2 = "addressLine2";
    public static final String NODE_ZIPCODE = "zipcode";
    public static final String NODE_COUNTRY_CODE = "countryCode";
    public static final String NODE_PHONE_NUMBER = "phoneNumber";
    public static final String NODE_SMS_OPT_IN = "smsOptIn";
    public static final String NODE_AREA_CODE = "areaCode";
    public static final String CARD_TOKEN = "cardToken";
    public static final String REFERENCE_NUMBER = "referenceNumber";
    public static final String EXPIRATION_DATE = "expirationDate";
    public static final String PAYMENT_TYPE = "paymentType";
    public static final String RETAIL_PRICE = "retailPrice";
    public static final String SALES_EXTENDED = "salesExtended";
    public static final String PAYMENT_TXN_TYPE = "paymentTransactionType";
    public static final String PRODUCT_DESCRIPTION = "productDescription";
    public static final String SUBITEM = "subItem";
    public static final String APPOINTMENT_DATE = "appointmentDate";
    public static final String APPOINTMENT_START_TIME = "appointmentStartTime";
    public static final String APPOINTMENT_END_TIME = "appointmentEndTime";
    public static final String APPOINTMENT_ID = "appointmentId";
    public static final String APPOINTMENT = "appointment/";
    public static final String CANCELLATION_URL = "cancellationURL";
    public static final String ORDER_CANCEL_URL = "OrderCancelUrl";
    XPath xPath = XPathFactory.newInstance().newXPath();

    /**
     * Get the root element of the order XML file
     *
     * @param orderXmlFile order xml created by Hybris
     */
    public void getRootElement(String orderXmlFile) throws Throwable {
        LOGGER.info("getRootElement started");
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document document = dBuilder.parse(new InputSource(new StringReader(orderXmlFile)));
        rootElement = document.getDocumentElement();
        LOGGER.info("getRootElement completed");
    }

    /**
     * Get xml node value from order xml
     *
     * @param rootElement root element of the order xml file
     * @param nodeName    node name in xml file
     */
    public String getXmlNodeValue(Element rootElement, String nodeName) throws Throwable {
        LOGGER.info("getXmlNodeValue started for " + nodeName);
        String nodePath;
        if (nodeName.startsWith("/")) {
            nodePath = nodeName;
        } else {
            nodePath = returnNodePath(nodeName);
        }
        Node nodeElement = (Node) xPath.evaluate(nodePath, rootElement, XPathConstants.NODE);
        String nodeValue = nodeElement.getTextContent();
        LOGGER.info("getXmlNodeValue completed for " + nodeName);
        return nodeValue;
    }

    /**
     * Get xml node list from order xml
     *
     * @param rootElement root element of the order xml file
     * @param nodeName    node name in xml file
     */
    public NodeList getXmlNodeList(Element rootElement, String nodeName) throws Throwable {
        LOGGER.info("getXmlNodeList started for " + nodeName);
        String nodePath = returnNodePath(nodeName);
        NodeList nodeElements = (NodeList) xPath.evaluate(nodePath, rootElement, XPathConstants.NODESET);
        LOGGER.info("getXmlNodeList completed for " + nodePath);
        return nodeElements;
    }

    /**
     * Return node path from order xml based on matched node name
     *
     * @param nodeName name of the node element on order xml
     */
    public String returnNodePath(String nodeName) throws Throwable {
        LOGGER.info("returnNodePath started for " + nodeName);
        switch (nodeName) {
            case WEB_ORDER_ID:
                return SALES_ORDER_HEADER + WEB_ORDER_ID;
            case TXN_TIME_STAMP:
                return SALES_ORDER_HEADER + TXN_TIME_STAMP;
            case SITE_NUMBER:
                return SALES_ORDER_HEADER + SITE_NUMBER;
            case AMOUNT_OF_SALE:
                return SALES_ORDER_HEADER + AMOUNT_OF_SALE;
            case PROMOTION_ARTICLES:
                return SALES_ORDER_PROMOTION + PROMOTION_ARTICLES;
            case PROMOTION_DISCOUNT:
                return SALES_ORDER_PROMOTION + PROMOTION_DISCOUNT;
            case ORDER_TYPE:
                return SALES_ORDER_HEADER + ORDER_TYPE;
            case PRODUCTS:
                return SALES_ORDER_ITEM + PRODUCTS;
            case PRODUCTS_ACCESSORIES:
                return SALES_ORDER_SUBITEM + PRODUCTS;
            case CUSTOMER_TYPE:
                return SALES_ORDER_CUSTOMER + TYPE;
            case EMAIL_ADDRESS:
                return SALES_ORDER_CUSTOMER + EMAIL_ADDRESS;
            case WEB_ORDER_ORIGIN:
                return SALES_ORDER_HEADER + WEB_ORDER_ORIGIN;
            case SHIP_METHOD:
                return SALES_ORDER_ITEM + SHIPPING_METHOD;
            case SUBITEM + " " + PRODUCTS:
                return SALES_ORDER_SUBITEM + PRODUCTS;
            case YEAR:
            case MAKE:
            case MODEL:
            case TRIM:
                return SALES_ORDER_VEHICLE + nodeName;
            case ITEM_TAX_TYPE:
                return SALES_ORDER_ITEM + TAX_TYPE;
            case SUBITEM_TAX_TYPE:
                return SALES_ORDER_SUBITEM + TAX_TYPE;
            case PROMOTION_TAX_TYPE:
                return SALES_ORDER_PROMOTION + TAX_TYPE;
            case ITEM_TAX:
                return SALES_ORDER_ITEM + TAX_AMOUNT;
            case SUBITEM_TAX:
                return SALES_ORDER_SUBITEM + TAX_AMOUNT;
            case PROMOTION_TAX:
                return SALES_ORDER_PROMOTION + TAX_AMOUNT;
            case APPOINTMENT_DATE:
                return SALES_ORDER_HEADER + APPOINTMENT + APPOINTMENT_DATE;
            case APPOINTMENT_START_TIME:
                return SALES_ORDER_HEADER + APPOINTMENT + APPOINTMENT_START_TIME;
            case APPOINTMENT_END_TIME:
                return SALES_ORDER_HEADER + APPOINTMENT + APPOINTMENT_END_TIME;
            case APPOINTMENT_ID:
                return SALES_ORDER_HEADER + APPOINTMENT + APPOINTMENT_ID;
            case CANCELLATION_URL:
                return SALES_ORDER_HEADER + CANCELLATION_URL;
            default:
                Assert.fail("FAIL: Could not find node name that matched string passed from step: " + nodeName);
                return null;
        }
    }

    /**
     * Return expected values to be verified on order Xml.
     *
     * @param value expected value for order
     */
    public String returnExpectedValueForOrder(String value) throws Throwable {
        LOGGER.info("returnExpectedValueForOrder started for ");
        String returnVal = "";
        switch (value) {
            case WEB_ORDER_ID:
                return driver.scenarioData.getCurrentOrderNumber();
            case SITE_NUMBER:
                return driver.scenarioData.genericData.get(Constants.STORE_CODE);
            case AMOUNT_OF_SALE:
                return driver.scenarioData.genericData.get(Constants.ORDER_TOTAL);
            case PROMOTION_DISCOUNT:
                return driver.scenarioData.cartInstantPromotionPrice.toString();
            case ORDER_TYPE:
                return driver.scenarioData.genericData.get(Constants.ORDER_TYPE);
            case CUSTOMER_TYPE:
                returnVal = driver.scenarioData.genericData.get(Constants.CUSTOMER_TYPE);
                if (returnVal == null)
                    returnVal = ConstantsDtc.GUEST_CUSTOMER;
                return returnVal;
            case WEB_ORDER_ORIGIN:
                returnVal = Config.getSiteRegion();
                if (!returnVal.equalsIgnoreCase(ConstantsDtc.DTD))
                    returnVal = Constants.STORE.toLowerCase();
                return returnVal;
            case SHIP_METHOD:
                return driver.scenarioData.genericData.get(Constants.SHIPPING_METHOD);
            case VEHICLE:
                return driver.scenarioData.genericData.get(VEHICLE);
            case TAX:
                return driver.scenarioData.genericData.get(ConstantsDtc.TAX);
            case APPOINTMENT_DATE:
                return driver.scenarioData.genericData.get(APPOINTMENT_DATE);
            case APPOINTMENT_START_TIME:
                return driver.scenarioData.genericData.get(APPOINTMENT_START_TIME);
            default:
                Assert.fail("FAIL: Could not find node name that matched string passed from step");
                return null;
        }
    }

    public ArrayList<String> returnExpectedProductCodes() throws Throwable {
        LOGGER.info("returnExpectedProductCodes started");
        ArrayList<String> productCodes = new ArrayList<>();
        for (int i = 0; i < driver.scenarioData.productInfoList.size(); i++) {
            if (Boolean.parseBoolean(commonActions.productInfoListGetValue(ConstantsDtc.IN_CART, i))) {
                productCodes.add(commonActions.productInfoListGetValue(ConstantsDtc.ITEM, i));
            }
        }
        LOGGER.info("returnExpectedProductCodes completed");
        return productCodes;
    }

    /**
     * Assert expected value in order xml
     *
     * @param expectedValue expected node value
     * @param actualValue   actual node value from order xml
     * @param nodeName      webOrderId or siteNumber or amountOfSale or orderType - Covers for all single node value
     */
    public void assertOrderXmlValue(String expectedValue, String actualValue, String nodeName) throws Throwable {
        LOGGER.info("assertOrderXmlValue started for node " + nodeName +
                ". Actual value = " + actualValue + ". Expected value = " + expectedValue);
        if (nodeName.contains(NODE_PHONE_NUMBER)) {
            expectedValue = expectedValue.replaceAll("-","");
        }
        if (nodeName.contains(NODE_ZIPCODE)) {
            Assert.assertTrue("FAIL: Expected value for " + nodeName +
                            " is: " + expectedValue + " does not match to actual value: " + actualValue,
                    actualValue.contains(expectedValue));
        } else {
            Assert.assertTrue("FAIL: Expected value for " + nodeName +
                            " is: " + expectedValue + " does not match to actual value: " + actualValue,
                    actualValue.equalsIgnoreCase(expectedValue));
        }
        LOGGER.info("assertOrderXmlValue completed for node " + nodeName +
                ". Actual value = " + actualValue + ". Expected value = " + expectedValue);
    }

    /**
     * Assert expected order entries with order xml list values
     *
     * @param expectedValue expected node value
     * @param actualValue   actual node value from order xml
     * @param nodeName      productCode or discounts - Covers for all multiple node values
     */
    public void assertOrderXmlValues(String expectedValue, String actualValue, String nodeName) throws Throwable {
        LOGGER.info("assertOrderXmlValues started for node " + nodeName +
                ". Actual value = " + actualValue + ". Expected value = " + expectedValue);
        Assert.assertTrue("FAIL: Expected value for " + nodeName +
                        " is: " + expectedValue + " does not contain actual value: " + actualValue,
                expectedValue.equals(actualValue));
        LOGGER.info("assertOrderXmlValues completed for node " + nodeName +
                ". Actual value = " + actualValue + ". Expected value = " + expectedValue);
    }

    /**
     * Assert node value in order xml is not null
     *
     * @param actualValue actual node value from order xml
     * @param nodeName    node name in order xml
     */
    public void assertOrderXmlValueNotNull(String actualValue, String nodeName) throws Throwable {
        LOGGER.info("assertOrderXmlValueNotNull started for node " + nodeName);
        Assert.assertTrue("FAIL: Order xml value for " + nodeName +
                " is null!", actualValue != null);
        LOGGER.info("assertOrderXmlValueNotNull completed for node " + nodeName);
    }

    /**
     * return nodepath, actual value and expecetd value for customer data nodes from order xml
     *
     * @param node customer node name in order XML
     */
    public String returnCustomerNodeValues(String node) throws Throwable {
        LOGGER.info("returnCustomerNodeValues started for " + node);
        String nodePath = null;
        String nodeName = null;
        String expectedValue;
        switch (node) {
            case NODE_FIRST_NAME:
                nodeName = CommonActions.FIRST_NAME;
                nodePath = SALES_ORDER_CUSTOMER + NODE_FIRST_NAME;
                break;
            case NODE_LAST_NAME:
                nodeName = CommonActions.LAST_NAME;
                nodePath = SALES_ORDER_CUSTOMER + NODE_LAST_NAME;
                break;
            case NODE_ADDRESS_LINE1:
                nodeName = CommonActions.ADDRESS_LINE_1;
                nodePath = SALES_ORDER_CUSTOMER + ADDRESS_NODE + NODE_ADDRESS_LINE1;
                break;
            case NODE_ADDRESS_LINE2:
                nodeName = CommonActions.ADDRESS_LINE_2;
                nodePath = SALES_ORDER_CUSTOMER + ADDRESS_NODE + NODE_ADDRESS_LINE2;
                break;
            case NODE_ZIPCODE:
                nodeName = ConstantsDtc.ZIP_CODE;
                nodePath = SALES_ORDER_CUSTOMER + ADDRESS_NODE + NODE_ZIPCODE;
                break;
            case NODE_COUNTRY_CODE:
                nodeName = ConstantsDtc.COUNTRY;
                nodePath = SALES_ORDER_CUSTOMER + ADDRESS_NODE + NODE_COUNTRY_CODE;
                break;
            case EMAIL_ADDRESS:
                nodeName = CommonActions.EMAIL_ADDRESS;
                nodePath = SALES_ORDER_CUSTOMER + EMAIL_ADDRESS;
                break;
            case NODE_SMS_OPT_IN:
                nodePath = SALES_ORDER_CUSTOMER + NODE_SMS_OPT_IN;
                break;
            case ConstantsDtc.PHONE_TYPE:
                nodeName = ConstantsDtc.PHONE_TYPE;
                nodePath = SALES_ORDER_CUSTOMER + PHONE_NODE + TYPE;
                break;
            case NODE_PHONE_NUMBER:
                nodeName = Constants.PHONE_NUMBER;
                nodePath = SALES_ORDER_CUSTOMER + PHONE_NODE + NODE_PHONE_NUMBER;
                break;
            case Constants.PHONE + NODE_COUNTRY_CODE:
                nodePath = SALES_ORDER_CUSTOMER + PHONE_NODE + NODE_COUNTRY_CODE;
                break;
            case Constants.CITY:
                nodeName = Constants.CITY;
                nodePath = SALES_ORDER_CUSTOMER + ADDRESS_NODE + Constants.CITY.toLowerCase();
                break;
            case ConstantsDtc.STATE:
                nodeName = ConstantsDtc.STATE;
                nodePath = SALES_ORDER_CUSTOMER + ADDRESS_NODE + ConstantsDtc.STATE.toLowerCase();
                break;
            default:
                Assert.fail("FAIL: Could not find node name that matched string passed from step");
        }
        String actualValue = getXmlNodeValue(rootElement, nodePath);
        if (node.equalsIgnoreCase(NODE_PHONE_NUMBER)) {
            actualValue = getXmlNodeValue(rootElement, SALES_ORDER_CUSTOMER + PHONE_NODE + NODE_AREA_CODE) +
                    getXmlNodeValue(rootElement, nodePath);
        }
        expectedValue = driver.scenarioData.genericData.get(nodeName);
        if (node.equalsIgnoreCase(Constants.PHONE + NODE_COUNTRY_CODE)) {
            expectedValue = "+1";
        } else if (node.equalsIgnoreCase(NODE_SMS_OPT_IN)) {
            expectedValue = CommonActions.FALSE;
        }
        String nodeValues = actualValue + "&" + expectedValue + "#" + nodePath;
        LOGGER.info("returnCustomerNodeValues completed for " + node);
        return nodeValues;
    }

    /**
     * return Prod Code, Prod quantity, Retails Price, Total price for subItem nodes from order xml
     *
     * @param list subItem child node List
     */
    public String returnSubItemNodeValues(NodeList list) throws Throwable {
        LOGGER.info("returnSubItemNodeValues started");
        String subItemProdCode = list.item(0).getTextContent();
        String subItemProdQuantity = list.item(1).getTextContent();
        String subItemProdRetailPrice = list.item(5).getTextContent();
        String subItemProdTotalPrice = list.item(6).getTextContent();
        String actualSubItemProductValues = PRODUCTS + ":" + subItemProdCode + "," + Constants.JSON_PARAM_QUANTITY + ":"
                + subItemProdQuantity + "," + RETAIL_PRICE + ":" + subItemProdRetailPrice + "," + SALES_EXTENDED + ":" +
                subItemProdTotalPrice;
        LOGGER.info("returnSubItemNodeValues completed");
        return actualSubItemProductValues;
    }

    /**
     * return Prod Code, Prod quantity, Retails Price, Total price for Item nodes from order xml
     *
     * @param list subItem child node List
     */
    public String returnItemNodeValues(NodeList list) throws Throwable {
        LOGGER.info("returnItemNodeValues started");
        String itemProdCode = list.item(2).getTextContent();
        String itemProdQunatity = list.item(3).getTextContent();
        String itemProdRetailPrice = list.item(7).getTextContent();
        String itemProdSalesExtended = list.item(8).getTextContent();
        String actualitemProductValues = PRODUCTS + ":" + itemProdCode + "," + Constants.JSON_PARAM_QUANTITY + ":"
                + itemProdQunatity + "," + RETAIL_PRICE + ":" + itemProdRetailPrice + "," + SALES_EXTENDED + ":" +
                itemProdSalesExtended;
        LOGGER.info("returnItemNodeValues completed");
        return actualitemProductValues;
    }

    /**
     * return nodepath, actual value and expecetd value for customer data nodes from order xml
     *
     * @param node customer node name in order XML
     */
    public String returnPaymentNodeValues(String node) throws Throwable {
        LOGGER.info("returnPaymentNodeValues started for " + node);
        String nodePath = null;
        String nodeName = null;
        String expectedValue;
        switch (node) {
            case CARD_TOKEN:
                nodeName = CARD_TOKEN;
                nodePath = SALES_ORDER_PAYMENT + CARD_TOKEN;
                break;
            case EXPIRATION_DATE:
                nodeName = EXPIRATION_DATE;
                nodePath = SALES_ORDER_PAYMENT + EXPIRATION_DATE;
                break;
            case PAYMENT_TYPE:
                nodeName = PAYMENT_TYPE;
                nodePath = SALES_ORDER_PAYMENT + PAYMENT_TYPE;
                break;
            case AMOUNT:
                nodePath = SALES_ORDER_PAYMENT + AMOUNT;
                break;
            case NODE_ADDRESS_LINE1:
                nodeName = Constants.Billing + " " + CommonActions.ADDRESS_LINE_1;
                nodePath = SALES_ORDER_PAYMENT + BILLING_ADDRESS + NODE_ADDRESS_LINE1;
                break;
            case NODE_ADDRESS_LINE2:
                nodeName = Constants.Billing + " " + CommonActions.ADDRESS_LINE_2;
                nodePath = SALES_ORDER_PAYMENT + BILLING_ADDRESS + NODE_ADDRESS_LINE2;
                break;
            case NODE_ZIPCODE:
                nodeName = Constants.Billing + " " + ConstantsDtc.ZIP_CODE;
                nodePath = SALES_ORDER_PAYMENT + BILLING_ADDRESS + NODE_ZIPCODE;
                break;
            case Constants.TYPE:
                nodeName = Constants.Billing + " " + Constants.TYPE;
                nodePath = SALES_ORDER_PAYMENT + BILLING_ADDRESS + Constants.TYPE;
                break;
            case NODE_COUNTRY_CODE:
                nodeName = Constants.Billing + " " + ConstantsDtc.COUNTRY;
                nodePath = SALES_ORDER_PAYMENT + BILLING_ADDRESS + NODE_COUNTRY_CODE;
                break;
            case Constants.CITY:
                nodeName = Constants.Billing + " " + Constants.CITY;
                nodePath = SALES_ORDER_PAYMENT + BILLING_ADDRESS + Constants.CITY.toLowerCase();
                break;
            case ConstantsDtc.STATE:
                nodeName = Constants.Billing + " " + ConstantsDtc.STATE;
                nodePath = SALES_ORDER_PAYMENT + BILLING_ADDRESS + ConstantsDtc.STATE.toLowerCase();
                break;
            case PAYMENT_TXN_TYPE:
                nodePath = SALES_ORDER_PAYMENT + PAYMENT_TXN_TYPE;
                break;
            case REFERENCE_NUMBER:
                nodeName = REFERENCE_NUMBER;
                nodePath = SALES_ORDER_PAYMENT + REFERENCE_NUMBER;
                break;
            default:
                Assert.fail("FAIL: Could not find node name that matched string passed from step");
        }
        String actualValue = getXmlNodeValue(rootElement, nodePath);
        expectedValue = driver.scenarioData.genericData.get(nodeName);
        if (node.equalsIgnoreCase(PAYMENT_TXN_TYPE)) {
            expectedValue = Constants.SETTLE;
        } else if (node.equalsIgnoreCase(AMOUNT)) {
            expectedValue = driver.scenarioData.genericData.get(Constants.ORDER_TOTAL);
        } else if (node.equalsIgnoreCase(ConstantsDtc.STATE)) {
            expectedValue = CommonUtils.replaceStateCountryNameWithCode(expectedValue);
        }
        String nodeValues = actualValue + "&" + expectedValue + "#" + nodePath;
        LOGGER.info("returnCustomerNodeValues completed for " + node);
        return nodeValues;
    }

}
