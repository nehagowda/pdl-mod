package orderxmls.steps;

import common.Config;
import common.Constants;
import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.en.And;
import dtc.data.ConstantsDtc;
import dtc.pages.CommonActions;
import orderxmls.pages.OrderXmls;
import cucumber.api.java.en.When;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.w3c.dom.NodeList;
import utilities.CommonUtils;
import utilities.Driver;
import webservices.pages.WebServices;

import java.text.DecimalFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by csahoo on 7/15/18.
 */

public class OrderXmlSteps {
    private OrderXmls orderXmls;
    private Driver driver;
    private Scenario scenario;
    private WebServices webServices;
    private CommonActions commonActions;

    public OrderXmlSteps(Driver driver) {
        this.driver = driver;
        orderXmls = new OrderXmls(driver);
        webServices = new WebServices(driver);
        commonActions = new CommonActions(driver);
    }

    @cucumber.api.java.Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @When("^I read the order xml file$")
    public void i_read_the_order_xml_file() throws Throwable {
        String orderID = driver.scenarioData.getCurrentOrderNumber();
        String file = webServices.getOrderXml(orderID);
        orderXmls.getRootElement(file);
    }

    @And("^I assert order xml node values$")
    public void i_verify_order_xml_node_values(DataTable table) throws Throwable {
        int column = 0;
        List<List<String>> rows = table.raw();
        for (int row = 0; row < rows.size(); row++) {
            String text = rows.get(row).get(column);
            String expectedValue = orderXmls.returnExpectedValueForOrder(text);
            String actualValue = "";
            if (text.equalsIgnoreCase(Constants.VEHICLE)) {
                actualValue = orderXmls.getXmlNodeValue(OrderXmls.rootElement, OrderXmls.YEAR);
                actualValue += " " + orderXmls.getXmlNodeValue(OrderXmls.rootElement, OrderXmls.MAKE);
                actualValue += " " + orderXmls.getXmlNodeValue(OrderXmls.rootElement, OrderXmls.MODEL);
                actualValue += " " + orderXmls.getXmlNodeValue(OrderXmls.rootElement, OrderXmls.TRIM);
            } else {
                actualValue = orderXmls.getXmlNodeValue(OrderXmls.rootElement, text);
            }
            scenario.write("OrderXml Node value for " + text + " is: '" + actualValue + "' expected value: '"
                    + expectedValue + "'.");
            orderXmls.assertOrderXmlValue(expectedValue, actualValue, text);
        }
    }

    @And("^I assert order xml list values$")
    public void i_verify_order_xml_node_list_values(DataTable table) throws Throwable {
        int column = 0;
        String actualValue = "";
        List<List<String>> rows = table.raw();
        for (int row = 0; row < rows.size(); row++) {
            String text = rows.get(row).get(column);
            if (text.equalsIgnoreCase(OrderXmls.PROMOTION_DISCOUNT)) {
                NodeList actualListValue = orderXmls.getXmlNodeList(OrderXmls.rootElement, text);
                double actualValueDbl = 0;
                double expectedValueDbl = 0;
                String expectedValueList = orderXmls.returnExpectedValueForOrder(text).
                        replace("[", "").replace("]", "").trim();
                String[] expectedValues = expectedValueList.split(",");
                for (String expectedVal : expectedValues) {
                    if (expectedVal.trim().isEmpty())
                        expectedVal = "0.0";
                    expectedValueDbl += Double.parseDouble(expectedVal);
                }
                String expectedValue = String.valueOf(String.valueOf(expectedValueDbl));
                for (int i = 0; i < actualListValue.getLength(); i++) {
                    actualValue = actualListValue.item(i).getTextContent();
                    actualValueDbl += Double.parseDouble(actualValue);
                }
                actualValue = String.valueOf(commonActions.cleanMonetaryStringToDouble(String.valueOf(actualValueDbl)));
                scenario.write("OrderXml Node List value for " + text + " is: '" + actualValue
                        + "' expected value: '" + expectedValue + "'.");
                orderXmls.assertOrderXmlValues(expectedValue, actualValue, text);
            } else if (text.equalsIgnoreCase(OrderXmls.PRODUCTS)) {
                NodeList actualListValue = orderXmls.getXmlNodeList(OrderXmls.rootElement, text);
                ArrayList<String> expectedValues = orderXmls.returnExpectedProductCodes();
                for (int i = 0; i < actualListValue.getLength(); i++) {
                    actualValue = actualListValue.item(i).getTextContent();
                    scenario.write("OrderXml Node List value for " + text + " is: '" + actualValue
                            + "' expected values: '" + expectedValues.toString().
                            replace("[", "").replace("]", "") + "'.");
                    if (!expectedValues.contains(actualValue)) {
                        Assert.fail("OrderXml Node List value for " + text + " is: '" + actualValue
                                + "' expected values: '" + expectedValues);
                    }
                }
            } else if (text.equalsIgnoreCase(OrderXmls.TAX)) {
                String expectedValue = orderXmls.returnExpectedValueForOrder(text);
                ArrayList<Double> taxes = new ArrayList<>();
                taxes.add(getCityStateOtherTaxSum(OrderXmls.ITEM_TAX));
                taxes.add(getCityStateOtherTaxSum(OrderXmls.SUBITEM_TAX));
                taxes.add(getCityStateOtherTaxSum(OrderXmls.PROMOTION_TAX));
                actualValue = String.valueOf(new DecimalFormat(".##").format(taxes.stream().mapToDouble(a -> a).sum()));
                scenario.write("OrderXml Node value for total sales tax (item tax + subitem tax - promotion tax): '"
                        + actualValue + "'. Expected value: '" + expectedValue + "'");
                orderXmls.assertOrderXmlValue(actualValue, expectedValue, text);
            }
        }
    }

    /**
     * Get the total of taxes for a tax entity: Item, Subitem, or Promotion
     *
     * @param taxableEntity - Item, Subitem, or Promotion
     * @return sum of taxes within the taxable entity
     */
    private double getCityStateOtherTaxSum(String taxableEntity) throws Throwable {
        double taxSum = 0;
        double taxValue = 0;
        String taxType = "";
        NodeList taxNodes = orderXmls.getXmlNodeList(OrderXmls.rootElement, taxableEntity);
        NodeList taxTypeNodes = orderXmls.getXmlNodeList(OrderXmls.rootElement, taxableEntity + " " + Constants.TYPE);
        for (int i = 0; i < taxNodes.getLength(); i++) {
            taxType = taxTypeNodes.item(i).getTextContent();
            // FET is not included in tax calculation.
            if (taxType.equalsIgnoreCase(Constants.FET))
                continue;
            taxValue = Double.parseDouble(taxNodes.item(i).getTextContent());
            scenario.write("OrderXml Node value for '" + taxType + " " +
                    taxableEntity.toLowerCase() + "' is " + taxValue);
            taxSum += taxValue;
        }
        return taxSum;
    }

    @And("^I assert order xml node value is not null$")
    public void i_verify_order_xml_node_value_is_not_null(DataTable table) throws Throwable {
        int column = 0;
        List<List<String>> rows = table.raw();
        for (int row = 0; row < rows.size(); row++) {
            String text = rows.get(row).get(column);
            String actualValue = orderXmls.getXmlNodeValue(OrderXmls.rootElement, text);
            scenario.write("OrderXml Node value for " + text + " is: '" + actualValue + "'.");
            orderXmls.assertOrderXmlValueNotNull(actualValue, text);
            if (text.equalsIgnoreCase(OrderXmls.APPOINTMENT_END_TIME)) {
                String apptEndTime = commonActions.convertToMilitaryTime(actualValue);
                String apptStartTime = commonActions.convertToMilitaryTime(orderXmls.getXmlNodeValue(OrderXmls.rootElement,
                        OrderXmls.APPOINTMENT_START_TIME));
                Assert.assertTrue("FAIL: Appointment end time: " + apptEndTime + " is greater than appointment " +
                                "start time: " + apptStartTime,
                        LocalTime.parse(apptEndTime).isAfter(LocalTime.parse(apptStartTime)));
            }
        }
    }

    @And("^I assert order xml customer node values$")
    public void i_verify_order_xml_customer_node_value() throws Throwable {
        for (String node : OrderXmls.customerDataList) {
            String nodeValues = orderXmls.returnCustomerNodeValues(node);
            String nodePath = nodeValues.split("#")[1].trim();
            String actualValue = nodeValues.split("&")[0].trim();
            String expectedValue = nodeValues.split("&")[1].split("#")[0].trim();
            if (node.equalsIgnoreCase(ConstantsDtc.STATE) || node.equalsIgnoreCase(OrderXmls.NODE_COUNTRY_CODE)) {
                expectedValue = CommonUtils.replaceStateCountryNameWithCode(expectedValue);
            }
            scenario.write("OrderXml Node value for " + node + " is: '" + actualValue + "' expected value: '"
                    + expectedValue + "'.");
            orderXmls.assertOrderXmlValue(expectedValue, actualValue, nodePath);
        }
    }

    @And("^I assert order xml subItems$")
    public void i_assert_order_xml_subitem_node_values() throws Throwable {
        NodeList actualListValue = orderXmls.getXmlNodeList(OrderXmls.rootElement, OrderXmls.SUBITEM + " "
                + OrderXmls.PRODUCTS);
        ArrayList<String> subItems = new ArrayList<>();
        for (int i = 0; i < actualListValue.getLength(); i++) {
            subItems.add(actualListValue.item(i).getTextContent());
        }
        CommonActions.stringAndCount.clear();
        commonActions.getStringCountInArray(subItems);
        for (int i = 0; i < actualListValue.getLength(); i++) {
            NodeList subItemChildNodes = actualListValue.item(i).getParentNode().getChildNodes();
            String actualValue = orderXmls.returnSubItemNodeValues(subItemChildNodes);
            String subItemProdCode = subItemChildNodes.item(0).getTextContent();
            String subItemProdDescription = subItemChildNodes.item(4).getTextContent();
            String expectedValue = driver.scenarioData.genericData.get(subItemProdCode);
            if (subItemProdDescription.toLowerCase().contains(ConstantsDtc.LUG.toLowerCase())) {
                expectedValue = driver.scenarioData.genericData.get(ConstantsDtc.WHEEL_INSTALL_KIT)
                        .split(",", 2)[1].trim();
                actualValue = actualValue.split(",", 2)[1].trim();
            } else if (subItemProdDescription.toLowerCase().contains(ConstantsDtc.HUB.toLowerCase())) {
                expectedValue = driver.scenarioData.genericData.get(ConstantsDtc.HUB_CENTRIC_RING)
                        .split(",", 2)[1].trim();
                actualValue = actualValue.split(",", 2)[1].trim();
            }
            scenario.write("OrderXml subItem count for " + subItemProdDescription + " is: '"
                    + CommonActions.stringAndCount.get(subItemProdCode) + "'");
            if (CommonActions.stringAndCount.get(subItemProdCode) > 1) {
                if (subItemProdDescription.contains(ConstantsDtc.CERTIFICATES)) {
                    String certificateKey = actualValue.split(OrderXmls.RETAIL_PRICE + ":")[1].trim().split("\\.")[0];
                    expectedValue = driver.scenarioData.genericData.get(certificateKey);
                } else if (Config.getSiteRegion().equals(Constants.DT) &&
                        !subItemProdDescription.contains(ConstantsDtc.TPMS) &&
                        !subItemProdCode.equals(Constants.INSTALLATION_PRODUCT_CODE)) {
                    double salesExtended = (Double.valueOf(expectedValue.split(",", 4)[3].split(":")[1])) / 2;
                    expectedValue = expectedValue.split(",", 4)[0] + ","
                            + expectedValue.split(",", 4)[1].split("\\d")[0] + "2" + ","
                            + expectedValue.split(",", 4)[2] + ","
                            + expectedValue.split(",", 4)[3].split("\\d")[0] + salesExtended;
                }
            }
            if (Config.getSiteRegion().equals(Constants.DTD) &&
                    !subItemProdDescription.contains(ConstantsDtc.ENVIRONMENTAL_FEE)) {
                orderXmls.assertOrderXmlValue(expectedValue, actualValue, subItemProdDescription);
            } else if (Config.getSiteRegion().equals(Constants.DT) &&
                    !subItemProdDescription.contains(ConstantsDtc.TPMS)) {
                orderXmls.assertOrderXmlValue(expectedValue, actualValue, subItemProdDescription);
            }
            scenario.write("OrderXml subItem value for " + subItemProdDescription + " is: '" + actualValue
                    + "' expected value: '" + expectedValue + "'.");
        }
    }

    @And("^I assert order xml items for \"(.*?)\"$")
    public void i_assert_order_xml_service_item_node_values(String text) throws Throwable {
        NodeList actualListValue = orderXmls.getXmlNodeList(OrderXmls.rootElement, OrderXmls.PRODUCTS);
        for (int i = 0; i < actualListValue.getLength(); i++) {
            String expectedValue;
            NodeList itemChildNodes = actualListValue.item(i).getParentNode().getChildNodes();
            String actualValue = orderXmls.returnItemNodeValues(itemChildNodes);
            String itemProdCode = itemChildNodes.item(2).getTextContent();
            String itemProdDescription = itemChildNodes.item(6).getTextContent();
            if (OrderXmls.serviceOrdersList.indexOf(itemProdCode) >= 0) {
                actualValue = actualValue.split(",", 2)[1].trim();
                expectedValue = driver.scenarioData.genericData.get(text);
            } else {
                expectedValue = driver.scenarioData.genericData.get(itemProdCode);
            }
            orderXmls.assertOrderXmlValue(expectedValue, actualValue, itemProdDescription);
            scenario.write("OrderXml subItem value for " + itemProdDescription + " is: '" + actualValue
                    + "' expected value: '" + expectedValue + "'.");
        }
    }

    @And("^I assert order xml payment node values$")
    public void i_verify_order_xml_payment_node_value() throws Throwable {
        ArrayList<String> paymentNodeList = null;
        if (Config.getSiteRegion().equalsIgnoreCase(ConstantsDtc.DT)) {
            paymentNodeList = OrderXmls.dtPaymentNodeList;
        } else {
            paymentNodeList = OrderXmls.dtdPaymentNodeList;
        }
        for (String node : paymentNodeList) {
            String nodeValues = orderXmls.returnPaymentNodeValues(node).toString();
            String nodePath = nodeValues.split("#")[1].trim();
            String actualValue = StringUtils.stripStart(nodeValues.split("&")[0].trim(), "0");
            if (node.equalsIgnoreCase(OrderXmls.CARD_TOKEN)) {
                actualValue = actualValue.substring(0, 6);
            }
            String expectedValue = nodeValues.split("&")[1].split("#")[0].trim();
            orderXmls.assertOrderXmlValue(expectedValue, actualValue, nodePath);
            scenario.write("OrderXml Node value for " + node + " is: '" + actualValue + "' expected value: '"
                    + expectedValue + "'.");
        }
    }

    @And("^I get order xml node value$")
    public void i_get_order_xml_node_value(DataTable table) throws Throwable {
        int column = 0;
        List<List<String>> rows = table.raw();
        for (int row = 0; row < rows.size(); row++) {
            String text = rows.get(row).get(column);
            String actualValue = orderXmls.getXmlNodeValue(OrderXmls.rootElement, text);
            scenario.write("OrderXml Node value for " + text + " is: '" + actualValue + "'.");
            if (text.equalsIgnoreCase(OrderXmls.CANCELLATION_URL)) {
                driver.scenarioData.genericData.put(OrderXmls.ORDER_CANCEL_URL, actualValue);
            }
        }
    }

    @And("^I assert paypal order xml payment node values$")
    public void i_verify_paypal_order_xml_payment_node_value() throws Throwable {
        ArrayList<String> paymentNodeList = OrderXmls.paypalPaymentNodeList;
        for (String node : paymentNodeList) {
            String nodeValues = orderXmls.returnPaymentNodeValues(node).toString();
            String nodePath = nodeValues.split("#")[1].trim();
            String actualValue = StringUtils.stripStart(nodeValues.split("&")[0].trim(), "0");
            String expectedValue = nodeValues.split("&")[1].split("#")[0].trim();
            orderXmls.assertOrderXmlValue(expectedValue, actualValue, nodePath);
            scenario.write("OrderXml Node value for " + node + " is: '" + actualValue + "' expected value: '"
                    + expectedValue + "'.");
        }
    }
}