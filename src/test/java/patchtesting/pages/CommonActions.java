package patchtesting.pages;

import common.Config;
import common.Constants;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Keys;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.KeyEvent;


import java.io.IOException;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Set;
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Logger;




/**
 * Created by eseverson on 5/21/18.
 */
public class CommonActions {

    private Driver driver;
    private WebDriver webDriver;
    private final Logger LOGGER = Logger.getLogger(CommonActions.class.getName());


    public CommonActions(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(className = "mwEmbedKalturaIframe")
    public static WebElement commoniFrame1;

    @FindBy(className = "accessibilityLabel")
    public WebElement currentTime;

    @FindBy(className = "playlistAPI")
    public WebElement videoContainer;

    @FindBy(xpath = "/html/body/div/div[4]/div[2]/div[2]")
    public WebElement timeDisplay;

    @FindBy(className = "play-btn-large")
    public static WebElement largePlayBtn;

    @FindBy(id="plugin")
    public static WebElement pdfPlugin;


    public static final By li = By.tagName("li");

    public static final By imgTagBy = By.tagName("img");

    private static final String A = "a";
    private static final String P = "P";
    private static final String H2 = "h2";
    private static final String TEXTCONTENT = "textContent";

    /**
     * Selects a sub-menu option via provided menu and sub-menu options
     *
     * @param menuOption    Menu header to hover over
     * @param subMenuOption Sub-menu option to click
     */
    public void selectSubMenuOption(String menuOption, String subMenuOption) {
        LOGGER.info("selectSubMenuOption started");
        driver.waitForMilliseconds();
        By topMenuLink = By.linkText(menuOption);
        driver.mouseHoverOverElement(topMenuLink);
        driver.waitForMilliseconds();
        By subMenuLink = By.linkText(subMenuOption);
        webDriver.findElement(subMenuLink).click();
        LOGGER.info("selectSubMenuOption completed");
    }

    /**
     * Switches frame back to the default content
     */
    public void switchToDefaultContentWindow() {
        LOGGER.info("switchToDefaultContentWindow started");
        webDriver.switchTo().defaultContent();
        LOGGER.info("switchToDefaultContentWindow completed");
    }
    /**
     * Selects a link from the passed in link text
     *
     * @linkText Text of the link to click in the section
     */
    public void selectLinkFromPage(String linkText) {
        LOGGER.info("selectLinkFromPage started");
        try {
            webDriver.findElement(By.linkText(linkText)).click();
        } catch (Exception e) {
            driver.getElementWithText(By.tagName(Constants.BUTTON), linkText).click();
        }
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        LOGGER.info("selectLinkFromPage completed");
    }
    /**
     * This Method will click the link by linktext in IE
     * and will automate the IE Download pop up
     * @param linkText linktext of the element
     */
    public void selectDownloadLinkFromPageForIe(String linkText){
        LOGGER.info("selectDownloadLinkFromPageForIe started");
        driver.jsClick(webDriver.findElement(By.linkText(linkText)));
        automateIeDownloadPopUp();
        LOGGER.info("selectDownloadLinkFromPageForIe completed");
    }
    /**
     * This method automates the Download pop up in IE
     */

     public void automateIeDownloadPopUp(){
        LOGGER.info("automateIeDownloadPopUp started");
        try {
            driver.waitForMilliseconds(Constants.THREE_THOUSAND);
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_TAB);
            robot.keyRelease(KeyEvent.VK_TAB);
            Thread.sleep(Constants.ONE_THOUSAND);
            robot.keyPress(KeyEvent.VK_ENTER);
            robot.keyRelease(KeyEvent.VK_ENTER);
            Thread.sleep(Constants.ONE_THOUSAND);
        }catch(AWTException e){
            e.printStackTrace();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
        LOGGER.info("automateIeDownloadPopUp nded");
     }

    /**
     * Verifies page URL. Switches based on window text passed in.
     *
     * @window New window or Same window
     * @url Text of the link to click in the section
     */
    public void verifyPageURL(String url) {
        LOGGER.info("verifyPageURL started");
        driver.waitForPageToLoad();
        String currentUrl = webDriver.getCurrentUrl();
        Assert.assertTrue("FAIL: Current page URL (" + currentUrl + ") did not contain: " + url,
                currentUrl.contains(url));
        LOGGER.info("verifyPageURL completed");
    }

    /**
     * Switches to a newly opened window handle
     */
    public void switchToNewWindowHandle() {
        LOGGER.info("verifyPageURL completed");
        String mainHandle = webDriver.getWindowHandle();
        Set allHandles = webDriver.getWindowHandles();
        Iterator iter = allHandles.iterator();
        while (iter.hasNext()) {
            String popupHandle = iter.next().toString();
            if (!popupHandle.contains(mainHandle)) {
                webDriver.switchTo().window(popupHandle);
            }
        }
        LOGGER.info("verifyPageURL completed");
    }

    /**
     * Verifies video is playing on the page
     */
    public void selectFirstVideoAndVerifyVideoPlaying() {
        LOGGER.info("selectFirstVideoAndVerifyVideoPlaying started");
        driver.waitForPageToLoad();
        driver.waitForMilliseconds();
        webDriver.switchTo().frame(commoniFrame1);
        videoContainer.findElement(li).click();
        driver.waitForMilliseconds(Constants.FOUR_THOUSAND);
        String timeString = currentTime.getAttribute(TEXTCONTENT);
        int timeInt = Integer.parseInt(timeString.substring(2, 4));
        Assert.assertTrue("FAIL: Video time display was not greater than zero.", timeInt > 0);
        webDriver.switchTo().defaultContent();
        LOGGER.info("selectFirstVideoAndVerifyVideoPlaying completed");
    }

    /**
     * Switches to the Video Iframe
     */
    public void switchToVideoFrame(){
        LOGGER.info("switchToVideoFrame started");
        driver.waitForPageToLoad();
        driver.waitForMilliseconds();
        driver.waitForMilliseconds(Constants.FOUR_THOUSAND);
        webDriver.switchTo().frame(commoniFrame1);
        LOGGER.info("switchToVideoFrame completed");
    }

    /**
     * Verifies if the video is playing
     *
     * @param text text of the video WebElement
     */
    public void verifyVideoIsPlaying(String text){
        LOGGER.info("verifyVideoIsPlaying started");
        driver.waitForMilliseconds(Constants.SEVEN_THOUSAND);
        try {
            String timeString = currentTime.getAttribute(TEXTCONTENT);
            int timeInt = Integer.parseInt(timeString.substring(2, 4));
            Assert.assertTrue("FAIL: Video time display was not greater than zero.", timeInt > 0);
        }catch (StaleElementReferenceException e)
        {
            LOGGER.info("StaleELementReferenceException catch block started");
            webDriver.navigate().refresh();
            webDriver.switchTo().defaultContent();
            switchToVideoFrame();
            clickOnExactText(text);
            verifyVideoIsPlaying(text);
            LOGGER.info("StaleELementReferenceException catch block completed");
        }catch (StringIndexOutOfBoundsException e){
            LOGGER.info("StringIndexOutOfBoundsException catch block started");
            webDriver.navigate().refresh();
            webDriver.switchTo().defaultContent();
            switchToVideoFrame();
            clickOnExactText(text);
            verifyVideoIsPlaying(text);
            LOGGER.info("StringIndexOutOfBoundsException catch block completed");
        }
        webDriver.switchTo().defaultContent();
        LOGGER.info("verifyVideoIsPlaying completed");
    }

    /**
     * Verifies video is playing on the screen
     */
    public void verifyPopupVideoIsPlaying() {
        LOGGER.info("verifyPopupVideoIsPlaying started");
        driver.waitForMilliseconds(Constants.FOUR_THOUSAND);
        // Need video (and for it to be playing) for this to pass
        String timeString = timeDisplay.getAttribute(TEXTCONTENT);
        int timeInt = Integer.parseInt(timeString.substring(2, 4));
        Assert.assertTrue("FAIL: Video does not appear to be playing.", timeInt > 0);
        LOGGER.info("verifyPopupVideoIsPlaying completed");
    }

    /**
     * Switches frame context to the specified iFrame
     *
     * @param position Position within the DOM of the iframe
     */
    public void switchToVideoContentFrame(int position) {
        LOGGER.info("switchToVideoContent started");
        driver.waitForPageToLoad();
        webDriver.switchTo().frame(position);
        driver.waitForElementClickable(commoniFrame1);
        webDriver.switchTo().frame(commoniFrame1);
        LOGGER.info("switchToVideoContent completed");
    }

    /**
     * method to wait until the file is downloaded
     *
     * @param fileNameWithExtension name of the downloaded file + extension
     */
    public void waitUntilFileIsDownloaded(String fileNameWithExtension){
        LOGGER.info("waitUntilFileIsDownloaded started");
        int counter=Constants.TEN;
        String filePath = Constants.DOWNLOAD_PATH;
        boolean flag=false;
        File dir = new File(filePath);
        File[] dir_contents = dir.listFiles();
        while(flag == false && counter > 0){
            for (int i = 0; i < dir_contents.length; i++) {
                dir_contents = dir.listFiles();
                if (dir_contents[i].getName().equals(fileNameWithExtension)) {
                    flag = true;
                    break;
                }
            }
            if(flag==true)
                break;
            else {
                driver.waitForMilliseconds(Constants.FOUR_THOUSAND);
                counter--;
            }
        }
        LOGGER.info("waitUntilFileIsDownloaded completed");
    }

    /**
     * Verifies the file downloaded to the correct path with the correct extension
     *
     * @param fileNameWithExtension Name of the downloaded file + extension
     */
    public void isFileDownloaded(String fileNameWithExtension) {
        LOGGER.info("isFileDownloaded started");
        waitUntilFileIsDownloaded(fileNameWithExtension);
        boolean flag = false;
        String filePath = "C:\\Users\\" + System.getProperty("user.name") + "\\Downloads\\";

        File dir = new File(filePath);
        File[] dir_contents = dir.listFiles();
        for (int i = 0; i < dir_contents.length; i++) {
            if (dir_contents[i].getName().equals(fileNameWithExtension)) {
                flag = true;
                break;
            }
        }
        Assert.assertTrue("FAIL: File not downloaded to the Downloads folder.", flag);
        LOGGER.info("isFileDownloaded completed");
    }

    /**
     * Deletes downloaded file passed in via name
     *
     * @param name File + extension name
     */
    public void deleteDownloadedFile(String name) {
        LOGGER.info("deleteDownloadFile started");
        try {
            String filePath = Constants.DOWNLOAD_PATH+ name;
            Files.deleteIfExists(Paths.get(filePath));
        } catch (NoSuchFileException e) {
            LOGGER.info("No such file/directory exists");
        } catch (IOException e) {
            e.printStackTrace();
        }
        LOGGER.info("deleteDownloadFile file completed");
    }

    /**
     * Verifies the passed in text of a header element is displayed on the page
     *
     * @param text Text value to assert on the page
     */
    public void verifyElementDisplayedOnPage(String text) {
        LOGGER.info("verifyElementDisplayedOnPage started");
        driver.waitForPageToLoad();
        List<String> commonTags = new ArrayList<String>(Arrays.asList(Constants.SPAN, P, A, H2));
        for (String tag : commonTags) {
            try {
                WebElement textEle = driver.getElementWithText(By.tagName(tag), text);
                if (textEle != null) {
                    Assert.assertTrue("FAIL: Element '" + text + "' was not found on page.",
                            driver.isElementDisplayed(textEle));
                    break;
                }
            } catch (Exception e) {
                LOGGER.info("Could not find an element on page by tag type '" + tag
                        + "' with text value '" + text + "'");
            }
        }
        LOGGER.info("verifyElementDisplayedOnPage completed");
    }

    /**
     * Navigates to page with url
     *
     * @param url The url to navigate to
     */
    public void navigateToPage(String url) {
        LOGGER.info("navigateToPage started with: " + url);
        try {
            driver.getUrl(url);
            driver.waitForPageToLoad();
        } catch (Exception e) {
            Assert.fail("FAIL: Navigating to page with url: " + url + ". FAILED with error: " + e);
        }
        LOGGER.info("navigateToPage completed with: " + url);
    }

    /**
     * Selects image with the passed in 'alt' attribute
     *
     * @param altText Text contained in the 'alt' attribute
     */
    public void selectImageWithAltText(String altText) {
        LOGGER.info("selectImageWithAltText started");
        WebElement image = driver.getElementWithAttribute(imgTagBy, Constants.ATTRIBUTE_ALT, altText);
        image.click();
        LOGGER.info("selectImageWithAltText completed");
    }
    /**
     * Performs Ctrl+A and Ctrl+C
     */
    public void ctrlActrlC() throws Throwable {
        LOGGER.info("CtrlACtrlC started");
        if(Config.isChrome()) {
           LOGGER.info("CtrlA start");
           Actions actionObj = new Actions(webDriver);
           actionObj.moveToElement(pdfPlugin).doubleClick().build().perform();
            Thread.sleep(Constants.ONE_THOUSAND);
           actionObj.keyDown(Keys.CONTROL)
                   .sendKeys(Keys.chord("a"))
                   .perform();
            LOGGER.info("CtrlA end");
           Thread.sleep(Constants.ONE_THOUSAND);
           LOGGER.info("CtrlC start");
           actionObj.keyDown(Keys.CONTROL)
                   .sendKeys(Keys.chord("c"))
                   .perform();
           Thread.sleep(Constants.TWO_THOUSAND);
           LOGGER.info("CtrlC end");
       }
       else{
            LOGGER.info("CtrlA started");
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            Thread.sleep(Constants.ONE_THOUSAND);
            robot.keyPress(KeyEvent.VK_A);
            Thread.sleep(Constants.ONE_THOUSAND);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            Thread.sleep(Constants.ONE_THOUSAND);
            robot.keyRelease(KeyEvent.VK_A);
            Thread.sleep(Constants.ONE_THOUSAND);
            LOGGER.info("CtrlA completed");
            LOGGER.info("CtrlC started");
            robot.keyPress(KeyEvent.VK_CONTROL);
            Thread.sleep(Constants.ONE_THOUSAND);
            robot.keyPress(KeyEvent.VK_C);
            Thread.sleep(Constants.ONE_THOUSAND);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            Thread.sleep(Constants.ONE_THOUSAND);
            robot.keyRelease(KeyEvent.VK_C);
            Thread.sleep(Constants.ONE_THOUSAND);
            LOGGER.info("CtrlC completed");
        }
        LOGGER.info("CtrlACtrlC completed");
    }

    /**
     * Checks if the System ClipBoard Contains the input String
     *
     * @param reqTextInPdf Input to validate
     */
    public void verifyPdfContent(String reqTextInPdf) {
        LOGGER.info("VerifyPdfContent started");
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        try {
            String data = (String) clipboard.getData(DataFlavor.stringFlavor);
            Assert.assertTrue("FAIL: The data you copied is: "+data.toLowerCase()+" And the text being validated is: "+reqTextInPdf,data.toLowerCase().contains(reqTextInPdf.toLowerCase()));
            StringSelection stringSelection = new StringSelection("");
            Toolkit.getDefaultToolkit().getSystemClipboard().setContents(
                    stringSelection, null);
        } catch (HeadlessException e) {
            e.printStackTrace();
        } catch (UnsupportedFlavorException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        LOGGER.info("VerifyPdfContent completed");
    }

    /**
     * Method to Click on WebElement with Text
     * @param text Text of the WebElement
     */
    public void clickOnExactText(String text){
        LOGGER.info("clickOnExactText started");
        driver.waitForPageToLoad();
        driver.waitForMilliseconds();
        webDriver.findElement(By.xpath("//*[text()='" +text+ "']")).click();
        LOGGER.info("clickOnExactText completed");
    }

    public void openDownloadedFilefromDownloadFolder(String fileName){
        try {
            if (Desktop.isDesktopSupported()) {
                Desktop.getDesktop().open(new File("C:\\Users\\" + System.getProperty("user.name") + "\\Downloads\\" + fileName));
            }
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

   public void closeOpenedFile(String fileName){
        String documentType=null;
        if(fileName.contains("xlsx")||fileName.contains("XLSX")){
            documentType="excel.exe";
        }else  if(fileName.contains("docx")||fileName.contains("DOCX")||fileName.contains("doc")||fileName.contains("DOC")){
            documentType="winword.exe";
        }
       try {
           Runtime rt = Runtime.getRuntime();

           rt.exec("taskkill /F /IM "+documentType);
       }catch(IOException e){
           LOGGER.info("Not able to close"+fileName);
       }
   }

   public void validateTextInOpenedFile(String expectedText) throws InterruptedException{
        String documentText=null;
        try {
            Thread.sleep(4000);
            Robot robot = new Robot();
            robot.keyPress(KeyEvent.VK_CONTROL);
            Thread.sleep(500);
            robot.keyPress(KeyEvent.VK_A);
            Thread.sleep(500);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            Thread.sleep(500);
            robot.keyRelease(KeyEvent.VK_A);
            Thread.sleep(500);
            LOGGER.info("CtrlA ended");
            LOGGER.info("CtrlC started");
            robot.keyPress(KeyEvent.VK_CONTROL);
            Thread.sleep(500);
            robot.keyPress(KeyEvent.VK_C);
            Thread.sleep(500);
            robot.keyRelease(KeyEvent.VK_CONTROL);
            Thread.sleep(500);
            robot.keyRelease(KeyEvent.VK_C);
            Thread.sleep(500);
            LOGGER.info("CtrlC ended");

            Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
            try {
                String data = (String) clipboard.getData(DataFlavor.stringFlavor);
                Assert.assertTrue(data.contains(expectedText));
                StringSelection stringSelection = new StringSelection("");
                Toolkit.getDefaultToolkit().getSystemClipboard().setContents(
                        stringSelection, null);
            } catch (HeadlessException e) {
                e.printStackTrace();
            }catch (UnsupportedFlavorException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }catch(AWTException e){
            LOGGER.info("Not able to copy Text using Robot");
        }


   }
}