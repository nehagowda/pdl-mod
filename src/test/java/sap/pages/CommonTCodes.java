package sap.pages;

import common.Constants;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;
import webservices.pages.WebServices;

import java.util.List;
import java.util.logging.Logger;

public class CommonTCodes {

    private Driver driver;
    private WebDriver webDriver;
    private WebServices webServices;
    private CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(CommonTCodes.class.getName());

    public CommonTCodes(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        webServices = new WebServices(driver);
        commonActions = new CommonActions(driver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//*[text()='Transaction Code']/following::input[1]")
    private static WebElement transactionCodeSe93;

    @FindBy(xpath = "//*[text()='Site']/following::input[1]")
    public static WebElement plantOrSiteSe93;

    @FindBy(xpath = "//*[text()='PDC Type']/following::input[1]")
    private static WebElement pdcTypeSe93;

    @FindBy(xpath = "//*[text()='Select']/following::input[1]")
    private static WebElement selectInputBoxSe93;

    @FindBy(xpath = "//*[text()='Ref DocNo']/following::input[1]")
    private static WebElement referenceDocumentNumberSe93;

    @FindBy(xpath = "//*[text()='Article.Doc No.']/following::input[1]")
    private static WebElement articleDocNumberInScanner;

    @FindBy(xpath = "//*[text()='Materi']/following::input[2]")
    public static WebElement articleDocumentNumberDisplay;

    @FindBy(xpath = "//*[text()='System']/following::div[5]/child::div")
    private static WebElement messageInSe93;

    @FindBy(xpath = "//*[text()='Vendor Number']/following::input[1]")
    private static WebElement vendorNumberInputInScanner;

    @FindBy(xpath = "//*[contains(@id,'[1,16]_c') and not (contains(@id,'[1,16]_c-r'))]")
    private static WebElement site;

    @FindBy(xpath = "//*[text()='Site']/following::input[1]")
    private static WebElement siteInput;

    @FindBy(xpath = "//input[contains(@title,'Movement Type')]")
    private static WebElement movementTypeFieldInMb03;

    @FindBy(id = "plugin")
    private static WebElement pdfuserArea;

    @FindBy(xpath = "//*[contains(text(),'SMART')][1]/preceding::img[6]")
    private static WebElement firstSmartFormSp01;

    @FindBy(id = "URLSPW-0")
    private static WebElement iframeId;

    @FindBy(xpath = "//*[@title='Print ']")
    private static WebElement printButtonForPo;

    @FindBy(xpath = "//div[contains(@id,'_vscroll-Nxt')]")
    private static WebElement scrollBarNextButton;

    private static By tableFieldNamesInSe16By = By.xpath("//tbody[contains(@id,'content')]//following::div[contains(@id,'mrss-cont-left')]//following::tr");

    private static By tableInputFieldsInSe16By = By.xpath("//tbody[contains(@id,'content')]//following::div[contains(@id,'mrss-cont-none')]//following::tr");

    private static By tableContentNamesInSe16nBy = By.xpath("//table[contains(@id,'mrss-cont-left-content')]//child::tr");

    private static By fifthRowInDataTableBy = By.xpath("//*[contains(@id,'1,5#if-r')]");

    private static By fifthRowInDataTableInputBy = By.xpath("//input[contains(@id,'1,5#if')]");

    private static By selectAllCheckBoxesBy = By.xpath("//*[contains(@lsdata,'CHECK')]");

    private static final String TRANSACTION_CODE = "Transaction Code";
    private static final String PDC_TYPE = "PDC Type";
    private static final String SCANNER_SELECT = "Scanner Select";
    private static final String REF_DOC_NO = "RefDocNo input";
    private static final String MESSAGE_IN_SE93 = "Expected Message";
    private static final String VENDOR_NUMBER_INPUT_IN_SCANNER = "vendor input";
    private static final String ROW_XPATH_UNDER_SE16N_TABLE = "//tbody[contains(@id,'content')]//following::div[contains(@id,'mrss-cont-none')]//following::tr";
    private static final String FROM = "from";
    private static final String SCANNER_SITE = "Scanner Site";
    private static final String SITE_INPUT = "Site Input";
    private static final String TO = "to";
    private static final String MATERIAL_DOCUMENT_NUMBER_KEY = "Art. Doc.Item";
    private static final String MOVEMENT_TYPE = "Movement Type in MB03";
    private static final String FIRST_SMART_FORM = "First Smart Form";
    private static final String PDF_SMART_FORM = "PDF Smart Form";
    private static final String PRINT_BUTTON_PO = "Print Button";
    private static final String APPOINTMENT_START_TIME = "AppointmentStartTime";


    /**
     * return the webelement.
     *
     * @param elementName - Name od the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started");
        switch (elementName) {
            case TRANSACTION_CODE:
                return transactionCodeSe93;
            case PDC_TYPE:
                return pdcTypeSe93;
            case SCANNER_SELECT:
                return selectInputBoxSe93;
            case REF_DOC_NO:
                return referenceDocumentNumberSe93;
            case MESSAGE_IN_SE93:
                return messageInSe93;
            case VENDOR_NUMBER_INPUT_IN_SCANNER:
                return vendorNumberInputInScanner;
            case Constants.SITE:
                return site;
            case SCANNER_SITE:
                return plantOrSiteSe93;
            case SITE_INPUT:
                return siteInput;
            case MOVEMENT_TYPE:
                return movementTypeFieldInMb03;
            case FIRST_SMART_FORM:
                return firstSmartFormSp01;
            case PDF_SMART_FORM:
                return pdfuserArea;
            case PRINT_BUTTON_PO:
                return printButtonForPo;
            default:
                Assert.fail("FAIL: Could not find element that matched string passed from step");
                return null;
        }
    }

    /**
     * gets the article document number and inputs into the Article document
     * number field in (RTV -save and scan)Scanner
     */
    public void inputArticleDocumentNumberInScanner() {
        LOGGER.info("inputArticleDocumentNumberInScanner started");
        String scannerArticleDocumentNumber = driver.scenarioData.getData(NetWeaverBusinessClient.ARTICLE_DOCUMENT_NUMBER_DETAILS_KEY) + driver.scenarioData.getData(MATERIAL_DOCUMENT_NUMBER_KEY);
        if (!scannerArticleDocumentNumber.isEmpty()) {
            commonActions.sendKeys(articleDocNumberInScanner, scannerArticleDocumentNumber);
        } else {
            Assert.fail("Article Document Number is Empty");
        }
        LOGGER.info("inputArticleDocumentNumberInScanner completed");
    }

    /**
     * gets the vendor number from mim and inputs into the vendor
     * number field in (RTV - generate)Scanner
     */
    public void inputVendorNumberInScanner() {
        LOGGER.info("inputVendorNumberInScanner started");
        driver.waitForElementVisible(vendorNumberInputInScanner);
        String vendorNumber = driver.scenarioData.getData(CommonActions.VENDOR);
        if (!vendorNumber.isEmpty()) {
            driver.clearInputAndSendKeys(vendorNumberInputInScanner, vendorNumber);
        } else {
            Assert.fail("Vendor Number is Empty");
        }
        LOGGER.info("inputVendorNumberInScanner completed");
    }

    /**
     * returns the index of the element with field name
     *
     * @param fieldName - row name in SE16n
     * @return - Integer value of the index
     */
    private int returnRowNumberForFieldInSe16n(String fieldName) {
        LOGGER.info("returnRowNumberForFieldInSe16n started");
        int fieldNumber = 0;
        List<WebElement> fieldNames = webDriver.findElements(tableFieldNamesInSe16By);
        for (int i = 0; i < fieldNames.size(); i++) {
            if (fieldNames.get(i).getText().toLowerCase().contains(fieldName.toLowerCase())) {
                fieldNumber = i;
                break;
            }
        }
        LOGGER.info("returnRowNumberForFieldInSe16n completed");
        return fieldNumber + 1;
    }

    /**
     * Returns the input element based on the row name specified
     *
     * @param fieldName - row name in SE16n
     * @param fromOrTo  - Specify if the input element should be from field or To field
     * @return input field Web Element
     */
    public WebElement returnInputElementBasedOnFieldName(String fieldName, String fromOrTo) {
        LOGGER.info("returnInputElementBasedOnFieldName started");
        driver.waitForMilliseconds();
        int indexNumber = returnRowNumberForFieldInSe16n(fieldName);
        int rowNumber = 0;
        if (fromOrTo.toLowerCase().equals(FROM)) {
            rowNumber = 1;
        } else if (fromOrTo.toLowerCase().equals(TO)) {
            rowNumber = 2;
        } else {
            Assert.fail("please specify if input column is a 'from' or 'to' field");
        }

        WebElement field = webDriver.findElements(By.xpath(ROW_XPATH_UNDER_SE16N_TABLE + "[" + indexNumber + "]")).
                get(0).findElements(By.tagName("td")).get(rowNumber);
        LOGGER.info("returnInputElementBasedOnFieldName completed");
        return field;
    }

    /**
     * Method to enter the saved data into the specific table input field
     *
     * @param fieldName - row name in se16n
     * @param fromOrTo  - Specify if the input element should be from field or To field
     * @param key       - Hash Map key
     */
    public void enterSavedDataIntoTableInSe16n(String fieldName, String fromOrTo, String key) {
        LOGGER.info("enterSavedDataIntoTableInSe16n started");
        commonActions.sendKeys(returnInputElementBasedOnFieldName(fieldName, fromOrTo), driver.scenarioData.
                getData(key));
        LOGGER.info("enterSavedDataIntoTableInSe16n completed");
    }

    /**
     * Enters purchase order number in a particular element.
     *
     * @param labelName - label name of the input box
     */
    public void enterPurchaseOrderNumber(String labelName) {
        LOGGER.info("enterPurchaseOrderNumber started");
        driver.waitForPageToLoad();
        commonActions.sendKeysWithLabelName(labelName, driver.scenarioData.getCurrentPurchaseOrderNumber());
        LOGGER.info("enterPurchaseOrderNumber completed");
    }

    /**
     * Saves Reversed Article Doc Number
     */
    public void saveRevArtDocument() {
        LOGGER.info("saveRevArtDocument started");
        try {
            commonActions.savePurchaseOrderNumber(webDriver.findElement(fifthRowInDataTableBy));
        } catch (AssertionError e) {
            String purchaseOrderNumber = webDriver.findElement(fifthRowInDataTableInputBy).getAttribute(Constants.VALUE)
                    .replaceAll("[^0-9]", "");
            driver.scenarioData.setCurrentPurchaseOrderNumber(purchaseOrderNumber);
            if (purchaseOrderNumber.isEmpty()) {
                Assert.fail("Purchase Order Number is Empty");
            }
        }
        LOGGER.info("saveRevArtDocument completed");
    }

    /**
     * Asserts value attribute of a webelement
     *
     * @param webElement - Web Element name
     * @param value      - Value attribute of a web element
     */
    public void assertValueAttribute(String webElement, String value) {
        LOGGER.info("assertValueAttribute started");
        driver.assertElementAttributeString(returnElement(webElement), Constants.VALUE, value);
        LOGGER.info("assertValueAttribute completed");
    }

    /**
     * Clicks element in common Tcodes
     *
     * @param elementName - Element name to return a web element
     */
    public void clickOnElementInCommonTcodes(String elementName) {
        LOGGER.info("clickOnElementInCommonTcodes started");
        commonActions.click(returnElement(elementName));
        LOGGER.info("clickOnElementInCommonTcodes completed");
    }

    /**
     * Method selects all check boxes in SP01
     */
    public void selectAllCheckBoxesInSp01() {
        LOGGER.info("selectAllCheckBoxesInSp01 started");
        List<WebElement> elements = webDriver.findElements(selectAllCheckBoxesBy);
        for (int i = 0; i < elements.size(); i++) {
            commonActions.click(elements.get(i));
        }
        LOGGER.info("selectAllCheckBoxesInSp01 completed");
    }

    /**
     * Deals with Delete PDF Pop UP
     */
    public void acknowledgeDeleteForPdfs() {
        LOGGER.info("acknowledgeDeleteForPdfs started");
        commonActions.switchFrameContext(0);
        if (driver.isElementDisplayed(commonActions.returnElementWithPartialText("Delete all"))) {
            commonActions.clickOnPartialText("Delete all");
        } else {
            commonActions.clickOnPartialText("Yes");
        }
        commonActions.switchToMainWindow();
        LOGGER.info("acknowledgeDeleteForPdfs completed");
    }

    /**
     * Selects line item with PO number
     */
    public void selectLineItemWithPoNumber() {
        LOGGER.info("selectLineItemWithPoNumber started");
        try {
            driver.waitForPageToLoad();
            WebElement element = webDriver.findElement(By.xpath("//*[text()='" + driver.scenarioData.
                    getCurrentPurchaseOrderNumber() + "']/preceding::div[1]"));
            driver.waitForElementVisible(element);
            element.click();
        } catch (StaleElementReferenceException mse) {
            WebElement staleElement = webDriver.findElement(By.xpath("//*[@value='" + driver.scenarioData.
                    getCurrentPurchaseOrderNumber() + "']/preceding::div[1]"));
            driver.waitForElementVisible(staleElement);
            staleElement.click();
        }
        LOGGER.info("selectLineItemWithPoNumber completed");
    }

    /**
     * Gets the number value of the element and changes the price between two numbers every time it runs
     *
     * @param elementName - element to get the value from
     * @param fieldPosition - Position of the field.
     */
    public void changePriceInput(String elementName, int fieldPosition) {
        LOGGER.info("changePriceInput started");
        String price = commonActions.returnInputFieldWithLabel(elementName,fieldPosition).getAttribute(Constants.VALUE);
        if (price.equals("6,000.00")) {
            commonActions.sendKeysWithLabelName(elementName ,"6002", fieldPosition);
        } else {
            commonActions.sendKeysWithLabelName(elementName, "6000", fieldPosition);
        }
        LOGGER.info("changePriceInput completed");
    }

    /**
     *  Returns Web Element from inside the table
     *
     * @param rowNumber - Row number in the table
     * @param columnNumber - Column Number in the table
     * @param position - position of the element
     * @return - Web Element
     */
    public WebElement returnElementFromATable(String rowNumber, String columnNumber, int position) {
        LOGGER.info("returnElementFromATable started");
        return webDriver.findElements(By.xpath("//*[contains(@id,'[" + rowNumber + "," + columnNumber + "]_c') ]" +
                "[contains(@id,'[" + rowNumber + "," + columnNumber + "]_c-r')]")).get(position - 1);
    }

    /**
     * To send keys into the table
     *
     * @param text - Input text to enter
     * @param rowNumber - Row number in the table
     * @param columnNumber - Column number in the table
     * @param position - position of the element
     */
    public void sendKeysInATable(String text, String rowNumber, String columnNumber, int position) {
        LOGGER.info("sendKeysInATable started");
        commonActions.click(returnElementFromATable(rowNumber, columnNumber, position));
        Actions actions = new Actions(webDriver);
        for (int i = 0; i < 50; i++) {
            actions.sendKeys(Keys.BACK_SPACE).build().perform();
        }
        commonActions.sendKeys(returnElementFromATable(rowNumber, columnNumber, position), text);
        LOGGER.info("sendKeysInATable completed");
    }

    /**
     * This method will scroll till it locates the element and then double click. Furthermore it will handle the stale
     * element reference exception and click the element again.
     * We have a double click method in driver class which also can be used but exception is not handled.
     *
     * @param element - WebElement which is not in visible screen needs to be double clicked.
     */
    public void scrollAndDoubleClickInSap(WebElement element) {
        LOGGER.info("scrollAndDoubleClickInSap started");
        driver.waitForPageToLoad();
        while (!driver.isElementDisplayed(element))
            commonActions.click(scrollBarNextButton);
        try {
            driver.doubleClick(element);
        } catch (StaleElementReferenceException s) {
            WebElement staleElement = webDriver.findElement(commonActions.extractByLocatorFromWebElement(element));
            driver.doubleClick(staleElement);
        }
        LOGGER.info("scrollAndDoubleClickInSap completed");
    }

    /**
     * verifies the green status type of payment proposal
     */
    public void verifyPaymentProposalSucessStatus() {
        LOGGER.info("verifyPaymentProposalSucessStatus started");
        Assert.assertTrue("Payment Proposal Failed!", driver.isElementDisplayed(commonActions.paymentProposalSuccess));
        LOGGER.info("verifyPaymentProposalSucessStatus completed");
    }

    /**
     * Converts Web Appointment to Car format
     *
     * @return
     */
    private String webAppointmentConversionToCarFormat() {
        LOGGER.info("webAppointmentConversionToCarFormat started");
        String todaysDate = driver.scenarioData.getData("appointmentDate").replace("-", "");
        String timeStamp = driver.scenarioData.getData("appointmentStartTime").replace(":", "");
        driver.scenarioData.setData("convertedWebAppointmentDate", todaysDate+timeStamp+"00");
        LOGGER.info("Appointment format: " + driver.scenarioData.getData("convertedWebAppointmentDate"));
        LOGGER.info("webAppointmentConversionToCarFormat completed");
        return driver.scenarioData.getData("convertedWebAppointmentDate");
    }

    /**
     * Validates appointment in Item Service
     *
     * @param webValueKey - Web Appointment Key
     * @param carValueKey - Car Appointment Key
     * @throws Throwable
     */
    public void validateAppointmentItemService(String webValueKey, String carValueKey) throws Throwable {
        LOGGER.info("validateAppointmentItemService started");
        if (webValueKey.contains(APPOINTMENT_START_TIME)) {
            driver.scenarioData.setData(webValueKey, webAppointmentConversionToCarFormat());
        }
        webServices.returnJsonResponseForItemLevelOrderHistoryServiceRequestBody(Constants.HYBORDRNUM);
        webServices.setDataForCarItemOrderHistoryValidation(Constants.HYBORDRNUM);
        LOGGER.info("Car Appointment Date: " + driver.scenarioData.getData(carValueKey));
        int maxAttempts = 30;
        int attempt = 0;
        if (webValueKey.contains("resche")) {
            do {
                if (!driver.scenarioData.getData(webValueKey).equalsIgnoreCase(driver.scenarioData.getData(carValueKey))) {
                    webServices.returnJsonResponseForItemLevelOrderHistoryServiceRequestBody(Constants.HYBORDRNUM);
                    webServices.setDataForCarItemOrderHistoryValidation(Constants.HYBORDRNUM);
                    LOGGER.info("Car Appointment Date (" + maxAttempts + "): " + driver.scenarioData.getData(carValueKey));
                    attempt++;
                    driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
                } else {
                    break;
                }
            } while (attempt < maxAttempts);
        }
        Assert.assertTrue(driver.scenarioData.getData(webValueKey).equalsIgnoreCase(driver.scenarioData.getData(carValueKey)));
        LOGGER.info("validateAppointmentItemService completed");
    }

    /**
     * shortcut for saving
     */
    public void shortcutForSave() {
        LOGGER.info("shortcutForSave started");
        Actions actions = new Actions(webDriver);
        actions.keyDown(Keys.CONTROL)
                .sendKeys(Keys.chord("s"))
                .perform();
        LOGGER.info("shortcutForSave completed");
    }

    /**
     * Validates appointment in Header service
     *
     * @param webValueKey - Web Appointment Key
     * @param carValueKey - Car Appointment Key
     * @param emailId     - Email id
     * @throws Throwable
     */
    public void validateAppointmentHeaderService(String webValueKey, String carValueKey, String emailId) throws Throwable {
        LOGGER.info("validateAppointmentHeaderService started");
        if (webValueKey.contains(APPOINTMENT_START_TIME)) {
            driver.scenarioData.setData(webValueKey, webAppointmentConversionToCarFormat());
        }
        webServices.assertDataForHybrisCustomerCarHeaderOrderHistoryValidation(emailId);
        LOGGER.info("Car Appointment Date: " + driver.scenarioData.getData(carValueKey));
        int maxAttempts = 30;
        int attempt = 0;
        if (webValueKey.contains("resche")) {
            do {
                if (!driver.scenarioData.getData(webValueKey).equalsIgnoreCase(driver.scenarioData.getData(carValueKey))) {
                    webServices.assertDataForHybrisCustomerCarHeaderOrderHistoryValidation(emailId);
                    LOGGER.info("Car Appointment Date (" + maxAttempts + "): " + driver.scenarioData.getData(carValueKey));
                    attempt++;
                    driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
                } else {
                    break;
                }
            } while (attempt < maxAttempts);
        }
        Assert.assertTrue(driver.scenarioData.getData(webValueKey).equalsIgnoreCase(driver.scenarioData.getData(carValueKey)));
        LOGGER.info("validateAppointmentHeaderService completed");
    }
}
