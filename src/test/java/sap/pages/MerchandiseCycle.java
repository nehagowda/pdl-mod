package sap.pages;

import common.Config;
import common.Constants;
import common.CommonExcel;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;
import java.util.Random;
import java.util.logging.Logger;

public class MerchandiseCycle {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private CommonTCodes commonTCodes;
    private CommonExcel commonExcel;
    private final Logger LOGGER = Logger.getLogger(MerchandiseCycle.class.getName());

    public MerchandiseCycle(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        commonTCodes = new CommonTCodes(driver);
        PageFactory.initElements(webDriver, this);
        commonExcel = new CommonExcel();
    }

    @FindBy(id = "M0:U:::1:34")
    private static WebElement article;

    @FindBy(id = "M0:U:::17:34")
    private static WebElement articleZrpm;

    @FindBy(id = "M0:U:::2:34")
    private static WebElement salesOrganization;

    @FindBy(id = "M0:U:::3:34")
    private static WebElement distributionChannel;

    @FindBy(id = "M0:U:::2:34")
    private static WebElement distributionChannelZrPM;

    @FindBy(id = "M0:U:::3:34")
    private static WebElement priceList;

    @FindBy(id = "M0:U:::4:34")
    private static WebElement competitor;

    @FindBy(id = "M0:U:::22:34")
    private static WebElement purchasePriceDeterum;

    @FindBy(id = "M0:U:::23:34")
    private static WebElement salesPriceDeterum;

    @FindBy(id = "M0:U:::27:34")
    private static WebElement listVariant;

    @FindBy(id = "M0:D:13::btn[8]")
    private static WebElement execute;

    @FindBy(id = "M0:D:13::btn[7]")
    private static WebElement selectAll;

    @FindBy(xpath = "//*[text()='FinalPrOld']/following::input[7]")
    private static WebElement finalPrValue;

    @FindBy(id = "M0:D:13::btn[35]")
    private static WebElement changeListField;

    @FindBy(id = "M1:U:::6:33-txt")
    private static WebElement percentageChange;

    @FindBy(id = "M1:U:::1:9")
    private static WebElement amount;

    @FindBy(id = "M1:D:13::btn[0]")
    private static WebElement copy;

    @FindBy(id = "M1:U:::7:33-txt")
    private static WebElement changeByAbsoluteValule;

    @FindBy(id = "wnd[0]/sbar_msg")
    private static WebElement successImageForTransaction;

    @FindBy(id = "M0:D:10::btn[11]-img")
    private static WebElement saveButton;

    @FindBy(id = "M2:D:13::btn[5]")
    private static WebElement checkOk;

    @FindBy(id = "M1:D:13::btn[0]")
    private static WebElement continuePopup;

    @FindBy(id = "M1:U:::0:5")
    private static WebElement overrideCompetitor;

    @FindBy(xpath = "//span[contains(@id,'1,10#if-r')]")
    private static WebElement overridePrice;

    @FindBy(xpath = "//span[contains(@id,',26#if')]")
    private static WebElement mapPrice;

    @FindBy(xpath = "//span[contains(@id,'grid#14')]//input[ contains(@id,'1,2#if')]")
    private static WebElement savedSuccessfully;

    @FindBy(xpath = "//span[contains(@id,'grid#17')]//input[ contains(@id,'1,2#if')]")
    private static WebElement finalSaveSuccessful;

    @FindBy(id = "M0:U:::6:34")
    private static WebElement pricingArticle;

    @FindBy(xpath = "//*[text()='Test Run']/preceding::*[1]")
    private static WebElement testRunCheckbox;

    @FindBy(xpath = "//*[text()='Display Errors Only']/following::img[1]")
    private static WebElement displayErrorsOnly;

    @FindBy(id = "grid#17.115#1,14#icp")
    private static WebElement yellowTriangle;

    @FindBy(id = "grid#20.115#1,7#if")
    private static WebElement oldPriceValue;

    @FindBy(xpath = "(//span[contains(@id,'1,13')])[2]")
    private static WebElement newPriceValue;

    @FindBy(id = "M1:U:::0:20")
    private static WebElement outputDevice;

    @FindBy(id = "M1:D:13::btn[13]-img")
    private static WebElement greenCheckPrintParam;

    @FindBy(id = "M2:D:13::btn[0]-img")
    private static WebElement greenCheckInformation;

    @FindBy(id = "M1:U:::1:2")
    private static WebElement immediate;

    @FindBy(id = "M1:D:13::btn[0]-img")
    private static WebElement checkStartTime;

    @FindBy(id = "M1:D:13::btn[11]-img")
    private static WebElement saveStartTime;

    @FindBy(id = "wnd[0]/sbar_msg-txt")
    private static WebElement backgroundJobMessage;

    @FindBy(id = "M0:U:::0:22")
    private static WebElement jobName;

    @FindBy(id = "M0:46:::12:64_l")
    private static WebElement spoolList;

    @FindBy(id = "M0:U:::7:34")
    private static WebElement validityDateFrom;

    @FindBy(id = "M0:46:::12:64_l")
    private static WebElement jobStatus;

    @FindBy(xpath = "(//*[contains(@lsdata,'s_b_more.png')])[1]")
    private static WebElement moreButtonNextToArticle;

    @FindBy(xpath = "//div[contains(@id,'_vscroll-Nxt')]")
    private static WebElement scrollNext;

    @FindBy(xpath = "//div[contains(@id,'_hscroll-Nxt')]")
    public static WebElement scrollRight;

    @FindBy(xpath = "//img[contains(@src,'exec')]")
    private static WebElement execInMardEntriesTable;

    @FindBy(xpath = "//input[contains(@id,'[1,2]_c')]")
    private static WebElement inputFieldInMard;

    @FindBy(xpath = "//label[text()='Site']/following::input[4]")
    private static WebElement siteInputField;

    @FindBy(xpath = "//label[text()='Movement Type']/following::input[1]")
    private static WebElement movementTypeMb1c;

    @FindBy(xpath = "//label[text()='Site']/following::input[1]")
    private static WebElement siteMb1c;

    @FindBy(xpath = "//label[text()='Storage Location']/following::input[1]")
    private static WebElement storageLocationMb1c;

    @FindBy(xpath = "//*[contains(@title,'New Items')]")
    private static WebElement newItemsButtonMb1c;

    @FindBy(xpath = "//tr[@class='lsScrollbar__container--next']")
    private static WebElement scrollInMard;

    @FindBy(xpath = "//*[text()='Do not use cycle']/preceding::*[1]")
    private static WebElement doNotUseCycle;

    @FindBy(id = "M0:U:::19:34")
    private static WebElement crossSection;

    @FindBy(id = "M0:U:::20:34")
    private static WebElement aspectRatio;

    @FindBy(id = "M0:U:::21:34")
    private static WebElement rimSize;

    @FindBy(xpath = "(//*[contains(@id,'#2,7#dd')])[1]")
    private static WebElement firstPositionRow;

    @FindBy(xpath = "(//*[contains(@id,'#3,7#dd')])[1]")
    private static WebElement secondPositionRow;

    @FindBy(id = "grid#11.115#2,7#dd-btn")
    private static WebElement dropDown;

    @FindBy(id = "grid#11.115#2,10#cb-lbl")
    private static WebElement firstCheckbox;

    @FindBy(id = "grid#11.115#3,10#cb-lbl")
    private static WebElement secondCheckbox;

    @FindBy(id = "grid#11.115#2,11#if-r")
    private static WebElement priceRelationshipFirstRow;

    @FindBy(id = "grid#11.115#3,11#if-r")
    private static WebElement priceRelationshipSecondRow;

    @FindBy(xpath = "//*[contains(text(),'Article')]/following::img[1]")
    private static WebElement multipleSelection;

    @FindBy(xpath = "//*[contains(@title,'Copy (F8)')]")
    private static WebElement copyMultipleSelection;

    @FindBy(xpath="//*[contains(@id,'1,5#if')][not (contains(@id,'1,5#if-r'))][contains(@value,'C0000000')]")
    private static WebElement matchedCompetitor;

    @FindBy(xpath = "//*[contains(@id,'1,2#icp')]")
    private static WebElement matchCheckbox;

    @FindBy(xpath = "//*[contains(@id,'4,3#if')][not (contains(@id,'4,3#if-r'))][contains(@value,'C000000011')]")
    private static WebElement costco;

    @FindBy(xpath = "//*[contains(@id,'9,3#if')][not (contains(@id,'9,3#if-r'))]")
    private static WebElement walmart;

    private static final By inputTagsInItemsTableMb1cBy = By.xpath("//span[contains(text(),'Items')]/following::input");

    private final String MOVEMENT_TYPE = "Movement Type MB1C";
    private final String SITE = "Site MB1C";
    private final String STORAGE_LOCATION = "Storage Location MB1C";
    public final String ARTICLE = "Article";
    public final String ARTICLE_ZRPM = "Article For Zrpm";
    public final String SALES_ORGANIZATION = "Sales Organization";
    public final String DISTRIBUTION_CHANNEL = "Distribution Channel";
    public final String DISTRIBUTION_CHANNEL_ZRPM = "Distribution Channel For Zrpm";
    public final String PURCHASE_PRICE_DETERM = "Purchase Price Determ";
    public final String SALES_PRICE_DETERM = "Sales Price Determ";
    public final String PRICE_LIST = "Price List";
    public final String COMPETITOR = "Competitor";
    public final String LIST_VARIANT = "List Variant";
    public final String FINAL_PR_VALUE = "Final Pr Value";
    public final String CHANGE_LIST_FIELD = "Change List Field";
    public final String PERCENTAGE_CHANGE = "Percentage Change";
    public final String AMOUNT = "Amount";
    public final String COPY = "Copy";
    public final String CHANGE_BY_ABSOLUTE_VALUE = "Change By Absolute Value";
    public final String SUCCESS_IMAGE_FOR_TRANSACTION = "Success Message For Transaction";
    public final String SAVE_BUTTON = "Save Button";
    public final String OVERRIDE_PRICE = "Override Price";
    public final String MAP_PRICE = "MAP Pricing";
    public final String CONTINUE_POPUP = "Continue";
    public final String OVERRIDE_COMPETITOR = "Override Competitor";
    public final String SAVED_SUCCESSFULLY = "Saved Successfully";
    public final String FINAL_SAVE_SUCCESSFUL = "Final Save Successful";
    private static final String CHECK_OK = "Check Ok";
    public final String PRICING_ARTICLE = "Pricing Article";
    public final String TEST_RUN = "Test Run";
    public final String DISPLAY_ERRORS_ONLY = "Display Errors Only";
    public final String YELLOW_TRIANGLE = "ID";
    public final String OLD_PRICE_VALUE = "Cond. Old";
    public final String NEW_PRICE_VALUE = "New Cond.";
    public final String OUTPUT_DEVICE = "Output Device";
    public final String GREEN_CHECK_PRINT_PARAM = "Green Check on Print Parameters";
    public final String GREEN_CHECK_INFORMATION = "Green Check on Information";
    public final String IMMEDIATE = "immediate";
    public final String CHECK_START_TIME = "Check Start Time";
    public final String SAVE_START_TIME = "Save Start Time";
    public final String BACKGROUND_JOB_MESSAGE = "Background job message";
    public final String JOB_NAME = "Job name";
    public final String SPOOL_LIST = "Spool list";
    public final String DATE_FROM = "Validitiy Date from";
    public final String JOB_STATUS = "Job Status";
    public final String EXCEL_SHEET_LOCATION_ARTICLES_STG = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\Functions\\SAP\\Data Tables\\articlesSTG.xlsx";
    public final String EXCEL_SHEET_LOCATION_ARTICLES_QA = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\Functions\\SAP\\Data Tables\\articlesQA.xlsx";
    public static final String SITE_INPUT_FIELD = "Site";
    private static final String TIRES = "tires";
    private static final String EXTENSION = "xlsx";
    private static final String NEW_ENTRIES = "New Entries";
    private static int rowCount = 0;
    public final String DO_NOT_USE_CYCLE = "Do not use cycle";
    public final String CROSS_SECTION = "Cross Section";
    public final String ASPECT_RATIO = "Aspect Ratio";
    public final String RIM_SIZE ="Rim Size";
    public final String FIRST_POSITION_ROW = "first";
    public final String SECOND_POSITION_ROW = "second";
    public final String DROPDOWN =  "DropDown";
    public final String FIRST_CHECKBOX = "first checkbox";
    public final String SECOND_CHECKBOX = "second checkbox";
    public final String PRICE_RELATIONSHIP_FIRST_ROW = "first row";
    public final String PRICE_RELATIONSHIP_SECOND_ROW = "second row";
    public final String MULTIPLE_SELECTION = "Multiple selection";
    public final String COPY_MULTIPLE_SELECTION = "Copy F8";
    public final String MATCH_CHECKBOX = "Match";

    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started");
        switch (elementName) {
            case STORAGE_LOCATION:
                return storageLocationMb1c;
            case SITE:
                return siteMb1c;
            case MOVEMENT_TYPE:
                return movementTypeMb1c;
            case ARTICLE:
                return article;
            case ARTICLE_ZRPM:
                return articleZrpm;
            case SALES_ORGANIZATION:
                return salesOrganization;
            case DISTRIBUTION_CHANNEL:
                return distributionChannel;
            case DISTRIBUTION_CHANNEL_ZRPM:
                return distributionChannelZrPM;
            case PURCHASE_PRICE_DETERM:
                return purchasePriceDeterum;
            case SALES_PRICE_DETERM:
                return salesPriceDeterum;
            case PRICE_LIST:
                return priceList;
            case COMPETITOR:
                return competitor;
            case LIST_VARIANT:
                return listVariant;
            case Constants.EXECUTE:
                return execute;
            case Constants.SELECT_ALL:
                return selectAll;
            case FINAL_PR_VALUE:
                return finalPrValue;
            case CHANGE_LIST_FIELD:
                return changeListField;
            case PERCENTAGE_CHANGE:
                return percentageChange;
            case AMOUNT:
                return amount;
            case COPY:
                return copy;
            case CHANGE_BY_ABSOLUTE_VALUE:
                return changeByAbsoluteValule;
            case SUCCESS_IMAGE_FOR_TRANSACTION:
                return successImageForTransaction;
            case SAVE_BUTTON:
                return saveButton;
            case CHECK_OK:
                return checkOk;
            case OVERRIDE_PRICE:
                return overridePrice;
            case MAP_PRICE:
                return mapPrice;
            case CONTINUE_POPUP:
                return continuePopup;
            case OVERRIDE_COMPETITOR:
                return overrideCompetitor;
            case SAVED_SUCCESSFULLY:
                return savedSuccessfully;
            case FINAL_SAVE_SUCCESSFUL:
                return finalSaveSuccessful;
            case PRICING_ARTICLE:
                return pricingArticle;
            case TEST_RUN:
                return testRunCheckbox;
            case DISPLAY_ERRORS_ONLY:
                return displayErrorsOnly;
            case YELLOW_TRIANGLE:
                return yellowTriangle;
            case OLD_PRICE_VALUE:
                return oldPriceValue;
            case NEW_PRICE_VALUE:
                return newPriceValue;
            case OUTPUT_DEVICE:
                return outputDevice;
            case GREEN_CHECK_PRINT_PARAM:
                return greenCheckPrintParam;
            case GREEN_CHECK_INFORMATION:
                return greenCheckInformation;
            case IMMEDIATE:
                return immediate;
            case CHECK_START_TIME:
                return checkStartTime;
            case SAVE_START_TIME:
                return saveStartTime;
            case BACKGROUND_JOB_MESSAGE:
                return backgroundJobMessage;
            case JOB_NAME:
                return jobName;
            case SPOOL_LIST:
                return spoolList;
            case DATE_FROM:
                return validityDateFrom;
            case JOB_STATUS:
                return jobStatus;
            case SITE_INPUT_FIELD:
                return siteInputField;
            case DO_NOT_USE_CYCLE:
                return doNotUseCycle;
            case CROSS_SECTION:
                return crossSection;
            case ASPECT_RATIO:
                return aspectRatio;
            case RIM_SIZE:
                return rimSize;
            case FIRST_POSITION_ROW:
                return firstPositionRow;
            case SECOND_POSITION_ROW:
                return secondPositionRow;
            case DROPDOWN:
                return dropDown;
            case FIRST_CHECKBOX:
                return firstCheckbox;
            case SECOND_CHECKBOX:
                return secondCheckbox;
            case PRICE_RELATIONSHIP_FIRST_ROW:
                return priceRelationshipFirstRow;
            case PRICE_RELATIONSHIP_SECOND_ROW:
                return priceRelationshipSecondRow;
            case MULTIPLE_SELECTION:
                return multipleSelection;
            case COPY_MULTIPLE_SELECTION:
                return copyMultipleSelection;
            case MATCH_CHECKBOX:
                return matchCheckbox;
            default:
                Assert.fail("FAIL: Could not find icon that matched string passed from step");
                return null;
        }
    }

    /**
     * This Method is click any element in merchandise cycle page.
     *
     * @param elementName - send in elementName and gets the WebElement from returnElement method.
     */
    public void clickInMerchandiseCyclePage(String elementName) {
        LOGGER.info("clickInMerchandiseCyclePage started");
        commonActions.click(returnElement(elementName));
        LOGGER.info("clickInMerchandiseCyclePage completed");
    }

    /**
     * This Method selects the desired radio button in merchandise cycle page.
     *
     * @param option - is the text matching the button to be selected.
     */
    public void clickOnDesiredParameterInMerchandiseCyclePage(String option) {
        LOGGER.info("clickOnDesiredParameterInMerchandiseCyclePage started");
        WebElement element = webDriver.findElement(By.xpath("//*[text()='" + option + "']/preceding::img[1]"));
        commonActions.click(element);
        LOGGER.info("clickOnDesiredParameterInMerchandiseCyclePage completed");
    }

    /**
     * Asserts text displayed contains text expected
     *
     * @param elementName The element to check
     * @param message     Text to verify
     */
    public void assertTextDisplayedInMerchandiseCyclePage(String elementName, String message) {
        LOGGER.info("assertTextDisplayedInMerchandiseCyclePage started");
        driver.verifyTextDisplayed(returnElement(elementName), message);
        LOGGER.info("assertTextDisplayedInMerchandiseCyclePage completed");
    }

    /**
     * Verifies displayed value matches expected value in Merchandise Cycle page
     *
     * @param elementName string to verify the value of
     * @param value       expected value of the field to compare to
     */
    public void assertValDisplayedMatchesValExpectedInMerchandiseCyclePage(String elementName, String value) {
        LOGGER.info("assertValDisplayedMatchesValExpectedInMerchandiseCyclePage started");
        driver.waitForPageToLoad();
        driver.assertElementAttributeString(returnElement(elementName), Constants.VALUE, value);
        LOGGER.info("assertValDisplayedMatchesValExpectedInMerchandiseCyclePage completed");
    }

    /**
     * Save the updated price to excel file
     *
     * @param file    File name with full file path
     * @param tabName Excel sheet tab name
     * @param append  'true' or 'false' whether to append to end of the data in the file
     */
    public void storePriceToExcel(String file, String tabName, boolean append) {
        LOGGER.info("storePriceToExcel started");
        String price = overridePrice.getAttribute(Constants.VALUE);
        try {
            commonExcel.saveDataToExcel(file, price, tabName, append);
        } catch (Exception e) {
            Assert.fail("FAIL: Exception occurred in writing '" + price + "' to the excel file: " + file + "'");
        }
        LOGGER.info("storePriceToExcel completed for Order # " + price);
    }

    /**
     * Method to scroll
     *
     * @param visibleElement - Element which has to be visible
     * @param scrollButton - Web element for the scroll button
     */
    public void scroll(WebElement visibleElement, WebElement scrollButton) {
        LOGGER.info("scroll started");
        if (driver.isElementDisplayed(scrollButton)) {
            while (!visibleElement.isDisplayed()) {
                try {
                    driver.waitForElementVisible(scrollButton);
                    scrollButton.click();
                } catch (StaleElementReferenceException s) {
                    WebElement element = webDriver.findElement(commonActions.extractByLocatorFromWebElement(scrollButton));
                    commonActions.click(element);
                }
            }
        }
        LOGGER.info("scroll completed");
    }

    /**
     * Sends input for pricing update
     *
     * @param element - WebELement
     */
    public void sendKeysPriceWithType(WebElement element) {
        LOGGER.info("sendKeysPriceWithType started");
        Random random = new Random();
        String input = String.valueOf(random.nextInt((51)) + 100);
        driver.scenarioData.setCurrentPrice(input);
        scroll(element, scrollRight);
        commonActions.sendKeys(element, input);
        LOGGER.info("sendKeysPriceWithType completed");
    }

    /**
     * Grab the updated price after saving it
     *
     * @param priceType price parameter type
     * @return String price
     */
    public String returnPrice(String priceType) {
        LOGGER.info("returnPrice started");
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        String updatedPrice;
        if (priceType.equalsIgnoreCase(MAP_PRICE)) {
            updatedPrice = mapPrice.getAttribute(Constants.VALUE);
        } else {
            updatedPrice = overridePrice.getAttribute(Constants.VALUE);
        }
        LOGGER.info("returnPrice completed");
        return updatedPrice;
    }

    /**
     * This Method Verifies the new price
     *
     * @param element    - WebElement
     * @param validation - value to verify
     */
    public void verifyValueAttribute(WebElement element, String validation) {
        LOGGER.info("verifyValueAttribute started");
        driver.waitForElementVisible(element);
        String value = element.getAttribute(Constants.VALUE);
        Assert.assertTrue("FAIL: The value found of : " + value + " did not match the expected validation of: " + validation + ".", value.equalsIgnoreCase(validation));
        LOGGER.info("verifyValueAttribute completed");
    }

    /**
     * Gets scenario data for the price.
     *
     * @return string value of price in scenario data.
     */
    public String getNewPrice() {
        LOGGER.info("getNewPrice started");
        String newPrice;
        try {
            return newPrice = driver.scenarioData.getCurrentPrice();
        } catch (Exception e) {
            Assert.fail("FAIL: Was unable to get new price from excel");
        }
        LOGGER.info("getNewPrice completed");
        return null;
    }

    /**
     * Method will read the list of articles from Excel sheet and stores it into driver.scenarioData.setData()
     * hash map function
     *
     * @throws Throwable
     */
    private void storeListOfArticlesFromExcel() throws Throwable {
        LOGGER.info("storeListOfArticlesFromExcel started");
        String fileName;
        if (Config.getDataSet().equalsIgnoreCase(Constants.QA)) {
            fileName = EXCEL_SHEET_LOCATION_ARTICLES_QA;
        } else {
            fileName = EXCEL_SHEET_LOCATION_ARTICLES_STG;
        }
        File file = new File(fileName);
        FileInputStream inputStream = new FileInputStream(file);
        String sheetname = TIRES;
        Sheet sheet;
        if (fileName.toLowerCase().contains(EXTENSION)) {
            XSSFWorkbook xssfworkbook = new XSSFWorkbook(inputStream);
            XSSFSheet xssfsheet = xssfworkbook.getSheet(sheetname);
            sheet = xssfsheet;
        } else {
            HSSFWorkbook hssfworkbook = new HSSFWorkbook(inputStream);
            HSSFSheet hssfsheet = hssfworkbook.getSheet(sheetname);
            sheet = hssfsheet;
        }
        rowCount = sheet.getLastRowNum();
        for (int i = 0; i <= rowCount; i++) {
            Row articleRows = sheet.getRow(i);
            try {
                Double cellValue = articleRows.getCell(0).getNumericCellValue();
                if (cellValue != null) {
                    driver.scenarioData.setData(ARTICLE + i, cellValue.toString().substring(0,
                            cellValue.toString().length() - 2));
                }
            } catch (IllegalStateException ise) {
                String cellValue = articleRows.getCell(0).getStringCellValue();
                if (cellValue != null) {
                    driver.scenarioData.setData(ARTICLE + i, cellValue);
                }
            }
        }
        arrangeAllArticlesInAscendingOrder();
        LOGGER.info("storeListOfArticlesFromExcel completed");
    }

    /**
     * This method will key in all articles into MARD Table in se16n which were saved in
     * driver.scenarioData.getData() hash map function
     *
     * @throws Throwable
     */
    public void keyInAllArticlesInMardTable() throws Throwable {
        LOGGER.info("keyInAllArticlesInMardTable started");
        storeListOfArticlesFromExcel();
        int numberOfNewEntries;
        commonActions.click(moreButtonNextToArticle);
        commonActions.switchFrameContext(0);
        if (rowCount < 5) {
            numberOfNewEntries = 1;
        } else {
            int totalArticlesInExcelSheet = rowCount + 1;
            double totalEmptyFieldsInMard = (double) totalArticlesInExcelSheet / 5;
            numberOfNewEntries = (int) Math.ceil(totalEmptyFieldsInMard);
        }
        for (int i = 0; i <= numberOfNewEntries; i++) {
            commonActions.clickOnExactText(NEW_ENTRIES);
        }
        for (int i = 0; i <= rowCount; i++) {
            driver.waitForMilliseconds();
            commonActions.sendKeys(commonTCodes.returnElementFromATable("1", "2", 1),
                    driver.scenarioData.getData(ARTICLE + i));
            try {
                driver.waitForElementVisible(scrollNext);
                scrollNext.click();
            } catch (StaleElementReferenceException s) {
                WebElement staleElement = webDriver.findElement
                        (commonActions.extractByLocatorFromWebElement(scrollNext));
                staleElement.click();
            }
        }
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        driver.performKeyAction(Keys.F8);
        commonActions.switchToMainWindow();
        LOGGER.info("keyInAllArticlesInMardTable completed");
    }

    /**
     * This method will arrange all the saved articles in Ascending order and saves it to driver.scenarioData.genericData
     * hash map
     */
    private void arrangeAllArticlesInAscendingOrder() {
        LOGGER.info("arrangeAllArticlesInAscendingOrder started");
        int[] articleOrderArray = new int[rowCount + 1];
        for (int i = 0; i <= rowCount; i++) {
            articleOrderArray[i] = Integer.valueOf(driver.scenarioData.getData(ARTICLE + i));
        }
        for (int i = 0; i < articleOrderArray.length; i++) {
            Arrays.sort(articleOrderArray);
            driver.scenarioData.setData(ARTICLE + i, String.valueOf(articleOrderArray[i]));
            LOGGER.info(i + ") Article: " +
                    driver.scenarioData.getData(ARTICLE + i));
        }
        LOGGER.info("arrangeAllArticlesInAscendingOrder completed");
    }

    /**
     * This method will read the value of inventory remaining for each article and subtracts it.
     * The resultant value is stored in driver.scenarioData.setData() hash map function.
     * Key for each value is the article itself.
     */
    public void checkAndStoreInventory() {
        LOGGER.info("checkAndStoreInventory started");
        for (int i = 0; i <= rowCount; i++) {
            String article = driver.scenarioData.getData(ARTICLE + i);
            String attributeText;
            WebElement element = webDriver.findElement
                    (By.xpath("//div[contains(text(),'" + article + "')]/following::div[16]"));
            if (element.getText().contains("-")) {
                String cleanedArrtibuteText = element.getText().replace("-", "");
                attributeText = ("-" + cleanedArrtibuteText).replace(" ", "");
            } else {
                attributeText = element.getText();
            }
            Double inventory = Constants.ONE_THOUSAND - Double.valueOf(attributeText
                    .replace(",", ""));
            String remainingInventory = String.valueOf(inventory);
            driver.scenarioData.setData(driver.scenarioData.getData(ARTICLE + i), remainingInventory);
            commonActions.click(scrollInMard);
            LOGGER.info("Inventory which will be added for article: " +
                    driver.scenarioData.getData(ARTICLE + i) + " is: " + remainingInventory);
        }
        LOGGER.info("checkAndStoreInventory completed");
    }

    /**
     * This method will input articles in MB1c under movement type 501, Storage location 0001 and respective site.
     */
    public void inputArticlesToUpdateInventory() {
        LOGGER.info("inputArticlesToUpdateInventory started");
        driver.waitForPageToLoad();
        int numberOfEntryLoops;
        int articlesWithInventoryLessThanThousandCounter = 0;
        int articlesWithInventoryEqualToThousandCounter = 0;
        int totalArticlesCounter = rowCount;
        int numberOfRows = webDriver.findElements(inputTagsInItemsTableMb1cBy).size() / 7;
        LOGGER.info("Number of rows: " + numberOfRows);
        if (rowCount < numberOfRows) {
            numberOfEntryLoops = 1;
        } else {
            int totalArticleCount = rowCount + 1;
            double totalNumberOfRows = (double) numberOfRows;
            numberOfEntryLoops = (int) Math.ceil(totalArticleCount / totalNumberOfRows);
        }
        //TODO: Method should be modified to create rows only for articles which has inventory less than 1000
        for (int i = 1; i <= numberOfEntryLoops; i++) {
            if (totalArticlesCounter >= 0) {
                for (int j = 0; j < numberOfRows; j++) {
                    if (Float.valueOf(driver.scenarioData.
                            getData(driver.scenarioData.getData(ARTICLE + totalArticlesCounter))) > 0) {
                        articlesWithInventoryEqualToThousandCounter++;
                        driver.waitForMilliseconds();
                        String articleId = "M0:46:3::" + j + ":7";
                        String quantityId = "M0:46:3::" + j + ":26";
                        driver.waitForMilliseconds();
                        driver.clearInputAndSendKeys(webDriver.findElement(By.id(articleId)),
                                driver.scenarioData.getData(ARTICLE + totalArticlesCounter));
                        driver.clearInputAndSendKeys(webDriver.findElement(By.id(quantityId)),
                                driver.scenarioData.getData(driver.scenarioData.getData(ARTICLE + totalArticlesCounter)));
                    } else {
                        articlesWithInventoryLessThanThousandCounter++;
                    }
                    totalArticlesCounter--;
                    if (totalArticlesCounter < 0) {
                        break;
                    }
                }
                commonActions.click(newItemsButtonMb1c);
            }
        }
        LOGGER.info(" Articles with inventory less than thousand: " + articlesWithInventoryLessThanThousandCounter);
        LOGGER.info("Articles with inventory equal to thousand: " + articlesWithInventoryEqualToThousandCounter);
        LOGGER.info("Total Number of articles: " + (rowCount + 1));
        LOGGER.info("inputArticlesToUpdateInventory completed");
    }

    /**
     * This method compares updated SAP price to the Hybris price (for online catalag version)
     */
    public void compareSapToHybrisPrice(){
        LOGGER.info("compareSapToHybrisPrice started");
        driver.scenarioData.setCurrentPrice("101");
        String hybrisPrice = driver.scenarioData.getCurrentPrice();
        Assert.fail("FAIL: Expected price: " + getNewPrice() + ", but Actual was : " + hybrisPrice);
        LOGGER.info("compareSapToHybrisPrice completed");
    }

    /**
     * This method save the competitors matched in the initial screen to compare to for the next step
     * @return String matchedCompetitor displayed on the initial screen
     */
    public String saveMatchedCompetitor() {
        LOGGER.info("saveMatchedCompetitor started");
        String matchedCompetitorValue = matchedCompetitor.getAttribute(Constants.VALUE);
        LOGGER.info("saveMatchedCompetitor completed");
        return matchedCompetitorValue;
    }

    /**
     * This method sets the new matched competitor based on the previous selection result
     * @param - currentMatchedCompetitor
     *
     */
    public void setNewMatchedCompetitor(String currentMatchedCompetitor){
        LOGGER.info("setNewMatchedCompetitor started");
        if (currentMatchedCompetitor.equalsIgnoreCase(Constants.COSTCO)){
            walmart.click();
        }else
            costco.click();
        LOGGER.info("setNewMatchedCompetitor completed");
    }
}