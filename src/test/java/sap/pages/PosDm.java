package sap.pages;

import common.Config;
import common.Constants;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;
import webservices.pages.WebServices;
import java.awt.datatransfer.Clipboard;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;
import java.util.HashMap;
import java.util.logging.Logger;

public class PosDm {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private IDocPage iDocPage;
    private final Logger LOGGER = Logger.getLogger(PosDm.class.getName());


    public PosDm(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        iDocPage = new IDocPage(driver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "M0:U:1:2B265::0:34")
    private static WebElement store;

    @FindBy(id = "M0:U:3:4B270::0:30")
    private static WebElement postingDate;

    @FindBy(id = "M0:D:13::btn[8]")
    private static WebElement execute;

    @FindBy(id = "M1:D:13::btn[8]")
    private static WebElement executeButtonInFrame;

    @FindBy(id = "M0:U:3::0:1-title")
    private static WebElement header;

    @FindBy(xpath = "//span[contains(@id,',1#icp-r')]")
    private static WebElement transactionStatus;

    @FindBy(id = "M0:U:3:4B271::2:32")
    private static WebElement transactionNumber;

    @FindBy(id = "M0:D:13::btn[17]")
    private static WebElement getVariantButton;

    @FindBy(id = "M1:U:::2:34")
    private static WebElement createdBy;

    @FindBy(xpath = "//input[@id=\"M0:D:10::okcd\"]")
    private static WebElement tCode;

    @FindBy(xpath = "//img[@title=\"OK\"]")
    private static WebElement successfulTransaction;

    @FindBy(xpath = "//img[@action=\"close\"]")
    private static WebElement closeButton;

    @FindBy(xpath = "//*[contains(text(),'LNME')]")
    private static WebElement customerLastName;

    @FindBy(xpath = "//*[contains(text(),'FNME')]")
    private static WebElement customerFirstName;

    @FindBy(xpath = "//*[contains(text(),'ADDR1')]")
    private static WebElement customerAddressLine1;

    @FindBy(xpath = "//*[contains(text(),'CUSTCITY')]")
    private static WebElement customerCity;

    @FindBy(xpath = "//*[contains(text(),'CUSTSTATE')]")
    private static WebElement customerState;

    @FindBy(xpath = "//*[contains(text(),'COUNTRY')]")
    private static WebElement customerCountry;

    @FindBy(xpath = "//*[contains(text(),'ZIP')]")
    private static WebElement customerZip;

    @FindBy(xpath = "//*[contains(text(),'HOMEPH')]")
    private static WebElement customerHomePhone;

    @FindBy(xpath = "//*[contains(text(),'WORKPH')]")
    private static WebElement customerWorkPhone;

    @FindBy(xpath = "//*[contains(text(),'PHONE')]")
    private static WebElement customerPhone;

    @FindBy(xpath = "//div[contains(@id,'_vscroll-Nxt')]")
    private static WebElement scrollBarNextButton;

    @FindBy(xpath = "//div[contains(@id,'_hscroll-Nxt')]")
    private static WebElement scrollBarNextButtonHorizontal;

    @FindBy(xpath = "//span[contains(text(),\"QA_AGGREGATION\")]")
    private static WebElement QAAggregation;

    @FindBy(xpath = "//span[contains(text(),\"QA_OUTBOUND_PR\")]")
    private static WebElement QAOutboundPR;

    @FindBy(xpath = "//input[contains(@value,'QA_IDOC_PROC')]")
    private static WebElement QAIdocProc;

    @FindBy(xpath = "//div[contains(@class,'urST5SCMetricInner')]")
    private static WebElement salesMovementRow;

    @FindBy(xpath = "//div[contains(@id,'152_toolbar_btn18')]")
    private static WebElement processTaskOnline;

    @FindBy(xpath = "//div[contains(@id,'176_btn1')]")
    private static WebElement executeProcessing;

    @FindBy(xpath = "//span[contains(@id,'_btn5-r')]")
    private static WebElement documentFlowOutBound;

    @FindBy(xpath = "//span[contains(text(),'Outbox: IDoc')]")
    private static WebElement externalDocument;

    @FindBy(xpath = "//span[contains(@id,'42#0#2#mi')]")
    private static WebElement externalDocumentRtc;

    @FindBy(xpath = "//div[contains(@lsdata,'Document')]")
    private static WebElement documentFlowButton;

    @FindBy(xpath = "//div[contains(@lsdata,'Overview')][contains(@lsdata,'Transactions')]")
    private static WebElement overviewbutton;

    @FindBy(xpath = "//span[contains(text(),'Outbox: IDoc')]/following::span[contains(text(),'0')][1]")
    private static WebElement externalDocument1;

    @FindBy(xpath = "//span[contains(text(),'Outbox: IDoc')]/following::span[6]")
    private static WebElement outboxIdoc;

    @FindBy(xpath = "//span[contains(text(),'Outbox: IDoc')]/following::span[contains(text(),'0')][2]")
    private static WebElement externalDocument2;

    @FindBy(id = "M0:U:::2:21")
    private static WebElement tableNameInput;

    @FindBy(xpath = "//*[text()='DOCNUM']/following::input[1]")
    private static WebElement aggrNumber;

    @FindBy(xpath = "//*[text()='DOCNUM']/following::div[5]")
    private static WebElement multipleSelection;

    @FindBy(id = "M0:U:::6:34")
    private static WebElement logicalSystem;

    @FindBy(xpath = "//td[contains(@id,'[2,2]')]")
    private static WebElement singleValue;

    @FindBy(id = "M1:D:13::btn[8]")
    private static WebElement copyButton;

    @FindBy(xpath = "//span[contains(@id,'1,8#if') and contains(@tabindex,'-1')]")
    private static WebElement objKey1;

    @FindBy(xpath = "//span[contains(@id,'2,8#if') and contains(@tabindex,'-1')]")
    private static WebElement objKey2;

    @FindBy(xpath = "//span[contains(@tabindex,'-1') and contains(@id,'1,24#if')]")
    private static WebElement transactionId1;

    @FindBy(xpath = "//span[contains(@tabindex,'-1') and contains(@id,'2,24#if')]")
    private static WebElement transactionId2;

    @FindBy(xpath = "//*[text()='TID']/following::input[1]")
    private static WebElement transactionInputField;

    @FindBy(xpath = "//*[text()='TID']/following::div[5]")
    private static WebElement transactionArrowButton;

    @FindBy(xpath = "//td[contains(@id,'2,2')]")
    private static WebElement singleValueRowSap;

    @FindBy(id = "M1:D:13::btn[8]-img")
    private static WebElement copyButtonSap;

    @FindBy(id = "M0:D:13::btn[8]-img")
    private static WebElement executeButtonSAP;

    @FindBy(xpath = "//*[contains(@title,'DOCNUM')]/following::span[contains(@id,'1,2#if')][1]")
    private static WebElement idocValue1;

    @FindBy(xpath = "//*[contains(@title,'DOCNUM')]/following::span[contains(@id,'1,2#if')][1]/child::input[1]")
    private static WebElement idocValueStg1;

    @FindBy(xpath = "//*[contains(@title,'DOCNUM')]/following::span[contains(@id,'2,2#if')][1]")
    private static WebElement idocValue2;

    @FindBy(xpath = "//*[contains(@title,'DOCNUM')]/following::span[contains(@id,'2,2#if')][1]/child::input[1]")
    private static WebElement idocValueStg2;

    @FindBy(xpath = "//img[@action='close']")
    private static WebElement closeimage;

    @FindBy(xpath = "//div[contains(@id,'M0:U-scrollV-Nxt')]")
    private static WebElement scrollVerticalWper;

    @FindBy(id = "M0:U:::0:21")
    private static WebElement tablese16n;

    @FindBy(id = "//*[contains(text(),'Layout')]/following::input[1]")
    private static WebElement layoutSe16n;

    @FindBy(xpath = "//span[contains(@id,'[2,3]_c-r')]")
    private static WebElement idocInputSe16n;

    @FindBy(xpath = "//span[contains(@id,',1#if-r')]")
    private static WebElement idocRowSe16n;

    @FindBy(xpath = "//span[contains(@id,',2#if-r')]")
    private static WebElement messageTypeRowSe16n;

    @FindBy(xpath = "//div[contains(text(),'WPUUMS')]/preceding::div[8]")
    private static WebElement wpuumsOutbound;

    @FindBy(xpath = "//div[contains(text(),'WPUTAB')]/preceding::div[8]")
    private static WebElement wputabOutbound;

    @FindBy(xpath = "//span[contains(@id,'1,7#if-r')]")
    private static WebElement totalAmountPosdm;

    @FindBy(xpath = "//*[contains(text(),'Store')]//following::span[1]")
    private static WebElement store1001ExpandNode;

    @FindBy(xpath = "//*[contains(text(),'Store')]//following::span[1]")
    private static WebElement store1981ExpandNode;

    @FindBy(xpath = "//a[contains(text(),'Financial Transaction')]/following::a[contains(text(),'310')][1]")
    private static WebElement layawayTransactionAmountField;

    @FindBy(id = "M0:D:10::btn[3]")
    private static WebElement backButtonPosdm;

    @FindBy(xpath = "//div[contains(text(),'1002')]")
    private static WebElement store1002;

    @FindBy(xpath = "//div[contains(@id,'46.154_hscroll-Prev')]")
    private static WebElement scrollPrev;

    @FindBy(xpath = "//div[contains(text(),'WEB')]")
    private static WebElement webVirtual;

    @FindBy(xpath = "//span[text()='Type']/following::span[contains(@id,',9#if')][1]")
    private static WebElement transtype;

    @FindBy(xpath = "//*[contains(@title,'Table selection menu')]/following::img[1]")
    private static WebElement errorButton;

    @FindBy(xpath = "(//*[contains(@title,'Change Task Status ')])[1]")
    private static WebElement changeTask;

    @FindBy(xpath = "//a[contains(text(),'PRODTAXED')]")
    private static WebElement prodTaxedFlag;

    @FindBy(xpath = "//*[text()='Start of Transaction']/following::input[2]")
    private static WebElement endDatePosDm;

    @FindBy(xpath = "//*[text()='G/L account document']/preceding::div[2]")
    public static WebElement bopisGlAccountForWpufib;

    @FindBy(xpath = "//*[text()='G/L account document'][2]/preceding::div[2]")
    public static WebElement glAccountDocumentLine2;

    @FindBy(xpath = "//*[text()='Store']/following::img[1]")
    private static WebElement storeMultipleOptions;

    @FindBy(xpath = "//span[contains(@id,'[2,2]_c-r')]/parent::div[1]")
    private static WebElement storeLineTwo;

    @FindBy(xpath = "//*[contains(text(),'156 STATUS')]")
    private static WebElement statusInPosDm;

    @FindBy(xpath = "//*[contains(text(),'Taxes')]")
    private static WebElement taxesLineItem;

    @FindBy(xpath = "//*[contains(text(),'Store')]//following::span[1]")
    private static WebElement store1002ExpandNode;

    @FindBy(xpath = "//*[text()='Balancing']//following::div[contains(@id,'_hscroll-Nxt')]")
    private static WebElement balancingScrollButton;

    @FindBy(xpath = "//*[text()='Balancing']//following::div[contains(@id,'_hscroll-Prev')]")
    private static WebElement balancingScrollButtonPrev;

    @FindBy(xpath = "//*[text()='Store']//following::div[contains(@id,'_hscroll-Nxt')][1]")
    private static WebElement storeSalesButtonNxt;

    @FindBy(xpath = "//*[text()='Store']//following::div[contains(@id,'_hscroll-Prev')][1]")
    private static WebElement storeSalesButtonPrev;

    @FindBy(xpath = "//*[text()='Total Financial Amt']")
    private static WebElement totalSalesAmount;

    @FindBy(xpath = "//*[text()='TransCurr.']")
    private static WebElement transactionCurreny;

    @FindBy(xpath = "//*[text()='Sales Totals']")
    private static WebElement salesTotals;

    @FindBy(xpath = "//*[text()='Store']")
    private static WebElement storeRow;

    @FindBy(xpath = "//*[text()='Store']//following::span[contains(@id,'24#if-r')][1]")
    private static WebElement salesTotalAmount;

    @FindBy(xpath = "//*[text()='Balancing']/following::span[contains(@id,'15#header-CONTENT')]")
    private static WebElement startRowInBalancing;


    public static final By rowsInPosDmBy = By.xpath("//tr[@iidx]");

    private static final By expandNodeLineItemBy = By.xpath("//*[contains(@title,'Collapse Node')]");

    private static final By collapseNodeLineItemBy = By.xpath("//*[contains(@title,'Expand Node')]");

    private static final By goodsMovementDataBy = By.xpath("//a[contains(text(),'Goods movements')]/following::a[text()]");

    public static final By transactionNumberColumnsBy = By.xpath("//*[contains(@id,'10#if-r')]");

    public static final By totalRecordColumnsBy = By.xpath("//*[contains(@id,'9#if-r')]");

    public static final By hybrisOrderNumberBy = By.xpath("//*[contains(text(),'HYBORD')]");

    private static final By dtdWebOrderNumberBy = By.xpath("//a[contains(text(),'Customer information')]//preceding::a[1]");

    private static final By saleItemDataBy = By.xpath("//a[contains(text(),'Sales Item Data')]/following::a[text()]");

    private static final By taskStatusOverview = By.xpath("//img[@title=\"OK\"]");

    private static final By lineItemsBy = By.xpath("//div[contains(@class,'urST5SCMetricInner')]");

    private static final By storeExpandNodeBy = By.xpath("//span[text()='Store']/following::span[1]");

    private static final By tenderElementStatusBy = By.xpath("//*[contains(text(),'Balancing')]/following::tr[@vpm='mrss-cont']/child::td[2]//child::tr[@iidx][@rr]");

    public static final String TRANSACTION_TYPE = "Transaction Type";
    public static final String WEB_VIRTUAL = "Web Virtual";
    private static final String SCROLL_PREV = "Scroll prev";
    private static final String BACK_BUTTON = "Back";
    private static final String IDOC_FIELD_IN_WPER = "Idoc Field in WPER";
    private static final String OUTBOX_IDOC = "Outbox IDOC";
    private static final String STORE = "Store";
    private static final String GL_ACCOUNT_FOR_WPUFIB = "G/L Account from WPUFIB";
    private static final String POSTING_DATE = "Posting Date";
    private static final String EXECUTE = "Execute";
    private static final String EXECUTE_BUTTON_IN_FRAME = "Execute Button In Frame";
    private static final String TCODE = "Tcode";
    private static final String HEADER = "Header";
    private static final String TRANSACTION_NUMBER = "Transaction Number";
    private static final String TRANSACTION_STATUS = "Transaction Status";
    private static final String CLOSE_BUTTON = "Close Button";
    private static final String CITY = "City";
    private static final String STATE = "State";
    private static final String ZIP = "Zip";
    private static final String COUNTRY = "Country";
    private static final String FIRST_NAME = "First Name";
    private static final String LAST_NAME = "Last Name";
    private static final String ADDRESS_LINE_1 = "Address Line1";
    private static final String GET_VARIANT_BUTTON = "Get Variant";
    private static final String CREATED_BY = "Created By";
    private static final String QA_Aggregation = "QA Aggregation";
    private static final String QA_OUTBOUND_PR = "QA Outbound PR";
    private static final String QA_IDOC_PROC = "QA Idoc Proc";
    private static final String SALES_MOVEMENT_ROW = "Sales movement row";
    private static final String PROCESS_TASK_ONLINE = "Process task online";
    private static final String EXECUTE_PROCESSING = "Execute processing";
    private static final String DOCUMENT_FLOW_OUT_BOUNT = "Document flow outbound";
    private static final String EXTERNAL_DOCUMENT = "external document";
    private static final String DOCUMENT_FLOW = "document flow button";
    private static final String EXTERNAL_DOCUMENT_VALUE1 = "External document value 1";
    private static final String EXTERNAL_DOCUMENT_VALUE2 = "External document value 2";
    private static final String TABLE_NAME = "Table name";
    private static final String AGGREGATOR_NUMBER = "Aggregator number";
    private static final String MULTIPLE_SELECTION = "Multiple selection";
    private static final String LOGICAL_SYSTEM = "Logical system";
    private static final String SINGLE_VALUE_ROW = "Single value row";
    private static final String COPY_BUTTON = "Copy button";
    private static final String OBJECT_KEY1 = "Object key 1";
    private static final String OBJECT_KEY2 = "Object key 2";
    private static final String TRANSACTION_ID_1 = "Transaction Id 1";
    private static final String TRANSACTION_ID_2 = "Transaction Id 2";
    private static final String TRANSACTION_INPUT_FIELD = "Transaction input field";
    private static final String TRANSACTION_ARROW_BUTTON = "Transaction arrow button";
    private static final String SINGLE_VALUE_ROW_SAP = "Single value row SAP";
    private static final String COPY_BUTTON_SAP = "Copy button SAP";
    private static final String EXECUTE_BUTTON_SAP = "Execute button SAP";
    private static final String IDOC_VALUE_1 = "Idoc value 1";
    private static final String IDOC_VALUE_2 = "Idoc value 2";
    private static final String HOME_PHONE = "Home phone";
    private static final String WORK_PHONE = "Work Phone";
    private static final String PHONE = "Phone";
    private static final String CLOSE = "Close";
    private static final String VERTICAL_SCROLL = "Vertical Scroll";
    private static final String HORIZONTAL_SCROLL = "Horizontal Scroll";
    private static final String VERTICAL_SCROLL_WPER = "Vertical Scroll Wper";
    private static final String TABLE_SE16N = "Table SE16N";
    private static final String LAYOUT_SE16N = "Layout";
    private static final String IDOC_INPUT_SE16N = "Idoc Input SE16N";
    public static final String WPUUMS_ROW = "WPUUMS";
    public static final String WPUTAB_ROW = "WPUTAB";
    public static final String WPUUMS_ROW_OUTBOUND = "WPUUMS Row";
    public static final String WPUTAB_ROW_OUTBOUND = "WPUTAB Row";
    public static final String IDOC_ROW_SE16N = "Idoc Row";
    public static final String MESSAGE_TYPE_SE16N = "WPUUMS/WPUTAB";
    private static final String TOTAL_AMOUNT = "Total Amount posdm";
    private static final String STORE_1001_EXPAND_NODE = "1001";
    private static final String STORE_1981_EXPAND_NODE = "1981";
    private static final String STORE_1002_EXPAND_NODE = "1002";
    private static final String LAYWAY_TRANSACTION_AMOUNT = "Layaway transaction amount";
    private static final String OVERVIEW = "Overview of POS Transactions ";
    private static final String WPUFIB = "WPUFIB";
    private static final String STORE_1002 = "Store 1002";
    private static final String PRODTAXED = "PRODTAXED";
    private static final String WEBELEMENT_LINE_ITEMS = "//a[contains(text(),'Sales Item Data')]/following::a[text()]";
    public static final String POSDM_SCRIPTS_DATA_LOCATION = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\POSDM Transactional Data";
    public static final String HYBRIS_ORDER_NUMBER_HASH_MAP_KEY = "hybrisOrderNumber";
    public static final String TIME_STAMP_AFTER_HASP_MAP_KEY = "after";
    public static final String TIME_STAMP_BEFORE_HASP_MAP_KEY = "before";
    private static final String BOPIS_GL_ACCOUNT_FOR_WPUFIB = "Bopis G/L Account from WPUFIB";
    public static final String SALES_ITEM_DATA = "Sales Item Data";
    public static final String MEANS_OF_PAYMENT = "Means of Payment";
    private static final String CUSTOMER_ENHANCEMENTS = "Customer Enhancements";
    private static final String PODM_STATUS = "status";
    public static final String BOPIS_SCRIPTS_DATA_LOCATION = "\\\\corpfiles\\corporate\\DTC-IT-Quality Assurance" +
            "-TESTING\\Bubble files\\Functions\\Store POS\\Data Tables";
    private static final String PLACEHOLDER = "Placeholder";
    private static final String USED = "used";
    private static final String START_OF_TRANSACTION = "Start of Transaction";
    private static final String TOTAL_SALES_AMT = "Total Sales Amt";
    private static final String TRANSACTION_CURRENCY = "Transaction Currency";
    private static final String START = "Start";
    private static final String BAlANCING_SCROLL_BUTTON = "Balancing Scroll Button";
    private static final String BAlANCING_SCROLL_BUTTON_PREV = "Balancing Scroll Button Prev";
    private static final String STORE_TOTAL_SCROLL = "STORE TOTAL SCROLL";
    private static final String SALES_TOTAL_ROW = "SALES TOTAL ROW";
    private static final String SALES_TOTAL_VALUE = "SALES TOTAL VALUE";
    private static final String STORE_ROW = "STORE ROW";
    private static final String STORE_TOTAL_SCROLL_PREV = "STORE_TOTAL_SCROLL_PREV";
    private static final String GET_VARIANT = "Get Variant";
    private static final String CREATED_BY_VARIANT = "Created by";
    private static final String VARIANT = "Variant";
    private static final String CHOOSE = "Choose";
    private static final String EXPAND_NODE = "Expand Node";
    private static final String TOTAL_POS = "Total POS";
    private static final String MATCH = "match";
    private static final String MIS_MATCH = "mismatch";
    private static final String TOT = "Tot.";
    private static final String TOTAL = "Total";
    private static final String TOTAL_MNS_OF_PAYMENT = "Total Mns of Payment";
    private static final String TOTAL_TAXES = "Total Taxes";
    private static final String TOTAL_TAX_AMOUNT = "Total Tax Amount";
    private static final String BACK = "Back";
    private static final String DATA_SAVED = "Data saved";
    private static final String SALES_TOTAL = "Sales Totals ";
    private static final String SHEET_NAME_1601 = "salesitemtype";
    private static final String INBOUND_MONITOR = "Inbound Monitor";
    private static final String ZTOT = "ZTOT";
    private static final String T_1605 = "1605";
    private static final String T_1603 = "1603";
    private static final String T_1602 = "1602";
    private static final String T_1601 = "1601";
    private static final String T_1009 = "1009";
    private static final String TABLE_SELECTION_MENU = "Table selection menu";
    private static final String SELECT_ALL = "Select All";
    private static final String PROCESS_TASKS_ONLINE = "Process Tasks Online ";
    private static final String EXECUTE_PROCESSING_BUTTON_TEXT = "Execute Processing ";
    private static final String NO_DATA_EXISTS = "No data exists";
    private static final String TOTALS_RECORDS = "Totals Records";
    private static final String BALANCING = "Balancing";
    private static final String SALES_MOVEMENT = "Sales Movement";
    public static final String ERROR_BUTTON = "Error Button";
    public static final String CHANGE_TASK = "Change Task";
    private int numberOfTaxesLineItems = 0;
    private int numberOfSalesItemTypes = 0;

    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started");
        switch (elementName) {
            case CHANGE_TASK:
                return changeTask;
            case ERROR_BUTTON:
                return errorButton;
            case TRANSACTION_TYPE:
                return transtype;
            case WEB_VIRTUAL:
                return webVirtual;
            case SCROLL_PREV:
                return scrollPrev;
            case STORE_1002:
                return store1002;
            case WPUFIB:
                return IDocPage.wpufib;
            case IDOC_FIELD_IN_WPER:
                return IDocPage.iDocFieldInWper;
            case OUTBOX_IDOC:
                return outboxIdoc;
            case GL_ACCOUNT_FOR_WPUFIB:
                return IDocPage.glAccountForWpufib;
            case STORE:
                return store;
            case POSTING_DATE:
                return postingDate;
            case EXECUTE:
                return execute;
            case TCODE:
                return tCode;
            case HEADER:
                return header;
            case TRANSACTION_STATUS:
                return transactionStatus;
            case TRANSACTION_NUMBER:
                return transactionNumber;
            case CLOSE_BUTTON:
                return closeButton;
            case FIRST_NAME:
                return customerFirstName;
            case LAST_NAME:
                return customerLastName;
            case ADDRESS_LINE_1:
                return customerAddressLine1;
            case CITY:
                return customerCity;
            case STATE:
                return customerState;
            case ZIP:
                return customerZip;
            case COUNTRY:
                return customerCountry;
            case GET_VARIANT_BUTTON:
                return getVariantButton;
            case CREATED_BY:
                return createdBy;
            case EXECUTE_BUTTON_IN_FRAME:
                return executeButtonInFrame;
            case QA_Aggregation:
                return QAAggregation;
            case QA_OUTBOUND_PR:
                return QAOutboundPR;
            case QA_IDOC_PROC:
                return QAIdocProc;
            case SALES_MOVEMENT_ROW:
                return salesMovementRow;
            case PROCESS_TASK_ONLINE:
                return processTaskOnline;
            case EXECUTE_PROCESSING:
                return executeProcessing;
            case DOCUMENT_FLOW_OUT_BOUNT:
                return documentFlowOutBound;
            case EXTERNAL_DOCUMENT:
                if (Config.getDataSet().contains(Constants.QA) || Config.getDataSet().contains(Constants.RK2)) {
                    return externalDocument;
                } else {
                    return externalDocumentRtc;
                }
            case DOCUMENT_FLOW:
                return documentFlowButton;
            case OVERVIEW:
                return overviewbutton;
            case EXTERNAL_DOCUMENT_VALUE1:
                return externalDocument1;
            case EXTERNAL_DOCUMENT_VALUE2:
                return externalDocument2;
            case TABLE_NAME:
                return tableNameInput;
            case AGGREGATOR_NUMBER:
                return aggrNumber;
            case MULTIPLE_SELECTION:
                return multipleSelection;
            case LOGICAL_SYSTEM:
                return logicalSystem;
            case SINGLE_VALUE_ROW:
                return singleValue;
            case COPY_BUTTON:
                return copyButton;
            case OBJECT_KEY1:
                return objKey1;
            case OBJECT_KEY2:
                return objKey2;
            case TRANSACTION_ID_1:
                return transactionId1;
            case TRANSACTION_ID_2:
                return transactionId2;
            case TRANSACTION_INPUT_FIELD:
                return transactionInputField;
            case TRANSACTION_ARROW_BUTTON:
                return transactionArrowButton;
            case SINGLE_VALUE_ROW_SAP:
                return singleValueRowSap;
            case COPY_BUTTON_SAP:
                return copyButtonSap;
            case EXECUTE_BUTTON_SAP:
                return executeButtonSAP;
            case IDOC_VALUE_1:
                if(Config.getDataSet().equalsIgnoreCase(Constants.STG)){
                    return idocValueStg1;
                } else {
                    return idocValue1;
                }
            case IDOC_VALUE_2:
                if(Config.getDataSet().equalsIgnoreCase(Constants.STG)){
                    return idocValueStg2;
                } else {
                    return idocValue2;
                }
            case HOME_PHONE:
                return customerHomePhone;
            case WORK_PHONE:
                return customerWorkPhone;
            case PHONE:
                return customerPhone;
            case CLOSE:
                return closeButton;
            case VERTICAL_SCROLL:
                return scrollBarNextButton;
            case HORIZONTAL_SCROLL:
                return scrollBarNextButtonHorizontal;
            case VERTICAL_SCROLL_WPER:
                return scrollVerticalWper;
            case LAYOUT_SE16N:
                return layoutSe16n;
            case TABLE_SE16N:
                return tablese16n;
            case IDOC_INPUT_SE16N:
                return idocInputSe16n;
            case IDOC_ROW_SE16N:
                return idocRowSe16n;
            case MESSAGE_TYPE_SE16N:
                return messageTypeRowSe16n;
            case WPUTAB_ROW_OUTBOUND:
                return wputabOutbound;
            case WPUUMS_ROW_OUTBOUND:
                return wpuumsOutbound;
            case STORE_1001_EXPAND_NODE:
                return store1001ExpandNode;
            case STORE_1981_EXPAND_NODE:
                return store1981ExpandNode;
            case LAYWAY_TRANSACTION_AMOUNT:
                return layawayTransactionAmountField;
            case BACK_BUTTON:
                return backButtonPosdm;
            case PRODTAXED:
                return prodTaxedFlag;
            case BOPIS_GL_ACCOUNT_FOR_WPUFIB:
                return bopisGlAccountForWpufib;
            case PODM_STATUS:
                return statusInPosDm;
            case STORE_1002_EXPAND_NODE:
                return store1002ExpandNode;
            case TOTAL_SALES_AMT:
                return totalSalesAmount;
            case TRANSACTION_CURRENCY:
                return transactionCurreny;
            case BAlANCING_SCROLL_BUTTON:
                return balancingScrollButton;
            case BAlANCING_SCROLL_BUTTON_PREV:
                return balancingScrollButtonPrev;
            case STORE_TOTAL_SCROLL:
                return storeSalesButtonNxt;
            case STORE_TOTAL_SCROLL_PREV:
                return storeSalesButtonPrev;
            case START:
                return startRowInBalancing;
            case SALES_TOTAL_ROW:
                return salesTotals;
            case STORE_ROW:
                return storeRow;
            case SALES_TOTAL_VALUE:
                return salesTotalAmount;
            default:
                Assert.fail("FAIL: Could not find icon that matched string passed from step");
                return null;
        }
    }

    /**
     * This Method is click any element in posdm page.
     *
     * @param elementName - send in elementName and gets the WebElement from returnElement method.
     */
    public void clickInPosDmPage(String elementName) {
        LOGGER.info("clickInPosDmPage started");
        commonActions.click(returnElement(elementName));
        LOGGER.info("clickInPosDmPage completed");
    }

    /**
     * Verifies if the transactions were successful.
     */
    public void verifyIfTransactionsAreSuccessful() {
        LOGGER.info("verifyIfTransactionsAreSuccessful started");
        List<WebElement> input = webDriver.findElements(taskStatusOverview);
        for (int i = 0; i < input.size(); i++) {
            driver.isElementDisplayed(input.get(i));
        }
        LOGGER.info("verifyIfTransactionsAreSuccessful completed");
    }

    /**
     * Verifies article number and sales price in posdm.
     */
    public void assertArticleAndSalesPriceOnSalesItemDataInPosdm() {
        LOGGER.info("assertArticleAndSalesPriceOnSalesItemDataInPosdm started");
        List<WebElement> elements = webDriver.findElements(saleItemDataBy);
        for (int i = 0; i < IDocPage.rowCount; i++) {
            int hashMapKeyIncrement = i + 1;
            if (!driver.isElementDisplayed(elements.get(i))) {
                while (!elements.get(i).isDisplayed()) {
                    try {
                        driver.waitForElementVisible(scrollBarNextButton);
                        scrollBarNextButton.click();
                    } catch (StaleElementReferenceException s) {
                        WebElement element = webDriver.findElement(commonActions.extractByLocatorFromWebElement(scrollBarNextButton));
                        commonActions.click(element);
                    }
                }
            }
            driver.verifyTextDisplayedCaseInsensitive(elements.get(i), driver.scenarioData.getData
                    (CommonActions.ARTICLE_FROM_LINE + hashMapKeyIncrement));
            Assert.assertTrue("FAIL: Text expected is not displayed! '" + driver.scenarioData.getData
                    (CommonActions.AMOUNT_FROM_LINE + hashMapKeyIncrement).replace("-", "") + "' " +
                    "is not present in '" + elements.get(i).getText(), elements.get(i).getText().replace(",",
                    "").contains(driver.scenarioData.getData
                    (CommonActions.AMOUNT_FROM_LINE + hashMapKeyIncrement).replace("-", "")));
            LOGGER.info("assertArticleAndSalesPriceOnSalesItemDataInPosdm completed");
        }
    }

    /**
     * This method will click the element multiple times.
     *
     * @param counter     - Integer, Number of times the element has to be clicked
     * @param elementName - element Name and returnWebElement will return the web element.
     */
    public void clickMultipleTimes(int counter, String elementName) {
        LOGGER.info("clickMultipleTimes started");
        if (driver.isElementDisplayed(returnElement(elementName))) {
            while (counter != 0) {
                try {
                    driver.waitForElementClickable(returnElement(elementName));
                    try {
                        returnElement(elementName).click();
                    } catch (ElementClickInterceptedException ecie) {
                        driver.clickWithActions(returnElement(elementName));
                    }
                    counter--;
                } catch (StaleElementReferenceException s) {
                    WebElement element = webDriver.findElement(commonActions.extractByLocatorFromWebElement(returnElement(elementName)));
                    commonActions.click(element);
                    counter--;
                }
            }
        } else {

        }
        LOGGER.info("clickMultipleTimes completed");
    }

    /**
     * This element will click the input "scrollElementName" till it finds the element "visibleElementName"
     *
     * @param visibleElementName- The element which you are looking for
     * @param scrollElementName-  The element which you have to click in order to scroll
     */
    public void scrollTillElementIsVisible(String visibleElementName, String scrollElementName) {
        LOGGER.info("scrollTillElementIsVisible started");
        if (driver.isElementDisplayed(returnElement(scrollElementName))) {
            while (!returnElement(visibleElementName).isDisplayed()) {
                try {
                    driver.waitForElementVisible(returnElement(scrollElementName));
                    returnElement(scrollElementName).click();
                } catch (StaleElementReferenceException s) {
                    WebElement element = webDriver.findElement(commonActions.extractByLocatorFromWebElement(returnElement(scrollElementName)));
                    commonActions.click(element);
                }
            }
        }
        LOGGER.info("scrollTillElementIsVisible completed");
    }

    /**
     * Send keys into elements in PosDm page
     *
     * @param elementName - send in elementName and gets the WebELement from returnElement method.
     * @param inputText   - value to enter
     */
    public void sendKeysInPosDmPage(String elementName, String inputText) {
        LOGGER.info("sendKeysInPosDmPage started");
        driver.waitForPageToLoad();
        commonActions.sendKeys(returnElement(elementName), inputText);
        LOGGER.info("sendKeysInPosDmPage completed");
    }

    /**
     * Saves the element text value. Key is the element name.
     *
     * @param elementName value of the string that should be saved into HashMap
     */
    public void saveElementValueFromText(String elementName) {
        LOGGER.info("saveElementValueFromText started");
        if (elementName.equalsIgnoreCase(BOPIS_GL_ACCOUNT_FOR_WPUFIB)) {
            driver.waitForElementVisible(returnElement(elementName), Constants.ONE_HUNDRED_EIGHTY);
            if (returnElement(elementName).getText().contains("-")) {
                driver.scenarioData.genericData.put(elementName, glAccountDocumentLine2.getText());
            }
        }
        driver.waitForElementVisible(returnElement(elementName), Constants.ONE_HUNDRED_EIGHTY);
        driver.scenarioData.genericData.put(elementName, returnElement(elementName).getText());
        LOGGER.info("saveElementValueFromText completed");
    }

    /**
     * Saves the element text value. Key is the element name.
     *
     * @param elementName value of the string that should be saved into HashMap
     */
    public void saveElementValueFromValueAttribute(String elementName) {
        LOGGER.info("saveElementValueFromValueAttribute started");
        driver.waitForElementVisible(returnElement(elementName), Constants.ONE_HUNDRED_EIGHTY);
        String elementAttribute = returnElement(elementName).getText();
        if(elementAttribute.equalsIgnoreCase("")){
            elementAttribute = returnElement(elementName).getAttribute(Constants.VALUE);
        }
        driver.scenarioData.genericData.put(elementName, elementAttribute);
        LOGGER.info("saveElementValueFromValueAttribute completed");
    }

    /**
     * Sends saved values into element provided.
     *
     * @param elementName string where the saved hashmap value has to be entered
     * @param key         String key from hash map
     */
    public void enterSavedValuesIntoElement(String key, String elementName) {
        LOGGER.info("enterSavedValuesIntoElement started");
        if(elementName.equalsIgnoreCase(SINGLE_VALUE_ROW)) {
            driver.clickWithActions(returnElement(elementName));
            driver.waitForMilliseconds();
        }
        driver.performSendKeysWithActions(returnElement(elementName), driver.scenarioData.genericData.get(key));
        LOGGER.info("enterSavedValuesIntoElement completed");
    }

    /**
     * This method will confirm the job is complete in POSDM. It gets the User name from environment variables and
     * matches with the user on confirmation page after job is done.
     */
    public void aggregateJobConfirmation() {
        LOGGER.info("aggregateJobConfirmation started");
        WebElement element = webDriver.findElement(By.xpath("//span[contains(text(),'" + Config.getSapUserName().toUpperCase() + "')]"));
        driver.waitForElementVisible(element, Constants.FIVE_HUNDRED);
        LOGGER.info("aggregateJobConfirmation completed");
    }

    /**
     * Asserts Total amount on POSDM Workbench
     *
     * @param sheetName - Excel sheet name
     * @throws Exception
     */
    public void verifyTotalTransactionAmount(String sheetName, String location) throws Exception {
        LOGGER.info("verifyTotalTransactionAmount started");
        iDocPage.readFromExcelAndReturnSingleValue(location, sheetName,
                IDocPage.rowCount + 1, 6, "Total Amount");
        if (driver.scenarioData.getData("Total Amount").contains("-")) {

            Assert.assertTrue("Fail: WebElement text is: " + totalAmountPosdm.getText().replace(",",
                    "") + " And validation text is: " + driver.scenarioData.getData
                    ("Total Amount").replace("-", ""), totalAmountPosdm.getText().replace(
                    ",", "").contains(driver.scenarioData.getData
                    ("Total Amount").replace("-", "")));

            driver.verifyTextDisplayedCaseInsensitive(totalAmountPosdm, "-");
        } else {
            Assert.assertTrue("Fail: WebElement text is: " + totalAmountPosdm.getText().replace(",",
                    "") + " And validation text is: " + driver.scenarioData.getData
                    ("Total Amount").replace("-", ""), totalAmountPosdm.getText().replace(
                    ",", "").contains(driver.scenarioData.getData
                    ("Total Amount").replace("-", "")));
        }
        LOGGER.info("verifyTotalTransactionAmount completed");
    }

    /**
     * This method will expand the transaction folder in /n/posdw/mon0
     */
    public void expandDateColumn() {
        LOGGER.info("expandDateColumn started");
        commonActions.click(webDriver.findElement(By.xpath("//span[text()='" + driver.getTodayDate("MM/dd/yyyy") + "']/preceding::span[2]")));
        LOGGER.info("expandDateColumn completed");
    }

    /**
     * This method will expand the transaction folder in /n/posdw/mon0
     */
    public void expandDateColumnNoDate() {
        LOGGER.info("expandDateColumnNoDate started");
        WebElement dateNode = webDriver.findElement(By.xpath("//span[text()='Posting Date']/following::span[1]"));
        if(dateNode.getAttribute(Constants.TITLE).equalsIgnoreCase(EXPAND_NODE)) {
            commonActions.click(dateNode);
        }
        LOGGER.info("Checked for Expanding column date");
        LOGGER.info("expandDateColumnNoDate completed");
    }

    /**
     * This method will expand the transaction folder in /n/posdw/mon0
     */
    public void collapseDateColumnNoDate() {
        LOGGER.info("collapseDateColumnNoDate started");
        WebElement dateNode = webDriver.findElement(By.xpath("//span[text()='Posting Date']/following::span[1]"));
        if(!dateNode.getAttribute(Constants.TITLE).equalsIgnoreCase(EXPAND_NODE)) {
            commonActions.click(dateNode);
        }
        LOGGER.info("Checked for Expanding column date");
        LOGGER.info("expandDateColumnNoDate completed");
    }

    /**
     * Verifies article number in posdt.
     */
    public void assertArticlesItemDataInPosDt() {
        LOGGER.info("assertArticlesItemDataInPosDt started");
        List<WebElement> elements = webDriver.findElements(goodsMovementDataBy);
        for (int i = 0; i < IDocPage.rowCount; i++) {
            int hashMapKeyIncrement = i + 1;
            if (!driver.isElementDisplayed(elements.get(i))) {
                while (!elements.get(i).isDisplayed()) {
                    try {
                        driver.waitForElementVisible(scrollBarNextButton);
                        scrollBarNextButton.click();
                    } catch (StaleElementReferenceException s) {
                        WebElement element = webDriver.findElement(commonActions.extractByLocatorFromWebElement(scrollBarNextButton));
                        commonActions.click(element);
                    }
                }
            }
            driver.verifyTextDisplayedCaseInsensitive(elements.get(i), driver.scenarioData.getData
                    (CommonActions.ARTICLE_FROM_LINE + hashMapKeyIncrement));
            LOGGER.info("assertArticlesItemDataInPosDt completed");
        }
    }

    /**
     * This method will confirm the job is complete in POSDM. It gets the User name from environment variables and
     * matches with the user on confirmation page after job is done.
     */
    public void waitForBwOrCarToExecute() {
        LOGGER.info("waitForBworCarToExecute started");
        WebElement element = webDriver.findElement(By.xpath("//*[contains(text(),'" + driver.scenarioData.getData(IDocPage.IDOC_NUMBER_FROM_EXCEL) + "')]"));
        driver.waitForElementVisible(element, Constants.FIVE_HUNDRED);
        LOGGER.info("waitForBworCarToExecute completed");
    }

    /**
     * returns node element for a line item with input article to expand or collapse
     *
     * @param articleNumber  - Article number
     * @param lineItemNumber - invoice line number in transaction data
     * @param nodeTitle      - Expand or Collapse
     * @return WebElement of node for a line item
     */
    private WebElement assertArticleNumberAndReturnNodeElementForLineItem(String articleNumber, int lineItemNumber, String nodeTitle) {
        LOGGER.info("assertArticleNumberAndReturnNodeElementForLineItem started");
        List<WebElement> salesLineItems = webDriver.findElements(saleItemDataBy);
        String lineItemText = salesLineItems.get(lineItemNumber - 1).getText();
        Assert.assertTrue("FAIL: The line item does not contain the article: " +
                articleNumber + " | Line item text = " + lineItemText, lineItemText.contains(articleNumber));
        LOGGER.info("assertArticleNumberAndReturnNodeElementForLineItem completed");
        return webDriver.findElement(By.xpath(WEBELEMENT_LINE_ITEMS + "[" + lineItemNumber + "]/preceding::span[@title='" + nodeTitle + "'][1]"));
    }

    /**
     * Generates time stamp according to the timezone in POSDM format
     *
     * @param state - State for time zone
     * @param key   - Hash map kay to store the time stamp
     */
    public void recordTimeStampInPosDmFormat(String state, String key, int additionalMinutes) {
        LOGGER.info("recordTimeStampInPosDmFormat started");
        String todaysDate = driver.getTodayDate("yyyy/MM/dd").replace("/", "");
        Calendar cal = Calendar.getInstance();
        if (key.equalsIgnoreCase(Constants.AFTER)) {
            cal.add(Calendar.MINUTE, 5);
        }
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.HOURS_MINUTES_SECONDS);
        sdf.setTimeZone(TimeZone.getTimeZone(commonActions.returnTimeZoneBasedOnState(state)));
        String timeStamp = sdf.format(cal.getTime()).replace(":", "");
        driver.scenarioData.setData(key, todaysDate + timeStamp);
        LOGGER.info("Time " + key + " order creation: " + todaysDate + timeStamp);
        LOGGER.info("recordTimeStampInPosDmFormat completed");
    }

    /**
     * method will look for the generated web order and extracts the dtd order number to search in DTD
     */
    public void searchForhybrisOrderRelatedTransaction() {
        LOGGER.info("searchForhybrisOrderRelatedTransaction started");
        int orderNumber = Integer.valueOf(driver.scenarioData.getCurrentOrderNumber());
        String cleanedOrderNumber = String.valueOf(orderNumber);
        boolean isFound = false;
        int limit = webDriver.findElements(transactionNumberColumnsBy).size();
        for (int i = 0; i < limit; i++) {
            WebElement element = webDriver.findElements(transactionNumberColumnsBy).get(i);
            commonActions.doubleClickOnElement(element);
            driver.waitForPageToLoad();
            if (webDriver.findElement(hybrisOrderNumberBy).getText().contains(cleanedOrderNumber)) {
                String dtdOrderNumber = webDriver.findElement(dtdWebOrderNumberBy).getText();
                //Todo: Integrate with excel. For now a back up when search with web order number fails in DTD POS
                driver.scenarioData.setData(HYBRIS_ORDER_NUMBER_HASH_MAP_KEY, dtdOrderNumber.substring(0,
                        dtdOrderNumber.length() - 5));
                LOGGER.info("Order number Captured: " + driver.scenarioData.
                        getData(HYBRIS_ORDER_NUMBER_HASH_MAP_KEY));
                isFound = true;
                break;
            } else {
                driver.performKeyAction(Keys.F3);
            }
            driver.waitForPageToLoad();
        }
        if (!isFound) {
            Assert.fail("Failed to find Hybris order");
        }
        LOGGER.info("searchForhybrisOrderRelatedTransaction completed");
    }

    /**
     * Method to enter start and end dates in posdm
     *
     * @param startKey - Hash map key for start time stamp
     * @param endKey   - Hash map key for end time stamp
     */
    public void enterStartAndEndDateInPosDm(String startKey, String endKey) {
        LOGGER.info("enterStartAndEndDateInPosDm started");
        commonActions.sendKeysWithLabelName(START_OF_TRANSACTION, driver.scenarioData.getData(startKey));
        commonActions.sendKeys(endDatePosDm, driver.scenarioData.getData(endKey));
        LOGGER.info("enterStartAndEndDateInPosDm completed");
    }

    /**
     * Method to enter start and end dates in posdm
     *
     */
    public void enterStartAndEndDateInPosDm() {
        LOGGER.info("enterStartAndEndDateInPosDm started");
        commonActions.sendKeysWithLabelName(START_OF_TRANSACTION, Config.getTranStartTime());
        commonActions.sendKeys(endDatePosDm, Config.getTranEndTime());
        LOGGER.info("enterStartAndEndDateInPosDm completed");
    }

    /**
     * Asserts articles in posdm sales data with articles saved from order look up response
     *
     * @throws Throwable - Exception
     */
    public void assertArticlesAndAmountForSalesOrderIntegrationInPosDm() throws Throwable {
        LOGGER.info("assertArticlesAndAmountForSalesOrderIntegrationInPosDm started");
        List<WebElement> numberOfRows = webDriver.findElements(rowsInPosDmBy);
        int startIndex = 0;
        int endIndex = 0;
        String articleLineItem = null;
        boolean isArticleFound = false;
        HashMap<Integer,String> indexMap = new HashMap<>();
        for (int i = 0; i < numberOfRows.size(); i++) {
            if (numberOfRows.get(i).getText().contains(SALES_ITEM_DATA)) {
                startIndex = i + 1;
            } else if (numberOfRows.get(i).getText().contains(MEANS_OF_PAYMENT)) {
                endIndex = i;
            }
        }
        Assert.assertFalse("Transaction did not load", startIndex == 0 || endIndex == 0);
        numberOfRows.get(endIndex).getText();
        numberOfRows.get(startIndex).getText();
        for (Integer k = 0; k < endIndex; k++) {
            indexMap.put(k,PLACEHOLDER);
        }
        for (int i = 0; i < WebServices.totalNumberOfArticles; i++) {
            if (driver.scenarioData.
                    getData(Constants.CODE_ORDER_SERVICE_KEY + i).matches("[0-9]+")) {
                for (int j = startIndex; j < endIndex; j++) {
                    if (indexMap.get(j).equalsIgnoreCase(PLACEHOLDER)) {
                        if (numberOfRows.get(j).getText().contains(driver.scenarioData.
                                getData(Constants.CODE_ORDER_SERVICE_KEY + i))) {
                            isArticleFound = true;
                            indexMap.put(j, USED);
                            LOGGER.info("Found Article: " + driver.scenarioData.
                                    getData(Constants.CODE_ORDER_SERVICE_KEY + i) + " in: " +
                                    numberOfRows.get(j).getText());
                            articleLineItem = numberOfRows.get(j).getText();
                            if (!WebServices.isPromo) {
                                if (!WebServices.isFet) {
                                    Assert.assertTrue(articleLineItem.replace(",", "") + "!=" + driver.
                                            scenarioData.getData(Constants.VALUE_ORDER_SERVICE_KEY + i), articleLineItem.
                                            replace(",", "").
                                            contains(driver.scenarioData.getData(Constants.VALUE_ORDER_SERVICE_KEY + i)));
                                    LOGGER.info("Found Article: " + driver.scenarioData.
                                            getData(Constants.VALUE_ORDER_SERVICE_KEY + i) + " in: " +
                                            articleLineItem);
                                }
                            }
                            break;
                        }
                    }
                }
                Assert.assertTrue("FAIL: For Article: " + driver.scenarioData.
                        getData(Constants.CODE_ORDER_SERVICE_KEY + i + ", and Amount: " + driver.scenarioData.
                                getData(Constants.VALUE_ORDER_SERVICE_KEY + i) + ", Not found in: " + articleLineItem)
                        , isArticleFound);
            }
            isArticleFound = false;
        }
        LOGGER.info("assertArticlesAndAmountForSalesOrderIntegrationInPosDm completed");
    }

    /**
     * Enter the site number for DTD into the Store field
     */
    public void enterSiteNumberForDTD() {
        LOGGER.info("enterSiteNumberForDTD started");
        if (Config.getDataSet().equals(Constants.QA)) {
            commonActions.sendKeysWithLabelName(STORE, Constants.AZ_DC);
            Actions actions = new Actions(webDriver);
            actions.click(storeMultipleOptions).build().perform();
            commonActions.switchToMainWindow();
            commonActions.switchFrameContext(0);
            commonActions.sendKeys(storeLineTwo, Constants.TX_DC);
            driver.performKeyAction(Keys.F8);
            commonActions.switchToMainWindow();
            commonActions.switchFrameContext(1);
        } else if (Config.getDataSet().equals(Constants.STG)) {
            commonActions.sendKeysWithLabelName(STORE, Constants.GA_DC);
            Actions actions = new Actions(webDriver);
            actions.click(storeMultipleOptions).build().perform();
            commonActions.switchToMainWindow();
            commonActions.switchFrameContext(0);
            commonActions.sendKeys(storeLineTwo, Constants.OH_DC);
            driver.performKeyAction(Keys.F8);
            commonActions.switchToMainWindow();
            commonActions.switchFrameContext(1);
        } else {
            Assert.fail("please specify valid -DdataSet in VM options");
        }
        LOGGER.info("enterSiteNumberForDTD completed");
    }

    /**
     * Enter the Store Number from Json response into the Store field
     */
    public void enterStoreNumberFromJsonResponse() {
        LOGGER.info("enterStoreNumberFromJsonResponse started");
        commonActions.sendKeysWithLabelName(STORE, driver.scenarioData.getData(Constants.STORE_NUMBER_ORDER_SERVICE_KEY));
        LOGGER.info("enterStoreNumberFromJsonResponse completed");
    }

    /**
     * Enter the Store Number from Json response into the Store field
     */
    public void enterStoreNumberFromEnvironmentVariables() {
        LOGGER.info("enterStoreNumberFromJsonResponse started");
        commonActions.sendKeysWithLabelName(STORE, Config.getSiteNumber());
        LOGGER.info("enterStoreNumberFromJsonResponse completed");
    }

    /**
     * Verifies if a field value in order history is empty
     *
     * @param fieldNameKey
     */
    public void assertOrderHistoryFieldValueIsEmpty(String fieldNameKey) {
        LOGGER.info("assertOrderHistoryFieldValueIsEmpty Started");
        if (Config.getCustomerType().equalsIgnoreCase(Constants.REGISTERED) && !Config.getDataSet().equalsIgnoreCase(Constants.STG))
            Assert.assertTrue("FAIL: Field Value for " + fieldNameKey + " is NULL", !driver.scenarioData.getData(fieldNameKey).isEmpty());
        LOGGER.info("assertOrderHistoryFieldValueIsEmpty Completed");
    }

    /**
     * Selects Variant
     *
     * @param variantName - Variant name
     */
    public void selectVariant(String variantName) {
        LOGGER.info("selectVariant started");
        commonActions.selectElementWithTitle(GET_VARIANT);
        commonActions.switchToMainWindow();
        commonActions.switchFrameContext(0);
        commonActions.returnInputFieldWithLabel(VARIANT).clear();
        driver.waitForMilliseconds();
        driver.performKeyAction(Keys.F8);
        commonActions.switchToMainWindow();
        commonActions.switchFrameContext(0);
        commonActions.clickOnElementWithPartialValueAttribute(variantName);
        driver.waitForMilliseconds();
        commonActions.selectElementWithTitle(CHOOSE);
        commonActions.switchToMainWindow();
        LOGGER.info("selectVariant completed");
    }

    /**
     * Expands first node in the tree
     */
    public void expandFirstStoreNode() {
        LOGGER.info("expandFirstStoreNode started");
        WebElement storeExpandNode = webDriver.findElement(storeExpandNodeBy);
        if (storeExpandNode.getAttribute(Constants.TITLE).equalsIgnoreCase(EXPAND_NODE)) {
            driver.jsClick(storeExpandNode);
        }
        LOGGER.info("Checked for first store node");
        LOGGER.info("expandFirstStoreNode completed");
    }

    /**
     * Determines the column number of a particular column in balancing
     *
     * @param columnName - Name of the column
     * @return - index
     */
    private int determineColumnNumber(String columnName) {
        LOGGER.info("determineColumnNumber started");
        int columnNumber = 0;
        List<WebElement> columnElements = webDriver.findElements(By.xpath("//*[contains(text(),'Balancing')]" +
                "/following::tr[@vpm='mrss-hdr']/child::td[2]//following::th"));
        for (int i = 0; i < columnElements.size(); i++) {
            if (columnElements.get(i).getText().toLowerCase().replace("\n", "").
                    equalsIgnoreCase(columnName.toLowerCase() + " ")) {
                columnNumber = i + 1;
                break;
            }
        }
        if (columnNumber == 0) {
            Assert.fail("did not capture");
        }
        LOGGER.info("determineColumnNumber completed");
        return columnNumber;
    }

    /**
     * Waits for the content under balancing to load
     *
     * @param transactionType - Transaction Type
     */
    private void waitForTableHeaderPerTransaction(String transactionType) {
        LOGGER.info("waitForTableHeaderPerTransaction started");
        driver.waitForPageToLoad();
        if (transactionType.equalsIgnoreCase(T_1603) || transactionType.equalsIgnoreCase(T_1605)) {
            driver.waitForElementVisible(webDriver.findElement(returnElementWithPartialText("Tender")));
        } else if (transactionType.equalsIgnoreCase(T_1602)) {
            driver.waitForElementVisible(webDriver.findElement(returnElementWithPartialText("Taxes")));
        } else if (transactionType.equalsIgnoreCase(T_1601)) {
            driver.waitForElementVisible(webDriver.findElement(returnElementWithPartialText("Sales Turnover")));
        }
        LOGGER.info("waitForTableHeaderPerTransaction completed");
    }

    /**
     * Method will check total amount and total pos amount for each line and checks if there is a mismatch.
     * Stores the values if there is a mismatch.
     *
     * @param transactionType - Transaction type
     */
    private void compareAmountsAndSaveCorrespondingValues(String transactionType) {
        LOGGER.info("compareAmountsAndSaveCorrespondingValues started");
        waitForTableHeaderPerTransaction(transactionType);
        scrollTillElementIsVisible(TOTAL_SALES_AMT, BAlANCING_SCROLL_BUTTON);
        List<WebElement> tenderElementsStatus = webDriver.findElements(tenderElementStatusBy);
        for (int i = 1; i < tenderElementsStatus.size(); i++) {
            String lineElement = "//*[contains(text(),'Balancing')]/following::tr[@vpm='mrss-cont']/child:" +
                    ":td[2]//child::tr[@iidx][@rr][" + (i + 1) + "]";
            String totalPos = lineElement + "//child::td[" + determineColumnNumber(TOTAL_POS) + "]";
            String totalSalesAmt = lineElement + "//child::td[" + determineColumnNumber(TOTAL_SALES_AMT) + "]";
            String totalPosAmount = webDriver.findElement(By.xpath(totalPos)).getText();
            String totalSalesAmount = webDriver.findElement(By.xpath(totalSalesAmt)).getText();
            String tenderTypeXpath = "//*[contains(text(),'Balancing')]/following::tr[@vpm='mrss-cont']/child:" +
                    ":td[1]//child::tr[@iidx][@rr][" + (i + 1) + "]";
            String tenderTypName = webDriver.findElement(By.xpath(tenderTypeXpath)).getText();
            driver.scenarioData.setData(tenderTypName.substring(tenderTypName.length() - 5).replace(")",
                    "") + TOTAL_POS, totalPosAmount);
            LOGGER.info("Total POS:" + totalPosAmount);
            LOGGER.info("Total Sales:" + totalSalesAmount);
            LOGGER.info("TenderName:" + tenderTypName);

            driver.scenarioData.setData(tenderTypName.substring(tenderTypName.length() - 5).replace(")",
                    "") + TOTAL_SALES_AMT, totalSalesAmount);
            driver.scenarioData.setData(tenderTypName.substring(tenderTypName.length() - 5).replace(")",
                    ""), MATCH);
            if (!totalPosAmount.equals(totalSalesAmount)) {
                driver.scenarioData.setData(tenderTypName.substring(tenderTypName.length() - 5).replace(")",
                        ""), MIS_MATCH);
            }
        }
        LOGGER.info("compareAmountsAndSaveCorrespondingValues completed");
    }

    /**
     * returns element with partial text
     *
     * @param text - partial text
     */
    private By returnElementWithPartialText(String text) {
        LOGGER.info("returnElementWithPartialText started and completed");
        return By.xpath("//*[contains(text(),'" + text + "')]");
    }

    /**
     * Method will check total amount and total pos amount for each line and checks if there is a mismatch.
     * Stores the values if there is a mismatch.
     *
     * @param transactionType - Transaction type
     * @throws Throwable
     */
    private void compareAmountsAndSaveCorrespondingValues1601(String transactionType) throws Throwable {
        LOGGER.info("compareAmountsAndSaveCorrespondingValues1601 started");
        waitForTableHeaderPerTransaction(transactionType);
        scrollTillElementIsVisible(TOTAL_SALES_AMT, BAlANCING_SCROLL_BUTTON);
        List<WebElement> tenderElementsStatus = webDriver.findElements(tenderElementStatusBy);
        for (int i = 1; i < tenderElementsStatus.size(); i++) {
            String lineElement = "//*[contains(text(),'Balancing')]/following::tr[@vpm='mrss-cont']/child:" +
                    ":td[2]//child::tr[@iidx][@rr][" + (i + 1) + "]";
            String totalPos = lineElement + "//child::td[" + determineColumnNumber(TOTAL_POS) + "]";
            String totalSalesAmt = lineElement + "//child::td[" + determineColumnNumber(TOTAL_SALES_AMT) + "]";
            String totalPosAmount = webDriver.findElement(By.xpath(totalPos)).getText();
            String totalSalesAmount = webDriver.findElement(By.xpath(totalSalesAmt)).getText();
            String tenderTypeXpath = "//*[contains(text(),'Balancing')]/following::tr[@vpm='mrss-cont']/child:" +
                    ":td[1]//child::tr[@iidx][@rr][" + (i + 1) + "]";
            String tenderTypName = webDriver.findElement(By.xpath(tenderTypeXpath)).getText();
            driver.scenarioData.setData(returnSalesItemCode(tenderTypName) + TOTAL_POS, totalPosAmount);
            LOGGER.info("Total POS:" + totalPosAmount);
            LOGGER.info("Total Sales:" + totalSalesAmount);
            LOGGER.info("TenderName:" + tenderTypName);

            driver.scenarioData.setData(returnSalesItemCode(tenderTypName) + TOTAL_SALES_AMT, totalSalesAmount);
            driver.scenarioData.setData(returnSalesItemCode(tenderTypName), MATCH);
            if (!totalPosAmount.equals(totalSalesAmount)) {
                driver.scenarioData.setData(returnSalesItemCode(tenderTypName), MIS_MATCH);
            }
        }
        LOGGER.info("compareAmountsAndSaveCorrespondingValues1601 completed");
    }

    /**
     * Method will check total records in posdm and total pos records for each line and checks if there is a mismatch.
     * Stores the values if there is a mismatch.
     *
     * @param transactionType - Transaction type
     * @throws Throwable
     */
    private void compareTotalRecordsAndSaveCorrespondingValues(String transactionType) {
        LOGGER.info("compareTotalRecordsAndSaveCorrespondingValues started");
        waitForTableHeaderPerTransaction(transactionType);
        scrollTillElementIsVisible(START, BAlANCING_SCROLL_BUTTON);
        List<WebElement> tenderElementsStatus = webDriver.findElements(tenderElementStatusBy);
        numberOfTaxesLineItems = tenderElementsStatus.size() - 1;
        for (int i = 1; i < tenderElementsStatus.size(); i++) {
            String lineElement = "//*[contains(text(),'Balancing')]/following::tr[@vpm='mrss-cont']/child::td[2]" +
                    "//child::tr[@iidx][@rr][" + (i + 1) + "]";
            String totalStoreSentTransactions = lineElement + "//child::td[" + determineColumnNumber(TOT) + "]";
            String totalTransactions = lineElement + "//child::td[" + determineColumnNumber(TOTAL) + "]";
            String totalStoreTransactions = webDriver.findElement(By.xpath(totalStoreSentTransactions)).getText();
            String totalSystemTransactions = webDriver.findElement(By.xpath(totalTransactions)).getText();
            String tenderTypeXpath = "//*[contains(text(),'Balancing')]/following::tr[@vpm='mrss-cont']/child::td[1]" +
                    "//child::tr[@iidx][" + (i + 1) + "]";
            String tenderTypName = webDriver.findElement(By.xpath(tenderTypeXpath)).getText();
            driver.scenarioData.setData(tenderTypName.substring(tenderTypName.length() - 5).replace(")",
                    "") + TOT, totalStoreTransactions);
            LOGGER.info("Total POS sent transactions:" + totalStoreTransactions);
            LOGGER.info("Total Transactions in system:" + totalSystemTransactions);
            LOGGER.info("TaxesName:" + tenderTypName);
            driver.scenarioData.setData(tenderTypName.substring(tenderTypName.length() - 5).replace(")",
                    "") + TOTAL, totalSystemTransactions);
            driver.scenarioData.setData(tenderTypName.substring(tenderTypName.length() - 5).replace(")",
                    ""), MATCH);
            if (!totalStoreTransactions.equals(totalSystemTransactions)) {
                driver.scenarioData.setData(tenderTypName.substring(tenderTypName.length() - 5).replace(
                        ")", ""), MIS_MATCH);
            }
        }
        scrollTillElementIsVisible(TRANSACTION_CURRENCY, BAlANCING_SCROLL_BUTTON_PREV);
        LOGGER.info("compareTotalRecordsAndSaveCorrespondingValues completed");
    }

    /**
     * Method will check total amount and total pos amount for each line and checks if there is a mismatch.
     * Stores the values if there is a mismatch. This is for 1601
     *
     * @param transactionType - Transaction type
     * @throws Throwable
     */
    private void compareTotalRecordsAndSaveCorrespondingValuesForSalesItemTypes1601(String transactionType)
            throws Throwable {
        LOGGER.info("compareTotalRecordsAndSaveCorrespondingValuesForSalesItemTypes1601 started");
        waitForTableHeaderPerTransaction(transactionType);
        scrollTillElementIsVisible(START, BAlANCING_SCROLL_BUTTON);
        List<WebElement> tenderElementsStatus = webDriver.findElements(tenderElementStatusBy);
        numberOfTaxesLineItems = tenderElementsStatus.size() - 1;
        for (int i = 1; i < tenderElementsStatus.size(); i++) {
            String lineElement = "//*[contains(text(),'Balancing')]/following::tr[@vpm='mrss-cont']/child:" +
                    ":td[2]//child::tr[@iidx][" + (i + 1) + "]";
            String totalStoreSentTransactions = lineElement + "//child::td[" + determineColumnNumber(TOT) + "]";
            String totalTransactions = lineElement + "//child::td[" + determineColumnNumber(TOTAL) + "]";
            String totalStoreTransactions = webDriver.findElement(By.xpath(totalStoreSentTransactions)).getText();
            String totalSystemTransactions = webDriver.findElement(By.xpath(totalTransactions)).getText();
            String tenderTypeXpath = "//*[contains(text(),'Balancing')]/following::tr[@vpm='mrss-cont']/child:" +
                    ":td[1]//child::tr[@iidx][" + (i + 1) + "]";
            String tenderTypName = webDriver.findElement(By.xpath(tenderTypeXpath)).getText();
            driver.scenarioData.setData(returnSalesItemCode(tenderTypName) + TOT, totalStoreTransactions);
            LOGGER.info("Total POS sent transactions:" + totalStoreTransactions);
            LOGGER.info("Total Transactions in system:" + totalSystemTransactions);
            LOGGER.info("TaxesName:" + tenderTypName);

            driver.scenarioData.setData(returnSalesItemCode(tenderTypName) + TOTAL, totalSystemTransactions);
            driver.scenarioData.setData(returnSalesItemCode(tenderTypName), MATCH);
            if (!totalStoreTransactions.equals(totalSystemTransactions)) {
                driver.scenarioData.setData(returnSalesItemCode(tenderTypName), MIS_MATCH);
            }
        }
        scrollTillElementIsVisible(TRANSACTION_CURRENCY, BAlANCING_SCROLL_BUTTON_PREV);
        LOGGER.info("compareTotalRecordsAndSaveCorrespondingValuesForSalesItemTypes1601 completed");
    }

    /**
     * Method will complete fix process for 1603
     */
    private void fix1603Errors() {
        LOGGER.info("fix1603Errors started");
        commonActions.selectElementWithTitle(OVERVIEW);
        commonActions.doubleClickOnElement(webDriver.findElement(totalRecordColumnsBy));
        try {
            commonActions.waitForElementWithPartialText(TOTAL_MNS_OF_PAYMENT, 30);
        } catch (AssertionError ae) {
            commonActions.doubleClickOnElement(webDriver.findElement(totalRecordColumnsBy));
            commonActions.waitForElementWithPartialText(TOTAL_MNS_OF_PAYMENT, 30);
        }
        Actions actions = new Actions(webDriver);
        commonActions.clickOnPartialText(TOTAL_MNS_OF_PAYMENT);
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        actions.keyDown(Keys.CONTROL)
                .sendKeys(Keys.F1)
                .perform();
        driver.waitForMilliseconds(Constants.EIGHT_THOUSAND);
        List<WebElement> numberOfRows = webDriver.findElements(rowsInPosDmBy);
        int startIndex = 0;
        int endIndex = 0;
        for (int i = 0; i < numberOfRows.size(); i++) {
            if (numberOfRows.get(i).getText().contains(TOTAL_MNS_OF_PAYMENT)) {
                startIndex = i + 1;
            } else if (numberOfRows.get(i).getText().contains(CUSTOMER_ENHANCEMENTS)) {
                endIndex = i;
            }
        }
        Assert.assertFalse("Transaction did not load", startIndex == 0 || endIndex == 0);
        LOGGER.info("End index: " + numberOfRows.get(endIndex).getText());
        LOGGER.info("Start index: " + numberOfRows.get(startIndex).getText());

        for (int i = startIndex; i < endIndex; i++) {
            if (driver.scenarioData.getData(numberOfRows.get(i).getText().substring(0, 4).replace(" ",
                    "")).equalsIgnoreCase(MIS_MATCH)) {

                sendKeysInWorkBench(returnElementToInputInWorkBench(numberOfRows.get(i).getText().substring(0, 4)
                                .replace(" ", "")),
                        driver.scenarioData.getData(numberOfRows.get(i).getText().substring(0, 4).replace
                                (" ", "") + TOTAL_SALES_AMT));
                driver.waitForPageToLoad();
                numberOfRows = webDriver.findElements(rowsInPosDmBy);

            }
        }
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        actions.keyDown(Keys.CONTROL)
                .sendKeys(Keys.chord("s"))
                .perform();
        commonActions.waitForElementWithText(DATA_SAVED, 30);
        commonActions.clickOnExactText(BACK);
        LOGGER.info("fix1603Errors completed");
    }

    /**
     * Method will complete error fix process for 1602
     */
    private void fix1602Errors() {
        LOGGER.info("fix1602Errors started");
        commonActions.selectElementWithTitle(OVERVIEW);
        commonActions.doubleClickOnElement(webDriver.findElement(totalRecordColumnsBy));
        try {
            commonActions.waitForElementWithPartialText(TOTAL_TAX_AMOUNT, 30);
        } catch (AssertionError ae) {
            commonActions.selectElementWithTitle(OVERVIEW);
            commonActions.doubleClickOnElement(webDriver.findElement(totalRecordColumnsBy));
            commonActions.waitForElementWithPartialText(TOTAL_TAX_AMOUNT, 30);
        }
        Actions actions = new Actions(webDriver);
        commonActions.clickOnPartialText(TOTAL_TAX_AMOUNT);
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        actions.keyDown(Keys.CONTROL)
                .sendKeys(Keys.F1)
                .perform();
        driver.waitForMilliseconds(Constants.EIGHT_THOUSAND);
        List<WebElement> numberOfRows = webDriver.findElements(rowsInPosDmBy);
        int startIndex = 0;
        for (int i = 0; i < numberOfRows.size(); i++) {
            if (numberOfRows.get(i).getText().contains(TOTAL_TAX_AMOUNT)) {
                startIndex = i + 1;
            }
        }
        int endIndex = startIndex + numberOfTaxesLineItems;
        Assert.assertFalse("Transaction did not load", startIndex == 0 || endIndex == 0);
        LOGGER.info("End index: " + numberOfRows.get(endIndex).getText());
        LOGGER.info("Start index: " + numberOfRows.get(startIndex).getText());
        for (int i = startIndex; i < endIndex; i++) {
            if (driver.scenarioData.getData(numberOfRows.get(i).getText().substring(0, 4).replace(" ",
                    "")).equalsIgnoreCase(MIS_MATCH)) {
                sendKeysInWorkBench(returnElementToInputInWorkBench(numberOfRows.get(i).getText().substring(0, 4)
                                .replace(" ", "")),
                        driver.scenarioData.getData(numberOfRows.get(i).getText().substring(0, 4).replace
                                (" ", "") + TOTAL_SALES_AMT));
                driver.waitForPageToLoad();
                sendKeysInWorkBench(returnElementToInputInWorkBenchForNumberOfTaxItems(numberOfRows.get(i).getText
                                ().substring(0, 4).replace(" ", "")),
                        driver.scenarioData.getData(numberOfRows.get(i).getText().substring(0, 4).replace
                                (" ", "") + TOTAL));
                driver.waitForPageToLoad();
                numberOfRows = webDriver.findElements(rowsInPosDmBy);

            }
        }
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        actions.keyDown(Keys.CONTROL)
                .sendKeys(Keys.chord("s"))
                .perform();
        commonActions.waitForElementWithPartialText(DATA_SAVED, 30);
        commonActions.clickOnExactText(BACK);
        LOGGER.info("fix1602Errors completed");

    }

    /**
     * Method will complete error fix process for 1601
     */
    private void fix1601Errors() {
        LOGGER.info("fix1601Errors started");
        commonActions.selectElementWithTitle(OVERVIEW);
        commonActions.doubleClickOnElement(webDriver.findElement(totalRecordColumnsBy));
        try {
            commonActions.waitForElementWithPartialText(SALES_TOTAL, 30);
        } catch (AssertionError ae) {
            commonActions.selectElementWithTitle(OVERVIEW);
            commonActions.doubleClickOnElement(webDriver.findElement(totalRecordColumnsBy));
            commonActions.waitForElementWithText(SALES_TOTAL, 30);
        }
        Actions actions = new Actions(webDriver);
        commonActions.clickOnPartialText(SALES_TOTAL);
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        actions.keyDown(Keys.CONTROL)
                .sendKeys(Keys.F1)
                .perform();
        driver.waitForMilliseconds(Constants.EIGHT_THOUSAND);
        List<WebElement> numberOfRows = webDriver.findElements(rowsInPosDmBy);
        int startIndex = 0;
        int endIndex = 0;
        for (int i = 0; i < numberOfRows.size(); i++) {
            if (numberOfRows.get(i).getText().contains(SALES_TOTAL)) {
                startIndex = i + 1;
            } else if (numberOfRows.get(i).getText().contains(CUSTOMER_ENHANCEMENTS)) {
                endIndex = i;
            }
        }
        Assert.assertFalse("Transaction did not load", startIndex == 0 || endIndex == 0);
        LOGGER.info("End index: " + numberOfRows.get(endIndex).getText());
        LOGGER.info("Start index: " + numberOfRows.get(startIndex).getText());
        for (int i = startIndex; i < endIndex; i++) {
            if (driver.scenarioData.getData(numberOfRows.get(i).getText().substring(0, 4).replace(" ",
                    "")).equalsIgnoreCase(MIS_MATCH)) {
                sendKeysInWorkBench(returnElementToInputInWorkBench(numberOfRows.get(i).getText().substring(0, 4)
                                .replace(" ", "")),
                        driver.scenarioData.getData(numberOfRows.get(i).getText().substring(0, 4).replace
                                (" ", "") + TOTAL_SALES_AMT));
                driver.waitForPageToLoad();
                sendKeysInWorkBench(returnElementToInputInWorkBenchForNumberOfTaxItems(numberOfRows.get(i).
                                getText().substring(0, 4).replace(" ", "")),
                        driver.scenarioData.getData(numberOfRows.get(i).getText().substring(0, 4).replace
                                (" ", "") + TOTAL));
                driver.waitForPageToLoad();
                numberOfRows = webDriver.findElements(rowsInPosDmBy);
            }
        }
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        actions.keyDown(Keys.CONTROL)
                .sendKeys(Keys.chord("s"))
                .perform();
        commonActions.waitForElementWithText(DATA_SAVED, 30);
        commonActions.clickOnExactText(BACK);
        LOGGER.info("fix1601Errors completed");
    }

    /**
     * Method will complete error fix process for 1009
     */
    private void fix1009Errors() {
        LOGGER.info("fix1009Errors started");
        commonActions.clickOnExactText(OVERVIEW);
        commonActions.doubleClickOnElement(webDriver.findElement(totalRecordColumnsBy));
        try {
            commonActions.waitForElementWithText(SALES_ITEM_DATA, 30);
        } catch (AssertionError ae) {
            commonActions.clickOnExactText(OVERVIEW);
            commonActions.doubleClickOnElement(webDriver.findElement(totalRecordColumnsBy));
            commonActions.waitForElementWithText(SALES_ITEM_DATA, 30);
        }
        scrollTillElementIsVisible(SALES_TOTAL_ROW, STORE_TOTAL_SCROLL);
        String totalAmount = returnElement(SALES_TOTAL_VALUE).getText();
        scrollTillElementIsVisible(STORE_ROW, STORE_TOTAL_SCROLL_PREV);
        Actions actions = new Actions(webDriver);
        commonActions.clickOnPartialText(MEANS_OF_PAYMENT);
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        actions.keyDown(Keys.CONTROL)
                .sendKeys(Keys.F1)
                .perform();
        driver.waitForMilliseconds(Constants.EIGHT_THOUSAND);
        sendKeysInWorkBench(returnElementToInputInWorkBench(ZTOT), totalAmount);
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        actions.keyDown(Keys.CONTROL)
                .sendKeys(Keys.chord("s"))
                .perform();
        commonActions.waitForElementWithText(DATA_SAVED, 30);
        commonActions.clickOnExactText(BACK);
        LOGGER.info("fix1009Errors completed");
    }

    /**
     * Method will return sales item type code for a particular sales item type
     *
     * @param salesItemType - Sales item type string
     * @return - Sales item type code
     * @throws Throwable
     */
    private String returnSalesItemCode(String salesItemType) throws Throwable {
        LOGGER.info("returnSalesItemCode started");
        String salesItemCode = null;
        File file = new File(Constants.SAP_POSDM_PROD_LOCATION);
        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook xssfworkbook = new XSSFWorkbook(inputStream);
        XSSFSheet xssfsheet = xssfworkbook.getSheet(SHEET_NAME_1601);
        int totalRows = xssfsheet.getLastRowNum();
        for (int i = 0; i <= totalRows; i++) {
            Row row = xssfsheet.getRow(i);
            String cellValue = row.getCell(1).getStringCellValue();
            if (cellValue.toLowerCase().replace(" ", "").contains(salesItemType.toLowerCase().replace(" ", ""))) {
                salesItemCode = row.getCell(0).getStringCellValue();
                numberOfSalesItemTypes++;
            }

        }
        if (numberOfSalesItemTypes != 1) {
            Assert.fail("Found more then 1 record or no record for same sales item type");
        }
        numberOfSalesItemTypes = 0;
        LOGGER.info("returnSalesItemCode completed");
        return salesItemCode;
    }

    /**
     * Selects the transaction and executes the tasks
     *
     * @param transactionType - Transaction type
     */
    private void executeTask(String transactionType) {
        LOGGER.info("executeTask started");
        if (!driver.isElementDisplayed(returnElementWithPartialText(INBOUND_MONITOR), 30)) {
            commonActions.clickOnExactText(BACK);
            commonActions.waitForElementWithText(INBOUND_MONITOR);
        }
        if (!transactionType.equalsIgnoreCase(T_1009)) {
            commonActions.selectElementWithTitle(TABLE_SELECTION_MENU);
            commonActions.clickOnExactText(SELECT_ALL);
        } else {
            List<WebElement> transactionLines = webDriver.findElements(lineItemsBy);
            commonActions.click(transactionLines.get(0));
        }
        commonActions.selectElementWithTitle(PROCESS_TASKS_ONLINE);
        commonActions.switchToMainWindow();
        commonActions.switchFrameContext(0);
        commonActions.selectElementWithTitle(EXECUTE_PROCESSING_BUTTON_TEXT);
        commonActions.selectElementWithTitle("Close Dialog ");
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        commonActions.switchToMainWindow();
        commonActions.switchFrameContext(2);
        driver.waitForPageToLoad();
        if (driver.isElementDisplayed(returnElementWithPartialText("Naviation area is no longer valid"), 30)) {
            LOGGER.info("Naviation or No data message found");
        }
        commonActions.switchToMainWindow();
        commonActions.switchFrameContext(1);
        LOGGER.info("Task Execution Complete");
        LOGGER.info("executeTask completed");
    }

    /**
     * Returns web element to edit values
     */
    private WebElement returnElementToInputInWorkBench(String fieldName) {
        LOGGER.info("returnElementToInputInWorkBench started and completed");
        return webDriver.findElement(By.xpath("//*[contains(@value,'" + fieldName + "')]/following::span[1]"));
    }

    /**
     * Returns web element to edit values
     */
    private WebElement returnElementToInputInWorkBenchForNumberOfTaxItems(String fieldName) {
        LOGGER.info("returnElementToInputInWorkBenchForNumberOfTaxItems started and completed");
        return webDriver.findElement(By.xpath("//*[contains(@value,'" + fieldName + "')]/following::span[2]"));
    }

    /**
     * Sets the input string value to system clip board
     *
     * @param content - input content
     */
    private void setClipBoardContent(String content) {
        LOGGER.info("setClipBoardContent started");
        StringSelection selection = new StringSelection(content);
        Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
        clipboard.setContents(selection, selection);
        LOGGER.info("setClipBoardContent completed");
    }

    /**
     * Sends keys into table in posdm using copy paste function
     *
     * @param element - Web Element to focus
     * @param text    - Text to paste
     */
    private void sendKeysInWorkBench(WebElement element, String text) {
        LOGGER.info("sendKeysInWorkBench started");
        Actions actions = new Actions(webDriver);
        commonActions.click(element);
        driver.waitForMilliseconds();
        setClipBoardContent(text);
        actions.keyDown(Keys.CONTROL)
                .sendKeys(Keys.chord("v"))
                .perform();
        driver.performKeyAction(Keys.TAB);
        LOGGER.info("sendKeysInWorkBench completed");
    }

    /**
     * Method will call multiple methods to finish error fix process for 1603
     *
     * @param transactionType - Type Of transaction type to be dealt with
     */
    public void checkAnalyzeAndCorrectErrorsFor1603(String transactionType) {
        LOGGER.info("checkAnalyzeAndCorrectErrorsFor1603 started");
        while (!webDriver.getPageSource().contains(NO_DATA_EXISTS)) {
            expandDateColumnNoDate();
            expandFirstStoreNode();
            try {
                commonActions.doubleClickOnExactText(TOTALS_RECORDS);
            } catch (NoSuchElementException nse) {
                expandFirstStoreNode();
                commonActions.doubleClickOnExactText(TOTALS_RECORDS);
                LOGGER.info("Got Here: No Such Element for total records");
            }
            try {
                commonActions.selectElementWithTitle(TABLE_SELECTION_MENU);
            } catch (NoSuchElementException nse) {
                expandFirstStoreNode();
                commonActions.doubleClickOnExactText(TOTALS_RECORDS);
                commonActions.selectElementWithTitle(TABLE_SELECTION_MENU);
                LOGGER.info("Got Here: No Such Element for: " + TABLE_SELECTION_MENU);
            }
            commonActions.clickOnExactText(SELECT_ALL);
            commonActions.clickOnExactText(BALANCING);
            compareAmountsAndSaveCorrespondingValues(transactionType);
            fix1603Errors();
            executeTask(T_1603);
        }
        LOGGER.info("checkAnalyzeAndCorrectErrorsFor1603 completed");
    }

    /**
     * Method will call multiple methods to finish error fix process for 1602
     */
    public void checkAnalyzeAndCorrectErrorsFor1602() {
        LOGGER.info("checkAnalyzeAndCorrectErrorsFor1602 started");
        while (!webDriver.getPageSource().contains(NO_DATA_EXISTS)) {
            expandDateColumnNoDate();
            expandFirstStoreNode();
            try {
                commonActions.doubleClickOnExactText(TOTALS_RECORDS);
            } catch (NoSuchElementException nse) {
                expandFirstStoreNode();
                commonActions.doubleClickOnExactText(TOTALS_RECORDS);
                LOGGER.info("Got Here: No Such Element for total records");
            }
            try {
                commonActions.selectElementWithTitle(TABLE_SELECTION_MENU);
            } catch (NoSuchElementException nse) {
                expandFirstStoreNode();
                commonActions.doubleClickOnExactText(TOTALS_RECORDS);
                commonActions.selectElementWithTitle(TABLE_SELECTION_MENU);
                LOGGER.info("Got Here: No Such Element for t s m");
            }
            commonActions.clickOnExactText(SELECT_ALL);
            commonActions.clickOnExactText(BALANCING);
            compareAmountsAndSaveCorrespondingValues(T_1602);
            compareTotalRecordsAndSaveCorrespondingValues(T_1602);
            fix1602Errors();
            executeTask(T_1602);
        }
        LOGGER.info("checkAnalyzeAndCorrectErrorsFor1602 completed");
    }

    /**
     * Method will call multiple methods to finish error fix process for 1601
     */
    public void checkAnalyzeAndCorrectErrorsFor1601() throws Throwable {
        LOGGER.info("checkAnalyzeAndCorrectErrorsFor1601 started");
        while (!webDriver.getPageSource().contains(NO_DATA_EXISTS)) {
            expandDateColumnNoDate();
            expandFirstStoreNode();
            try {
                commonActions.doubleClickOnExactText(TOTALS_RECORDS);
            } catch (NoSuchElementException nse) {
                expandFirstStoreNode();
                commonActions.doubleClickOnExactText(TOTALS_RECORDS);
                LOGGER.info("Got Here: No Such Element for total records");
            }
            try {
                commonActions.selectElementWithTitle(TABLE_SELECTION_MENU);
            } catch (NoSuchElementException nse) {
                expandFirstStoreNode();
                commonActions.doubleClickOnExactText(TOTALS_RECORDS);
                commonActions.selectElementWithTitle(TABLE_SELECTION_MENU);
                LOGGER.info("Got Here: No Such Element for t s m");
            }
            commonActions.clickOnExactText(SELECT_ALL);
            commonActions.clickOnExactText(BALANCING);
            compareAmountsAndSaveCorrespondingValues1601(T_1601);
            compareTotalRecordsAndSaveCorrespondingValuesForSalesItemTypes1601(T_1601);
            fix1601Errors();
            executeTask(T_1601);
        }
        LOGGER.info("checkAnalyzeAndCorrectErrorsFor1601 completed");
    }

    /**
     * Method will call multiple methods to finish error fix process for 1009
     */
    public void checkAnalyzeAndCorrectErrorsFor1009() throws Throwable {
        LOGGER.info("checkAnalyzeAndCorrectErrorsFor1009 started");
        while (!webDriver.getPageSource().contains(NO_DATA_EXISTS)) {
            expandDateColumnNoDate();
            expandFirstStoreNode();
            try {
                commonActions.doubleClickOnExactText(SALES_MOVEMENT);
            } catch (NoSuchElementException nse) {
                LOGGER.info("Got Here: No Such Element for Sales Movement");
                expandFirstStoreNode();
                commonActions.doubleClickOnExactText(SALES_MOVEMENT);
            }
            fix1009Errors();
            executeTask(T_1009);
        }
        LOGGER.info("checkAnalyzeAndCorrectErrorsFor1009 completed");
    }

    /**
     * Method changes status of tasks to be ready and hence enabling edits to be done
     */
    public void changeStatusToEnableEdit() throws Throwable {
        LOGGER.info("changeStatusToEnableEdit started.");
        commonActions.doubleClickOnElement(returnElement(ERROR_BUTTON));
        commonActions.switchToMainWindow();
        commonActions.switchFrameContext(2);
        commonActions.doubleClickOnElement(returnElement(CHANGE_TASK));
        LOGGER.info("changeStatusToEnableEdit completed.");
    }

}