package sap.pages;

import common.Constants;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.Keys;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import utilities.Driver;
import webservices.pages.WebServices;

import java.io.File;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

public class PosDt {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private IDocPage iDocPage;
    private WebServices webServices;
    private final Logger LOGGER = Logger.getLogger(PosDt.class.getName());


    public PosDt(Driver driver) {
        this.driver = driver;
        webDriver = driver.getDriver();
        commonActions = new CommonActions(driver);
        iDocPage = new IDocPage(driver);
        webServices = new WebServices(driver);
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(id = "M0:D:13::btn[8]")
    private static WebElement execute;

    @FindBy(id = "M0:U:3::0:1-title")
    private static WebElement header;

    @FindBy(xpath = "//*[text()='Transaction Number']/following::input[1]")
    private static WebElement transactionNumber;

    @FindBy(xpath = "//input[@id=\"M0:D:10::okcd\"]")
    private static WebElement tCode;

    @FindBy(xpath = "//img[@title=\"OK\"]")
    private static WebElement successfulTransaction;

    @FindBy(xpath = "//img[@action=\"close\"]")
    private static WebElement closeButton;

    @FindBy(xpath = "//span[contains(@id,',1#icp-r')]")
    private static WebElement transactionStatus;

    @FindBy(xpath = "//div[contains(@id,'_vscroll-Nxt')]")
    private static WebElement scrollBarNextButton;

    @FindBy(xpath = "//input[contains(@id,'1,7#if')]")
    private static WebElement totalAmountPosdt;

    @FindBy(xpath = "//input[contains(@tabindex,'-1') and contains(@id,'1,24#if')]")
    private static WebElement transactionId1;

    @FindBy(xpath = "//input[contains(@tabindex,'-1') and contains(@id,'2,24#if')]")
    private static WebElement transactionId2;

    @FindBy(xpath = "//span[text()='Type']/following::input[contains(@id,',9#if')][1]")
    private static WebElement transType;

    @FindBy(xpath = "//span[text()='Store']")
    private static WebElement store;

    private static final By webOrderNumberBy = By.xpath("//*[contains(text(),'Customer information')]//preceding::a[2]");

    private static final By firstLineItemExpandBy = By.xpath("//*[contains(text(),'Sales Item Data')]/following::*[@title='Expand Node'][1]");

    private static final By purchaseOrderNumberFieldBy = By.xpath("//*[contains(text(),'PURORDERNO')]");

    private static final By vtvVehicleTableFieldsBy = By.xpath("//table[contains(@id,'OmrtContainer')]//following::tr[contains(@vpm,'mrss-hdr')]//following::th//div//table");

    private static final By scrollBarNextButtonBy = By.xpath("//div[contains(@id,'_vscroll-Nxt')]");
    private static final By saleItemDataBy = By.xpath("//span[contains(text(),'Sales Item Data')]/following::span[text()]");
    private static final By goodsMovementDataBy = By.xpath("//*[contains(text(),'Goods movements')]/following::span[contains(@id,'text')]");
    private static final By taskStatusOverview = By.xpath("//img[@title=\"OK\"]");
    private static final By VTVFieldsBy = By.xpath("//div[contains(@vpm,'mrss-cont')]//following::div//following::div[contains(@class,'urST5ContentDiv')]//input");
    private static final String EXECUTE = "Execute";
    private static final String TCODE = "Tcode";
    private static final String HEADER = "Header";
    private static final String TRANSACTION_NUMBER = "Transaction Number";
    private static final String TRANSACTION_STATUS = "Transaction Status";
    private static final String CLOSE_BUTTON = "Close Button";
    public static final String TRANSACTION_TYPE = "Transaction Type";
    private static final String TOTAL_AMOUNT_KEY = "Total Amount";
    private static final String TRANSACTION_ID_1 = "Transaction Id 1";
    private static final String TRANSACTION_ID_2 = "Transaction Id 2";
    private static final String BACK = "Back";
    public static final String EXCEL_SHEET_LOCATION_VTV_DATA = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleDataGoodScan.xlsx";
    public static final String CREATE_AIRCHECK_ACTIVITY_EXISTING_CUSTOMER_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData1.xlsx";
    public static final String CREATE_AIRCHECK_ACTIVITY_NEW_CUSTOMER_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData2.xlsx";
    public static final String CREATE_SERVICE_ACTIVITY_PARTIAL_INFORMATION_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData3.xlsx";
    public static final String CUSTOMER_SEARCH_VIN_WITH_CUSTOMER_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData4.xlsx";
    public static final String CUSTOMER_SEARCH_VIN_WITH_NO_CUSTOMER_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData5.xlsx";
    public static final String CUSTOMER_SEARCH_LICENSE_PLATE_WITH_CUSTOMER_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData6.xlsx";
    public static final String CUSTOMER_SEARCH_LICENSE_PLATE_WITH_NO_CUSTOMER_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData7.xlsx";
    public static final String CUSTOMER_SEARCH_WITH_ALL_TRIM_ASSEMBLIES_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData8.xlsx";
    public static final String CUSTOMER_SEARCH_WITH_NO_TRIM_ASSEMBLIES_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData9.xlsx";
    public static final String CUSTOMER_SEARCH_WITH_TRIM_MANY_ASSEMBLIES_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData10.xlsx";
    public static final String VEHICLE_SEARCH_OE_OR_NON_OE_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData11.xlsx";
    public static final String VEHICLE_SEARCH_LOOKUP_YEAR_MAKE_MODEL_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData12.xlsx";
    public static final String VEHICLE_SEARCH_WITH_IMAGE_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData13.xlsx";
    public static final String VEHICLE_SEARCH_WITH_NO_IMAGE_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData14.xlsx";
    public static final String VEHICLE_SEARCH_TRIMS_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData15.xlsx";
    public static final String VEHICLE_SEARCH_STAGGERED_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData16.xlsx";
    public static final String VEHICLE_SEARCH_DUALLY_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData17.xlsx";
    public static final String CREATE_SERVICE_ACTIVITY_ADD_SERVICES_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData18.xlsx";
    public static final String CREATE_SERVICE_ACTIVITY_EXISTING_CUSTOMER_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData19.xlsx";
    public static final String CREATE_SERVICE_ACTIVITY_UPDATE_SERVICES_EXCEL = "L:\\DTC-IT-Quality Assurance-TESTING\\Automation Files\\SAP\\SAP SELENIUM DATA\\vtv\\VehicleData21.xlsx";
    public static final String CREATE_AIRCHECK_ACTIVITY_EXISTING_CUSTOMER = "create air check activity for existing customer";
    public static final String CREATE_AIRCHECK_ACTIVITY_NEW_CUSTOMER = "create air check activity for new customer";
    public static final String CREATE_SERVICE_ACTIVITY_PARTIAL_INFORMATION = "create service activity partial information";
    public static final String CUSTOMER_SEARCH_VIN_WITH_CUSTOMER = "customer search vin with customer";
    public static final String CUSTOMER_SEARCH_VIN_WITH_NO_CUSTOMER = "customer search vin with no customer";
    public static final String CUSTOMER_SEARCH_LICENSE_PLATE_WITH_CUSTOMER = "customer search license plate with customer";
    public static final String CUSTOMER_SEARCH_LICENSE_PLATE_WITH_NO_CUSTOMER = "customer search license plate with no customer";
    public static final String CUSTOMER_SEARCH_WITH_ALL_TRIM_ASSEMBLIES = "customer search with all trim assemblies";
    public static final String CUSTOMER_SEARCH_WITH_NO_TRIM_ASSEMBLIES = "customer search with no trim assemblies";
    public static final String CUSTOMER_SEARCH_WITH_TRIM_MANY_ASSEMBLIES = "customer search with trim many assemblies";
    public static final String VEHICLE_SEARCH_OE_OR_NON_OE = "vehicle search oe or non oe";
    public static final String VEHICLE_SEARCH_LOOKUP_YEAR_MAKE_MODEL = "vehicle search lookup by year,make and model";
    public static final String VEHICLE_SEARCH_WITH_IMAGE = "vehicle search with image";
    public static final String VEHICLE_SEARCH_WITH_NO_IMAGE = "vehicle search with no image";
    public static final String VEHICLE_SEARCH_TRIMS = "vehicle search trims";
    public static final String VEHICLE_SEARCH_STAGGERED = "vehicle search staggered";
    public static final String VEHICLE_SEARCH_DUALLY = "vehicle search dually";
    public static final String CREATE_SERVICE_ACTIVITY_ADD_SERVICES = "create service activity add service";
    public static final String CREATE_SERVICE_ACTIVITY_EXISTING_CUSTOMER = "create service activity existing customer";
    public static final String CREATE_SERVICE_ACTIVITY_NEW_CUSTOMER = "create service activity new customer";
    public static final String CREATE_SERVICE_ACTIVITY_UPDATE_SERVICES = "create service activity update service";
    private static final String ZVTV_VEHICLE = "ZVTV_VEHICLE";
    public static final String VIN = "vin";
    public static final String VTV_CUSTOMER_ID = "vtvcustomerid";
    public static final String VEHICLE_ID = "vehicleid";
    public static final String IS_STAGGERED = "IsStaggered";

    public static HashMap<String, String> vtvDataSheet = new HashMap<>();

    static {
        vtvDataSheet.put(CREATE_AIRCHECK_ACTIVITY_EXISTING_CUSTOMER, CREATE_AIRCHECK_ACTIVITY_EXISTING_CUSTOMER_EXCEL);
        vtvDataSheet.put(CREATE_AIRCHECK_ACTIVITY_NEW_CUSTOMER, CREATE_AIRCHECK_ACTIVITY_NEW_CUSTOMER_EXCEL);
        vtvDataSheet.put(CREATE_SERVICE_ACTIVITY_PARTIAL_INFORMATION, CREATE_SERVICE_ACTIVITY_PARTIAL_INFORMATION_EXCEL);
        vtvDataSheet.put(CUSTOMER_SEARCH_VIN_WITH_CUSTOMER, CUSTOMER_SEARCH_VIN_WITH_CUSTOMER_EXCEL);
        vtvDataSheet.put(CUSTOMER_SEARCH_VIN_WITH_NO_CUSTOMER, CUSTOMER_SEARCH_VIN_WITH_NO_CUSTOMER_EXCEL);
        vtvDataSheet.put(CUSTOMER_SEARCH_LICENSE_PLATE_WITH_CUSTOMER, CUSTOMER_SEARCH_LICENSE_PLATE_WITH_CUSTOMER_EXCEL);
        vtvDataSheet.put(CUSTOMER_SEARCH_LICENSE_PLATE_WITH_NO_CUSTOMER, CUSTOMER_SEARCH_LICENSE_PLATE_WITH_NO_CUSTOMER_EXCEL);
        vtvDataSheet.put(CUSTOMER_SEARCH_WITH_ALL_TRIM_ASSEMBLIES, CUSTOMER_SEARCH_WITH_ALL_TRIM_ASSEMBLIES_EXCEL);
        vtvDataSheet.put(CUSTOMER_SEARCH_WITH_NO_TRIM_ASSEMBLIES, CUSTOMER_SEARCH_WITH_NO_TRIM_ASSEMBLIES_EXCEL);
        vtvDataSheet.put(CUSTOMER_SEARCH_WITH_TRIM_MANY_ASSEMBLIES, CUSTOMER_SEARCH_WITH_TRIM_MANY_ASSEMBLIES_EXCEL);
        vtvDataSheet.put(VEHICLE_SEARCH_OE_OR_NON_OE, VEHICLE_SEARCH_OE_OR_NON_OE_EXCEL);
        vtvDataSheet.put(VEHICLE_SEARCH_LOOKUP_YEAR_MAKE_MODEL, VEHICLE_SEARCH_LOOKUP_YEAR_MAKE_MODEL_EXCEL);
        vtvDataSheet.put(VEHICLE_SEARCH_WITH_IMAGE, VEHICLE_SEARCH_WITH_IMAGE_EXCEL);
        vtvDataSheet.put(VEHICLE_SEARCH_WITH_NO_IMAGE, VEHICLE_SEARCH_WITH_NO_IMAGE_EXCEL);
        vtvDataSheet.put(VEHICLE_SEARCH_TRIMS, VEHICLE_SEARCH_TRIMS_EXCEL);
        vtvDataSheet.put(VEHICLE_SEARCH_STAGGERED, VEHICLE_SEARCH_STAGGERED_EXCEL);
        vtvDataSheet.put(VEHICLE_SEARCH_DUALLY, VEHICLE_SEARCH_DUALLY_EXCEL);
        vtvDataSheet.put(CREATE_SERVICE_ACTIVITY_ADD_SERVICES, CREATE_SERVICE_ACTIVITY_ADD_SERVICES_EXCEL);
        vtvDataSheet.put(CREATE_SERVICE_ACTIVITY_EXISTING_CUSTOMER, CREATE_SERVICE_ACTIVITY_EXISTING_CUSTOMER_EXCEL);
        vtvDataSheet.put(CREATE_SERVICE_ACTIVITY_UPDATE_SERVICES, CREATE_SERVICE_ACTIVITY_UPDATE_SERVICES_EXCEL);
    }


    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElement(String elementName) {
        LOGGER.info("returnElement started");
        switch (elementName) {
            case EXECUTE:
                return execute;
            case TCODE:
                return tCode;
            case HEADER:
                return header;
            case TRANSACTION_STATUS:
                return transactionStatus;
            case TRANSACTION_NUMBER:
                return transactionNumber;
            case CLOSE_BUTTON:
                return closeButton;
            case TRANSACTION_TYPE:
                return transType;
            case TRANSACTION_ID_1:
                return transactionId1;
            case TRANSACTION_ID_2:
                return transactionId2;
            default:
                Assert.fail("FAIL: Could not find icon that matched string passed from step");
                return null;
        }
    }

    /**
     * This Method clicks an element in posdt page.
     *
     * @param elementName - send in elementName and gets the WebElement from returnElement method.
     */
    public void clickInPosDtPage(String elementName) {
        LOGGER.info("clickInPosDtPage started");
        commonActions.click(returnElement(elementName));
        LOGGER.info("clickInPosDtPage completed");
    }

    /**
     * Verifies if the transactions were successful.
     */
    public void verifyIfTransactionsAreSuccessful() {
        LOGGER.info("verifyIfTransactionsAreSuccessful started");
        List<WebElement> input = webDriver.findElements(taskStatusOverview);
        for (int i = 0; i < input.size(); i++)
            driver.isElementDisplayed(input.get(i));
        LOGGER.info("verifyIfTransactionsAreSuccessful completed");
    }

    /**
     * Verifies article number and sales price in posdt.
     */
    public void assertArticleAndSalesPriceOnSalesItemDataInPosDt() {
        LOGGER.info("assertArticleAndSalesPriceOnSalesItemDataInPosDt started");
        List<WebElement> elements = webDriver.findElements(saleItemDataBy);
        for (int i = 0; i < IDocPage.rowCount; i++) {
            int hashMapKeyIncrement = i + 1;
            if (!driver.isElementDisplayed(elements.get(i))) {
                while (!elements.get(i).isDisplayed()) {
                    try {
                        driver.waitForElementVisible(scrollBarNextButton);
                        scrollBarNextButton.click();
                    } catch (StaleElementReferenceException s) {
                        WebElement element = webDriver.findElement(commonActions.extractByLocatorFromWebElement(scrollBarNextButton));
                        commonActions.click(element);
                    }
                }
            }
            driver.verifyTextDisplayedCaseInsensitive(elements.get(i), driver.scenarioData.getData
                    (CommonActions.ARTICLE_FROM_LINE + hashMapKeyIncrement));
            Assert.assertTrue("FAIL: Text expected is not displayed! '" + driver.scenarioData.getData
                    (CommonActions.AMOUNT_FROM_LINE + hashMapKeyIncrement).replace("-", "") + "' " +
                    "is not present in '" + elements.get(i).getText(), elements.get(i).getText().replace(",",
                    "").contains(driver.scenarioData.getData
                    (CommonActions.AMOUNT_FROM_LINE + hashMapKeyIncrement).replace("-", "")));
            LOGGER.info("assertArticleAndSalesPriceOnSalesItemDataInPosDt completed");
        }
    }

    /**
     * Verifies article number in posdt.
     */
    public void assertArticlesItemDataInPosDt() {
        LOGGER.info("assertArticlesItemDataInPosDt started");
        driver.waitForPageToLoad();
        List<WebElement> elements = webDriver.findElements(goodsMovementDataBy);
        for (int i = 0; i < IDocPage.rowCount; i++) {
            int hashMapKeyIncrement = i + 1;
            if (!driver.isElementDisplayed(elements.get(i))) {
                while (!elements.get(i).isDisplayed()) {
                    try {
                        driver.waitForElementVisible(scrollBarNextButton);
                        scrollBarNextButton.click();
                    } catch (StaleElementReferenceException s) {
                        WebElement element = webDriver.findElement(commonActions.extractByLocatorFromWebElement(scrollBarNextButton));
                        commonActions.click(element);
                    }
                }
            }
            driver.verifyTextDisplayedCaseInsensitive(elements.get(i), driver.scenarioData.getData
                    (CommonActions.ARTICLE_FROM_LINE + hashMapKeyIncrement));
            LOGGER.info("assertArticlesItemDataInPosDt completed");
        }
    }

    /**
     * This method will click the scroll bar multiple times in CAQ Trickle feed.
     *
     * @param counter - Integer, Number of times the element has to be clicked
     */
    public void clickMultipleTimes(int counter) {
        LOGGER.info("clickMultipleTimes started");
        while (counter != 0) {
            try {
                commonActions.click(scrollBarNextButton);
                counter--;
            } catch (StaleElementReferenceException s) {
                WebElement element = webDriver.findElement(scrollBarNextButtonBy);
                commonActions.click(element);
                counter--;
            }
        }
        LOGGER.info("clickMultipleTimes completed");
    }

    public void doubleClickOnValueAttribute(String valueAttribute) {
        WebElement scrollButton = webDriver.findElements(By.xpath("//div[contains(@id,'_hscroll-Nxt')]")).get(2);
        commonActions.click(scrollButton);
        commonActions.click(scrollButton);
        commonActions.click(scrollButton);
        commonActions.click(scrollButton);
        commonActions.doubleClickOnElement(webDriver.findElement(By.xpath("//*[contains(@value,'" + valueAttribute + "')]")));
    }

    /**
     * Extracts total amount from excel and asserts in posdt
     *
     * @throws Exception
     */
    public void verifyTotalTransactionAmount(String sheetName, String location) throws Exception {
        LOGGER.info("verifyTotalTransactionAmount started");
        iDocPage.readFromExcelAndReturnSingleValue(location, sheetName,
                IDocPage.rowCount + 1, 6, TOTAL_AMOUNT_KEY);
        if (driver.scenarioData.getData("Total Amount").contains("-")) {
            Assert.assertTrue("Fail: WebElement text is: " + totalAmountPosdt.getAttribute(Constants.VALUE).replace(",",
                    "") + " And validation text is: " + driver.scenarioData.getData
                    (TOTAL_AMOUNT_KEY).replace("-", ""), totalAmountPosdt.getAttribute(Constants.VALUE).replace(
                    ",", "").contains(driver.scenarioData.getData
                    (TOTAL_AMOUNT_KEY).replace("-", "")));

            driver.assertElementAttributeString(totalAmountPosdt, Constants.VALUE, "-");
        } else {
            Assert.assertTrue("Fail: WebElement text is: " + totalAmountPosdt.getAttribute(Constants.VALUE).replace(",",
                    "") + " And validation text is: " + driver.scenarioData.getData
                    (TOTAL_AMOUNT_KEY).replace("-", ""), totalAmountPosdt.getAttribute(Constants.VALUE).replace(
                    ",", "").contains(driver.scenarioData.getData
                    (TOTAL_AMOUNT_KEY).replace("-", "")));
        }
        LOGGER.info("verifyTotalTransactionAmount completed");
    }

    /**
     * method will look for the generated web order and extracts the dtd order number to search in DTD
     */
    public void searchForHybrisOrderRelatedTransactionInPosDt() {
        LOGGER.info("searchForhybrisOrderRelatedTransactionInPosDt started");
        int maxAttempts = 20;
        int attempt = 0;
        do {
            if (!driver.isElementDisplayed(store)) {
                commonActions.clickOnPartialText(BACK);
                driver.waitForMilliseconds(Constants.TWO_THOUSAND);
                commonActions.clickOnPartialText(EXECUTE);
                driver.waitForMilliseconds(Constants.THREE_THOUSAND);
                attempt++;
            } else {
                break;
            }
        } while (attempt < maxAttempts);
        int orderNumber = Integer.valueOf(driver.scenarioData.getCurrentOrderNumber());
        String cleanedOrderNumber = String.valueOf(orderNumber);
        boolean isFound = false;
        int limit = webDriver.findElements(PosDm.transactionNumberColumnsBy).size();
        for (int i = 0; i < limit; i++) {
            WebElement element = webDriver.findElements(PosDm.transactionNumberColumnsBy).get(i);
            driver.waitForMilliseconds();
            commonActions.doubleClickOnElement(element);
            driver.waitForPageToLoad();
            if (webDriver.findElement(PosDm.hybrisOrderNumberBy).getText().contains(cleanedOrderNumber)) {
                String dtdOrderNumber = webDriver.findElement(webOrderNumberBy).getText();
                //Todo: Integrate with excel. For now a back up when search with web order number fails in DTD POS
                driver.scenarioData.setData(PosDm.HYBRIS_ORDER_NUMBER_HASH_MAP_KEY, dtdOrderNumber.substring(0,
                        dtdOrderNumber.length() - 5));
                LOGGER.info("Order number Captured: " + driver.scenarioData.
                        getData(PosDm.HYBRIS_ORDER_NUMBER_HASH_MAP_KEY));
                isFound = true;
                break;
            } else {
                driver.performKeyAction(Keys.F3);
            }
            driver.waitForPageToLoad();
        }
        if (!isFound) {
            Assert.fail("Failed to find Hybris order");
        }
        LOGGER.info("searchForhybrisOrderRelatedTransactionInPosDt completed");
    }

    public void getPurchaseOrderNumberForWebOrders() {
        LOGGER.info("getPurchaseOrderNumberForWebOrders started");
        commonActions.click(webDriver.findElement(firstLineItemExpandBy));
        Assert.assertTrue("FAIL: Purchase Order number not found in CAR", driver.isElementDisplayed(purchaseOrderNumberFieldBy));
        driver.scenarioData.setCurrentPurchaseOrderNumber(webDriver.findElement(purchaseOrderNumberFieldBy).getText().substring(5).replaceAll("[^0-9]", ""));
        LOGGER.info("getPurchaseOrderNumberForWebOrders completed");
    }

    /**
     * Saves the element text value. Key is the element name.
     *
     * @param elementName value of the string that should be saved into HashMap
     */
    public void saveElementValueFromText(String elementName) {
        LOGGER.info("saveElementValueFromText started");
        driver.waitForElementVisible(returnElement(elementName), Constants.ONE_HUNDRED_EIGHTY);
        driver.scenarioData.setData(elementName, returnElement(elementName).getText());
        if (driver.scenarioData.getData(elementName).equalsIgnoreCase("")) {
            driver.scenarioData.setData(elementName, returnElement(elementName).getAttribute(Constants.VALUE));
        }
        LOGGER.info("saveElementValueFromText completed");
    }

    /**
     * Returns the value for corresponding field name on VTV data sheet
     *
     * @param FieldValue Excel Sheet field names that are required to be validated
     * @return String
     */
    public String returnCorrespondingTableFieldValueVtv(String FieldValue) {
        LOGGER.info("returnCorrespondingTableFieldValueVtv started for field " + FieldValue);
        switch (FieldValue) {
            case "year":
                return "VYEAR";
            case "make":
                return "MAKE";
            case "model":
                return "MODEL";
            case "vin":
                return "VIN";
            case "customerid":
                return "VTVCUSTOMERID";
            case "vehicleId":
                return "VEHICLEID";
            case "trim":
                return "TRIMDESC";
            case "trimId":
                return "TRIMID";
            case "trimDesc":
                return "TRIMDESC";
            case "ChassisId":
                return "CHASSISID";
            case "Assembly":
                return "ASSEMBLY";
            case "AssemblyId":
                return "ASSEMBLYID";
            case "VehicleColor":
                return "VEHICLECOLOR";
            case "IsStaggered":
                return "ISSTAGGERED";
            case "IsDualRearWheel":
                return "ISDUALREARWHEEL";
            case "IsNonOriginalEquipment":
                return "ISNONORIGINALEQUIPMENT";
            case "FrontPsi":
                return "FRONTPSI";
            case "FrontAssemblyDetails":
                return "FRONTASSEMBLYDETAILS";
            case "RearPsi":
                return "REARPSI";
            case "RearAssemblyDetails":
                return "REARASSEMBLYDETAILS";
            case "VehicleImage":
                return "VEHICLEIMAGEURL";
            case "FullName":
                return "FULLNAME";
            case "FirstName":
                return "FIRSTNAME";
            case "LastName":
                return "LASTNAME";
            case "Phone":
                return "PHONE";
            case "Email":
                return "EMAIL";
            default:
                Assert.fail("Could not find the field value " + FieldValue + " that matched the given strings. " +
                        "Please input the right value");
                return null;
        }
    }

    /**
     * Verifies the VTV test data with the xml values
     *
     * @param fieldName field whose value needs to be evaluated
     * @param vinNumber VIN number of the vehicle
     * @throws Throwable
     */
    public void assertVtvVehicleTableValuesXml(String fieldName, String vinNumber) throws Throwable {
        LOGGER.info("assertVtvVehicleTableValuesXml started");
        int fieldRowNumber = 0;
        List<WebElement> vehicleFieldValues = webDriver.findElements(vtvVehicleTableFieldsBy);
        for (int i = 0; i < vehicleFieldValues.size(); i++) {
            if (vehicleFieldValues.get(i).getText().replace("\n", "").replace(" ", "").
                    equalsIgnoreCase(returnCorrespondingTableFieldValueVtv(fieldName))) {
                fieldRowNumber = i;
                break;
            }
        }
        List<WebElement> fieldValues = webDriver.findElements(By.xpath("//input[@value='" + vinNumber + "']//following::" +
                "table[contains(@id,'mrss-cont-none-content')]//following::input"));
        Assert.assertTrue("FAIL: assertion failed",
                fieldValues.get(fieldRowNumber).getAttribute(Constants.VALUE).
                        equalsIgnoreCase(webServices.returnVehicleFieldValueFromVtvResponse(fieldName, vinNumber)));
        LOGGER.info("assertVtvVehicleTableValuesXml completed");
    }

    /**
     * Verifies the VTV test data with the data in the excel sheet
     *
     * @param fieldName Field whose value needs to be evaluated
     * @param table     Vehicle or customer data (ZVTV_VEHICLE or ZVTV_CUSTOMER)
     * @param vinNumber VIN number of the vehicle
     * @param scenario  Specifies the name of the scenario to access the respective excel data sheet
     * @throws Throwable
     */

    public void assertVtvVehicleTableValuesExcel(String fieldName, String table, String vinNumber, String scenario) throws Throwable {
        LOGGER.info("assertVtvVehicleTableValuesExcel started for table " + table + " and vin " + vinNumber);
        int k = 0;
        List<WebElement> fieldValues;
        List<WebElement> vehicleFieldValues;
        vehicleFieldValues = webDriver.findElements(vtvVehicleTableFieldsBy);

        for (int i = 0; i < vehicleFieldValues.size(); i++) {
            k = i;
            if (vehicleFieldValues.get(i).getText().replace("\n", "").replace(" ", "").
                    equalsIgnoreCase(returnCorrespondingTableFieldValueVtv(fieldName))) {
                break;
            }
        }
        File fs = new File(returnFileLocationVtvDataSheet(scenario));
        FileInputStream fis = new FileInputStream(fs);
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        Cell cell = null;

        int rowcount = sheet.getLastRowNum() + 1;
        int colcount = sheet.getRow(0).getLastCellNum();
        String[][] data = new String[rowcount][colcount];

        for (int row = 0; row < rowcount; row++) {
            XSSFRow sheetRow = sheet.getRow(row);
            for (int col = 0; col < colcount; col++) {
                try {
                    cell = sheetRow.getCell(col);
                    if (cell == null) {
                        data[row][col] = "";
                    } else {
                        data[row][col] = cell.toString();
                    }
                } catch (Exception e) {
                    row = row++;
                    col = col++;

                    if (cell == null) {
                        data[row][col] = "";
                    } else {
                        data[row][col] = cell.toString();
                    }
                }
            }
        }

        fieldValues = webDriver.findElements(VTVFieldsBy);

        for (int i = 0; i < rowcount; i++) {
            for (int j = 0; j < colcount; j++) {
                if (data[i][j].equalsIgnoreCase(fieldName)) {
                    if (fieldName.equalsIgnoreCase(IS_STAGGERED)) {
                        if (data[i][1].equalsIgnoreCase(CommonActions.TRUE)) {
                            data[i][1] = "X";
                        }
                    }
                    Assert.assertTrue("FAIL: assertion failed, values do not match " + fieldValues.get(k).
                                    getAttribute(Constants.VALUE).toLowerCase().replace(" ", "").split("png")[0].replace("-", "") + "not equal to " + data[i][1].toLowerCase().replace
                                    ("-", "").replace(")", "").replace("(", "").
                                    replace(" ", "").split("png")[0],
                            data[i][1].toLowerCase().replace("-", "").replace(")", "").replace("(", "").
                                    replace(" ", "").split("png")[0].contains(fieldValues.get(k).
                                    getAttribute(Constants.VALUE).toLowerCase().replace(" ", "").split("png")[0].replace("-", "")));
                    break;
                }
            }
        }
        LOGGER.info("assertVtvVehicleTableValuesExcel completed for table " + table + " and vin " + vinNumber);
    }

    /**
     * Returns the location of the file based on the scenario
     *
     * @param scenario Name of the scenario
     * @return location of the file
     */
    public String returnFileLocationVtvDataSheet(String scenario) {
        return vtvDataSheet.get(scenario);
    }

    /**
     * return the VIN, Vehicle ID and Customer ID from the specified excel sheet
     *
     * @param labelName Name of the label on VTV Vehicle selection screen
     * @param position  Position of the first occurance of the labelName on VTV Vehicle selection screen
     * @param valueName Value of the specified field in excel data sheet
     * @param scenario  Specifies the name of the scenario to access the respective excel data sheet
     * @throws Throwable
     */
    public void returnRequiredValuesFromExcelForVTV(String labelName, int position, String valueName, String scenario) throws Throwable {
        LOGGER.info("returnRequiredValuesFromExcelForVTV started");
        File fs = new File(returnFileLocationVtvDataSheet(scenario));
        FileInputStream fis = new FileInputStream(fs);
        XSSFWorkbook workbook = new XSSFWorkbook(fis);
        XSSFSheet sheet = workbook.getSheetAt(0);
        String value;
        Row row;
        if (valueName.equalsIgnoreCase(PosDt.VIN)) {
            row = sheet.getRow(28);
            value = String.valueOf(row.getCell(1));
            driver.scenarioData.setData(PosDt.VIN, value);
        } else if (valueName.equalsIgnoreCase(PosDt.VEHICLE_ID)) {
            row = sheet.getRow(26);
            value = String.valueOf(row.getCell(1));
            driver.scenarioData.setData(PosDt.VEHICLE_ID, value);
        } else {
            row = sheet.getRow(37);
            value = String.valueOf(row.getCell(1));
            driver.scenarioData.setData(PosDt.VTV_CUSTOMER_ID, value);
        }
        commonActions.sendKeysWithLabelName(labelName, value, position);
        LOGGER.info("returnRequiredValuesFromExcelForVTV started");
    }
}