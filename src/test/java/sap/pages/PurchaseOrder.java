package sap.pages;

import common.Config;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import utilities.CommonUtils;
import utilities.Driver;
import org.junit.Assert;
import common.Constants;

/**
 * Created by mnabizadeh on 5/14/18.
 */
public class PurchaseOrder {

    private Driver driver;
    private WebDriver webDriver;
    private CommonActions commonActions;
    private final Logger LOGGER = Logger.getLogger(PurchaseOrder.class.getName());

    public PurchaseOrder(Driver driver) {
        this.driver = driver;
        commonActions = new CommonActions(driver);
        webDriver = driver.getDriver();
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(xpath = "//td[contains(@id,'title')]")
    private static WebElement pageTitle;

    @FindBy(xpath = "//*[contains(text(),'Hide Overview')]//following::input[1]")
    private static WebElement poActionTypeDropDownInMigo;

    @FindBy(xpath = "//*[contains(text(),'Hide Overview')]//following::input[2]")
    private static WebElement poOrderTypeDropDownInMigo;

    @FindBy(xpath = "//*[contains(text(),'Hide Overview')]//following::input[3]")
    private static WebElement poOrderInputInMigo;

    @FindBy(xpath = "//*[contains(text(),'HeaderText')]/following::table[3]")
    private static WebElement poItemListTable;

    @FindBy(xpath = "//*[text()=\"Item OK\"]")
    private static WebElement itemOKCheckBoxContatiner;

    @FindBy(id = "wnd[0]/sbar_msg-txt")
    private static WebElement alertBanner;

    @FindBy(id = "M0:U:1:1:2::0:21")
    private static WebElement receiptValidationPOInput;

    @FindBy(id = "M0:U")
    private static WebElement applicationBackground;

    @FindBy(id = "M1:U:1::0:21")
    private static WebElement purchaseOrderPopupInput;

    @FindBy(id = "M1:D:13::btn[0]")
    private static WebElement otherDocumentPopupButton;

    @FindBy(id = "M0:U:1:4:2:1:2:1:2B271:1::0:58")
    private static WebElement acknowledgementInput;

    @FindBy(id = "M0:U:1:4:2:1:2:1:2B271:1::0:80-img")
    private static WebElement acknowledgementCheckbox;

    @FindBy(id = "MEPO1334-BSTAESAPLMEGUI-key-2")
    private static WebElement dtcConfirmationOptionalSelection;

    @FindBy(id = "MEPO1334-BSTAESAPLMEGUI-r")
    private static WebElement confirmationDropDownContainer;

    @FindBy(xpath = "//span[contains(text(),'Personal')]/following::input[1]")
    private static WebElement purchaseOrderTypeDropDown;

    @FindBy(id = "wnd[0]/sbar_msg-txt")
    private static WebElement purchaseOrderNumberAlertContainer;

    @FindBy(xpath = "//span[contains(text(),'Document Overview On')]//following::span[7]")
    private static WebElement otherPurchaseOrder;

    @FindBy(id = "M0:U:1:4:1:1::0:15-focus")
    private static WebElement outputsTab;

    @FindBy(id = "M0:U:1:4:1:1:18B276::0:0-caption")
    private static WebElement displayOutputsButton;

    @FindBy(className = "lsButton__text urBtnCntTxt")
    private static WebElement otherDocButton;

    @FindBy(id = "M0:U:1:4:2:1:2:1::0:12-title")
    private static WebElement purchaseOrderHistoryTab;

    @FindBy(id = "tbl1940[1,1]_c")
    private static WebElement oaPartnerFunction;

    @FindBy(id = "M0:U:1:4:1:1:4B262::0:22")
    private static WebElement quantityInUnitOfEntry;

    @FindBy(id = "tbl1940[1,3]_c")
    private static WebElement oaVendorValue;

    @FindBy(id = "tbl1940[2,1]_c")
    private static WebElement vnPartnerFunction;

    @FindBy(id = "tbl1940[3,1]_c")
    private static WebElement piPartnerFunction;

    @FindBy(id = "tbl1940[4,1]_c")
    private static WebElement gsPartnerFunction;

    @FindBy(id = "M0:U:1:2:2:1:1:2B258:1:2:1-cnt")
    public static WebElement textDisplayed;

    @FindBy(id = "M0:U:1:2:2:1:1:2B259:1:1::0:22")
    private static WebElement streetNumber;

    @FindBy(id = "M0:U:1:2:2:1:1:2B259:1:1::0:30")
    private static WebElement streetAddress;

    @FindBy(id = "M0:U:1:2:2:1:1:2B259:1:1::1:22")
    private static WebElement city;

    @FindBy(id = "M0:U:1:2:2:1:1:2B259:1:1::1:67")
    private static WebElement zipCode;

    @FindBy(id = "M0:U:1:2:2:1:1:2B259:1:1::2:22")
    private static WebElement country;

    @FindBy(xpath = ("//td[contains(@id,\"[3,0]\")]"))
    private static WebElement purchaseOrderTableHeaderEmailOutputSelector;

    @FindBy(id = "M0:U:1-mrss-cont-left-content")
    private static WebElement articleDocumentOutputTableHeaderContainer;

    @FindBy(id = "M0:D:13::btn[26]-r")
    private static WebElement processingLogTab;

    @FindBy(id = "M1:U-content")
    private static WebElement processingLogPopupTable;

    @FindBy(id = "M0:U:1:2:2:1:1:2B260::0:77")
    private static WebElement poYourReference;

    @FindBy(id = "M1:U:::6:6_l")
    private static WebElement znfyStatus;

    @FindBy(xpath = "//*[text()='ZNFY']")
    private static WebElement znfy;

    @FindBy(id = "M0:D:13::btn[26]")
    private static WebElement processingLog;

    @FindBy(id = "M0:U:1:4:2:1:2:1::0:12-title")
    private static WebElement purchaseOrderHistory;

    @FindBy(id = "M0:U:1:4:1:1::0:1-title")
    private static WebElement quantityTab;

    @FindBy(id = "M0:U:1:4:1:1::0:15-title")
    private static WebElement output;

    @FindBy(id = "M0:U:1:4:1:1:18B276::0:0-caption")
    private static WebElement displayOutput;

    @FindBy(id = "M0:U:1:4::0:0")
    private static WebElement detailDataHeader;

    @FindBy(id = "M0:46:1:2:1::0:0")
    private static WebElement headData;

    @FindBy(id = "M0:46:1:4:1::0:0")
    private static WebElement itemDetail;

    @FindBy(xpath = "//*[@id=\"M0:U:1:4:2:1:2:1-prevLeft\"]")
    private static WebElement tabLeftArrowDetail;

    @FindBy(id = "M0:U:1:2:1::0:0")
    private static WebElement buttonStatus;

    @FindBy(xpath = "//*[text()='Status'][1]/preceding::*[text()='Status'][2]")
    private static WebElement statusTab;

    @FindBy(xpath = "//*[text()='Confirmations' and (@id)]")
    private static WebElement confirmationTab;

    @FindBy(id = "webguiPopupWindow1-ttltxt")
    private static WebElement popupWindowTitle;

    @FindBy(id = "M1:U:::4:0")
    private static WebElement popupWindowSaveButton;

    @FindBy(id = "M1:D:13::btn[0]")
    private static WebElement popupWindowTickButton;

    @FindBy(xpath = "//*[contains(@id, 'MEPO1334') and contains(@id, 'key-0')]")
    private static WebElement confirmOptionalChoice;

    @FindBy(xpath = "//*[contains(@id, 'MEPO1334') and contains(@id, 'key-1')]")
    private static WebElement confirmationsChoice;

    @FindBy(xpath = "//*[contains(@id, 'MEPO1334') and contains(@id, 'key-2')]")
    private static WebElement dtcConfirmOptionalChoice;

    @FindBy(xpath = "//*[contains(@id, 'MEPO1334') and contains(@id, 'key-3')]")
    private static WebElement delivRoughGrChoice;

    @FindBy(xpath = "//*[contains(@id, 'MEPO1334') and contains(@id, 'key-4')]")
    private static WebElement inboundDeliveryChoice;

    @FindBy(xpath = "//*[contains(@id, 'MEPO1334') and contains(@id, 'key-5')]")
    private static WebElement roughGrChoice;

    @FindBy(xpath = "//*[contains(@id, 'MEPO1334') and contains(@id, 'key-7')]")
    private static WebElement noChoice;

    @FindBy(xpath = "//*[text()='Conf. Control']//following::div[3]")
    private static WebElement confirmationDropDown;

    @FindBy(xpath = "//*[text()='Select Document']//following::input[1]")
    private static WebElement purchaseOrderInput;

    @FindBy(xpath = "//*[contains(text(),'Tax Code')]//following::input[1]")
    private static WebElement taxCodeInInvoiceMe21n;

    @FindBy(xpath = "//*[text()='Tax']//following::input[1]")
    private static WebElement taxFieldInMe21n;

    @FindBy(xpath = "//*[text()='Supplying Site']//following::input[1]")
    private static WebElement supplyingSite;

    @FindBy(xpath = "//*[text()='Supplying Plant']//following::input[1]")
    private static WebElement supplyingPlant;

    @FindBy(xpath = "//*[contains(@id,'[1,9]_c-btn')]")
    private static WebElement purchaseOrderTaxCode;

    @FindBy(xpath = "//*[contains(text(), 'Item OK')]/preceding::img[1]")
    private static WebElement itemOk;

    @FindBy(id = "M1:D:13::btn[2]")
    private static WebElement continueEnter;

    @FindBy(id = "URLSPW-0")
    private static WebElement iFrame;

    @FindBy(id = "M0:U:1:4:2:1:1::0:61-img")
    private static WebElement nextArticle;

    @FindBy(xpath = "//span[contains(@id,'wnd[0]/sbar_msg-txt')]")
    private static WebElement successfulMessage;

    @FindBy(xpath = "//*[contains(@id,'[1,5]_c') and not (contains(@id,'[1,5]_c-r'))]")
    private static WebElement purchaseOrderTableArticle;

    @FindBy(xpath = "//*[contains(@id,'[1,6]_c') and not (contains(@id,'[1,6]_c-r'))]")
    private static WebElement purchaseOrderTableShortText;

    @FindBy(xpath = "//*[contains(@id,'[1,3]_c-r')]")
    private static WebElement purchaseOrderTableAssetCategory;

    @FindBy(xpath = "//*[contains(@id,'[1,7]_c') and not (contains(@id,'[1,7]_c-r'))]")
    private static WebElement purchaseOrderTableQuantity;

    @FindBy(xpath = " //*[contains(text(),'Article Data')]/following::*[contains(@id,'[1,7]_c')][1]")
    private static WebElement purchaseOrderTableAssetID;

    @FindBy(xpath = "//*[contains(@id,'[1,16]_c') and not (contains(@id,'[1,16]_c-r'))]")
    private static WebElement siteOrPlant;

    @FindBy(xpath = "//*[contains(@id,'[1,12]_c') and not (contains(@id,'[1,12]_c-r'))]")
    private static WebElement siteInStoreTransferPurchaseOrder;

    @FindBy(xpath = "//*[text()='Net']/following::input[1]")
    private static WebElement netCost;

    @FindBy(xpath = "//*[text()='Driver Name']/following::input[1]")
    private static WebElement driverName;

    @FindBy(xpath = "//*[text()='RGA/RMA #']/following::input[1]")
    private static WebElement rgaRma;

    @FindBy(xpath = "//*[text()='BOL/PRO #']/following::input[1]")
    public static WebElement bolPro;

    @FindBy(xpath = "//*[text()='Original PO #']/following::input[1]")
    private static WebElement originalPo;

    @FindBy(xpath = "//*[text()='Method of Return']/following::input[1]")
    private static WebElement methodOfReturn;

    @FindBy(xpath = "//span[text()='Confirmations']")
    private static WebElement confirmationsTabInMe21n;

    @FindBy(xpath = "//span[text()='Delivery Address']/following::a[2]")
    private static WebElement rightIndicatorInMe21n;

    @FindBy(xpath = "//*[contains(@title,'Next Item')]")
    private static WebElement nextItemButtonInMigo;

    @FindBy(xpath = "//div[contains(@id,'_vscroll-Nxt')]")
    private static WebElement scrollBarNextButton;

    @FindBy(xpath = "//*[contains(text(),'Payment methods')]//following::input[1]")
    private static WebElement paymentMethods;

    @FindBy(xpath = "//*[contains(@id,'[1,4]_c') and not ( contains(@id,'[1,4]_c-r'))  and not (contains(@id,'[1,4]_c-btn'))]")
    public static WebElement mediumInput;

    @FindBy(xpath = "//*[contains(@id,'[1,11]_c') and not (contains(@id,'[1,11]_c-r'))]")
    public static WebElement purchaseOrderTableNetPrice;

    @FindBy(xpath = "//div[contains(text(),'KR')][1]/preceding::div[2]")
    private static WebElement krDocumentNumber;

    @FindBy(xpath = "//*[text()='Document Number']/following::input[1]")
    private static WebElement documentNumber;

    @FindBy(xpath = "//span[contains(@id,'[1,2]_c')]")
    private static WebElement glAccountNumber;

    @FindBy(xpath = "//div[contains(@id,'[1,4]_c-btn')]")
    private static WebElement debitOrCredit;

    @FindBy(xpath = "//*[contains(@id,'[1,5]_c') and not (contains(@id,'[1,5]_c-r'))]")
    private static WebElement amount;

    @FindBy(xpath = "//*[text()='Deliv. Compl.']/preceding::img[contains(@class,'urCImgOnDsbl')][1]")
    private static WebElement deliveryCompleteCheckBox;

    @FindBy(xpath = "//*[contains(@title,'\"Delivery Completed\" Indicator')]")
    private static WebElement deliveryCompleteCheckButton;

    @FindBy(xpath = "//*[text()='Posting date']/following::input[2]")
    private static WebElement postingDateToField;

    @FindBy(xpath = "//*[text()='Posting date']/following::input[1]")
    private static WebElement postingDateFromField;

    @FindBy(xpath = "//*[text()='Posting Date']/following::input[1]")
    private static WebElement postingDate;

    @FindBy(xpath = "//input[contains(@title,'Date on Which the Program Is to Be Run')]")
    private static WebElement runDateInF110;

    @FindBy(xpath = "//input[contains(@title,'Additional Identification')]")
    private static WebElement identificationCodeInput;

    @FindBy(xpath = "//*[contains(@id,'[1,1]_c') and not (contains(@id,'[1,1]_c-r'))]")
    private static WebElement companyCodesFieldInPaymentsControlTable;

    @FindBy(xpath = "//*[contains(@id,'[1,2]_c') and not (contains(@id,'[1,2]_c-r'))]")
    private static WebElement pmtMethsFieldInPaymentsControlTable;

    @FindBy(xpath = "//*[contains(@id,'[1,3]_c') and not (contains(@id,'[1,3]_c-r'))]")
    private static WebElement nextPDateInPaymentsControlTable;

    @FindBy(xpath = "//span[contains(@id,'[3,2]_c')]")
    private static WebElement rffous_c;

    @FindBy(xpath = "//input[contains(@title,'Invoice Date in Document')]")
    private static WebElement invoiceDateInputInMiro;

    @FindBy(xpath = "(//*[text()='Values']/following::input[1])[1]")
    private static WebElement valuesUnderFreeSelection;

    @FindBy(id = "ls-inputfieldhelpbutton")
    private static WebElement optionsButtonForFieldName;

    @FindBy(xpath = "//input[contains(@id,'[1,2]_c')]")
    private static WebElement paymentTypeInF110;

    @FindBy(xpath = "//*[contains(@id,'1,13#if-r')]/child::*")
    private static WebElement clearingDocumentNumber;

    @FindBy(xpath = "//*[text()='\\\\RTKCI\\sapmnt\\RTK\\COMM']/preceding::span[1]")
    private static WebElement dirCommInAL11;

    @FindBy(xpath = "//*[@value ='\\\\RTQCI\\sapmnt\\RTQ\\COMM']/preceding::span[1]")
    private static WebElement dirComm2InAL11;

    @FindBy(xpath = "//*[text()='int']")
    private static WebElement intInTable;

    @FindBy(xpath = "//*[@value ='int']")
    private static WebElement intInTable2;

    @FindBy(xpath = "//*[text()='o']")
    private static WebElement oInTable;

    @FindBy(xpath = "//*[@value ='o']")
    private static WebElement oInTable2;

    @FindBy(xpath = "(//input[contains(@title,'Last Changed On')])[1]")
    private static WebElement lastChangedOnFrom;

    @FindBy(xpath = "(//input[contains(@title,'Last changed at')])[1]")
    private static WebElement enterTimeBeforeFileCreationInAl11;

    @FindBy(xpath = "(//input[contains(@title,'Last changed at')])[2]")
    private static WebElement entertimeAfterFileCreationInAl11;

    @FindBy(xpath = "//*[contains(@id,'1,15#if')]/child::*")
    private static WebElement amountInEkbe;

    @FindBy(xpath = "//*[contains(@id,'1,11#if')]/child::*")
    private static WebElement postingDateInEkbe;

    @FindBy(xpath = "//*[contains(@id,\"1,3#if-r\")]")
    private static WebElement articleDocumentNumber;

    @FindBy(xpath = "//*[text()='Amt.in loc.cur.']/following::input[1]")
    private static WebElement amountInMiro;

    @FindBy(xpath = "//*[text()='Document Date']/following::input[1]")
    private static WebElement documentDate;

    @FindBy(xpath = "//span[contains(@id,',5]_c')]/parent::div//child::img")
    private static WebElement bookingOKInputRow;

    @FindBy(xpath = "//*[contains(@id,'[1,5]_c') and not (contains(@id,'[1,5]_c-txt'))]")
    private static WebElement bookingOKInputRow2;

    @FindBy(xpath = "(//span[contains(text(),'VSegCatID')]/following::*[contains(@id,'1,3#if')])[2]")
    private static WebElement vSegCatId;

    @FindBy(xpath = "(//*[contains(@id,'1,4#if')])[2]")
    private static WebElement vehSegCatID;

    @FindBy(xpath = "(//span[contains(text(),'Vehicle ID')]/following::*[contains(@id,'1,1#if')])[2]")
    private static WebElement vehicleIDFromVehList;

    private static final By lastChangedOnBy = By.xpath("(//*[contains(@title,'Last Changed On')])[1]");

    private static final By lastChangedATBy= By.xpath( "(//*[contains(@title,'Last changed at')])[1]");

    private static final By companyCodesFieldBy = By.xpath( "//input[contains(@id,'[1,1]_c')]");

    private static final By articleNumberInputRowBy = By.xpath("//*[contains(@id,',5]_c')][not (contains(@id,',5]_c-r'))]");

    private static final By quantityNumberInputRowBy = By.xpath("//*[contains(@id,',7]_c')][not (contains(@id,',7]_c-r'))]");

    private static final By siteNumberInputRowBy = By.xpath("//*[contains(@id,',16]_c')][not (contains(@id,',16]_c-r'))]");

    private static final By storeNumberInputRowBy = By.xpath("//*[contains(@id,',17]_c')][not (contains(@id,',17]_c-r'))]");

    private static final By netPriceInputRowBy = By.xpath("//*[contains(@id,',11]_c')][not (contains(@id,',11]_c-r'))]");

    private static final By returnItemInputRowBy = By.xpath("//span[contains(@id,',23]_c')]/parent::div");

    private static final By returnItemCheckBoxImageBy = By.xpath("//span[contains(@id,',23]_c')]/parent::div//child::img");

    private static final By returnItemCheckBoxSpanBy = By.xpath("//span[contains(@id,',23]_c')]/parent::div/child::span");

    private static final By idocRowTextContainerBy = By.xpath("//div[contains(@lsdata,\"IDoc\")]");

    private static final By oaVendorValueBy = By.xpath("//input[contains(@id, \"[1,1]_c\")]");

    private static final By vnVendorValueBy = By.xpath("//input[contains(@id, \"[2,1]_c\")]");

    private static final By gsVendorValueBy = By.xpath("//input[contains(@id, \"[3,1]_c\")]");

    private static final By oaParentVendorValueBy = By.xpath("//span[contains(@id, \"[1,1]_c\")]");

    private static final By poItemInTableBy = By.xpath("//*[contains(@id,',2]_c') and not (contains(@id,',2]_c-r'))]");

    private static final By purchaseOrderTypeDropDownBy = By.id("M0:U:1:1:2::0:0");

    private static final By secondPurchaseOrderTableArticleBy = By.xpath("//input[contains(@id,\"[2,5]_c\")]");

    private static final By secondPurchaseOrderTableQuantityBy = By.xpath("//input[contains(@id,\"[2,7]_c\")]");

    private static final By firstPOTableDeliveryDateCellBy = By.xpath("//*[contains(@id,\"[1,10]_c\")]");

    private static final By firstPOTableNetPriceCellBy = By.xpath("//*[contains(@id,\"[1,11]_c\")]");

    private static final By firstPOTableCurrencyCellBy = By.xpath("//*[contains(@id,\"[1,12]_c\")]");

    private static final By secondPOTableSiteCellBy = By.xpath("//*[contains(@id,\"[2,16]_c\")]");

    private static final By firstPOTableStorageLocationCellBy = By.xpath("//*[contains(@id,\"[1,17]_c\")]");

    private static final By articleDocumentNumberValidationBy = By.xpath("(//*[contains(@id,'1,3#if')])[2]");

    private static final By goodsReceiptSalesTotalBy = By.xpath("//*[contains(@id,\"7,14#if-r\")]");

    private static final By shortTextBy = By.xpath("//input[contains(@id,\"[2,6]_c\")]");

    private static final By orderUnitBy = By.xpath("//input[contains(@id,\"[2,8]_c\")]");

    private static final By dateCatBy = By.xpath("//input[contains(@id,\"[2,9]_c\")]");

    private static final By netPriceBy = By.xpath("//input[contains(@id,\"[2,11]_c\")]");

    private static final By priceUnitBy = By.xpath("//input[contains(@id,\"[2,13]_c\")]");

    private static final By orderPriceBy = By.xpath("//input[contains(@id,\"[2,14]_c\")]");

    private static final By merchandiseCategoryBy = By.xpath("//input[contains(@id,\"[2,15]_c\")]");

    private static final By purchaseOrderTypeBy = By.xpath("//span[contains(text(),'Personal')]/following::input[1]");

    private static final By amountInLocalCurrencyRowBy = By.xpath("//div[contains(@id,'61_l')]");

    private static final By documentNumberRowBy = By.xpath("//div[contains(@id,':9_l')]");

    public static String randomNumber;
    public static final String JOB_NAME_PART_1 = "F110-";
    private static final By NWBCDeliveryDateBy = By.id("WD68");
    private static final By NWBCSupplyingSiteSTOBy = By.id("WD71");
    private static final By NWBCReicevingSiteSTOBy = By.id("WD7F");
    private static final By NWBCArticleBy = By.id("WDCA");
    private static final By NWBCSTOArticleBy = By.id("WDC2");
    private static final By NWBCDTDArticleBy = By.id("WDD3");
    private static final By NWBCQuantityBy = By.id("WDD8");
    private static final By NWBC_QUANTITYBy = By.id("WDCF");
    private static final By NWBC_STO_QUANTITYBy = By.id("WDC7");
    private static final String articleDocumentPostedRegex = "Article document [0-9]{10} posted";
    private static final String storeMerchPOChangedRegex = "Store Merch PO [0-9]{10} changed";
    private static final String ORDER_ACKNOWLEDGED = "Order Acknowledged";
    private static final String ARTICLE_NUMBER = "Article Number";
    private static final String ASSET_CATEGORY = "Asset Category";
    private static final String ASSET_ID = "Asset ID";
    private static final String ARTICLE_NUMBER_2 = "Article Number 2";
    private static final String QUANTITY = "Quantity";
    private static final String QUANTITY_2 = "Quantity 2";
    private static final String QUANTITY_IN_UNIT_OF_ENTRY = "Quantity in Unit of Entry";
    private static final String DELIVERY_DATE = "Deliv. Date";
    private static final String NET_PRICE = "Net Price";
    private static final String NET_COST = "Net Cost";
    private static final String BOOKING_OK = "Booking OK";
    private static final String RETURNS_ITEM = "Returns Item";
    private static final String CURRENCY = "Currency";
    private static final String STORAGE_LOCATION = "Stor. Location";
    private static final String PO_TYPE = "PO Type";
    private static final String DEFAULT_VALUE = "defaultValue";
    private static final String NWBC_DELIVERY_DATE = "Delivery Date";
    private static final String NWBC_RECEIVING_SITE = "Receiving Site";
    private static final String NWBC_STO_SUPPLYING_SITE = "STO Supplying Site";
    private static final String NWBC_QTY = "NWBC Quantity";
    private static final String NWBC_STO_ARTICLE = "STO Article";
    private static final String NWBC_TRANSFER_GOODS_MOVEMENT_ARTICLE = "Goods Movement Article";
    private static final String NWBC_TRANSFER_GOODS_MOVEMENT_QUANTITY = "Goods Movement Quantity";
    private static final String NWBC_STO_QTY = "STO Quantity";
    private static final String EXPAND_HEADER = "Expand";
    private static final String DETAIL_HEADER = "Open detail";
    private static final String ITEM_DETAIL_HEADER = "Expand Item Details";
    private static final String OTHER_PURCHASE_ORDER = "Other Purchase Order";
    private static final String PURCHASE_ORDER_HISTORY = "Purchase Order History";
    private static final String QUANTITY_TAB = "Quantity";
    private static final String OUTPUT = "Output";
    private static final String DISPLAY_OUTPUT = "Display Output";
    private static final String ZNFY = "ZNFY";
    private static final String PROCESSING_LOG = "Processing Log";
    private static final String CONFIRM_OPTIONAL = "confirm optional";
    private static final String CONFIRMATIONS = "confirmations";
    private static final String DTC_CONFIRM_OPTIONAL = "DTC Confirm Optional";
    private static final String INV_DELIV_ROUGH_GR = "Inv Deliv Rough GR";
    private static final String INBOUND_DELIVERY = "Inbound delivery";
    private static final String ROUGH_GR = "Rough GR";
    private static final String EMPTY = "Empty";
    private static final String SAVE_DOCUMENT = "Save Document";
    private static final String CONFIRMATIONS_TAB = "Confirmations tab";
    private static final String SITE_2 = "Site 2";
    private static final String SUPPLYING_SITE = "Supplying Site/Plant";
    private static final String SITE_IN_STORE_TRANSFER_PURCHASE_ORDER = "Site In Store Transfer";
    private static final String TAX_ME21N = "Tax";
    private static final String TAX_CODE = "Tax Code";
    private static final String SHORT_TEXT = "Short Text";
    private static final String TAX_CODE_IN_MIRO = "Tax Code in PO Reference";
    private static final String PURCHASE_ORDER_INPUT_GR = "Goods Reciept";
    private static final String ITEM_OK = "Item OK";
    private static final String NEXT = "Next";
    private static final String SHORT_TEXT_2 = "short text 2";
    private static final String ORDER_UNIT_2 = "Order unit 2";
    private static final String DATE_CATEGORY_2 = "Date category 2";
    private static final String NET_PRICE_2 = "Net price 2";
    private static final String PRICE_UNIT_2 = "Price unit 2";
    private static final String ORDER_PRICE_2 = "Order price 2";
    private static final String MERCHANDISE_CATEGORY_2 = "Merchandise category 2";
    private static final String NEXT_ARTICLE = "Next article";
    private static final String SUCCESSFUL_MESSAGE = "Successful message";
    private static final String DRIVER_NAME = "Driver Name";
    private static final String RGA_RMA = "RGA/RMA#";
    private static final String BOL_PRO = "BOL/PRO#";
    private static final String ORIGINAL_PO = "Original PO#";
    private static final String METHOD_OF_RETURN = "Method of Return";
    private static final String VENDOR = "Vendor";
    private static final String NEXT_ITEM_BUTTON_IN_MIGO = "Next Item Button In Migo";
    private static final String PAYMENT_METHODS = "Payment methods";
    private static final String COMPANY_CODES = "Company Codes";
    private static final String COMPANY_CODES_IN_PAYMENT_CONTROLS = "Company Codes In Payment Control";
    private static final String PMT_METHS_IN_PAYMENT_CONTROLS = "Pmt meths";
    private static final String MEDIUM_INPUT = "Medium Input";
    private static final String KR_DOCUMENT_NUMBER = "KR Document number";
    private static final String DOCUMENT_NUMBER = "Document Number";
    private static final String GL_ACCOUNT_NUMBER = "G/L Account number";
    private static final String DEBIT_CREDIT = "Debit or Credit";
    private static final String AMOUNT = "Amount";
    private static final String STATUS_TAB = "Status Tab";
    private static final String DELIVERY_COMPLETE_CHECK_BUTTON = "Delivery complete button";
    private static final String DELIVERY_COMPLETE_CHECK_BOX = "Delivery complete check box";
    private static final String POSTING_DATE_FROM_FIELD = "Posting Date From";
    private static final String POSTING_DATE_TO_FIELD = "Posting Date To";
    private static final String PAYMENT_RUN_DATE = "Payment Run Date";
    private static final String NEXT_PDATE = "Next p/date";
    private static final String RFFOUS_C = "RFFOUS_C";
    private static final String PAYMENT_TYPE_IN_F110 = "Payment type in F110";
    private static final String INVOICE_DATE_INPUT_IN_MIRO = "Invoice date";
    private static final String VALUES_FREE_SELECTION = "Values";
    private static final String OPTIONS_BUTTON_FOR_FIELD_NAME = "drop down";
    private static final String CLEARING_DOCUMENT = "Clearing document";
    private static final String IDENTIFICATION = "Identification";
    private static final String DIR_COMM_IN_AL11 = "Second Dir_Comm in Al11";
    private static final String INT_IN_TABLE = "int";
    private static final String O_IN_TABLE = "o";
    private static final String LAST_CHANGED_ON = "Last Changed On";
    private static final String TIME_BEFORE_FILE_CREATION_IN_AL11 = "Time before file creation in Al11";
    private static final String TIME_AFTER_FILE_CREATION_IN_AL11 = "Time after file creation in Al11";
    private static final String AMOUNT_IN_EKBE = "Amount in EKBE";
    private static final String POSTING_DATE_IN_EKBE = "Posting Date in EKBE";
    private static final String ARTICLE_DOCUMENT_NUMBER = "Article Document Number";
    private static final String AMOUNT_IN_MIRO = "Amount in Miro";
    private static final String POSTING_DATE = "Posting Date";
    private static final String DOCUMENT_DATE = "Document Date";
    private static final String V_SEG_CAT_ID = "VSegCatId";
    private static final String VEH_SEG_CAT_ID = "Veh Seg Cat ID";
    private static final String VEHICLE_ID_FROM_VEHLIST = "Vehicle ID From Vehlist";

    /**
     * return the webelement.
     *
     * @param elementName - Name of the Web Element
     * @return - WebElement
     */
    public WebElement returnElementToClick(String elementName) {
        LOGGER.info("returnElementToClick started");
        switch (elementName) {
            case VEHICLE_ID_FROM_VEHLIST:
                return vehicleIDFromVehList;
            case VEH_SEG_CAT_ID:
                return vehSegCatID;
            case V_SEG_CAT_ID:
                return vSegCatId;
            case O_IN_TABLE:
                if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
                    return oInTable2;
                } else {
                    return oInTable;
                }
            case BOOKING_OK:
                if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
                    return bookingOKInputRow2;
                } else {
                    return bookingOKInputRow;
                }
            case DOCUMENT_DATE:
                return documentDate;
            case ARTICLE_DOCUMENT_NUMBER:
                return articleDocumentNumber;
            case POSTING_DATE_IN_EKBE:
                return postingDateInEkbe;
            case AMOUNT_IN_EKBE:
                return amountInEkbe;
            case INT_IN_TABLE:
                if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
                    return intInTable2;
                } else {
                    return intInTable;
                }
            case TIME_AFTER_FILE_CREATION_IN_AL11:
                return entertimeAfterFileCreationInAl11;
            case TIME_BEFORE_FILE_CREATION_IN_AL11:
                return enterTimeBeforeFileCreationInAl11;
            case LAST_CHANGED_ON:
                return lastChangedOnFrom;
            case DIR_COMM_IN_AL11:
                if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
                    return dirComm2InAL11;
                } else {
                    return dirCommInAL11;
                }
                case IDENTIFICATION:
                return identificationCodeInput;
            case DOCUMENT_NUMBER:
                return documentNumber;
            case OPTIONS_BUTTON_FOR_FIELD_NAME:
                return optionsButtonForFieldName;
            case VALUES_FREE_SELECTION:
                return valuesUnderFreeSelection;
            case INVOICE_DATE_INPUT_IN_MIRO:
                return invoiceDateInputInMiro;
            case NEXT_PDATE:
                return  nextPDateInPaymentsControlTable;
            case PAYMENT_RUN_DATE:
                return runDateInF110;
            case DELIVERY_COMPLETE_CHECK_BUTTON:
                return deliveryCompleteCheckButton;
            case DELIVERY_COMPLETE_CHECK_BOX:
                return deliveryCompleteCheckBox;
            case STATUS_TAB:
                return statusTab;
            case KR_DOCUMENT_NUMBER:
                return krDocumentNumber;
            case NET_PRICE:
                return purchaseOrderTableNetPrice;
            case SHORT_TEXT:
                return purchaseOrderTableShortText;
            case MEDIUM_INPUT:
                return mediumInput;
            case PAYMENT_METHODS:
                return paymentMethods;
            case NET_COST:
                return netCost;
            case OTHER_PURCHASE_ORDER:
                return otherPurchaseOrder;
            case PURCHASE_ORDER_HISTORY:
                return purchaseOrderHistory;
            case QUANTITY_TAB:
                return quantityTab;
            case OUTPUT:
                return output;
            case DISPLAY_OUTPUT:
                return displayOutputsButton;
            case ZNFY:
                return znfy;
            case PROCESSING_LOG:
                return processingLog;
            case CONFIRMATIONS_TAB:
                return confirmationTab;
            case NEXT_ARTICLE:
                return nextArticle;
            case SUCCESSFUL_MESSAGE:
                return successfulMessage;
            case METHOD_OF_RETURN:
                return methodOfReturn;
            default:
                Assert.fail("FAIL: Could not find icon that matched string passed from step");
                return null;
        }
    }

    /**
     * return the By locator.
     *
     * @param elementName - Name of the Web Element
     * @return - By
     */
    public By returnByLocator(String elementName) {
        LOGGER.info("returnByLocator started");
        switch (elementName) {
            case COMPANY_CODES:
                return companyCodesFieldBy;
            case ARTICLE_NUMBER:
                return articleNumberInputRowBy;
            case QUANTITY:
                return quantityNumberInputRowBy;
            case Constants.SITE:
                return siteNumberInputRowBy;
            case Constants.STORE:
                return storeNumberInputRowBy;
            case NET_PRICE:
                return netPriceInputRowBy;
            case RETURNS_ITEM:
                return returnItemInputRowBy;
            default:
                Assert.fail("FAIL: Could not find icon that matched string passed from step");
                return null;
        }
    }

    /**
     * Asserts text displayed as page title
     *
     * @param text page title to verify
     */
    public void verifyPageTitle(String text) {
        LOGGER.info("verifyPageTitle started");
        driver.waitForElementVisible(pageTitle);
        String titleString = pageTitle.getText();
        Assert.assertTrue("FAIL: Page title was not found!", titleString.contains(text));
        LOGGER.info("verifyPageTitle completed");
    }

    /**
     * Asserts text displayed in the po dropdown selections
     *
     * @param dropDownDefaultAction expected text of po action dropdown
     * @param dropDownDefaultOrder  expected text of po order type dropdown
     */
    public void verifyDefaultDropDowns(String dropDownDefaultAction, String dropDownDefaultOrder) {
        LOGGER.info("verifyDefaultDropDowns started");
        driver.waitForElementVisible(poActionTypeDropDownInMigo);
        String assertTextAction = poActionTypeDropDownInMigo.getAttribute(Constants.VALUE);
        String assertTestOrder = poOrderTypeDropDownInMigo.getAttribute(Constants.VALUE);
        Assert.assertTrue("FAIL: PO action drop down did not contain expected result " + dropDownDefaultAction,
                assertTextAction.equalsIgnoreCase(dropDownDefaultAction));
        Assert.assertTrue("FAIL: PO order type drop down did not contain expected result " +
                dropDownDefaultOrder, assertTestOrder.equalsIgnoreCase(dropDownDefaultOrder));
        LOGGER.info("verifyDefaultDropDowns completed");
    }

    /**
     * Calls the getPONumber and takes the returned number and sends to the purchase order input.
     */
    public void enterPurchaseOrderNumber() {
        LOGGER.info("enterPurchaseOrderNumber started");
        driver.waitForElementVisible(poOrderInputInMigo);
        String purchaseOrderNumber = getPONumber();
        poOrderInputInMigo.clear();
        driver.waitForMilliseconds();
        poOrderInputInMigo.sendKeys(purchaseOrderNumber, Keys.ENTER);
        driver.waitForPageToLoad();
        LOGGER.info("enterPurchaseOrderNumber completed");
    }

    /**
     * Gets scenario data for the purchase order.
     *
     * @return string value of purchase order number in scenario data.
     */
    public String getPONumber() {
        LOGGER.info("getPONumber started");
        String purchaseOrderNumber;
        try {
            return purchaseOrderNumber = driver.scenarioData.getCurrentPurchaseOrderNumber();
        } catch (Exception e) {
            Assert.fail("FAIL: Was unable to get purchase order number from excel");
        }
        LOGGER.info("getPONumber completed");
        return null;
    }

    /**
     * Enter purchase order number with brand
     */
    public void enterPurchaseOrderNumberWithBrand(String brand) {
        LOGGER.info("enterPurchaseOrderNumberWithBrand started = " + brand);
        String purchaseOrderNumber = null;
        try {
            driver.waitForElementVisible(poOrderInputInMigo);
            purchaseOrderNumber = driver.scenarioData.getPOVendorData(brand);
            poOrderInputInMigo.click();
            poOrderInputInMigo.clear();
            poOrderInputInMigo.sendKeys(purchaseOrderNumber, Keys.ENTER);
            driver.waitForPageToLoad();
        } catch (Exception e) {
            Assert.fail("FAIL: Was unable to enter purchase order number");
        }
        LOGGER.info("enterPurchaseOrderNumberWithBrand completed");
    }

    /**
     * Gets scenario data for article document number.
     *
     * @return string value of article document number in scenario data.
     */
    public String getArticleDocumentNumber() {
        LOGGER.info("getArticleDocumentNumber started");
        String articleDocumentNumber;
        try {
            return articleDocumentNumber = driver.scenarioData.getCurrentArticleDocumentNumber();
        } catch (Exception e) {
            Assert.fail("FAIL: Was unable to get article document number from scenario data");
        }
        LOGGER.info("getArticleDocumentNumber completed");
        return null;
    }

    /**
     * Gets scenario data for sales total.
     *
     * @return string value of sales total in scenario data.
     */
    public String getSalesTotalNumber() {
        LOGGER.info("getSalesTotalNumber started");
        String salesTotal = null;
        try {
            return salesTotal = driver.scenarioData.getCurrentTransactionTotal();
        } catch (Exception e) {
            Assert.fail("FAIL: Was unable to get sales total from scenario data");
        }
        LOGGER.info("getSalesTotalNumber completed");
        return null;
    }

    /**
     * Gets a list of all articles in the purchase order and loops through filling our confirmation header and order
     * acknowledgement inputs and checkbox.
     */
    public void fillConfirmationHeaderAndOrderAcknowledgement() {
        LOGGER.info("fillConfirmationHeaderAndOrderAcknowledgement started");
        driver.waitForElementVisible(poItemListTable);

        int poItemListSize = poItemListTable.findElements(poItemInTableBy).size();

        for (int i = 0; i < poItemListSize; i++) {
            List<WebElement> poItemList = poItemListTable.findElements(poItemInTableBy);
            WebElement poItem = poItemList.get(i);
            poItem.click();
            poItem.sendKeys(Keys.ENTER);
            driver.waitForMilliseconds();
            acknowledgementInput.click();
            acknowledgementInput.sendKeys(ORDER_ACKNOWLEDGED);
            acknowledgementCheckbox.click();
            confirmationDropDownContainer.click();
            dtcConfirmationOptionalSelection.click();
            driver.waitForMilliseconds();
        }
        LOGGER.info("fillConfirmationHeaderAndOrderAcknowledgement completed");
    }

    /**
     * Asserts the alert banner contains expected text.
     */
    public void verifyArticleDocumentPosted() {
        LOGGER.info("verifyArticleDocumentPosted started");
        driver.waitForElementVisible(alertBanner);
        Assert.assertTrue("FAIL: did not find the alert banner's expected text!",
                alertBanner.getText().matches(articleDocumentPostedRegex));
        LOGGER.info("verifyArticleDocumentPosted completed");
    }

    /**
     * Takes the article document number and saves to scenario data.
     */
    public void saveArticleDocumentNumberToScenarioData() {
        LOGGER.info("saveArticleDocumentNumberToScenarioData started");
        driver.waitForElementVisible(alertBanner);
        String cleanedArticleDocumentNumber = alertBanner.getText().replaceAll("[^\\d]", "");
        driver.scenarioData.setArticleDocumentNumber(cleanedArticleDocumentNumber);
        LOGGER.info("saveArticleDocumentNumberToScenarioData completed");
    }

    /**
     * Asserts the alert banner contains the expected text
     */
    public void verifyStoreMerchPOChanged() {
        LOGGER.info("verifyStoreMerchPOChanged started");
        Assert.assertTrue("FAIL: did not find the alert banner's expected text!",
                alertBanner.getText().matches(storeMerchPOChangedRegex));
        LOGGER.info("verifyStoreMerchPOChanged completed");
    }

    /**
     * Sends keyboard shortcut to application background to open the 'Other Purchase Order' search popup.
     */
    public void sendOtherPOShortcut() {
        LOGGER.info("sendOtherPOShortcut started");
        driver.waitForElementVisible(poItemInTableBy);
        applicationBackground.sendKeys(Keys.chord(Keys.SHIFT, Keys.F5));
        LOGGER.info("sendOtherPOShortcut completed");
    }

    /**
     * Sends keyboard shortcut to open the 'Item Details' search .
     */
    public void collapseShortcut() {
        LOGGER.info("collapseShortcut started");
        driver.waitForElementVisible(scrollBarNextButton);
        applicationBackground.sendKeys(Keys.chord(Keys.SHIFT, Keys.F7));
        LOGGER.info("collapseShortcut completed");
    }

    /**
     * Takes passed in purchase order value and sends to the purchase order input on 'Other Purhcase Order' popup.
     *
     * @param purchaseOrder string of purchase order number gathered at step level from getPONumber method.
     */
    public void enterPurchaseOrderNumberInPopupSearch(String purchaseOrder) {
        LOGGER.info("enterPurchaseOrderNumberInPopupSearch started");
        driver.waitForElementVisible(purchaseOrderPopupInput);
        purchaseOrderPopupInput.click();
        purchaseOrderPopupInput.clear();
        purchaseOrderPopupInput.sendKeys(purchaseOrder);
        LOGGER.info("enterPurchaseOrderNumberInPopupSearch completed");
    }

    /**
     * Searches the purchase order passed to it by clicking on other purchase order icon - switches to the iFrame
     *
     * @param purchaseOrder string of purchase order number
     */
    public void searchOtherPurchaseOrderNumber(String purchaseOrder) {
        LOGGER.info("searchOtherPurchaseOrderNumber started");
        driver.waitForPageToLoad();
        commonActions.switchFrameContext(Constants.DEFAULT_CONTENT);
        commonActions.switchFrameContext(0);
        commonActions.sendKeys(purchaseOrderInput, purchaseOrder);
        LOGGER.info("searchOtherPurchaseOrderNumber completed");
    }

    /**
     * Selects the target field and sends the string value to fill it
     *
     * @param field input to have value set
     * @param value value sent as string to be input into field
     */
    public void enterValueIntoPOField(String field, String value) {
        LOGGER.info("enterValueIntoPOField started");
        WebElement input = getPOInputField(field);
        commonActions.sendKeys(input, value);
        if (field.equalsIgnoreCase("vendor")) {
            for (int i = 0; i < 3; i++) {
                driver.waitForPageToLoad();
                driver.performKeyAction(Keys.ENTER);
            }
            driver.waitForPageToLoad();
        } else {
            driver.performKeyAction(Keys.ENTER);
        }
        LOGGER.info("enterValueIntoPOField completed");
    }

    /**
     * Enters values into the items table in any PO related page
     *
     * @param fieldName - Name of the column
     * @param value     - value to enter
     * @param rowNumber - row number for entry
     */
    public void enterValuesIntoItemTableForPoCreation(String fieldName, String value, int rowNumber) {
        LOGGER.info("enterValuesIntoItemTableForPoCreation started");
        WebElement inputField = webDriver.findElements(returnByLocator(fieldName)).get(rowNumber - 1);
        try {
            driver.waitForPageToLoad();
            driver.performSendKeysWithActions(inputField, value);
        } catch (StaleElementReferenceException s) {
            WebElement staleInputField = webDriver.findElements(returnByLocator(fieldName)).get(rowNumber - 1);
            driver.performSendKeysWithActions(staleInputField, value);
        }
        driver.performKeyAction(Keys.ENTER);
        LOGGER.info("enterValuesIntoItemTableForPoCreation completed");
    }

    /**
     * click on element in po creation
     *
     * @param fieldName - Name of the column
     * @param rowNumber - row number for entry
     */
    public void clickElementOnItemTableForPoCreation(String fieldName, int rowNumber) {
        LOGGER.info("clickElementOnItemTableForPoCreation started");
        WebElement inputField = webDriver.findElements(returnByLocator(fieldName)).get(rowNumber - 1);
        int count = 0;
        boolean isChecked = true;
        String attribute;
        WebElement attributeElement;
        try {
            driver.moveToElementClick(inputField);
        } catch (StaleElementReferenceException s) {
            WebElement staleInputField = webDriver.findElements(returnByLocator(fieldName)).get(rowNumber - 1);
            driver.moveToElementClick(staleInputField);
        }
        if (fieldName.equalsIgnoreCase(RETURNS_ITEM)) {
            while (isChecked) {
                if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
                    attributeElement = webDriver.findElements(returnItemCheckBoxSpanBy).get(rowNumber - 1);
                    attribute = "unchecked";
                } else {
                    attributeElement = webDriver.findElements(returnItemCheckBoxImageBy).get(rowNumber - 1);
                    attribute = "urCImgOff";
                }
                if (!checkForCheckBox(attributeElement, attribute)) {
                    commonActions.click(inputField);
                    count++;
                    LOGGER.info("count: " + count);
                } else {
                    isChecked = false;
                }
            }
            LOGGER.info("clickElementOnItemTableForPoCreation completed");
        }
    }

    private boolean checkForCheckBox(WebElement element,String attribute) {
        return !element.getAttribute(Constants.CLASS).contains(attribute);
    }


    /**
     * Returns a specific input field for purchase order creation.
     *
     * @param field string used to match correct input element
     * @return WebElement returned for input
     */
    public WebElement getPOInputField(String field) {
        LOGGER.info(" getPOInputField started");
        switch (field) {
            case POSTING_DATE:
                return postingDate;
            case AMOUNT_IN_MIRO:
                return amountInMiro;
            case CLEARING_DOCUMENT:
                return clearingDocumentNumber;
            case PAYMENT_TYPE_IN_F110:
                return paymentTypeInF110;
            case RFFOUS_C:
                return rffous_c;
            case PMT_METHS_IN_PAYMENT_CONTROLS:
                return pmtMethsFieldInPaymentsControlTable;
            case COMPANY_CODES_IN_PAYMENT_CONTROLS:
                return companyCodesFieldInPaymentsControlTable;
            case POSTING_DATE_FROM_FIELD:
                return postingDateFromField;
            case POSTING_DATE_TO_FIELD:
                return postingDateToField;
            case SITE_IN_STORE_TRANSFER_PURCHASE_ORDER:
                return siteInStoreTransferPurchaseOrder;
            case SUPPLYING_SITE:
                if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
                    return supplyingPlant;
                } else {
                    return supplyingSite;
                }
            case TAX_CODE:
                return taxCodeInInvoiceMe21n;
            case AMOUNT:
                return amount;
            case GL_ACCOUNT_NUMBER:
                return glAccountNumber;
            case DEBIT_CREDIT:
                return debitOrCredit;
            case SHORT_TEXT:
                return purchaseOrderTableShortText;
            case TAX_CODE_IN_MIRO:
                return purchaseOrderTaxCode;
            case ASSET_ID:
                return purchaseOrderTableAssetID;
            case ASSET_CATEGORY:
                return purchaseOrderTableAssetCategory;
            case ARTICLE_NUMBER:
                return purchaseOrderTableArticle;
            case QUANTITY:
                return purchaseOrderTableQuantity;
            case QUANTITY_2:
                return webDriver.findElement(secondPurchaseOrderTableQuantityBy);
            case DELIVERY_DATE:
                return webDriver.findElements(firstPOTableDeliveryDateCellBy).get(1);
            case NET_PRICE:
                return purchaseOrderTableNetPrice;
            case CURRENCY:
                return webDriver.findElements(firstPOTableCurrencyCellBy).get(1);
            case Constants.SITE:
                return siteOrPlant;
            case STORAGE_LOCATION:
                return webDriver.findElements(firstPOTableStorageLocationCellBy).get(1);
            case PO_TYPE:
                return webDriver.findElement(purchaseOrderTypeBy);
            case NWBC_DELIVERY_DATE:
                return webDriver.findElement(NWBCDeliveryDateBy);
            case NWBC_RECEIVING_SITE:
                return webDriver.findElement(NWBCReicevingSiteSTOBy);
            case NWBC_STO_SUPPLYING_SITE:
                return webDriver.findElement(NWBCSupplyingSiteSTOBy);
            case Constants.ARTICLE:
                return webDriver.findElement(NWBCArticleBy);
            case NWBC_TRANSFER_GOODS_MOVEMENT_ARTICLE:
                return webDriver.findElement(NWBCDTDArticleBy);
            case NWBC_TRANSFER_GOODS_MOVEMENT_QUANTITY:
                return webDriver.findElement(NWBCQuantityBy);
            case NWBC_STO_ARTICLE:
                return webDriver.findElement(NWBCSTOArticleBy);
            case NWBC_QTY:
                return webDriver.findElement(NWBC_QUANTITYBy);
            case NWBC_STO_QTY:
                return webDriver.findElement(NWBC_STO_QUANTITYBy);
            case QUANTITY_IN_UNIT_OF_ENTRY:
                return quantityInUnitOfEntry;
            case ARTICLE_NUMBER_2:
                return webDriver.findElement(secondPurchaseOrderTableArticleBy);
            case SITE_2:
                return webDriver.findElement(secondPOTableSiteCellBy);
            case TAX_ME21N:
                return taxFieldInMe21n;
            case PURCHASE_ORDER_INPUT_GR:
                return poOrderInputInMigo;
            case ITEM_OK:
                return itemOk;
            case SHORT_TEXT_2:
                return webDriver.findElement(shortTextBy);
            case ORDER_UNIT_2:
                return webDriver.findElement(orderUnitBy);
            case DATE_CATEGORY_2:
                return webDriver.findElement(dateCatBy);
            case NET_PRICE_2:
                return webDriver.findElement(netPriceBy);
            case PRICE_UNIT_2:
                return webDriver.findElement(priceUnitBy);
            case ORDER_PRICE_2:
                return webDriver.findElement(orderPriceBy);
            case MERCHANDISE_CATEGORY_2:
                return webDriver.findElement(merchandiseCategoryBy);
            case DRIVER_NAME:
                return driverName;
            case RGA_RMA:
                return rgaRma;
            case BOL_PRO:
                return bolPro;
            case ORIGINAL_PO:
                return originalPo;
            case METHOD_OF_RETURN:
                return methodOfReturn;
            case VENDOR:
                return commonActions.returnElementWithLabelName(VENDOR);
            case NEXT_ITEM_BUTTON_IN_MIGO:
                return nextItemButtonInMigo;
            case ZNFY:
                return znfy;
            default:
                return null;
        }
    }

    /**
     * saves purchase order number to scenario data memory
     *
     * @param purchaseOrderNumber string number value of purchase order number
     */
    public void savePurchaseOrderNumberToScenarioData(String purchaseOrderNumber) {
        LOGGER.info("savePurchaseOrderNumberToScenarioData started");
        driver.scenarioData.setCurrentOrderNumber(purchaseOrderNumber);
        LOGGER.info("savePurchaseOrderNumberToScenarioData completed");
    }

    /**
     * Sends string value to Purchase Order Type dropdown
     *
     * @param poType String value of PO type
     */
    public void inputPOTypeByText(String poType) {
        LOGGER.info("inputPOTypeByText started");
        inputPoType(poType);
        while(!webDriver.findElement(purchaseOrderTypeBy).getAttribute(Constants.VALUE).equalsIgnoreCase(poType)){
            inputPoType(poType);
        }
        LOGGER.info("inputPOTypeByText completed");
    }

    private void inputPoType(String poType) {
        driver.clickInputAndSendKeys(webDriver.findElement(purchaseOrderTypeBy),poType);
        driver.performKeyAction(Keys.ENTER);
        driver.waitForMilliseconds();
    }

    /**
     * Following test case flow, the previously created purchase order number should default when going to ME23N, as a
     * result we are verifying that this is indeed the default.
     *
     * @param purchaseOrderNumber previously created purchase order number saved to scenario data passed at step
     *                            definition level
     */
    public void orderReceiptValidationPOSearchDefault(String purchaseOrderNumber) {
        LOGGER.info("orderReceiptValidationPOSearchDefault started");
        driver.waitForElementVisible(receiptValidationPOInput);
        String foundPurchaseOrderNumber = receiptValidationPOInput.getAttribute(DEFAULT_VALUE);
        Assert.assertTrue("FAIL: The purchase order input of " + foundPurchaseOrderNumber +
                        " did not default to the saved purchase order number of " + purchaseOrderNumber,
                foundPurchaseOrderNumber.equalsIgnoreCase(purchaseOrderNumber));
        LOGGER.info("orderReceiptValidationPOSearchDefault completed");
    }

    /**
     * Selects the purchase order history tab in ME23N
     */
    public void selectPurchaseOrderHistoryTab() {
        LOGGER.info("selectPurchaseOrderHistoryTab started");
        Boolean isPresent = driver.isElementDisplayed(tabLeftArrowDetail);
        if (isPresent) {
            tabLeftArrowDetail.click();
        }
        driver.waitForElementClickable(purchaseOrderHistoryTab);
        purchaseOrderHistoryTab.click();
        LOGGER.info("selectPurchaseOrderHistoryTab completed");
    }

    /**
     * Gets the most recent article document saved to scenario data and compares to the found article document number.
     *
     * @param articleDocumentNumber Number grabbed at step definition level pulled from Scenario data
     */
    public void verifyArticleDocumentMatchesSavedNumber(String articleDocumentNumber) {
        LOGGER.info("verifyArticleDocumentMatchesSavedNumber started");
        driver.waitForElementVisible(articleDocumentNumberValidationBy);
        try {
            String articleDocumentFound = webDriver.findElement(articleDocumentNumberValidationBy).getText();
            Assert.assertTrue("FAIL: Article Document number " + articleDocumentFound +
                            " did not match expected value of " + articleDocumentNumber,
                    articleDocumentFound.equalsIgnoreCase(articleDocumentNumber));
        } catch (AssertionError ae) {
            String articleDocumentFound = webDriver.findElement(articleDocumentNumberValidationBy).getAttribute(Constants.VALUE);
            Assert.assertTrue("FAIL: Article Document number " + articleDocumentFound +
                            " did not match expected value of " + articleDocumentNumber,
                    articleDocumentFound.equalsIgnoreCase(articleDocumentNumber));
        }
        LOGGER.info("verifyArticleDocumentMatchesSavedNumber completed");
    }

    /**
     * opens item overview header if closed on RTV page
     */
    public void itemOverviewHeaderCollapse() {
        LOGGER.info("itemOverviewHeaderCollapse started");
        Actions actions = new Actions(webDriver);
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        actions.keyDown(Keys.CONTROL).sendKeys(Keys.F3).build().perform();
        actions.keyUp(Keys.CONTROL).build().perform();
        LOGGER.info("itemOverviewHeaderCollapse completed");
    }

    /**
     * opens header if closed
     */
    public void headerExpand() {
        LOGGER.info("HeaderExpand started");
        Actions actions = new Actions(webDriver);
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        actions.keyDown(Keys.CONTROL).sendKeys(Keys.F2).build().perform();
        actions.keyUp(Keys.CONTROL).build().perform();
        LOGGER.info("HeaderExpand completed");
    }

    /**
     * collapse header if needed
     */
    public void headerCollapse() {
        LOGGER.info("HeaderCollapse started");
        Actions actions = new Actions(webDriver);
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        actions.keyDown(Keys.CONTROL).sendKeys(Keys.F5).build().perform();
        actions.keyUp(Keys.CONTROL).build().perform();
        LOGGER.info("HeaderCollapse completed");
    }

    /**
     * opens item details if closed
     */
    public void itemDetailsExpand() {
        LOGGER.info("itemDetailsExpand started");
        Actions actions = new Actions(webDriver);
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        actions.keyDown(Keys.CONTROL).sendKeys(Keys.F4).build().perform();
        actions.keyUp(Keys.CONTROL).build().perform();
        LOGGER.info("itemDetailsExpand completed");
    }

    /**
     * Clicks on the article document number to pull up document information.
     */
    public void selectArticleDocumentNumber() {
        LOGGER.info("selectArticleDocumentNumber started");
        driver.waitForElementVisible(articleDocumentNumberValidationBy);
        WebElement articleDocument = commonActions.returnElementWithPartialText(driver.scenarioData.getCurrentArticleDocumentNumber());
        articleDocument.click();
        driver.waitForMilliseconds(Constants.FIVE_THOUSAND);
        LOGGER.info("selectArticleDocumentNumber completed");
    }

    /**
     * Gets the previously saved transaction total and compares it to the found sales total.
     *
     * @param salesTotal saved transaction total grabbed at step definition level
     */
    public void verifyGoodsReceiptSaleTotal(String salesTotal) {
        LOGGER.info("verifyGoodsReceiptSaleTotal started");
        driver.waitForElementVisible(goodsReceiptSalesTotalBy);
        String salesTotalFound = webDriver.findElement(goodsReceiptSalesTotalBy).findElement(CommonActions.inputTagBy)
                .getAttribute(Constants.VALUE);
        Assert.assertTrue("FAIL: Sales total found " + salesTotalFound + " did not match expected value of " +
                salesTotal, salesTotalFound.equalsIgnoreCase(salesTotal));
        LOGGER.info("verifyGoodsReceiptSaleTotal completed");
    }

    /**
     * Verifies partner function line under partner tab has the correct value
     *
     * @param value String representing text value expected to be present in each row
     */
    public void assertPartnerFunctionValue(String value) {
        LOGGER.info("assertPartnerFunctionValue started");
        WebElement expectedValueEl;
        String expectedValue = "";

        if (value.equalsIgnoreCase("OA")) {
            driver.waitForMilliseconds(Constants.ONE_THOUSAND);
            expectedValue = webDriver.findElement(oaParentVendorValueBy).findElement(oaVendorValueBy)
                    .getAttribute(Constants.VALUE);
        } else if (value.equalsIgnoreCase("VN")) {
            expectedValueEl = webDriver.findElement(vnVendorValueBy);
            expectedValue = expectedValueEl.getAttribute(Constants.VALUE);
        } else if (value.equalsIgnoreCase("GS")) {
            expectedValueEl = webDriver.findElement(gsVendorValueBy);
            expectedValue = expectedValueEl.getAttribute(Constants.VALUE);
        } else {
            Assert.fail("FAIL: itemType of:\"" + value + "\" was NOT recognized!");
        }
        Assert.assertTrue("Value displayed " + expectedValue + " is not equal to expected " + value,
                value.equals(expectedValue));
        LOGGER.info("assertPartnerFunctionValue completed");
    }

    /**
     * Asserts address displayed matches vendor address
     */
    public void verifyVendorAddress() {
        LOGGER.info("verifyVendorAddress started");
        driver.waitForPageToLoad();
        String streetNumberVal = streetNumber.getAttribute(Constants.VALUE);
        String streetAddressVal = streetAddress.getAttribute(Constants.VALUE);
        String CityVal = city.getAttribute(Constants.VALUE);
        String zipCodeVal = zipCode.getAttribute(Constants.VALUE);
        String vendorAddress = streetNumberVal + " " + streetAddressVal + "," + " " + CityVal + "," + " " + zipCodeVal;
        Assert.assertEquals("FAIL: Vendor Address displayed " + vendorAddress + " is not equal to expected " +
                Constants.AE_VENDOR_STORE_ADDRESS, Constants.AE_VENDOR_STORE_ADDRESS.equals(vendorAddress));
        LOGGER.info("verifyVendorAddress completed");
    }

    /**
     * Verifies PO header line item
     *
     * @param value      String representing text value expected to be present in each row
     * @param poLineItem Partner Function abbreviation for each row
     */
    public void assertPoLineItemValue(String value, String poLineItem) {
        LOGGER.info("assertPoLineItemValue started");
        driver.waitForPageToLoad();
        WebElement expectedValueEl;
        String expectedValue;
        expectedValueEl = getPOInputField(poLineItem);
        expectedValue = expectedValueEl.getAttribute(Constants.VALUE);

        if (expectedValueEl != null) {
            Assert.assertTrue("Value displayed " + expectedValue + " is not equal to expected " + value,
                    value.equals(expectedValue));
        } else {
            Assert.fail("FAIL: itemType of:\"" + poLineItem + "\" was NOT recognized!");
        }
        LOGGER.info("assertPoLineItemValue completed");
    }

    /**
     * Selects the "Outputs" tab of the article document info page, then selects the "Display Outputs" button.
     */
    public void selectDisplayOutputsOnOutputsTab() {
        LOGGER.info("selectDisplayOutputsOnOutputsTab started");
        driver.waitForElementClickable(outputsTab);
        outputsTab.click();
        driver.waitForElementClickable(displayOutputsButton);
        displayOutputsButton.click();
        LOGGER.info("selectDisplayOutputsOnOutputsTab completed");
    }

    /**
     * Selects the email process code output row, then selects the processing log.
     */
    public void selectEmailLogOutputRowHeader() {
        LOGGER.info("selectEmailLogOutputRowHeader started");
        driver.waitForElementClickable(articleDocumentOutputTableHeaderContainer);
        purchaseOrderTableHeaderEmailOutputSelector.click();
        driver.waitForElementClickable(processingLogTab);
        processingLogTab.click();
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        LOGGER.info("selectEmailLogOutputRowHeader completed");
    }

    /**
     * Grabs the IDoc number from the popup window text container.
     */
    public void extractIDocEmailTriggerNumber() {
        LOGGER.info("extractIDocEmailTriggerNumber started");
        String IDocNumber = webDriver.findElement(idocRowTextContainerBy).getText()
                .replaceAll("[^\\d]", "");
        driver.scenarioData.setCurrentIDocNumber(IDocNumber);
        LOGGER.info("IDoc Number: " + driver.scenarioData.getCurrentIDocNumber());
        LOGGER.info("extractIDocEmailTriggerNumber completed");
    }

    /**
     * Verifies previously created web order number matches your reference field
     */
    public void verifyReferenceWebOrder() {
        LOGGER.info("verifyReferenceWebOrder started");
        String webOrderNum = driver.scenarioData.getCurrentOrderNumber();
        String referenceNum = poYourReference.getAttribute(Constants.VALUE);
        Assert.assertTrue("FAIL: Reference number found " + referenceNum + " did not match expected value of " +
                webOrderNum, referenceNum.equalsIgnoreCase(webOrderNum));
        LOGGER.info("verifyReferenceWebOrder completed");
    }

    /**
     * checks if the processing log for ZNFY contains idoc not generated.
     */
    public void verifyZnfyProcessingLog() {
        LOGGER.info("verifyZnfyProcessingLog started");
        driver.waitForElementVisible(znfyStatus);
        Assert.assertTrue("FAIL: Not Fully Recieved - message did not appear", znfyStatus.getText().contains(Constants.NOT_YET_FULLY_RECEIVED));
        LOGGER.info("verifyZnfyProcessingLog completed");
    }

    /**
     * Calls returnElementToClick with string to find Element in POPage, then clicks on the returned result
     *
     * @param element String passed in a step level of Element to click. Valid options listed as strings variables at top
     */
    public void clickReturnedElementOnPurchaseOrderPage(String element) {
        LOGGER.info("clickReturnedElementOnPurchaseOrderPage started");
        driver.waitForPageToLoad();
        commonActions.click(returnElementToClick(element));
        LOGGER.info("clickReturnedElementOnPurchaseOrderPage completed");
    }

    /**
     * Clicks on the Header button WebElement.
     */
    public void clickOnHeaderButton() {
        LOGGER.info("clickOnHeaderButton started");
        driver.waitForElementClickable(buttonStatus);
        if (buttonStatus.getAttribute(Constants.TITLE).equalsIgnoreCase(EXPAND_HEADER)) {
            buttonStatus.click();
        }
        LOGGER.info("clickOnHeaderButton completed");
    }

    /**
     * opens the Detail data header in purchase order page if it is collapsed
     */
    public void detailDataHeaderCollapse() {
        LOGGER.info("detailDataHeaderCollapse started");
        if (commonActions.titleAttributeContains(detailDataHeader, DETAIL_HEADER)) {
            detailDataHeader.click();
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        }
        LOGGER.info("detailDataHeaderCollapse completed");
    }

    /**
     * opens the head Data header in purchase order page if it is collapsed
     */
    public void headDataHeaderCollapse() {
        LOGGER.info("headDataHeaderCollapse started");
        if (commonActions.titleAttributeContains(headData, EXPAND_HEADER)) {
            headData.click();
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        }
        LOGGER.info("headDataHeaderCollapse completed");
    }

    /**
     * opens the Item detail header in purchase order page if it is collapsed
     */
    public void itemDetailHeaderCollapse() {
        LOGGER.info("itemDetailHeaderCollapse started");
        if (commonActions.titleAttributeContains(itemDetail, ITEM_DETAIL_HEADER)) {
            itemDetail.click();
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        }
        LOGGER.info("itemDetailHeaderCollapse completed");
    }

    /**
     * Selects confirmation choice drop down based on user's choice
     *
     * @param choiceName = Confirmaiton choice type
     */
    public void choiceOfConfirmation(String choiceName) {
        LOGGER.info("choiceOfConfirmation started");
        commonActions.click(confirmationDropDown);
        switch (choiceName) {
            case CONFIRM_OPTIONAL:
                commonActions.click(confirmOptionalChoice);
                break;
            case CONFIRMATIONS:
                commonActions.click(confirmationsChoice);
                break;
            case DTC_CONFIRM_OPTIONAL:
                commonActions.click(dtcConfirmOptionalChoice);
                break;
            case EMPTY:
                commonActions.click(noChoice);
                break;
            case INV_DELIV_ROUGH_GR:
                commonActions.click(delivRoughGrChoice);
                break;
            case INBOUND_DELIVERY:
                commonActions.click(inboundDeliveryChoice);
                break;
            case ROUGH_GR:
                commonActions.click(roughGrChoice);
                break;
            default:
                Assert.fail("FAIL: Wrong selection of choice");
        }
        LOGGER.info("choiceOfConfirmation completed");
    }

    /**
     * Confirms to save document popup based on "Changes" or "no changes" made
     */
    public void saveDocument() {
        LOGGER.info("saveDocument started");
        if (popupWindowTitle.getText().equalsIgnoreCase(SAVE_DOCUMENT)) {
            commonActions.click(popupWindowSaveButton);
        } else {
            commonActions.click(popupWindowTickButton);
        }
        LOGGER.info("saveDocument completed");
    }

    /**
     * Verifies the value attribute of Individual WebElements in ME21N
     *
     * @param elementName - send in elementName and gets the WebELement from getPOInputField method.
     * @param validation  - value to verify
     */
    public void verifyValueAttributeInMe21n(String elementName, String validation) {
        LOGGER.info("verifyValueAttributeInMe21n started");
        driver.assertElementAttributeString(getPOInputField(elementName), Constants.VALUE, validation);
        LOGGER.info("verifyValueAttributeInMe21n completed");
    }

    /**
     * Click method for elements in Purchase Order Creation pages
     *
     * @param elementName - send in elementName and gets the WebELement from getPOInputField method.
     */
    public void clickInPoPages(String elementName) {
        LOGGER.info("clickInPoPages started with element name " + elementName);
        commonActions.click(getPOInputField(elementName));
        LOGGER.info("clickInPoPages completed with element name" + elementName);
    }

    /**
     * This method will click on the confirmation tab. If the confirmation tab in me21n is not visible it will click on the right indicator and then click on confirmation tab
     */
    public void clickOnConfirmationTabInMe21n() {
        LOGGER.info("clickOnConfirmationTabInMe21n started");
        if (confirmationsTabInMe21n.isDisplayed()) {
            commonActions.click(confirmationsTabInMe21n);
        } else {
            commonActions.click(rightIndicatorInMe21n);
            driver.waitForPageToLoad();
            commonActions.click(confirmationsTabInMe21n);
        }
        LOGGER.info("clickOnConfirmationTabInMe21n completed");
    }

    /**
     * method to read multiple values from excel under different columns
     *
     * @param articleNumber - takes in columnName: Article Number and looks up for the same in excel to return value
     * @param quantity - takes in columnName: Quantity and looks up for the same in excel to return value
     * @param site - takes in columnName: Site and looks up for the same in excel to return value
     */
    public void enterMultipleValuesInExcel(String articleNumber, String quantity, String site) throws IOException {
        LOGGER.info("enterMultipleValuesInExcel started.");
        int n = commonActions.getNumberOfRows(Constants.SAP_DATA_SOURCE_LOCATION +
                        driver.scenarioData.getData(CommonActions.FOLDER_NAME) + "\\" +
                        driver.scenarioData.getData(CommonActions.EXCEL_FILE_NAME),
                driver.scenarioData.getData(CommonActions.SHEET_NAME_1), "Article Number");
        int rowNumber = 1;
        for (int i = 1; i <= n; i++) {
            if (i == 1) {
                commonActions.getExcelDataByColumn(Constants.SAP_DATA_SOURCE_LOCATION +
                                driver.scenarioData.getData(CommonActions.FOLDER_NAME) + "\\" +
                                driver.scenarioData.getData(CommonActions.EXCEL_FILE_NAME),
                        driver.scenarioData.getData(CommonActions.SHEET_NAME_1), articleNumber);
                commonActions.getExcelDataByColumn(Constants.SAP_DATA_SOURCE_LOCATION +
                                driver.scenarioData.getData(CommonActions.FOLDER_NAME) + "\\" +
                                driver.scenarioData.getData(CommonActions.EXCEL_FILE_NAME),
                        driver.scenarioData.getData(CommonActions.SHEET_NAME_1), quantity);
                commonActions.getExcelDataByColumn(Constants.SAP_DATA_SOURCE_LOCATION +
                                driver.scenarioData.getData(CommonActions.FOLDER_NAME) + "\\" +
                                driver.scenarioData.getData(CommonActions.EXCEL_FILE_NAME),
                        driver.scenarioData.getData(CommonActions.SHEET_NAME_1), site);
            }
            enterValuesIntoItemTableForPoCreation(articleNumber, driver.scenarioData.getData(articleNumber + i), rowNumber);
            enterValuesIntoItemTableForPoCreation(quantity, driver.scenarioData.getData(quantity + i), rowNumber);
            enterValuesIntoItemTableForPoCreation(site, driver.scenarioData.getData(site + i), rowNumber);
            if (i == 1) {
                rowNumber = 2;
            }
            if (n >= 4) {
                driver.waitSeconds(Constants.FIVE);
                commonActions.collapseItemDetails();
                commonActions.click(scrollBarNextButton);
            }
        }
        LOGGER.info("enterMultipleValuesInExcel completed.");
    }

    /**
     * This method is written to enter the asset id based on which environment the test case is running in.
     */
    public void setAssetId() {
        LOGGER.info("setAssetId started");
        String assetId = "";
        if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            assetId = Constants.ASSET_ID_IN_RTQ;
        } else {
            assetId = Constants.ASSET_ID_IN_RTK;
        }
        commonActions.sendKeys(purchaseOrderTableAssetID, assetId);
        LOGGER.info("setAssetId completed");
    }

    /**
     * This method is used to save any value in procure to pay
     *
     * @param elementName = net cost value
     */
    public void saveValueFromPage(String elementName) {
        LOGGER.info("saveValueFromPage started");
        driver.waitForPageToLoad();
        if (returnElementToClick(elementName).getText().isEmpty()) {
            driver.scenarioData.setData(elementName, returnElementToClick(elementName).getAttribute(Constants.VALUE));
        } else {
            driver.scenarioData.setData(elementName, returnElementToClick(elementName).getText());
        }
        LOGGER.info(" saveValueFromPage completed. SavedValue of " + elementName + " = " +
                returnElementToClick(elementName).getText());
    }

    /**
     * This method is used to enter saved value in procure to pay
     *
     * @param key         = location where you want to enter the saved value
     * @param elementName = saved value
     */
    public void enterSavedValuesIntoElement(String key, String elementName) {
        LOGGER.info("enterSavedValuesIntoElement started");
        commonActions.sendKeys(commonActions.returnElementWithLabelName(elementName), driver.scenarioData.getData(key));
        LOGGER.info("returned value of element: "+ elementName + " = " +  driver.scenarioData.getData(key));
        LOGGER.info("enterSavedValuesIntoElement completed");
    }

    /**
     * This method is used to enter saved value in procure to pay
     *
     * @param key         = location where you want to enter the saved value
     * @param elementName = saved value
     */
    public void enterSavedValuesIntoTableElement(String key, String elementName) {
        LOGGER.info("enterSavedValuesIntoTableElement started");
        commonActions.sendKeys(getPOInputField(elementName), driver.scenarioData.getData(key));
        LOGGER.info("returned value of element: " + elementName + " = " + driver.scenarioData.getData(key));
        LOGGER.info("enterSavedValuesIntoTableElement completed");
    }

    /**
     * This method checks the checkbox for delivery complete and selects locator based on the environment
     */
    public void deliveryCompleteCheck() {
        LOGGER.info("deliveryCompleteCheck started");
        boolean result;
        if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            WebElement element = returnElementToClick(DELIVERY_COMPLETE_CHECK_BUTTON);
            result = element.getAttribute("class").contains("checked");
        } else {
            WebElement element = returnElementToClick(DELIVERY_COMPLETE_CHECK_BOX);
            result = element.isDisplayed();
        }
        Assert.assertTrue("FAIL: Delivery Complete Box is not checked!", result);
        LOGGER.info("deliveryCompleteCheck completed");
    }

    /**
     * This method will enter the date with respect to the months passed
     *
     * @param elementName - String , Name of the input field.
     * @param months      - Number of months to add/subtract from current month.
     * @param tense       - phase of date to be entered(past|present|future)
     */
    public void getsAndEntersDate(String elementName, int months, String tense) {
        LOGGER.info("getsAndEntersDate started");
        String expectedDate = commonActions.getDate("MM/dd/yyyy", months, tense);
        commonActions.sendKeys(returnElementToClick(elementName), expectedDate);
        LOGGER.info("getsAndEntersDate completed");
    }

    /**
     * Generates a random identification code and enters into the field
     */
    public void generateAndEnterUniqueIdentificationCode() {
        LOGGER.info("generateAndEnterUniqueIdentificationCode started");
        randomNumber = driver.scenarioData.getData(PAYMENT_METHODS)
                + String.valueOf(CommonUtils.getRandomNumber(10000, 99999));
        driver.clearInputAndSendKeys(identificationCodeInput, randomNumber);
        LOGGER.info("generateAndEnterUniqueIdentificationCode completed");
    }

    /**
     * This method will get the document number which has due amount to be paid to the vendor from FBL1N
     */
    public void findTheRightDataForAmount() {
        LOGGER.info("findTheRightDataForAmount started");
        List<WebElement> amountRow = webDriver.findElements(amountInLocalCurrencyRowBy);
        List<WebElement> documentRow = webDriver.findElements(documentNumberRowBy);
        for (int i = 0; i <= amountRow.size() - 1; i++) {
            if (amountRow.get(i).getText().contains("-")) {
                driver.scenarioData.setCurrentOrderNumber(documentRow.get(i - 2).getText());
                LOGGER.info("Current order number = " + driver.scenarioData.getCurrentOrderNumber());
                break;
            }
        }
        Assert.assertFalse("None of the row has amount which has to be paid",
                driver.scenarioData.getCurrentOrderNumber().isEmpty());
        LOGGER.info("findTheRightDataForAmount completed");
    }

    /**
     * This method will fetch the order number from scenario data and Enters the Order number into the element.
     *
     * @param elementName - Name of the input field.
     */
    public void getAndEnterTheDocumentNumberWhichHasTransactionTotal(String elementName) {
        LOGGER.info("getAndEnterTheDocumentNumberWhichHasTransactionTotal started");
        commonActions.sendKeys(returnElementToClick(elementName), driver.scenarioData.getCurrentOrderNumber());
        LOGGER.info("retrieved Doc No: " + driver.scenarioData.getCurrentOrderNumber());
        LOGGER.info("getAndEnterTheDocumentNumberWhichHasTransactionTotal completed");
    }

    /**
     * This method will fetch the Article order number from scenario data and Enters the Article Order number into the element.
     *
     * @param elementName - Name of the input field.
     */
    public void getAndEnterTheArticleDocumentNumber(String elementName) {
        LOGGER.info("getAndEnterTheArticleDocumentNumber started");
        driver.waitForPageToLoad();
        commonActions.sendKeys(returnElementToClick(elementName), driver.scenarioData.getCurrentArticleDocumentNumber());
        LOGGER.info("retrieved Article Document Number: " + driver.scenarioData.getCurrentArticleDocumentNumber());
        LOGGER.info("getAndEnterTheArticleDocumentNumber completed");
    }

    /**
     * This method clicks on two columns using the control key
     */
    public void selectTwoColumnsUsingControlKey() {
        LOGGER.info("selectTwoColumnsUsingControlKey started");
        driver.waitForPageToLoad();
        Actions actions = new Actions(webDriver);
        try {
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
            actions.keyDown(Keys.CONTROL).click(webDriver.findElement(lastChangedOnBy)).build().perform();
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
            actions.click(webDriver.findElement(lastChangedATBy)).build().perform();
        } catch (StaleElementReferenceException s) {
            WebElement staleElement1 = webDriver.findElement(lastChangedOnBy);
            WebElement staleElement2 = webDriver.findElement(lastChangedATBy);
            actions.click(staleElement1).build().perform();
            driver.waitForMilliseconds(Constants.TWO_THOUSAND);
            actions.click(staleElement2).build().perform();
        }
        actions.keyUp(Keys.CONTROL).build().perform();
        LOGGER.info("selectTwoColumnsUsingControlKey completed");
    }

    /**
     * This method is to enter the saved timestamps of payment before and after creating a file
     *
     * @param timeStamp   Enters the saved time before/after creating a file
     * @param elementName Name of the input field
     */
    public void enterSavedTimesBeforeAndAfterFileCreation(String timeStamp, String elementName) {
        LOGGER.info("enterSavedTimesBeforeAndAfterFileCreation started");
        driver.clearInputAndSendKeys(returnElementToClick(elementName),
                driver.scenarioData.genericData.get("Time stamp " + timeStamp + " creating a file"));
        LOGGER.info("enterSavedTimesBeforeAndAfterFileCreation completed");
    }

    /**
     * If field is not found on current screen it scrolls to element
     * and then selects the target field and sends the string value to fill it
     *
     * @param field Input to have value set
     * @param value Value sent as string to be input into field
     */
    public void scrollAndEnterValueIntoPOField(String field, String value) {
        LOGGER.info("scrollAndEnterValueIntoPOField started");
        WebElement input = getPOInputField(field);
        driver.moveToElementClick(input);
        commonActions.sendKeys(input, value);
        if (field.equalsIgnoreCase(Constants.VENDOR)) {
            for (int i = 0; i < 3; i++) {
                driver.waitForPageToLoad();
                driver.performKeyAction(Keys.ENTER);
            }
            driver.waitForPageToLoad();
        } else {
            driver.performKeyAction(Keys.ENTER);
        }
        LOGGER.info("scrollAndEnterValueIntoPOField completed");
    }
}