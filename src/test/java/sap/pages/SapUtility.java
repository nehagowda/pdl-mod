package sap.pages;

import common.Config;
import common.Constants;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import utilities.Driver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.logging.Logger;

public class SapUtility {

    Driver driver = new Driver();
    private final Logger LOGGER = Logger.getLogger(SapUtility.class.getName());

    public SapUtility() {

    }

    public static final String FEATURE_FILE_NAME_KEY = "featureFileName";
    public static final String SCENARIO_NAME_KEY = "scenarioName";
    public static final String TEST_STATUS_KEY = "testStatus";
    private static final String TEST_NAME = "Test Name";
    private static final String TOTAL_NUMBER_OF_EXECUTIONS = "Total Number of executions";
    private static final String TOTAL_PASSED = "Total passed";
    private static final String TOTAL_FAILED = "Total failed";
    private static final String STABILITY_PERCENTAGE = "Stability Percentage";
    private static final String TOTAL_QA = "Total qa";
    private static final String QA_PASSED = "qa passed";
    private static final String QA_FAILED = "qa failed";
    private static final String TOTAL_STG = "Total stg";
    private static final String STAGE_PASSED = "stg passed";
    private static final String STAGE_FAILED = "stg failed";
    private static final String TOTAL_DEV = "Total dev";
    private static final String DEV_PASSED = "dev passed";
    private static final String DEV_FAILED = "dev failed";
    private static final String TOTAL = "Total ";
    private static final String FILE_EXTENSION = "Metrics\\metrics.xlsx";

    /**
     * Method to update metrics excel sheet with test results
     *
     * @param featureName - name of the feature
     * @param scenarioName - Name of the scenario
     * @param testStatus - Test Execution Status
     * @throws Throwable - IO Exception
     */
    public void updateTestResultForScenario(String featureName, String scenarioName, String testStatus) throws Throwable {
        LOGGER.info("updateTestResultForScenario started");
        File file = new File(Constants.SAP_DATA_SOURCE_LOCATION + FILE_EXTENSION);
        boolean isScenarioFound = false;
        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook xssfworkbook = new XSSFWorkbook(inputStream);
        XSSFSheet xssfsheet = xssfworkbook.getSheet(featureName);
        if (xssfsheet == null) {
            xssfsheet = xssfworkbook.createSheet(featureName);
            Row row;
            row = xssfsheet.createRow(0);
            Cell cell;
            cell = row.createCell(0);
            cell.setCellValue(TEST_NAME);
            cell = row.createCell(1);
            cell.setCellValue(TOTAL_NUMBER_OF_EXECUTIONS);
            cell = row.createCell(2);
            cell.setCellValue(TOTAL_PASSED);
            cell = row.createCell(3);
            cell.setCellValue(TOTAL_FAILED);
            cell = row.createCell(4);
            cell.setCellValue(STABILITY_PERCENTAGE);
            cell = row.createCell(5);
            cell.setCellValue(TOTAL_QA);
            cell = row.createCell(6);
            cell.setCellValue(QA_PASSED);
            cell = row.createCell(7);
            cell.setCellValue(QA_FAILED);
            cell = row.createCell(8);
            cell.setCellValue(TOTAL_STG);
            cell = row.createCell(9);
            cell.setCellValue(STAGE_PASSED);
            cell = row.createCell(10);
            cell.setCellValue(STAGE_FAILED);
            cell = row.createCell(11);
            cell.setCellValue(TOTAL_DEV);
            cell = row.createCell(12);
            cell.setCellValue(DEV_PASSED);
            cell = row.createCell(13);
            cell.setCellValue(DEV_FAILED);
            FileOutputStream outputStream = new FileOutputStream(file);
            xssfworkbook.write(outputStream);
            outputStream.close();
        }
        int totalRows = xssfsheet.getLastRowNum();
        for (int i = 0; i <= totalRows; i++) {
            Row row = xssfsheet.getRow(i);
            String cellValue = row.getCell(0).getStringCellValue();
            if (cellValue.contains(scenarioName)) {
                computeAndEnterTestResults
                        (featureName, i + 1, TOTAL_NUMBER_OF_EXECUTIONS);
                computeAndEnterTestResults
                        (featureName, i + 1, TOTAL + testStatus.toLowerCase());
                computeAndEnterTestResults
                        (featureName, i + 1, TOTAL+Config.getDataSet().toLowerCase());
                computeAndEnterTestResults
                        (featureName, i + 1,
                                Config.getDataSet().toLowerCase() + " " + testStatus.toLowerCase());
                isScenarioFound = true;
                break;
            }
        }
        if (!isScenarioFound) {
            driver.writeValuesIntoExcel(Constants.SAP_DATA_SOURCE_LOCATION +
                    FILE_EXTENSION, featureName, scenarioName, totalRows + 2, 1);
            computeAndEnterTestResults
                    (featureName, totalRows + 2, TOTAL_NUMBER_OF_EXECUTIONS);
            computeAndEnterTestResults
                    (featureName, totalRows + 2, TOTAL + testStatus.toLowerCase());
            computeAndEnterTestResults
                    (featureName, totalRows + 2, TOTAL + Config.getDataSet().toLowerCase());
            computeAndEnterTestResults
                    (featureName, totalRows + 2,
                            Config.getDataSet().toLowerCase() + " " + testStatus.toLowerCase());
        }
        LOGGER.info("updateTestResultForScenario completed");
    }

    /**
     * Computes and enters test results
     *
     * @param featureName - Name of the feature
     * @param rowNumber - Row number
     * @param metricName - name of the column
     * @throws Throwable - IO Exception
     */
    private void computeAndEnterTestResults(String featureName, int rowNumber, String metricName) throws Throwable {
        LOGGER.info("computeAndEnterTestResults started");
        int cellNumber = 10;
        File file = new File(Constants.SAP_DATA_SOURCE_LOCATION + FILE_EXTENSION);
        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook xssfworkbook = new XSSFWorkbook(inputStream);
        XSSFSheet xssfsheet = xssfworkbook.getSheet(featureName);
        Row orderRow = xssfsheet.getRow(rowNumber - 1);
        if (orderRow == null) {
            orderRow = xssfsheet.createRow(rowNumber - 1);
        }
        Row firstRow = xssfsheet.getRow(0);
        for (int i = 0; i <= firstRow.getLastCellNum(); i++) {
            String columnOutput = firstRow.getCell(i).getStringCellValue();
            if (metricName.equalsIgnoreCase(columnOutput)) {
                cellNumber = i;
                break;
            }
        }
            Cell cell = orderRow.getCell(cellNumber);
            if (cell != null) {
                double rowValue = cell.getNumericCellValue();
                cell.setCellValue(rowValue + 1);
            } else {
                cell = orderRow.createCell(cellNumber);
                cell.setCellValue(1);
            }
        FileOutputStream outputStream = new FileOutputStream(file);
        xssfworkbook.write(outputStream);
        outputStream.close();
        computeStabilityPercentage(featureName, rowNumber);
        LOGGER.info("computeAndEnterTestResults completed");
    }

    /**
     * Computes the stability percentage per scenario
     *
     * @param featureName - name of the feature
     * @param rowNumber - row number
     * @throws Throwable - IO Exception
     */
    private void computeStabilityPercentage(String featureName, int rowNumber) throws Throwable{
        LOGGER.info("computeStabilityPercentage started");
        File file = new File(Constants.SAP_DATA_SOURCE_LOCATION + FILE_EXTENSION);
        FileInputStream inputStream = new FileInputStream(file);
        XSSFWorkbook xssfworkbook = new XSSFWorkbook(inputStream);
        XSSFSheet xssfsheet = xssfworkbook.getSheet(featureName);
        Row orderRow = xssfsheet.getRow(rowNumber - 1);
        if (orderRow == null) {
            orderRow = xssfsheet.createRow(rowNumber - 1);
        }
        int passedCellNumber = 10;
        double passedCellValue;
        double totalCellValue;
        Row firstRow = xssfsheet.getRow(0);
        for (int i = 0; i <= firstRow.getLastCellNum(); i++) {
            String columnOutput = firstRow.getCell(i).getStringCellValue();
            if ((columnOutput).equalsIgnoreCase(TOTAL_PASSED)) {
                passedCellNumber = i;
                break;
            }
        }
        Cell passedCell1 = orderRow.getCell(passedCellNumber);
        Cell totalCell = orderRow.getCell(passedCellNumber - 1);
        try{
             passedCellValue = passedCell1.getNumericCellValue();
        } catch (NullPointerException ne){
            passedCell1 = orderRow.createCell(passedCellNumber);
            passedCell1.setCellValue(0);
            passedCellValue = passedCell1.getNumericCellValue();
        }
        totalCellValue = totalCell.getNumericCellValue();
        String percentage = String.valueOf(Math.round((passedCellValue / totalCellValue) * 100)) + "%";
        Cell cell = orderRow.getCell(passedCellNumber + 2);
        if (cell != null) {
                cell.setCellValue(percentage);
            } else {
                cell = orderRow.createCell(passedCellNumber + 2);
                cell.setCellValue(percentage);
            }
            FileOutputStream outputStream = new FileOutputStream(file);
            xssfworkbook.write(outputStream);
            outputStream.close();
        LOGGER.info("computeStabilityPercentage completed");
    }
}