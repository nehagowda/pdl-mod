package sap.steps;

import common.Config;
import common.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import org.openqa.selenium.Keys;
import sap.pages.CommonActions;
import sap.pages.PosDm;
import utilities.CommonUtils;
import utilities.Driver;
import wm.pages.TransactionSearch;

public class CommonActionsSteps {

    private Driver driver;
    private CommonActions commonActions;
    private TransactionSearch transactionSearch;
    private Scenario scenario;

    private static final String GENERATED_PURCHASE_ORDER = "Generated Purchase Order";
    private static final String START_TIME = "Start Time";
    private static final String END_TIME = "End Time";
    private static final String FULL_NAME = "Full Name";
    private static final String WEB_ORDER = "Web Order";
    private static final String GENERATED_RTV_ORDER = "Generated Return Order";

    public CommonActionsSteps(Driver driver) {
        this.driver = driver;
        commonActions = new CommonActions(driver);
        transactionSearch = new TransactionSearch(driver);
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @When("^I click on \"([^\"]*)\"$")
    public void i_click_on_exact_text(String text) {
        commonActions.clickOnExactText(text);
    }

    @When("^I click on \"([^\"]*)\" text or value$")
    public void i_click_on_exact_text_or_value(String text) {
        if (!Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            commonActions.clickOnPartialText(text);
        } else {
            commonActions.clickOnExactValueAttribute(text);
        }
    }

    @When("^I click on element with partial value \"([^\"]*)\"$")
    public void i_click_on_element_with_partial_value(String text) {
        commonActions.clickOnElementWithPartialValueAttribute(text);
    }

    @When("^I switch to the pop up window$")
    public void i_switch_to_the_pop_up_window() throws Throwable {
        commonActions.switchToNewWindowHandle();
    }

    @When("^I delete all cookies$")
    public void i_delete_all_cookies() throws Throwable {
        commonActions.deleteCookies();
    }

    @Then("^I switch back to the main window in SAP$")
    public void i_switch_back_to_the_main_window() throws Throwable {
        commonActions.switchFrameContext(Constants.DEFAULT_CONTENT);
    }

    @When("^I switch to the iFrame at Index \"([0-9]*)\" in SAP$")
    public void i_switch_to_iframe_at_index(int index) throws Throwable {
        commonActions.switchFrameContext(index);
    }

    @When("^I switch to the first iFrame in SAP$")
    public void i_switch_to_the_first_iFrame() throws Throwable {
        commonActions.switchFrameContext(0);
    }

    @When("^I switch to the parent iFrame in SAP$")
    public void i_switch_to_the_parent_iframe() throws Throwable {
        commonActions.switchFrameContext(Constants.PARENT);
    }

    @When("^I double click on \"([^\"]*)\"$")
    public void i_double_click_on_exact_text(String text) {
        commonActions.doubleClickOnExactText(text);
    }

    @When("^I double click value or text attribute with value \"([^\"]*)\"$")
    public void i_double_click_value_or_text_attribute_with_value(String text) {
        commonActions.doubleClickValueOrTextAttribute(text);
    }

    @When("^I click on partial text \"([^\"]*)\"$")
    public void i_click_on_partial_text(String partialText) throws Throwable {
        commonActions.clickOnPartialText(partialText);
    }

    @When("^I click on partial data \"([^\"]*)\"$")
    public void i_click_on_partial_data(String partialText) throws Throwable {
        commonActions.clickOnRequiredPartialText(partialText);
    }

    @When("^I wait for element with text of \"([^\"]*)\" to be visible$")
    public void i_wait_for_element_with_text_to_be_visible(String elementText) throws Throwable {
        commonActions.waitForElementWithText(elementText);
    }

    @When("^I wait \"([0-9]*)\" seconds for element with text of \"([^\"]*)\" to be visible$")
    public void i_wait_seconds_for_element_with_text_to_be_visible(int timeInSeconds, String elementText) throws Throwable {
        commonActions.waitForElementWithText(elementText, timeInSeconds);
    }

    @When("^I wait \"([0-9]*)\" seconds for element with partial text of \"([^\"]*)\" to be visible$")
    public void i_wait_for_seconds_element_with_partial_text_to_be_visible(int timeInSeconds, String elementText) throws Throwable {
        commonActions.waitForElementWithPartialText(elementText, timeInSeconds);
    }

    @Then("^I verify the success status in SAP$")
    public void i_verify_the_success_status_in_sap() throws Throwable {
        commonActions.verifySucessStatusInSap();
    }

    @Then("^I verify the Error status in SAP$")
    public void i_verify_the_error_status_in_sap() throws Throwable {
        commonActions.verifyErrorStatusInSap();
    }

    @When("^I click on element with value of \"([^\"]*)\"$")
    public void i_click_on_element_with_value_of(String valueAttributeValue) throws Throwable {
        commonActions.clickOnExactValueAttribute(valueAttributeValue);
    }

    @When("^I double click on element with value of \"([^\"]*)\"$")
    public void i_double_click_on_element_with_value_of(String valueAttributeValue) throws Throwable {
        commonActions.doubleClickOnExactValueAttribute(valueAttributeValue);
    }

    @Then("^I extract web order from \"([^\"]*)\" sheet which is used in sap and web order from \"([^\"]*)\" sheet name and verify if they are same$")
    public void i_verify_web_order_processed_in_legacy_is_same_as_web_order_received_by_legacy(String sapSheetname, String webSheetName) throws Throwable {
        commonActions.verifyWebOrderReceivedIsSameAsOrderProcessedFromLegacy(sapSheetname, webSheetName);
    }

    @Then("^I verify if tasks with errors exist in posdm/posdt$")
    public void i_verify_if_tasks_with_errors_exist_in_posdm_posdt() throws Throwable {
        if (driver.isElementDisplayed(CommonActions.errorsInTasksExist)) {
            Assert.fail("FAIL: " + driver.scenarioData.getData("invoice") + " is the invoice number for which errors in tasks exist");
        }
    }

    @When("^I double click on partial text \"([^\"]*)\"$")
    public void i_double_click_on_partial_text(String partialText) throws Throwable {
        commonActions.doubleClickOnPartialText(partialText);
    }

    @When("^I double click on partial text or value \"([^\"]*)\"$")
    public void i_double_click_on_partial_text_or_value(String partialText) throws Throwable {
        if (!Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            commonActions.doubleClickOnPartialText(partialText);
        } else {
            commonActions.doubleClickOnExactValueAttribute(partialText);
        }
    }

    @When("^I copy the pdf content to clip board$")
    public void i_copy_the_pdf_content_to_clip_board() throws Throwable {
        driver.waitForPageToLoad();
        commonActions.selectAndCopyPdfContent();
    }

    @Then("^I verify the PDF in SAP has Content Like \"([^\"]*)\"$")
    public void i_verify_the_pdf_has_content(String reqText) throws Throwable {
        commonActions.verifyPdfContent(reqText);
    }

    @When("^I execute$")
    public void i_execute() throws Throwable {
        driver.waitForPageToLoad();
        driver.performKeyAction(Keys.F8);
    }

    @When("^I login with System, Client, Username, and Password$")
    public void i_login_with_system_client_username_and_password() throws Throwable {
        commonActions.login();
    }

    @When("^I login with System, Client, Username, and Password to \"([^\"]*)\"$")
    public void i_login_with_system_client_username_and_password(String module) throws Throwable {
        commonActions.login(module);
    }

    @When("^I login with System, Client, Username \"([^\"]*)\", and Password to \"([^\"]*)\" for dataset \"([^\"]*)\"$")
    public void i_login_with_system_client_username_and_password_from_examples(String userName, String module, String dataSet) throws Throwable {
        commonActions.login(module, userName, dataSet);
    }

    @When("^I enter t-code \"([^\"]*)\" in the command field$")
    public void i_enter_t_code_in_the_command_field(String tCode) throws Throwable {
        commonActions.enterTCode(tCode);
    }

    @When("^I enter t-code \"([^\"]*)\" in the command field for module \"([^\"]*)\"$")
    public void i_enter_t_code_in_the_command_field_for_module(String tCode, String module) throws Throwable {
        if (module.equalsIgnoreCase(Constants.SAP_POSDM) || module.equalsIgnoreCase(Constants.SAP_POSDT)) {
            commonActions.switchFrameContext(0);
        }
        commonActions.enterTCode(tCode);
    }

    @When("^I enter \"([^\"]*)\" into the field with label \"([^\"]*)\"$")
    public void i_enter_into_the_field_with_label_in_idoc_page(String value, String labelName) throws Throwable {
        commonActions.sendKeysWithLabelName(labelName, value);
    }

    @When("^I enter \"([^\"]*)\" into the field with label \"([^\"]*)\" and hit tab$")
    public void i_enter_into_the_field_with_label_in_idoc_page_and_hit_tab(String value, String labelName) throws Throwable {
        commonActions.sendKeysWithLabelName(labelName, value);
        i_hit_tab();
        driver.waitForMilliseconds();
    }

    @When("^I enter site information from environment variables into the field with label \"([^\"]*)\" and hit tab$")
    public void i_enter_site_information_into_the_field_with_label_in_idoc_page_and_hit_tab(String labelName) throws Throwable {
        commonActions.sendKeysWithLabelName(labelName, Config.getSiteNumber());
        i_hit_tab();
        driver.waitForMilliseconds();
    }

    @When("^I enter \"([^\"]*)\" into the field with label \"([^\"]*)\" at position \"([0-9]*)\"$")
    public void i_enter_into_the_field_with_label_in_idoc_page_at_position(String value, String labelName, String position) throws Throwable {
        commonActions.sendKeysWithLabelNameForMultipleInputFields(labelName, position, value);
    }

    @When("^I click checkbox or radio button with label \"([^\"]*)\"$")
    public void i_click_checkbox_or_radio_button_with_label_in_idoc_page(String labelName) throws Throwable {
        commonActions.clickOnRadioOrCheckBoxWithLabel(labelName);
    }

    @When("^I enter \"([^\"]*)\" from preferred data source into the field with label \"([^\"]*)\"$")
    public void i_enter_from_preferred_data_source_into_the_field_with_label_in_idoc_page(String value, String labelName) throws Throwable {
        if(!Config.getSapDataSourceFlag().equalsIgnoreCase("default")){
            value = commonActions.returnDataFromExcel(labelName);
        }
        driver.waitForPageToLoad(2000);
        commonActions.sendKeysWithLabelName(labelName, value);
    }

    @When("^I select the input field with label \"([^\"]*)\" at position \"([0-9]*)\" and enter \"([^\"]*)\"$")
    public void i_select_the_input_field_with_label_at_position_and_enter(String labelName, int position, String value) throws Throwable {
        commonActions.sendKeysWithLabelName(labelName, value, position);
    }

    @When("^I select the input field with label \"([^\"]*)\" at position \"([0-9]*)\" and enter \"([^\"]*)\" from preferred data source$")
    public void i_select_the_input_field_with_label_at_position_and_enter_from_preferred_data_source(String labelName, int position, String value) throws Throwable {
        if(!Config.getSapDataSourceFlag().equalsIgnoreCase("default")){
            value = commonActions.returnDataFromExcel(labelName);
        }
        commonActions.sendKeysWithLabelName(labelName, value, position);
    }

    @When("^I wait for iframe with check ok to appear$")
    public void i_wait_for_iframe_with_check_ok_to_appear() throws Throwable {
        commonActions.waitForIframeToAppear();
    }

    @When("^I hit return$")
    public void i_hit_return() throws Throwable {
        driver.waitForMilliseconds();
        driver.performKeyAction(Keys.ENTER);
    }

    @When("^I key \"([^\"]*)\" the control key$")
    public void i_key_up_or_down_the_control_key(String upOrDown) throws Throwable {
        commonActions.performHoldAndReleaseKeyAction(Keys.CONTROL, upOrDown);
    }

    @When("^I hit tab$")
    public void i_hit_tab() throws Throwable {
        driver.performKeyAction(Keys.TAB);
    }

    @And("^I enter today's date into the field with label \"([^\"]*)\"$")
    public void i_enter_todays_date_with_label(String labelName) throws Throwable {
        commonActions.sendKeysTodaysDateWithLabelName(labelName);
    }

    @And("^I accept the alert message$")
    public void i_fix_the_alert_message() throws Throwable {
        commonActions.acceptTheAlert();
    }

    @When("^I click on element with title attribute value \"([^\"]*)\"$")
    public void i_click_on_element_with_title_attribute_value(String elementTitle) throws Throwable {
        commonActions.selectElementWithTitle(elementTitle);
    }

    @When("^I double click on element with title attribute value \"([^\"]*)\"$")
    public void i_double_click_on_element_with_title_attribute_value(String elementTitle) throws Throwable {
        commonActions.doubleClickElementWithTitle(elementTitle);
    }

    @When("^I refresh the browser$")
    public void i_refresh_the_browser() throws Throwable {
        commonActions.refreshBrowser();
    }

    @When("^I hit F7$")
    public void i_select_all() throws Throwable {
        driver.performKeyAction(Keys.F7);
    }

    @When("^I enter previously saved PO into field with label name \"([^\"]*)\"$")
    public void i_enter_saved_PO_into_field_with_label_name(String labelName) throws Throwable {
        commonActions.enterSavedPONumberFromScenarioDataWithLabelName(labelName);
    }

    @When("^I enter previously saved PO into field with title \"([^\"]*)\"$")
    public void i_enter_saved_PO_into_field_with_title(String title) throws Throwable {
        commonActions.enterPOInElementWithTitle(title);
    }

    @When("^I save the purchase order number to the scenario data$")
    public void i_save_purchase_order_number_to_scenario_data() throws Throwable {
        String purchaseOrderNumber = commonActions.returnPurchaseOrderNumber();
        driver.scenarioData.setCurrentPurchaseOrderNumber(purchaseOrderNumber);
        scenario.write(GENERATED_PURCHASE_ORDER + " = " + driver.scenarioData.getCurrentPurchaseOrderNumber());
    }

    @When("^I go back$")
    public void i_go_back() throws Throwable {
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        driver.performKeyAction(Keys.F3);
    }

    @When("^I click display/change$")
    public void i_hit_display_change() throws Throwable {
        driver.waitForMilliseconds(Constants.TWO_THOUSAND);
        driver.performKeyAction(Keys.F5);
    }

    @When("^I enter \"([^\"]*)\" value into column name \"([^\"]*)\" at line number \"([0-9]*)\" in items table in nwbc$")
    public void i_enter_value_into_column_name_at_row_number_in_items_table_in_nwbc(String value, String columnName, int rowNumber) throws Throwable {
        commonActions.sendKeysIntoInputFieldsInItemsTableInNwbc(columnName, rowNumber, value);
    }

    @When("^I enter \"([^\"]*)\" value into column name \"([^\"]*)\" at line number \"([0-9]*)\" in items table in nwbc from preferred data source$")
    public void i_enter_value_into_column_name_at_row_number_in_items_table_in_nwbc_from_preferred_data_source(String value, String columnName, int rowNumber) throws Throwable {
        if (!Config.getSapDataSourceFlag().equalsIgnoreCase(CommonActions.DEFAULT)) {
            value = commonActions.returnDataFromExcel(columnName);
        }
        commonActions.sendKeysIntoInputFieldsInItemsTableInNwbc(columnName, rowNumber, value);
    }

    @When("^I verify \"([^\"]*)\" value for column name \"([^\"]*)\" at line number \"([0-9]*)\" in items table in nwbc$")
    public void i_verify_value_into_column_name_at_row_number_in_items_table_in_nwbc(String value, String columnName, int rowNumber) throws Throwable {
        commonActions.assertTextForInputFieldsInItemsTableInNwbc(columnName, rowNumber, value);
    }

    @When("^I verify \"([^\"]*)\" value for column name \"([^\"]*)\" at line number \"([0-9]*)\" in items table in nwbc from preferred data source$")
    public void i_verify_value_into_column_name_at_row_number_in_items_table_in_nwbc_from_preferred_data_source(String value, String columnName, int rowNumber) throws Throwable {
        if (!Config.getSapDataSourceFlag().equalsIgnoreCase(CommonActions.DEFAULT)) {
            value = commonActions.returnDataFromExcel(columnName);
        }
        commonActions.assertTextForInputFieldsInItemsTableInNwbc(columnName, rowNumber, value);
    }

    @When("^I save \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_save_sheet_name_column_name_and_row_number(String folderName, String excelFileName, String sheetName, String rowNumber) throws Throwable {
        driver.scenarioData.setData(CommonActions.FOLDER_NAME, folderName);
        driver.scenarioData.setData(CommonActions.EXCEL_FILE_NAME, excelFileName);
        driver.scenarioData.setData(CommonActions.SHEET_NAME_1, sheetName);
        driver.scenarioData.setData(CommonActions.ROW_NUMBER, rowNumber);
    }

    @When("^I load start time, end time, store number, web order, article, quantity$")
    public void i_load_start_time_end_time_store_number_web_order_article_quantity() throws Throwable {
        driver.scenarioData.setData(PosDm.TIME_STAMP_BEFORE_HASP_MAP_KEY,
                commonActions.returnDataFromExcel(START_TIME));
        driver.scenarioData.setData(PosDm.TIME_STAMP_AFTER_HASP_MAP_KEY,
                commonActions.returnDataFromExcel(END_TIME));
        driver.scenarioData.setData(Constants.STORE_NUMBER_ORDER_SERVICE_KEY,
                commonActions.returnDataFromExcel(Constants.SITE));
        driver.scenarioData.setData(Constants.ARTICLE, commonActions.returnDataFromExcel(Constants.ARTICLE));
        driver.scenarioData.setData(Constants.QUANTITY_ORDER_SERVICE_KEY,
                commonActions.returnDataFromExcel(Constants.QUANTITY_ORDER_SERVICE_KEY));
        driver.scenarioData.setData(Constants.DATE, commonActions.returnDataFromExcel(CommonActions.DATE));
        driver.scenarioData.setData(Constants.NAME, commonActions.returnDataFromExcel(FULL_NAME));
        driver.scenarioData.setCurrentOrderNumber(commonActions.returnDataFromExcel(WEB_ORDER));
    }

    @When("^I save \"([^\"]*)\", \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_save_sheet_name_column_name(String folderName,String excelFileName, String sheetName) throws Throwable {
        driver.scenarioData.setData(CommonActions.FOLDER_NAME,folderName);
        driver.scenarioData.setData(CommonActions.EXCEL_FILE_NAME,excelFileName);
        driver.scenarioData.setData(CommonActions.SHEET_NAME_1, sheetName);
    }

    @When("^I load data from jenkins$")
    public void i_load_data_from_jenkins() throws Throwable {
        if(Config.getSapDataSourceFlag().equalsIgnoreCase(Constants.JENKINS)){
            driver.scenarioData.setData(Constants.DATE, Config.getTranDate());
            driver.scenarioData.setData(Constants.INVOICE, Config.getInvoiceNumber());
        }
    }

    @When("^I wait for first iframe with text \"([^\"]*)\"$")
    public void i_wait_for_first_iframe_with_text(String text) throws Throwable {
        commonActions.waitForFirstIframeWithTextInDolAp2n(text);
    }

    @When("^I click on get variant$")
    public void i_click_on_get_variant() throws Throwable {
        commonActions.getVariantShortCut();
    }

    @When("^I cancel existing sessions while logging in$")
    public void i_cancel_existing_sessions_while_logging_in() throws Throwable {
        commonActions.cancelSession = true;
    }

    @When("I check for element with text \"([^\"]*)\"")
    public void i_check_for_element_with_text(String text) throws Throwable {
        commonActions.assertElementDisplayedWithText(text);
    }

    @When("I check for \"([^\"]*)\" element with text from scenario data")
    public void i_check_for_element_with_text_from_scenario_data(String key) throws Throwable {
        commonActions.assertElementDisplayedWithText(driver.scenarioData.getData(key));
    }

    @When("I check for element with value \"([^\"]*)\"")
    public void i_check_for_element_with_value(String value) throws Throwable {
        commonActions.assertElementDisplayedWithValueAttribute(value);
    }

    @When("I check for element with title \"([^\"]*)\"")
    public void i_check_for_element_with_title(String text) throws Throwable {
        commonActions.assertElementDisplayedWithTitleAttribute(text);
    }

    @When("^I enter \"([^\"]*)\" with random number into \"([^\"]*)\" in Incoming Invoice page$")
    public void i_enter_with_random_number_into_incoming_invoice_page(String value, String elementName) throws Throwable {
        String randomReferenceNumber = value + CommonUtils.getRandomNumber(Constants.RANDOM_GENERATOR_MIN, Constants.RANDOM_GENERATOR_MAX);
        commonActions.sendKeys(commonActions.returnElementWithLabelName(elementName), randomReferenceNumber);
    }

    @When("^I click on element in stage with value \"([^\"]*)\"$")
    public void i_click_on_element_in_stage_with_value(String elementTitle) throws Throwable {
        if(Config.getDataSet().equalsIgnoreCase(Constants.STG)){
            commonActions.selectElementWithTitle(elementTitle);
        }
    }

    @When("^I enter \"([^\"]*)\" previously saved value into column name \"([^\"]*)\" at line number \"([0-9]*)\" in items table in nwbc$")
    public void i_enter_previously_saved_value_into_column_name_at_row_number_in_items_table_in_nwbc(String value, String columnName, int rowNumber) throws Throwable {
        commonActions.sendSavedKeysIntoInputFieldsInItemsTableInNwbc(columnName, rowNumber, value);
    }

    @When("^I click on the verticle scroll \"(Up|Down)\" button$")
    public void i_click_on_the_verticle_scroll(String direction) throws Throwable{
        commonActions.scrollUpOrDown(direction);
    }

    @When("^I enter reference number from prefered data source$")
    public void i_enter_reference_number_from_preferred_data_source() throws Throwable {
        String reference = commonActions.enterRandomString(10);
        commonActions.sendKeysWithLabelName("Reference", reference);
    }
}