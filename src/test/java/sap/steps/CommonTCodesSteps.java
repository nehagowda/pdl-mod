package sap.steps;

import common.Config;
import common.Constants;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.Keys;
import sap.pages.CommonActions;
import sap.pages.CommonTCodes;
import utilities.Driver;

public class CommonTCodesSteps {


    private Driver driver;
    private CommonActions commonActions;
    private CommonTCodes commonTCodes;

    public CommonTCodesSteps(Driver driver) {
        this.driver = driver;
        commonTCodes = new CommonTCodes(driver);
        commonActions = new CommonActions(driver);
    }

    @When("^I select \"([^\"]*)\" and enter \"([^\"]*)\" in common tcodes$")
    public void i_enter_value_in_input_field(String elementName, String input) throws Throwable {
        commonActions.sendKeys(commonTCodes.returnElement(elementName), input);
    }

    @When("^I enter Purchase Order Number into \"([^\"]*)\" in common tcodes$")
    public void i_enter_purchase_order_number(String elementName) throws Throwable {
        commonActions.getAndEnterPurchaseOrderNumber(commonTCodes.returnElement(elementName));
    }

    @When("^I wait for \"([^\"]*)\" field to be visible in common tcodes$")
    public void i_wait_for_field_to_be_visible(String elementName) throws Throwable {
        driver.waitForPageToLoad();
        driver.waitForElementVisible(commonTCodes.returnElement(elementName), Constants.ONE_HUNDRED_TWENTY);
    }

    @When("^I enter article document number in Scanner's RTV-Scan and Save$")
    public void i_enter_article_document_number_in_scanners_rtv_scan_and_save() throws Throwable {
        commonTCodes.inputArticleDocumentNumberInScanner();
    }

    @When("^I enter vendor number in Scanner's RTV - generate$")
    public void i_enter_vendor_number_in_scanners_rtv_generate() throws Throwable {
        commonTCodes.inputVendorNumberInScanner();
    }

    @Then("^I verify the value of \"([^\"]*)\" field contains \"([^\"]*)\" in common tcodes$")
    public void i_verify_the_value_of_field_contains_in_common_tcodes(String elementName, String validation) throws Throwable {
        commonActions.verifyTextAttribute(commonTCodes.returnElement(elementName), validation);
    }

    @Then("^I verify Article Document number is present in Item Overview$")
    public void i_verify_article_document_number_is_present_in_item_overview() throws Throwable {
        driver.assertElementAttributeString(CommonTCodes.articleDocumentNumberDisplay, Constants.VALUE,
                driver.scenarioData.getData("Article Document Num") + driver.scenarioData.getData("Art. Doc.Item"));
    }

    @When("^I select \"([^\"]*)\" field and enter \"([^\"]*)\" in \"([^\"]*)\" field in se16n$")
    public void i_select_field_and_enter_in_field_in_se16n(String fieldName, String value, String fromOrTo) throws Throwable {
        commonActions.sendKeys(commonTCodes.returnInputElementBasedOnFieldName(fieldName, fromOrTo), value);
    }

    @When("^I select \"([^\"]*)\" field and enter previously saved po in \"([^\"]*)\" field in se16n$")
    public void i_select_field_and_enter_in_field_in_se16n(String fieldName, String fromOrTo) throws Throwable {
        commonActions.sendKeys(commonTCodes.returnInputElementBasedOnFieldName(fieldName, fromOrTo), driver.scenarioData.getCurrentPurchaseOrderNumber());
    }

    @When("^I select \"([^\"]*)\" field and enter the value of \"([^\"]*)\" saved previously in \"([^\"]*)\" field in se16n table$")
    public void i_select_field_and_enter_the_value_of_saved_previously_in_field_in_se16n_table(String fieldName, String key, String fromOrTo) throws Throwable {
        commonTCodes.enterSavedDataIntoTableInSe16n(fieldName, fromOrTo, key);
    }


    @When("^I enter order number into field with \"([^\"]*)\" label$")
    public void i_select_and_enter_order_number(String elementName) throws Throwable {
        commonTCodes.enterPurchaseOrderNumber(elementName);
    }

    @When("^I select \"([^\"]*)\" and set the site \"([^\"]*)\" in scanner$")
    public void i_select_element_and_set_the_site_in_scanner(String elementName, String input) throws Throwable {
        String setSite = "";
        if (input.equalsIgnoreCase(Constants.STORE)) {
            if (Config.getDataSet().equalsIgnoreCase(Constants.QA) ||
                    Config.getDataSet().equalsIgnoreCase(Constants.DEV)) {
                setSite = Constants.NWBC_RTK_SITE;
            } else if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
                setSite = Constants.NWBC_RTQ_SITE;
            }
        } else {
            setSite = input;
        }
        try {
            commonActions.sendKeys(commonTCodes.returnElement(elementName), setSite);
        } catch (AssertionError a) {
            commonActions.sendKeysWithLabelName("Site", setSite);
        }
        driver.performKeyAction(Keys.TAB);
    }

    @When("^I save Reversed article document$")
    public void i_save_reversed_article_document() throws Throwable {
        commonTCodes.saveRevArtDocument();
    }

    @Then("^I verify the value attribute of element \"([^\"]*)\" matches \"([^\"]*)\"$")
    public void i_verify_the_value_attribute_of_element_matches(String elementName, String valueAttribute) throws Throwable {
        commonTCodes.assertValueAttribute(elementName, valueAttribute);
    }

    @When("^I click on \"([^\"]*)\" in common Tcodes$")
    public void i_click_on_elements_in_common_tcodes(String elementname) throws Throwable {
        commonTCodes.clickOnElementInCommonTcodes(elementname);
    }

    @When("^I select all check boxes$")
    public void i_select_all_checkboxes() throws Throwable {
        commonTCodes.selectAllCheckBoxesInSp01();
    }

    @When("^I acknowledge pdf delete$")
    public void i_acknowledge_pdf_delete() throws Throwable {
        commonTCodes.acknowledgeDeleteForPdfs();
    }

    @When("^I select line item with Purchase order number$")
    public void i_select_line_item_with_purchase_order_number() throws Throwable {
        commonTCodes.selectLineItemWithPoNumber();
    }

    @When("^I enter new price into field with label \"([^\"]*)\" in position \"([0-9]*)\" in order to cash page$")
    public void i_enter_new_price_into_field_with_label_and_position_in_order_to_cash_page(String elementName, int fieldPosition) throws Throwable {
        commonTCodes.changePriceInput(elementName, fieldPosition);
    }

    @When("I enter \"([^\"]*)\" into the table at row number \"([^\"]*)\" and column number \"([^\"]*)\" at position \"([0-9]*)\" in commonTcodes")
    public void i_enter_into_the_table_at_row_number_and_column_number(String text, String rowNumber, String columnNumber, int position) throws Throwable {
        commonTCodes.sendKeysInATable(text, rowNumber, columnNumber, position);
    }

    @Then("^I verify Payment Success$")
    public void i_verify_payment_success() throws Throwable {
        commonTCodes.verifyPaymentProposalSucessStatus();
    }

    @Then("^I compare \"([^\"]*)\" value from web and \"([^\"]*)\" from CAR for customer with email \"([^\"]*)\"$")
    public void i_compare_value_from_web_and_from_car(String webValueKey, String carValueKey, String email) throws Throwable {
        commonTCodes.validateAppointmentItemService(webValueKey, carValueKey);
        commonTCodes.validateAppointmentHeaderService(webValueKey, carValueKey, email);
    }

    @Then("^I save$")
    public void i_save() throws Throwable {
        commonTCodes.shortcutForSave();
    }
}