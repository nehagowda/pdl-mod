package sap.steps;

import common.Config;
import common.Constants;
import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import sap.pages.CommonActions;
import sap.pages.IDocPage;
import utilities.CommonUtils;
import utilities.Driver;

public class IDocPageSteps {

    private Driver driver;
    private IDocPage iDocPage;
    private CommonActions commonActions;
    private Scenario scenario;

    private static final String GENERATED_IDOC_NUMBER = "Generated Idoc number";
    private static final String TAX_AMOUNT = "Tax amount";

    public IDocPageSteps(Driver driver) {
        this.driver = driver;
        iDocPage = new IDocPage(driver);
        commonActions = new CommonActions(driver);
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @When("^I select the \"([^\"]*)\" button on the Idoc page$")
    public void i_select_the_button_on_idoc_page(String element) throws Throwable {
        iDocPage.clickIniDocPage(element);
    }

    @And("^I enter iDoc Number$")
    public void i_enter_idoc_number() throws Throwable {
        iDocPage.inputIniDocPage();
    }

    @Then("^I verify iDoc status is successful$")
    public void i_verify_idoc_status_is_successful() throws Throwable {
        driver.assertElementAttributeString(iDocPage.greenLight, Constants.TITLE, iDocPage.IDOC_STATUS_TEXT);
    }

    @Then("^I verify the \"(.?)\" element on the iDoc page has a \"(.?)\" attribute of \"(.?)\"$")
    public void i_verify_element_value(String element, String attribute, String text) throws Throwable {
        driver.assertElementAttributeString(iDocPage.returnElement(element), attribute, text);
    }

    @Then("^I verify the vehicle \"([^\"]*)\" to \"([^\"]*)\"$")
    public void I_verify_the_vehicle_information_to(String key, String vehicle_field) throws Throwable {
        iDocPage.verifyVehicleDetails(key, vehicle_field);
    }

    @Then("^I verify the customer \"([^\"]*)\" information of type \"([^\"]*)\"$")
    public void I_verify_the_customer_information(String customerField, String customerType) throws Throwable {
        iDocPage.verifyCustomerData(customerType, customerField);
    }

    @When("^I update idoc values for \"([^\"]*)\" and \"([^\"]*)\"$")
    public void i_update_idoc_values_for_and(String type, String vendor) throws Throwable {
        iDocPage.updateIdocValues(type, vendor);
    }

    @When("^I enter Idoc Number into \"([^\"]*)\" in idoc page$")
    public void i_enter_idoc_number_into_idoc_page(String elementName) throws Throwable {
        iDocPage.getAndEnterIdocNumberInIdoc(elementName);
    }

    @When("^I save the \"([^\"]*)\" number in idoc page$")
    public void i_save_the_number(String elementName) throws Throwable {
        iDocPage.saveIdocNumberInIdocPage(elementName);
        scenario.write(GENERATED_IDOC_NUMBER + " = " + driver.scenarioData.getCurrentIDocNumber());
    }

    @Then("^I enter \"([^\"]*)\" information on \"([^\"]*)\" in IDOC page$")
    public void i_enter_information_on_in_idoc_page(String value, String element) throws Throwable {
        commonActions.sendKeys(iDocPage.returnElement(element), value);
    }

    @Then("^I enter \"([^\"]*)\" information on \"([^\"]*)\" in IDOC page and verify text input$")
    public void i_enter_information_on_in_idoc_page_and_verify_text_input(String value, String element) throws Throwable {
        iDocPage.inputGlAccountInDol(element, value);
    }

    @When("^I click on \"([^\"]*)\" in IDOC page$")
    public void i_click_on_element(String elementName) throws Throwable {
        iDocPage.clickIniDocPage(elementName);
    }

    @When("^I click on \"([^\"]*)\" in IDOC page from Environment variables$")
    public void i_click_on_element_extracted_from_environment_variables(String environmentVarialbeValue) throws Throwable {
        iDocPage.clickIniDocPage(commonActions.returnDataFromEnvironmentVariables(environmentVarialbeValue));
    }

    @Then("I verify the \"([^\"]*)\" tax code count \"([^\"]*)\" in IDOC page")
    public void i_verify_i0_tax_text_count_in_idoc_page(String taxCode, String count) {
        iDocPage.verifyTaxCodeTextCountInIDOC(taxCode, count);
    }

    @When("^I switch to \"([^\"]*)\" Iframe in Idoc page$")
    public void i_switch_to_iframe_in_nwbc(String elementName) throws Throwable {
        iDocPage.switchToIframeInIdoc(elementName);
    }

    @When("^I wait for \"([^\"]*)\" iFrame to appear$")
    public void i_wait_for_iframe_to_appear(String elementName) throws Throwable {
        driver.waitForElementVisible(iDocPage.returnElement(elementName), Constants.ONE_HUNDRED_TWENTY);
    }

    @Then("^I verify \"([^\"]*)\" is displayed in iDoc page$")
    public void i_verify_status_for_the_record(String elementName) throws Throwable {
        driver.isElementDisplayed(iDocPage.returnElement(elementName));
    }

    @When("^I enter \"([^\"]*)\" with random number into \"([^\"]*)\" in idoc page$")
    public void i_enter_with_random_number_into_idoc_page(String value, String elementName) throws Throwable {
        String randomReferenceNumber = value + CommonUtils.getRandomNumber(Constants.RANDOM_GENERATOR_MIN, Constants.RANDOM_GENERATOR_MAX);
        commonActions.sendKeysWithLabelName(elementName, randomReferenceNumber);
    }

    @When("^I enter Today's date into \"([^\"]*)\" in idoc page$")
    public void i_enter_today_date_into_element_in_idoc_page(String elementName) throws Throwable {
        iDocPage.getAndEnterTodayDateInIdocPage(elementName);
    }

    @When("^I enter current date in yyyymmdd into the field with label \"([^\"]*)\"$")
    public void i_current_date_in_format_into_the_field_with_label_in_idoc_page(String labelName) throws Throwable {
        iDocPage.getAndEnterTodayDateInFieldWithLabelName(labelName);
    }

    @Then("^I valdiate that the date format is mm/dd/yyyy$")
    public void i_validate_that_the_date_format_is() throws Throwable {
        iDocPage.validateTheDateFormatInPts();
    }

    @When("^I wait for \"([^\"]*)\" field to be visible in idoc page$")
    public void i_wait_for_field_to_be_visible(String elementName) throws Throwable {
        driver.waitForPageToLoad();
        driver.waitForElementVisible(iDocPage.returnElement(elementName), Constants.ONE_HUNDRED_TWENTY);
    }

    @Then("^I validate \"([^\"]*)\" entered matches original \"([^\"]*)\" in Idoc page$")
    public void i_validate_entered_matches_original(String elementName, String value) throws Throwable {
        if (value.equalsIgnoreCase(CommonActions.ENVIRONMENT_FEE) || value.equalsIgnoreCase(CommonActions.SALES_TAX)) {
            try {
                if (!commonActions.returnDataFromEnvironmentVariables(value).isEmpty()) {
                    driver.jsScrollToElement(iDocPage.returnElement(elementName));
                    iDocPage.assertValDisplayedMatchesValExpectedinIdocPage(elementName, commonActions.returnDataFromEnvironmentVariables(value));
                }
            } catch (NullPointerException N) {
                N.printStackTrace();
            }
        } else {
            driver.jsScrollToElement(iDocPage.returnElement(elementName));
            iDocPage.assertValDisplayedMatchesValExpectedinIdocPage(elementName, value);
        }
    }

    @Then("^I validate \"([^\"]*)\" entered matches original \"([^\"]*)\" in Idoc page with input values from environment variables$")
    public void i_validate_entered_matches_original_from_environment_variables(String elementName, String value) throws Throwable {
        iDocPage.assertValDisplayedMatchesValExpectedinIdocPage(elementName, commonActions.returnDataFromEnvironmentVariables(value));
    }

    @When("^I enter a test idoc to copy from$")
    public void i_enter_a_test_idoc_to_copy_from() throws Throwable {
        iDocPage.enterTestIdocFromExcel();
    }

    @When("^I enter existing idoc to copy from on input with label \"([^\"]*)\"$")
    public void i_enter_existing_idoc_to_copy_from_on_input_with_label(String labelName) throws Throwable {
        iDocPage.enterExistingIdocInLabel(labelName);
    }

    @And("^I print \"([^\"]*)\" element value in Idoc page$")
    public void i_print_tax_amount_in_idoc_page(String elementName) throws Throwable {
        scenario.write(TAX_AMOUNT + " = " + iDocPage.returnElement(elementName).getAttribute(Constants.VALUE));
    }

    @When("^I enter Today's date into \"([^\"]*)\" in FB60$")
    public void i_enter_today_date_into_element_in_fb60(String elementName) throws Throwable {
        iDocPage.enterTodaydateInFb60(elementName);
    }

    @And("^I scroll to following \"([^\"]*)\" element$")
    public void i_scroll_to_following_element(String elementName) throws Throwable {
        driver.moveToElementClick(iDocPage.returnElement(elementName));
    }

    @And("^I save the document number from \"([^\"]*)\" in Idoc page$")
    public void i_save_the_doc_number_from_in_idoc_page(String elementName) throws Throwable {
        iDocPage.saveDocNumber(elementName);
    }

    @Then("^I validate \"([^\"]*)\" entered matches original saved document number$")
    public void i_validate_entered_matches_original_saved_document_number(String elementName) throws Throwable {
        iDocPage.assertsSavedDocumentNumber(elementName);
    }

    @Then("^I verify \"([^\"]*)\" matches \"([^\"]*)\" displayed in Idoc page$")
    public void i_verify_matches_displayed_in_vehicle_page(String elementName, String text) throws Throwable {
        iDocPage.assertTextDisplayedInIdoc(elementName, text);
    }

    @And("^I save the Article document number from \"([^\"]*)\" in Idoc page$")
    public void i_save_the_article_document_number_from_field_in_idoc_page(String elementName) throws Throwable {
        iDocPage.saveArticleDocumentNumbersInIDocPage(elementName);
    }

    @And("^I enter the Article document number into \"([^\"]*)\" in Idoc page$")
    public void i_enter_the_article_document_number_into_field_in_idoc_page(String elementName) throws Throwable {
        iDocPage.getAndEnterArticleDocumentNumberInIdocPage(elementName);
    }

    @And("^I enter the G/L Account number into \"([^\"]*)\" in fb03$")
    public void i_enter_the_GL_Account_into_field_in_idoc_page(String elementName) throws Throwable {
        commonActions.sendKeys(iDocPage.returnElement(elementName), driver.scenarioData.getData("G/L Account from WPUFIB"));
    }

    @And("^I enter the Bopis G/L Account number into \"([^\"]*)\" field in fb03$")
    public void i_enter_the_bopis_GL_Account_into_field_in_idoc_page(String elementName) throws Throwable {
        commonActions.sendKeysWithLabelName(elementName, driver.scenarioData.getData("Bopis G/L Account from WPUFIB"));
    }

    @When("^I double click on \"([^\"]*)\" in Idoc page$")
    public void i_double_click_on_idoc_page(String elementName) throws Throwable {
        iDocPage.doubleClick(elementName);
    }

    @When("^I extract \"([^\"]*)\" from excel sheet which is in \"([0-9]*)\"th row and \"([0-9]*)\"th cell from sheet name \"([^\"]*)\"$")
    public void i_extract_invoice_number_from_excel(String key, int rowNumber, int cellNumber, String sheetName) throws Throwable {
        iDocPage.readFromExcelAndReturnSingleValue(Constants.fullPathFileNames.get(Constants.INVOICE_SAP), sheetName, rowNumber, cellNumber, key);
    }

    @When("^I extract \"([^\"]*)\" from excel sheet which is in \"([0-9]*)\"th row and \"([0-9]*)\"th cell from scenario data$")
    public void i_extract_invoice_number_from_excel_scenario_data(String key, int rowNumber, int cellNumber) throws Throwable {
        if(!Config.getSapDataSourceFlag().equalsIgnoreCase(Constants.JENKINS)) {
            iDocPage.readFromExcelAndReturnSingleValue(Constants.SAP_DATA_SOURCE_LOCATION +
                            driver.scenarioData.getData(CommonActions.FOLDER_NAME) +
                            "\\" + driver.scenarioData.getData(CommonActions.EXCEL_FILE_NAME),
                    driver.scenarioData.getData(CommonActions.SHEET_NAME_1), rowNumber, cellNumber, key);
        }
    }

    @When("^I extract \"([^\"]*)\" from excel sheet which is in \"([0-9]*)\"th row and \"([0-9]*)\"th cell from sheet name \"([^\"]*)\" and file name \"([^\"]*)\"$")
    public void i_extract_invoice_number_from_excel_from_sheet_name_and_file_name(String key, int rowNumber, int cellNumber, String sheetName, String fileName) throws Throwable {
        iDocPage.readFromExcelAndReturnSingleValue(Constants.DATA_STORAGE_FOLDER + fileName, sheetName, rowNumber, cellNumber, key);
        if(key.equalsIgnoreCase(Constants.WEB_ORDER)){
            driver.scenarioData.setCurrentOrderNumber(driver.scenarioData.getData(Constants.WEB_ORDER.toLowerCase()));
        }
    }

    @When("^I set web order to current order number in scenario data$")
    public void i_set_web_order_to_current_order_number_in_scenario_data() throws Throwable {
        driver.scenarioData.setCurrentOrderNumber(driver.scenarioData.getData(Constants.WEB_ORDER.toLowerCase()));
    }

    @When("^I extract \"([^\"]*)\" from excel sheet which is in \"([0-9]*)\"th row and \"([0-9]*)\"th cell from sheet name \"([^\"]*)\" from environment variables$")
    public void i_extract_invoice_number_from_excel_from_environment_variables(String key, int rowNumber, int cellNumber, String sheetName) throws Throwable {
        iDocPage.readFromExcelAndReturnSingleValue(Constants.fullPathFileNames.get(Constants.INVOICE_SAP), commonActions.returnDataFromEnvironmentVariables(sheetName), rowNumber, cellNumber, key);
    }

    @When("^I extract all values from excel from sheet name \"([^\"]*)\"$")
    public void i_extract_all_values_from_excel_sheet_name(String sheetName) throws Throwable {
        iDocPage.readAllInvoiceDetailsFromExcel(Constants.fullPathFileNames.get(Constants.INVOICE_SAP), sheetName);
    }

    @When("^I extract all values from excel from sheet name from scenario data$")
    public void i_extract_all_values_from_excel_sheet_name_from_scenario_data() throws Throwable {
        iDocPage.readAllInvoiceDetailsFromExcel(Constants.SAP_DATA_SOURCE_LOCATION +
                driver.scenarioData.getData(CommonActions.FOLDER_NAME) +
                "\\" + driver.scenarioData.getData(CommonActions.EXCEL_FILE_NAME), driver.scenarioData.getData(CommonActions.SHEET_NAME_1));
    }

    @When("^I extract all values from excel from sheet name \"([^\"]*)\" and file name \"([^\"]*)\"$")
    public void i_extract_all_values_from_excel(String sheetName, String fileName) throws Throwable {
        iDocPage.readAllInvoiceDetailsFromExcel(Constants.DATA_STORAGE_FOLDER + fileName, sheetName);
    }

    @When("^I extract all values from excel from sheet name \"([^\"]*)\" from environment varaibles$")
    public void i_extract_all_values_from_excel_from_environment_variables(String sheetName) throws Throwable {
        iDocPage.readAllInvoiceDetailsFromExcel(Constants.fullPathFileNames.get(Constants.INVOICE_SAP), commonActions.returnDataFromEnvironmentVariables(sheetName));
    }

    @When("^I click on Idoc number extracted$")
    public void i_click_on_idoc_number_extracted() throws Throwable {
        iDocPage.clickOnIdocExtractedFromExcel();
    }

    @When("^I click on \"([^\"]*)\" extracted from EDIDS table$")
    public void i_click_on_extracted_from_edids_table(String key) throws Throwable {
        iDocPage.clickOnIdocExtractedFromEDIDS(key);
    }

    @Then("^I verify the Net Values and articles in billing document$")
    public void i_verify_the_net_values_and_articles_in_billing_document() throws Throwable {
        iDocPage.verifyBillingDocumentNetAmountWithArticlesAggregate();
    }

    @Then("^I verify the Net Values and Articles in billing document for aggregated$")
    public void i_verify_the_net_values_and_articles_in_billing_document_for_aggregated() throws Throwable {
        iDocPage.verifyBillingDocumentNetAmountWithArticlesAggregate();
    }

    @Then("^I verify grand total is equal to total from excel$")
    public void i_verify_grand_total_is_equal_to_total_from_excel() throws Throwable {
        iDocPage.verifyTotalAmountInBillingDocumentInWper();
    }

    @Then("^I validate \"([^\"]*)\" entered matches original \"([^\"]*)\" in wper$")
    public void i_validate_entered_matches_original_text(String elementName, String value) throws Throwable {
        if (value.equalsIgnoreCase(CommonActions.ENVIRONMENT_FEE) || value.equalsIgnoreCase(CommonActions.SALES_TAX)) {
            try {
                if (!commonActions.returnDataFromEnvironmentVariables(value).isEmpty()) {
                    driver.verifyTextDisplayedCaseInsensitive(iDocPage.returnElement(elementName), commonActions.returnDataFromEnvironmentVariables(value));
                }
            } catch (NullPointerException N) {

            }
        } else {
            driver.verifyTextDisplayedCaseInsensitive(iDocPage.returnElement(elementName), value);
        }
    }

    @And("^I click on the data record which has the recently deleted assembly$")
    public void i_click_on_the_data_record_which_has_the_recently_deleted_assembly() throws Throwable {
        iDocPage.selectAssemblyRow();
    }

    @When("^I click \"([^\"]*)\" till \"([^\"]*)\" is visible in idoc page$")
    public void i_click_element_till_visible_in_idoc_page(String scrollElementName, String visibleElementName) throws Throwable {
        iDocPage.scrollTillElementIsVisible(visibleElementName, scrollElementName);
    }

    @Then("^I validate the execution status of the idoc created$")
    public void i_validate_the_execution_status_of_the_idoc_created() throws Throwable {
        iDocPage.validateExecutionStatus();
    }

    @Then("^I verify \"([^\"]*)\" field is not empty in idoc page$")
    public void i_verify_field_is_not_empty_in_idoc_page(String elementName) throws Throwable {
        commonActions.assertElementTextPopulated(iDocPage.returnElement(elementName));
    }

    @When("I create test idocs for specified number from config in we19")
    public void i__create_test_idocs_for_specified_number_from_config_in_we19() throws Throwable {
        iDocPage.loopThroughIdocAndCreateStandardInBound(Integer.valueOf(Config.getNumberForTestRecords()));
    }

    @When("^I Attempt to Post/Resubmit each record$")
    public void i_attempt_to_post_resubmit_each_record() throws Throwable {
        iDocPage.processAllSelectedPtsRecords();
    }

    @Then("^I verify if tax field at row number \"([0-9]*)\", column number \"([^\"]*)\" contains \"([^\"]*)\"$")
    public void i_verify_if_tax_field_at_row_number_contains(int rowNumber, String columnNumber, String taxCode) throws Throwable {
        iDocPage.verifyTaxFields(columnNumber, rowNumber, taxCode);
    }

    @When("I select \"([^\"]*)\" variant")
    public void i_select_variant(String variant) throws Throwable {
        iDocPage.selectVariantInDol(variant);
    }
}
