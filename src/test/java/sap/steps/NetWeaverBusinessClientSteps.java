package sap.steps;

import common.Config;
import common.Constants;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import sap.pages.CommonActions;
import sap.pages.NetWeaverBusinessClient;
import sap.pages.PurchaseOrder;
import utilities.Driver;

/**
 * Created by mnabizadeh on 7/09/18.
 */
public class NetWeaverBusinessClientSteps {

    private PurchaseOrder purchaseOrder;
    private CommonActions commonActions;
    private NetWeaverBusinessClient netWeaverBusinessClient;
    private Driver driver;

    public NetWeaverBusinessClientSteps(Driver driver) {
        this.driver = driver;
        purchaseOrder = new PurchaseOrder(driver);
        commonActions = new CommonActions(driver);
        netWeaverBusinessClient = new NetWeaverBusinessClient(driver);
    }

    @When("^I click on \"([^\"]*)\" tab and select \"([^\"]*)\"$")
    public void i_click_on_and_select(String tab, String option) throws Throwable {
        netWeaverBusinessClient.selectTabOption(tab, option);
    }

    @Then("^I verify \"([^\"]*)\" screen is displayed in NWBC$")
    public void i_verify_screen_is_displayed(String text) throws Throwable {
        driver.verifyTextDisplayed(NetWeaverBusinessClient.createDTDTransfer, text);

    }

    @When("^I enter the \"([^\"]*)\" to \"([^\"]*)\"$")
    public void i_enter_the_to(String field, String value) throws Throwable {
        purchaseOrder.enterValueIntoPOField(field, value);
    }

    @When("^I click the Save and Preview button in NWBC$")
    public void i_click_the_save_and_preview_button() throws Throwable {
        netWeaverBusinessClient.saveAndPrintPreview();
    }

    @Then("^I verify \"([^\"]*)\" PO gets created$")
    public void i_verify_po_gets_created(String type) throws Throwable {
        netWeaverBusinessClient.verifyDTDSTOTransfer(type);
    }

    @Then("^I save NWBC purchase order number to the scenario data$")
    public void i_save_nwbc_purchase_order_number_to_the_scenario_data() throws Throwable {
        netWeaverBusinessClient.saveNWBCPONumberToScenarioData();
    }

    @When("^I click on the \"([^\"]*)\" on NWBC Navigation bar$")
    public void i_click_on_the_on_nwbc_navigation_bar(String navBarSelection) throws Throwable {
        netWeaverBusinessClient.selectNWBCNavigationOption(navBarSelection);
    }

    @When("^I click on NWBC \"Other Purchase Order\" icon$")
    public void i_click_on_nwbc_other_purchase_order_icon() throws Throwable {
        netWeaverBusinessClient.clickNWBCOtherPurchaseOrderIcon();
    }

    @When("^I search for the previously saved \"([^\"]*)\" PO number$")
    public void i_search_for_the_previously_saved_po_number(String type) throws Throwable {
        String nwbcPONumber = netWeaverBusinessClient.getPoNumber(type);
        purchaseOrder.searchOtherPurchaseOrderNumber(nwbcPONumber);
    }

    @When("^I click on \"([^\"]*)\" tab$")
    public void i_click_on_tab(String tab) throws Throwable {
        netWeaverBusinessClient.selectTab(tab);
    }

    @When("^I perform a goods movement search for the previously created PO$")
    public void i_perform_goods_movement_search_for_previous_po() throws Throwable {
        netWeaverBusinessClient.searchGoodsMovementPO();
    }

    @When("^I fill out and execute a search for the employee user name$")
    public void i_fill_out_and_execute_search_for_employee_user_name() throws Throwable {
        netWeaverBusinessClient.executeEmployeeUserNameSearch();
    }

    @When("^I set the site for the user to \"([^\"]*)\"$")
    public void i_set_site_for_user_to_input(String site) throws Throwable {
        netWeaverBusinessClient.setSiteForUser(site);
    }

    @Then("^I verify the site update saved successfully and close the popup$")
    public void i_verify_site_update_saved_successfully_and_close_popup() throws Throwable {
        netWeaverBusinessClient.verifySiteUpdateSaved();
        netWeaverBusinessClient.closeSiteUpdatePopup();
    }

    @When("^I switch to the save confirmation popup iFrame$")
    public void i_switch_to_save_confirmation_popup_iframe() throws Throwable {
        netWeaverBusinessClient.switchToSavePopupiFrame();
    }

    @When("^I select the \"Check Goods\" button in NWBC$")
    public void i_select_the_check_goods_issue_button() throws Throwable {
        netWeaverBusinessClient.selectCheckGoodsIssue();
    }

    @When("^I click on \"([^\"]*)\" in NWBC page$")
    public void i_click_on_element(String elementName) throws Throwable {
        netWeaverBusinessClient.clickInNwbcPage(elementName);
    }

    @When("^I select \"([^\"]*)\" and enter \"([^\"]*)\" in NWBC page$")
    public void i_enter_value_in_input_field(String elementName, String input) throws Throwable {
        driver.performSendKeysWithActions(netWeaverBusinessClient.returnElement(elementName), input);
    }

    @When("^I click on the purchase order number for open returns in NWBC page$")
    public void i_click_on_the_open_return_purchase_order_number() throws Throwable {
        netWeaverBusinessClient.getPurchaseOrderFromOpenReturns();
    }

    @When("^I switch to RTV popup Iframe$")
    public void i_switch_to_RTV_popup_Iframe() throws Throwable {
        driver.switchFrameContext(netWeaverBusinessClient.rtviFrame);
    }

    @Then("^I verify RTV confirmation message$")
    public void i_verify_rtv_confirmation_message() throws Throwable {
        netWeaverBusinessClient.verifyRtvConfirmationInNwbcPage();
    }

    @When("^I clear text from ArticleDocument, CustomerInvoice, DocumentYear and click continue$")
    public void i_clear_text_article_document_customer_invoice_document_year_click_continue() throws Throwable {
        netWeaverBusinessClient.clearTextFromArticleDocumentCustomerInvoiceDocumentYearField();
    }

    @When("^I save the \"([^\"]*)\" PO$")
    public void i_save_the_po(String elementName) throws Throwable {
        netWeaverBusinessClient.savePurchaseOrderNumbersInNWBCPage(elementName);
    }

    @When("^I enter Purchase Order Number into \"([^\"]*)\"$")
    public void i_enter_purchase_order_number(String elementName) throws Throwable {
        netWeaverBusinessClient.getAndEnterPurchaseOrderNumberInNwbc(elementName);
    }

    @Then("^I verify if the delete indicator is visible$")
    public void i_verify_if_the_delete_indicator_is_visible() throws Throwable {
        netWeaverBusinessClient.verifyDeleteIndicatorIsVisible();
    }


    @Then("^I validate reversed RTV PO success message$")
    public void i_validate_reversed_rtv_po_success_message() throws Throwable {
        netWeaverBusinessClient.checkReversedRTVPOSuccessMessage();
    }

    @Then("^I verify article numbers are displayed under Purchase Order History Tab$")
    public void i_verify_article_numbers_are_displayed_under_purchase_order_history_tab() {
        netWeaverBusinessClient.verifyArticleNumberInPurchaseOrderHistoryTab();
    }

    @Then("^I save the Return purchase order$")
    public void i_save_the_return_purchase_order() throws Throwable {
        netWeaverBusinessClient.saveReturnPurchaseOrder();
    }

    @When("^I switch to \"([^\"]*)\" Iframe in NWBC$")
    public void i_switch_to_iframe_in_nwbc(String elementName) throws Throwable {
        netWeaverBusinessClient.switchToIframeInNwbc(elementName);
    }

    @When("^I enter \"([0-9]*)\" tracking numbers$")
    public void i_enter_all_the_tracking_number(int quantity) throws Throwable {
        netWeaverBusinessClient.inputTrackingNumbersInReturnInformation(quantity);
    }

    @Then("^I verify \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\", \"([^\"]*)\" and tracking numbers in return information tab$")
    public void i_verify_all_the_values_in_return_information_tab(String driverName, String rga, String bolpro, String originalPo) throws Throwable {
        netWeaverBusinessClient.verifyReturnInformationTabValues(driverName, rga, bolpro, originalPo);
    }

    @When("^I clear one tracking number at row \"([0-9]*)\"$")
    public void i_clear_the_tracking_number(int rowNumber) throws Throwable {
        netWeaverBusinessClient.clearTrackingNumber(rowNumber);
    }

    @Then("^I verify the \"([^\"]*)\" field has \"([^\"]*)\" value in NWBC page$")
    public void i_verify_field_has_value_in_nwbc_page(String elementName, String validation) throws Throwable {
        netWeaverBusinessClient.verifyValueAttributeInNwbc(elementName, validation);
    }

    @Then("^I verify the tracking numbers for RTV in SE16N$")
    public void i_verify_tracking_numbers_for_rtv() throws Throwable {
        netWeaverBusinessClient.validateTrackingNumbersInSe16n();
    }

    @And("^I enter comment \"([^\"]*)\" in Purchase Order popup$")
    public void i_enter_comment_in_purchase_order_popup(String value) throws Throwable {
        netWeaverBusinessClient.enterCommentInPurchaseOrderPopup(value);
    }

    @When("^I set the site to \"([^\"]*)\"$")
    public void i_set_the_site_to(String site) throws Throwable {
        String setSite = "";
        if (site.equalsIgnoreCase(Constants.STORE)) {
            if (Config.getDataSet().equalsIgnoreCase(Constants.QA)
                    || Config.getDataSet().equalsIgnoreCase(Constants.DEV)) {
                setSite = Constants.NWBC_RTK_SITE;
            } else if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
                setSite = Constants.NWBC_RTQ_SITE;
            }
        } else {
            setSite = site;
        }
        netWeaverBusinessClient.setTheSiteLocationInNwbc(setSite);
    }

    @When("^I set the site to \"([^\"]*)\" or get site from column name \"([^\"]*)\"$")
    public void i_set_the_site_to_or_get_the_site_from_column_name(String site, String columnName) throws Throwable {
        if(!Config.getSapDataSourceFlag().equalsIgnoreCase(CommonActions.DEFAULT)){
            site = commonActions.returnDataFromExcel(columnName);
        }
        String setSite = "";
        if (site.equalsIgnoreCase(Constants.STORE)) {
            if (Config.getDataSet().equalsIgnoreCase(Constants.QA)
                    || Config.getDataSet().equalsIgnoreCase(Constants.DEV)) {
                setSite = Constants.NWBC_RTK_SITE;
            } else if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
                setSite = Constants.NWBC_RTQ_SITE;
            }
        } else {
            setSite = site;
        }
        netWeaverBusinessClient.setTheSiteLocationInNwbc(setSite);
    }

    @Then("^I reset the site to default$")
    public void i_reset_the_site_to_default() throws Throwable {
        String site;
        if (Config.getDataSet().equals(Constants.STG)) {
            site = Constants.NWBC_RTQ_SITE;
        } else {
            site = Constants.NWBC_RTK_SITE;
        }
        netWeaverBusinessClient.setTheSiteLocationInNwbc(site);
    }

    @When("^I edit one of the row in the warehouse adjustment table and edit the Article details with the view as \"([^\"]*)\"$")
    public void i_select_one_of_the_row_in_the_warehouse_adjustment_table_and_edit_the_article_details(String elementName) throws Throwable {
        NetWeaverBusinessClient.counter = 3;
        NetWeaverBusinessClient.mimAdjustmentTableIndex = 3;
        netWeaverBusinessClient.editWareHouseMimAdjustmentArticles(elementName);
    }

    @Then("^I enter alphanumeric number in BOL/PRO# field$")
    public void i_enter_alphanumberic_number_in_BOLPRO() throws Throwable {
        netWeaverBusinessClient.enterBolProNumber();
    }

    @When("^I select the edited row in the warehouse adjustment table and click on \"([^\"]*)\" with the view as \"([^\"]*)\"$")
    public void i_select_the_edited_row_in_the_warehouse_adjustment_table(String elementName, String viewName) throws Throwable {

    }

    @Then("^I verify the success message with green image$")
    public void i_verify_the_success_message_with_green_image() throws Throwable {
        netWeaverBusinessClient.verifySuccessStatus();
    }

    @Then("^I verify the error message with red image$")
    public void i_verify_the_error_message_with_red_image() throws Throwable {
        netWeaverBusinessClient.verifyErrorStatus();
    }

    @When("^I save Article document number, Article document item and vendor$")
    public void i_save_article_document_number_article_document_item_and_vendor() throws Throwable {
        netWeaverBusinessClient.saveDetailsFromMimAdjustmentTable();
    }

    @Then("^I save the value of \"([^\"]*)\" field in details tab$")
    public void i_save_the_value_of_field_in_details_tab(String elementName) throws Throwable {
        netWeaverBusinessClient.saveDetailsFromMimAdjustmentTable(elementName);
    }

    @Then("^I verify \"([^\"]*)\" field has \"([^\"]*)\" displayed in NWBC page$")
    public void i_verify_matches_displayed_in_nwbc_page(String elementName, String text) throws Throwable {
        netWeaverBusinessClient.assertTextDisplayedInNWBC(elementName, text);
    }

    @When("^I search for purchase order number in mim adjustment table and select the line item$")
    public void i_search_for_purchase_order_number_in_mim_adjustment_table_and_select_the_line_item() throws Throwable {
        netWeaverBusinessClient.enterPurchaseOrderNumberIntoMimAdjustmentFilter();
    }

    @When("^I wait for \"([^\"]*)\" field to be visible in NWBC page$")
    public void i_wait_for_field_to_be_visible(String elementName) throws Throwable {
        driver.waitForElementVisible(netWeaverBusinessClient.returnElement(elementName), Constants.ONE_HUNDRED_TWENTY);
    }

    @When("^I enter details in Undo Scrap under Corrective Actions in NWBC$")
    public void i_enter_details_in_undo_scrap_under_corrective_actions_in_nwbc() throws Throwable {
        netWeaverBusinessClient.enterDetailsToUndoScrap();
    }

    @When("^I select and save the reason for movement under scrap goods movement$")
    public void i_select_and_save_the_reason_for_movement_under_scrap_goods_movement() throws Throwable {
        netWeaverBusinessClient.setReasonForGoodsMovement();
    }

    @When("^I select \"([^\"]*)\" and enter Article Document number from NWBC mim adjustment table's Details tab$")
    public void i_select_enter_article_document_number_from_nwbc_mim_adjustment_table_details_tab(String elementName) throws Throwable {
        netWeaverBusinessClient.enterArticleDocumentNumberInNwbc(elementName);
    }

    @Then("^I verify \"([^\"]*)\" element is displayed$")
    public void i_verify_element_is_displayed(String elementName) throws Throwable {
        netWeaverBusinessClient.isElementDisplayedInNwbc(elementName);
    }

    @And("^I verify the PDF has the saved Article document Number$")
    public void i_verify_the_PDF_has_the_saved_article_document_number() throws Throwable {
        netWeaverBusinessClient.verifyArticleNumberInPDF();
    }


    @When("^I select row number \"([0-9]*)\" in adjustment table$")
    public void i_select_row_number_in_adjustment_table(int rowNumber) throws Throwable {
        netWeaverBusinessClient.selectRowInTable(rowNumber);
    }

    @When("^I enter \"([^\"]*)\" of the line item into input field with label \"([^\"]*)\"$")
    public void i_enter_value_of_the_line_item_into_input_field_with_label(String key, String labelName) throws Throwable {
        commonActions.sendKeysWithLabelName(labelName, driver.scenarioData.getData(key));
    }
}