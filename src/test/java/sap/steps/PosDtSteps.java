package sap.steps;

import common.Config;
import common.Constants;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import sap.pages.CommonActions;
import sap.pages.IDocPage;
import sap.pages.PosDt;
import utilities.Driver;

public class PosDtSteps {

    private Driver driver;
    private PosDt posDt;
    private CommonActions commonActions;

    public PosDtSteps(Driver driver) {
        this.driver = driver;
        posDt = new PosDt(driver);
        commonActions = new CommonActions(driver);
    }

    @When("^I enter \"([^\"]*)\" information on \"([^\"]*)\" in posdt page$")
    public void i_enter_information_on_in_order_to_posdt_page(String value, String element) throws Throwable {
        commonActions.sendKeys(posDt.returnElement(element), value);
    }

    @When("^I enter the Transaction Number generated in legacy System in \"([^\"]*)\" field in posdt$")
    public void i_enter_the_transaction_number_generated_in_legacy_system_in_posdt(String elementName) throws Throwable {
        String input = driver.scenarioData.getData(IDocPage.IDOC_NUMBER_FROM_EXCEL).replaceAll("[^0-9]", "");
        commonActions.sendKeys(posDt.returnElement(elementName), input);
    }

    @When("^I enter the Transaction Number for layaway generated in legacy system in \"([^\"]*)\" field in Posdt$")
    public void i_enter_the_transaction_number_with_generated_in_legacy_system_in_posdt(String elementName) throws Throwable {
        commonActions.sendKeys(posDt.returnElement(elementName), "*" + driver.scenarioData.getData(IDocPage.IDOC_NUMBER_FROM_EXCEL) + "*");
    }

    @When("^I click on \"([^\"]*)\" in posdt page$")
    public void i_click_on_element(String elementName) throws Throwable {
        posDt.clickInPosDtPage(elementName);
    }

    @Then("^I verify \"([^\"]*)\" is displayed in posdt page$")
    public void i_verify_is_element_displayed(String elementName) throws Throwable {
        driver.isElementDisplayed(posDt.returnElement(elementName));
    }

    @Then("^I verify all the transactions are successful in posdt$")
    public void i_verify_all_the_transactions_are_successful() throws Throwable {
        posDt.verifyIfTransactionsAreSuccessful();
    }

    @Then("^I verify Article Identifier and Sales Price for Line items in sales data in posdt$")
    public void i_verify_article_identifier_and_sales_price_for_line_in_sales_data() throws Throwable {
        posDt.assertArticleAndSalesPriceOnSalesItemDataInPosDt();
    }

    @Then("^I verify Article Identifier for Line items in sales data in posdt$")
    public void i_verify_article_identifier_for_line_in_sales_data() throws Throwable {
        posDt.assertArticlesItemDataInPosDt();
    }

    @When("^I double click on Transaction Number in posdt$")
    public void i_double_click_on_transaction_number() throws Throwable {
        posDt.doubleClickOnValueAttribute(driver.scenarioData.getData(IDocPage.IDOC_NUMBER_FROM_EXCEL));
    }

    @When("^I click scroll \"([0-9]*)\" times in posdt$")
    public void i_click_element_times(int numberOfTimes) throws Throwable {
        posDt.clickMultipleTimes(numberOfTimes);
    }

    @Then("^I verify total transaction amount in posdt$")
    public void i_verify_total_transaction_amount() throws Throwable {
        posDt.verifyTotalTransactionAmount(commonActions.
                returnDataFromEnvironmentVariables(CommonActions.SHEET_NAME), Constants.fullPathFileNames.get(Constants.INVOICE_SAP));
    }

    @Then("^I verify total transaction amount in posdt from sheet name \"([^\"]*)\"$")
    public void i_verify_total_transaction_amount_in_posdt_from_sheet(String sheetName) throws Throwable {
        posDt.verifyTotalTransactionAmount(sheetName, Constants.fullPathFileNames.get(Constants.INVOICE_SAP));
    }

    @Then("^I verify total transaction amount in posdt from scenario data$")
    public void i_verify_total_transaction_amount_in_posdt_from_scenario_data() throws Throwable {
        posDt.verifyTotalTransactionAmount(driver.scenarioData.getData(CommonActions.SHEET_NAME_1), Constants.SAP_DATA_SOURCE_LOCATION +
                driver.scenarioData.getData(CommonActions.FOLDER_NAME) +
                "\\" + driver.scenarioData.getData(CommonActions.EXCEL_FILE_NAME));
    }

    @Then("^I verify total transaction amount in posdt from sheet name \"([^\"]*)\" and file name \"([^\"]*)\"$")
    public void i_verify_total_transaction_amount_in_posdt_from_sheet_and_file(String sheetName, String fileName) throws Throwable {
        posDt.verifyTotalTransactionAmount(sheetName, Constants.DATA_STORAGE_FOLDER + fileName);
    }

    @Then("^I verify \"([^\"]*)\" field has the text of \"(Transaction Type)\" in posdt$")
    public void i_verify_field_has_the_text_of_in_posdt(String elementName, String dataType) throws Throwable {
        driver.assertElementAttributeString(posDt.returnElement(elementName), Constants.VALUE, commonActions.returnDataFromEnvironmentVariables(dataType));
    }

    @Then("^I verify \"([^\"]*)\" field has the text of \"([^\"]*)\" from examples in posdt$")
    public void i_verify_field_has_the_text_of_in_posdt_from_examples(String elementName, String transactionType) throws Throwable {
        if (Config.getSapDataSourceFlag().equalsIgnoreCase(Constants.JENKINS)) {
            transactionType = Config.getTransactionType();
        }
        driver.assertElementAttributeString(posDt.returnElement(elementName), Constants.VALUE, transactionType);
    }

    @When("^I search for hybris order number in posdt and save it to scenario data$")
    public void i_search_for_hybris_order_number_and_save_it_to_scenario_data() throws Throwable {
        posDt.searchForHybrisOrderRelatedTransactionInPosDt();
    }

    @When("^I expand the first line item and save the purchase order number$")
    public void i_expand_the_first_line_item_and_save_the_purchase_order_number() throws Throwable {
        posDt.getPurchaseOrderNumberForWebOrders();
    }

    @When("^I save web order from environmental variables to scenario data$")
    public void i_save_web_ordeR_from_environmental_variables_to_scenario_data() throws Throwable {
        driver.scenarioData.setCurrentOrderNumber(Config.getHybrisWebOrderId());
    }

    @When("^I save the \"([^\"]*)\" element text value in PosDt page$")
    public void i_save_the_element_text_value_in_posdm_page(String elementName) throws Throwable {
        posDt.saveElementValueFromText(elementName);
    }

    @Then("^I check the field value of \"([^\"]*)\" in \"(ZVTV_VEHICLE|ZVTV_CUSTOMER)\" table for \"(vin|customerid)\" for \"([^\"]*)\" scenario$")
    public void i_check_the_field_value_of_in_zvtv_table_for_id(String fieldName, String table, String id, String scenario) throws Throwable {
        String idValue;
        if (id.equalsIgnoreCase(PosDt.VIN)) {
            idValue = driver.scenarioData.getData(PosDt.VIN);
        } else {
            idValue = driver.scenarioData.getData(PosDt.VTV_CUSTOMER_ID);
        }
        posDt.assertVtvVehicleTableValuesExcel(fieldName, table, idValue, scenario);
    }

    @When("^I select the input field with the label \"([^\"]*)\" at position \"([0-9]*)\" and enter \"(VIN|VTVCUSTOMERID|VEHICLEID)\" for \"([^\"]*)\" scenario$")
    public void i_select_input_field_with_label_at_position_and_enter_id_for_scenario(String labelName, int position, String valueName, String scenario) throws Throwable {
        posDt.returnRequiredValuesFromExcelForVTV(labelName, position, valueName, scenario);
    }
}