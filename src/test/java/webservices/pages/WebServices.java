package webservices.pages;

import common.Config;
import common.Constants;
import dtc.data.ConstantsDtc;
import dtc.data.Customer;
import dtc.pages.CommonActions;
import cucumber.api.DataTable;
import io.restassured.RestAssured;
import io.restassured.config.EncoderConfig;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.apache.commons.io.FileUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;
import org.junit.Assert;
import org.xml.sax.SAXException;
import utilities.CommonUtils;
import utilities.Driver;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static io.restassured.RestAssured.given;
import static java.lang.Integer.parseInt;
import static junit.framework.TestCase.assertTrue;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

public class WebServices {
    private Driver driver;
    private final Logger LOGGER = Logger.getLogger(WebServices.class.getName());
    private HttpEntity httpEntity;
    private HttpResponse httpResponse;
    private JSONParser parser;
    private Customer customer;
    private CommonActions commonActions;
    private String requestAvsJson;

    public WebServices(Driver driver) {
        parser = new JSONParser();
        customer = new Customer();
        this.driver = driver;
        commonActions = new CommonActions(driver);
    }

    public static String orderXmlResponseBody;
    private static final int GET_STATUS_CODE = 200;
    private static final int POST_STATUS_CODE = 200;
    private static final String LIST = "list";
    public JSONObject responseBodyJsonObject;
    public Response responseBodyInXml;
    private String lastNode;
    private String parentNode;
    private static final Pattern NODE_LIST_ARRAY_PATTERN = Pattern.compile("(.*?)\\[(\\d+)\\]");
    private static final String USER_DIR = "user.dir";
    private static final String JSON_FILE_PATH = "/src/test/resources/json_Objects";
    private static final String MY_ACCOUNT_CUSTOMER_ENDPOINT = "/enterprise/update/request?access_token=";
    private static final String EXIST = "exist";
    private static final String EXIST_NOT_EQUAL = "exist notEqual";
    public static final String MY_ACCOUNT_EMAIL_ID = "myAccountEmailId";
    public static final String TOKEN = "token";
    private static final String DATA_TOKEN = "data.token";
    private static final String MY_ACCOUNT_CUSTOMER_SERVICE_EMAIL = "/webservice/customer/?emailId=";
    private static final String WEBSERVICE_CUSTOMER = "/webservice/customer/";
    private static final String ORDERXML_SERVICE_EXTENDED = "/webservice/orders/enterprise/update/request?orderCode=";
    private static final String MY_ACCOUNT_CUSTOMER_SERVICE_TOKEN = "&access_token=";
    private static final String HTTP_ESB = "http://esb";
    public static final String AUTHORIZATIONSERVER = "/authorizationserver/";
    private static final String ADDRESS_SERVICE_EXTENDED = "is.trtc.com:5555/ws/DTC_CORE_UTILITY_VALIDATE_ADDRESS_v1_";
    private static final String VALIDATEADDRESS = "/validateAddress";
    public static boolean isPromo;
    public static boolean isFet;
    public static boolean isAppointment;
    public static int totalNumberOfArticles = 0;
    private static final String CUSTOMER_ADDRESS_1 = "Customer_address_1";
    private static final String CUSTOMER_CITY = "Customer_city";
    private static final String CUSTOMER_STATE = "Customer_state";
    private static final String CUSTOMER_ZIPCODE = "Customer_zipcode";
    private static final String CUSTOMER_COMPLETE_ADDRESS = "Customer_complete_address";
    private static final String CORRECT = "correct";
    private static final String IN_CORRECT = "in correct";
    private static final String INVALID = "invalid";
    private static final String JSON_AVS_COMPLETE_ADDRESS = "response_body.Addresses[0].CompleteAddress";
    private static final String AVS_MESSAGE = "response_body.Message";
    public static final String XML_VALUE_ACCEPT = "application/xml";

    /**
     * Asserts the response code for Get call
     *
     * @param URI - URI of the Web Application
     */
    public void assertGetResponseForWebServiceUrl(String URI) {
        LOGGER.info("getResponseForWebServiceUrl started");
        given().get(URI).then().statusCode(GET_STATUS_CODE);
        LOGGER.info("getResponseForWebServiceUrl completed");
    }

    /**
     * Asserts the response of post call
     *
     * @param baseURI  - URI of Web Service
     * @param endPoint - End Point
     * @param fileName - FileName of the json file in saved in resources directory
     */
    public void assertPostResponseForWebServiceUrl(String baseURI, String endPoint, String fileName) {
        LOGGER.info("postResponseForWebServiceUrl started");
        String filepath;
        filepath = System.getProperty(Constants.USER_DIR) + Constants.PATH_TO_JSON_OBJECTS + fileName;
        File file = new File(filepath);
        given()
                .headers(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON).body(file).
                when()
                .post(baseURI + endPoint).
                then()
                .statusCode(POST_STATUS_CODE);
        LOGGER.info("postResponseForWebServiceUrl completed");
    }

    /**
     * Gets the resposne body and count of node from request call
     *
     * @param key - Key name on wich value is retrieved
     * @param URI - URI of the Web Application
     */
    public void getBodyAndSaveData(String key, String URI) {
        LOGGER.info("getBodyAndSaveData started");
        driver.scenarioData.genericData.put(key, String.valueOf(given().get(URI).getBody().jsonPath().getList(LIST).size()));
        LOGGER.info("getBodyAndSaveData completed");
    }

    /**
     * Asserts the response of post call and saves token id in scenario data
     */
    public void postResponseForHttpsSecureTokenWebServiceUrl() {
        LOGGER.info("postResponseForHttpsSecureTokenWebServiceUrl started");
        RestAssured.baseURI = returnServiceUrl(Constants.AUTHORIZATION_SERVICE);
        RestAssured.useRelaxedHTTPSValidation();
        RequestSpecification request = RestAssured.given();
        Response response = request.config(RestAssured.config().encoderConfig(EncoderConfig.encoderConfig().
                encodeContentTypeAs(Constants.CONTENT_TYPE, ContentType.URLENC))).
                contentType(ContentType.URLENC.withCharset(Constants.CHARSET)).formParam(Constants.GRANT_TYPE_KEY, Constants.
                CLIENT_CREDENTIALS).formParam(Constants.SCOPE_KEY, Constants.BASIC).formParam(Constants.CLIENT_ID_KEY, Constants.
                QUALITY_ANALYSIS).formParam(Constants.CLIENT_SECRET_KEY, Constants.SECRET).
                formParam(Constants.CONTENT_TYPE, Constants.CONTENT_TYPE_VAL)
                .when().post(Constants.OAUTH_TOKEN);
        response.getBody().asString();
        String tokenId = response.getBody().jsonPath().get(Constants.ACCESS_TOKEN).toString();
        driver.scenarioData.genericData.put(Constants.ACCESS_TOKEN_VAL, tokenId);
        String actualStatusCode = Integer.toString(response.getStatusCode());
        Assert.assertTrue("Fail: Status code (" + actualStatusCode + ") is not equal to expected status code (" +
                        Integer.toString(POST_STATUS_CODE) + ").",
                actualStatusCode.equalsIgnoreCase(Integer.toString(POST_STATUS_CODE)));
        LOGGER.info("postResponseForHttpsSecureTokenWebServiceUrl completed for TokenID: " + tokenId +
                ". Status code: " + actualStatusCode);
    }

    /**
     * Asserts the response of get call and gets the price from response body
     *
     * @param endpoint          - End Point
     * @param article           - article number to check the price
     * @param customerPriceList - Customer Price List of the updated article price
     * @param catalogVersion    - Catalog version of the updated article
     */
    public void getResponseForPricingEndpoint(String endpoint, String article, String customerPriceList, String catalogVersion) {
        LOGGER.info("getResponseForPricingEndpoint started");
        RestAssured.baseURI = Config.returnPriceUrl();
        RestAssured.useRelaxedHTTPSValidation();
        RequestSpecification request = RestAssured.given();
        Response response = request.get(endpoint + "/" + article + "/" + "price" + "/" + customerPriceList + "/" + catalogVersion + "?access_token=" + driver.scenarioData.genericData.get(Constants.ACCESS_TOKEN_VAL));
        String getResponse = response.getBody().asString();
        String updatedPrice = response.getBody().xmlPath().get(Constants.XML_PARAM_BODY).toString();
        int statusCode = response.getStatusCode();
        Assert.assertTrue("Fail: Status code is not equal to 200 ", String.valueOf(statusCode).equalsIgnoreCase(String.valueOf(GET_STATUS_CODE)));
        driver.scenarioData.genericData.put(Constants.UPDATED_PRICE_VAL, updatedPrice);
        LOGGER.info("getResponseForPricingEndpoint completed " + "Response Body is: " + getResponse + "Online Hybris Price is = " + updatedPrice);
    }

    /**
     * Get version number from the response for Get call
     *
     * @param endPoint - End Point
     * @return String   - The version number value from the webservice request
     */
    public static String getResponseForBuildNumberLookUpWebServiceUrl(String endPoint) {
        String Url = Config.returnBuildNumberLookupURL();
        if (Url == null)
            return null;
        RestAssured.baseURI = Url;
        RequestSpecification request = RestAssured.given();
        request.relaxedHTTPSValidation();
        Response response;
        try {
            response = request.get(endPoint);
        } catch (Exception e) {
            return null;
        }
        if (response.getStatusCode() != GET_STATUS_CODE) {
            return null;
        }
        try {
            return response.getBody().jsonPath().get(Constants.VERSION_NUMBER).toString();
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Method will make a get request and saves the entity
     *
     * @param siteRegionId - discounttire, discounttiredirect
     * @throws Throwable - Exception
     */
    public void assertResponseForOrderLookUpService(String siteRegionId) throws Throwable {
        LOGGER.info("assertResponseForOrderLookUpService started");
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(returnUrlForOrderLookUp(siteRegionId));
        httpResponse = httpClient.execute(httpGet);
        Assert.assertTrue("Fail: Status code is not equal to 200 ", String.valueOf(httpResponse.
                getStatusLine().getStatusCode()).
                equalsIgnoreCase(String.valueOf(GET_STATUS_CODE)));
        httpEntity = httpResponse.getEntity();
        String responseBody = EntityUtils.toString(httpEntity);
        responseBodyJsonObject = new JSONObject(responseBody);
        LOGGER.info("assertResponseForOrderLookUpService completed");
    }

    /**
     * Method will return the URL for hybris order service based on siteRegion, Order number and access token
     *
     * @param siteRegionId - discounttire, discounttiredirect
     * @return - URL for get request
     */
    private String returnUrlForOrderLookUp(String siteRegionId) {
        if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            return Constants.WEB_STAGE_BASE_URL + "/extovcocc/v2/" + siteRegionId + "/orders/" +
                    driver.scenarioData.getCurrentOrderNumber() + "?access_token=" +
                    driver.scenarioData.genericData.get(Constants.ACCESS_TOKEN_VAL);
        } else {
            return Config.getBaseUrl() + "/extovcocc/v2/" + siteRegionId + "/orders/" +
                    driver.scenarioData.getCurrentOrderNumber() + "?access_token=" +
                    driver.scenarioData.genericData.get(Constants.ACCESS_TOKEN_VAL);
        }
    }

    /**
     * This method will save Total Price, all article codes, quantities, and values to scenario data
     *
     * @throws Throwable - Exception
     */
    public void parseJsonAndSaveValues() throws Throwable {
        LOGGER.info("parseJsonAndSaveValues started");
        String orderNumber = driver.scenarioData.getCurrentOrderNumber();

        JSONObject totalDiscounts = responseBodyJsonObject.getJSONObject(Constants.JSON_PARAM_DISCOUNTS);
        isPromo = Integer.valueOf(totalDiscounts.get(Constants.JSON_PARAM_VALUE).toString().replace(".", "")) != 0;
        driver.scenarioData.setData(Constants.SITE_ORDER_SERVICE_KEY, responseBodyJsonObject.get(Constants.JSON_PARAM_SITE).toString());
        LOGGER.info("site id for order number : " + orderNumber + " is: " +
                driver.scenarioData.getData(Constants.SITE_ORDER_SERVICE_KEY));

        //This object has currencyIso, value, priceType, formattedValue.
        JSONObject totalPriceWithTax = responseBodyJsonObject.getJSONObject(Constants.JSON_PARAM_TOTAL_PRICE_WITH_TAX);
        driver.scenarioData.setData(Constants.TOTAL_PRICE_ORDER_SERVICE_KEY, totalPriceWithTax.get(Constants.
                JSON_PARAM_VALUE).toString());
        driver.scenarioData.setData(Constants.TOTAL_AMOUNT_EXCEL_COLUMN_LABEL, totalPriceWithTax.get(Constants.
                JSON_PARAM_VALUE).toString());
        LOGGER.info("Total Price with tax for order number: " + orderNumber + " is: " +
                driver.scenarioData.getData(Constants.TOTAL_PRICE_ORDER_SERVICE_KEY));

        if (Config.getSiteRegion().equalsIgnoreCase(Constants.DTD)) {
            //This object has currencyIso, value, priceType, formattedValue.
            JSONObject deliveryCost = responseBodyJsonObject.getJSONObject(Constants.JSON_PARAM_DELIVERY_COST);
            driver.scenarioData.setData(Constants.SHIPPING_AMOUNT_SERVICE_KEY, deliveryCost.get(Constants.JSON_PARAM_VALUE).toString());
            LOGGER.info("Shipping Price for order number: " + orderNumber + " is: " +
                    driver.scenarioData.getData(Constants.SHIPPING_AMOUNT_SERVICE_KEY));
        }

        //This array has all the arrays under unconsignedEntries field
        JSONArray jsonArray = responseBodyJsonObject.getJSONArray(Constants.JSON_PARAM_UNCONSIGNED);
        totalNumberOfArticles = jsonArray.length();
        for (int i = 0; i < jsonArray.length(); i++) {

            //This object contains entryNumber, quantity, totalprice[], product[[]],deliverypointOfService[].
            JSONObject json1 = jsonArray.getJSONObject(i);

            if (i == 0 && driver.scenarioData.getData(Constants.SITE_ORDER_SERVICE_KEY).
                    equalsIgnoreCase(Constants.SITE_ID_DT) && !isAppointment) {
                JSONObject deliveryPointOfService = (JSONObject) json1.get(Constants.JSON_PARAM_DELIVERY_POINT_OF_SERVICE);
                driver.scenarioData.setData(Constants.STORE_NUMBER_ORDER_SERVICE_KEY, deliveryPointOfService.
                        get(Constants.JSON_PARAM_NAME).toString());
                LOGGER.info("Delivery Point of service for order number: " + orderNumber + " is: " +
                        driver.scenarioData.getData(Constants.STORE_NUMBER_ORDER_SERVICE_KEY));
            }

            //This object contains code, name, url, purchasable, stock[], availableForPickUp
            JSONObject product = (JSONObject) json1.get(Constants.JSON_PARAM_PRODUCT);

            //This object contains currencyIso, value
            JSONObject totalprice = (JSONObject) json1.get(Constants.JSON_PARAM_TOTAL_PRICE);


            driver.scenarioData.setData(Constants.QUANTITY_ORDER_SERVICE_KEY + i, json1.get(Constants.
                    JSON_PARAM_QUANTITY).toString());
            driver.scenarioData.setData(Constants.CODE_ORDER_SERVICE_KEY + i, product.getString(Constants.JSON_PARAM_TOTAL_CODE));
            if (driver.scenarioData.getData(Constants.CODE_ORDER_SERVICE_KEY + i).toLowerCase().contains(Constants.FET)) {
                isFet = true;
            }
            driver.scenarioData.setData(Constants.VALUE_ORDER_SERVICE_KEY + i, totalprice.get(Constants.
                    JSON_PARAM_VALUE).toString());
            LOGGER.info("Quantity for line item: " + (i + 1) + " is: " + driver.scenarioData.getData(Constants.
                    QUANTITY_ORDER_SERVICE_KEY + i));
            LOGGER.info("Article Code for line item: " + (i + 1) + " is: " + driver.scenarioData.getData(Constants.
                    CODE_ORDER_SERVICE_KEY + i));
            LOGGER.info("Article value for line item: " + (i + 1) + " is: " + driver.scenarioData.getData(Constants.
                    VALUE_ORDER_SERVICE_KEY + i));
        }
        LOGGER.info("parseJsonAndSaveValues completed");
    }

    /**
     * Verifies the order entries
     *
     * @param orderType    service Order/Order
     * @param orderEntries product codes or service product codes
     */
    public void assertEntriesInOrderLookUpService(String orderType, String orderEntries) throws Throwable {
        LOGGER.info("assertEntriesInOrderLookUpService started");
        JSONArray jsonArray = null;
        if (driver.scenarioData.isStaggeredProduct()) {
            jsonArray = responseBodyJsonObject.getJSONArray(Constants.JSON_PARAM_PICKUP_ORDER_GROUPS).
                    getJSONObject(0).getJSONArray(Constants.JSON_PARAM_ENTRIES);
        } else {
            jsonArray = responseBodyJsonObject.getJSONArray(Constants.JSON_PARAM_ENTRIES);
        }
        for (int i = 0; i < jsonArray.length(); i++) {
            boolean found = false;
            String productCode = orderEntries;
            String orderNumber = driver.scenarioData.getCurrentOrderNumber();
            String entryName = "entries[" + i + "].product.code";
            if (driver.scenarioData.isStaggeredProduct()) {
                entryName = "pickupOrderGroups[0]." + entryName;
            }
            if (CommonUtils.containsIgnoreCase(orderEntries, ConstantsDtc.PRODUCT)) {
                StringBuilder productCodes = new StringBuilder();
                for (int j = 0; j < driver.scenarioData.productInfoList.size(); j++) {
                    if (Boolean.parseBoolean(commonActions.productInfoListGetValue(ConstantsDtc.IN_CART, j))) {
                        productCode = commonActions.productInfoListGetValue(ConstantsDtc.ITEM, j);
                        if (productCode.contains(getNodeValue(responseBodyJsonObject, entryName))) {
                            found = true;
                            break;
                        }
                    }
                    productCodes.append(productCode).append(", ");
                }
                if (!found) {
                    productCodes.setLength(productCodes.length() - 2);
                    productCode = String.valueOf(productCodes);
                }
            } else {
                found = productCode.contains(getNodeValue(responseBodyJsonObject, entryName));
            }
            Assert.assertTrue("Fail: The expected entry/entries for " + productCode + " not found with the order: "
                    + orderNumber, found);
            LOGGER.info("The expected entry for " + productCode + " was found for order " + orderNumber);
        }
        if (orderType.contains(Constants.SERVICE)) {
            String totalValue = Constants.JSON_PARAM_TOTAL_PRICE + "." + Constants.JSON_PARAM_VALUE;
            Assert.assertTrue("Fail: The total price value " + getNodeValue(responseBodyJsonObject, totalValue)
                            + " is not zero for service order: " + driver.scenarioData.getCurrentOrderNumber(),
                    getNodeValue(responseBodyJsonObject, totalValue).equals("0.0"));
        }
        LOGGER.info("assertEntriesInOrderLookUpService completed");
    }

    /**
     * method retrives Node values from Json Object
     *
     * @param jsonObject = Response Json Object
     * @param nodeName   = Attribute name which we want to check
     * @return String = Returns Node value
     */
    public String getNodeValue(JSONObject jsonObject, String nodeName) throws JSONException {
        LOGGER.info("getNodeValue started");
        JSONObject jsonObj = getCorrectJsonObject(jsonObject, nodeName);
        LOGGER.info("getNodeValue completed");
        return jsonObj.get(lastNode).toString();
    }

    /**
     * Returns the correct JsonObject of the parent attribute name
     *
     * @param jsonObj  = Response Json Object
     * @param nodeName = Attribute name which we want to check
     * @return JSONObject = Returns value in Json Object.
     */
    private JSONObject getCorrectJsonObject(JSONObject jsonObj, String nodeName) throws JSONException {
        LOGGER.info("getCorrectJsonObject started");
        lastNode = nodeName;
        if (nodeName.contains(".")) {
            List<String> nodeList = getJsonNodesList(nodeName);
            lastNode = nodeList.get(nodeList.size() - 1);
            parentNode = nodeList.get(0);
            for (int i = 0; i < nodeList.size() - 1; i++) {
                String node = nodeList.get(i);
                Matcher matcher = NODE_LIST_ARRAY_PATTERN.matcher(node);
                if (matcher.matches()) {
                    String arrayNode = matcher.group(1);
                    int index = parseInt(matcher.group(2));
                    jsonObj = (JSONObject) jsonObj.getJSONArray(arrayNode).get(index);
                } else {
                    jsonObj = jsonObj.getJSONObject(nodeList.get(i));
                }
            }
        }
        LOGGER.info("getCorrectJsonObject completed");
        return jsonObj;
    }

    /**
     * Method will return the URL for customer service based on siteRegion and dataSet
     *
     * @param service Service name
     * @return - URL for get request
     */
    private String returnServiceUrl(String service) {
        LOGGER.info("returnServiceUrl started for " + service);
        String baseURL = "";
        if (Config.getDataSet().equalsIgnoreCase(Constants.STG))
            baseURL = Constants.WEB_STAGE_BASE_URL;
        else
            baseURL = Config.getBaseUrl();

        if (baseURL.endsWith("/"))
            baseURL = baseURL.substring(0, baseURL.length() - 1);

        String extendedPath = "";
        String id = "";
        switch (service) {
            case Constants.CUSTOMER_SERVICE:
                extendedPath = WEBSERVICE_CUSTOMER;
                id = driver.scenarioData.genericData.get(Constants.CUSTOMER_ID);
                break;
            case Constants.ORDER_XML_SERVICE:
                extendedPath = ORDERXML_SERVICE_EXTENDED;
                id = driver.scenarioData.getCurrentOrderNumber();
                break;
            case Constants.AUTHORIZATION_SERVICE:
                extendedPath = AUTHORIZATIONSERVER;
                break;
            case Constants.AVS_SERVICE:
                baseURL = HTTP_ESB + Config.getDataSet();
                extendedPath = ADDRESS_SERVICE_EXTENDED + Config.getDataSet().toUpperCase() + VALIDATEADDRESS;
                break;
            case Constants.MYACCOUNT_CUSTOMER_SERVICE:
                extendedPath = MY_ACCOUNT_CUSTOMER_SERVICE_EMAIL
                        + driver.scenarioData.genericData.get(MY_ACCOUNT_EMAIL_ID) + MY_ACCOUNT_CUSTOMER_SERVICE_TOKEN;
                id = driver.scenarioData.genericData.get(Constants.ACCESS_TOKEN_VAL);
                break;
            default:
                Assert.fail("FAIL: Could not find node name that matched string passed from step");
        }
        String returnVal = baseURL + extendedPath + id;
        LOGGER.info("returnServiceUrl completed for " + service + ". Returning '" + returnVal + "'");
        return returnVal;
    }

    /**
     * Method verifies the values in Json response body
     *
     * @param presence : Parameter can be "exist" or "exist notEqual" or
     * @param table    : Response type can be JSON or XML or String.
     */
    public void verifyFollowingAttributesInResponse(String presence, DataTable table) throws Throwable {
        LOGGER.info("verifyFollowingAttributesInResponse " + presence + " started");
        for (List<String> rows : table.raw()) {
            String attribute = rows.get(0);
            String value = rows.get(1);
            if (presence.equals(EXIST) || presence.equals(EXIST_NOT_EQUAL))
                assertTrue("Expected: " + attribute + " Not Found", isNodePresent(responseBodyJsonObject, attribute));
            else
                assertFalse("Not Expected: " + attribute + " But Found", isNodePresent(responseBodyJsonObject, attribute));
            if (!(value.isEmpty())) {
                if (presence.equals(EXIST)) {
                    assertEquals(attribute + " didn't match expected", value, getNodeValue(responseBodyJsonObject, attribute));
                }
            } else if (presence.equals(EXIST_NOT_EQUAL)) {
                assertNotEquals(value, getNodeValue(responseBodyJsonObject, attribute));
            }
        }
        LOGGER.info("verifyFollowingAttributesInResponse " + presence + " completed");
    }

    /**
     * Checks if Node is present in response JsonObject
     *
     * @param jsonObject = Response Json Object
     * @param nodeName   = Attribute name which we want to check
     * @return boolean = True or false
     */
    public boolean isNodePresent(JSONObject jsonObject, String nodeName) throws JSONException {
        LOGGER.info("isNodePresent for " + nodeName + " started");
        JSONObject jsonObj = getCorrectJsonObject(jsonObject, nodeName);
        LOGGER.info("isNodePresent for " + nodeName + " completed");
        return jsonObj.has(lastNode);
    }

    /**
     * Returns list of all the child nodes for the parent attribute
     *
     * @param nodeName = Attribute name which we want to check
     * @return List = Returns Node value
     */
    private List<String> getJsonNodesList(String nodeName) {
        LOGGER.info("getJsonNodesList " + nodeName + " started");
        String[] nodeList = nodeName.split("\\.");
        LOGGER.info("getJsonNodesList " + nodeName + " completed");
        return Arrays.asList(nodeList);
    }

    /**
     * Verifies the XMl file with XSD schema file
     *
     * @param schemaXsdFileName   = schema file in .xsd file
     * @param responseXmlFileName = responsedata file in .xml
     */
    public void xmlSchemaValidation(String schemaXsdFileName, String responseXmlFileName) throws Exception {
        LOGGER.info("xmlSchemaValidation for " + schemaXsdFileName + " and " + responseXmlFileName + " started");
        String schemaXsdfilepath, responseXmlFilePath;
        schemaXsdfilepath = System.getProperty(USER_DIR) + JSON_FILE_PATH + schemaXsdFileName;
        responseXmlFilePath = System.getProperty(USER_DIR) + JSON_FILE_PATH + responseXmlFileName;
        try {
            SchemaFactory factory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
            Schema schema = factory.newSchema(new File(schemaXsdfilepath));
            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(responseXmlFilePath)));
        } catch (IOException | SAXException e) {
            System.out.println("Exception: " + e.getMessage());
        }
        LOGGER.info("xmlSchemaValidation for " + schemaXsdFileName + " and " + responseXmlFileName + " completed");
    }


    /**
     * Method does GET call to the customer service.
     */
    public void getCustomerService() throws Exception {
        LOGGER.info("getCustomerService started");
        RestAssured.baseURI = returnServiceUrl(Constants.CUSTOMER_SERVICE);
        RestAssured.useRelaxedHTTPSValidation();
        RequestSpecification request = RestAssured.given();
        responseBodyInXml = request.get(MY_ACCOUNT_CUSTOMER_ENDPOINT + driver.scenarioData.genericData.get(Constants.ACCESS_TOKEN_VAL));
        LOGGER.info("getCustomerService completed");
    }

    /**
     * Verifies the XMl data from step definition
     *
     * @param table : List of values passed from step definition
     */
    public void makeSureFollowingAttributesInResponseXml(DataTable table) throws Throwable {
        LOGGER.info("makeSureFollowingAttributesInResponseXml started");
        for (List<String> row : table.raw()) {
            String attribute = row.get(0);
            String value = row.get(1);
            assertEquals(attribute + " didn't match expected", responseBodyInXml.getBody().xmlPath().get(attribute).toString(), value);
        }
        LOGGER.info("makeSureFollowingAttributesInResponseXml completed");
    }

    /**
     * Method will make a call to My Account service with email and token passed
     *
     * @param emailValue : email Id whose token has to added for email auth and password reset
     */
    public void getResponseForMyAccountCustomerService(String emailValue) throws Throwable {
        LOGGER.info("getResponseForMyAccountCustomerService started for email Id = " + emailValue);
        RestAssured.useRelaxedHTTPSValidation();
        RequestSpecification request = RestAssured.given();
        Response response = request.get(returnServiceUrl(Constants.MYACCOUNT_CUSTOMER_SERVICE));
        String token = response.getBody().jsonPath().get(DATA_TOKEN).toString();
        driver.scenarioData.genericData.put(TOKEN, token);
        LOGGER.info("getResponseForMyAccountCustomerService completed for email Id = " + emailValue);
    }

    /**
     * This method changes the address field on request Json
     *
     * @param fileName name of the file to be sent as request body
     */
    public void addAJsonRequestDataFromFile(String fileName, String customerType, String addressType) throws Exception {
        LOGGER.info("addAJsonRequestDataFromFile started for file name " + fileName);
        String filepath;
        String completeAddress = "";
        filepath = System.getProperty(Constants.USER_DIR) + Constants.PATH_TO_JSON_OBJECTS + fileName;
        File file = new File(filepath);
        requestAvsJson = FileUtils.readFileToString(file, Constants.UTF8);
        if (addressType.equalsIgnoreCase(CORRECT)) {
            completeAddress = completeAddress + customer.getCustomer(customerType).address1;
            requestAvsJson = requestAvsJson.replace(CUSTOMER_ADDRESS_1, customer.getCustomer(customerType).address1);
        }
        if (addressType.equalsIgnoreCase(IN_CORRECT)) {
            completeAddress = completeAddress + customer.getCustomer(customerType).incorrectAddress1;
            requestAvsJson = requestAvsJson.replace(CUSTOMER_ADDRESS_1, customer.getCustomer(customerType).incorrectAddress1);
        }
        if (addressType.equalsIgnoreCase(INVALID)) {
            completeAddress = completeAddress + customer.getCustomer(customerType).invalidAddress;
            requestAvsJson = requestAvsJson.replace(CUSTOMER_ADDRESS_1, customer.getCustomer(customerType).invalidAddress);
        }
        if (requestAvsJson.contains(CUSTOMER_CITY)) {
            completeAddress = completeAddress + "," + customer.getCustomer(customerType).city;
            requestAvsJson = requestAvsJson.replace(CUSTOMER_CITY, customer.getCustomer(customerType).city);
        }
        if (requestAvsJson.contains(CUSTOMER_STATE)) {
            completeAddress = completeAddress + "," + customer.getCustomer(customerType).state;
            requestAvsJson = requestAvsJson.replace(CUSTOMER_STATE, customer.getCustomer(customerType).state);
        }
        if (requestAvsJson.contains(CUSTOMER_ZIPCODE)) {
            completeAddress = completeAddress + " " + customer.getCustomer(customerType).zip;
            requestAvsJson = requestAvsJson.replace(CUSTOMER_ZIPCODE, customer.getCustomer(customerType).zip);
        }
        if (requestAvsJson.contains(CUSTOMER_COMPLETE_ADDRESS)) {
            requestAvsJson = requestAvsJson.replace(CUSTOMER_COMPLETE_ADDRESS, completeAddress);
        }
        LOGGER.info("addAJsonRequestDataFromFile completed for file name " + fileName);
    }

    /**
     * This method does GET call to AVS service and return Complete address from response body
     *
     * @param addressType : Type of address , can be "correct" , "in correct" and "invalid"
     */
    public String getAvsResponseBody(String addressType) throws Throwable {
        LOGGER.info("getAvsResponseBody started for address type = " + addressType);
        String address = null;
        CloseableHttpClient client = HttpClients.createDefault();
        HttpPost httpPost = new HttpPost(returnServiceUrl(Constants.AVS_SERVICE));
        StringEntity entity = new StringEntity(requestAvsJson);
        httpPost.setEntity(entity);
        httpPost.setHeader(Constants.ACCEPT, Constants.APPLICATION_JSON);
        httpPost.setHeader(Constants.CONTENT_TYPE, Constants.APPLICATION_JSON);
        CloseableHttpResponse response = client.execute(httpPost);
        Assert.assertTrue("Fail: Status code is not equal to 200 ", String.valueOf(response.getStatusLine().getStatusCode()).
                equalsIgnoreCase(String.valueOf(GET_STATUS_CODE)));
        String responseBody = EntityUtils.toString(response.getEntity());
        JSONObject obj = new JSONObject(responseBody);
        client.close();
        if (addressType.equalsIgnoreCase(CORRECT) | addressType.equalsIgnoreCase(IN_CORRECT)) {
            address = getNodeValue(obj, JSON_AVS_COMPLETE_ADDRESS);
        } else if (addressType.equalsIgnoreCase(INVALID)) {
            address = getNodeValue(obj, AVS_MESSAGE);
        }
        LOGGER.info("getAvsResponseBody completed for address type = " + addressType);
        return address;
    }

    /**
     * Returns CAR Order History service type - Header/Item
     *
     * @param serviceName - Header/Item
     * @return - End Point
     */
    private String returnCarOrderHistoryServiceType(String serviceName) {
        LOGGER.info("returnCarOrderHistoryServiceType started");
        if (serviceName.equalsIgnoreCase("header")) {
            return Constants.CAR_HEADER_SERVICE_END_POINT;
        } else if (serviceName.equalsIgnoreCase("Item")) {
            return Constants.CAR_ITEM_SERVICE_END_POINT;
        } else {
            Assert.fail("Please specify 'header' or 'Item'");
            return null;
        }
    }

    /**
     * Returns CAR Order History service Url - Header/Item
     *
     * @param serviceName - Header/Item
     * @return - Url
     */
    private String returnCarOrderHistoryServiceUrl(String serviceName) {
        LOGGER.info("returnCarOrderHistoryServiceUrl started");
        if (Config.getDataSet().equalsIgnoreCase(Constants.QA)) {
            return Constants.HTTP + Constants.SAP_CAQ_SERVER + returnCarOrderHistoryServiceType(serviceName);
        }else if (Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            return Constants.HTTP + Constants.SAP_CAS_SERVER + returnCarOrderHistoryServiceType(serviceName);
        } else {
            Assert.fail("Please specify -DdataSet or Add implementation");
            return null;
        }
    }

    /**
     * Returns Site number
     *
     * @return - Site number
     */
    private String returnSiteNumber() {
        LOGGER.info("returnSiteNumber started");
        if (Config.getSiteRegion().equalsIgnoreCase(Constants.DT) && !WebServices.isAppointment) {
            return driver.scenarioData.getData(Constants.STORE_NUMBER_ORDER_SERVICE_KEY);
        } else if (Config.getSiteRegion().equalsIgnoreCase(Constants.DTD) && Config.getDataSet().equalsIgnoreCase(Constants.QA)) {
            return Constants.AZ_DC;
        } else if (Config.getSiteRegion().equalsIgnoreCase(Constants.DTD) && Config.getDataSet().equalsIgnoreCase(Constants.STG)) {
            return Constants.OH_DC;
        }else if (WebServices.isAppointment) {
            if (Config.getSiteRegion().equalsIgnoreCase(Constants.DT) && Config.getDataSet().equalsIgnoreCase(Constants.QA)) {
                return Constants.NWBC_RTK_SITE ;
            } else {
                return Constants.NWBC_RTQ_SITE;
            }
        } else {
            Assert.fail("Please specify -DdataSet and -DsiteRegion");
            return null;
        }

    }

    /**
     * Returns request body for Car order history Item Service
     *
     * @param fieldName - Field Name
     * @return - Request body
     */
    private String returnItemLevelOrderHistoryServiceRequestBody(String fieldName) {
        LOGGER.info("returnItemLevelOrderHistoryServiceRequestBody started");
        return "{\n" +
                "  \"IV_RETAILSTOREID\": \"" + "000000" + returnSiteNumber() + "\",\n" +
                "  \"IT_INVOICE_NUMBER\": [\n" +
                "    {\n" +
                "      \"FIELDNAME\": \"" + fieldName + "\",\n" +
                "      \"FIELDVALUE\": \"" + driver.scenarioData.getCurrentOrderNumber() + "\"\n" +
                "    }\n" +
                "  ]\n" +
                "}";
    }

    /**
     * Returns request body for Car order history Header service
     *
     * @param customerNumber - Customer Number
     * @return - Request body
     */
    private String returnHeaderLevelOrderHistoryServiceRequestBody(String customerNumber) {
        LOGGER.info("returnHeaderLevelOrderHistoryServiceRequestBody started");
        return "{\n" +
                "\"IV_CUSTOMER_ID\" : \"" + customerNumber + "\"\n" +
                "}";
    }

    /**
     * Returns JSON response for Car Order history Header Service
     *
     * @param customerNumber - Customer number
     * @return - JSON response in String format for Item service
     * @throws Throwable
     */
    private String returnJsonResponseForHeaderLevelOrderHistoryServiceRequestBody(String customerNumber) throws Throwable {
        LOGGER.info("returnJsonResponseForHeaderLevelOrderHistoryServiceRequestBody started");
        String password = driver.scenarioData.getData("sappassword");
        if (password.isEmpty())
            password = Config.getSapPassword();
        return given().auth().basic(Config.getSapUserName(), password)
                .body(returnHeaderLevelOrderHistoryServiceRequestBody(customerNumber))
                .post(returnCarOrderHistoryServiceUrl("header"))
                .getBody().asString();
    }

    /**
     * Returns JSON response for Car Order history Item Service
     *
     * @param fieldName - Field Name
     * @return - JSON response in String format for Item service
     * @throws Throwable
     */
    public String returnJsonResponseForItemLevelOrderHistoryServiceRequestBody(String fieldName) throws Throwable {
        LOGGER.info("returnJsonResponseForItemLevelOrderHistoryServiceRequestBody started");
        String password = driver.scenarioData.getData(sap.pages.CommonActions.SAP_PASSWORD_KEY);
        if(password.isEmpty())
            password = Config.getSapPassword();
        return given().auth().basic(Config.getSapUserName(), password)
                .body(returnItemLevelOrderHistoryServiceRequestBody(fieldName))
                .post(returnCarOrderHistoryServiceUrl(sap.pages.CommonActions.ITEM))
                .getBody().asString();
    }

    /**
     * Asserts Summary order service and saves values from JSON response to scenario data
     *
     * @param val - Email Id/Customer Number
     * @throws Throwable
     */
    public void assertDataForHybrisCustomerCarHeaderOrderHistoryValidation(String val) throws Throwable {
        int arrayIndex = 0;
        boolean isFound = false;
        JSONObject response = new JSONObject(returnJsonResponseForHeaderLevelOrderHistoryServiceRequestBody(returnCustomerIdForHeaderService(val)));
        JSONArray ET_ORDER_HEADER = response.getJSONArray(Constants.ET_ORDER_HEADER);
        JSONObject HYBORDRNUM = null;
        for (int i = 0; i < ET_ORDER_HEADER.length(); i++) {
            JSONObject ordersForCustomer = ET_ORDER_HEADER.getJSONObject(i);
            if (!ordersForCustomer.get(Constants.TRANSSTATUS).toString().equalsIgnoreCase("Converted")) {
                JSONArray TRANSNUMBER = ordersForCustomer.getJSONArray(Constants.TRANSNUMBER);
                for (int j = 0; j < TRANSNUMBER.length(); j++) {
                    if (TRANSNUMBER.getJSONObject(j).get(Constants.FIELDNAME).toString().equalsIgnoreCase(Constants.HYBORDRNUM)) {
                        HYBORDRNUM = TRANSNUMBER.getJSONObject(j);
                        break;
                    }
                }
                if (HYBORDRNUM != null) {
                    Assert.assertTrue(!HYBORDRNUM.get(Constants.FIELDVALUE).toString().equalsIgnoreCase(Constants.HYBORDRNUM));
                    if (HYBORDRNUM.get(Constants.FIELDVALUE).toString().equalsIgnoreCase(driver.scenarioData.getCurrentOrderNumber())) {
                        arrayIndex = i;
                        isFound = true;
                    }
                }
            }
        }
        if (!isFound) {
            Assert.fail("FAIL: web order number was not found for customer: " + returnCustomerIdForHeaderService(val));
        }
        JSONObject orderDetails = ET_ORDER_HEADER.getJSONObject(arrayIndex);
        JSONArray RETAILLINEITEM = orderDetails.getJSONArray(Constants.RETAILLINEITEM);
        for (int i = 0; i < RETAILLINEITEM.length(); i++) {
            JSONObject retailLineItems = RETAILLINEITEM.getJSONObject(i);
            driver.scenarioData.setData(Constants.HEADERITEMID + i, retailLineItems.get(Constants.ITEMID).toString());
            LOGGER.info(Constants.HEADERITEM + " " + i + " : " + driver.scenarioData.getData(Constants.ITEMID + i));
            driver.scenarioData.setData(Constants.HEADERRETAILQUANTITY + i, retailLineItems.get(Constants.RETAILQUANTITY).toString());
            LOGGER.info(Constants.HEADERRETAILQUANTITY + " " + i + " : " + driver.scenarioData.getData(Constants.RETAILQUANTITY + i));
        }
        driver.scenarioData.setData(Constants.HEADERTRANSSTATUS, orderDetails.get(Constants.TRANSSTATUS).toString());
        driver.scenarioData.setData(Constants.HEADERAPPTID, orderDetails.get(Constants.APPTID).toString());
        driver.scenarioData.setData(Constants.HEADERAPPOINTMENTDT, orderDetails.get(Constants.APPTDATE).toString());
        driver.scenarioData.setData(Constants.HEADERAPTSTRTIME, orderDetails.get(Constants.APTSTRTIME).toString());
        driver.scenarioData.setData(Constants.HEADERTOTALAMT, orderDetails.get(Constants.TOTALAMT).toString());
        driver.scenarioData.setData(Constants.HEADERTRANSTYPE, orderDetails.get(Constants.TRANSTYPE).toString());
        driver.scenarioData.setData(Constants.HEADERAPPTSTATUS, orderDetails.get(Constants.APPTSTATUS).toString());
        driver.scenarioData.setData(Constants.HEADERORDER_CHANNEL, orderDetails.get(Constants.ORDER_CHANNEL).toString());
        driver.scenarioData.setData(Constants.CUSTOMER_TYPE_CAR, orderDetails.get(Constants.CUSTOMER_TYPE_CAR).toString());
    }

    /**
     * Returns Customer Id for Header service input
     *
     * @param val - Email/Customer Id
     * @return - Customer Id
     * @throws Throwable
     */
    private String returnCustomerIdForHeaderService(String val) throws Throwable {
        if (val.contains("@")) {
            return getMyAccountCustomerServiceId(val);
        } else if (val.contains(Constants.JENKINS)){
            if(Config.getCustomerType().equalsIgnoreCase(Constants.REGISTERED)) {
                return getMyAccountCustomerServiceId(Config.getCustomerEmail());
            } else {
                return driver.scenarioData.getData(Constants.CUSTOMER_ID_CAR);
            }
        }
        return val;
    }

    /**
     * Saves Car item service response fields to scenario data
     *
     * @param fieldName - Field Name
     * @throws Throwable
     */
    public void setDataForCarItemOrderHistoryValidation(String fieldName) throws Throwable {
        JSONObject response = new JSONObject(returnJsonResponseForItemLevelOrderHistoryServiceRequestBody(fieldName));
        JSONArray ET_ORDER_ITEM = response.getJSONArray(Constants.ET_ORDER_ITEM);
        JSONObject orderDetails = ET_ORDER_ITEM.getJSONObject(0);
        JSONArray RETAILLINEITEM = orderDetails.getJSONArray(Constants.RETAILLINEITEM);
        for (int i = 0; i < RETAILLINEITEM.length(); i++) {
            JSONObject retailLineItems = RETAILLINEITEM.getJSONObject(i);
            driver.scenarioData.setData(Constants.ITEMID + i, retailLineItems.get(Constants.ITEMID).toString());
            LOGGER.info("ITEM " + i + " : " + driver.scenarioData.getData(Constants.ITEMID + i));
            driver.scenarioData.setData(Constants.RETAILQUANTITY + i, retailLineItems.get(Constants.RETAILQUANTITY).toString());
            LOGGER.info(Constants.RETAILQUANTITY + " " + i + " : " + driver.scenarioData.getData(Constants.RETAILQUANTITY + i));
            driver.scenarioData.setData(Constants.VEHICLEID, retailLineItems.get(Constants.VEHICLEID).toString());
            LOGGER.info(Constants.VEHICLEID + " " + i + " : " + driver.scenarioData.getData(Constants.VEHICLEID ));
            driver.scenarioData.setData(Constants.VEHTRIMID, retailLineItems.get(Constants.VEHTRIMID).toString());
            LOGGER.info(Constants.VEHTRIMID + " " + i + " : " + driver.scenarioData.getData(Constants.VEHTRIMID ));
            driver.scenarioData.setData(Constants.ASEMBLYLTR, retailLineItems.get(Constants.ASEMBLYLTR).toString());
            LOGGER.info(Constants.ASEMBLYLTR + " " + i + " : " + driver.scenarioData.getData(Constants.ASEMBLYLTR ));
        }
        driver.scenarioData.setData(Constants.CUSTOMER_ID_CAR, orderDetails.get(Constants.CUSTOMER_ID_CAR).toString());
        LOGGER.info(Constants.CUSTOMER_ID_CAR + " : " + driver.scenarioData.getData(Constants.CUSTOMER_ID_CAR));
        driver.scenarioData.setData(Constants.TRANSSTATUS, orderDetails.get(Constants.TRANSSTATUS).toString());
        driver.scenarioData.setData(Constants.APPTID, orderDetails.get(Constants.APPTID).toString());
        driver.scenarioData.setData(Constants.APPOINTMENTDT, orderDetails.get(Constants.APPOINTMENTDT).toString());
        driver.scenarioData.setData(Constants.APTSTRTIME, orderDetails.get(Constants.APTSTRTIME).toString());
        driver.scenarioData.setData(Constants.TOTALAMT, orderDetails.get(Constants.TOTALAMT).toString());
        driver.scenarioData.setData(Constants.TRANSTYPE, orderDetails.get(Constants.TRANSTYPE).toString());
        driver.scenarioData.setData(Constants.APPTSTATUS, orderDetails.get(Constants.APPTSTATUS).toString());
        driver.scenarioData.setData(Constants.ORDER_CHANNEL, orderDetails.get(Constants.ORDER_CHANNEL).toString());
    }

    /**
     * This method does GET call to order service and return order xml
     *
     * @param order Order Id
     */
    public String getOrderXml(String order) throws Throwable {
        LOGGER.info("getOrderXml started for " + order);
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpGet httpGet = new HttpGet(returnServiceUrl(Constants.ORDER_XML_SERVICE));
        httpGet.addHeader(Constants.ACCEPT, XML_VALUE_ACCEPT);
        httpGet.addHeader(Constants.CONTENT_TYPE, XML_VALUE_ACCEPT);
        httpResponse = httpClient.execute(httpGet);
        Assert.assertTrue("Fail: Status code is not equal to 200 ", String.valueOf(httpResponse.
                getStatusLine().getStatusCode()).
                equalsIgnoreCase(String.valueOf(GET_STATUS_CODE)));
        httpEntity = httpResponse.getEntity();
        orderXmlResponseBody = EntityUtils.toString(httpEntity);
        LOGGER.info("getOrderXml completed for " + order);
        LOGGER.info("Order XML: " + orderXmlResponseBody);
        return orderXmlResponseBody;
    }

    /**
     * This method gets the my account customer ID associated with the customer email ID
     *
     * @param emailId My account customer email ID
     * @return My account customer ID
     */
    public String getMyAccountCustomerServiceId(String emailId) throws Exception {
        LOGGER.info("getCustomerId started");
        String responseBody = given().get(Config.getBaseUrl() + MY_ACCOUNT_CUSTOMER_SERVICE_EMAIL + emailId).body().asString();
        JSONObject response = new JSONObject(responseBody);
        JSONObject data = response.getJSONObject(ConstantsDtc.DATA);
        LOGGER.info("getCustomerId completed");
        return data.get(ConstantsDtc.CUSTOMERID).toString();
    }

    /**
     * Returns service response for VTV
     *
     * @param vin Vehicle Identification Number
     * @return response body
     * @throws Exception
     */
    public String getServiceResponseForVtv(String vin) throws Exception {
        LOGGER.info("getServiceResponseForVtv started");
        String responseBody = given().get(Config.returnVtvCarServiceUrl() + vin).body().asString();
        LOGGER.info("RESPONSE BODY: \n" + responseBody);
        return responseBody;
    }

    /**
     * Returns the vehicle field value for the specified field name
     *
     * @param fieldName field name
     * @param vin       Vehicle Identification Number
     * @return vehicle field value for the specified field name
     * @throws Throwable
     */
    public String returnVehicleFieldValueFromVtvResponse(String fieldName, String vin) throws Throwable {
        LOGGER.info("returnVehicleFieldValueFromVtvResponse started");
        JSONObject response = new JSONObject(getServiceResponseForVtv(vin));
        String vehicleJson = response.get(Constants.VEHICLE.toLowerCase()).toString();
        JSONObject vehicleJsonObject = new JSONObject(vehicleJson);
        LOGGER.info("returnVehicleFieldValueFromVtvResponse completed");
        return vehicleJsonObject.get(fieldName).toString();
    }

    /**
     * Returns the Customer field value for the specified field name
     *
     * @param fieldName field name
     * @param vin       Vehicle Identification Number
     * @return vehicle field value for the specified field name
     * @throws Throwable
     */
    public String returnCustomerFieldValueFromVtvResponse(String fieldName, String vin) throws Throwable {
        LOGGER.info("returnCustomerFieldValueFromVtvResponse started");
        JSONObject response = new JSONObject(getServiceResponseForVtv(vin));
        String customerJson = response.get("customer").toString();
        JSONObject customerJsonObject = new JSONObject(customerJson);
        LOGGER.info("returnCustomerFieldValueFromVtvResponse completed");
        return customerJsonObject.get(fieldName).toString();
    }
}