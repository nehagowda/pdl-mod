package wm.steps;

import cucumber.api.Scenario;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import sap.pages.PurchaseOrder;
import utilities.Driver;
import wm.pages.TransactionSearch;
import sap.pages.NetWeaverBusinessClient;

/**
 * Created by mnabizadeh on 5/18/18.
 */
public class TransactionSearchSteps {

    private Driver driver;
    private TransactionSearch transactionSearch;
    private NetWeaverBusinessClient netWeaverBusinessClient;
    private PurchaseOrder purchaseOrder;
    private Scenario scenario;

    public TransactionSearchSteps(Driver driver) {
        this.driver = driver;
        transactionSearch = new TransactionSearch(driver);
        purchaseOrder = new PurchaseOrder(driver);
        netWeaverBusinessClient = new NetWeaverBusinessClient(driver);
    }

    @Before
    public void before(Scenario scenario) {
        this.scenario = scenario;
    }

    @Given("^I go to the wm homepage$")
    public void i_go_to_the_wm_homepage() throws Throwable {
        transactionSearch.goWMHome();
    }

    @When("^I login with Username and Password$")
    public void i_login_with_username_and_password() throws Throwable {
        transactionSearch.login();
    }

    @When("^I read the transaction number from \"([^\"]*)\" excel$")
    public void i_read_the_transaction_number_from_excel(String file) throws Throwable {
        transactionSearch.hybrisOrderNumberExcel(file);
    }

    @When("^I search for transaction number on interface monitor page$")
    public void i_search_for_transaction_number_on_interface_monitor_page() throws Throwable {
        transactionSearch.wmFunctionIdSearch();
    }

    @When("I search for invoice number extracted from excel on interface monitor page$")
    public void i_search_for_invoice_number_extracted_from_excel_on_interface_monitor_page() throws Throwable {
        transactionSearch.wmFunctionIdSearchForPosDm();
    }

    @When("^I select the date range \"([^\"]*)\"$")
    public void i_select_the_date_range(String date) throws Throwable {
        transactionSearch.selectDateRange(date);
    }

    @When("^I complete the search$")
    public void i_complete_the_search() throws Throwable {
        transactionSearch.searchButton();
    }

    @Then("^I verify transaction number appears with the status of completed$")
    public void i_verify_transaction_number_appears_with_the_status_of_completed() throws Throwable {
        transactionSearch.assertCompletedStatus();
    }

    @Then("^I verify that previous transaction does not continue to sap$")
    public void i_verify_that_previous_transaction_does_not_continue_to_sap() throws Throwable {
        transactionSearch.assertTransactionResultsReturn();
    }

    @When("^I enter \"([^\"]*)\" into the Group Name input$")
    public void enter_selection_into_group_name_input(String selection) throws Throwable {
        transactionSearch.fillGroupNameInput(selection);
    }

    @When("^I enter \"([^\"]*)\" into the Interface Name input$")
    public void enter_selection_into_interface_name_input(String selection) throws Throwable {
        transactionSearch.fillInterfaceNameInput(selection);
    }

    @Then("^I extract the Purchase Order Numbers and save them to the Extended Assortment excel$")
    public void i_extract_purchase_order_number_and_save_to_extended_assortment_excel() throws Throwable {
        transactionSearch.extractAndSavePONumberToExcel();
    }

    @Then("^I extract the Purchase Order Number and save it to scenario data$")
    public void i_extract_purchase_order_number_and_save_to_scenario_data() throws Throwable {
        transactionSearch.extractAndSavePONumberToScenarioData();
        scenario.write("Generated Purchase Order = " + driver.scenarioData.getCurrentPurchaseOrderNumber());
    }

    @When("^I select verbose logging$")
    public void i_select_verbose_logging() throws Throwable {
        transactionSearch.selectVerboseLogging();
    }

    @Then("^I extract the Purchase Order Numbers for each article and save it to scenario data$")
    public void i_extract_the_purchase_order_numbers_for_each_article_and_save_it_to_scenario_data() throws Throwable {
        transactionSearch.extractAndSavePONumbersToScenarioData();
        for (String key : driver.scenarioData.poVendor.keySet()){
            scenario.write("Vendor "+ key +" = " + driver.scenarioData.poVendor.get(key) +"\n");
        }
    }

    @When("^I search for the previously saved Extended Assortment PO numbers with \"([^\"]*)\"$")
    public void i_search_for_the_previously_saved_po_numbers_with(String vendor) throws Throwable {
        String automatedPONumber = driver.scenarioData.getPOVendorData(vendor);
        purchaseOrder.searchOtherPurchaseOrderNumber(automatedPONumber);
    }

    @When("^I hit search multiple times$")
    public void i_hit_search_multiple_times() throws Throwable {
        transactionSearch.waitForStatusToBeVisible();
    }

    @When("^I enter \"([^\"]*)\" in functional area input$")
    public void i_enter_in_functional_area_input(String input) throws Throwable {
        transactionSearch.selectFunctionalAreaInputAndEnter(input);
    }
}