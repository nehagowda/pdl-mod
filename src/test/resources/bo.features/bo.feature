@bo
@prices
Feature: Price Validation in BO Tests

  Background:
    Given I set baseUrl to "Backoffice"
    And   I navigate to the stored Base URL
    And   I login with Backoffice Username and Password

  @priceCheck
  Scenario Outline: BO_Login and Price Check
    When I expand "Price Settings"
    And  I expand "Prices"
    And  I expand "Price Rows"
    And  I select the "Switch search mode" button on the backoffice price rows page
    And  I enter "<Article Description>" information on "Article Description" in the backoffice price rows page "DropDown"
    And  I hit return
    And  I click on "Customer Price List" dropdown
    And  I click on "ug-0001" dropdown
    And  I hit return
    And  I click on "Search" dropdown
    Then I verify "OnlineArticle" matches "<Article Description>" displayed in the backoffice page

    Examples:
      | Article Description                                               |
      | PBX A/T Hardcore [34301] - Discount Tire Product Catalog : Online |