@nexus
Feature: Nexus Regression

  @nexusRegression_Tax_Env_Charged
  @nexusRegression
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_Validate the taxes and fees for the web order created with Legislation Passed State having Tires and Wheels_Regular Fitment_DTD ()
  """ Nexus P1 covered for State: SC, IL, IN, KY, MD, MS, NJ, NC, SC, PA"""
    When I go to the "<State/Excel sheet>" state homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Sensor" fee for item
    And  I select the optional "Certificates" fee for item
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I open the My Vehicles popup
    And  I select "shop wheels" link
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Enviro" is "Charged" for "<ItemCodeA>" product on "Shopping cart" page
    And  I verify "Enviro" is "Not-Charged" for "<ItemCodeB>" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Rebuild Kits" fee for item "<ItemCodeB>"
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Total" from shopping cart as "total"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I extract "Enviro" from checkout order summary as "environmental fee"
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "checkout" page
    And  I verify extracted "Taxes" with "sales tax" from "checkout" page
    And  I verify extracted "Total" with "total" from "checkout" page
    And  I record the time stamp "before" creating the order in YYYYMMDDHHMMSS format
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    When I take page screenshot
    Then I verify extracted "Enviro" with "environmental fee" from "Order Confirmation" page
    And  I verify "Enviro" is "Charged" for "<ItemCodeA>" product on "Shopping cart" page
    And  I verify "Enviro" is "Not-Charged" for "<ItemCodeB>" product on "Shopping cart" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page
    When I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttiredirect"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make | Model  | Trim         | Assembly       | FitmentOption | ItemCodeA | ItemCodeB | ShippingOption | Credit Card | Customer            | State/Excel sheet | Excel File         |
      | 2014 | Ford | Escape | Titanium AWD | 235 /50 R18 SL | tire          | 14294     | 51996     | Ground         | MasterCard  | DEFAULT_CUSTOMER_AL | al                | nexusWebOrders.xls |
      | 2014 | Ford | Escape | Titanium AWD | 235 /50 R18 SL | tire          | 14294     | 51996     | Ground         | MasterCard  | DEFAULT_CUSTOMER_IL | il                | nexusWebOrders.xls |
      | 2014 | Ford | Escape | Titanium AWD | 235 /50 R18 SL | tire          | 14294     | 51996     | Ground         | MasterCard  | DEFAULT_CUSTOMER_IN | in                | nexusWebOrders.xls |
      | 2014 | Ford | Escape | Titanium AWD | 235 /50 R18 SL | tire          | 14294     | 51996     | Ground         | MasterCard  | DEFAULT_CUSTOMER_KY | ky                | nexusWebOrders.xls |
      | 2014 | Ford | Escape | Titanium AWD | 235 /50 R18 SL | tire          | 14294     | 51996     | Ground         | MasterCard  | DEFAULT_CUSTOMER_MD | md                | nexusWebOrders.xls |
      | 2014 | Ford | Escape | Titanium AWD | 235 /50 R18 SL | tire          | 14294     | 51996     | Ground         | MasterCard  | DEFAULT_CUSTOMER_NJ | nj                | nexusWebOrders.xls |
      | 2014 | Ford | Escape | Titanium AWD | 235 /50 R18 SL | tire          | 14294     | 51996     | Ground         | MasterCard  | DEFAULT_CUSTOMER_PA | pa                | nexusWebOrders.xls |
      | 2014 | Ford | Escape | Titanium AWD | 235 /50 R18 SL | tire          | 14294     | 51996     | Ground         | MasterCard  | DEFAULT_CUSTOMER_SC | sc                | nexusWebOrders.xls |
      | 2014 | Ford | Escape | Titanium AWD | 235 /50 R18 SL | tire          | 14294     | 51996     | Ground         | MasterCard  | DEFAULT_CUSTOMER_ms | ms                | nexusWebOrders.xls |
      | 2014 | Ford | Escape | Titanium AWD | 235 /50 R18 SL | tire          | 14294     | 51996     | Ground         | MasterCard  | DEFAULT_CUSTOMER_nc | nc                | nexusWebOrders.xls |

  @nexusRegression_TAXENV_NotCharged
  @nexusRegression
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_Verify Taxes and Fees for Web Order created with NO Legislation Passed State_With Regular Fitment_DTD (ALM#16621,16601,16594)
  """ Nexus P1 covered for State: FL, NY, VA """
    Given I go to the "<State/Excel sheet>" state homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Sensor" fee for item
    And  I select the optional "Certificates" fee for item
    Then I verify "Tax" is "Not-Charged" for "all" product on "Shopping cart" page
    When I open the My Vehicles popup
    And  I select "shop wheels" link
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Not-Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Rebuild Kits" fee for item "<ItemCodeB>"
    Then I verify "Tax" is "Not-Charged" for "all" product on "Shopping cart" page
    When I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Not-Charged" for "all" product on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Not-Charged" for "all" product on "Checkout" page
    When I take page screenshot
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I record the time stamp "before" creating the order in YYYYMMDDHHMMSS format
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    When I take page screenshot
    Then I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page
    When I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttiredirect"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | ItemCodeA | ItemCodeB | ShippingOption | Credit Card | Customer            | State/Excel sheet | Excel File         |
      | 2012 | Honda | Civic | Coupe DX | None     | tire          | 26899     | 75908     | Next Day Air   | MasterCard  | DEFAULT_CUSTOMER_FL | fl                | nexusWebOrders.xls |
      | 2012 | Honda | Civic | Coupe DX | None     | tire          | 26899     | 75908     | Next Day Air   | MasterCard  | DEFAULT_CUSTOMER_NY | ny                | nexusWebOrders.xls |
      | 2012 | Honda | Civic | Coupe DX | None     | tire          | 26899     | 75908     | Next Day Air   | MasterCard  | DEFAULT_CUSTOMER_VA | va                | nexusWebOrders.xls |


  @nexusRegression_TAX_Charged
  @nexusRegression
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_Verify web order created for the legislation passed state not having environmental Fee_DTD
  """ Nexus P1 covered for State: CT, MA, MI, MN, WI """
    Given I go to the "<State/Excel sheet>" state homepage
    When I do a free text search for "<ItemCodeA>" and hit enter
    And  I add item to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCodeB>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Not-Charged" for "all" product on "Shopping cart" page
    When I select the optional "Heat Cycling" fee for item
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "<Checkout>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    And  I take page screenshot
    When I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I record the time stamp "before" creating the order in YYYYMMDDHHMMSS format
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    When I take page screenshot
    Then I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page
    When I store the order number
    And  I take page screenshot
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttiredirect"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I write the total amount into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<State/Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | ItemCodeA | ItemCodeB | Checkout | ShippingOption | Credit Card | Customer            | State/Excel sheet | Excel File         |
      | 40358     | 15279     | default  | Second Day Air | Discover    | DEFAULT_CUSTOMER_CT | ct                | nexusWebOrders.xls |
      | 40358     | 15279     | default  | Second Day Air | Discover    | DEFAULT_CUSTOMER_MA | ma                | nexusWebOrders.xls |
      | 40358     | 15279     | default  | Second Day Air | Discover    | DEFAULT_CUSTOMER_MI | mi                | nexusWebOrders.xls |
      | 40358     | 15279     | default  | Second Day Air | Discover    | DEFAULT_CUSTOMER_MN | mn                | nexusWebOrders.xls |
      | 40358     | 15279     | default  | Second Day Air | Discover    | DEFAULT_CUSTOMER_WI | wi                | nexusWebOrders.xls | 

  @nexusRegressionPosdmPosdt
  Scenario Outline: Transaction validation for transaction types in DTD
    When I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from sheet name "<State/Excel sheet>" and file name "<Excel File>"
    And  I extract all values from excel from sheet name "<State/Excel sheet>" and file name "<Excel File>"
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    Then I verify total transaction amount in posdm/posdt from sheet name "<State/Excel sheet>" and file name "<Excel File>"
    And  I verify "Transaction Type" field has the text of "<Transaction Type>" in posdm
    When I double click on Transaction Number
    Then I verify Article Identifier and Sales Price for Line items in sales data
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame in SAP
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I execute
    Then I verify total transaction amount in posdt from sheet name "<State/Excel sheet>" and file name "<Excel File>"
    And  I verify "Transaction Type" field has the text of "<Transaction Type>" from examples in posdt
    When I double click on Transaction Number in posdt
    Then I verify Article Identifier and Sales Price for Line items in sales data in posdt

    Examples:
      | State/Excel sheet | TCode 1       | Transaction Type | Excel File         |
      | al                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | il                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | in                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | ky                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | md                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | nj                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | pa                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | sc                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | ms                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | nc                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | fl                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | ny                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | va                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | ct                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | ma                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | mi                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | mn                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |
      | wi                | /n/posdw/mon0 | 1202             | nexusInvoices.xlsx |

  @nexusScreenshotCleanUpJob
  Scenario: Deletes all screenshots created in individual folder.
#    When I delete all nexus screenshots created

