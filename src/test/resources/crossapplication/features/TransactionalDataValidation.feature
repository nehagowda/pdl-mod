Feature: Transactional Data Validation

  @dtd
  @nexusTaxUat
  @nexusTaxUat02
  Scenario Outline: Create Web Order for SHIP To states having legislation passed  GA,SC,PA,IN
  (Taxes and Env Fee displayed for the Ship To States)
    Given I go to the homepage
    When I do a free text search for "<ItemCodeA>" and hit enter
    And  I add item to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCodeB>" and hit enter
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    When I extract the product price from "Cart" page
    And  I zoom the web page by "45" and take Screenshot and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris Shopping Cart" extracted from examples
    Then I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "<Checkout>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I take Screenshots of all items in checkout page and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris checkoutpage" extracted from examples
    And  I take page screenshot
    Then I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I take Screenshots of all items in checkout page and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris shipping Address" extracted from examples
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I take Screenshot and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris Order Number" extracted from examples
    And  I store the order number
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Sheet Name/Screenshot Folder>" into row number "2" and cell number "2"
    And  I write the web order number into the excel sheet with sheet name "<Sheet Name/Screenshot Folder>" into row number "2" and cell number "1"
    And  I verify the required fees and add-ons sections are expanded
    Then I verify "Enviro" is "Charged" for "<ItemCodeA>" product on "Order Confirmation" page
    And  I verify "Enviro" is "Charged" for "<ItemCodeB>" product on "Order Confirmation" page
    When I zoom the web page by "42" and take Screenshot and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris Order Confirmation" extracted from examples
    Then I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page

    Examples:
      | ItemCodeA | ItemCodeB | Checkout | Customer            | ShippingOption | Credit Card | Sheet Name/Screenshot Folder |
      | 14279     | 40445     | default  | default_customer_ga | Ground         | Discover    | ga                           |
      | 14279     | 40445     | default  | default_customer_SC | Ground         | Discover    | sc                           |
      | 14279     | 40445     | default  | default_customer_PA | Ground         | Discover    | pa                           |
      | 14279     | 40445     | default  | default_customer_IN | Ground         | Discover    | in                           |

  @dtd
  @nexusTaxUat
  @nexusTaxUat04
  Scenario Outline:Create Web Order for DC Rates SHIP To states having Promotion OHIO
  (Taxes and Env Fee displayed)
    Given I go to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I open the My Vehicles popup
    And  I select "shop wheels" link
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    Then I verify "Enviro" is "Charged" for "<ItemCodeA>" product on "Shopping cart" page
    When I zoom the web page by "50" and take Screenshot and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris Shopping Cart" extracted from examples
    And  I select the checkout option "<Checkout>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I take Screenshots of all items in checkout page and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris shipping page" extracted from examples
    Then I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I take Screenshots of all items in checkout page and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris shipping page" extracted from examples
    And  I take page screenshot
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I take Screenshot and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris Order Number" extracted from examples
    And  I store the order number
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Sheet Name/Screenshot Folder>" into row number "2" and cell number "2"
    And  I write the web order number into the excel sheet with sheet name "<Sheet Name/Screenshot Folder>" into row number "2" and cell number "1"
    And  I verify the required fees and add-ons sections are expanded
    Then I verify "Enviro" is "Charged" for "<ItemCodeA>" product on "Order Confirmation" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page
    When I zoom the web page by "42" and take Screenshot and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris Order Confirmation" extracted from examples

    Examples:
      | Year | Make   | Model  | Trim        | Assembly | FitmentOption | ItemCodeA | ItemCodeB | Checkout | Customer            | ShippingOption | Credit Card | Sheet Name/Screenshot Folder |
      | 2012 | Nissan | Altima | Sedan 2.5 S | none     | tire          | 14068     | 69173     | default  | default_customer_oh | Second Day Air | Discover    | oh                           |

  @dtd
  @nexusTaxUat
  @nexusTaxUat05
  Scenario Outline: Create Web order for SHIP To states with Legislation passed and Env Fee is NOT charged. MA,MI
  (Taxes are Charged and Env. Fee is NOT charged)
    Given I go to the homepage
    When I do a free text search for "<ItemCodeA>" and hit enter
    And  I add item to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCodeB>" and hit enter
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    When I extract the product price from "Cart" page
    Then I verify "Enviro" is "Charged" for "<ItemCodeA>" product on "Shopping cart" page
    And  I verify "Enviro" is "Charged" for "<ItemCodeB>" product on "Shopping cart" page
    When I zoom the web page by "45" and take Screenshot and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris Shopping Cart" extracted from examples
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "Valve Stem" fee for item
    And  I select the optional "Studding" fee for item
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "<Checkout>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I take Screenshots of all items in checkout page and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris checkoutpage" extracted from examples
    And  I take page screenshot
    Then I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I take Screenshots of all items in checkout page and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris shipping page" extracted from examples
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I store the order number
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Sheet Name/Screenshot Folder>" into row number "2" and cell number "2"
    And  I write the web order number into the excel sheet with sheet name "<Sheet Name/Screenshot Folder>" into row number "2" and cell number "1"
    And  I verify the required fees and add-ons sections are expanded
    And  I take Screenshot and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris Order Number" extracted from examples
    Then I verify "Enviro" is "Not-Charged" for "<ItemCodeA>" product on "Order Confirmation" page
    And  I verify "Enviro" is "Not-Charged" for "<ItemCodeB>" product on "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page
    When I zoom the web page by "42" and take Screenshot and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris Order Confirmation" extracted from examples

    Examples:
      | ItemCodeA | ItemCodeB | Checkout | Customer            | ShippingOption | Credit Card | Sheet Name/Screenshot Folder |
      | 14279     | 11120     | default  | default_customer_ma | Second Day Air | Discover    | ma                           |
      | 14279     | 11120     | default  | default_customer_mi | Second Day Air | Discover    | mi                           |

  @dtd
  @nexusTaxUat
  @nexusTaxUat07
  Scenario Outline: Create Web Order for SHIP To states having legislation passed Illinois
    Given I go to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    When I zoom the web page by "50" and take Screenshot and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris Shopping Cart" extracted from examples
    Then I verify the "Estimated Environmental Fee" label displayed on "Shopping cart" page
    And  I verify the "Estimated Taxes" label displayed on "Shopping cart" page
    When I select the checkout option "default"
    Then I verify "Checkout" page is displayed
    When I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I take Screenshots of all items in checkout page and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris checkoutpage" extracted from examples
    And  I take page screenshot
    Then I verify the "Est. Enviro. Fee" label displayed on "Checkout" page
    And  I verify the "Estimated Taxes" label displayed on "Checkout" page
    When I enter shipping info as "<Customer>"
    And  I submit the updated address information and "check for AVS popup"
    Then I verify Tax Estimation Message is displayed for invalid response from "AVS Service" on "Checkout" page
    When I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify the "Est. Enviro. Fee" label displayed on "Checkout" page
    And  I verify the "Estimated Taxes" label displayed on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify the "Est. Enviro. Fee" label displayed on "Checkout" page
    And  I verify the "Estimated Taxes" label displayed on "Checkout" page
    And  I take Screenshots of all items in checkout page and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris shipping page" extracted from examples
    When I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify the "Est. Enviro. Fee" label displayed on "Checkout" page
    And  I verify the "Estimated Taxes" label displayed on "Checkout" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I store the order number
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Sheet Name/Screenshot Folder>" into row number "2" and cell number "2"
    And  I write the web order number into the excel sheet with sheet name "<Sheet Name/Screenshot Folder>" into row number "2" and cell number "1"
    Then I verify Tax Estimation Message is displayed for invalid response from "AVS Service" on "Order Confirmation" page
    When I take Screenshot and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris Order Number" extracted from examples
    And  I take page screenshot
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify the "Estimated Environmental Fee" label displayed on "Order Confirmation" page
    And  I verify the "Estimated Taxes" label displayed on "Order Confirmation" page
    When I zoom the web page by "50" and take Screenshot and save it to "<Sheet Name/Screenshot Folder>" folder with a file name of "Hybris Order Confirmation" extracted from examples

    Examples:
      | Year | Make   | Model  | Trim        | Assembly | FitmentOption | ItemCode | Customer            | ShippingOption | Credit Card      | Sheet Name/Screenshot Folder |
      | 2012 | Nissan | Altima | Sedan 2.5 S | none     | tire          | 14692    | default_customer_IL | Ground         | MasterCard Bopis | il                           |

  @orderLookUpService
  Scenario Outline: Order Look Up Demo Test
    Given I go to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    When I select the checkout option "default"
    Then I verify "Checkout" page is displayed
    When I enter shipping info as "<Customer>"
    And  I submit the updated address information and "check for AVS popup"
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I store the order number
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttiredirect"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body

    Examples:
      | Year | Make   | Model  | Trim        | Assembly | FitmentOption | ItemCode | Customer            | ShippingOption | Credit Card      |
      | 2012 | Nissan | Altima | Sedan 2.5 S | none     | tire          | 30427    | default_customer_IL | Ground         | MasterCard Bopis |

  @nexusuatposdmposdtsap
  Scenario Outline: EXCEL -> SAP ECC -> POSDM -> POSDT
    When I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from sheet name "<Sheet Name>" from environment variables
    And  I extract all values from excel from sheet name "<Sheet Name>" from environment varaibles
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    And  I wait for execution to be completed
    And  I click on "document flow button" in posdm page
    And  I wait for "5" seconds
    And  I click on "Document flow outbound" in posdm page
    And  I scroll to following "external document" element in PosDm page
    And  I save the "Outbox IDOC" element text value in PosDm page
    And  I click on "Overview" in posdm page
    Then I verify if tasks with errors exist in posdm/posdt
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Financial transaction status"
    When I click on "Transaction Status" in posdm page
    And  I switch back to the main window
    And  I switch to the first iFrame in SAP
    Then I verify all the transactions are successful
    When I click on "Close Button" in posdm page
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    Then I verify total transaction amount in posdm/posdt
    And  I verify "Transaction Type" field has the text of "Transaction Type" in posdm/posdt
    When I double click on Transaction Number
    Then I verify Article Identifier and Sales Price for Line items in sales data
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Invoice Sales"
    And  I click "Vertical Scroll" till "Zip" is visible in posdm
    And  I click "Vertical Scroll" "5" times in posdm
    Then I verify "Last Name" field has the text of "Last Name" in posdm/posdt
    And  I verify "Address Line1" field has the text of "Address Line" in posdm/posdt
    And  I verify "City" field has the text of "City" in posdm/posdt
    And  I verify "State" field has the text of "State" in posdm/posdt
    And  I verify "Zip" field has the text of "ZIP" in posdm/posdt
    And  I verify "Country" field has the text of "<Country>" in posdm
    When I switch back to the main window
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdt page
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I click on "Execute" in posdt page
    And  I wait for execution to be completed
    Then I verify if tasks with errors exist in posdm/posdt
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Financial transaction status"
    And  I click on "Transaction Status" in posdt page
    And  I switch back to the main window
    And  I switch to the first iFrame in SAP
    Then I verify all the transactions are successful in posdt
    When I click on "Close Button" in posdt page
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    Then I verify total transaction amount in posdm/posdt
    And  I verify "Transaction Type" field has the text of "Transaction Type" in posdm/posdt
    When I double click on Transaction Number in posdt
    Then I verify Article Identifier and Sales Price for Line items in sales data in posdt
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Invoice Sales"
    And  I click "Vertical Scroll" till "Last Name" is visible in posdm
    And  I click "Vertical Scroll" "10" times in posdm
    Then I verify "Last Name" field has the text of "Last Name" in posdm/posdt
    And  I verify "Address Line1" field has the text of "Address Line" in posdm/posdt
    And  I verify "City" field has the text of "City" in posdm/posdt
    And  I verify "State" field has the text of "State" in posdm/posdt
    And  I verify "Zip" field has the text of "ZIP" in posdm/posdt
    And  I verify "Country" field has the text of "<Country>" in posdm
    When I switch back to the main window
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I switch to the first iFrame
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table>" in PosDm page
    And  I hit return
    And  I enter saved "Outbox IDOC" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "Not Equal to"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDm page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table 2>" in PosDm page
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I click on "Execute button SAP" in posdm page
    And  I save the "Idoc value 1" element text value in PosDm page
    And  I enter t-code "<TCode>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I double click on "Article" in Idoc page
    Then I validate "Movement Type" entered matches original "<Movement Type>" in Idoc page with input values from environment variables
    When I click on "Back Button" in order to cash page
    And  I double click on "Billing" in Idoc page
    Then I verify the Net Values and articles in billing document
    When I click on "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on element with value of "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on "Views" in IDOC page
    And  I click on "List Output"
    And  I wait for "5" seconds
    Then I validate "STR Account" entered matches original "<STR Account>" in wper
    And  I validate "Sales Tax Account" entered matches original "<Sales Tax Account>" in wper
    And  I validate "Environment Fee Account" entered matches original "<Environment Fee>" in wper
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "SAP Accounting document"
    Examples:
      | TCode   | STR Account | Sales Tax Account | Environment Fee         | Movement Type | Sheet Name | Country | TCode 1       | Screenshot Folder | Table TCode | Outbound/Inbound Idoc Table | Outbound/Inbound Idoc Table 2 |
      | /n WPER | 119060      | Sales Tax Account | Environment Fee Account | Movement type | Sheet name | US      | /n/posdw/mon0 | Screenshot folder | /n SE16     | EDIDS                       | EDIDS                         |

  @nexusuatinvoiceposdmposdt
  Scenario Outline: EXCEL -> POSDM -> POSDT
    When I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from sheet name "<Sheet Name>" from environment variables
    And  I extract all values from excel from sheet name "<Sheet Name>" from environment varaibles
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    And  I wait for execution to be completed
    Then I verify if tasks with errors exist in posdm/posdt
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Financial transaction status"
    When I click on "Transaction Status" in posdm page
    And  I switch back to the main window
    And  I switch to the first iFrame in SAP
    Then I verify all the transactions are successful
    When I click on "Close Button" in posdm page
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    Then I verify total transaction amount in posdm/posdt
    And  I verify "Transaction Type" field has the text of "Transaction Type" in posdm/posdt
    When I double click on Transaction Number
    Then I verify Article Identifier and Sales Price for Line items in sales data
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Invoice Sales"
    And  I click "Vertical Scroll" till "Zip" is visible in posdm
    And  I click "Vertical Scroll" "3" times in posdm
    And  I verify "Last Name" field has the text of "<Last Name>" in posdm
    And  I verify "Address Line1" field has the text of "Address Line" in posdm/posdt
    And  I verify "City" field has the text of "City" in posdm/posdt
    And  I verify "State" field has the text of "State" in posdm/posdt
    And  I verify "Zip" field has the text of "ZIP" in posdm/posdt
    And  I verify "Country" field has the text of "<Country>" in posdm
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdt page
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I click on "Execute" in posdt page
    And  I wait for execution to be completed
    Then I verify if tasks with errors exist in posdm/posdt
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Financial transaction status"
    And  I click on "Transaction Status" in posdt page
    And  I switch back to the main window
    And  I switch to the first iFrame in SAP
    Then I verify all the transactions are successful in posdt
    When I click on "Close Button" in posdt page
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    Then I verify total transaction amount in posdm/posdt
    And  I verify "Transaction Type" field has the text of "Transaction Type" in posdm/posdt
    When I double click on Transaction Number in posdt
    Then I verify Article Identifier and Sales Price for Line items in sales data in posdt
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Invoice Sales"
    And  I click "Vertical Scroll" till "Zip" is visible in posdm
    And  I click "Vertical Scroll" "20" times in posdm
    Then I verify "Last Name" field has the text of "<Last Name>" in posdm
    And  I verify "Address Line1" field has the text of "Address Line" in posdm/posdt
    And  I verify "City" field has the text of "City" in posdm/posdt
    And  I verify "State" field has the text of "State" in posdm/posdt
    And  I verify "Zip" field has the text of "ZIP" in posdm/posdt
    And  I verify "Country" field has the text of "<Country>" in posdm

    Examples:
      | Sheet Name | Last Name | Country | TCode 1       | Screenshot Folder |
      | Sheet name | TEST      | US      | /n/posdw/mon0 | Screenshot folder |

  @dtdLayawayValitaionAdditionalPaymentstg
  Scenario Outline: INT LAYAWAY DTD ESB POSDM POSDT SAPECC STG
    When I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from sheet name "<Sheet Name>" from environment variables
    And  I extract all values from excel from sheet name "<Sheet Name>" from environment varaibles
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number for layaway generated in legacy system in "Transaction Number" field in PosdM
    And  I click on "Execute" in posdm page
    And  I expand date column in posdm
    And  I click on "1981 Expand Node" in posdm page
    And  I double click on "Financial Transactions"
    And  I click on "document flow button" in posdm page
    And  I click on "Document flow outbound" in posdm page
    And  I wait for "5" seconds
    And  I scroll to following "external document" element in PosDm page
    And  I save the "Outbox IDOC" element text value in PosDm page
    And  I click on "Overview" in posdm page
    Then I verify if tasks with errors exist in posdm/posdt
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Financial transaction status"
    And  I click on "Transaction Status" in posdm page
    And  I switch back to the main window
    And  I switch to the first iFrame in SAP
    Then I verify all the transactions are successful
    When I click on "Close Button" in posdm page
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    When I double click on Transaction Number
    Then I verify "Layaway transaction amount" field has the text of "Layaway Amount" in posdm/posdt
    When I click on "Back" in posdm page
    And  I double click on "Goods movements"
    And  I click on "Scroll prev" in posdm page
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Goods movement status"
    Then I verify if tasks with errors exist in posdm/posdt
    When I click on "Transaction Status" in posdm page
    And  I switch back to the main window
    And  I switch to the first iFrame in SAP
    Then I verify all the transactions are successful
    When I click on "Close Button" in posdm page
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    And  I double click on Transaction Number
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Article and Items list"
    Then I verify Article Identifier for Line items in sales data in posdm
    When I click "Vertical Scroll" till "Zip" is visible in posdm
    And  I click "Vertical Scroll" "3" times in posdm
    Then I verify "Last Name" field has the text of "<Last Name>" in posdm
    And  I verify "Address Line1" field has the text of "Address Line" in posdm/posdt
    And  I verify "City" field has the text of "City" in posdm/posdt
    And  I verify "State" field has the text of "State" in posdm/posdt
    And  I verify "Zip" field has the text of "ZIP" in posdm/posdt
    And  I verify "Country" field has the text of "<Country>" in posdm
    When I switch back to the main window
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number for layaway generated in legacy system in "Transaction Number" field in PosdM
    And  I click on "Execute" in posdm page
    And  I expand date column in posdm
    And  I click on "1981 Expand Node" in posdm page
    And  I double click on "Financial Transactions"
    Then I verify if tasks with errors exist in posdm/posdt
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Financial transaction status"
    And  I click on "Transaction Status" in posdm page
    And  I switch back to the main window
    And  I switch to the first iFrame in SAP
    Then I verify all the transactions are successful
    When I click on "Close Button" in posdm page
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    When I double click on Transaction Number
    Then I verify "Layaway transaction amount" field has the text of "Layaway Amount" in posdm/posdt
    When I click on "Back" in posdm page
    And  I double click on "Goods movements"
    And  I click on "Scroll prev" in posdm page
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Goods movement status"
    Then I verify if tasks with errors exist in posdm/posdt
    When I click on "Transaction Status" in posdm page
    And  I switch back to the main window
    And  I switch to the first iFrame in SAP
    Then I verify all the transactions are successful
    When I click on "Close Button" in posdm page
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    And  I double click on Transaction Number
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Article and Items list"
    Then I verify Article Identifier for Line items in sales data in posdt
    And  I click "Vertical Scroll" till "Address Line1" is visible in posdm
    And  I click "Vertical Scroll" "5" times in posdm
    Then I verify "Last Name" field has the text of "<Last Name>" in posdm
    And  I verify "Address Line1" field has the text of "Address Line" in posdm/posdt
    And  I verify "City" field has the text of "City" in posdm/posdt
    And  I verify "State" field has the text of "State" in posdm/posdt
    And  I verify "Zip" field has the text of "ZIP" in posdm/posdt
    And  I verify "Country" field has the text of "<Country>" in posdm
    When I switch back to the main window
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I switch to the first iFrame
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table>" in PosDm page
    And  I hit return
    And  I enter saved "Outbox IDOC" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "Not Equal to"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDm page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table 2>" in PosDm page
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I click on "Execute button SAP" in posdm page
    And  I save the "Idoc value 1" element text value in PosDm page
    And  I enter t-code "<SAPECC TCode 1>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "WPUFIB"
    And  I save the "G/L Account from WPUFIB" element text value in PosDm page
    And  I enter t-code "<TCode3>" in the command field
    And  I enter the G/L Account number into "Document number input FB03" in fb03
    And  I hit return
    Then I validate "STR FB03" entered matches original "119060" in Idoc page
    And  I validate "Layaway fb03" entered matches original "214620" in Idoc page
    When I wait for "5" seconds
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "FB03 Accounting Document"

    Examples:
      | Sheet Name | Last Name | Country | TCode 1       | TCode3  | Screenshot Folder | SAPECC TCode 1 | Table TCode | Outbound/Inbound Idoc Table | Outbound/Inbound Idoc Table 2 |
      | Sheet name | TEST      | US      | /n/posdw/mon0 | /n fb03 | Screenshot folder | /n WPER        | /n SE16     | EDIDS                       | EDIDS                         |

  @dtdLayawayValitaionAdditionalPaymentstgPart2
  Scenario Outline: INT LAYAWAY DTD ESB POSDM POSDT SAPECC STG part 2
    When I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from sheet name "<Sheet Name>" from environment variables
    And  I extract all values from excel from sheet name "<Sheet Name>" from environment varaibles
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number for layaway generated in legacy system in "Transaction Number" field in PosdM
    And  I click on "Execute" in posdm page
    And  I click on "document flow button" in posdm page
    And  I click on "Document flow outbound" in posdm page
    And  I scroll to following "external document" element in PosDm page
    And  I save the "Outbox IDOC" element text value in PosDm page
    And  I click on "Overview" in posdm page
    Then I verify if tasks with errors exist in posdm/posdt
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Financial transaction status"
    And  I click on "Transaction Status" in posdm page
    And  I switch back to the main window
    And  I switch to the first iFrame in SAP
    Then I verify all the transactions are successful
    When I click on "Close Button" in posdm page
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    And  I double click on Transaction Number
    Then I verify "Layaway transaction amount" field has the text of "Layaway Amount" in posdm/posdt
    When I click "Vertical Scroll" till "Zip" is visible in posdm
    And  I click "Vertical Scroll" "3" times in posdm
    Then I verify "Last Name" field has the text of "<Last Name>" in posdm
    And  I verify "Address Line1" field has the text of "Address Line" in posdm/posdt
    And  I verify "City" field has the text of "City" in posdm/posdt
    And  I verify "State" field has the text of "State" in posdm/posdt
    And  I verify "Zip" field has the text of "ZIP" in posdm/posdt
    And  I verify "Country" field has the text of "<Country>" in posdm
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number for layaway generated in legacy system in "Transaction Number" field in PosdM
    And  I click on "Execute" in posdm page
    Then I verify if tasks with errors exist in posdm/posdt
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Financial transaction status"
    And  I click on "Transaction Status" in posdm page
    And  I switch back to the main window
    And  I switch to the first iFrame in SAP
    Then I verify all the transactions are successful
    When I click on "Close Button" in posdm page
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    And  I double click on Transaction Number
    Then I verify "Layaway transaction amount" field has the text of "Layaway Amount" in posdm/posdt
    When I click "Vertical Scroll" till "Last Name" is visible in posdm
    And  I click "Vertical Scroll" "5" times in posdm
    Then I verify "Last Name" field has the text of "<Last Name>" in posdm
    And  I verify "Address Line1" field has the text of "Address Line" in posdm/posdt
    And  I verify "City" field has the text of "City" in posdm/posdt
    And  I verify "State" field has the text of "State" in posdm/posdt
    And  I verify "Zip" field has the text of "ZIP" in posdm/posdt
    And  I verify "Country" field has the text of "<Country>" in posdm
    When I switch back to the main window
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I switch to the first iFrame
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table>" in PosDm page
    And  I hit return
    And  I enter saved "Outbox IDOC" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "Not Equal to"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDm page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table 2>" in PosDm page
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I click on "Execute button SAP" in posdm page
    And  I wait for "30" seconds
    And  I save the "Idoc value 1" element text value in PosDm page
    And  I enter t-code "<SAPECC TCode 1>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "WPUFIB"
    And  I save the "G/L Account from WPUFIB" element text value in PosDm page
    And  I enter t-code "<TCode3>" in the command field
    And  I enter the G/L Account number into "Document number input FB03" in fb03
    And  I hit return
    Then I validate "STR FB03" entered matches original "119060" in Idoc page
    And  I validate "Layaway fb03" entered matches original "214620" in Idoc page
    When I wait for "5" seconds
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "FB03 Accounting Document"

    Examples:
      | Sheet Name | Last Name | Country | TCode 1       | TCode3  | Screenshot Folder | SAPECC TCode 1 | Table TCode | Outbound/Inbound Idoc Table | Outbound/Inbound Idoc Table 2 |
      | Sheet name | TEST      | US      | /n/posdw/mon0 | /n fb03 | Screenshot folder | /n WPER        | /n SE16     | EDIDS                       | EDIDS                         |

  @webOrdersEsbPosdmPosdtSap
  Scenario Outline: DTD Web Orders validation in ESB, POSDM, POSDT and SAP ECC
    When I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from sheet name "<Sheet Name>" from environment variables
    And  I extract all values from excel from sheet name "<Sheet Name>" from environment varaibles
    And  I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for invoice number extracted from excel on interface monitor page
    And  I select verbose logging
    And  I select the date range "This Day"
    And  I enter "POSDM Processing" in functional area input
    And  I complete the search
    Then I verify transaction number appears with the status of completed
    When I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    And  I wait for execution to be completed
    And  I click on "document flow button" in posdm page
    And  I wait for "5" seconds
    And  I click on "Document flow outbound" in posdm page
    And  I click "Vertical Scroll" till "external document" is visible in posdm
    And  I click "Vertical Scroll" "5" times in posdm
    And  I save the "Outbox IDOC" element text value in PosDm page
    And  I click on "Overview" in posdm page
    Then I verify if tasks with errors exist in posdm/posdt
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Financial transaction status"
    Then I verify total transaction amount in posdm/posdt
    And  I verify "Transaction Type" field has the text of "Transaction Type" in posdm/posdt
    When I double click on Transaction Number
    Then I verify Article Identifier and Sales Price for Line items in sales data
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Invoice Sales"
    And  I switch back to the main window
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I switch to the first iFrame
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table>" in PosDm page
    And  I hit return
    And  I enter saved "Outbox IDOC" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "Not Equal to"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDm page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table 2>" in PosDm page
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I click on "Execute button SAP" in posdm page
    And  I save the "Idoc value 1" element text value in PosDm page
    And  I enter t-code "<TCode>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I double click on "Article" in Idoc page
    Then I validate "Movement Type" entered matches original "<Movement Type>" in Idoc page with input values from environment variables
    When I click on "Back Button" in order to cash page
    And  I double click on "Billing" in Idoc page
    Then I verify the Net Values and articles in billing document
    When I click on "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on element with value of "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on "Views" in IDOC page
    And  I click on "List Output"
    And  I wait for "5" seconds
    Then I validate "STR Account" entered matches original "<STR Account>" in wper
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "SAP Accounting document"
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame in SAP
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I execute
    Then I verify if tasks with errors exist in posdm/posdt
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Financial transaction status"
    Then I verify total transaction amount in posdm/posdt
    And  I verify "Transaction Type" field has the text of "Transaction Type" in posdm/posdt
    When I double click on Transaction Number in posdt
    Then I verify Article Identifier and Sales Price for Line items in sales data in posdt
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Invoice Sales"

    Examples:
      | TCode   | STR Account | Movement Type | Sheet Name | Country | TCode 1       | Screenshot Folder | Table TCode | Outbound/Inbound Idoc Table | Outbound/Inbound Idoc Table 2 |
      | /n WPER | 119060      | Movement type | Sheet name | US      | /n/posdw/mon0 | Screenshot folder | /n SE16     | EDIDS                       | EDIDS                         |

  @invoiceValidationEsbPosdmPosdtEcc
  Scenario Outline: EXCEL -> ESB -> POSDM -> POSDT -> SAP ECC
    When I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from sheet name "<Sheet Name>" from environment variables
    And  I extract all values from excel from sheet name "<Sheet Name>" from environment varaibles
    And  I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for invoice number extracted from excel on interface monitor page
    And  I select verbose logging
    And  I select the date range "This Day"
    And  I enter "POSDM Processing" in functional area input
    And  I complete the search
    Then I verify transaction number appears with the status of completed
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "ESB status for POSDM"
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    Then I verify total transaction amount in posdm/posdt
    And  I verify "Transaction Type" field has the text of "Transaction Type" in posdm/posdt
    When I double click on Transaction Number
    Then I verify Article Identifier and Sales Price for Line items in sales data
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Invoice Sales"
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    And  I click on "Sales movement row" in posdm page
    And  I click on "Process task online" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on "Execute processing" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Close" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on "Close" in posdm page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter t-code "<POSDM TCode 1>" in the command field
    And  I click on "Get Variant" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I double click "QA Outbound PR" in posdm
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I confirm the job is complete
    And  I switch back to the main window in SAP
    And  I delete all cookies
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<SAPECC TCode 2>" in the command field
    And  I click on "Get Variant" in IDOC page
    And  I switch to the first iFrame in SAP
    And  I double click on "QA INBOUND" in Idoc page
    And  I switch back to the main window in SAP
    And  I click on the "iDoc Execute" icon from the SAP navigation bar
    And  I save the "WPUUMS Row" field's Idoc Number and assign it to "WPUUMS" key
    And  I save the "WPUTAB Row" field's Idoc Number and assign it to "WPUTAB" key
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "SAP Inbound idocs"
    And  I enter t-code "<SAPECC TCode 1>" in the command field
    And  I enter Today's date into "Creation Time" in FB60
    And  I enter saved "WPUUMS" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I double click on "AGGR Article WPUUMS" in Idoc page
    Then I validate "Movement Type" entered matches original "<Movement Type>" in Idoc page with input values from environment variables
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "SAP WPUUMS Article Document"
    And  I click on "Back Button" in order to cash page
    And  I double click on "AGGR Billing WPUUMS" in Idoc page
    Then I verify grand total is equal to total from excel
    And  I verify the Net Values and Articles in billing document for aggregated
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "SAP WPUUMS Billing Document"
    And  I click on "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on element with value of "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on "Views" in IDOC page
    And  I click on "List Output"
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "SAP WPUUMS Accounting Document"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<SAPECC TCode 1>" in the command field
    And  I enter Today's date into "Creation Time" in FB60
    And  I enter saved "WPUTAB" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I double click on "Billing document WPUTAB" in Idoc page
    And  I click on "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on element with value of "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on "Views" in IDOC page
    And  I click on "List Output"
    Then I validate "STR Account" entered matches original "119060" in wper
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "SAP WPUTAB Accounting Document"
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame in SAP
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I execute
    Then I verify total transaction amount in posdt
    And  I verify "Transaction Type" field has the text of "Transaction Type" in posdt
    When I double click on Transaction Number in posdt
    Then I verify Article Identifier and Sales Price for Line items in sales data in posdt
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Invoice Sales"

    Examples:
      | Sheet Name | First Name | Last Name | Country | TCode 1       | Home phone | Work Phone | phone      | Screenshot Folder | Movement Type | POSDM TCode 1 | SAPECC TCode 1 | SAPECC TCode 2 |
      | Sheet name | QTP        | TEST      | US      | /n/posdw/mon0 | 4806065544 | 4806065555 | 4805556666 | Screenshot folder | Movement type | /n/posdw/odis | /n WPER        | rwpos_para     |

  @dtdInitialLayawayValidation
  Scenario Outline: INT LAYAWAY DTD ESB POSDM POSDT SAPECC QA
    When I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from sheet name "<Sheet Name>" from environment variables
    And  I extract all values from excel from sheet name "<Sheet Name>" from environment varaibles
    And  I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for invoice number extracted from excel on interface monitor page
    And  I select verbose logging
    And  I select the date range "This Day"
    And  I enter "POSDM Processing" in functional area input
    And  I complete the search
    Then I verify transaction number appears with the status of completed
    When I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number for layaway generated in legacy system in "Transaction Number" field in PosdM
    And  I click on "Execute" in posdm page
    And  I expand date column in posdm
    And  I click on "1001 Expand Node" in posdm page
    And  I double click on "Financial Transactions"
    And  I click on "document flow button" in posdm page
    And  I click on "Document flow outbound" in posdm page
    And  I wait for "5" seconds
    And  I click "Vertical Scroll" till "external document" is visible in posdm
    And  I click "Vertical Scroll" "5" times in posdm
    And  I save the "Outbox IDOC" element text value in PosDm page
    And  I click on "Overview" in posdm page
    Then I verify if tasks with errors exist in posdm/posdt
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Financial transaction status"
    And  I double click on Transaction Number
    And  I click on "Back" in posdm page
    And  I double click on "Goods movements"
    And  I click on "Scroll prev" in posdm page
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Goods movement status"
    Then I verify if tasks with errors exist in posdm/posdt
    When I double click on Transaction Number
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Article and Items list"
    Then I verify Article Identifier for Line items in sales data in posdm
    When I switch back to the main window
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I switch to the first iFrame
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table>" in PosDm page
    And  I hit return
    And  I enter saved "Outbox IDOC" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "Not Equal to"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDm page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table 2>" in PosDm page
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I click on "Execute button SAP" in posdm page
    And  I save the "Idoc value 1" element text value in PosDm page
    And  I enter t-code "<SAPECC TCode 1>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "WPUFIB"
    And  I save the "G/L Account from WPUFIB" element text value in PosDm page
    And  I enter t-code "<TCode3>" in the command field
    And  I enter the G/L Account number into "Document number input FB03" in fb03
    And  I hit return
    Then I validate "STR FB03" entered matches original "119060" in Idoc page
    And  I validate "Layaway fb03" entered matches original "214620" in Idoc page
    When I wait for "5" seconds
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "FB03 Accounting Document"
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header"
    And  I enter the Transaction Number for layaway generated in legacy system in "Transaction Number" field in Posdt
    And  I execute
    And  I expand date column in posdm
    And  I click on "1001 Expand Node" in posdm page
    And  I double click on "Financial Transactions"
    Then I verify if tasks with errors exist in posdm/posdt
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Financial transaction status"
    And  I double click on Transaction Number in posdt
    And  I wait for "3" seconds
    And  I go back
    And  I double click on "Goods movements"
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Goods movement status"
    Then I verify if tasks with errors exist in posdm/posdt
    When I double click on Transaction Number in posdt
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Article and Items list"
    Then I verify Article Identifier for Line items in sales data in posdt

    Examples:
      | Sheet Name | Last Name | Country | TCode 1       | TCode3  | Screenshot Folder | SAPECC TCode 1 | Table TCode | Outbound/Inbound Idoc Table | Outbound/Inbound Idoc Table 2 |
      | Sheet name | TEST      | US      | /n/posdw/mon0 | /n fb03 | Screenshot folder | /n WPER        | /n SE16     | EDIDS                       | EDIDS                         |

  @dtdAdditionalLayawayValidation
  Scenario Outline: INT LAYAWAY DTD ESB POSDM POSDT SAPECC part 2 QA
    When I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from sheet name "<Sheet Name>" from environment variables
    And  I extract all values from excel from sheet name "<Sheet Name>" from environment varaibles
    And  I set baseUrl to "Web Method"
    And  I navigate to the stored Base URL
    And  I login with Username and Password
    And  I search for invoice number extracted from excel on interface monitor page
    And  I select verbose logging
    And  I select the date range "This Day"
    And  I enter "POSDM Processing" in functional area input
    And  I complete the search
    Then I verify transaction number appears with the status of completed
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "ESB status for POSDM"
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number for layaway generated in legacy system in "Transaction Number" field in PosdM
    And  I click on "Execute" in posdm page
    And  I click on "document flow button" in posdm page
    And  I click on "Document flow outbound" in posdm page
    And  I wait for "5" seconds
    And  I click "Vertical Scroll" till "external document" is visible in posdm
    And  I click "Vertical Scroll" "5" times in posdm
    And  I save the "Outbox IDOC" element text value in PosDm page
    And  I click on "Overview" in posdm page
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDM Financial transaction status"
    Then I verify if tasks with errors exist in posdm/posdt
    When I double click on Transaction Number
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I switch to the first iFrame
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table>" in PosDm page
    And  I hit return
    And  I enter saved "Outbox IDOC" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "Not Equal to"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDm page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table 2>" in PosDm page
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I click on "Execute button SAP" in posdm page
    And  I save the "Idoc value 1" element text value in PosDm page
    And  I enter t-code "<SAPECC TCode 1>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "WPUFIB"
    And  I save the "G/L Account from WPUFIB" element text value in PosDm page
    And  I enter t-code "<TCode3>" in the command field
    And  I enter the G/L Account number into "Document number input FB03" in fb03
    And  I hit return
    Then I validate "STR FB03" entered matches original "119060" in Idoc page
    And  I validate "Layaway fb03" entered matches original "214620" in Idoc page
    When I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "FB03 Accounting Document"
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header"
    And  I enter the Transaction Number for layaway generated in legacy system in "Transaction Number" field in Posdt
    And  I execute
    And  I double click on Transaction Number in posdt
    And  I wait for "3" seconds
    And  I take Screenshot and save it to "<Screenshot Folder>" folder with a file name of "POSDT Goods movement status"
    Then I verify if tasks with errors exist in posdm/posdt

    Examples:
      | Sheet Name | First Name | Last Name | Country | TCode 1       | Home phone | Work Phone | TCode3  | phone      | Screenshot Folder | SAPECC TCode 1 | Table TCode | Outbound/Inbound Idoc Table | Outbound/Inbound Idoc Table 2 |
      | Sheet name | QTP        | TEST      | US      | /n/posdw/mon0 | 4806065544 | 4806065555 | /n fb03 | 4805556666 | Screenshot folder | /n WPER        | /n SE16     | EDIDS                       | EDIDS                         |

  @dtd
  @web
  @sendWebOrderNumberToExcel
  @dtdOrderForSales
  Scenario Outline: Send Web Order for specified state and product code to SAP Rk2 Excel
    When I go to the "<State>" state homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    When I select the checkout option "default"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "Ground" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I save the order number to the "top" of the "<Column Name>" column of the "<File Tab>" tab of the "<Excel File>" excel file

    Examples:
      | Year | Make  | Model | Trim         | Assembly       | FitmentOption | ProductName                 | ItemCode | Credit Card            | Customer            | State | Excel File                           | Column Name           | File Tab  |
      | 2012 | Honda | Civic | Coupe DX     | None           | tire          | CONTROL CONTACT TOURING A/S | 19661    | CarCareOne Bopis       | DEFAULT_CUSTOMER_TX | TX    | WEBORDERS_SAP_RK2_DTD_STOREPOS_EXCEL | WebOrderID_CC1        | WebOrders |
      | 2016 | Ram   | 2500  | MEGA CAB 4WD | 275 /70 R18 E1 | tire          | Terra Grappler AT           | 40389    | Visa Bopis             | DEFAULT_CUSTOMER_NV | NV    | WEBORDERS_SAP_RK2_DTD_STOREPOS_EXCEL | WebOrderID_VISA       | WebOrders |
      | 2012 | Honda | Civic | Coupe DX     | None           | tire          | Silver Edition III          | 29935    | Discover Bopis         | DEFAULT_CUSTOMER_CO | CO    | WEBORDERS_SAP_RK2_DTD_STOREPOS_EXCEL | WebOrderID_Discover   | WebOrders |
      | 2012 | Honda | Civic | Coupe DX     | None           | tire          | CONTROL CONTACT TOURING A/S | 19661    | American Express Bopis | DEFAULT_CUSTOMER_TX | TX    | WEBORDERS_SAP_RK2_DTD_STOREPOS_EXCEL | WebOrderID_AMEX       | WebOrders |