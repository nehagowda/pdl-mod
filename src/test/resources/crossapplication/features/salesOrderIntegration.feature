@sap
@salesOrderIntegration
Feature: salesOrderIntegration

  @dt
  @at
  @web
  @myAccountOrders
  @bopisIntegrationRegression
  Scenario Outline: BOPIS_Regular Vehicle_With Appointment_CreditCard
    When I change to the default store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I enter customer information for "<Customer>"
    And  I click on the "Place Order" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I store the order number
    And  I take page screenshot
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"

    Examples:
      | Year | Make  | Model | Trim         | Assembly       | FitmentOption | ItemCode | Checkout         | Selection | Customer                          | Credit Card      | Customer            | Excel sheet | Excel File    |
      | 2016 | Ram   | 2500  | MEGA CAB 4WD | 275 /70 R18 E1 | tire          | 15949    | with appointment | BOPIS     | DEFAULT_CUSTOMER_BOPIS_VISA       | Visa Bopis       | DEFAULT_CUSTOMER_AZ | bopisvisa   | WebOrders.xls |
      | 2012 | Honda | Civic | Coupe DX     | None           | tire          | 19661    | with appointment | BOPIS     | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis | DEFAULT_CUSTOMER_AZ | bopismaster | WebOrders.xls |

  @dt
  @at
  @web
  @myAccountOrders
  @bopisIntegrationRegression
  Scenario Outline: BOPIS_Staggered Vehicle_Without Appointment_CreditCard
    When I change to the default store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select the checkout option "<Checkout>"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click next step for customer information
    And  I enter customer information for "<Customer>"
    And  I click on the "Place Order" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I store the order number
    And  I take page screenshot
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | FitmentOption | Checkout | Customer                    | Credit Card | Excel sheet | Excel File    |
      | 2010 | Chevrolet | Corvette | Base | None     | tire          | default  | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | bopiscc     | WebOrders.xls |

  @dt
  @at
  @web
  @myAccountOrders
  @bopisIntegrationRegression
  Scenario Outline: BOPIS_Staggered Vehicle_With Promotion_With Appointment_CC1
    When I change to the default store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I select install with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    And  I enter customer information for "<Customer>"
    And  I click on the "Place Order" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I store the order number
    And  I take page screenshot
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | FitmentOption | ItemCode | Checkout | Customer                   | Credit Card      | Excel sheet | Excel File    |
      | 2010 | Chevrolet | Corvette | Base | None     | tire          | 33537    | default  | DEFAULT_CUSTOMER_BOPIS_CC1 | CarCareOne Bopis | bopisccone  | WebOrders.xls |

  @dt
  @at
  @web
  @myAccountOrders
  @bopisIntegrationRegression
  Scenario Outline: BOPIS_Without Appointment_FET_Certificates_CreditCard
    When I change to the default store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I see a purchase quantity of "<Quantity>"
    And  I verify the item total "displayed" on cart page for "<ItemCode>"
    And  I verify the FET fee amount on the shopping cart if applicable to item "<ItemCode>"
    When I select the optional "Certificates" fee for item
    And  I select the checkout option "<Checkout>"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click next step for customer information
    And  I enter customer information for "<Customer>"
    And  I click on the "Place Order" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I store the order number
    And  I take page screenshot
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"

    Examples:
      | Year | Make | Model | Trim         | Assembly       | FitmentOption | ProductName   | ItemCode | Quantity | Checkout | Customer                    | Credit Card | Excel sheet | Excel File    |
      | 2016 | Ram  | 2500  | MEGA CAB 4WD | 275 /70 R18 E1 | tire          | Dura Grappler | 40480    | 4        | default  | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | bopisfet    | WebOrders.xls |

  @dt
  @at
  @web
  @myAccountOrders
  @ropisIntegrationRegression
  Scenario Outline: ROPIS_Regular Vehicle_With Appointment
    When I change to the default store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I schedule an appointment for my current store
    And  I select checkout with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify date and time on the customer details appointment page
    And  I enter customer information for "<Customer>"
    And  I enter "valid" customer phone number: "random"
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I click on the "Continue To Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I take page screenshot
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Customer            | Excel File    | Excel sheet      |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | DEFAULT_CUSTOMER_AZ | WebOrders.xls | ropisappointment |

  @dt
  @at
  @web
  @myAccountOrders
  @ropisIntegrationRegression
  Scenario Outline: ROPIS_Staggered Vehicle_Promotional Product_With Appointment
    When I change to the default store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I schedule an appointment for my current store
    And  I select checkout with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify date and time on the customer details appointment page
    When I enter customer information for "<Customer>"
    And  I enter "valid" customer phone number: "random"
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I take page screenshot
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | FitmentOption | ItemCode | Customer            | Excel sheet      | Excel File    |
      | 2010 | Chevrolet | Corvette | Base | none     | tire          | 33537    | DEFAULT_CUSTOMER_AZ | ropispromotional | WebOrders.xls |

  @dt
  @at
  @web
  @myAccountOrders
  @ropisIntegrationRegression
  Scenario Outline: ROPIS_Regular Vehicle_FET_Certificates_Without Appointment
    When I change to the default store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the FET fee amount on the shopping cart if applicable to item "<ItemCode>"
    And  I select the optional "Certificates" fee for item
    And  I select the checkout option "<Checkout>"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click next step for customer information
    And  I enter customer information for "<Customer>"
    And  I enter "valid" customer phone number: "random"
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I click on the "Continue To Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I take page screenshot
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make | Model | Trim         | Assembly       | FitmentOption | ItemCode | Checkout | Customer            | Excel sheet | Excel File    |
      | 2016 | Ram  | 2500  | MEGA CAB 4WD | 275 /70 R18 E1 | tire          | 40480    | default  | DEFAULT_CUSTOMER_AZ | ropisfet    | WebOrders.xls |

  @dt
  @at
  @web
  @myAccountOrders
  @ropisIntegrationRegression
  Scenario Outline: ROPIS_Free Text Search_Without Appointment
    When I change to the default store
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click next step for customer information
    And  I enter customer information for "<Customer>"
    And  I enter "valid" customer phone number: "random"
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I take page screenshot
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | ItemCode | Checkout | Customer            | Excel sheet        | Excel File    |
      | 40361    | default  | DEFAULT_CUSTOMER_AZ | ropisnoappointment | WebOrders.xls |

  @dt
  @at
  @web
  @myAccountOrders
  @ropisIntegrationRegression
  @bwSunsetSuiteWeb
  Scenario Outline: Schedule Appointment_One Service Option
    When I change to the default store
    And  I open the "APPOINTMENTS" navigation link
    And  I select service option(s): "<ServiceOption>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click continue for appointment customer details page
    And  I select "Schedule Appointment" after entering customer information for "<Customer>"
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer>"
    Then I should see an appointment confirmation message for "<Customer>" with service options: "<ServiceOption>"
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I take page screenshot
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | ServiceOption             | Customer            | Excel sheet        | Excel File    |
      | Tire Rotation and Balance | DEFAULT_CUSTOMER_AZ | serviceappointment | WebOrders.xls |

  @dt
  @at
  @web
  @myAccountOrders
  @ropisIntegrationRegression
  @bwSunsetSuiteWeb
  Scenario Outline: Schedule Appointment_Three Service Options
    When I change to the default store
    And  I open the "APPOINTMENTS" navigation link
    And  I select service option(s): "<ServiceOption>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click continue for appointment customer details page
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I select "Schedule Appointment" after entering customer information for "<Customer>"
    Then I should see an appointment confirmation message for "<Customer>" with service options: "<ServiceOption>"
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I take page screenshot
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | ServiceOption                                         | Customer            | Excel sheet                    | Excel File    |
      | Tire Rotation and Balance,Tire Inspection,Flat Repair | DEFAULT_CUSTOMER_AZ | serviceappointmentthreeoptions | WebOrders.xls |

  @dtd
  @web
  @myAccountOrders
  @dtdGuestPaypalExpress
  @dtdIntegrationRegression
  Scenario Outline: DTD_Free Text Search_Paypal express
    When I go to the homepage
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I select paypal checkout
    And  I switch to "Paypal" window
    And  I log into paypal as "<Customer>"
    And  I continue with the paypal payment
    And  I switch to main window
    When I click on the "Continue to Shipping Method" button
    And  I select the default shipping option as "<Customer>"
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer>"
    Then I place the order for "<Customer>"
    And  I am brought to the order confirmation page
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | ItemCode | Customer           | Excel sheet | Excel File    |
      | 29935    | paypal_customer_az | dtdpaypal   | WebOrders.xls |

  @dtd
  @web
  @myAccountOrders
  @dtdIntegrationRegression
  @dtdStaggeredVehicleCreditCard
  Scenario Outline: DTD_Staggered Vehicle_Credit Card
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    Then I verify "Checkout" page is displayed
    When I enter shipping info as "<Customer>"
    And  I click on the "Continue to Shipping Method" button
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I record the time stamp "before" creating the order in YYYYMMDDHHMMSS format
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | FitmentOption | ItemCode | Customer            | ShippingOption | Credit Card      | Excel sheet  | Excel File    |
      | 2010 | Chevrolet | Corvette | Base | none     | tire          | 36259    | default_customer_az | Ground         | MasterCard Bopis | dtdstaggered | WebOrders.xls |

  @dtd
  @web
  @myAccountOrders
  @dtdIntegrationRegression
  @dtdFreeTextFETOverSizedStuddedTirePaypal
  Scenario Outline: DTD_Free Text Search_FET,Over Sized,Heat Cycling,Studding fees_Taxes_Paypal
    When I go to the homepage
    And  I do a free text search for "<ItemCodeA>" and hit enter
    And  I add item to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCodeB>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    Then I verify the FET fee amount on the shopping cart if applicable to item "<ItemCodeA>"
    When I select the optional "Heat Cycling" fee for item
    And  I select the optional "Studding" fee for item
    And  I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select the default shipping option as "<Customer>"
    And  I select the "<Shipping>" payment option
    And  I select paypal checkout
    And  I switch to "Paypal" window
    And  I log into paypal as "<Customer>"
    And  I record the time stamp "before" creating the order in YYYYMMDDHHMMSS format
    And  I continue with the paypal payment
    And  I switch to main window
    Then I am brought to the order confirmation page
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | ItemCodeA | ItemCodeB | Checkout | Customer           | Shipping | Excel sheet | Excel File    |
      | 10937     | 40297     | default  | paypal_customer_az | paypal   | dtdfet      | WebOrders.xls |

  @dtd
  @web
  @myAccountOrders
  @dtdIntegrationRegression
  @dtdRegularTireAndWheelCreditCard
  Scenario Outline: DTD_Regular Vehicle_Tire and Wheel_With Promotion_Credit Card
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select "Add To Cart"
    And  I add item to my cart and "Continue Shopping"
    And  I open the My Vehicles popup
    And  I select "shop wheel" link
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I click on the "Continue To Payment" button
    And  I enter payment info with "<credit card>" and confirm Checkout Summary as "<Customer>"
    And  I record the time stamp "before" creating the order in YYYYMMDDHHMMSS format
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Customer            | Checkout | credit card | Excel sheet | Excel File    |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | default_customer_az | default  | visa        | dtdpromo    | WebOrders.xls |
    
  @dtd
  @web
  @myAccountOrders
  @dtdIntegrationRegression
  @dtdFreeTextSearchDifferentShippingAndBillingAddress
  Scenario Outline: DTD_Free Text Search_Different Shipping and Billing Address
    When I go to the homepage
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Shipping Customer>" and continue to next page
    And  I select shipping option: "Ground" as "<Shipping Customer>"
    And  I record the time stamp "before" creating the order in YYYYMMDDHHMMSS format
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Payment Customer>"
    And  I place the order for "<Payment Customer>"
    Then I am brought to the order confirmation page
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer>"
    And  I store the order number
    And  I store the total amount
    And  I write the total amount into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "2" in excel file with name "<Excel File>"
    And  I write the web order number into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "1" in excel file with name "<Excel File>"
    And  I write the date into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "3" in excel file with name "<Excel File>"
    And  I write the timestamp before placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "4" in excel file with name "<Excel File>"
    And  I write the timestamp after placing the order into the excel sheet with sheet name "<Excel sheet>" into row number "2" and cell number "5" in excel file with name "<Excel File>"

    Examples:
      | ItemCode | Checkout | Shipping Customer   | Payment Customer     | Excel sheet        | Excel File    |
      | 26899    | default  | default_customer_az | default_customer_can | dtdbillingshipping | WebOrders.xls |


  @posdmPosdtValidationForWebSpos
  Scenario Outline: Transaction validation for transaction types for Web SPOS
    When I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from sheet name "<Sheet Name>"
    And  I extract all values from excel from sheet name "<Sheet Name>"
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    Then I verify total transaction amount in posdm/posdt from sheet name "<Sheet Name>"
    And  I verify "Transaction Type" field has the text of "<Transaction Type>" in posdm
    When I double click on Transaction Number
    Then I verify Article Identifier and Sales Price for Line items in sales data
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame in SAP
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I execute
    Then I verify total transaction amount in posdt from sheet name "<Sheet Name>"
    And  I verify "Transaction Type" field has the text of "<Transaction Type>" from examples in posdt
    When I double click on Transaction Number in posdt
    Then I verify Article Identifier and Sales Price for Line items in sales data in posdt

    Examples:
      | Sheet Name              | TCode 1       | Transaction Type |
      | bopismaster_SPOS        | /n/posdw/mon0 | 1207             |
      | bopisvisa_SPOS          | /n/posdw/mon0 | 1207             |
      | bopiscc_SPOS            | /n/posdw/mon0 | 1207             |
      | bopisccone_SPOS         | /n/posdw/mon0 | 1207             |
      | bopisfet_SPOS           | /n/posdw/mon0 | 1207             |
      | ropisappointment_SPOS   | /n/posdw/mon0 | 1202             |
      | ropispromotional_SPOS   | /n/posdw/mon0 | 1202             |
      | ropisfet_SPOS           | /n/posdw/mon0 | 1202             |
      | ropisnoappointment_SPOS | /n/posdw/mon0 | 1202             |

  @posdmPosdtValidationForWebDtd
  Scenario Outline: Transaction validation for transaction types for Web DTD
    When I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from sheet name "<Sheet Name>"
    And  I extract all values from excel from sheet name "<Sheet Name>"
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    Then I verify total transaction amount in posdm/posdt from sheet name "<Sheet Name>"
    And  I verify "Transaction Type" field has the text of "<Transaction Type>" in posdm
    When I double click on Transaction Number
    Then I verify Article Identifier and Sales Price for Line items in sales data
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame in SAP
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I execute
    Then I verify total transaction amount in posdt from sheet name "<Sheet Name>"
    And  I verify "Transaction Type" field has the text of "<Transaction Type>" from examples in posdt
    When I double click on Transaction Number in posdt
    Then I verify Article Identifier and Sales Price for Line items in sales data in posdt

    Examples:
      | Sheet Name       | TCode 1       | Transaction Type |
      | dtdpaypal_DTD    | /n/posdw/mon0 | 1202             |
      | dtdstaggered_DTD | /n/posdw/mon0 | 1202             |
      | dtdfet_DTD       | /n/posdw/mon0 | 1202             |
      | dtdpromo_DTD     | /n/posdw/mon0 | 1202             |

  @sapValidationForWebSpos
  Scenario Outline: Transaction validation in SAP for Web orders for DT
    When I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from sheet name "<Sheet Name>"
    And  I extract all values from excel from sheet name "<Sheet Name>"
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    And  I wait for execution to be completed
    And  I click on "document flow button" in posdm page
    And  I wait for "5" seconds
    And  I click on "Document flow outbound" in posdm page
    And  I click "Vertical Scroll" till "external document" is visible in posdm
    And  I click "Vertical Scroll" "5" times in posdm
    And  I save the "Outbox IDOC" element text value in PosDm page
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table>" in PosDm page
    And  I hit return
    And  I enter saved "Outbox IDOC" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "Not Equal to"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDm page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table 2>" in PosDm page
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I click on "Execute button SAP" in posdm page
    And  I save the "Idoc value 1" element text value for web
    And  I enter t-code "<TCode>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I double click on "Article" in Idoc page
    And  I click on "Back Button" in order to cash page
    And  I double click on "Billing" in Idoc page
    Then I verify the Net Values and articles in billing document
    When I click on "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on element with value of "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on "Views" in IDOC page
    And  I click on "List Output"
    And  I wait for "5" seconds
    Then I validate "<GL Type>" entered matches original "<GL Account>" in wper

    Examples:
      | TCode   | TCode 1       | Table TCode | Outbound/Inbound Idoc Table | Outbound/Inbound Idoc Table 2 | Sheet Name              | GL Type       | GL Account |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | bopismaster_SPOS        | Bopis Deposit | 214635     |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | bopisvisa_SPOS          | Bopis Deposit | 214635     |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | bopiscc_SPOS            | Bopis Deposit | 214635     |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | bopisccone_SPOS         | Bopis Deposit | 214635     |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | bopisfet_SPOS           | Bopis Deposit | 214635     |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | ropisappointment_SPOS   | STR Account   | 119060     |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | ropispromotional_SPOS   | STR Account   | 119060     |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | ropisfet_SPOS           | STR Account   | 119060     |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | ropisnoappointment_SPOS | STR Account   | 119060     |

  @sapValidationForWebDtd
  Scenario Outline: Transaction validation in SAP for Web orders for DTD
    When I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from sheet name "<Sheet Name>"
    And  I extract all values from excel from sheet name "<Sheet Name>"
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    And  I wait for execution to be completed
    And  I click on "document flow button" in posdm page
    And  I wait for "5" seconds
    And  I click on "Document flow outbound" in posdm page
    And  I click "Vertical Scroll" till "external document" is visible in posdm
    And  I click "Vertical Scroll" "5" times in posdm
    And  I save the "Outbox IDOC" element text value in PosDm page
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table>" in PosDm page
    And  I hit return
    And  I enter saved "Outbox IDOC" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "Not Equal to"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDm page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table 2>" in PosDm page
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I click on "Execute button SAP" in posdm page
    And  I save the "Idoc value 1" element text value for web
    And  I enter t-code "<TCode>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I double click on "Article" in Idoc page
    And  I click on "Back Button" in order to cash page
    And  I double click on "Billing" in Idoc page
    Then I verify the Net Values and articles in billing document
    When I click on "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on element with value of "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on "Views" in IDOC page
    And  I click on "List Output"
    And  I wait for "5" seconds
    Then I validate "<GL Type>" entered matches original "<GL Account>" in wper

    Examples:
      | TCode   | TCode 1       | Table TCode | Outbound/Inbound Idoc Table | Outbound/Inbound Idoc Table 2 | Sheet Name       | GL Type     | GL Account |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | dtdpaypal_DTD    | STR Account | 119060     |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | dtdstaggered_DTD | STR Account | 119060     |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | dtdfet_DTD       | STR Account | 119060     |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | dtdpromo_DTD     | STR Account | 119060     |

  @dtdWebOrderSalesOrderValidation
  @salesOrderValidation
  Scenario Outline: DTD Web Order Sales Order Validation [Method - Hybris Web Order direct Search]
    And  I extract "web order" from excel sheet which is in "2"th row and "1"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "date" from excel sheet which is in "2"th row and "3"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "before" from excel sheet which is in "2"th row and "4"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "after" from excel sheet which is in "2"th row and "5"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttiredirect"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    When I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header" in posdm page
    And  I enter start time and end time in posdm
    And  I enter site numbers for DTD in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I click on "Execute" in posdm page
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter start time and end time in posdm
    And  I enter site numbers for DTD in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I execute
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number in posdt and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    Examples:
      | TCode 1       | Excel File    | Excel Sheet        | Transaction Type |
      | /n/posdw/mon0 | WebOrders.xls | dtdpaypal          | 1014             |
      | /n/posdw/mon0 | WebOrders.xls | dtdstaggered       | 1014             |
      | /n/posdw/mon0 | WebOrders.xls | dtdfet             | 1014             |
      | /n/posdw/mon0 | WebOrders.xls | dtdpromo           | 1014             |
      | /n/posdw/mon0 | WebOrders.xls | dtdbillingshipping | 1014             |

  @dtWebOrderSalesOrderValidation
  @salesOrderValidation
  Scenario Outline: DT Web Order Sales Order Validation [Method - Hybris Web Order direct Search]
    And  I extract "web order" from excel sheet which is in "2"th row and "1"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "date" from excel sheet which is in "2"th row and "3"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "before" from excel sheet which is in "2"th row and "4"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "after" from excel sheet which is in "2"th row and "5"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttire"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header" in posdm page
    And  I enter start time and end time in posdm
    And  I enter store number for DT orders in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I click on "Execute" in posdm page
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter start time and end time in posdm
    And  I enter store number for DT orders in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I execute
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number in posdt and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    Examples:
      | TCode 1       | Excel File    | Excel Sheet                    | Transaction Type |
      | /n/posdw/mon0 | WebOrders.xls | ropisappointment               | 1014             |
      | /n/posdw/mon0 | WebOrders.xls | ropispromotional               | 1014             |
      | /n/posdw/mon0 | WebOrders.xls | ropisfet                       | 1014             |
      | /n/posdw/mon0 | WebOrders.xls | ropisnoappointment             | 1014             |

  @dtWebOrderSalesOrderValidation
  @salesOrderValidation
  @bwSunsetSuiteSap
  Scenario Outline: DT Service Appointment Sales Order Validation [Method - Hybris Web Order direct Search]
    And  I extract "web order" from excel sheet which is in "2"th row and "1"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "date" from excel sheet which is in "2"th row and "3"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "before" from excel sheet which is in "2"th row and "4"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "after" from excel sheet which is in "2"th row and "5"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I set appointment Flag to true
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttire"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header" in posdm page
    And  I enter start time and end time in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I click on "Execute" in posdm page
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter start time and end time in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I execute
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number in posdt and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    Examples:
      | TCode 1       | Excel File    | Excel Sheet                    | Transaction Type |
      | /n/posdw/mon0 | WebOrders.xls | serviceappointment             | 1003             |
      | /n/posdw/mon0 | WebOrders.xls | serviceappointmentthreeoptions | 1003             |

  @dtdLegacyWebOrderValidation
  Scenario Outline: DTD Web Order Sales Order Validation [Method - Legacy Web Order direct Search]
    When I extract "invoice" from excel sheet which is in "2"th row and "2"th cell from sheet name "<Excel Sheet>" and file name "<Genesis Excel File>"
    And  I extract "web order" from excel sheet which is in "2"th row and "1"th cell from sheet name "<Excel Sheet>" and file name "<Web Order Excel File>"
    And  I extract "date" from excel sheet which is in "2"th row and "3"th cell from sheet name "<Excel Sheet>" and file name "<Web Order Excel File>"
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttiredirect"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header" in posdm page
    And  I enter site numbers for DTD in posdm
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter site numbers for DTD in posdm
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I execute
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number in posdt and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types

    Examples:
      | TCode 1       | Excel Sheet | Genesis Excel File      | Web Order Excel File |
      | /n/posdw/mon0 | ct          | nexusGenesisOutput.xlsx | nexusWebOrders.xls   |

  @dtLegacyWebOrderValidation
  Scenario Outline: DT Web Order Sales Order Validation [Method - Legacy Web Order direct Search]
    When I extract "invoice" from excel sheet which is in "2"th row and "2"th cell from sheet name "<Excel Sheet>" and file name "<Genesis Excel File>"
    And  I extract "web order" from excel sheet which is in "2"th row and "1"th cell from sheet name "<Excel Sheet>" and file name "<Web Order Excel File>"
    And  I extract "date" from excel sheet which is in "2"th row and "3"th cell from sheet name "<Excel Sheet>" and file name "<Web Order Excel File>"
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttire"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header" in posdm page
    And  I enter store number for DT orders in posdm
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter store number for DT orders in posdm
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I execute
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number in posdt and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    And  I make a post call to car item header service with field name "HYBORDRNUM"

    Examples:
      | TCode 1       | Excel Sheet | Genesis Excel File      | Web Order Excel File |
      | /n/posdw/mon0 | ct          | nexusGenesisOutput.xlsx | nexusWebOrders.xls   |

  @bopisWebOrderSalesOrderValidation
  @salesOrderValidation
  Scenario Outline: BOPIS Web Order Sales Order Validation [Method - Hybris Web Order direct Search]
    And  I extract "web order" from excel sheet which is in "2"th row and "1"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I extract "date" from excel sheet which is in "2"th row and "3"th cell from sheet name "<Excel Sheet>" and file name "<Excel File>"
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttire"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header" in posdm page
    And  I enter BOPIS web order number into field name with label "Transaction Number"
    And  I click on "Execute" in posdm page
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter BOPIS web order number into field name with label "Transaction Number"
    And  I execute
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number in posdt and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types

    Examples:
      | TCode 1       | Excel File    | Excel Sheet |
      | /n/posdw/mon0 | WebOrders.xls | bopisvisa   |
      | /n/posdw/mon0 | WebOrders.xls | bopismaster |
      | /n/posdw/mon0 | WebOrders.xls | bopiscc     |
      | /n/posdw/mon0 | WebOrders.xls | bopisccone  |
      | /n/posdw/mon0 | WebOrders.xls | bopisfet    |