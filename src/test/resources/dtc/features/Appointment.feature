@appointment
Feature: Appointments

  Background:
    Given I change to the default store

  @at
  @dt
  @web
  @8657
  @mobile
  @smoketest
  @appointmentSmoke
  @sendStoreLocationToPhone
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS SmokeTest DT or AT Schedule Service 3  (ALM #8657)
    When I search for store within "25" miles of "<ZipCode>"
    And  I "continue" the Welcome Popup
    And  I select "Share" for store #"1" in the location results
    Then I verify the "Share Store Details" modal is displayed
    When I select send to "Phone" "<PhoneNumber>" on "Store Details" page
    Then I verify "Success" message displayed
    When I close popup modal
    And  I schedule an appointment for my current store
    And  I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select first available appointment date
    And  I click continue for appointment customer details page
    And  I select "Schedule Appointment" after entering customer information for "<Customer>"
    Then I should see an appointment confirmation message for "<Customer>" with service options: "<ServiceOptions>"
    And  I store the order number

    Examples:
      | ZipCode | PhoneNumber | ServiceOptions                | Customer            |
      | 32003   | 5555555555  | New Tires/Wheels Consultation | DEFAULT_CUSTOMER_AZ |

  @at
  @dt
  @web
  @7001
  @mobile
  @ordersRegression
  @serviceUpAptwithProductsShoppingCart
  Scenario Outline: ServiceUp Appointment with Products Shopping Cart (ALM #7001)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    And  I see a purchase quantity of "<Quantity>"
    And  I extract the "sales tax" on the cart page
    When I select the checkout option "<Checkout>"
    Then I verify "install with appointment" option is enabled on the Checkout page
    When I create an appointment with defaults from Checkout
    Then I extract date and time for validation
    When I click next step for customer information
    Then I verify date and time on the customer details appointment page
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I store the order number
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | Year | Make  | Model  | Trim     | Assembly | Header      | ProductName   | ItemCode | Quantity | Checkout         | Customer            |
      | 2015 | Honda | Accord | Coupe EX | none     | tire result | Assurance A/S | 24824    | 4        | with appointment | default_customer_az |

  @dt
  @at
  @bba
  @9384
  @scheduleAppt35DaysInAdvance
  @web
  @mobile
  @appointmentBBA
  Scenario Outline: WebServiceAppointment_35BusinessDaysInAdvance (ALM #9384)
    When I open the "SERVICES" navigation link
    And  I click on the "Schedule an appointment" link
    And  I select service option(s): "<ServiceOption>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select the "<available>" appointment to verify total available appointment days are "35"
    And  I click continue for appointment customer details page
    And  I select "Schedule Appointment" after entering customer information for "<Customer>"
    Then I should see an appointment confirmation message for "<Customer>" with service options: "<ServiceOption>"
    And  I should see my previously selected store, date and time, in the appointment details section
    And  I store the order number

    Examples:
      | ServiceOption             | Customer            | available |
      | Tire Rotation and Balance | DEFAULT_CUSTOMER_AZ | Last      |

  @dt
  @at
  @bba
  @8153
  @scheduleAppt10DaysInAdvance
  @web
  @appointmentBBA
  Scenario Outline: ScheduleAppointment_10BusinessDaysInAdvance (ALM #8153)
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I verify "ProductName" "<ProductName>" is in the cart
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify "install with appointment" option is enabled on the Checkout page
    And  I verify an appointment for a checkout with item(s) can be created for 10 business days later
    When I reserve items and complete checkout for "<Customer>"
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I store the order number

    Examples:
      | ProductName        | ItemCode | Checkout         | Customer            |
      | Silver Edition III | 29935    | with appointment | default_customer_az |

  @dt
  @at
  @8153
  @bba
  @scheduleAppt10DaysInAdvance
  @mobile
  @appointmentBBA
  Scenario Outline: Mobile - ScheduleAppointment_10BusinessDaysInAdvance (ALM #8153)
    When I do a free text search for "<ItemCode>"
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify "install with appointment" option is enabled on the Checkout page
    And  I verify an appointment for a checkout with item(s) can be created for 10 business days later
    When I reserve items and complete checkout for "<Customer>"
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I store the order number

    Examples:
      | ProductName        | ItemCode | Checkout         | Customer            |
      | Silver Edition III | 29935    | with appointment | default_customer_az |

  @at
  @dt
  @web
  @8656
  @15779
  @mobile
  @smoketest
  @appointmentSmoke
  @ordersRegression
  Scenario Outline: Reserve product without Appointment using Vehicle Search via My Vehicles (ALM #8656, @15779)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId           |
      | amountOfSale         |
      | siteNumber           |
      | orderType            |
      | customerType         |
      | webOrderOrigin       |
      | vehicle              |
    And  I assert order xml list values
      | tax          |
      | productCode  |
      | discount     |
    And  I assert order xml customer node values
    And  I assert order xml subItems
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |

    Examples:
      | Year | Make   | Model | Trim       | Assembly                            | Checkout            | Customer            | Reason                              |
      | 2010 | Nissan | 370Z  | Coupe Base | F 245 /40 R19 SL - R 275 /35 R19 SL | without appointment | default_customer_az | Make an appointment at a later time |

  @dt
  @at
  @bba
  @web
  @8528
  @mobile
  @appointmentBBA
  @reserveWithAppointmentWithoutChangingStoreViaHomepageVehicleSearch
  Scenario Outline: Reserve Product with Appointment via Search by Vehicle using the Homepage (ALM #8528)
  """| 2015 | Honda | Accord | Coupe EX | none     | All Wheels    | Results for Wheels | 56178    | with appointment | default_customer_az |"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "wheel"
    Then I verify the PLP header message contains "<Header>"
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify "ProductName" "<ProductName>" is in the cart
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify "install with appointment" option is enabled on the Checkout page
    When I create an appointment with defaults from Checkout
    And  I reserve items and complete checkout for "<Customer>"
    Then I store the order number

    Examples:
      | Year | Make  | Model  | Trim     | Assembly | Header       | ItemCode | ProductName | Checkout         | Customer            |
      | 2015 | Honda | Accord | Coupe EX | none     | wheel result | 20744    | Cross       | with appointment | default_customer_az |

  @dt
  @at
  @bba
  @web
  @8531
  @appointmentBBA
  @reserveWithoutAppointmentWithoutChangingStore
  Scenario Outline: Reserve Product without Appointment (ALM #8531)
  """| PBX A/T HARDCORE | 34302    | without appointment | default_customer_az |"""
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I verify "ProductName" "<ProductName>" is in the cart
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify "install without appointment" option is enabled on the Checkout page
    When I reserve items and complete checkout for "<Customer>"
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I store the order number

    Examples:
      | ProductName        | ItemCode | Checkout | Customer            |
      | Silver Edition III | 29935    | default  | default_customer_az |
      | Assurance A/S      | 24824    | default  | default_customer_az |

  @dt
  @at
  @bba
  @mobile
  @8531
  @appointmentBBA
  @reserveWithoutAppointmentWithoutChangingStore
  Scenario Outline: Mobile - Reserve Product without Appointment (ALM #8531)
    When I do a free text search for "<ItemCode>"
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify "install with appointment" option is enabled on the Checkout page
    When I reserve items for "<Customer>"
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I store the order number

    Examples:
      | ProductName        | ItemCode | Checkout            | Customer            |
      | Silver Edition III | 29935    | without appointment | default_customer_az |

  @at
  @dt
  @web
  @8655
  @8724
  @smoketest
  @appointmentSmoke
  @reserveWithAppointmentViaFreeTextSearch
  Scenario Outline: Reserve Product with Appointment (ALM #8655,8724)
  """| PBX A/T HARDCORE | 34302    | with appointment | default_customer_az |
    TODO: Staging data for 8724. dtqa1 does not appear to contain this product/itemCode; Item needs to be rebate eligible
    | PROVIDER ENTRADA HT | 12168    | with appointment | default_customer_az |"""
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify "install with appointment" option is enabled on the Checkout page
    When I create an appointment with defaults from Checkout
    And  I reserve items for "<Customer>"
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I store the order number

    Examples:      
      | ProductName        | ItemCode | Checkout         | Customer            |
      | Silver Edition III | 29935    | with appointment | default_customer_az |

  @dt
  @at
  @8655
  @8724
  @mobile
  @smoketest
  @appointmentSmoke
  @reserveWithAppointmentViaFreeTextSearch
  Scenario Outline: Mobile - Reserve Product with Appointment (ALM #8655,8724)
    When I do a free text search for "<ItemCode>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify "install with appointment" option is enabled on the Checkout page
    When I create an appointment with defaults from Checkout
    And  I reserve items for "<Customer>"
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I store the order number

    Examples:      
      | ProductName        | ItemCode | Checkout         | Customer            |
      | Silver Edition III | 29935    | with appointment | default_customer_az |

  @dt
  @at
  @bba
  @web
  @25450
  @appointmentBBA
  @checkoutWithoutApptWithSpecialOrderArticle
  Scenario Outline: Checkout without appointment with special order article (ALM #25450)
  """|85250   |AZP 20 |34299    |PBX A/T HARDCORE  |without appointment | default_customer_az |"""
    When I go to the homepage
    And  I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I verify "ProductName" "<ProductName>" is in the cart
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    And  I select the checkout without install reason "<Reason>"
    And  I reserve items and complete checkout for "<Customer>"
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I store the order number

    Examples:      
      | ZipCode | ProductName        | ItemCode | Checkout            | Customer            | Reason                              |
      | 48911   | Silver Edition III | 29935    | without appointment | default_customer_az | Make an appointment at a later time |

  @dt
  @at
  @bba
  @25450
  @mobile
  @appointmentBBA
  @checkoutWithoutApptWithSpecialOrderArticle
  Scenario Outline: Mobile - Checkout without appointment with special order article (ALM #25450)
    When I go to the homepage
    And  I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    When I do a free text search for "<ItemCode>"
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    And  I select the checkout without install reason "<Reason>"
    And  I reserve items and complete checkout for "<Customer>"
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I store the order number

    Examples:
      | ZipCode | ProductName        | ItemCode | Checkout            | Customer            | Reason                              |
      | 48911   | Silver Edition III | 29935    | without appointment | default_customer_az | Make an appointment at a later time |

  @dt
  @web
  @8530
  @scheduleApptCheckout10DaysInAdvanceDT
  Scenario Outline: HYBRIS_209_APPOINTMENT_Checkout_Schedule Appointment Not Changing Store_10 Business days (ALM #8530)
  """TODO - AT / DT PROD, STG, QA1 will fail when run with Internet Explorer; see defect 7018"""
    When I search for store within "25" miles of "<City>"
    And  I select make "<Zip>" my store
    And  I do a free text search for "<SearchTerm>" and hit enter
    And  I select "All-Season tires" from the Product Brand page
    And  I select the "In Stock" checkbox
    And  I select from the "Quick Filters" filter section, "single" option(s): "In Stock"
    And  I select from the "Brands" filter section, "single" option(s): "<BrandOption>"
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify "install with appointment" option is enabled on the Checkout page
    And  I verify an appointment for a checkout with item(s) can be created for 10 business days later
    When I reserve items and complete checkout for "<Customer>"
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I store the order number

    Examples:
      | City       | Zip   | SearchTerm | BrandOption | ItemCode | ProductName        | Checkout         | Customer            |
      | Scottsdale | 85260 | tires      | ARIZONIAN   | 29935    | Silver Edition III | with appointment | default_customer_az |

  @at
  @web
  @8530
  @scheduleApptCheckout10DaysInAdvanceAT
  Scenario Outline: HYBRIS_209_APPOINTMENT_Checkout_Schedule Appointment Not Changing Store_10 Business days (ALM #8530)
  """TODO - AT / DT PROD, STG, QA1 will fail when run with Internet Explorer; see defect 7018"""
    When I do a free text search for "<SearchTerm>" and hit enter
    And  I select "All-Season tires" from the Product Brand page
    And  I select from the "Quick Filters" filter section, "single" option(s): "In Stock"
    And  I select from the "Brands" filter section, "single" option(s): "<BrandOption>"
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify "install with appointment" option is enabled on the Checkout page
    And  I verify an appointment for a checkout with item(s) can be created for 10 business days later
    When I reserve items and complete checkout for "<Customer>"
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I store the order number

    Examples:
      | SearchTerm | BrandOption | ItemCode | ProductName | Checkout         | Customer            |
      | tires      | VEENTO TIRE | 17901    | G-3         | with appointment | default_customer_az |

  @at
  @dt
  @web
  @8616
  @scheduleServiceFromServiceHeaderLink
  Scenario Outline: Schedule Appointment for Tire Rotation/Rebalance (ALM #8616)
    When I open the "SERVICES" navigation link
    And  I click the "Schedule an appointment" menu option
    And  I select service option(s): "<ServiceOption>"
    And  I select default date and time
    Then I verify default store on the customer details appointment page
    When I click continue for appointment customer details page
    Then I verify date and time on the customer details appointment page
    When I select "Schedule Appointment" after entering customer information for "<Customer>"
    Then I should see an appointment confirmation message for "<Customer>" with service options: "<ServiceOption>"
    And  I should see my previously selected store, date and time, in the appointment details section
    And  I store the order number

    Examples:
      | ServiceOption             | Customer            |
      | Tire Rotation and Balance | DEFAULT_CUSTOMER_AZ |

  @dt
  @at
  @web
  @12315
  @12196
  @regression
  @appointmentRegression
  Scenario Outline: HYBRIS_ORDERS_BOPIS_Cart Checkout_Validating redirection to Appointment Details (ALM #12315, #12196)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I verify "ProductName" "<ProductName>" is in the cart
    And  I verify the required fees and add-ons sections are expanded
    When I extract the "sales tax" on the cart page
    And  I select the checkout option "<Checkout>"
    Then I verify "install without appointment" option is enabled on the Checkout page
    And  I verify "install with appointment" option is enabled on the Checkout page
    And  I should see install without appointment tooltip
    And  I verify install without appointment tooltip message
    When I select install with appointment
    Then I verify list of available appointment dates
    And  I verify unavailable time slots are disabled
    And  I verify time slot hours increase from top to bottom
    And  I verify time slot list is scrollable
    And  I verify message bar with appointment day date and time
    When I close the Appointment Selected message bar
    Then I verify the Appointment Selected message is not displayed
    When I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify date and time on the customer details appointment page
    When I click Edit Appointment link
    And  I select last available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify date and time on the customer details appointment page
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I store the order number
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ProductName        | ItemCode | Checkout | Customer            |
      | 2012 | Honda | Civic | Coupe DX | none     | Silver Edition III | 29935    | default  | DEFAULT_CUSTOMER_AZ |

  @dt
  @at
  @web
  @12197
  @regression
  @appointmentRegression
  Scenario Outline: HYBRIS_ORDERS_BOPIS_Cart Checkout_Validating redirection to Appointment Details (ALM #12197)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I extract the "sales tax" on the cart page
    And  I select the checkout option "<Checkout>"
    Then I verify "install without appointment" option is enabled on the Checkout page
    And  I verify "install with appointment" option is enabled on the Checkout page
    And  I should see install without appointment tooltip
    And  I verify install without appointment tooltip message
    When I select install with appointment
    Then I verify list of available appointment dates
    And  I verify unavailable time slots are disabled
    And  I verify time slot hours increase from top to bottom
    When I select first available appointment date
    Then I verify time slot list is scrollable
    And  I verify message bar with appointment day date and time
    When I close the Appointment Selected message bar
    Then I verify the Appointment Selected message is not displayed
    When I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify date and time on the customer details appointment page
    When I click Edit Appointment link
    And  I select last available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify date and time on the customer details appointment page
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    And  I store the order number
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode | Checkout | Customer            |
      | 2012 | Honda | Civic | Coupe DX | none     | 29935    | default  | default_customer_az |

  @dt
  @at
  @web
  @12210
  @regression
  @appointmentRegression
  Scenario Outline: HYBRIS_ORDERS_BOPIS_Cart Checkout_Validating available hours for appointment on the basis of stock level_58 (ALM #12210)
  """ TODO: Find articles we can use that match both front-end and SAP so validations can be made effectively.
  This test can fail because the inventory message doesn't reflect the actual stock count. """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item with message "<Stock Status>" to my cart
    And  I click View Cart button
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    And  I select install with appointment
    Then I verify appointment dates and appointment times sections are displayed
    And  I verify list of available appointment dates
    And  I verify appointment dates start on expected date "<Stock Status>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Checkout | Stock Status                             |
      | 2012 | Honda | Civic | Coupe DX | none     | default  | Available today                          |
      | 2012 | Honda | Civic | Coupe DX | none     | default  | Order now, available in 3 - 5 days       |
      | 2012 | Honda | Civic | Coupe DX | none     | default  | Order now, available as soon as tomorrow |
      | 2012 | Honda | Civic | Coupe DX | none     | default  | Order now, available in 2 days           |

  @dt
  @at
  @web
  @12192
  @regression
  @appointmentRegression
  Scenario Outline: HYBRIS_ORDERS_BOPIS_Validating appointmentsection on the checkout_52 (ALM #12192)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add "in stock" item of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "<Checkout>"
    Then I verify "install without appointment" option is enabled on the Checkout page
    And  I verify "install with appointment" option is enabled on the Checkout page
    When I select install with appointment
    Then I verify appointment dates and appointment times sections are displayed
    And  I verify list of available appointment dates
    When I select first available appointment date
    Then I verify appointment timeslots are displayed
    And  I verify appointment dates start on expected date "Available today"
    And  I verify the correct month and year are displayed in the Appointment Date header
    And  I verify the dates falling in different months are divided by a tab displaying month and year
    And  I verify unavailable time slots are disabled and available time slots are enabled
    And  I verify the time list header date is displayed and correct
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Checkout |
      | 2012 | Honda | Civic | Coupe DX | none     | default  |

  @dt
  @at
  @web
  @12216
  @12221
  @12223
  @12225
  @12229
  @regression
  @appointmentRegression
  Scenario Outline: HYBRIS_ORDERS_BOPIS_Verify the appointment details in checkout page (ALM #12216, #12221, #12223, #12225, #12229)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify "install without appointment" option is enabled on the Checkout page
    And  I verify "install with appointment" option is enabled on the Checkout page
    And  I verify the "install with appointment" option is selected
    And  I should see install without appointment tooltip
    When I select install without appointment
    Then I verify "install with appointment" option is enabled on the Checkout page
    And  I verify the Continue to Customer Details button is "disabled"
    And  I verify default install without appointment reason selected
    And  I verify the reasons listed in the reason dropdown list
    When I select the checkout without install reason "Not sure of my availability"
    Then I verify the reserve appointment message displayed for "Not sure of my availability"
    When I select the checkout without install reason "Make an appointment at a later time"
    Then I verify the reserve appointment message displayed for "Make an appointment at a later time"
    When I select the checkout without install reason "These items are for multiple vehicles"
    Then I verify the reserve appointment message displayed for "These items are for multiple vehicles"
    When I click next step for customer information
    And  I select edit link for Appointment Details
    Then I verify the "install with appointment" option is selected
    And  I verify "install without appointment" option is enabled on the Checkout page
    When I select install with appointment
    Then I verify appointment dates and appointment times sections are displayed
    And  I verify the "CONTINUE TO CUSTOMER DETAILS" button is "disabled"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Checkout |
      | 2012 | Honda | Civic | Coupe DX | none     | none     |

  @dt
  @at
  @web
  @14243
  @regression
  @appointmentRegression
  Scenario Outline: HYBRIS_ORDERS_BOPIS_Verify the appointment details in checkout page_standard (ALM #14243)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I extract the "Instant Savings" on the cart page
    When I extract the "sales tax" on the cart page
    When I schedule an appointment for my current store
    And  I select checkout with appointment
    And  I click on the "Continue to Checkout" button
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify date and time on the customer details appointment page
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    And  I should see my previously selected store, date and time, in the appointment details section
    When I store the order number
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Customer            |
      | 2012 | Honda     | Civic    | Coupe DX | none     | DEFAULT_CUSTOMER_AZ |
      | 2010 | Chevrolet | Corvette | Base     | none     | DEFAULT_CUSTOMER_AZ |

  @dt
  @web
  @12212
  @regression
  @appointmentRegression
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_Cart Checkout Validating when appointments are not available (ALM #12212)
    When I change to the store with url "<StoreUrl>"
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify WalkIns Welcome message displays
    And  I verify View Times did not display on any unavailable appointment dates
    And  I verify store is closed on Sunday

    Examples:
      | StoreUrl                | Year | Make  | Model | Trim     | Assembly | Checkout |
      | /store/or/tigard/s/1173 | 2012 | Honda | Civic | Coupe DX | none     | none     |

  @at
  @dt
  @web
  @9987
  @15458
  @regression
  @appointmentRegression
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_ScheduleAppointmentWithServicesMenuOption and User_Set_Appointment_Range_8AM_And_545PM_DT_TC01 (ALM#15458,9987)
    When I open the "APPOINTMENTS" navigation link
    Then I verify default store on the customer details appointment page
    When I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    Then I verify selected service option(s): "<ServiceOptions>" is displayed on Service Appointment page
    And  I verify "APPOINTMENT DETAILS" section is displayed and active
    And  I verify the appointment time range
    When I select first available appointment date
    And  I extract date and time for validation
    Then I verify message bar with appointment day date and time
    When I click continue for appointment customer details page
    Then I verify date and time on the customer details appointment page
    When I select "Schedule Appointment" after entering customer information for "DEFAULT_CUSTOMER_AZ"
    Then I should see an appointment confirmation message for "DEFAULT_CUSTOMER_AZ" with service options: "<ServiceOptions>"
    And  I should see my previously selected store, date and time, in the appointment details section

    Examples:
      | ServiceOptions                |
      | New Tires/Wheels Consultation |
      | Tire Rotation and Balance     |
      | Tire Balancing                |
      | Tire Inspection               |
      | Flat Repair                   |
      | TPMS Service                  |
      | Winter Tire Change            |

  @at
  @dt
  @web
  @9987
  @15544
  @mobile
  @regression
  @appointmentRegression
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_ScheduleAppointmentWithServicesfromFooter and User_Set_Appointment_Range_8AM_And_545PM_DT_TC01(ALM#15544,9987)
    When I select "footer" "Services"
    And  I click on the "SCHEDULE AN APPOINTMENT" link
    Then I verify default store on the customer details appointment page
    When I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    Then I verify selected service option(s): "<ServiceOptions>" is displayed on Service Appointment page
    And  I verify "APPOINTMENT DETAILS" section is displayed and active
    And  I verify the appointment time range
    When I select first available appointment date
    And  I extract date and time for validation
    Then I verify message bar with appointment day date and time
    When I click continue for appointment customer details page
    Then I verify date and time on the customer details appointment page
    When I select "Schedule Appointment" after entering customer information for "DEFAULT_CUSTOMER_AZ"
    Then I should see an appointment confirmation message for "DEFAULT_CUSTOMER_AZ" with service options: "<ServiceOptions>"
    And  I should see my previously selected store, date and time, in the appointment details section

    Examples:
      | ServiceOptions                |
      | New Tires/Wheels Consultation |
      | Tire Rotation and Balance     |
      | Tire Balancing                |
      | Tire Inspection               |
      | Flat Repair                   |
      | TPMS Service                  |
      | Winter Tire Change            |

  @at
  @dt
  @web
  @15543
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_ScheduleAppointmentWithServicesfromMyStore (ALM # 15543)
  """ This fails in IE due to known issue in populating fields """
    When I click on "My Store" title
    And  I click on the "SCHEDULE APPOINTMENT" link
    Then I verify default store on the customer details appointment page
    When I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    Then I verify selected service option(s): "<ServiceOptions>" is displayed on Service Appointment page
    And  I verify "APPOINTMENT DETAILS" section is displayed and active
    When I select first available appointment date
    And  I extract date and time for validation
    Then I verify message bar with appointment day date and time
    When I click continue for appointment customer details page
    Then I verify date and time on the customer details appointment page
    When I select "Schedule Appointment" after entering customer information for "DEFAULT_CUSTOMER_AZ"
    Then I should see an appointment confirmation message for "DEFAULT_CUSTOMER_AZ" with service options: "<ServiceOptions>"
    And  I should see my previously selected store, date and time, in the appointment details section
    When I click on the "Appointments" link
    Then I verify default store on the customer details appointment page

    Examples:
      | ServiceOptions                |
      | New Tires/Wheels Consultation |
      | Tire Rotation and Balance     |
      | Tire Balancing                |
      | Tire Inspection               |
      | Flat Repair                   |
      | TPMS Service                  |
      | Winter Tire Change            |

  @at
  @dt
  @web
  @15490
  Scenario Outline: HYBRIS_APPOINTMENTS_Validating dynamic peak time and 1st available appointment time for Holiday Hours (ALM #15490)
  """Test will only work in the QA environments which has Full and Partial Holidays set for stores """
    When I do a free text search for "<Item Code>"
    And  I select "<Product Name>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<Product Name>"
    When I add item to my cart and "View shopping Cart"
    Then I verify "ProductName" "<Product Name>" is in the cart
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "with appointment"
    Then I verify "install with appointment" option is enabled on the Checkout page
    And  I verify an appointment for a checkout with item(s) can be created for 10 business days later
    And  I verify time slot list is scrollable
    When I scroll time slot list to reach "<Holiday Type>" Holiday
    Then I verify graph showing store schedule is "<Display Expectation>"
    And  I verify the 'First Available Appointment Time' message is "<Display Expectation>"

    Examples:
      | Item Code | Product Name       | Holiday Type | Display Expectation |
      | 29935     | Silver Edition III | Fullday      | not displayed       |
      | 29935     | Silver Edition III | Partial      | displayed           |

  @dt
  @at
  @web
  @15816
  @15818
  @15817
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_Link _Validation when there is item in the cart (ALM #15816, 15818, 15817)
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I verify "ProductName" "<ProductName>" is in the cart
    And  I verify the required fees and add-ons sections are expanded
    When I open the "APPOINTMENTS" navigation link
    Then I verify the window with header "Action needed" is displayed
    And  I verify "CHECKOUT WITH APPOINTMENT" option is displayed on appointments 'Action Needed' modal
    And  I verify "APPOINTMENT ONLY" option is displayed on appointments 'Action Needed' modal
    When I select "CHECKOUT WITH APPOINTMENT" button
    Then I am brought to the page with path "<Expected Checkout Path>"
    And  I am brought to the page with header "Checkout"
    When I navigate back to previous page
    And  I open the "APPOINTMENTS" navigation link
    Then I verify the window with header "Action needed" is displayed
    When I select "APPOINTMENT ONLY" button
    Then I am brought to the page with path "<Expected Service Appointment Path>"
    And  I am brought to the page with header "Service Appointment"

    Examples:
      | ProductName        | ItemCode | Expected Checkout Path    | Expected Service Appointment Path |
      | Silver Edition III | 29935    | checkout/appointment-info | /schedule-appointment             |

  @at
  @dt
  @web
  @15958
  @nonExtendedAssortmentOrders
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_EXTENDEDASSORTMENT_Validate checkout flow when I add tires from secondary suppliers having Qty greater than 8 regular fitment (ALM #15958)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCodeB>" of type "none" to my cart and "Continue Shopping"
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I update quantity for item "<ItemCodeA>" to "<UpdatedQuantityA>"
    Then I verify quantity for "<ItemCodeB>" is set to "<UpdatedQuantityA>" in the "cart"
    When I update quantity for item "<ItemCodeB>" to "<UpdatedQuantityB>"
    Then I verify quantity for "<ItemCodeB>" is set to "<UpdatedQuantityB>" in the "cart"
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I verify "install with appointment" option is enabled on the Checkout page
    And  I verify the Continue to Customer Details button is "disabled"
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    When I select first available appointment date
    And  I click next step for customer information
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the order total on order confirmation page matches with "cart" order total
    And  I verify the sales tax on order confirmation page matches with "cart" sales tax for "<Customer>"
    And  I verify the "<InventoryMessage>" displayed for "<ItemCodeA>" on "Order Confirmation" page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCodeB>" on "Order Confirmation" page

    Examples:
      | ZipCode | Year | Make  | Model | Trim               | Assembly | ItemCodeA | ItemCodeB | UpdatedQuantityA | UpdatedQuantityB | Customer                    | Credit Card | InventoryMessage                         |
      | 80601   | 2014 | Honda | Civic | Coupe LX Automatic | none     | 30192     | 31820     | 4                | 5                | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow |

  @at
  @dt
  @web
  @15972
  @nonExtendedAssortmentOrders
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_EXTENDEDASSORTMENT_Validate checkout flow when I add tires sourced from suppliers with 3 brands are added to cart for regular fitment (ALM #15972)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    And  I add item "<ItemCodeB>" of type "none" to my cart and "Continue Shopping"
    And  I add item "<ItemCodeC>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I should see product "<ItemCodeC>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I update quantity for item "<ItemCodeA>" to "<UpdatedQuantity>"
    And  I update quantity for item "<ItemCodeB>" to "<UpdatedQuantity>"
    And  I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I verify "install with appointment" option is enabled on the Checkout page
    And  I verify the Continue to Customer Details button is "disabled"
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    When I select first available appointment date
    And  I click next step for customer information
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the order total on order confirmation page matches with "cart" order total
    And  I verify the sales tax on order confirmation page matches with "cart" sales tax for "<Customer>"
    And  I verify the "<InventoryMessage>" displayed for "<ItemCodeA>" on "Order Confirmation" page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCodeB>" on "Order Confirmation" page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCodeC>" on "Order Confirmation" page

    Examples:
      | ZipCode | Year | Make  | Model | Trim     | Assembly | ItemCodeA | ItemCodeB | ItemCodeC | UpdatedQuantity | Customer                    | Credit Card | InventoryMessage                         |
      | 80601   | 2012 | Honda | Civic | Coupe DX | None     | 26899     | 29935     | 25458     | 2               | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow |

  @at
  @dt
  @web
  @15980
  @nonExtendedAssortmentOrders
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tire available in secondary supplier and wheels not avaialble in my store added with regular fitment (ALM #15980)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I verify "install with appointment" option is enabled on the Checkout page
    And  I verify the Continue to Customer Details button is "disabled"
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    When I select first available appointment date
    And  I click next step for customer information
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the order total on order confirmation page matches with "cart" order total
    And  I verify the sales tax on order confirmation page matches with "cart" sales tax for "<Customer>"
    And  I verify the "<InventoryMessageA>" displayed for "<ItemCodeA>" on "Order Confirmation" page
    And  I verify the "<InventoryMessageB>" displayed for "<ItemCodeB>" on "Order Confirmation" page

    Examples:
      | ZipCode | Year | Make  | Model | Trim               | Assembly | ItemCodeA | ItemCodeB | Customer                    | Credit Card | InventoryMessageA                        | InventoryMessageB                  |
      | 80601   | 2014 | Honda | Civic | Coupe LX Automatic | none     | 30192     | 78001     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow | Order now, available in 3 - 5 days |

  @at
  @dt
  @web
  @15981
  @nonExtendedAssortmentOrders
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tire available in secondary supplier and no fitment is selected (ALM #15981)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I verify "install with appointment" option is enabled on the Checkout page
    And  I verify the Continue to Customer Details button is "disabled"
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    When I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    And  I verify the order total on order confirmation page matches with "cart" order total
    And  I verify the sales tax on order confirmation page matches with "cart" sales tax for "<Customer>"
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "Order Confirmation" page

    Examples:
      | ZipCode | ItemCode | Customer                    | Customer                    | InventoryMessage                         |
      | 80601   | 17595    | DEFAULT_CUSTOMER_BOPIS_VISA | DEFAULT_CUSTOMER_BOPIS_VISA | Order now, available as soon as tomorrow |

  @at
  @dt
  @web
  @15982
  @nonExtendedAssortmentOrders
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tire available in My Store and Nearby Stores (ALM #15982)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I verify "install with appointment" option is enabled on the Checkout page
    And  I verify the Continue to Customer Details button is "disabled"
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    When I select first available appointment date
    And  I click next step for customer information
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the order total on order confirmation page matches with "cart" order total
    And  I verify the sales tax on order confirmation page matches with "cart" sales tax for "<Customer>"
    And  I verify the "<InventoryMessage>" displayed for "<ItemCode>" on "Order Confirmation" page

    Examples:
      | ZipCode | Year | Make  | Model | Trim               | Assembly | ItemCode | Customer                    | Credit Card | InventoryMessage                   |
      | 80601   | 2014 | Honda | Civic | Coupe LX Automatic | none     | 25454    | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available in 3 - 5 days |
      | 80601   | 2014 | Honda | Civic | Coupe LX Automatic | none     | 34362    | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Available today                    |

  @at
  @dt
  @web
  @15961
  @nonExtendedAssortmentOrders
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tire available in secondary supplier and wheels not avaialble in my store added with Staggered fitment (ALM #15961)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment
    When I add item "<ItemCodeA>" of type "sets" to my cart and "Continue Shopping"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item "<ItemCodeC>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I should see product "<ItemCodeC>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I verify "install with appointment" option is enabled on the Checkout page
    And  I verify the Continue to Customer Details button is "disabled"
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    When I select first available appointment date
    And  I click next step for customer information
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the order total on order confirmation page matches with "cart" order total
    And  I verify the sales tax on order confirmation page matches with "cart" sales tax for "<Customer>"
    And  I verify the "<InventoryMessage>" displayed for "<ItemCodeA>" on "Order Confirmation" page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCodeB>" on "Order Confirmation" page
    And  I verify the "<InventoryMessageC>" displayed for "<ItemCodeC>" on "Order Confirmation" page

    Examples:
      | ZipCode | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | ItemCodeC | Customer                    | Credit Card | InventoryMessage                         | InventoryMessageC                  |
      | 80601   | 2010 | Chevrolet | Corvette | Base | none     | 30018     | 30019     | 74964     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow | Order now, available in 3 - 5 days |

  @at
  @dt
  @web
  @15983
  @nonExtendedAssortmentOrders
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tires available in secondary supplier added with quantity great than 8 for Staggered fitment (ALM #15983)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment
    When I add item "<ItemCodeA>" of type "sets" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I update quantity for item "<ItemCodeA>" to "<QuantityA>"
    And  I update quantity for item "<ItemCodeB>" to "<QuantityB>"
    Then I verify quantity for "<ItemCodeA>" is set to "<QuantityA>" in the "cart"
    And  I verify quantity for "<ItemCodeB>" is set to "<QuantityB>" in the "cart"
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I verify "install with appointment" option is enabled on the Checkout page
    And  I verify the Continue to Customer Details button is "disabled"
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    When I select first available appointment date
    And  I click next step for customer information
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the order total on order confirmation page matches with "cart" order total
    And  I verify the sales tax on order confirmation page matches with "cart" sales tax for "<Customer>"
    And  I verify the "<InventoryMessage>" displayed for "<ItemCodeA>" on "Order Confirmation" page
    And  I verify the "<InventoryMessage>" displayed for "<ItemCodeB>" on "Order Confirmation" page

    Examples:
      | ZipCode | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | QuantityA | QuantityB | Customer                    | Credit Card | InventoryMessage                         |
      | 80601   | 2010 | Chevrolet | Corvette | Base | none     | 30018     | 30019     | 5         | 4         | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Order now, available as soon as tomorrow |

  @dt
  @web
  @sendWebOrderNumberToExcel
  @sendRopisWebOrderNumberToExcel
  @sendRopisWebOrderNumberToExcelNonStaggered
  Scenario Outline: Ropis Appointment - Send Web Order Number to Excel - Non Staggered Fitment
  """ Will fail if ROPIS/BOPIS option is not turned on. Will fail if there are no available appointments at the store. """
    When I change to the integrated test store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I schedule an appointment for my current store
    And  I select checkout with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify date and time on the customer details appointment page
    When I enter customer information for "<Customer>"
    And  I enter "valid" customer phone number: "random"
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I store the order number
    And  I save the order number to the "end" of the "<File Tab>" tab of the "<Excel File>" excel file

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Customer            | Excel File                      | File Tab |
      | 2012 | Honda | Civic | Coupe DX | none     | DEFAULT_CUSTOMER_AZ | WEBORDERS_LEGACY_STOREPOS_EXCEL | Ropis    |

  @dt
  @web
  @sendWebOrderNumberToExcel
  @sendRopisWebOrderNumberToExcel
  @sendRopisWebOrderNumberToExcelStaggered
  Scenario Outline: Ropis Appointment - Send Web Order Number to Excel - Staggered Fitment
  """ Will fail if ROPIS/BOPIS option is not turned on. Will fail if there are no available appointments at the store.
  Revisit the file, column, and tab this will be sent to """
    When I change to the integrated test store
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I schedule an appointment for my current store
    And  I select checkout with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify date and time on the customer details appointment page
    When I enter customer information for "<Customer>"
    And  I enter "valid" customer phone number: "random"
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I store the order number
    And  I save the order number to the "end" of the "<File Tab>" tab of the "<Excel File>" excel file

    Examples:
      | Year | Make      | Model    | Trim | Assembly | Customer            | Excel File                      | File Tab |
      | 2010 | Chevrolet | Corvette | Base | none     | DEFAULT_CUSTOMER_AZ | WEBORDERS_LEGACY_STOREPOS_EXCEL | Ropis    |

  @dt
  @at
  @web
  @9784
  Scenario Outline:  When user edits the appointment selected, the previous dates before the date of selection are not appearing in the calendar (ALM#9784)
  """ Fails in ie due to known ie resolution issue """
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I verify "ProductName" "<ProductName>" is in the cart
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify "install without appointment" option is enabled on the Checkout page
    And  I verify "install with appointment" option is enabled on the Checkout page
    When I select install with appointment
    Then I verify list of available appointment dates
    When I save all the available appointment dates
    And  I select appointment date after "3" days
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify date and time on the customer details appointment page
    When I click Edit Appointment link
    Then I verify Previous dates are displayed on the appointment page

    Examples:
      | ProductName        | ItemCode | Checkout |
      | Silver Edition III | 29935    | default  |

  @dt
  @web
  @HolidayHours
  @FullDayHoliday
  @FullDayHolidayLocalOutage
  @FullDayHolidayCompanyFunction
  Scenario Outline:Verify Full day holiday from Services Header
    When I click on "APPOINTMENTS" header link
    And  I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    Then I verify the date "<HolidayDate>" is displayed on the appointment page
    When I select "<Month>" and day "<Date>" to view its appointment timings
    Then I should see the "<Month>" and "<Date>" on the appointment header
    And  I should see the store holiday comment "<HolidayComment>" is displayed
    And  I should see the store holiday message for "Full Day Holiday" is displayed as "<StoreMessage>"
    And  I verify the next store open timings for "Full Day Holiday" as "<OpenTimings>" displayed

    Examples:
      | HolidayDate      | Month    | Date | ServiceOptions                | HolidayComment | StoreMessage                                                                                             | OpenTimings                         |
      | Fri 9 Festival   | November | 9    | New Tires/Wheels Consultation | Festival       | In observance of a Holiday, the store remains closed and will reopen on November 12, Saturday at 5:00 PM | on November 12, Saturday at 5:00 PM |
      | Sat 10 Labor Day | November | 10   | New Tires/Wheels Consultation | Labor Day      | In observance of a Holiday, the store remains closed and will reopen on November 12, Sunday at 5:00 PM   | on November 12, Sunday at 5:00 PM   |

  @dt
  @web
  @HolidayHours
  @PartialDayCompanyFunction
  @PartialDayLocalOutage
  @PartialDayHoliday
  Scenario Outline:Verify Partial Day CompanyFunction from Services Header
    When I click on "APPOINTMENTS" header link
    And  I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select "<Month>" and day "<Date>" to view its appointment timings
    Then I should see the "<Month>" and "<Date>" on the appointment header
    And  I should see the store holiday message for "<HolidayType>" is displayed as "<StoreMessage>"
    And  I verify the next store open timings for "Partial Day Holiday" as "<OpenTimings>" displayed

    Examples:
      | Month    | Date | ServiceOptions                | HolidayType              | StoreMessage                                                                                                       | OpenTimings                          |
      | November | 12   | New Tires/Wheels Consultation | Partial Day Local Outage | In observance of a Local Outage, the store remains closed and will reopen on November 12, Tuesday at 5:00 PM       | on November 12, Tuesday at 5:00 PM   |
      | November | 13   | New Tires/Wheels Consultation | Partial Day Holiday      | In observance of a Company Function, the store remains closed and will reopen on November 13, Wednesday at 4:00 PM | on November 13, Wednesday at 4:00 PM |
      | November | 14   | New Tires/Wheels Consultation | Partial Day Holiday      | In observance of a Company Function, the store remains closed and will reopen on November 15, Thursday at 8:00 AM  | on November 15, Thursday at 8:00 AM  |

  @dt
  @web
  @HolidayHours
  @SundayHolidayHeader
  Scenario Outline:Verify Sunday Holiday from Services Header
    When I click on "APPOINTMENTS" header link
    And  I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select "<Month>" and day "<Date>" to view its appointment timings
    Then I should see the "<Month>" and "<Date>" on the appointment header
    And  I should see the store holiday message for "<HolidayType>" is displayed as "<StoreMessage>"

    Examples:
      | Month    | Date | ServiceOptions                | HolidayType    | StoreMessage  |
      | November | 11   | New Tires/Wheels Consultation | Sunday Holiday | Sunday Closed |

  @dt
  @web
  @HolidayHours
  @SundayHolidayFooter
  Scenario Outline:Verify Sunday Holiday from Services Footer
    When I select "footer" "Services"
    And  I click on the "SCHEDULE AN APPOINTMENT" link
    And  I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select "<Month>" and day "<Date>" to view its appointment timings
    Then I should see the "<Month>" and "<Date>" on the appointment header
    And  I should see the store holiday message for "<HolidayType>" is displayed as "<StoreMessage>"

    Examples:
      | Month    | Date | ServiceOptions                | HolidayType    | StoreMessage  |
      | November | 11   | New Tires/Wheels Consultation | Sunday Holiday | Sunday Closed |

  @dt
  @web
  @HolidayHours
  @FullDayHolidayGeneralFooter
  @FullDayHolidayLocalOutageFooter
  @FullDayHolidayCompanyFunctionFooter
  Scenario Outline:Verify Full day holiday from Services Footer
    When I select "footer" "Services"
    And  I click on the "SCHEDULE AN APPOINTMENT" link
    And  I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    Then I verify the date "<HolidayDate>" is displayed on the appointment page
    When I select "<Month>" and day "<Date>" to view its appointment timings
    Then I should see the "<Month>" and "<Date>" on the appointment header
    And  I should see the store holiday comment "<HolidayComment>" is displayed
    And  I should see the store holiday message for "Full Day Holiday" is displayed as "<StoreMessage>"
    And  I verify the next store open timings for "Full Day Holiday" as "<OpenTimings>" displayed

    Examples:
      | HolidayDate      | Month    | Date | ServiceOptions                | HolidayComment | StoreMessage                                                                                             | OpenTimings                         |
      | Fri 9 Festival   | November | 9    | New Tires/Wheels Consultation | Festival       | In observance of a Holiday, the store remains closed and will reopen on November 12, Saturday at 5:00 PM | on November 12, Saturday at 5:00 PM |
      | Sat 10 Labor Day | November | 10   | New Tires/Wheels Consultation | Labor Day      | In observance of a Holiday, the store remains closed and will reopen on November 12, Sunday at 5:00 PM   | on November 12, Sunday at 5:00 PM   |

  @dt
  @web
  @HolidayHours
  @PartialDayCompanyFunctionFooter
  @PartialDayLocalOutageFooter
  @PartialDayHolidayFooter
  Scenario Outline:Verify Partial Day holidays from Services Footer
    When I select "footer" "Services"
    And  I click on the "SCHEDULE AN APPOINTMENT" link
    And  I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select "<Month>" and day "<Date>" to view its appointment timings
    Then I should see the "<Month>" and "<Date>" on the appointment header
    And  I should see the store holiday message for "<HolidayType>" is displayed as "<StoreMessage>"
    And  I verify the next store open timings for "Partial Day Holiday" as "<OpenTimings>" displayed

    Examples:
      | Month    | Date | ServiceOptions                | HolidayType              | StoreMessage                                                                                                       | OpenTimings                          |
      | November | 12   | New Tires/Wheels Consultation | Partial Day Local Outage | In observance of a Local Outage, the store remains closed and will reopen on November 12, Tuesday at 5:00 PM       | on November 12, Tuesday at 5:00 PM   |
      | November | 13   | New Tires/Wheels Consultation | Partial Day Holiday      | In observance of a Company Function, the store remains closed and will reopen on November 13, Wednesday at 4:00 PM | on November 13, Wednesday at 4:00 PM |
      | November | 14   | New Tires/Wheels Consultation | Partial Day Holiday      | In observance of a Company Function, the store remains closed and will reopen on November 15, Thursday at 8:00 AM  | on November 15, Thursday at 8:00 AM  |

  @dt
  @web
  @HolidayHours
  @FullDayHolidaycheckout
  @FullDayHolidayLocalOutagecheckout
  @FullDayHolidayCompanyFunctioncheckout
  Scenario Outline:Verify FullDay Holiday from Checkout appointment flow
    When I open the "TIRES" navigation link
    And  I click the "Tire Type" View All link in the header
    And  I select "all-terrain tires" from the Product Brand page
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "with appointment"
    Then I verify the date "<HolidayDate>" is displayed on the appointment page
    When I select "<Month>" and day "<Date>" to view its appointment timings
    Then I should see the "<Month>" and "<Date>" on the appointment header
    And  I should see the store holiday comment "<HolidayComment>" is displayed
    And  I should see the store holiday message for "Full Day Holiday" is displayed as "<StoreMessage>"
    And  I verify the next store open timings for "Full Day Holiday" as "<OpenTimings>" displayed

    Examples:
      | HolidayDate      | Month    | Date | HolidayComment | StoreMessage                                                                                             | OpenTimings                         |
      | Fri 9 Festival   | November | 9    | Festival       | In observance of a Holiday, the store remains closed and will reopen on November 12, Saturday at 5:00 PM | on November 12, Saturday at 5:00 PM |
      | Sat 10 Labor Day | November | 10   | Labor Day      | In observance of a Holiday, the store remains closed and will reopen on November 12, Sunday at 5:00 PM   | on November 12, Sunday at 5:00 PM   |

  @dt
  @web
  @HolidayHours
  @PartialDayHolidayCheckout
  @PartialDayLocalOutagecheckout
  @PartialDayCompanyFunctioncheckout
  Scenario Outline:Verify Partial Day Company Function for Checkout appointment flow
    When I open the "TIRES" navigation link
    And  I click the "Tire Type" View All link in the header
    And  I select "all-terrain tires" from the Product Brand page
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "with appointment"
    And  I select "<Month>" and day "<Date>" to view its appointment timings
    Then I should see the "<Month>" and "<Date>" on the appointment header
    And  I should see the store holiday message for "<HolidayType>" is displayed as "<StoreMessage>"
    And  I verify the next store open timings for "<HolidayType>" as "<OpenTimings>" displayed

    Examples:
      | Month    | Date | HolidayType              | StoreMessage                                                                                                       | OpenTimings                          |
      | November | 12   | Partial Day Local Outage | In observance of a Local Outage, the store remains closed and will reopen on November 12, Tuesday at 5:00 PM       | on November 12, Tuesday at 5:00 PM   |
      | November | 13   | Partial Day Holiday      | In observance of a Company Function, the store remains closed and will reopen on November 13, Wednesday at 4:00 PM | on November 13, Wednesday at 4:00 PM |
      | November | 14   | Partial Day Holiday      | In observance of a Company Function, the store remains closed and will reopen on November 15, Thursday at 8:00 AM  | on November 15, Thursday at 8:00 AM  |

  @at
  @dt
  @web
  @core
  @21269
  @regression
  @appointmentRegression
  @coreScenarioAppointments
  @coreScenarioAppointmentsWithAppointmentBopis
  Scenario Outline:  Install w/ Appointments via PLP with BOPIS - staggered and non-staggered fitment (ALM#21269)
    When I open the "TIRES" navigation link
    And  I click the "TIRES BY VEHICLE" menu option
    And  I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    Then I should see the fitment panel with vehicle "<VehicleType>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item to my cart and "View shopping Cart"
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify date and time on the customer details appointment page
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I should see reservation confirmation message with expected product name and item code
    And  I verify date and time on the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |

    Examples:
      | VehicleType             | Customer                          | Credit Card      |
      | VEHICLE_NON_STAGGERED_1 | DEFAULT_CUSTOMER_BOPIS_VISA       | Visa Bopis       |
      | VEHICLE_STAGGERED_1     | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis |

  @dt
  @at
  @web
  @core
  @21268
  @regression
  @appointmentRegression
  @coreScenarioAppointments
  @coreScenarioAppointmentsWalkinsWelcome
  Scenario Outline: Install w/ Appointments with Walk-ins Welcome Validation (ALM#21268)
    When I change to the store with url "<StoreUrl>"
    And  I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install with appointment
    Then I verify WalkIns Welcome message displays
    And  I verify View Times did not display on any unavailable appointment dates
    And  I verify store is closed on Sunday

    Examples:
      | StoreUrl               | VehicleType             |
      | /store/nm/hobbs/s/1774 | VEHICLE_NON_STAGGERED_1 |
      | /store/nm/hobbs/s/1774 | VEHICLE_STAGGERED_1     |

  @dt
  @at
  @bvt
  @web
  @core
  @21267
  @bvtOrders
  @ordersRegression
  @appointmentRegression
  @coreScenarioAppointments
  @bvtScenarioAppointmentsWithServiceAppointment
  Scenario Outline:  Schedule Service Appointment (ALM#21267)
    When I open the "APPOINTMENTS" navigation link
    And  I select service option(s): "<ServiceOption>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click continue for appointment customer details page
    And  I select "Make Appointment" after entering customer information for "<Customer>"
    Then I should see an appointment confirmation message for "<Customer>" with service options: "<ServiceOption>"
    And  I should see my previously selected store, date and time, in the appointment details section
    And  I store the order number
    And  I read the order xml file
    Then I assert order xml items for "<ServiceOption>"
    And  I assert order xml node values
      | webOrderId           |
      | amountOfSale         |
      | siteNumber           |
      | orderType            |
      | customerType         |
      | webOrderOrigin       |
      | appointmentDate      |
      | appointmentStartTime |
    And  I assert order xml customer node values
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
      | appointmentId        |
      | appointmentEndTime   |

    Examples:
      | ServiceOption             | Customer            |
      | Tire Rotation and Balance | DEFAULT_CUSTOMER_AZ |

  @dt
  @at
  @web
  @core
  @21265
  @regression
  @appointmentRegression
  @coreScenarioAppointments
  @coreScenarioAppointmentsWithAppointmentAddToCartOnPlp
  Scenario Outline:  Reserve Product w/ Appointments via PLP (ALM#21265)
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    Then I should see the fitment panel with vehicle "<VehicleType>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "<Fitment>" fitment
    When I reduce the "Price Range" slider filter
    Then I verify that the price range is correct
    When I clear all the currently active filters on the PLP page
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the subtotal amount on the "cart" page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify date and time on the customer details appointment page
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I should see my previously selected store, date and time, in the appointment details section
    And  I verify the appointment confirmation order placed date is correct
    And  I verify the appointment confirmation store name matches my store on shopping cart
    And  I verify the appointment confirmation sales tax matches sales tax amount on shopping cart
    And  I verify the appointment confirmation order total matches shopping cart order total
    And  I store the order number
    And  I should see reservation confirmation message with expected product name and item code
    And  I verify the "order" created successfully with "product" entry
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | VehicleType             | Customer            | Fitment       |
      | VEHICLE_NON_STAGGERED_1 | default_customer_az | non-staggered |
      | VEHICLE_STAGGERED_1     | default_customer_az | staggered     |

  @dt
  @at
  @web
  @core
  @21266
  @regression
  @appointmentRegression
  @coreScenarioAppointments
  @coreScenarioAppointmentsFreeTextAddToCartFromPDP
  Scenario Outline:  Reserve Product w/o Appointments: Free text Search - Add to cart from PDP (ALM#21266)
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I verify "ProductName" "<ProductName>" is in the cart
    And  I verify the required fees and add-ons sections are expanded
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the subtotal amount on the "cart" page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "default"
    Then I verify "install without appointment" option is enabled on the Checkout page
    When I select the checkout without install reason "<Reason>"
    When I click next step for customer information
    Then I verify the checkout appointment details reason matches checkout without install reason "<Reason>"
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I verify the appointment confirmation order placed date is correct
    And  I verify the appointment confirmation store name matches my store on shopping cart
    And  I verify the appointment confirmation sales tax matches sales tax amount on shopping cart
    And  I verify the appointment confirmation order total matches shopping cart order total
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I store the order number
    And  I verify the "order" created successfully with "<ItemCode>" entry

    Examples:
      | VehicleType             | ProductName       | ItemCode          | Customer            | Reason                      |
      | VEHICLE_NON_STAGGERED_1 | PRODUCT_DEFAULT_1 | PRODUCT_DEFAULT_1 | DEFAULT_CUSTOMER_AZ | Not sure of my availability |

  @dt
  @at
  @web
  @19760
  @canonicalEnhancements
  @newServiceAppointmentShopButtons
  Scenario Outline: HYBRIS New service appointment shop buttons (ALM #19760)
    When I remove all vehicles on session from fitment section
    And  I schedule an appointment for my current store
    And  I select service option(s): "<ServiceOption1>"
    Then I verify element "Shop tires" is "displayed" in appointment page
    And  I verify element "Shop wheels" is "displayed" in appointment page
    When I select service option(s): "<ServiceOption2>"
    Then I verify element "Shop tires" is "displayed" in appointment page
    And  I verify element "Shop wheels" is "displayed" in appointment page
    When I select service option(s): "<ServiceOption1>"
    Then I verify element "Shop tires" is "not displayed" in appointment page
    And  I verify element "Shop wheels" is "not displayed" in appointment page
    When I select service option(s): "<ServiceOption1>"
    And  I click on the "Shop Tires" link
    And  I close popup modal
    And  I click on the "Shop Wheels" link
    And  I do a "default" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "none"
    Then I verify My Vehicle in the header displays as "<Make1> <Model1>"
    When I click on the "Continue" button
    Then I verify the message on the "PLP" banner contains "These wheels fit your vehicle"
    When I schedule an appointment for my current store
    And  I select service option(s): "<ServiceOption1>"
    And  I click on the "Shop Tires" link
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"

    Examples:
      | ServiceOption1                | ServiceOption2            | Year1 | Make1 | Model1 | Trim1    |
      | New Tires/Wheels Consultation | Tire Rotation and Balance | 2012  | Honda | Civic  | Coupe DX |

  @dt
  @at
  @web
  @20893
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_VEHICLE IN SERVICE APPOINTMENT_No vehciles_Guest (ALM#20893)
    When I click on "APPOINTMENTS" header link
    And  I select service option(s): "<ServiceOption>"
    And  I click on the "Set Appointment Details" button
    Then I verify "add new vehicle" is displayed on Service Appointment page
    And  I should see text "You have no recent vehicle searches" present in the page source
    When I click on the "Continue" button
    Then I verify "No vehicle selected" is displayed on Service Appointment page
    And  I verify "add vehicle" is displayed on Service Appointment page
    And  I verify list of available appointment dates
    And  I verify unavailable time slots are disabled
    And  I verify time slot hours increase from top to bottom
    And  I verify time slot list is scrollable
    And  I verify message bar with appointment day date and time
    When I close the Appointment Selected message bar
    Then I verify the Appointment Selected message is not displayed
    When I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify date and time on the customer details appointment page
    And  I verify "No vehicle selected" is displayed on Service Appointment page
    And  I verify "add vehicle" is displayed on Service Appointment page
    When I select "Make Appointment" after entering customer information for "<Customer>"
    Then I should see an appointment confirmation message for "<Customer>" with service options: "<ServiceOption>"

    Examples:
      | ServiceOption  | Customer            |
      | Tire Balancing | DEFAULT_CUSTOMER_AZ |

  @dt
  @at
  @web
  @20895
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_VEHICLE IN SERVICE APPOINTMENT_Add vehicles from Recent search_Guest - vehicle added on first Service Appointment page (ALM#20895)
    When I click on "APPOINTMENTS" header link
    And  I select service option(s): "<ServiceOption>"
    And  I click on the "Set Appointment Details" button
    Then I verify "add new vehicle" is displayed on Service Appointment page
    And  I should see text "You have no recent vehicle searches" present in the page source
    When I click on the "add new vehicle" link on Service Appointment page
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I wait for "1" seconds
    And  I close popup modal
    Then I verify My Vehicle in the header displays as "<Make> <Model>"
    And  I should see "<Year> <Make> <Model> <Trim>" details on the "Service Appointment" page
    When I click on the "Continue" button
    Then I verify list of available appointment dates
    And  I verify My Vehicle in the header displays as "<Make> <Model>"
    And  I verify unavailable time slots are disabled
    And  I verify time slot hours increase from top to bottom
    And  I verify time slot list is scrollable
    And  I verify message bar with appointment day date and time
    When I close the Appointment Selected message bar
    Then I verify the Appointment Selected message is not displayed
    When I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify My Vehicle in the header displays as "<Make> <Model>"
    And  I should see "<Year> <Make> <Model> <Trim>" details on the "Service Appointment" page
    And  I verify date and time on the customer details appointment page
    And  I select "Make Appointment" after entering customer information for "<Customer>"
    And  I should see an appointment confirmation message for "<Customer>" with service options: "<ServiceOption>"

    Examples:
      | ServiceOption  | Customer            | Year | Make  | Model | Trim     |
      | Tire Balancing | DEFAULT_CUSTOMER_AZ | 2012 | Honda | Civic | Coupe DX |

  @dt
  @at
  @web
  @20895
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_VEHICLE IN SERVICE APPOINTMENT_Add vehicles from Recent search_Guest - vehicle added on second Service Appointment page (ALM#20895)
    When I click on "APPOINTMENTS" header link
    And  I select service option(s): "<ServiceOption>"
    And  I click on the "Set Appointment Details" button
    Then I verify "add new vehicle" is displayed on Service Appointment page
    And  I should see text "You have no recent vehicle searches" present in the page source
    When I click on the "Continue" button
    Then I verify "No vehicle selected" is displayed on Service Appointment page
    And  I verify "add vehicle" is displayed on Service Appointment page
    When I click on the "add vehicle" link on Service Appointment page
    And  I click on the "add new vehicle" link on Service Appointment page
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I click on the "Continue" button
    Then I verify My Vehicle in the header displays as "<Make> <Model>"
    And  I should see "<Year> <Make> <Model> <Trim>" details on the "Service Appointment" page
    And  I verify list of available appointment dates
    And  I verify My Vehicle in the header displays as "<Make> <Model>"
    And  I verify unavailable time slots are disabled
    And  I verify time slot hours increase from top to bottom
    And  I verify time slot list is scrollable
    And  I verify message bar with appointment day date and time
    When I close the Appointment Selected message bar
    Then I verify the Appointment Selected message is not displayed
    When I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify My Vehicle in the header displays as "<Make> <Model>"
    And  I should see "<Year> <Make> <Model> <Trim>" details on the "Service Appointment" page
    And  I verify date and time on the customer details appointment page
    When I select "Make Appointment" after entering customer information for "<Customer>"
    Then I should see an appointment confirmation message for "<Customer>" with service options: "<ServiceOption>"

    Examples:
      | ServiceOption  | Customer            | Year | Make  | Model | Trim     |
      | Tire Balancing | DEFAULT_CUSTOMER_AZ | 2012 | Honda | Civic | Coupe DX |

  @dt
  @at
  @web
  @20896
  Scenario Outline: HYBRIS_APPOINTMENTS_APPOINTMENTS_VEHICLE IN SERVICE APPOINTMENT_Change vehicles_Guest (ALM#20896)
    When I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "none"
    And  I do a "homepage" vehicle search with details "<Year2>" "<Make2>" "<Model2>" "<Trim2>" "none"
    And  I select "footer" "Services"
    And  I click on the "SCHEDULE AN APPOINTMENT" link
    And  I select service option(s): "<ServiceOption>"
    And  I click on the "Set Appointment Details" button
    Then I verify selected vehicle on Service Appointment page is "<Year2> <Make2> <Model2> <Trim2>"
    And  I verify selected vehicle on Service Appointment page contains edit vehicle link and unselected vehicles do not
    When I select "<Year1> <Make1> <Model1> <Trim1>" on Service Appointment page
    Then I verify selected vehicle on Service Appointment page is "<Year1> <Make1> <Model1> <Trim1>"
    And  I verify selected vehicle on Service Appointment page contains edit vehicle link and unselected vehicles do not
    And  I verify "<Year1> <Make1> <Model1> <Trim1>" vehicle on Service Appointment page has "O.E." tire size badge and associated label
    And  I verify "<Year2> <Make2> <Model2> <Trim2>" vehicle on Service Appointment page has "O.E." tire size badge and associated label
    When I select edit vehicle link
    And  I "expand" "optional tire and wheel size"
    And  I select the "17" fitment box option
    And  I select a fitment option "215/45-17"
    And  I close popup modal
    Then I verify "<Year1> <Make1> <Model1> <Trim1>" vehicle on Service Appointment page has "<offset1>" tire size badge and associated label
    When I select "<Year2> <Make2> <Model2> <Trim2>" on Service Appointment page
    Then I verify selected vehicle on Service Appointment page is "<Year2> <Make2> <Model2> <Trim2>"
    And  I verify selected vehicle on Service Appointment page contains edit vehicle link and unselected vehicles do not
    When I select edit vehicle link
    And  I "expand" "optional tire and wheel size"
    And  I select the "17" fitment box option
    And  I select a fitment option "215/55-17"
    And  I click on the "Continue" button
    Then I verify "<Year2> <Make2> <Model2> <Trim2>" vehicle on Service Appointment page has "<offset2>" tire size badge and associated label
    When I click on the "change vehicle" link
    Then I verify "<Year1> <Make1> <Model1> <Trim1>" vehicle on Service Appointment page has "<offset1>" tire size badge and associated label
    And  I verify "<Year2> <Make2> <Model2> <Trim2>" vehicle on Service Appointment page has "<offset2>" tire size badge and associated label
    And  I verify My Vehicle in the header displays as "<Make2> <Model2>"
    When I click on the "Continue" button
    Then I verify "<Year2> <Make2> <Model2> <Trim2>" vehicle on Service Appointment page has "<offset2>" tire size badge and associated label
    And  I should see "<Year2> <Make2> <Model2> <Trim2>" details on the "Service Appointment" page
    And  I verify list of available appointment dates
    And  I verify My Vehicle in the header displays as "<Make2> <Model2>"
    And  I verify unavailable time slots are disabled
    And  I verify time slot hours increase from top to bottom
    And  I verify time slot list is scrollable
    And  I verify message bar with appointment day date and time
    When I close the Appointment Selected message bar
    Then I verify the Appointment Selected message is not displayed
    When I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify My Vehicle in the header displays as "<Make2> <Model2>"
    And  I should see "<Year2> <Make2> <Model2> <Trim2>" details on the "Service Appointment" page
    And  I verify date and time on the customer details appointment page
    When I select "Make Appointment" after entering customer information for "<Customer>"
    Then I should see an appointment confirmation message for "<Customer>" with service options: "<ServiceOption>"

    Examples:
      | ServiceOption  | Customer            | Year1 | Make1 | Model1 | Trim1    | offset1 | Year2 | Make2  | Model2 | Trim2 | offset2 |
      | Tire Balancing | DEFAULT_CUSTOMER_AZ | 2012  | Honda | Civic  | Coupe DX | +2      | 2016  | Toyota | Camry  | LE    | +1      |