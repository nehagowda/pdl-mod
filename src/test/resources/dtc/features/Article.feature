@article
Feature: Article

  Background:
    Given I change to the default store

  @dt
  @at
  @dtd
  @web
  @6796
  @regression
  @articleRegression
  Scenario Outline: HYBRIS_PRODUCT DETAIL PAGE_WHEELS AND TIRES_FacetValue_PLP_tire (ALM #6796)
    When I do a free text search for "<Search Term>" and hit enter
    Then I verify the PLP header message contains "Results for "<Search Term>""
    And  I verify the "Review Ratings, Speed Rating, Good Better Best" filter section(s) is/are displayed

    Examples:
      | Search Term |
      | tir         |
      | ti          |

  @dt
  @at
  @dtd
  @web
  @6789
  @regression
  @articleRegression
  Scenario: HYBRIS_PRODUCT DETAIL PAGE_WHEELS AND TIRES_Tire_Compare_Upto 3 (ALM #6789)
  """TODO CCL fails in DTD & AT due to defect #9285 - Missing attributes from compare product sections"""
    When I do a free text search for "tir" and hit enter
    Then I verify the PLP header message contains "Results for "tir""
    When I select the first "3" results to compare
    And  I click the compare products Compare button
    Then I am brought to the page with header "Compare products"
    And  I verify expected attributes are displayed for compare page section "Size specifications"
    And  I verify expected attributes are displayed for compare page section "Tread and traction specifications"
    And  I verify expected attributes are displayed for compare page section "Safety and performance specifications"

  @dt
  @at
  @dtd
  @web
  @6788
  @6791
  @regression
  @articleRegression
  Scenario Outline: HYBRIS_PRODUCT DETAIL PAGE_ TIRES_Tire_PDP (ALM #6788, 6791)
    When I do a free text search for "<Item Code>" and hit enter
    Then I verify expected "<Attribute Type>" attributes are displayed for product detail page section "SIZE"
    And  I verify expected "<Attribute Type>" attributes are displayed for product detail page section "<Section>"
    And  I verify expected "<Attribute Type>" attributes are displayed for product detail page section "SAFETY & PERFORMANCE"

    Examples:
      | Item Code | Attribute Type | Section              |
      | 29935     | Tire           | TREAD & TRACTION     |
      | 49240     | Wheel          | STYLE & CONSTRUCTION |

  @dt
  @at
  @dtd
  @web
  @6794
  @regression
  @articleRegression
  Scenario Outline: HYBRIS_PRODUCT DETAIL PAGE_WHEELS AND TIRES_TireSet_PDP_verify all field (ALM #6794)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify expected "Tire" attributes are displayed for product detail page section "SIZE"
    And  I verify expected "Tire" attributes are displayed for product detail page section "TREAD & TRACTION"
    And  I verify expected "Tire" attributes are displayed for product detail page section "SAFETY & PERFORMANCE"

    Examples:
      | Year | Make   | Model | Trim       | Assembly |
      | 2014 | Nissan | 370Z  | Coupe Base | none     |

  @dtd
  @web
  @6856
  @6855
  @regression
  @articleRegression
  Scenario Outline: HYBRIS_PRODUCT_DISPLAY RULES Shipping Block Rule Compare Page With Restricted Products Checkout (ALM #6856, 6855)
  """TODO CCL - data commented out as the products called for in ALM #6855 are unable to be added to cart Using Michelin
    products for time being. Entered data defect #9379
    | YOKOHAMA    | 43727, 44339, 43576 | 43727            | not displayed  |
    TODO: Validate new cannot ship Nitto items message on the PDP page """
    When I do a free text search for "<Search Term>" and hit enter
    And  I select shop all from the Product Brand page
    And  I select item number(s): "<Item Numbers>" from the results list to compare
    And  I click the compare products Compare button
    Then I am brought to the page with header "Compare products"
    And  I verify "Add to cart" is enabled for each product being compared
    And  I verify the "We cannot ship items to locations" message is "<Display Status>" for all compared products
    When I select item number: "<Item To Purchase>" from compare products to view its product details
    Then I verify the "We cannot ship items to locations" message is "<Display Status>" on the product detail page
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "none"
    And  I enter shipping info as "default_customer_can" and continue to next page
    Then I verify the "Shipping Restriction" modal messaging as well as controls are "<Display Status>"

    Examples:
      | Search Term | Item Numbers        | Item To Purchase | Display Status |
      | NITTO       | 17079, 19345, 19350 | 19350            | displayed      |
      | Michelin    | 35613, 17077, 34302 | 34302            | not displayed  |
