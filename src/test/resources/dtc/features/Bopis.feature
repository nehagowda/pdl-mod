@bopis
Feature: Bopis

  Background:
    Given I change to the default store

  @at
  @dt
  @web
  @11666
  @11543
  @12056
  @12006
  @ordersRegression
  @bopisNotSplitPayment
  Scenario Outline: HYBRIS_ORDERS_BOPIS _Placing order with 1 credit card and no recipient information_01_standard/Staggered (ALM #11666, #11543, #12056, #12006)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml customer node values
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
      | appointmentId        |
      | appointmentEndTime   |
    And  I assert order xml payment node values

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Checkout         | Customer                     | Credit Card        |
      | 2012 | Honda     | Civic    | Coupe DX | None     | with appointment | DEFAULT_CUSTOMER_BOPIS_VISA  | Visa Bopis         |
      | 2010 | Chevrolet | Corvette | Base     | None     | with appointment | DEFAULT_CUSTOMER_BOPIS_CC1_2 | CarCareOne_2 Bopis |

  @at
  @dt
  @web
  @11667
  @bopisSplitPayment
  Scenario Outline: HYBRIS_ORDERS_BOPIS _Placing order with 2 credit cards and no recipient information_02_standard/Staggered (ALM#11667)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "<FitmentType>" fitment
    When I add "in stock" item of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "<Checkout>"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer1>"
    And  I enter payment info with "<Credit Card1>" and confirm Checkout Summary as "<Customer1>"
    And  I "expand" "Split credit card payment"
    And  I set the split payment amount to "half" for "secondary" form
    And  I enter payment info with "<Credit Card2>" and confirm Checkout Summary as "<Customer2>"
    And  I save card details
    Then I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    When I place the order for "<Customer1>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer1>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | FitmentType   | Checkout         | Customer1                       | Credit Card1           | Customer2                         | Credit Card2       |
      | 2012 | Honda     | Civic    | Coupe DX | None     | non-staggered | with appointment | DEFAULT_CUSTOMER_BOPIS_CC1      | CarCareOne Bopis       | DEFAULT_CUSTOMER_BOPIS_DISCOVER   | Discover Bopis     |
      | 2012 | Honda     | Civic    | Coupe DX | None     | non-staggered | with appointment | DEFAULT_CUSTOMER_BOPIS_CC1      | CarCareOne Bopis       | DEFAULT_CUSTOMER_BOPIS_CC1_2      | CarCareOne_2 Bopis |
      | 2010 | Chevrolet | Corvette | Base     | None     | staggered     | with appointment | DEFAULT_CUSTOMER_BOPIS_DISCOVER | Discover Bopis         | DEFAULT_CUSTOMER_BOPIS_VISA       | Visa Bopis         |
      | 2010 | Chevrolet | Corvette | Base     | None     | staggered     | with appointment | DEFAULT_CUSTOMER_BOPIS_AMEX     | American Express Bopis | DEFAULT_CUSTOMER_BOPIS_VISA       | Visa Bopis         |

  @at
  @dt
  @web
  @core
  @11668
  @11550
  @12058
  @15840
  @ordersRegression
  @coreScenarioBopis
  @bopisNotSplitPayment
  Scenario Outline: HYBRIS_ORDERS_BOPIS _Placing order with 1 credit card with other recipient information_01_standard/Staggered (ALM #11668,#11550,#12058,#15840)
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "<FitmentType>" fitment
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "<Checkout>"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I "expand" "Someone else will pick up my order"
    And  I enter information for other recipient "<Other Customer>" requesting delivery by "<Recipient Info>" and "<Update>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    Then I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml payment node values
    And  I assert order xml subItems

    Examples:
      | VehicleType               | FitmentType   | Checkout         | Customer                    | Credit Card            | Other Customer      | Recipient Info | Update        |
      | VEHICLE_NON_STAGGERED_1   | non-staggered | with appointment | DEFAULT_CUSTOMER_BOPIS_CC1  | CarCareOne Bopis       | DEFAULT_CUSTOMER_AZ | email          | email updates |
      | VEHICLE_STAGGERED_1       | staggered     | with appointment | DEFAULT_CUSTOMER_BOPIS_AMEX | American Express Bopis | DEFAULT_CUSTOMER_AZ | phone          | text updates  |

  @at
  @dt
  @web
  @11670
  @bopisSplitPayment
  Scenario Outline: HYBRIS_ORDERS_BOPIS _Placing order with 2 credit card with other recipient information_04_standard/staggered (ALM#11670)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "<FitmentType>" fitment
    When I add "in stock" item of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "<Checkout>"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer1>"
    And  I "expand" "Someone else will pick up my order"
    And  I enter information for other recipient "<Other Customer>" requesting delivery by "<Recipient Info>" and "<Update>"
    And  I enter payment info with "<Credit Card1>" and confirm Checkout Summary as "<Customer1>"
    And  I "expand" "Split credit card payment"
    When I set the split payment amount to "half" for "secondary" form
    And  I enter payment info with "<Credit Card2>" and confirm Checkout Summary as "<Customer2>"
    And  I save card details
    Then I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    When I place the order for "<Customer1>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer1>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | FitmentType   | Checkout         | Customer1                         | Credit Card1       | Customer2                       | Credit Card2           | Other Customer      | Recipient Info | Update        |
      | 2012 | Honda     | Civic    | Coupe DX | None     | non-staggered | with appointment | DEFAULT_CUSTOMER_BOPIS_VISA       | Visa Bopis         | DEFAULT_CUSTOMER_BOPIS_CC1      | CarCareOne Bopis       | DEFAULT_CUSTOMER_AZ | email          | email updates |
      | 2012 | Honda     | Civic    | Coupe DX | None     | non-staggered | with appointment | DEFAULT_CUSTOMER_BOPIS_CC1_2      | CarCareOne_2 Bopis | DEFAULT_CUSTOMER_BOPIS_CC1      | CarCareOne Bopis       | DEFAULT_CUSTOMER_AZ | phone          | text updates  |
      | 2010 | Chevrolet | Corvette | Base     | None     | staggered     | with appointment | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis   | DEFAULT_CUSTOMER_BOPIS_DISCOVER | Discover Bopis         | DEFAULT_CUSTOMER_AZ | email          | email updates |
      | 2010 | Chevrolet | Corvette | Base     | None     | staggered     | with appointment | DEFAULT_CUSTOMER_BOPIS_CC1        | CarCareOne Bopis   | DEFAULT_CUSTOMER_BOPIS_AMEX     | American Express Bopis | DEFAULT_CUSTOMER_AZ | phone          | text updates  |

  @at
  @dt
  @web
  @15629
  @mobile
  @ordersRegression
  @bopisNotSplitPayment
  Scenario Outline: HYBRIS_ORDERS_BOPIS_BOPIS Order without appointment when a product is added with vehicle (ALM#15629)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml customer node values
    And  I assert order xml payment node values
    And  I assert order xml subItems

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Customer                    | Credit Card |
      | 2012 | Honda | Civic | Coupe DX | none     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @at
  @dt
  @web
  @11551
  @11877
  @bopisSplitPayment
  Scenario Outline: HYBRIS_ORDERS_BOPIS_Authorization decline using two credit cards, with 1 successful authorization and 1 decline _03 (ALM #11551, #11877)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "with appointment"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Primary Customer>"
    And  I enter payment info with "<Primary Card>" and confirm Checkout Summary as "<Primary Customer>"
    And  I "expand" "Split credit card payment"
    And  I set the split payment amount to "half" for "secondary" form
    And  I enter payment info with "<Secondary Card>" and confirm Checkout Summary as "<Secondary Customer>"
    And  I enter invalid "Security Code" of "000" into "secondary" form
    And  I save card details
    Then I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    When I place the order for "<Primary Customer>"
    Then I verify "was declined by processor" error message is "displayed" on "primary" form
    And  I enter valid "Credit Card Number" into "primary" form for "<Primary Card>"
    And  I enter valid "Security Code" into "primary" form for "<Primary Card>"
    And  I save card details
    And  I enter valid "Credit Card Number" into "secondary" form for "<Secondary Card>"
    And  I enter valid "Security Code" into "secondary" form for "<Secondary Card>"
    And  I save card details
    And  I place the order for "<Primary Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Primary Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Primary Customer            | Primary Card | Secondary Customer                | Secondary Card   |
      | 2012 | Honda     | Civic    | Coupe DX | None     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis   | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis |
      | 2010 | Chevrolet | Corvette | Base     | None     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis   | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis |

  @at
  @dt
  @web
  @11565
  @bopisSplitPayment
  Scenario Outline: HYBRIS_ORDERS_BOPIS_Authorization decline using two credit cards, with 2 declines _05 (ALM#11565)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "with appointment"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Primary Customer>"
    And  I enter payment info with "<Primary Card>" and confirm Checkout Summary as "<Primary Customer>"
    And  I enter invalid "Security Code" of "000" into "primary" form
    And  I "expand" "Split credit card payment"
    And  I set the split payment amount to "half" for "secondary" form
    And  I enter payment info with "<Secondary Card>" and confirm Checkout Summary as "<Secondary Customer>"
    And  I enter invalid "Security Code" of "000" into "secondary" form
    And  I save card details
    Then I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    When I place the order for "<Primary Customer>"
    Then I verify "was declined by processor" error message is "displayed" on "primary" form
    And  I enter valid "Credit Card Number" into "primary" form for "<Primary Card>"
    And  I enter valid "Security Code" into "primary" form for "<Primary Card>"
    And  I save card details
    And  I enter valid "Credit Card Number" into "secondary" form for "<Secondary Card>"
    And  I enter valid "Security Code" into "secondary" form for "<Secondary Card>"
    And  I save card details
    And  I place the order for "<Primary Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Primary Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Primary Customer            | Primary Card | Secondary Customer                | Secondary Card   |
      | 2012 | Honda     | Civic    | Coupe DX | None     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis   | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis |
      | 2010 | Chevrolet | Corvette | Base     | None     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis   | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis |

  @dt
  @web
  @15839
  @defect
  @bopisNotSplitPayment
  Scenario Outline: HYBRIS_ORDERS_BOPIS_Validating recipient fields does not appear when enableRecipient is set as  No or NA in hmc (ALM#15839)
  """ This is failing due to defect 12288"""
    When I change to the store with url "<StoreUrl>"
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "without appointment"
    And  I click next step for customer information
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    Then I verify "Someone else will pick up my order" is "not displayed"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | StoreUrl                    | Year | Make  | Model | Trim     | Assembly | Customer                    | Credit Card |
      | /store/az/scottsdale/s/1009 | 2012 | Honda | Civic | Coupe DX | none     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @at
  @dt
  @web
  @8749
  @bopisNotSplitPayment
  Scenario Outline: HYBRIS_ORDERS_BOPIS _Should not go to confirmation when saving card details after correcting invalid CVV (ALM#8749)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "with appointment"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Primary Customer>"
    And  I enter payment info with "<Primary Card>" and confirm Checkout Summary as "<Primary Customer>"
    And  I enter invalid "Security Code" of "000" into "primary" form
    Then I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    When I place the order for "<Primary Customer>"
    Then I verify "was declined by processor" error message is "displayed" on "primary" form
    When I enter valid "Security Code" into "primary" form for "<Primary Card>"
    And  I enter valid "Credit Card Number" into "primary" form for "<Primary Card>"
    And  I save card details
    And  I place the order for "<Primary Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Primary Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Primary Customer            | Primary Card |
      | 2012 | Honda     | Civic    | Coupe DX | None     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis   |
      | 2010 | Chevrolet | Corvette | Base     | None     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis   |

  @dt
  @web
  @12062
  @bopisSplitPayment
  @core_do_not_run_split_payment_turned_off
  Scenario Outline: HYBRIS_ORDERS_BOPIS_Validating amount field for payment page_48 (ALM#12062)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "with appointment"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Primary Customer>"
    And  I enter payment info with "<Primary Card>" and confirm Checkout Summary as "<Primary Customer>"
    And  I "expand" "Split credit card payment"
    Then I verify amount field in "primary" form equals "order total"
    And  I verify amount field in "secondary" form equals "0.00"
    When I enter payment info with "<Secondary Card>" and confirm Checkout Summary as "<Secondary Customer>"
    And  I save card details
    Then I verify "Amount is invalid" error message is "displayed" on "secondary" form
    When I set the split payment amount to "0.00" for "primary" form
    Then I verify "Amount is invalid" error message is "displayed" on "primary" form
    When I set the split payment amount to "half" for "secondary" form
    Then I verify the two payments equal the order total
    When I set the split payment amount to "10.00" for "primary" form
    Then I verify the two payments equal the order total
    When I save card details
    Then I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    When I place the order for "<Primary Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Primary Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Primary Customer            | Primary Card | Secondary Customer                | Secondary Card   |
      | 2012 | Honda | Civic | Coupe DX | None     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis   | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis |

  @dt
  @web
  @bvt
  @core
  @11870
  @11871
  @11882
  @11883
  @bvtOrders
  @coreScenarioBopis
  @bopisNotSplitPayment
  @bvtScenarioBopisInvalidPaymentDetails
  Scenario Outline: HYBRIS_ORDERS_BOPIS _Error message validation when details entered for the card is wrong_26, when card details are missing_27, when there is address mismatch_21, when there is CVV mismatch_20 (ALM#11882,11883,11871,11870)
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "with appointment"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Primary Customer>"
    And  I enter payment info with "<Primary Card>" and confirm Checkout Summary as "<Primary Customer>"
    Then I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    When I enter invalid "Credit Card Number" of "" into "primary" form
    And  I enter valid "Security Code" into "primary" form for "<Primary Card>"
    And  I enter valid "Exp Date" into "primary" form for "<Primary Customer>"
    And  I enter valid "Name on Card" into "primary" form for "<Primary Customer>"
    And  I enter valid "Address Line 1" into "primary" form for "<Primary Customer>"
    And  I enter valid "Zip Code" into "primary" form for "<Primary Customer>"
    Then I verify "Credit Card Number cannot be empty" error message is "displayed" on "primary" form
    When I enter invalid "Name on Card" of "" into "primary" form
    Then I verify "Name on Card cannot be empty" error message is "displayed" on "primary" form
    When I enter invalid "Security Code" of "" into "primary" form
    Then I verify "Security Code cannot be empty" error message is "displayed" on "primary" form
    When I enter invalid "Address Line 1" of "" into "primary" form
    Then I verify "Address Line 1 cannot be empty" error message is "displayed" on "primary" form
    When I enter invalid "Zip Code" of "" into "primary" form
    Then I verify "Zip Code cannot be empty" error message is "displayed" on "primary" form
    When I enter valid "Credit Card Number" into "primary" form for "<Primary Card>"
    Then I verify "Credit Card Number cannot be empty" error message is "not displayed" on "primary" form
    When I enter valid "Security Code" into "primary" form for "<Primary Card>"
    Then I verify "Security Code cannot be empty" error message is "not displayed" on "primary" form
    When I enter valid "Name on Card" into "primary" form for "<Primary Customer>"
    Then I verify "Name on Card cannot be empty" error message is "not displayed" on "primary" form
    When I enter valid "Address Line 1" into "primary" form for "<Primary Customer>"
    Then I verify "Address Line 1 cannot be empty" error message is "not displayed" on "primary" form
    When I enter valid "Zip Code" into "primary" form for "<Primary Customer>"
    Then I verify "Zip Code cannot be empty" error message is "not displayed" on "primary" form
    When I enter invalid "Credit Card Number" of "1111222233334444" into "primary" form
    And  I place the order for "<Primary Customer>"
    Then I verify "Unable to validate card. Please re-check number and try again." error message is "displayed" on "primary" form
    When I enter invalid "Credit Card Number" of "alpha12345671234" into "primary" form
    And  I enter valid "Security Code" into "primary" form for "<Primary Card>"
    And  I place the order for "<Primary Customer>"
    Then I verify "Unable to validate card. Please re-check number and try again." error message is "displayed" on "primary" form
    When I enter valid "Credit Card Number" into "primary" form for "<Primary Card>"
    And  I enter valid "Security Code" into "primary" form for "<Primary Card>"
    Then I enter past expiration date, place order, and verify "Future date must supersede the current date" error message is displayed on "primary" form
    When I enter valid "Exp Date" into "primary" form for "<Primary Customer>"
    And  I enter invalid "Address Line 1" of "<Invalid Address>" into "primary" form
    And  I enter valid "Zip Code" into "primary" form for "<Primary Customer>"
    And  I place the order for "<Primary Customer>"
    Then I verify "was declined by processor" error message is "displayed" on "primary" form
    When I enter valid "Credit Card Number" into "primary" form for "<Primary Card>"
    And  I enter valid "Security Code" into "primary" form for "<Primary Card>"
    And  I enter valid "Address Line 1" into "primary" form for "<Primary Customer>"
    And  I enter valid "Zip Code" into "primary" form for "<Primary Customer>"
    And  I enter payment info with "<Primary Card>" and confirm Checkout Summary as "<Primary Customer>"
    And  I enter invalid "Security Code" of "000" into "primary" form
    And  I place the order for "<Primary Customer>"
    Then I verify "was declined by processor" error message is "displayed" on "primary" form
    When I enter valid "Credit Card Number" into "primary" form for "<Primary Card>"
    And  I enter valid "Security Code" into "primary" form for "<Primary Card>"
    And  I place the order for "<Primary Customer>"
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Primary Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | VehicleType             | Primary Customer                  | Primary Card     | Invalid Address   |
      | VEHICLE_NON_STAGGERED_1 | DEFAULT_CUSTOMER_BOPIS_VISA       | Visa Bopis       | 2220 w mission ln |
      | VEHICLE_STAGGERED_1     | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis | 2220 w mission ln |

  @dt
  @web
  @19723
  @bopisNotSplitPayment
  Scenario Outline: HYBRIS_ORDERS_BOPIS _Verify Bopis order placed when certificates added and removed with promotion(ALM#19723)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the optional "Certificates" fee for item
    Then I verify that the certificate line item total is "displayed" for "<ItemCode>"
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCode>"
    And  I verify the subtotal price on the cart page
    And  I verify the tax on the cart page
    And  I verify the Total price on the cart summary page
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the optional "Certificates" fee for item
    Then I verify that the certificate unit price is "<Zero Price>" for "<ItemCode>"
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCode>"
    And  I verify the subtotal price on the cart page
    And  I verify the tax on the cart page
    And  I verify the Total price on the cart summary page
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "with appointment"
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    Then I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
          | webOrderId     |
          | amountOfSale   |
          | siteNumber     |
          | orderType      |
          | customerType   |
          | webOrderOrigin |
          | vehicle        |
    And  I assert order xml list values
          | productCode  |
          | discount     |
          | tax          |

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | ItemCode | Customer                    | Credit Card | Zero Price |
      | 2012 | Honda | Civic | Coupe DX | None     | tire          | 34302    | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | 0.00       |