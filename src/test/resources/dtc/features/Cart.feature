@cartPage
Feature: Shopping Cart

  Background:
    Given I change to the default store

  @dt
  @at
  @9619
  @clearCart
  @web
  Scenario Outline: Unable to clear the cart while trying to change the Recent Vehicle to the Current Vehicle (ALM #9619)
  """Step 4 has validation of Cart is empty and displays as "$0.00" but this is not true. item is added and cart
    updated After clicking clear, cart does not clear right away. only after closing the select fitment popup or
    selecting a fitment. This differs from the expected result in Step 8 of the testcase"""
    When I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "Continue Shopping"
    And  I open the My Vehicles popup
    Then I verify that My Vehicles displays "<Year> <Make> <Model>" as the current vehicle
    When I remove my "selected vehicle" vehicle
    And  I do a "my vehicles" vehicle search with details "2015" "Honda" "Accord" "Coupe EX" "none"
    Then I verify the options in the switch vehicle popup
    When I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify My Vehicle in the header displays as "Honda Accord"
    And  I verify the header cart total is "$0.00"

    Examples:
      | Year | Make   | Model | Trim       | Assembly                            | ItemCode |
      | 2010 | Nissan | 370Z  | Coupe Base | F 245 /40 R19 SL - R 275 /35 R19 SL | 25991    |

  @dt
  @at
  @9619
  @clearCart
  @mobile
  Scenario Outline: Unable to clear the cart while trying to change the Recent Vehicle to the Current Vehicle (ALM #9619)
    When I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "Continue Shopping"
    And  I open the My Vehicles popup
    Then I verify that My Vehicles displays "<Year> <Make> <Model>" as the current vehicle
    When I remove my "selected vehicle" vehicle
    And  I open the fitment popup
    And  I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "2015" "Honda" "Accord" "Coupe EX" "none"
    Then I verify the options in the switch vehicle popup
    When I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I verify the header cart item count is "0"
    When I open the My Vehicles popup
    Then I verify that My Vehicles displays "2015 Honda Accord" as the current vehicle

    Examples:
      | Year | Make   | Model | Trim       | Assembly                            | ItemCode |
      | 2010 | Nissan | 370Z  | Coupe Base | F 245 /40 R19 SL - R 275 /35 R19 SL | 25991    |

  @dt
  @web
  @core
  @11010
  @defect
  @coreScenarioCart
  @verifyPricesTaxesFees
  @coreScenarioVerifyTaxesFeesMiniCart
  Scenario Outline: HYBRIS_SHOPPINGCART_Validate Prices and Taxes of the product_DT (ALM #11010)
    """ Failing for Staggered Fitment due to JIRA defect oees0472. The tooltip for Certificates is not present on the Shopping Cart page. Commenting out failed step until it is fixed. """
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item to my cart and "View shopping Cart"
    Then I verify total price on mini cart
#    And  I verify the RRA Certificate message
    And  I verify the subtotal price on the cart page
    And  I verify the tax on the cart page
    And  I verify the Total price on the cart summary page
    When I select the optional "Certificates" fee for item
    And  I select mini cart
    Then I verify the RRA Certificate "BasePrice" on MiniCart page
    And  I verify the RRA Certificate "Quantity" on MiniCart page
    And  I verify the RRA Certificate "TotalPrice" on MiniCart page
    And  I verify the installation fee amount on the shopping cart page
    And  I verify the subtotal price on the cart page
    And  I verify the tax on the cart page
    And  I verify the Total price on the cart summary page

    Examples:
      | VehicleType             |
      | VEHICLE_NON_STAGGERED_1 |
      | VEHICLE_STAGGERED_1     |

  @dt
  @at
  @dtd
  @bba
  @8892
  @tpmsCartValidation
  @web
  @cartBBA
  @cartRegression
  Scenario Outline: HYBRIS_202_ShoppingCart_Add_Product_to_Shopping_Cart_from_Compare_Page (ALM #8892)
    When I do a free text search for "Tire" and hit enter
    Then I am brought to the page with header "Tires"
    When I click on the "<ProductType>" link
    And  I select item number(s): "<Items>" from the results list to compare
    And  I click the compare products Compare button
    And  I add the first item to my cart and click "View shopping Cart" on the Compare Products page
    Then I verify product "<Item>" is "displayed" on the "cart" page

    Examples:
      | ProductType               | Items        | Item  |
      | Shop for All-Season tires | 18003, 29935 | 29935 |

  @11056
  @web
  @dt
  @verifyPricesTaxesFees
  Scenario Outline: HYBRIS_SHOPPINGCART_Validate Prices and Taxes for quantity updated in shopping cart page for Regular Vehicle_DT (ALM #11056)
    When I go to the homepage
    And  I search for store within "25" miles of "<Zipcode>"
    And  I "continue" the Welcome Popup
    And  I select make "<Zipcode>" my store
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I do a free text search for "<ItemCode>"
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    And  I see a purchase quantity of "<Quantity>"
    And  I verify the item total "displayed" on cart page for "<ItemCode>"
    And  I verify total price on mini cart
    When I update the quantity to "<New Quantity>"
    Then I should see quantity is set to "<New Quantity>" in the cart
    And  I verify the item total "displayed" on cart page for "<ItemCode>"
    And  I verify total price on mini cart
    And  I verify the "Environmental Fee" label present on the shopping cart page
    And  I verify the environment fee amount on the shopping cart page
    And  I verify the "Tire Disposal Fee" label present on the shopping cart page
    And  I verify the tire disposal fee amount on the shopping cart page
    And  I verify the FET fee amount on the shopping cart if applicable to item "<ItemCode>"
    And  I verify the RRA Certificate message
    When I select the optional "Certificates" fee for item
    And  I select mini cart
    Then I verify the RRA Certificate "BasePrice" on MiniCart page
    And  I verify the RRA Certificate "Quantity" on MiniCart page
    And  I verify the RRA Certificate "TotalPrice" on MiniCart page
    And  I verify the installation fee amount on the shopping cart page
    And  I verify the subtotal price on the cart page
    And  I verify the tax on the cart page
    And  I verify the Total price on the cart summary page

    Examples:
      | Year | Make  | Model | Trim         | Assembly       | ProductName        | ItemCode | Quantity | New Quantity | Zipcode |
      | 2016 | Ram   | 2500  | MEGA CAB 4WD | 275 /70 R18 E1 | Terra Grappler G2  | 10204    | 4        | 2            | 85250   |
      | 2012 | Honda | Civic | Coupe DX     | none           | Silver Edition III | 29935    | 4        | 2            | 85250   |

  @dt
  @at
  @dtd
  @bba
  @8886
  @8887
  @web
  @cartBBA
  @cartRegression
  Scenario Outline: ShoppingCart _Remove Product in Shopping Cart from PLP (ALM #8886, 8887)
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I verify "ProductName" "<ProductName>" is in the cart
    When I remove the item from the cart
    Then I should see product has been "removed" in cart message

    Examples:
      | ProductName        | ItemCode |
      | Silver Edition III | 29935    |

  @dt
  @at
  @dtd
  @8886
  @bba
  @mobile
  @cartBBA
  @cartRegression
  Scenario Outline: Mobile - ShoppingCart _Remove Product in Shopping Cart from PLP (ALM #8886)
    When I do a free text search for "<ItemCode>"
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    When I remove the item from the cart
    Then I should see product has been "removed" in cart message

    Examples:
      | ProductName        | ItemCode |
      | Silver Edition III | 29935    |

  @dt
  @at
  @dtd
  @bba
  @8880
  @8881
  @web
  @cartBBA
  @cartRegression
  Scenario Outline: ShoppingCart _Update Product in Shopping Cart from PLP (ALM #8880, 8881)
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    And  I update the quantity to "<UpdatedQuantity>"
    And  I should see quantity is set to "<UpdatedQuantity>" in the cart

    Examples:
      | ProductName        | ItemCode | UpdatedQuantity |
      | Silver Edition III | 29935    | 8               |

  @dt
  @at
  @dtd
  @8880
  @8881
  @bba
  @mobile
  @cartBBA
  @cartRegression
  Scenario Outline: Mobile - ShoppingCart _Update Product in Shopping Cart from PLP (ALM #8880, 8881)
    When I do a free text search for "<ItemCode>"
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    And  I update quantity for item "<ItemCode>" to "<UpdatedQuantity>"
    Then I verify quantity for "<ItemCode>" is set to "<UpdatedQuantity>" in the "cart"

    Examples:
      | ProductName        | ItemCode | UpdatedQuantity |
      | Silver Edition III | 29935    | 8               |

  @dt
  @at
  @web
  @mobile
  @12265
  @12297
  @regression
  @cartRegression
  Scenario Outline: ShoppingCart _Verify Installation fee when the quantity of two or more tire products updated regular vehicle (ALM #12265, 12297)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    Then I verify the PLP header message contains "tire result"
    When I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    Then I verify the Installation price for item "<ItemCodeB>"
    And  I verify the Installation price for item "<ItemCodeA>"
    When I update quantity for item "<ItemCodeA>" to "<QtyItemCodeA>"
    Then I verify the Installation price for item "<ItemCodeA>"
    When I update quantity for item "<ItemCodeB>" to "<QtyItemCodeB>"
    Then I verify the Installation price for item "<ItemCodeB>"
    When I remove the item "<ItemCodeB>" from the cart
    Then I verify the item "<ItemCodeB>" is removed from the cart page
    And  I should see product "<ItemCodeA>" on the "cart" page
    And  I see a purchase quantity of "<QtyItemCodeA>"
    And  I verify the Installation price for item "<ItemCodeA>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCodeA | ItemCodeB | QtyItemCodeA | QtyItemCodeB |
      | 2012 | Honda | CIVIC | COUPE DX | none     | 29935     | 26899     | 2            | 6            |

  @dt
  @at
  @web
  @12299
  @regression
  @cartRegression
  Scenario Outline: ShoppingCart _Verify Installation fee is displayed as a line item for a tire products added to cart for a staggered vehicle (ALM #12299)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    And  I verify the product list page is displayed having "staggered" fitment
    When I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    And  I verify the "Installation" price for items

    Examples:
      | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB |
      | 2010 | Chevrolet | Corvette | Base | none     | 36241     | 36259     |

  @dt
  @at
  @web
  @12337
  @regression
  @cartRegression
  Scenario Outline: ShoppingCart _Verify Installation fee when a vehicle is added with matching tires and wheels (ALM #12337)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    Then I verify the PLP header message contains "tire result"
    When I open the My Vehicles popup
    Then I verify "default" My Vehicles popup displays add vehicle
    And  I verify that My Vehicles displays "<Year> <Make> <Model>" as the current vehicle
    When I select "shop wheels" link
    Then I verify the PLP header message contains "wheel result"
    When I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    And  I verify the Installation price for item "<ItemCodeA>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCodeA | ItemCodeB |
      | 2012 | Honda | CIVIC | COUPE DX | none     | 29935     | 25458     |

  @dt
  @at
  @web
  @13511
  @regression
  @cartRegression
  Scenario Outline: ShoppingCart _Verify Installation fee when tires and wheels are added with unmatching sizes for a regular vehicle (ALM #13511)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    Then I verify the PLP header message contains "tire result"
    When I open the My Vehicles popup
    Then I verify "default" My Vehicles popup displays add vehicle
    And  I verify that My Vehicles displays "<Year> <Make> <Model>" as the current vehicle
    When I select "shop wheels" link
    Then I verify the PLP header message contains "wheel result"
    When I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    And  I verify the Installation price for item "<ItemCodeA>"
    And  I verify the Installation price for item "<ItemCodeB>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCodeA | ItemCodeB |
      | 2012 | Honda | CIVIC | COUPE DX | none     | 29935     | 75918     |

  @dt
  @at
  @web
  @12307
  @regression
  @cartRegression
  Scenario Outline: ShoppingCart _Verify Installation fee when tires and wheels are added without vehicle (ALM #12307)
    When I go to the homepage
    And  I search for store within "25" miles of "<Zipcode>"
    And  I "continue" the Welcome Popup
    And  I select make "<Zipcode>" my store
    And  I click the discount tire logo
    And  I do a free text search for "Tire" and hit enter
    Then I am brought to the page with header "Tires"
    When I click on the "<ProductType>" link
    Then I verify the PLP header message contains "tire result"
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    Then I verify the PLP header message contains "tire result"
    When I do a free text search for "Wheels" and hit enter
    Then I verify the PLP header message contains "Results for "wheels""
    When I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    Then I verify the "Installation" label displayed for item "<ItemCodeA>"
    And  I verify the "Environmental Fee" label displayed for item "<ItemCodeA>"
    And  I verify the "Disposal Fee" label displayed for item "<ItemCodeA>"
    And  I verify the Installation price for item "<ItemCodeA>"
    And  I verify the "Installation" label displayed for item "<ItemCodeB>"
    And  I verify the Installation price for item "<ItemCodeB>"

    Examples:
      | ProductType               | ItemCodeA | ItemCodeB | Zipcode |
      | Shop for All-Season tires | 29935     | 75918     | 85250   |

  @dt
  @at
  @web
  @12290
  @regression
  @cartRegression
  Scenario Outline: ShoppingCart _Verify Installation Fee when a vehicle is added with tires and wheels of different quantity Regular Vehicle (ALM #12290)
    When I go to the homepage
    And  I search for store within "25" miles of "<Zipcode>"
    And  I "continue" the Welcome Popup
    And  I select make "<Zipcode>" my store
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    Then I verify the PLP header message contains "tire result"
    When I open the My Vehicles popup
    And  I select "shop wheels" link
    Then I verify the PLP header message contains "wheel result"
    When I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I update quantity for item "<ItemCodeB>" to "<QtyItemCodeB>"
    Then I verify the "Installation" quantity for item "<ItemCodeB>" display "<InstallQuantityB>"
    And  I verify the Installation price for item "<ItemCodeB>"
    And  I verify the "Installation" quantity for item "<ItemCodeA>" display "<InstallQuantityA>"
    And  I verify the Installation price for item "<ItemCodeA>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCodeA | ItemCodeB | QtyItemCodeB | InstallQuantityA | InstallQuantityB | Zipcode |
      | 2012 | Honda | CIVIC | COUPE DX | none     | 29935     |  75908    | 6            | 4                | 2                | 85250   |

  @dtd
  @web
  @12410
  @regression
  @cartRegression
  Scenario Outline: ShoppingCart _Validate Verbiage of calculating taxes and fees in Order summary Section_DTD (ALM #12410)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the cart summary verbiages are displayed

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | ItemCode |
      | 2012 | Honda     | CIVIC    | COUPE DX | none     | 29935    |
      | 2010 | Chevrolet | Corvette | Base     | none     | 30018    |

  @dt
  @at
  @dtd
  @web
  @11772
  @regression
  @cartRegression
  Scenario Outline: ShoppingCart _Validate Qty for Certificates in Minicart (ALM #11772)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    When I select the optional "Certificates" fee for item
    Then I verify the Certificate fee amount on the shopping cart page
    When I select mini cart
    Then I verify the RRA Certificate "Quantity" on MiniCart page

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | ItemCodeA |
      | 2012 | Honda     | CIVIC    | COUPE DX | none     | 29935     |
      | 2010 | Chevrolet | Corvette | Base     | none     | 30018     |

  @dt
  @web
  @11251
  @regression
  @cartRegression
  Scenario Outline: ShoppingCart _Validate Prices and Taxes for Tire and Wheel Products for Regular Vehicle_DT (ALM #11251)
    When I go to the homepage
    And  I search for store within "25" miles of "<Zipcode>"
    And  I "continue" the Welcome Popup
    And  I select make "<Zipcode>" my store
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    Then I verify the PLP header message contains "tire result"
    When I open the My Vehicles popup
    And  I select "shop wheels" link
    Then I verify the PLP header message contains "wheel result"
    When I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I verify the added products and prices displayed on cart page
    And  I verify the items total price on cart page
    And  I verify the "Wheel Install Kit" price for wheel item "<ItemCodeB>"
    And  I verify the "Hub Centric Ring" price for wheel item "<ItemCodeB>"
    And  I verify the item subtotal for item "<ItemCodeB>"
    And  I verify the Installation price for item "<ItemCodeA>"
    And  I verify the "Environmental Fee" price for item "<ItemCodeA>"
    And  I verify the "Disposal Fee" price for item "<ItemCodeA>"
    And  I verify the item subtotal for item "<ItemCodeA>"
    And  I verify the optional "Certificates" fee is displayed
    And  I verify the cart subtotal for tire and wheel items
    And  I verify the total tax for tire and wheel items
    And  I verify the Total price on the cart summary page
    When I select the checkout option "<Checkout>"
    Then I verify "install with appointment" option is enabled on the Checkout page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCodeA | ItemCodeB | Checkout | Zipcode |
      | 2012 | Honda | CIVIC | COUPE DX | none     |   29935   | 75908     | default  | 85250   |

  @dt
  @web
  @11158
  @regression
  @cartRegression
  Scenario Outline: SHOPPINGCART_Validate Prices in the Mini Cart Modal in different pages without adding a Vehicle_DT (ALM #11158)
    When I change to the store with url "<StoreUrl>"
    And  I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ProductName>"
    And  I verify Mini Cart quick total "before" adding product
    When I add item to my cart and "Close Added To Cart popup"
    And  I extract the mini cart quick total
    Then I should see product detail page with "<ProductName>"
    And  I verify Mini Cart quick total "after" adding product
    When I select mini cart
    Then I verify the added product "<ProductName>" is displayed in Mini Cart
    And  I verify the "View cart" is displayed in Mini Cart
    And  I verify the "Continue Shopping" is displayed in Mini Cart
    When I select View Cart on Mini Cart
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify Mini Cart quick total "after" adding product
    When I select mini cart
    Then I verify the "Installation" price in Mini Cart for product "<ItemCode>"
    And  I verify the "Environmental Fee" price in Mini Cart for product "<ItemCode>"
    And  I verify the "Disposal Fee" price in Mini Cart for product "<ItemCode>"
    And  I verify Mini Cart item total for product "<ProductName>" with item code "<ItemCode>"
    And  I verify Mini Cart total for product "<ProductName>"

    Examples:
      | ProductName        | ItemCode | StoreUrl                    |
      | Silver Edition III | 29935    | /store/az/scottsdale/s/1022 |

  @dtd
  @web
  @11127
  @regression
  @cartRegression
  Scenario Outline: SHOPPING CART_Validate Prices in the Mini Cart Modal in different pages without adding a Vehicle_DTD (ALM #11127)
  """TODO Failing due to defect #9312"""
    When I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ProductName>"
    And  I verify Mini Cart quick total "before" adding product
    When I add item to my cart and "Close Added To Cart popup"
    Then I should see product detail page with "<ProductName>"
    When I extract the mini cart quick total
    Then I verify Mini Cart quick total "after" adding product
    When I select mini cart
    Then I verify the added product "<ProductName>" is displayed in Mini Cart
    And  I verify the "View cart" is displayed in Mini Cart
    And  I verify the "Continue Shopping" is displayed in Mini Cart
    When I select View Cart on Mini Cart
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify Mini Cart quick total "after" adding product
    And  I select mini cart
    Then I verify the "Environmental Fee" price in Mini Cart for product "<ItemCode>"
    And  I verify Mini Cart item total for product "<ProductName>" with item code "<ItemCode>"
    And  I verify Mini Cart total for product "<ProductName>"
    When I select the checkout option "<Checkout>"
    Then I verify that Mini Cart is not displayed on "Shipping Details" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I select the default shipping option as "<Customer>"
    Then I verify that Mini Cart is not displayed on "Shipping Method" page
    When I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    Then I verify that Mini Cart is not displayed on "Payment" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify that Mini Cart is not displayed on "Order Confirmation" page

    Examples:
      | ProductName        | ItemCode | Checkout | Customer            | Credit Card |
      | Silver Edition III | 29935    | default  | default_customer_az | Visa        |

  @dt
  @at
  @web
  @9408
  @regression
  @cartRegression
  Scenario Outline: SHOPPING CART_PERSISTENT CART MODAL_When store is changed with product in the Cart (ALM #9408)
  """TODO - This is failing due to defect #10408"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I verify "checkout now" is enabled
    When I search for stores within "25" miles of non default zip code
    And  I select non default store as my store
    Then I verify switch store popup message is displayed
    And  I verify the switch store options are displayed

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCodeA |
      | 2012 | Honda | CIVIC | COUPE DX | none     | 29935     |

  @dt
  @at
  @dtd
  @web
  @5874
  @regression
  @cartRegression
  Scenario Outline: ShoppingCart_Validate Mini Cart Quick Total on PLP, Cart and Home Page (ALM #5874)
    When I go to the homepage
    And  I search for store within "25" miles of "<Zipcode>"
    And  I "continue" the Welcome Popup
    And  I select make "<Zipcode>" my store
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ProductName>"
    And  I verify Mini Cart quick total "before" adding product
    When I add item to my cart and "View shopping Cart"
    And  I extract the mini cart quick total
    Then I should see product "<ItemCode>" on the "cart" page
    When I navigate back to previous page
    Then I should see product detail page with "<ProductName>"
    And  I verify Mini Cart quick total "after" adding product
    When I select mini cart
    And  I select View Cart on Mini Cart
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify Mini Cart quick total "after" adding product
    When I click the discount tire logo
    Then I am brought to the homepage
    And  I verify Mini Cart quick total "after" adding product

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode | ProductName        | Zipcode |
      | 2012 | Honda | CIVIC | COUPE DX | none     | 29935    | Silver Edition III | 85250   |

  @dt
  @at
  @dtd
  @web
  @6839
  @regression
  @cartRegression
  @coreScenarioCart
  Scenario Outline: ShoppingCart _Change Vehicle Fitment and select clear my cart and continue (ALM #6839)
    When I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item to my cart and "View shopping Cart"
    Then I see selected products on the cart page
    When I open the My Vehicles popup
    And  I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year2>" "<Make2>" "<Model2>" "<Trim2>" "<Assembly2>"
    Then I verify switch vehicle popup message is displayed
    And  I verify the options in the switch vehicle popup
    When I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I close popup modal
    Then I verify that My Vehicles displays "<Make2> <Model2>" in the header
    When I open the My Vehicles popup
    Then I verify recent vehicle "<Year1> <Make1> <Model1>" "displayed"

    Examples:
      | Year1 | Make1     | Model1   | Trim1    | Assembly1 | Year2 | Make2     | Model2   | Trim2    | Assembly2 |
      | 2012  | Honda     | CIVIC    | COUPE DX | none      | 2010  | Chevrolet | Corvette | Base     | none      |
      | 2010  | Chevrolet | Corvette | Base     | none      | 2012  | Honda     | CIVIC    | COUPE DX | none      |

  @dt
  @at
  @dtd
  @web
  @8611
  @regression
  @cartRegression
  Scenario Outline: ShoppingCart _Verify Installation fee when a vehicle is added with matching tires and wheels (ALM #8611)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    Then I verify the PLP header message contains "tire result"
    When I open the My Vehicles popup
    And  I select "shop wheels" link
    Then I verify the PLP header message contains "wheel result"
    When I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the applicable fees are displayed on cart page for item "<ItemCodeA>"
    And  I verify the applicable fees are displayed on cart page for item "<ItemCodeB>"
    When I open the My Vehicles popup
    And  I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" "tire" size search with details "<TireWidth>, <AspectRatio>, <TireDiameter>"
    Then I verify the PLP header message contains "tire result"
    And  I verify the "PLP" banner color is "Yellow"
    And  I verify the message on the "PLP" banner contains "Enter your vehicle to ensure these tires fit"
    And  I verify the message on the "PLP" banner contains "Enter vehicle"
    When I select mini cart
    And  I select View Cart on Mini Cart
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the applicable fees are displayed on cart page for item "<ItemCodeA>"
    And  I verify the applicable fees are displayed on cart page for item "<ItemCodeB>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCodeA | ItemCodeB | TireWidth | AspectRatio | TireDiameter |
      | 2012 | Honda | CIVIC | COUPE DX | none     | 18012     | 75908     | 185       | 70          | 14           |


  @dt
  @at
  @dtd
  @web
  @15615
  @regression
  @cartRegression
  Scenario Outline: HYBRIS-PRICING-MAP-Verify MAP price of product appears in mini cart (ALM #15615)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select shop all from the Product Brand page
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    When I select mini cart
    Then I verify the "<Product>" price in Mini Cart for product "<ItemCode>"
    And  I verify Mini Cart total for product "<Product>"

    Examples:
      | Product          | ItemCode |
      | Energy Saver A/S | 34152    |

  @at
  @dt
  @web
  @15707
  @mobile
  @extendedAssortment
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_EXTENDEDASSORTMENT_Verify Inventory Messaging for Tires on Cart Page with fitment regular vehicle (ALM #15707)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    And  I add item "<ItemCodeB>" of type "none" to my cart and "Continue Shopping"
    And  I add item "<ItemCodeC>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I should see product "<ItemCodeC>" on the "cart" page
    And  I verify 'MY STORE INVENTORY' section displayed for "<ItemCodeA>"
    And  I verify the "<InventoryMessageA>" displayed for "<ItemCodeA>" on "Shopping Cart" page
    And  I verify check nearby stores link is displayed for "<ItemCodeA>"
    And  I verify the "Inventory Message" tooltip for "<ItemCodeA>" is displayed
    And  I verify the stock count message on "Shopping Cart" page for "<ItemCodeA>"
    And  I verify 'MY STORE INVENTORY' section displayed for "<ItemCodeB>"
    And  I verify the "<InventoryMessageB>" displayed for "<ItemCodeB>" on "Shopping Cart" page
    And  I verify check nearby stores link is displayed for "<ItemCodeB>"
    And  I verify the "Inventory Message" tooltip for "<ItemCodeB>" is displayed
    And  I verify the stock count message on "Shopping Cart" page for "<ItemCodeB>"
    And  I verify 'MY STORE INVENTORY' section displayed for "<ItemCodeC>"
    And  I verify the "<InventoryMessageC>" displayed for "<ItemCodeC>" on "Shopping Cart" page
    And  I verify check nearby stores link is displayed for "<ItemCodeC>"
    And  I verify the "Inventory Message" tooltip for "<ItemCodeC>" is displayed
    And  I verify the stock count message on "Shopping Cart" page for "<ItemCodeC>"

    Examples:
      | ZipCode | Year | Make   | Model  | Trim  | Assembly       | ItemCodeA | ItemCodeB | ItemCodeC | InventoryMessageA                  | InventoryMessageB                        | InventoryMessageC              |
      | 85260   | 2015 | Nissan | Altima | Sedan | 215 /55 R17 SL | 30431     | 29888     | 30576     | Order now, available in 3 - 5 days | Order now, available as soon as tomorrow | Order now, available in 2 days |

  @at
  @dt
  @web
  @15829
  @mobile
  @extendedAssortment
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_EXTENDEDASSORTMENT_Verify Inventory Messaging for Tires and Wheels on Cart Page with fitment regular vehicle (ALM #15829)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<Zipcode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    And  I open the My Vehicles popup
    And  I select "shop wheels" link
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify 'MY STORE INVENTORY' section displayed for "<ItemCodeA>"
    And  I verify the "<InventoryMessageA>" displayed for "<ItemCodeA>" on "Shopping Cart" page
    And  I verify check nearby stores link is displayed for "<ItemCodeA>"
    And  I verify the stock count message on "Shopping Cart" page for "<ItemCodeA>"
    And  I verify 'MY STORE INVENTORY' section displayed for "<ItemCodeB>"
    And  I verify the "<InventoryMessageB>" displayed for "<ItemCodeB>" on "Shopping Cart" page
    And  I verify check nearby stores link is displayed for "<ItemCodeB>"
    And  I verify the stock count message on "Shopping Cart" page for "<ItemCodeB>"

    Examples:
      | ZipCode | Year | Make  | Model | Trim     | Assembly | ItemCodeA | ItemCodeB | InventoryMessageA | InventoryMessageB |
      | 85260   | 2012 | Honda | Civic | Coupe DX | none     | 19600     | 23500     | Available today   | Available today   |

  @at
  @dt
  @web
  @15831
  @mobile
  @extendedAssortment
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_EXTENDEDASSORTMENT_Verify Inventory Messaging for Tires on Cart Page free text search (ALM #15831)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a free text search for "<ItemCodeA>" and hit enter
    Then I should see product detail page with "<ItemNameA>"
    When I add item to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCodeB>" and hit enter
    Then I should see product detail page with "<ItemNameB>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify 'MY STORE INVENTORY' section displayed for "<ItemCodeA>"
    And  I verify the "<InventoryMessageA>" displayed for "<ItemCodeA>" on "Shopping Cart" page
    And  I verify check nearby stores link is displayed for "<ItemCodeA>"
    And  I verify the stock count message on "Shopping Cart" page for "<ItemCodeA>"
    And  I verify 'MY STORE INVENTORY' section displayed for "<ItemCodeB>"
    And  I verify the "<InventoryMessageB>" displayed for "<ItemCodeB>" on "Shopping Cart" page
    And  I verify check nearby stores link is displayed for "<ItemCodeB>"
    And  I verify the stock count message on "Shopping Cart" page for "<ItemCodeB>"

    Examples:
      | ZipCode | ItemCodeA | ItemCodeB | ItemNameA         | ItemNameB          | InventoryMessageA | InventoryMessageB                        |
      | 85260   | 29570     | 29888     | Pilot Super Sport | G-Force Comp 2 A/S | Available today   | Order now, available as soon as tomorrow |

  @at
  @dt
  @web
  @15832
  @mobile
  @extendedAssortment
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_EXTENDEDASSORTMENT_Verify Inventory Messaging for Tires and Wheels on Cart Page with fitment staggerde vehicle (ALM #15832)
    When I search for store within "25" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment
    When I add item "<ItemCodeA>" of type "sets" to my cart and "Continue Shopping"
    And  I open the My Vehicles popup
    And  I select "shop wheels" link
    And  I add item "<ItemCodeC>" of type "none" to my cart and "Continue Shopping"
    And  I open the My Vehicles popup
    And  I select "shop wheels" link
    And  I add item "<ItemCodeD>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I should see product "<ItemCodeC>" on the "cart" page
    And  I should see product "<ItemCodeD>" on the "cart" page
    And  I verify 'MY STORE INVENTORY' section displayed for "<ItemCodeA>"
    And  I verify the "<InventoryMessageA>" displayed for "<ItemCodeA>" on "Shopping Cart" page
    And  I verify check nearby stores link is displayed for "<ItemCodeA>"
    And  I verify the "Inventory Message" tooltip for "<ItemCodeA>" is displayed
    And  I verify the stock count message on "Shopping Cart" page for "<ItemCodeA>"
    And  I verify 'MY STORE INVENTORY' section displayed for "<ItemCodeB>"
    And  I verify the "<InventoryMessageB>" displayed for "<ItemCodeB>" on "Shopping Cart" page
    And  I verify check nearby stores link is displayed for "<ItemCodeB>"
    And  I verify the "Inventory Message" tooltip for "<ItemCodeB>" is displayed
    And  I verify the stock count message on "Shopping Cart" page for "<ItemCodeB>"
    And  I verify 'MY STORE INVENTORY' section displayed for "<ItemCodeC>"
    And  I verify the "<InventoryMessageC>" displayed for "<ItemCodeC>" on "Shopping Cart" page
    And  I verify check nearby stores link is displayed for "<ItemCodeC>"
    And  I verify the "Inventory Message" tooltip for "<ItemCodeC>" is displayed
    And  I verify the stock count message on "Shopping Cart" page for "<ItemCodeC>"
    And  I verify 'MY STORE INVENTORY' section displayed for "<ItemCodeD>"
    And  I verify the "<InventoryMessageD>" displayed for "<ItemCodeD>" on "Shopping Cart" page
    And  I verify check nearby stores link is displayed for "<ItemCodeD>"
    And  I verify the "Inventory Message" tooltip for "<ItemCodeD>" is displayed
    And  I verify the stock count message on "Shopping Cart" page for "<ItemCodeD>"

    Examples:
      | ZipCode | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | ItemCodeC | ItemCodeD | InventoryMessageA                        | InventoryMessageB             | InventoryMessageC                  | InventoryMessageD             |
      | 85260   | 2010 | Chevrolet | Corvette | Base | none     | 37068     | 14777     | 66319     | 66338     | Order now, available as soon as tomorrow | Order now, available as soon as tomorrow | Order now, available in 3 - 5 days | Order now, available as soon as tomorrow |

  @dt
  @at
  @web
  @12321
  Scenario Outline: HYBRIS_CART_SHOPPING CART_Validate Check Inventory modal on the shopping cart page (ALM # 12321)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    And  I see "<ItemCode>" on the product list page
    And  I verify check nearby stores link is displayed for "<ItemCode>" on "PLP" page
    When I select the "Check nearby stores" link for item "<ItemCode>" on "PLP" page
    Then I should verify that the Check Availability popup loaded
    And  I verify quantity for "<ItemCode>" is set to "<Quantity>" in the "Check Availability"
    When I close the Check Availability popup
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify check nearby stores link is displayed for "<ItemCode>"
    When I extract the product quantities from the "Cart" page
    And  I select the "Check nearby stores" link for item "<ItemCode>" on "Shopping Cart" page
    Then I should verify that the Check Availability popup loaded
    And  I verify quantity for item code: "<ItemCode>" on 'Check Inventory' popup matches cart page quantity
    And  I verify "Nearby Stores" is displayed on check inventory
    And  I verify 'Only Show In Stock' option is displayed on the 'Check Inventory' popup
    When I use the 'Show Stores Near' search with "<ZipCode>"
    Then I verify "Nearby Stores" is displayed on check inventory
    When I click "MAKE MY STORE" for the first available store
    And  I close the Check Inventory popup
    Then I verify the current 'My Store' matches the selection from the 'Check Inventory' popup
    And  I should see product "<ItemCode>" on the "cart" page

    Examples:
      | Year | Make      | Model    |   Trim     | Assembly | ItemCode | Quantity | ZipCode |
      | 2012 | Honda     | Civic    | Coupe DX   | none     | 18012    | 4        | 85260   |
      | 2014 | Ram       | 3500     | Dually 4WD | none     | 35365    | 6        | 85260   |
      | 2010 | Chevrolet | Corvette | Base       | none     | 36241    | 36259    | 86001   |

  @dtd
  @web
  @11041
  @mobile
  @nexusFt
  @nexusTax
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_Verify the Taxes charged when quantity of the products are updated in shopping cart page with Vehicle_DTD (ALM#11041)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Sensor" fee for item
    And  I select the optional "Certificates" fee for item
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I open the My Vehicles popup
    And  I select "shop wheels" link
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Rebuild Kits" fee for item "<ItemCodeB>"
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I update quantity for item "<ItemCodeA>" to "<Qty>"
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I update quantity for item "<ItemCodeB>" to "<Qty>"
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I take page screenshot

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCodeA | ItemCodeB |
      | 2011 | Honda | Civic | Coupe DX | none     | 40543     | 76474     |

  @dtd
  @web
  @8756
  @11261
  @16604
  @16595
  @12791
  @12552
  @16592
  @mobile
  @nexusFt
  @nexusTax
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_Validate environmental fee and Taxes calculated when Tire and Wheel Products added with vehicle_DTD_standard (ALM#11261,12791,12552)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Sensor" fee for item
    And  I select the optional "Certificates" fee for item
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I open the My Vehicles popup
    And  I select "shop wheels" link
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Rebuild Kits" fee for item "<ItemCodeB>"
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I take page screenshot
    And  I extract "Enviro" from checkout order summary as "environmental fee"
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I take page screenshot
    And  I verify the required fees and add-ons sections are expanded
    Then I verify extracted "Enviro" with "environmental fee" from "Order Confirmation" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page

    Examples:
      | Year | Make      | Model    | Trim          | Assembly       | ItemCodeA | ItemCodeB | Customer            | ShippingOption | Credit Card      |
      | 2014 | Ram       | 3500     | Crew Cab 4WD  | 285 /60 R20 E1 | 31936     | 54274     | default_customer_az | Second Day Air | MasterCard Bopis |
      | 2010 | Chevrolet | Corvette | Base          | none           | 25788     | 66319     | default_customer_oh | Ground         | MasterCard Bopis |

  @dtd
  @web
  @15404
  @16587
  @mobile
  @nexusFt
  @nexusTax
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_Verify Taxes and Fees applied with Tires having promotion is added _DTD (ALM # 15404)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "Certificates" fee for item
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I extract "Enviro" from checkout order summary as "environmental fee"
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "checkout" page
    And  I verify extracted "Taxes" with "sales tax" from "checkout" page
    And  I verify extracted "Total" with "total" from "checkout" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I take page screenshot
    And  I verify the required fees and add-ons sections are expanded
    Then I verify extracted "Enviro" with "environmental fee" from "Order Confirmation" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page
    When I take page screenshot

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode | Customer            | ShippingOption | Credit Card      |
      | 2012 | Honda | Civic | Coupe DX | none     | 29935    | default_customer_az | Ground         | MasterCard Bopis |

  @dtd
  @web
  @16602
  @mobile
  @nexusTax
  @nexusFt
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_Verify Taxes and Fees applied when a line item is removed from the cart page_DTD (ALM#16602)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Shopping cart" page is displayed
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I open the My Vehicles popup
    And  I select "shop wheels" link
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I remove the item "<ItemCodeB>" from the cart
    Then I verify the item "<ItemCodeB>" is removed from the cart page
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I take page screenshot
    And  I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I extract "Enviro" from checkout order summary as "environmental fee"
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "checkout" page
    And  I verify extracted "Taxes" with "sales tax" from "checkout" page
    And  I verify extracted "Total" with "total" from "checkout" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I take page screenshot
    And  I verify the required fees and add-ons sections are expanded
    Then I verify extracted "Enviro" with "environmental fee" from "Order Confirmation" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page
    When I take page screenshot

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCodeA | ItemCodeB | Customer            | ShippingOption | Credit Card      |
      | 2011 | Honda | Civic | Coupe DX | none     | 30184     | 76474     | default_customer_az | Ground         | MasterCard Bopis |

  @dt
  @web
  @storeChange
  @storeChangeStoreLocatorPage
  Scenario Outline: Verify the 'STORE CHANGED MODAL' using the store locator page
    When I search for store within "10" miles of "<ZipCode>"
    And  I select make "<ZipCode>" my store
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I launch the baseUrl
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I click on Store details button in My Store popup
    Then I verify the "<StoreTitle>", "<StoreAddress>" of the current store
    When I launch the baseUrl
    And  I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I select the "First" product result image on "PLP" page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I navigate back to previous page
    And  I click on the "Check nearby stores" link
    Then I should verify that the Check Availability popup loaded
    And  I verify the "<StoreAddress>" store MY STORE is displayed at top
    When I add item to my cart and "View shopping Cart"
    And  I extract my store name on the cart page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I select the checkout option "<Checkout>"
    Then I verify "<StoreAddress>" store on the customer information appointment page
    When I reserve items and complete checkout for "<Customer>"
    Then I verify the appointment confirmation store name matches my store on shopping cart

    Examples:
      | ZipCode | StoreAddress             | StoreTitle          | Checkout | Customer                  |
      | 85250   | 9199 E Talking Stick Way | Discount Tire Store | default  | email_validation_customer |

  @dt
  @web
  @storeChange
  @storeChangeStoreDetailsPage
  Scenario Outline: Verify the 'STORE CHANGED MODAL' using the store Details page
    When I go to the homepage
    And  I search for store within "25" miles of "<ZipCode>"
    And  I select "<ZipCode>" for store details
    And  I click 'Make This My Store' button on Store Details Page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I click on Store details button in My Store popup
    Then I verify the "<StoreTitle>", "<StoreAddress>" of the current store
    When I launch the baseUrl
    And  I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I select the "First" product result image on "PLP" page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I navigate back to previous page
    And  I click on the "Check nearby stores" link
    Then I should verify that the Check Availability popup loaded
    And  I verify the "<StoreAddress>" store MY STORE is displayed at top
    When I add item to my cart and "View shopping Cart"
    And  I extract my store name on the cart page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I select the checkout option "<Checkout>"
    Then I verify "<StoreAddress>" store on the customer information appointment page
    When I reserve items and complete checkout for "<Customer>"
    Then I verify the appointment confirmation store name matches my store on shopping cart
    When I launch the baseUrl
    And  I open the Store Locator page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup

    Examples:
      | ZipCode | StoreAddress             | StoreTitle          | Checkout | Customer                  |
      | 85250   | 9199 E Talking Stick Way | Discount Tire Store | default  | email_validation_customer |

  @dt
  @web
  @storeChange
  @storeChangeCheckAvailabilityPagePLP
  Scenario Outline: Verify the 'STORE CHANGED MODAL' using the check availability page through PLP
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I select the "Check nearby stores" link for item "<ItemCode>" on "PLP" page
    Then I should verify that the Check Availability popup loaded
    When I enter a zipcode of "<ZipCode>"
    And  I click go and wait for results to load
    And  I select store "2" to make my store
    And  I verify the Store Changed Modal contains "<Store>" and success message
    When I click on the "CONTINUE" button
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I select the "First" product result image on "PLP" page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I click on the "Check nearby stores" link
    Then I should verify that the Check Availability popup loaded
    And  I verify the "default" store MY STORE is displayed at top
    When I add item to my cart and "View shopping Cart"
    And  I extract my store name on the cart page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I select the checkout option "<Checkout>"
    Then I verify "<StoreAddress>" store on the customer information appointment page
    When I reserve items and complete checkout for "<Customer>"
    Then I verify the appointment confirmation store name matches my store on shopping cart
    When I launch the baseUrl
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I click on Store details button in My Store popup
    Then I verify the "<StoreTitle>", "<StoreAddress>" of the current store
    When I launch the baseUrl
    And  I open the Store Locator page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup

    Examples:
      | ZipCode | Store  | StoreAddress     | StoreTitle          | Checkout | ItemCode | Customer                  |
      | 86001   | AZF 01 | 1230 S MILTON RD | Discount Tire Store | default  | 29935    | email_validation_customer |

  @dt
  @web
  @storeChange
  @storeChangeCheckAvailabilityPagePDP
  Scenario Outline: Verify the 'STORE CHANGED MODAL' using the check availability page through PDP
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I select the "First" product result image on "PLP" page
    And  I select the "Check nearby stores" link for item "<ItemCode>" on "PDP" page
    Then I should verify that the Check Availability popup loaded
    When I enter a zipcode of "<ZipCode>"
    And  I click go and wait for results to load
    And  I select store "2" to make my store
    And  I verify the Store Changed Modal contains "<Store>" and success message
    When I click on the "CONTINUE" button
    And  I navigate back to previous page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I select the "First" product result image on "PLP" page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I click on the "Check nearby stores" link
    Then I should verify that the Check Availability popup loaded
    And  I verify the "default" store MY STORE is displayed at top
    When I add item to my cart and "View shopping Cart"
    And  I extract my store name on the cart page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I select the checkout option "<Checkout>"
    Then I verify "<StoreAddress>" store on the customer information appointment page
    When I reserve items and complete checkout for "<Customer>"
    Then I verify the appointment confirmation store name matches my store on shopping cart
    When I launch the baseUrl
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I click on Store details button in My Store popup
    Then I verify the "<StoreTitle>", "<StoreAddress>" of the current store
    When I launch the baseUrl
    And  I open the Store Locator page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup

    Examples:
      | ZipCode | Store  | StoreAddress     | StoreTitle          | Checkout | ItemCode | Customer                  |
      | 86001   | AZF 01 | 1230 S MILTON RD | Discount Tire Store | default  | 29935    | email_validation_customer |

  @dt
  @web
  @storeChange
  @storeChangeCheckAvailabilityPageShoppingCart
  Scenario Outline: Verify the 'CONFIRM STORE CHANGED MODAL' using the check availability page through shopping cart
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I add item to my cart and "View shopping Cart"
    And  I select the "Check nearby stores" link for item "<ItemCode>" on "Shopping Cart" page
    Then I should verify that the Check Availability popup loaded
    When I use the 'Show Stores Near' search with "<ZipCode>"
    And  I click "MAKE MY STORE" for the first available store
    Then I verify the 'Confirm Store Changed' Modal is displayed with warning message
    And  I click on the "Use this store" button
    And  I verify the Store Changed Modal contains "<Store>" and success message
    When I click on the "CONTINUE" button
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I navigate back to previous page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I select the "First" product result image on "PLP" page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I click on the "Check nearby stores" link
    Then I should verify that the Check Availability popup loaded
    And  I verify the "default" store MY STORE is displayed at top
    When I close the Check Availability popup
    And  I select mini cart and "View Cart"
    And  I extract my store name on the cart page
    And  I select the checkout option "<Checkout>"
    Then I verify "<StoreAddress>" store on the customer information appointment page
    When I reserve items and complete checkout for "<Customer>"
    Then I verify the appointment confirmation store name matches my store on shopping cart
    When I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I launch the baseUrl
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    When I click on Store details button in My Store popup
    Then I verify the "<StoreTitle>", "<StoreAddress>" of the current store
    When I launch the baseUrl
    And  I open the Store Locator page
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup

    Examples:
      | ZipCode | Store  | StoreAddress     | StoreTitle          | Checkout | ItemCode | Customer                  |
      | 86001   | AZF 01 | 1230 S MILTON RD | Discount Tire Store | default  | 29935    | email_validation_customer |

  @dt
  @web
  @storeChange
  @storeChangeByVehicle
  Scenario Outline: Verify the 'CONFIRM STORE CHANGED' modal reflects the changes for staggered products
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode1>" of type "none" to my cart and "Continue Shopping"
    And  I add item "<ItemCode2>" of type "none" to my cart and "View shopping Cart"
    And  I select the "Check nearby stores" link for item "<ItemCode1>" on "Shopping Cart" page
    Then I should verify that the Check Availability popup loaded
    When I use the 'Show Stores Near' search with "<ZipCode>"
    And  I click store "<StoreAddress>" from the check inventory popup
    Then I verify the 'Confirm Store Changed' Modal is displayed with warning message
    And  I verify the modal popup contains item "<ProductName1>" with "Product not available" message
    And  I verify the modal popup contains item "<ProductName2>" with "Product not available" message
    When I click on the "Use this store" button
    And  I verify the Store Changed Modal contains "<Store>" and success message
    When I click on the "CONTINUE" button
    And  I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup
    And  I should see product has been "removed" in cart message

    Examples:
      | ZipCode | Store  | StoreAddress       | ItemCode1 | ItemCode2 | ProductName1 | ProductName2      | Year | Make      | Model    | Trim | Assembly |
      | 32809   | Flo 06 | 6242 W Colonial Dr | 30018     | 35524     | Eagle F1 GS2 | Pilot Super Sport | 2012 | Chevrolet | Corvette | Base | none     |

  @dt
  @web
  @defect
  @storeChange
  @core_do_not_run
  @modalUIVerification
  @coreScenarioCart_do_not_run
  @coreScenarioCartModalUIVerification
  Scenario Outline: Verify the 'STORE CHANGED' and 'CONFIRM STORE CHANGED' modal UI
  """ Fails to find any stores if Google API is turned off. Failing when it is turned on as well due to
  wrong stores appearing and no MAKE THIS MY STORE button available. Will work with QAs and Dev team to determine
  if this is a defect. May be related to defect CAP-279."""
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I add item to my cart and "View shopping Cart"
    And  I select the first "Check nearby stores" link on "Shopping Cart" page
    Then I should verify that the Check Availability popup loaded
    When I use the 'Show Stores Near' search with "<ZipCode>"
    And  I click "MAKE MY STORE" for the first available store
    Then I verify the 'Confirm Store Changed' Modal is displayed with warning message
    And  I verify the modal popup contains product names
    And  I verify the "USE THIS STORE" button is "enabled"
    And  I verify the "CANCEL" button is "enabled"
    When I click "USE THIS STORE"
    Then I verify the Store Changed Modal is displayed
    And  I verify the Store Changed Modal contains "<StoreAddress>" and success message
    And  I verify the Store Changed Modal contains "<StoreTitle>" and success message
    And  I verify the Store Changed Modal contains "<UpdateMessage>" and success message
    And  I verify the "CONTINUE" button is "enabled"

    Examples:
      | ZipCode | StoreAddress  | StoreTitle                   | UpdateMessage                            |
      | 45805   | 3264 Elida Rd | Discount Tire Store (OHL 01) | Your store location has been updated to: |

  @dtd
  @web
  @bvt
  @core
  @21239
  @cartPayment
  @coreScenarioCart
  @ordersRegression
  @bvtScenarioTaxesAndFees_PayPalExpress
  Scenario Outline: DTD-Checkout Verify fees and taxes without fitment and With Paypal payment (ALM#21239)
    When I go to the homepage
    And  I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select paypal checkout
    And  I switch to "Paypal" window
    And  I log into paypal as "<Customer>"
    And  I continue with the paypal payment
    And  I switch to main window
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I click on the "Continue to Shipping Method" button
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I extract "Enviro" from checkout order summary as "environmental fee"
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I select the default shipping option as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "checkout" page
    And  I verify extracted "Taxes" with "sales tax" from "checkout" page
    And  I verify extracted "Total" with "total" from "checkout" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I verify the required fees and add-ons sections are expanded
    Then I verify extracted "Enviro" with "environmental fee" from "Order Confirmation" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page
    And  I verify the "order" created successfully with "<ItemCode>" entry
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | shipMethod     |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | Year | Make   | Model | Trim  | Assembly | ItemCode          | ProductName       | Customer           |
      | 2016 | Toyota | Camry | XLE   | none     | PRODUCT_DEFAULT_1 | PRODUCT_DEFAULT_1 | paypal_customer_az |

  @dt
  @web
  @bvt
  @defect
  @bvtCart
  @core_do_not_run
  @coreScenarioCart_do_not_run
  @bvtScenarioDoNotClearCart_ConfirmStoreChangedModalContainsCartItems
  Scenario Outline: Verify the 'CONFIRM STORE CHANGED' modal reflects the changes in cart items.
  """ failing due to defect CAP-279. Check Availability at Nearby Stores Search not working.
    Will fail if Google Maps API turned off as well. """
    When I go to the homepage
    And  I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I click on the "continue without vehicle" link
    And  I select shop all from the Product Brand page
    And  I do a free text search for "<ProductName1>" and hit enter
    And  I add item "<ItemCode1>" of type "none" to my cart and "Continue Shopping"
    And  I do a free text search for "<ProductName2>" and hit enter
    And  I add item "<ItemCode2>" of type "none" to my cart and "View shopping Cart"
    And  I select the "Check nearby stores" link for item "<ItemCode1>" on "Shopping Cart" page
    Then I should verify that the Check Availability popup loaded
    When I use the 'Show Stores Near' search with "<ZipCode>"
    And  I click store "<StoreAddress>" from the check inventory popup
    Then I verify the 'Confirm Store Changed' Modal is displayed with warning message
    And  I verify the modal popup contains product names
    And  I verify the "USE THIS STORE" button is "enabled"
    And  I verify the "CANCEL" button is "enabled"
    When I click on the "Use this store" button
    Then I verify the Store Changed Modal is displayed
    And  I verify the Store Changed Modal contains "<StoreAddress>" and success message
    And  I verify the Store Changed Modal contains "<StoreTitle>" and success message
    And  I verify the Store Changed Modal contains "<UpdateMessage>" and success message
    And  I verify the Store Changed Modal contains "<Store>" and success message
    When I click on the "CONTINUE" button
    Then I verify product "<ProductName1>" is "displayed" on the "cart" page
    And  I verify product "<ProductName2>" is "not displayed" on the "cart" page
    And  I verify the product prices have changed
    When I click on "My Store" title
    Then I verify the default "<StoreAddress>" in the popup

    Examples:
      | ZipCode | Store  | StoreAddress  | ItemCode1 | ItemCode2 | ProductName1           | ProductName2         | UpdateMessage                            | StoreTitle                  |
      | 45805   | OHL 01 | 3264 Elida Rd | 17198     | 35215     | PILOT SPORT A/S 3 5RIB | PILOT SPORT A/S PLUS | Your store location has been updated to: | Discount Tire Store (OHL 01)|

  @at
  @dt
  @web
  @bvt
  @core
  @21237
  @cartTaxesAndFees
  @coreScenarioCart
  @bvtScenarioFeesAndTaxesFreeTextSearch
  @coreScenarioCartVerifyPricesFeesTaxes
  @coreScenarioCartVerifyPricesFeesTaxesFreeTextSearch
  Scenario Outline: Verify prices, Fees and Taxes without fitment and with free text search (ALM#21237)
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    And  I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I verify the item price with PDP price and item total displayed on cart page
    And  I verify the "Environmental Fee" price for items
    And  I verify the "Disposal Fee" price for items
    And  I verify the "Installation" price for items
    And  I verify the subtotal price on the cart page
    And  I verify the tax on the cart page
    And  I verify the Total price on the cart summary page
    When I select the optional "Certificates" fee for item
    Then I verify the Certificate fee amount on the shopping cart page
    And  I verify the subtotal price on the cart page
    And  I verify the tax on the cart page
    And  I verify the Total price on the cart summary page
    When I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "<Checkout>"
    And  I select the checkout without install reason "<Reason>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I click next step for customer information
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    When I verify the required fees and add-ons sections are expanded
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page

    Examples:
      | VehicleType             | ItemCode          | ProductName       | Checkout | Reason                              | Customer                |
      | VEHICLE_NON_STAGGERED_1 | PRODUCT_DEFAULT_1 | PRODUCT_DEFAULT_1 | default  | Make an appointment at a later time | DEFAULT_CONFIG_CUSTOMER |

  @at
  @dt
  @web
  @core
  @21238
  @coreScenarioCart
  @coreScenarioCartVerifyPricesFeesTaxes
  @coreScenarioCartVerifyPricesFeesTaxesFitmentFlow
  Scenario Outline: Verify prices, Fees and Taxes - staggered and non-staggered fitment (ALM#21238)
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    Then I should see the fitment panel with vehicle "<VehicleType>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "<Fitment>" fitment
    When I add "in stock" item of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    And  I verify the added products and prices displayed on cart page
    And  I verify the items total price on cart page
    And  I verify the "Environmental Fee" price for items
    And  I verify the "Disposal Fee" price for items
    And  I verify the "Installation" price for items
    And  I verify the subtotal price on the cart page
    And  I verify the tax on the cart page
    And  I verify the Total price on the cart summary page
    When I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "<Checkout>"
    And  I select the checkout without install reason "<Reason>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I click next step for customer information
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the required fees and add-ons sections are expanded
    And  I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page

    Examples:
      | VehicleType             | Checkout | Reason                              | Customer                    | Credit Card | Fitment       |
      | VEHICLE_NON_STAGGERED_1 | default  | Make an appointment at a later time | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | non-staggered |
      | VEHICLE_STAGGERED_1     | default  | Make an appointment at a later time | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | staggered     |

  @web
  @dtd
  @core
  @21241
  @coreScenarioCart
  @coreScenarioTaxesAndFees
  @coreScenarioTaxesAndFees_CreditCardPayment_EnvironmentalFees_FreeTextSearch
  Scenario Outline: DTD-Checkout Verify fees(having Environment Fee) and taxes with credit card Payments(Master Card),ground Shipping & with Customers(US-AZ) (ALM#21241)
    When I go to the homepage
    And  I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "<Checkout>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I extract "Enviro" from checkout order summary as "environmental fee"
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    Then I verify extracted "Taxes" with "sales tax" from "checkout" page
    And  I verify extracted "Total" with "total" from "checkout" page
    When I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "checkout" page
    And  I verify extracted "Taxes" with "sales tax" from "checkout" page
    And  I verify extracted "Total" with "total" from "checkout" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I should see reservation confirmation message with expected product name and item code
    And  I verify the required fees and add-ons sections are expanded
    And  I verify extracted "Enviro" with "environmental fee" from "Order Confirmation" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page

    Examples:
      | ItemCode          | ProductName       | Checkout | Customer            | ShippingOption | Credit Card |
      | PRODUCT_DEFAULT_1 | PRODUCT_DEFAULT_1 | default  | default_customer_az | Ground         | MasterCard  |

  @web
  @dtd
  @core
  @21242
  @coreScenarioCart
  @coreScenarioTaxesAndFees
  @coreScenarioTaxesAndFees_CreditCardPayments_NoEnvironmentalFees_FreeTextSearch
  @coreScenarioTaxesAndFees_CreditCardPayments_NoEnvironmentalFees_FreeTextSearch_Non_FPO
  Scenario Outline: DTD-Checkout Verify fees(not-having Environment Fee) and taxes with credit card Payments(Visa, Shipping(NextDay Air), Regular Customer (ALM#21242)
    When I go to the "<No Env Fee State>" state homepage
    And  I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "<Checkout>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    Then I verify extracted "Taxes" with "sales tax" from "checkout" page
    And  I verify extracted "Total" with "total" from "checkout" page
    When I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Taxes" with "sales tax" from "checkout" page
    And  I verify extracted "Total" with "total" from "checkout" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page

    Examples:
      | ItemCode          | ProductName       | Checkout | No Env Fee State | Customer            | ShippingOption | Credit Card |
      | PRODUCT_DEFAULT_1 | PRODUCT_DEFAULT_1 | default  | HI               | default_customer_hi | Ground         | Visa        |

  @web
  @dtd
  @core
  @21243
  @coreScenarioCart
  @coreScenarioTaxesAndFees
  @coreScenarioTaxesAndFees_CreditCardPayments_NoEnvironmentalFees_FreeTextSearch
  @coreScenarioTaxesAndFees_CreditCardPayments_NoEnvironmentalFees_FreeTextSearch_FPO_APO
  Scenario Outline: DTD-Checkout Verify fees(not-having Environment Fee) and taxes with credit card Payments(Visa, Shipping(NextDay Air), FPO / APO Customers (ALM#21243)
    When I go to the "<State>" state homepage
    And  I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Not-Charged" for "all" product on "Shopping cart" page
    When I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "<Checkout>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Not-Charged" for "all" product on "Checkout" page
    When I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    Then I verify extracted "Taxes" with "sales tax" from "checkout" page
    And  I verify extracted "Total" with "total" from "checkout" page
    When I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Taxes" with "sales tax" from "checkout" page
    And  I verify extracted "Total" with "total" from "checkout" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page

    Examples:
      | ItemCode          | ProductName       | Checkout | Customer     | ShippingOption         | Credit Card      | State |
      | PRODUCT_DEFAULT_1 | PRODUCT_DEFAULT_1 | default  | fpo_customer | Priority Mail Military | Discover         | FPO   |
      | PRODUCT_DEFAULT_1 | PRODUCT_DEFAULT_1 | default  | apo_customer | Priority Mail Military | American Express | FPO   |

  @web
  @dtd
  @core
  @21244
  @coreScenarioCart
  @coreScenarioTaxesAndFees
  @coreScenarioTaxesAndFees_CreditCardPayment_WithFitment
  Scenario Outline: Verify prices, Fees and Taxes with fitment on DTD, checkout with Credit Card - Core (ALM#21244)
  """Shipped to states having zero tax rates"""
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    Then I should see the fitment panel with vehicle "<VehicleType>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item to my cart and "View shopping Cart"
    And  I select the optional "Certificates" fee for items
    And  I select the optional "TPMS Rebuild Kit" fee for items
    Then I verify the "TPMS Rebuild Kit" price
    When I select the optional "TPMS Sensor" fee for items
    Then I verify the "TPMS Sensor" price
    When I select the optional "Valve Stem" fee for items
    Then I verify the "Valve Stem" price
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<ShippingCustomer>" and continue to next page
    And  I select shipping option: "<ShippingOption>" as "<ShippingCustomer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Tax" is "Not-Charged" for "all" product on "Checkout" page
    And  I verify "Enviro" is "Not-Charged" for "all" product on "Checkout" page
    When I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<BillingCustomer>"
    And  I place the order for "<BillingCustomer>"
    Then I am brought to the order confirmation page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page

    Examples:
      | VehicleType             | ShippingCustomer    | BillingCustomer     | ShippingOption | Credit Card |
      | VEHICLE_NON_STAGGERED_1 | DEFAULT_CUSTOMER_CA | DEFAULT_CUSTOMER_CA | Ground         | MasterCard  |
      | VEHICLE_NON_STAGGERED_1 | DEFAULT_CUSTOMER_CA | DEFAULT_CUSTOMER_AZ | Ground         | MasterCard  |
      | VEHICLE_STAGGERED_1     | DEFAULT_CUSTOMER_CA | DEFAULT_CUSTOMER_CA | Ground         | MasterCard  |
      | VEHICLE_STAGGERED_1     | DEFAULT_CUSTOMER_CA | DEFAULT_CUSTOMER_AZ | Ground         | MasterCard  |

  @dt
  @at
  @dtd
  @web
  @19774
  @miniCartClearAll
  @canonicalEnhancements
  Scenario Outline: Mini clear all cart functionality (ALM #19774)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    When I add item "<ItemCode>" of type "none" to my cart and "Continue Shopping"
    And  I select mini cart
    Then I verify the added product "<ProductName>" is displayed in Mini Cart
    When I click on the "clear cart" link
    And  I click on "Keep cart" element in cart page
    Then I verify the added product "<ProductName>" is displayed in Mini Cart
    When I click on the "clear cart" link
    And  I click on "Clear cart confirmation" element in cart page
    Then I verify the header cart total is "<price value>"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"

    Examples:
      | Year | Make   | Model | Trim       | Assembly                            | FitmentOption | ItemCode | price value | ProductName                     |
      | 2010 | Nissan | 370Z  | Coupe Base | F 245 /40 R19 SL - R 275 /35 R19 SL | tire          | 25991    | $0.00       | Bridgestone Potenza RE760 Sport |


  @dtd
  @web
  @mobile
  @19775
  @certificatesNewModal
  @canonicalEnhancements
  Scenario Outline: DTD new modal for certificates (ALM #19775)
    When I do a "homepage" "brand" "tire" search with option: "<Brand>"
    And  I select shop all from the Product Brand page
    And  I select the "In Stock" checkbox
    Then I verify saved store details in "PLP" page
    When I select the first available View Details from the results page
    And  I set first available size options on PDP page
    And  I add item to my cart and "View shopping Cart"
    And  I click on "Why buy certificates" element in cart page
    Then I verify element "Certificates new modal" has following text "Certificates for Repair & Replacement" in cart page

    Examples:
      | Brand  |
      | FALKEN |

  @at
  @dt
  @web
  @core
  @21245
  @coreScenarioCart
  @coreScenarioCartRemoveAndDeleteProducts
  Scenario Outline: Cart Page Remove and Delete functionality (ALM#21245)
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    Then I should see the fitment panel with vehicle "<VehicleType>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the added products and prices displayed on cart page
    And  I verify the required fees and add-ons sections are expanded
    And  I verify any displayed Instant Savings dollar value is green color
    When I remove the item from the cart
    Then I should see product has been "removed" in cart message
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I remove my "<VehicleType>" vehicle type
    And  I close popup modal
    Then I verify the added products and prices displayed on cart page
    And  I verify the required fees and add-ons sections are expanded
    And  I verify any displayed Instant Savings dollar value is green color
    When I click on the "Checkout Now" button
    And  I do a "default" vehicle search for "<VehicleType>" vehicle type
    And  I close popup modal
    And  I select mini cart and "Clear Cart"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page

    Examples:
      | VehicleType             | Checkout | Customer                    | Credit Card |
      | VEHICLE_NON_STAGGERED_1 | default  | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |
      | VEHICLE_STAGGERED_1     | default  | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @at
  @dt
  @dtd
  @web
  @core
  @21246
  @coreScenarioCart
  @coreScenarioCartContinueShoppingFromShoppingCartPage
  Scenario Outline: Cart Page verify Continue Shopping button click from Shopping Cart page navigates to fitment (ALM#21246)
    When I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I click on the "Continue Shopping" button
    Then I should see the fitment panel is "displayed"
    When I select mini cart and "Clear Cart"
    Then I should see the fitment panel is "displayed"
    When I do a "homepage" vehicle search for "<VehicleType1>" vehicle type
    Then I should see the fitment panel with vehicle "<VehicleType1>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I click on the "Continue Shopping" button
    Then I should see the fitment panel with vehicle "<VehicleType1>"
    When I close popup modal
    And  I select mini cart and "Clear Cart"
    Then I verify the PLP page is displayed
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search for "<VehicleType2>" vehicle type
    Then I should see the fitment panel with vehicle "<VehicleType2>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I click on the "Continue Shopping" button
    Then I should see the fitment panel with vehicle "<VehicleType2>"
    When I close popup modal

    Examples:
      | ItemCode          | ProductName       | VehicleType1            | VehicleType2        |
      | PRODUCT_DEFAULT_1 | PRODUCT_DEFAULT_1 | VEHICLE_NON_STAGGERED_1 | VEHICLE_STAGGERED_1 |

  @dt
  @at
  @dtd
  @web
  @20790
  @20789
  @suggestedSelling
  Scenario Outline: HYBRIS_CART_SHOPPING CART_Suggested Selling_Verify the carousel of suggestions is getting displayed on PDP when going through Treadwell Flow with Vehicle in Session(ALM#20790, 20789)
  """Fails due to defect OCW-3310: The Treadwell recommended product does not match the product at position 1 on add to cart popup modal"""
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    Then I verify treadwell logo is displayed with title and subtitle on "treadwell modal"
    When I set "Primary Driving Location" to "<ZipCode>"
    And  I select view recommended tires
    Then I verify treadwell details section "present"
    And  I verify treadwell product banner display "TOP RECOMMENDATION" "<TopRanking>" on "PLP" page
    And  I verify treadwell top recommended product is "displayed" in 'Top 3 Tiles'
    And  I verify treadwell product banner display "RECOMMENDED" "<RecommendedRanking>" on "PLP" page
    When I extract the product at position "1" from "PLP"
    And  I select the "3rd" product result image on PLP page with "Add to Cart" button
    And  I click on the "Add to Cart" button
    Then I verify suggested selling carousel inner-tile for "Add to Cart" is "displayed" with title "<Title>" with no more than five products and no duplicate products in the carousel
    And  I verify the product is displayed at position "1" in the 'suggested selling carousel'

    Examples:
      | Title                    | Year | Make  | Model | Trim     | Assembly | ZipCode | TopRanking | RecommendedRanking |
      | CUSTOMERS ALSO PURCHASED | 2012 | Honda | Civic | Coupe DX | none     | 86001   |  #1        | #2                 |

  @dt
  @at
  @dtd
  @web
  @20630
  @suggestedSelling
    Scenario Outline: HYBRIS_CART_SHOPPING CART_Suggested Selling_Verify the carousel of suggestions is getting displayed on Items Added to Cart modal when Tire is browsed_Guest(ALM#20630)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I extract the product at position "2" from "PLP"
    And  I select the "2nd" product result image on PLP page with "Add to Cart" button
    And  I click on the "Add to Cart" button
    Then I verify suggested selling carousel inner-tile for "Add to Cart" is "displayed" with title "<Title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "Add to cart" page

    Examples:
      | Title                    | Year | Make  | Model | Trim     | Assembly |
      | CUSTOMERS ALSO PURCHASED | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @at
  @web
  @dtd
  @20631
  @suggestedSelling
  Scenario Outline: HYBRIS_CART_SHOPPING CART_Suggested Selling_Verify the carousel of suggestions is getting displayed on Items Added to Cart modal when Wheel is browsed_Guest(ALM#20631)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "wheel" and shop for all tires/wheels
    And  I extract the product at position "2" from "PLP"
    And  I select the "3rd" product result image on PLP page with "Add to Cart" button
    And  I click on the "Add to Cart" button
    Then I verify suggested selling carousel inner-tile for "Add to Cart" is "displayed" with title "<Title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "Add to cart" page

    Examples:
      | Title                    | Year | Make  | Model | Trim     | Assembly |
      | CUSTOMERS ALSO PURCHASED | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @at
  @web
  @20810
  @suggestedSelling
  Scenario Outline: HYBRIS_CART_SHOPPING CART_Suggested Selling_Verify the carousel of suggestions is getting displayed on Items Added to Cart modal when Tire is browsed via Check Availability Modal_Guest(ALM#20810)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I extract the product at position "2" from "PLP"
    And  I select the "3rd" product result image on PLP page with "Add to Cart" button
    And  I click on the "Check nearby stores" link
    Then I should verify that the Check Availability popup loaded
    When I click on the "Add to Cart" button
    Then I verify suggested selling carousel inner-tile for "Add to Cart" is "displayed" with title "<Title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "Add to cart" page

    Examples:
      | Title                    | Year | Make  | Model | Trim     | Assembly |
      | CUSTOMERS ALSO PURCHASED | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @at
  @web
  @20812
  @suggestedSelling
  Scenario Outline: HYBRIS_CART_SHOPPING CART_Suggested Selling_Verify the carousel of suggestions is getting displayed on Items Added to Cart modal when Wheel is browsed via Check Availability Modal(ALM#20812)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I extract the product at position "1" from "PLP"
    And  I go to the homepage
    And  I click on the "<Year> <Make>" button
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the PLP header message contains "wheel result"
    When I select the "3rd" product result image on PLP page with "Add to Cart" button
    And  I click on the "check nearby stores" link
    Then I should verify that the Check Availability popup loaded
    When I click on the "Add to Cart" button
    Then I verify suggested selling carousel inner-tile for "Add to Cart" is "displayed" with title "<Title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "Add to cart" page
    And  I verify the product is displayed at position "1" in the 'suggested selling carousel'

    Examples:
      | Title                    | Year | Make  | Model | Trim     | Assembly |
      | CUSTOMERS ALSO PURCHASED | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @at
  @dtd
  @web
  @20704
  @suggestedSelling
    Scenario Outline: HYBRIS_CART_SHOPPING CART_Suggested Selling_Verify the carousel of suggestions on Items Added to Cart modal is not displaying the Tire added to cart(ALM#20704)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I select the "3rd" product result image on PLP page with "Add to Cart" button
    And  I click on the "view details" link
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I click on the "Add to Cart" button
    And  I extract "Product name cart" from shopping cart as "Product name cart"
    And  I go to the homepage
    And  I click on the "<Year> <Make>" button
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the "4th" product result image on PLP page with "Add to Cart" button
    And  I click on the "Add to Cart" button
    Then I verify the product in cart is not displayed in the "Add to Cart" carousel

    Examples:
      | Year | Make  | Model | Trim     | Assembly |
      | 2012 | Honda | Civic | Coupe DX | none     |

  @dt
  @at
  @dtd
  @web
  @20707
  @suggestedSelling
  Scenario Outline: HYBRIS_CART_SHOPPING CART_Suggested Selling_Verify the carousel of suggestions on Items Added to Cart modal is not displaying the Tire added to cart(ALM#20707)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the PLP header message contains "wheel result"
    When I select the "3rd" product result image on PLP page with "Add to Cart" button
    And  I click on the "view details" link
    Then I verify the "PDP" banner color is "Green"
    And  I verify the message on the "PDP" banner contains "These wheels fit your vehicle"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I click on the "Add to Cart" button
    And  I extract "Product name cart" from shopping cart as "Product name cart"
    And  I go to the homepage
    And  I click on the "<Year> <Make>" button
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the "4th" product result image on PLP page with "Add to Cart" button
    And  I click on the "Add to Cart" button
    Then I verify the product in cart is not displayed in the "Add to Cart" carousel

    Examples:
      | Year | Make   | Model  | Trim  | Assembly       |
      | 2015 | Nissan | Altima | Sedan | 215 /55 R17 SL |