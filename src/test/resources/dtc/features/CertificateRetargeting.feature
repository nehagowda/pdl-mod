@certificateRetargeting
Feature: Certificate Re-targeting
"""Import 'certificate re-targeting automation data.impex' in hac before running below scenarios."""

  Background:
    Given I go to the homepage

  @dtd
  @web
  @mobile
  @certificatesCart_01
  Scenario Outline: Certificates cart page_Check original product with limited information_vehicle applied
    When I navigate to the "<certificatesCartPath>" url
    Then I verify the vehicle "<vehicleInfo>" is displayed on "certificates cart" page
    And  I verify "ProductBrand" "<productBrand>" is in the cart
    And  I verify "ProductName" "<productName>" is in the cart
    And  I verify "ProductSize" "<productSize>" is in the cart
    And  I verify "ItemCode" "<productCode>" is in the cart
    And  I verify that product inventory is not displayed
    And  I verify that "shop tires" button is not displayed on cart page
    And  I verify that "shop wheels" button is not displayed on cart page

    Examples:
      | certificatesCartPath                   | productBrand | productName   | productSize            | productCode | vehicleInfo                  |
      | /cart/cert-auto-test-vehicle-available | Bridgestone  | Potenza RE-11 | 285 /35 R19 99W SL BSW | 17928       | 2010 Chevrolet Corvette Base |

  @dtd
  @web
  @mobile
  @certificateCart_02
  Scenario Outline: Certificates cart page_verify the Certificates information
    When I navigate to the "<certificatesCartPath>" url
    Then I should see certificates displayed under item column
    And  I verify the "Certificates" tooltip for "<productCode>" is displayed
    And  I verify that the certificate unit price is "<certificateUnitPrice>" for "<productCode>"
    And  I verify that the certificate quantity is "<certificateQty>" for "<productCode>"
    And  I verify that the certificate quantity is not modifiable
    And  I verify that the certificate line item total is "displayed" for "<productCode>"
    And  I verify that the subtotal on the cart page is "<cartSubtotal>"
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    And  I verify the Total price on the cart summary page
    And  I verify that "remove button" is not displayed

    Examples:
      | certificatesCartPath                   | productCode | certificateUnitPrice | certificateQty | cartSubtotal |
      | /cart/cert-auto-test-vehicle-available | 17928       | 38.50                | 1              | 38.50        |
      | /cart/cert-auto-test-qty-more-than-one | 34302       | 14.25                | 4              | 57.00        |

  @dtd
  @web
  @mobile
  @certificateCart_03
  Scenario Outline: Certificates cart page_Checkout
    When I navigate to the "<certificatesCartPath>" url
    And  I click checkout now button
    Then I verify the checkout page is displayed without showing Certificates for Repair and Replacement modal
    And  I should see "<CheckoutShippingMethod>" url is launched

    Examples:
      | certificatesCartPath                   | CheckoutShippingMethod    |
      | /cart/cert-auto-test-vehicle-available | /checkout/shipping-method |

  @dtd
  @web
  @mobile
  @certificatesCheckout_01
  Scenario Outline: User views the certificates shipping method page
  """TODO: The Customer data may need to be updated accordingly in Customer.java when run on stage or UAT"""
  """Check for both US and Canada orders"""
    When I navigate to the "<certificatesCartPath>" url
    And  I click checkout now button
    Then I verify that shipping address from the original order for "<Customer>" are now pre-populated
    And  I verify that "Edit Shipping Details link" is not displayed
    And  I verify the Delivery Email Address field is pre-populated from "<Customer>"

    Examples:
      | certificatesCartPath                   | Customer                  |
      | /cart/cert-auto-test-vehicle-available | CERTIFICATES_CUSTOMER_US  |
      | /cart/cert-auto-test-canada-address    | CERTIFICATES_CUSTOMER_CAN |

  @dtd
  @web
  @mobile
  @certificatesCheckout_02
  Scenario Outline: User updates the certificates delivery email address
  """TODO: The Customer data may need to be updated accordingly in Customer.java when run on stage or UAT"""
  """Check for both US and Canada orders"""
    When I navigate to the "<certificatesCartPath>" url
    And  I click checkout now button
    Then I verify the Delivery Email Address field is pre-populated from "<Customer>"
    When I update the delivery email address to "<newDeliveryEmailAddress1>"
    And  I click on the "Continue To Payment" button
    Then I verify "<newDeliveryEmailAddress1>" is displayed as delivery email address
    And  I verify the email under shipping details is still populated from "<Customer>"
    When I select edit delivery email address
    And  I update the delivery email address to "<newDeliveryEmailAddress2>"
    And  I click on the "Continue To Payment" button
    Then I verify "<newDeliveryEmailAddress2>" is displayed as delivery email address
    And  I verify the email under shipping details is still populated from "<Customer>"

    Examples:
      | certificatesCartPath                   | Customer                  | newDeliveryEmailAddress1     | newDeliveryEmailAddress2     |
      | /cart/cert-auto-test-vehicle-available | CERTIFICATES_CUSTOMER_US  | atuotest_update_US1@dtd.com  | atuotest_update_US2@dtd.com  |
      | /cart/cert-auto-test-canada-address    | CERTIFICATES_CUSTOMER_CAN | atuotest_update_CAN1@dtd.com | atuotest_update_CAN2@dtd.com |

  @dtd
  @web
  @mobile
  @certificatesCheckout_03
  Scenario Outline: User views Order Summary in shipping method and billing page
    When I navigate to the "<certificatesCartPath>" url
    And  I extract "Quantity" from shopping cart as "quantity"
    And  I extract "ProductBrand" from shopping cart as "product brand"
    And  I extract "ProductName" from shopping cart as "product name"
    And  I extract "ProductSize" from shopping cart as "product size"
    And  I extract "Item Price" from shopping cart as "item price"
    And  I extract "Subtotal" from shopping cart as "subtotal"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I click checkout now button
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify that "show fee details link" is not displayed in "checkout" page
    And  I verify extracted "Quantity" with "quantity" from "cart" page
    And  I verify extracted "ProductBrand" with "product brand" from "cart" page
    And  I verify extracted "ProductName" with "product name" from "cart" page
    And  I verify extracted "ProductSize" with "product size" from "cart" page
    And  I verify extracted "Item Price" with "item price" from "cart" page
    And  I verify extracted "Subtotal" with "subtotal" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I click on the "Continue To Payment" button
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify that "show fee details link" is not displayed in "checkout" page
    And  I verify extracted "Quantity" with "quantity" from "cart" page
    And  I verify extracted "ProductBrand" with "product brand" from "cart" page
    And  I verify extracted "ProductName" with "product name" from "cart" page
    And  I verify extracted "ProductSize" with "product size" from "cart" page
    And  I verify extracted "Item Price" with "item price" from "cart" page
    And  I verify extracted "Subtotal" with "subtotal" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page

    Examples:
      | certificatesCartPath                   |
      | /cart/cert-auto-test-vehicle-available |
      | /cart/cert-auto-test-qty-more-than-one |

  @dtd
  @web
  @mobile
  @certificateCart_04
  Scenario Outline: Certificates cart_Original Order with Certificates not Eligible for Some Products
    When I navigate to the "<certificatesCartPath>" url
    Then I should see product "<wheelProduct>" on the "cart" page
    And  I should see product "<tireProduct>" on the "cart" page
    And  I verify the "Certificates" tooltip for "<wheelProduct>" is displayed
    And  I verify that the certificate unit price is "not displayed" for "<wheelProduct>"
    And  I verify that the certificate quantity is "<wheelCertificateQty>" for "<wheelProduct>"
    And  I verify that the certificate line item total is "Not Eligible" for "<wheelProduct>"
    And  I verify that the certificate line item subtotal is "not displayed" for "<wheelProduct>"
    And  I verify the "Certificates" tooltip for "<tireProduct>" is displayed
    And  I verify that the certificate unit price is "<tireCertificateUnitPrice>" for "<tireProduct>"
    And  I verify that the certificate quantity is "<tireCertificateQty>" for "<tireProduct>"
    And  I verify that the certificate line item total is "displayed" for "<tireProduct>"
    And  I verify that the certificate line item subtotal is "displayed" for "<tireProduct>"
    And  I verify that the subtotal on the cart page is "<cartSubtotal>"
    And  I verify the Total price on the cart summary page
    And  I verify that "shop tires" button is not displayed on cart page
    And  I verify that "shop wheels" button is not displayed on cart page

    Examples:
      | certificatesCartPath                  | wheelProduct | tireProduct | wheelCertificateQty | tireCertificateUnitPrice | tireCertificateQty | cartSubtotal |
      | /cart/cert-auto-test-tire-wheel-mixed | 62212        | 10556       | 4                   | 34.50                    | 4                  | 138.00       |

  @dtd
  @web
  @mobile
  @certificateCart_05
  Scenario Outline: Certificates cart_Expired Certificates Error
  """TODO: The errorMsg in example may need to be updated if the default error message is updated in backoffice"""
    When I navigate to the "<CertificatesCartPath>" url
    Then I should see product "<Product>" on the "cart" page
    And  I verify the page title is "Shopping Cart"
    And  I verify the configured error message "<ErrorMsg>" is displayed
    And  I verify the Learn More link is "displayed" after the error message
    When I click the Learn More link
    Then I should see "<CustomerServiceCertificate>" url is launched
    When I navigate back to previous page
    Then I should see certificates displayed under item column
    And  I verify the "Certificates" tooltip for "<Product>" is displayed
    And  I verify that the certificate unit price is "<CertificateUnitPrice>" for "<Product>"
    And  I verify that the certificate quantity is "<CertificateQty>" for "<Product>"
    And  I verify that the certificate line item total is "Expired" for "<Product>"
    And  I verify that the certificate line item subtotal is "not displayed" for "<Product>"
    And  I verify that cart order summary section is not displayed
    And  I verify that "shop tires" button is displayed on cart page
    And  I verify that "shop wheels" button is displayed on cart page
    And  I verify that "checkout" button is not displayed on cart page

    Examples:
      | CertificatesCartPath         | Product | ErrorMsg            | CertificateUnitPrice | CertificateQty | CustomerServiceCertificate     |
      | /cart/cert-auto-test-expired | 43437   | Expired Certificate | 16.75                | 1              | /customer-service/certificates |

  @dtd
  @web
  @mobile
  @certificateCart_06
  Scenario Outline: Certificates cart_Paid Certificates Error
  """TODO: The errorMsg in example may need to be updated if the default error message is updated in backoffice"""
    When I navigate to the "<certificatesCartPath>" url
    Then I verify the page title is "Certificates Paid"
    And  I verify the configured error message "<errorMsg>" is displayed
    And  I verify the Learn More link is "not displayed" after the error message
    And  I verify that cart order details is not displayed
    And  I verify that "shop tires" button is displayed on cart page
    And  I verify that "shop wheels" button is displayed on cart page
    And  I verify that "checkout" button is not displayed on cart page

    Examples:
      | certificatesCartPath      | errorMsg                       |
      | /cart/cert-auto-test-paid | Certificates Already Purchased |