@bvtccnonstaggered
Feature: CreditCardCC1NonStaggered

  @at
  @dt
  @web
  @bvt
  @core
  @21262
  @bvtOrders
  @coreScenarioOrders
  @bvtAddToCartFromPDP_DTandAT_nonStaggered
  Scenario Outline: Credit Card and CC1 transaction - Add To Cart from PDP - non-staggered fitment (ALM#21262)
    When I change to the store with url "<StoreUrl>"
    And  I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    Then I should see the fitment panel with vehicle "<VehicleType>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "<Fitment>" fitment
    When I select the "First" available product result image on PLP page
    And  I add item to my cart and "View shopping Cart"
    Then I verify the item price with PDP price and item total displayed on cart page
    When I select the checkout option "with appointment"
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click continue for appointment customer details page
    Then I verify date and time on the customer details appointment page
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page

    Examples:
      | StoreUrl                   | VehicleType             | Customer                          | Credit Card        | Fitment       |
      | /store/az/flagstaff/s/1002 | VEHICLE_NON_STAGGERED_1 | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis   | non-staggered |
      | /store/ca/chino/s/1038     | VEHICLE_NON_STAGGERED_1 | DEFAULT_CUSTOMER_BOPIS_CC1_2      | CarCareOne_2 Bopis | non-staggered |