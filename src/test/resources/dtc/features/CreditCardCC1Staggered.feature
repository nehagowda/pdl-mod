@bvtbopisdtat
Feature: CreditCardCC1Staggered

  @at
  @dt
  @web
  @bvt
  @core
  @21247
  @bvtOrders
  @coreScenarioOrders
  @bvtAddToCartFromPDPDTandAT_staggered
  Scenario Outline: Credit Card and CC1 transaction - Add to Cart from PLP - staggered fitment (ALM#21247)
    When I change to the store with url "<StoreUrl>"
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the product list page is displayed having "<Fitment>" fitment
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    And  I verify the added products and prices displayed on cart page
    When I select the checkout option "<Checkout>"
    And  I select the checkout without install reason "<Reason>"
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page

    Examples:
      | StoreUrl                   | Year | Make   | Model | Trim       | Assembly | FitmentOption | Checkout | Reason                              | Customer                        | Credit Card      | Fitment   |
      | /store/az/flagstaff/s/1002 | 2014 | Nissan | 370Z  | Coupe Base | none     | tire          | default  | Make an appointment at a later time | DEFAULT_CUSTOMER_BOPIS_CC1      | CarCareOne Bopis | staggered |
      | /store/ca/chino/s/1038     | 2014 | Nissan | 370Z  | Coupe Base | none     | tire          | default  | Make an appointment at a later time | DEFAULT_CUSTOMER_BOPIS_DISCOVER | Discover Bopis   | staggered |