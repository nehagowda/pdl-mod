@bvtdtdShoppingCart
Feature: DtdShoppingCart

  @dtd
  @web
  @bvt
  @cart
  @core
  @C001
  @9635
  @16593
  @mobile
  @nexusFt
  @nexusTax
  @nexusTaxSit
  @coreScenarioCart
  @bvtScenarioTaxesAndFees_EnvFee
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_Validate the taxes and fees for the web order created with Legislation Passed State having Tires and Wheels_Regular Fitment_DTD (ALM#9635,16593)
    When I go to the "<Env Fee State>" state homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    And  I verify "Enviro" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Sensor" fee for item
    And  I select the optional "Certificates" fee for item
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    And  I verify "Enviro" is "Charged" for "<ItemCodeA>" product on "Shopping cart" page
    And  I verify "Enviro" is "Not-Charged" for "<ItemCodeB>" product on "Shopping cart" page
    And  I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I select the optional "TPMS Rebuild Kits" fee for item "<ItemCodeB>"
    Then I verify "Tax" is "Charged" for "all" product on "Shopping cart" page
    When I extract "Enviro" from shopping cart as "environmental fee"
    And  I extract "Total" from shopping cart as "total"
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "cart" page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I extract "Enviro" from checkout order summary as "environmental fee"
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify extracted "Enviro" with "environmental fee" from "checkout" page
    And  I verify extracted "Taxes" with "sales tax" from "checkout" page
    And  I verify extracted "Total" with "total" from "checkout" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify extracted "Enviro" with "environmental fee" from "Order Confirmation" page
    And  I verify "Enviro" is "Charged" for "<ItemCodeA>" product on "Shopping cart" page
    And  I verify "Enviro" is "Not-Charged" for "<ItemCodeB>" product on "Shopping cart" page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page

    Examples:
      | Year | Make | Model  | Trim     | Assembly | FitmentOption | ItemCodeA | ItemCodeB | ShippingOption | Credit Card       | Env Fee State | Customer            |
      | 2012| Honda | Civic  | Coupe DX | none     | tire          | 29935     | 75908     | Ground         | MasterCard Bopis  | CA            | DEFAULT_CUSTOMER_CA |

  @dtd
  @web
  @bvt
  @cart
  @C004
  @16571
  @16596
  @mobile
  @nexusFt
  @nexusTax
  @nexusTaxSit
  @bvtScenarioTaxesAndFees_NoEnvFee
  Scenario Outline: HYBRIS_CART_SHOPPINGCART_Verify web order created for the legislation passed state not having environmental Fee_DTD (ALM #16596,16571)
    When I go to the "<No Env Fee State>" state homepage
    And  I do a free text search for "<ItemCodeA>" and hit enter
    And  I add item to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCodeB>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Shopping cart" page
    And  I verify "Tax" is "Not-Charged" for "all" product on "Shopping cart" page
    When I select the optional "Heat Cycling" fee for item
    And  I extract "Taxes" from shopping cart as "sales tax"
    And  I extract "Total" from shopping cart as "total"
    And  I select the checkout option "<Checkout>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I verify extracted "Taxes" with "sales tax" from "cart" page
    And  I verify extracted "Total" with "total" from "cart" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify "Enviro" is "Not-Charged" for "all" product on "Checkout" page
    And  I verify "Tax" is "Charged" for "all" product on "Checkout" page
    When I take page screenshot
    And  I extract "Taxes" from checkout order summary as "sales tax"
    And  I extract "Total" from checkout order summary as "total"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify extracted "Taxes" with "sales tax" from "Order Confirmation" page
    And  I verify extracted "Total" with "total" from "Order Confirmation" page

    Examples:
      | ItemCodeA | ItemCodeB | Checkout | ShippingOption | Credit Card | No Env Fee State | Customer            |
      | 40361     | 25254     | default  | Ground         | Discover    | HI               | DEFAULT_CUSTOMER_HI |