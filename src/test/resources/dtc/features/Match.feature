@match
Feature: Match

  Background:
    Given I change to the default store

  @dt
  @at
  @bba
  @dtd
  @web
  @8791
  @mobile
  @matchBBA
  @tpmsCartValidation
  Scenario Outline: Verify the TPMS line item appears in the shopping cart (ALM # 8791)
  """Availability of TPMS Rebuilt message is linked with Vehicle As per Rule, If vehicle is not selected, TPMS will
    not appear in any environment"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify "ItemCode" "<ItemCode>" is in the cart
    And  I verify the required fees and add-ons sections are expanded
    And  I should see quantity is set to "<Quantity>" in the cart
    And  I verify the "TPMS Rebuild Kits" label present on the shopping cart page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode | Quantity |
      | 2012 | Honda | Civic | Coupe DX | none     | 29935    | 4        |

  @dt
  @at
  @bba
  @dtd
  @web
  @8665
  @matchBBA
  Scenario Outline: Verify the TPMS line item appears in the shopping cart for Tire (ALM # 8665)
  """Availability of TPMS Rebuilt message is linked with Vehicle As per Rule, If vehicle is not selected, TPMS will
    not appear in any environment"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I verify "ItemCode" "<ItemCode>" is in the cart
    And  I verify the required fees and add-ons sections are expanded
    And  I verify the "TPMS Rebuild Kits" label present on the shopping cart page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode | ProductName        |
      | 2012 | Honda | Civic | Coupe DX | none     | 29935    | Silver Edition III |

  @dt
  @at
  @dtd
  @bba
  @web
  @8781
  @matchBBA
  Scenario Outline: HYBRIS_130-131_Match_Fitment Search_Optional Sizes_Tires (ALM #8781)
  """TODO: BUG 8566 - TireSize 205/65-15 missing in STG and PROD"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I select the "First" product result image on "PLP" page
    Then I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SizeOption | TireSize  |
      | 2012 | Honda | Civic | Coupe EX | none     | 15         | 205/65-15 |
      | 2012 | Honda | Civic | Coupe DX | none     | 16         | 205/55-16 |

  @dt
  @at
  @dtd
  @bba
  @8781
  @mobile
  @matchBBA
  Scenario Outline: Mobile - HYBRIS_130-131_Match_Fitment Search_Optional Sizes_Tires (ALM #8781)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I select the "First" product result image on "PLP" page
    Then I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SizeOption | TireSize  |
      | 2012 | Honda | Civic | Coupe EX | none     | 15         | 205/65-15 |
      | 2012 | Honda | Civic | Coupe DX | none     | 16         | 205/55-16 |

  @dt
  @at
  @bba
  @web
  @8369
  @9616
  @mobile
  @matchBBA
  @searchAndVerifyFitmentOptionsLinks
  Scenario Outline: Fitment Search from Home Page (Wheels)(Tires) (ALM #8369, 9616)
  """| 2012 | Honda  | Civic | Coupe DX   | None                                | Staggered     |"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I am brought to the fitment page with the "<Staggering>" vehicle message
    And  I verify all the "<Staggering>" fitment option links by clicking on them

    Examples:
      | Year | Make   | Model | Trim       | Assembly                            | Staggering    |
      | 2010 | Nissan | 370Z  | Coupe Base | F 245 /40 R19 SL - R 275 /35 R19 SL | Non Staggered |

  @dt
  @web
  @8380
  @8663
  @searchForTiresViaHeaderTiresByVehicle
  Scenario Outline: Fitment Search for Tires by Vehicle under Shop for Regular (Tires) (ALM #8380, 8663)
  """Also, Fitment Search for Tires by Vehicle type under Category Page (Tires) (ALM #8663) TODO: ALM testcase 8663 is
    marked for REVIEW due to inconsistency between title and test steps.
    First data table covers 'Fitment Search from Tires by Vehicle under Shop (Tires)' and 'Fitment Search from Tires by
    Vehicle under Shop (Normal /Regular Vehicle Tires)'
    Second data table covers '118_Match_Fitment Search_Shop Modal_By Vehicle_Staggered Tires_DT' TODO: AB 4/10/2017,
    "All tire sets" was not an available option
    Third data table covers 'Fitment Search from Tires by Vehicle under Shop (Light Truck (LT) Vehicle Tires)' TODO:
    This data only exists on staging"""
    When I open the "TIRES" navigation link
    And  I click the "TIRES BY VEHICLE" menu option
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"

    Examples:
      | Year | Make      | Model          | Trim       | Assembly       | Header      |
      | 2015 | Honda     | Accord         | Coupe EX   | none           | tire result |
      | 2015 | Chevrolet | Silverado 2500 | Crew Cab   | 245 /75 R17 E1 | tire result |

  @dt
  @web
  @8380
  @8663
  @searchForTiresViaHeaderTiresByVehicle
  Scenario Outline: Fitment Search for Tires by Vehicle under Shop for staggered (Tires) (ALM #8380, 8663)
  """Also, Fitment Search for Tires by Vehicle type under Category Page (Tires) (ALM #8663) TODO: ALM testcase 8663 is
    marked for REVIEW due to inconsistency between title and test steps.
    First data table covers 'Fitment Search from Tires by Vehicle under Shop (Tires)' and 'Fitment Search from Tires by
    Vehicle under Shop (Normal /Regular Vehicle Tires)'
    Second data table covers '118_Match_Fitment Search_Shop Modal_By Vehicle_Staggered Tires_DT' TODO: AB 4/10/2017,
    "All tire sets" was not an available option
    Third data table covers 'Fitment Search from Tires by Vehicle under Shop (Light Truck (LT) Vehicle Tires)' TODO:
    This data only exists on staging"""
    When I open the "TIRES" navigation link
    And  I click the "TIRES BY VEHICLE" menu option
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment

    Examples:
      | Year | Make   | Model | Trim       | Assembly                            |
      | 2010 | Nissan | 370Z  | Coupe Base | F 245 /40 R19 SL - R 275 /35 R19 SL |

  @dt
  @web
  @8393
  @searchByVehicleForWheelsViaHomepageMenuAndTestPagination
  Scenario Outline: Match_Fitment Search_Shop Category_By Vehicle_Wheels_DT (ALM #8393)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "wheel"
    Then I verify the PLP header message contains "<Header>"
    And  I verify that each page displays 10 items and total number of pages is equal to total count / 10 on "PLP" page
    And  I verify that the sort by dropdown value is set to "Relevance"

    Examples:
      | Year | Make  | Model  | Trim     | Assembly | Header       |
      | 2015 | Honda | Accord | Coupe EX | none     | wheel result |

  @at
  @dt
  @web
  @bba
  @dtd
  @9020
  @8399
  @19643
  @mobile
  @matchBBA
  @solrSearch
  @vehiclePresentation
  Scenario Outline: Search Vehicle In Session Shop By Size Yellow Warning Notification(ALM #8399,9020,19643)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    And  I verify the message on the "PLP" banner contains "<FitTireMessage>"
    When I go to the homepage
    And  I do a "homepage" "tire" size search with details "<TireWidth>, <AspectRatio>, <TireDiameter>"
    And  I click on the "CONTINUE WITH THIS VEHICLE" link
    Then I verify the "PLP" banner color is "Red"
    And  I verify the message on the "PLP" banner contains "<NoFitTireMessage>"
    And  I verify the message on the "PLP" banner contains "<TireFitLink>"
    And  I verify the "PLP" results banner message contains "<String>"
    When I open the My Vehicles popup
    And  I select "shop tires" link
    When I go to the homepage
    And  I do a "homepage" "wheel" size search with details "<WheelSize>"
    And  I click on the "CONTINUE WITH THIS VEHICLE" link
    Then I verify the "PLP" banner color is "Yellow"
    And  I verify the message on the "PLP" banner contains "<MayNotFitWheelMessage>"
    And  I verify the message on the "PLP" banner contains "<WheelsFitLink>"
    When I do a free text search for "<TireBrand>" and hit enter
    Then I verify the "PLP" banner color is "Green"
    And  I verify the PLP header message contains "<TireBrand>"
    And  I verify the message on the "PLP" banner contains "<productFitmeassage>"
    And  I verify the message on the "PLP" banner contains "<MayNotFitLink>"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I click on the "<MayNotFitLink>" link
    Then I verify the PLP header message contains "<TireBrand>"
    And  I verify the "PLP" banner color is "Yellow"
    And  I verify the message on the "PLP" banner contains "<MayNotFitMessage>"
    And  I verify the message on the "PLP" banner contains "<ProductFitLink>"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I select the "First" product result image on "PLP" page
    Then I verify the "PDP" banner color is "Red"
    And  I verify the message on the "PDP" banner contains "<NoFitTireMessage>"
    And  I verify the message on the "PDP" banner contains "<TireFitLink>"
    And  I verify Vehicle Details Banner on "PDP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I click on the "<TireFitLink>" link
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "<FitTireMessage>"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I go to the homepage
    And  I do a "homepage" "Brand" "tire" search with option: "<Brand>"
    And  I click on the "CONTINUE WITH THIS VEHICLE" link
    And  I select "<SubCategory>" to shop
    Then I verify the "PLP" banner color is "Red"
    And  I verify the message on the "PLP" banner contains "<NoFitTireMessage>"
    And  I verify the message on the "PLP" banner contains "<TireFitLink>"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | TireWidth | AspectRatio | TireDiameter | String    | WheelSize | TireBrand | Brand | SubCategory      | NoFitTireMessage                   | WheelsFitLink        | ProductFitLink         | MayNotFitLink                    | MayNotFitWheelMessage                        | MayNotFitMessage                               | productFitmeassage              | FitTireMessage               | TireFitLink         |
      | 2012 | Honda | Civic | Coupe DX | none     | 185       | 65          | 14           | 185/65R14 | 19        | Michellin | Nitto | All-Season tires | These tires don't fit your vehicle | shop wheels that fit | shop products that fit | view all results for this search | We're not sure these wheels fit your vehicle | We're not sure these products fit your vehicle | These products fit your vehicle | These tires fit your vehicle | shop tires that fit |

  @dt
  @at
  @dtd
  @bba
  @web
  @8400
  @mobile
  @matchBBA
  @searchByOptionalTireSelectionViaHomepage
  @searchByOptionalWheelSelectionViaHomepage
  Scenario Outline: Fitment Search for Optional Tire/Wheel Sizes (ALM #8400)
  """TODO - Updated Test Data as per parameters values @8400 Test Id"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I "expand" "optional tire and wheel size"
    And  I select a fitment option "<SizeOption>"
    And  I select a fitment option "<TireSize>"
    Then I verify the PLP header message contains "<Header>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SizeOption | TireSize  | Header       |
      | 2012 | Honda | Civic | Coupe DX | None     | 15         | 215/60-15 | tire result  |
      | 2012 | Honda | Civic | Coupe DX | None     | 19         | 225/35-19 | wheel result |

  @dt
  @at
  @dtd
  @bba
  @web
  @8783
  @searchBBA
  Scenario Outline: HYBRIS_111_Match_Fitment Search_Home Page_DTD (ALM #8783)
  """TODO: Data only exists in STG; Request entered into Data spreadsheet"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    And  I am brought to the fitment page with the "Non Staggered" vehicle message
    And  I verify all the "Basic Non Staggered" fitment option links by clicking on them

    Examples:
      | Year | Make   | Model | Trim      | Assembly |
      | 2016 | Toyota | Camry | Hybrid LE | none     |


  @dt
  @at
  @web
  @9010
  @mobile
  @searchGlobalNavigation
  Scenario Outline: Fitment Match Search Global Navigation (ALM #9010)
    When I go to the homepage
    When I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I am brought to the page with path "/fitmentresult/tires"
    When I navigate back to previous page
    And  I select a fitment option "Best Selling"
    Then I am brought to the page with path "/fitmentresult/tires?sort=bestSeller-asc"
    When I navigate back to previous page
    And  I select a fitment option "Tires on promotion"
    Then I am brought to the page with path "/fitmentresult/tires?sort=onpromotion-asc"
    When I navigate back to previous page
    And  I select a fitment option "All Wheels"
    Then I am brought to the page with path "/fitmentresult/wheels"
    When I navigate back to previous page
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "Optional Tire Sizes"
    And  I select a fitment option "<TireSize>"
    Then I am brought to the page with path "/fitmentresult/tires/optionalPlusSize/215/55-17"
    When I go to the homepage
    And  I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "Optional Wheel Sizes"
    And  I select a fitment option "17" (+0)"
    Then I am brought to the page with path "/fitmentresult/wheels/optionalPlusSize/17"

    Examples:
      | Year | Make  | Model  | Trim     | Assembly | TireSize  |
      | 2015 | Honda | Accord | Coupe EX | none     | 215/55-17 |

  @dt
  @web
  @5764
  @regression
  @matchRegression
  Scenario Outline: HYBRIS_FITMENT_MATCH_My vehicleicon_wheelSet (ALM #5764)
    When I go to the homepage
    And  I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "wheel"
    Then I am brought to the page with path "/fitmentresult/wheels/staggered/front"
    And  I verify "FRONT" staggered option tab is displayed on PLP result page
    And  I verify "REAR" staggered option tab is displayed on PLP result page
    And  I verify "FRONT" wheel diameter matches with the size of each product on the results page
    When I select "REAR" staggered tab on PLP result page
    Then I verify "REAR" wheel diameter matches with the size of each product on the results page

    Examples:
      | Year | Make      | Model    | Trim | Assembly |
      | 2010 | Chevrolet | Corvette | Base | none     |

  @dt
  @web
  @core
  @21261
  @regression
  @foundItLower
  @matchRegression
  @foundItLowerTest
  @coreScenarioPriceMatch
  Scenario Outline: Verify 'Found it Lower?' modal form submission (ALM#21261)
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select "All-Season tires" from the Product Brand page
    And  I select the "First" product result image on "PLP" page
    And  I set first available size options on PDP page
    And  I extract the product details on the PDP page
    And  I click on "Found it Lower" link
    Then I verify "Found it Lower" modal is displayed
    And  I verify modal title is "FOUND IT LOWER?"
    And  I verify the details displayed on modal for product
    And  I verify modal form text
    And  I verify the "SUBMIT" button is "disabled"
    And  I verify modal footer text
    When I enter "<LowerPrice>" into the "Lower Price(Per Tire)" field
    And  I enter "<LowerPriceLink>" into the "Link to Lower Price" field
    And  I enter "<FirstName>" into the "First Name" field
    And  I enter "<LastName>" into the "Last Name" field
    And  I enter "<Email>" into the "Email" field
    And  I enter "<DaytimePhone>" into the "Daytime Phone" field
    And  I enter "<PostalCode>" into the "Postal Code" field
    Then I verify the "SUBMIT" button is "enabled"
    When I click on the "SUBMIT" button
    Then I verify the success modal is displayed

    Examples:
      | FirstName | LastName | Email        | DaytimePhone | PostalCode | LowerPrice | LowerPriceLink |
      | QA        | Tester   | test@123.com | 0987654321   | 85210      | 50.00      | www.abc.com    |

  @dtd
  @web
  @bvt
  @core
  @21260
  @regression
  @priceMatch
  @foundItLower
  @matchRegression
  @instantPriceMatchTest
  @coreScenarioPriceMatch
  @bvtInstantPriceMatchTest
  Scenario Outline: Verify 'Instant Price Match' modal form submission (ALM#21260)
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    And  I extract the product details on the PDP page
    And  I click on "Instant Price Match" link
    And  I verify modal title is "INSTANT PRICE MATCH"
    And  I verify the details displayed on modal for product
    And  I verify modal form text
    And  I verify the "SUBMIT" button is "disabled"
    And  I verify modal footer text
    When I enter "<LowerPrice>" into the "Lower Price(Per Tire)" field
    And  I enter "<LowerPriceLink>" into the "Link to Lower Price" field
    And  I enter "<FirstName>" into the "First Name" field
    And  I enter "<LastName>" into the "Last Name" field
    And  I enter "<Email>" into the "Email" field
    And  I enter "<DaytimePhone>" into the "Daytime Phone" field
    And  I enter "<PostalCode>" into the "Postal Code" field
    And  I click on the "SUBMIT" button
    Then I verify the success modal is displayed

    Examples:
      | FirstName | LastName | Email        | DaytimePhone | PostalCode | LowerPrice | LowerPriceLink | ItemCode | ProductName        |
      | QA        | Tester   | test@123.com | 0987654321   | 85210      | 50.00      | www.abc.com    | 29935    | Silver Edition III |