@myAccount
Feature: My Accounts
"""TODO: Current Customer Email Test Data will be replaced by Automation Test Data In Customer Data refactoring branch
   Once we reset mechanism setup by Developers"""

  Background:
    Given I change to the default store

  @dt
  @at
  @dtd
  @web
  @myAccountUI
  Scenario Outline: Verify the MyAccount Fields (ALM#-----)
    When I go to the homepage
    Then I verify Join/Sign-in is displayed on "homepage" page
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    And  I should see "Email" field is displayed
    And  I should see "Password" field is displayed
    And  I verify that "Sign-In" is disabled by default
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    Then I should see "Sign-In" is enabled
    And  I verify keep me signed-in option is displayed
    When I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header

    Examples:
      | Email                | password  |
      | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @14979
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Field Validation and Sign In (ALM#14979)
    When I go to the homepage
    Then I verify Join/Sign-in is displayed on "homepage" page
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    When I enter "<New Email>" into "Email" field
    And  I enter "<Valid Password>" into "Password" field
    Then I should see "Sign-In" is enabled
    When I click on the "Sign-in" button
    Then I verify invalid credentials error validation message is "displayed"
    When I enter "<Wrong Email>" into "Email" field
    And  I enter "<Valid Password>" into "Password" field
    And  I click on the "Sign-in" button
    Then I verify invalid credentials error validation message is "displayed"
    When I enter "<Valid Email>" into "Email" field
    And  I enter "<Wrong Password>" into "Password" field
    And  I click on the "Sign-in" button
    Then I verify invalid credentials error validation message is "displayed"
    When I enter "<Email>" into "Email" field
    And  I enter "<password>" into "Password" field
    Then I verify invalid credentials error validation message is "not displayed"
    And  I should see "Sign-In" is enabled
    When I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header

    Examples:
      | New Email             | Valid Password | Wrong Email | Valid Email           | Wrong Password | Email                | password  |
      | itsybitsy20@gmail.com | Discount9      | abc@xyz.com | itsybitsy16@gmail.com | T3STing789     | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @6798
  @myAccount_smoke
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Sign In to an Account_SMOKE (ALM#6798)
    When I go to the homepage
    Then I verify Join/Sign-in is displayed on "homepage" page
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    Then I should see "Sign-In" is enabled
    And  I verify keep me signed-in option is displayed
    When I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header

    Examples:
      | Email                | password  |
      | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @15002
  @15006
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Sign In_Forget and Reset Password validation_INTEGRATION (ALM#15002,15006)
    When I go to the homepage
    Then I verify Join/Sign-in is displayed on "homepage" page
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    And  I should see "Email" field is displayed
    And  I should see "Password" field is displayed
    And  I verify keep me signed-in option is displayed
    And  I verify that "Sign-In" is disabled by default
    When I select forgot password option
    Then I verify Reset Your Password instruction displayed on forgot password modal
    And  I verify Reset Your Password email validation error message should display for all invalid email addresses
    When I enter "<My Email>" in reset password email address field
    Then I should see "RESET PASSWORD" is enabled
    When I click on the "RESET PASSWORD" button
    Then I verify Reset Your Password instruction displayed after reset password
    And  I verify my account sign-in modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "<My Email>" email and token passed
    And  I navigate to My Account password reset url
    And  I enter "<New Password>" in reset password field
    Then I should see "RESET PASSWORD" is enabled
    When I click on the "RESET PASSWORD" button
    Then I verify Join/Sign-in is displayed on "homepage" page
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    And  I enter "<New Password>" in password field
    Then I should see "Sign-In" is enabled
    When I click on the "Sign-in" button
    Then I should see my "nitishvoid" name displayed on homepage header
    When I click on the "View my account" link
    And  I click on "password" edit link
    And  I enter "<New Password>" into "current password" field
    And  I enter "<Old Password>" into "new password" field
    And  I enter "<Old Password>" into "re type new password" field
    And  I click on the "Update" button

    Examples:
      | My Email             | New Password | Old Password |
      | sampletest@gmail.com | random1      | Discount1    |

  @dt
  @at
  @dtd
  @6558
  @web
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Sign In_Global Header (ALM#6558)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    And  I verify Join/Sign-in is displayed on "homepage" page
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    And  I verify Join/Sign-in is displayed on "global header" page
    When I go to the homepage
    And  I open the "WHEELS" navigation link
    And  I do a "homepage" "wheel" size search with details "<Diameter>, <WheelWidth>, <BoltPattern>"
    Then I verify the PLP header message contains "<Header1>"
    And  I verify Join/Sign-in is displayed on "global header" page
    When I go to the homepage
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I verify Join/Sign-in is displayed on "global header" page
    When I go to the homepage
    And  I select "footer" "About Us"
    Then I verify Join/Sign-in is displayed on "global header" page
    When I select "footer" "Contact Us"
    Then I verify Join/Sign-in is displayed on "global header" page
    When I select "footer" "Credit"
    Then I verify Join/Sign-in is displayed on "global header" page
    When I select "footer" "Customer Care"
    Then I verify Join/Sign-in is displayed on "global header" page
    When I select "footer" "More Topics..."
    Then I verify Join/Sign-in is displayed on "global header" page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Header      | Diameter | WheelWidth | BoltPattern       | Header1      | ItemCode |
      | 2012 | Honda | Civic | Coupe DX | none     | tire result | 15       | 6.5        | 5-114.3 MM/5-4.5" | wheel result | 29935    |

  @dt
  @at
  @dtd
  @web
  @15034
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Sign In_Form UI Validation (ALM#15034)
    When I go to the homepage
    Then I verify Join/Sign-in is displayed on "homepage" page
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    Then I should see "Sign-In" is enabled
    When I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I select my account header navigation
    And  I click on the "View my account" link
    Then I am brought to the page with header "<Header>"
    When I select my account header navigation
    Then I verify the "Sign-out" link is displayed
    When I click on the "Sign-out" link
    Then I verify Join/Sign-in is displayed on "homepage" page

    Examples:
      | Header             | Email                | password  |
      | Hello, AutouserNew | autouser_c@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @6800
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Profile Tab_Sign In and Edit Email Address_INTEGRATION (ALM#6800)
    When I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    Then I should see "Sign-In" is enabled
    When I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I select my account header navigation
    And  I click on the "View My Account" link
    And  I click on "E-mail Address" edit link
    Then I should see my value populated in "Email" field
    And  I verify that "Update" is disabled by default
    When I enter "<New Email>" into "Email" field
    Then I should see "Update" is enabled
    When I click on the "Update" button
    Then I verify successfully updated your e-mail address message displayed
    And  I verify my updated email authentication link message displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "<New Email>" email and token passed
    And  I navigate to My Account email authenticate url

    Examples:
      | Header     | New Email          | Email                | password  |
      | My Account | newemail@gmail.com | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @defect
  @6799
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Profile Tab_Sign In and Edit Password (ALM#6799)
  """Defect: The password field is not collapsing after the password is updated"""
    When I go to the homepage
    Then I verify Join/Sign-in is displayed on "homepage" page
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I select my account header navigation
    And  I click on the "View My Account" link
    Then I am brought to the page with header "<Header>"
    When I click on "password" edit link
    Then I verify that "Update" is disabled by default
    When I enter "<password>" into "current password" field
    And  I enter "<new Password>" into "new password" field
    And  I enter "<new Password>" into "re type new password" field
    Then I should see "Update" is enabled
    When I click on the "Update" button
    Then I am brought to the page with header "<Header>"
    When I click on "password" edit link
    And  I close popup modal
    And  I enter "<new Password>" into "current password" field
    And  I enter "<password>" into "new password" field
    And  I enter "<password>" into "re type new password" field
    Then I should see "Update" is enabled
    When I click on the "Update" button
    Then I am brought to the page with header "<Header>"

    Examples:
      | Header     | Email                | password  | new Password |
      | My Account | autouser_a@gmail.com | Discount1 | Discount2    |
      | My Account | autouser_a@gmail.com | Discount2 | Discount1    |

  @dt
  @at
  @web
  @15105
  @myAccountOrders
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout Flow_Sign In DT with only Primary Info (ALM#15105)
    When I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    When I select the checkout option "default"
    And  I select the checkout without install reason "<Reason>"
    And  I click next step for customer information
    And  I select "sign in to skip this step." link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I am brought to the page with header "Checkout"
    And  I verify "First Name, Last Name, Address Line 1, Address Line 2, City, State, Zip Code, Country, Email, Phone Type, Phone Number" values for "<Customer>" are now pre-populated
    When I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    And  I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId   |
      | amountOfSale |
      | siteNumber   |
      | orderType    |
      | customerType |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml customer node values
    And  I assert order xml subItems

    Examples:
      | VehicleType             | ItemCode          | ProductName       | Reason                              | Email                | password  | Customer          |
      | VEHICLE_NON_STAGGERED_1 | PRODUCT_DEFAULT_1 | PRODUCT_DEFAULT_1 | Make an appointment at a later time | autouser_a@gmail.com | Discount1 | MY_ACCOUNT_USER_A |

  @dtd
  @web
  @5778
  @myAccountOrders
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout Flow_Sign In DTD with only Primary Info (ALM#5778)
  """ My account feature is decided to be turned off for DTD orders, hence taking out customer type validation"""
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    Then I verify 'sign in to skip this step' is "displayed" in Checkout Page
    When I click on the "sign in to skip this step." link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the page with header "Checkout"
    And  I verify "First Name, Last Name, Address Line 1, Address Line 2, zip-code, Email, Phone Number" values for "<My Account Customer>" are now pre-populated
    And  I click on the "Continue To Shipping Method" button
    And  I select the default shipping option as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | webOrderOrigin |
      | siteNumber     |
      | amountOfSale   |
      | orderType      |
      | shipMethod     |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |

    Examples:
      | ItemCode | ProductName        | Customer            | Credit Card | My Account Customer | Email                | password  |
      | 29935    | Silver Edition III | default_customer_az | Visa        | MY_ACCOUNT_USER_A   | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @web
  @15109
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout Flow_No Sign In link_DT (ALM#15109)
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    Then I should see "Sign-In" is enabled
    When I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select the checkout without install reason "<Reason>"
    And  I click next step for customer information
    Then I verify 'sign in to skip this step' is "not displayed" in Checkout Page
    And  I verify "First Name, Last Name, Email, Phone Number" values for "<Customer>" are now pre-populated

    Examples:
      | ItemCode | ProductName        | Customer          | Reason                              | Email                | password  |
      | 29935    | Silver Edition III | MY_ACCOUNT_USER_A | Make an appointment at a later time | autouser_a@gmail.com | Discount1 |

  @dtd
  @web
  @15037
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout Flow_No Sign In link_DTD (ALM#15037)
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    Then I should see "Sign-In" is enabled
    When I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify 'sign in to skip this step' is "not displayed" in Checkout Page
    And  I verify "First Name, Last Name, Zip, Email, Phone Number" values for "<Customer>" are now pre-populated

    Examples:
      | ItemCode | ProductName        | Checkout | Customer          | Email                | password  |
      | 29935    | Silver Edition III | default  | MY_ACCOUNT_USER_A | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @15010
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Profile Tab_ Add alternate contact Modal (ALM#15010)
  """FAILS if the alternate contact is not present"""
    When I go to the homepage
    Then I verify Join/Sign-in is displayed on "homepage" page
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    Then I should see "Sign-In" is enabled
    When I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I select my account header navigation
    And  I click on the "View my account" link
    Then I am brought to the page with header "<Header>"
    And  I verify "CONTACT INFO" is displayed
    And  I verify "Contact Info Edit Link" element displayed
    And  I verify "Alternate Contact" element displayed
    When I click on the "delete" link
    And  I click on the "No, Keep it" button
    Then I verify contact info for both primary and alternate user
    When I click on the "delete" link
    And  I click on the "Yes, delete it" button
    And  I click on the "add alternate contact" link
    And  I add contact info as "<Alternate Customer>" for "alternate" customer
    And  I enter "<Alternate Email>" into the "E-mail Address" field
    And  I click on the "UPDATE" button
    Then I verify Edit and Delete links are displayed for "alternate" address

    Examples:
      | Header     | Alternate Customer     | Email                | password  | Alternate Email     |
      | My Account | MY_ACCOUNT_USER_QATEST | autouser_a@gmail.com | Discount1 | qatest345@gmail.com |

  @dtd
  @web
  @15110
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout Flow_Sign In DTD with both Primary and alternate Contact Selection (ALM#15110)
    When I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "<Checkout>"
    Then I verify 'sign in to skip this step' is "displayed" in Checkout Page
    When I select "sign in to skip this step." link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the page with header "Checkout"
    And  I verify "First Name, Last Name, Address Line 1, Address Line 2, Zip, Email, Phone Number" values for "<Primary Customer>" are now pre-populated
    When I click the discount tire logo
    And  I select my account header navigation
    And  I click on the "View my account" link
    And  I click on "alternate contact" edit link
    And  I add contact info as "<Alternate Customer>" for "alternate" customer
    And  I enter "<incorrect Alternate Address>" into the "create account address line 1" field
    And  I click on the "Update" button
    And  I click on the "Use USPS Corrected Address" button
    And  I go to the homepage
    And  I select mini cart and "View Cart"
    And  I select the checkout option "<Checkout>"
    Then I verify contact selection drop down displays "Primary Information"
    When I select "Alternate Information" from drop down
    Then I verify "Address Line 1, Address Line 2, Zip, Phone Number" values for "<Alternate Customer>" are now pre-populated

    Examples:
      | ItemCode | ProductName        | Checkout | Primary Customer  | Alternate Customer     | Email                | password  | incorrect Alternate Address |
      | 29935    | Silver Edition III | default  | MY_ACCOUNT_USER_A | MY_ACCOUNT_USER_QATEST | autouser_a@gmail.com | Discount1 | 950 Forrrer Blvd            |

  @dt
  @at
  @web
  @16634
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout Flow_Sign In DT with both Primary and Alternate Contact Selection (ALM#16634)
    When I do a free text search for "<ItemCode>" and hit enter
    Then I should see product detail page with "<ProductName>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "without appointment"
    And  I select install without appointment
    Then I verify "install without appointment" option is enabled on the Checkout page
    And  I verify the reasons listed in the reason dropdown list
    When I select the checkout without install reason "<Reason>"
    Then I verify the reserve appointment message displayed for "<Reason>"
    When I click next step for customer information
    Then I verify 'sign in to skip this step' is "displayed" in Checkout Page
    When I select "sign in to skip this step." link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the page with header "Checkout"
    And  I verify "First Name, Last Name, Email, Phone Number" values for "<Primary Customer>" are now pre-populated
    When I click the discount tire logo
    And  I select my account header navigation
    And  I click on the "View my account" link
    And  I click on "alternate contact" edit link
    And  I add contact info as "<Alternate Customer>" for "alternate" customer
    And  I set "Address Line 1" to "<incorrectAddress 1>"
    And  I click on the "UPDATE" button
    And  I click on the "Use USPS Corrected Address" button
    And  I go to the homepage
    And  I select mini cart and "View Cart"
    And  I select the checkout option "without appointment"
    And  I select install without appointment
    Then I verify "install without appointment" option is enabled on the Checkout page
    And  I verify the reasons listed in the reason dropdown list
    When I select the checkout without install reason "<Reason>"
    And  I click next step for customer information
    Then I verify contact selection drop down displays "Primary Information"
    When I select "Alternate Information" from drop down
    Then I verify " Address Line 1, Address Line 2, Zip Code,Phone Number" values for "<Alternate Customer>" are now pre-populated

    Examples:
      | ItemCode | ProductName        | Alternate Customer     | Reason                              | Email                | password  | Primary Customer  | incorrectAddress 1 |
      | 29935    | Silver Edition III | MY_ACCOUNT_USER_QATEST | Make an appointment at a later time | autouser_a@gmail.com | Discount1 | MY_ACCOUNT_USER_A | 950 Forrerr Blvd   |

  @dt
  @at
  @web
  @6560
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout BOPIS Flow_Sign In DT with only Primary info (ALM#6560)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click next step for customer information
    Then I verify 'sign in to skip this step' is "displayed" in Checkout Page
    When I click on the "sign in to skip this step." link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the page with header "Checkout"
    When I click on the "Continue to Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<BOPISCustomer>"
    And  I place the order for "<BOPISCustomer>"
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode | ProductName        | BOPISCustomer                     | Credit Card            | Email                | password  |
      | 2012 | Honda | Civic | Coupe DX | none     | 29935    | Silver Edition III | DEFAULT_CUSTOMER_BOPIS_VISA       | Visa Bopis             | autouser_a@gmail.com | Discount1 |
      | 2012 | Honda | Civic | Coupe DX | none     | 29935    | Silver Edition III | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis       | autouser_a@gmail.com | Discount1 |
      | 2012 | Honda | Civic | Coupe DX | none     | 29935    | Silver Edition III | DEFAULT_CUSTOMER_BOPIS_DISCOVER   | Discover Bopis         | autouser_a@gmail.com | Discount1 |
      | 2012 | Honda | Civic | Coupe DX | none     | 29935    | Silver Edition III | DEFAULT_CUSTOMER_BOPIS_AMEX       | American Express Bopis | autouser_a@gmail.com | Discount1 |
      | 2012 | Honda | Civic | Coupe DX | none     | 29935    | Silver Edition III | DEFAULT_CUSTOMER_BOPIS_CC1        | CarCareOne Bopis       | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @web
  @14327
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout BOPIS Flow_Sign In DT with Alternate Info (ALM#14327)
  """Failing on customer address validation due to billing address being sent to POS and ESB as the customer address. 'I assert order xml customer node values' will be commented out until resolved. """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click next step for customer information
    Then I verify 'sign in to skip this step' is "displayed" in Checkout Page
    When I click on the "sign in to skip this step." link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I verify contact selection drop down displays "Primary Information"
    When I select "Alternate Information" from drop down
    Then I verify "Email, Phone Number" values for "<Customer>" are now pre-populated
    When I click on the "Continue to Payment" button
    And  I enter payment info and confirm Checkout Summary as "<BOPISCustomer>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I verify "<Email1>" is displayed on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId   |
      | amountOfSale |
      | siteNumber   |
      | orderType    |
      | customerType |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml payment node values
    And  I assert order xml subItems
#    And  I assert order xml customer node values

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode | ProductName        | BOPISCustomer                     | Credit Card      | Email                | password  | Customer               | Email1              |
      | 2012 | Honda | Civic | Coupe DX | none     | 29935    | Silver Edition III | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis | autouser_a@gmail.com | Discount1 | MY_ACCOUNT_USER_QATEST | qatest345@gmail.com |
      | 2012 | Honda | Civic | Coupe DX | none     | 29935    | Silver Edition III | DEFAULT_CUSTOMER_BOPIS_CC1        | CarCareOne Bopis | autouser_a@gmail.com | Discount1 | MY_ACCOUNT_USER_QATEST | qatest345@gmail.com |

  @dt
  @at
  @dtd
  @web
  @6552
  @5768
  @mobile
  @myVehiclesModal
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal_Recent searches duplicates and no recent searches/saved vehicles messaging_user (ALM#5768,6552)
    When I go to the homepage
    Then I verify "Join/Sign In" element displayed
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    Then I verify "default" My Vehicles popup displays add vehicle
    When I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify 'My Vehicles' section displays only "1" vehicles on "Homepage"
    And  I close popup modal
    Then I verify 'My Vehicles' section displays only "1" vehicles on "Homepage"
    And  I verify my vehicle details: "<Year>, <Make>, <Model>, <Trim>" are "displayed" in the 'My Vehicles' section of the fitment grid
    When I open the My Vehicles popup
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    And  I verify "default" My Vehicles popup displays add vehicle
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>"
    Then I verify 'My Vehicles' section displays only "2" vehicles on "Homepage"
    And  I close popup modal
    Then I verify 'My Vehicles' section displays only "2" vehicles on "Homepage"
    And  I verify my vehicle details: "<Year1>, <Make1>, <Model1>, <Trim1>" are "displayed" in the 'My Vehicles' section of the fitment grid
    When I open the My Vehicles popup
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year1>" "<Make1>" "<Model1>" and "<Trim1>"
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "<SizeOption>" fitment box option
    And  I select a fitment option "<TireSize>"
    And  I open the My Vehicles popup
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I close popup modal
    And  I click the discount tire logo
    Then I verify 'My Vehicles' section displays only "3" vehicles on "Homepage"
    And  I verify my vehicle details: "<Year>, <Make>, <Model>, <Trim>" are "displayed" in the 'My Vehicles' section of the fitment grid
    When I open the My Vehicles popup
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | SizeOption | TireSize  | Year1 | Make1     | Model1   | Trim1 | Assembly1 | Email                | password  |
      | 2012 | Honda | Civic | Coupe DX | none     | 15         | 205/65-15 | 2010  | Chevrolet | Corvette | Base  | none      | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @6561
  @mobile
  @myVehiclesModal
  @staggeredExperience
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal_Save Selected wheel Size for Staggered Vehicles (ALM#6561)
    When I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "wheel"
    And  I open the My Vehicles popup
    Then I verify Vehicle Details Banner on "My Vehicles Modal" contains "Edit Vehicle"
    And  I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    Then I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "Non-OE REAR Wheel" fitment banner with "<OELabel>" and "<OERearWheelSize>"
    And  I select edit vehicle link
    And  I "expand" "optional tire and wheel size"
    And  I select a fitment option "<OEFrontWheelSize>\"/<OERearWheelSize>\""
    Then I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "Non-OE FRONT Wheel" fitment banner with "<OELabel1>" and "<OEFrontWheelSize>"
    And  I select edit vehicle link
    And  I "expand" "optional tire and wheel size"
    And  I select a fitment option "<OEFrontWheelSize2>\"/<OERearWheelSize2>\""
    Then I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "Non-OE REAR Wheel" fitment banner with "<OELabel2>" and "<OERearWheelSize2>"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | OELabel | OEFrontWheelSize | OERearWheelSize | FrontSize | OELabel1 | RearSize | OELabel2 | OEFrontWheelSize2 | OERearWheelSize2 |
      | 2010 | Chevrolet | Corvette | Base | none     | O.E.    | 18               | 19              | 18        | +0       | 20       | +1       | 19                | 20               |

  @dt
  @at
  @dtd
  @web
  @16413
  @mobile
  @myVehiclesModal
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicle Modal_Save Selected Tire Size for Staggered Vehicles (ALM#16413)
    When I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I open the My Vehicles popup
    Then I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE REAR Tire" fitment banner with "<OELabel>" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    And  I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE REAR Wheel" fitment banner with "<OELabel>" and "<OERearWheelSize>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I open the My Vehicles popup
    Then I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE FRONT Tire" fitment banner with "<OELabel>" and "<OEFrontTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE FRONT Wheel" fitment banner with "<OELabel>" and "<OEFrontWheelSize>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I open the My Vehicles popup
    Then I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE REAR Tire" fitment banner with "<OELabel>" and "<OERearTireSize>"
    And  I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "OE REAR Wheel" fitment banner with "<OELabel>" and "<OERearWheelSize>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I open the My Vehicles popup
    Then I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "Non-OE set REAR Tire" fitment banner with "<Non-OELabel>" and "<FrontSize>"
    And  I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "Non-OE set REAR Tire" fitment banner with "<Non-OELabel>" and "<RearSize>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I open the My Vehicles popup
    Then I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "Non-OE FRONT Tire" fitment banner with "<Non-OELabel>" and "<OptionalFrontBanner>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I open the My Vehicles popup
    Then I verify "Staggered" Vehicle Details Banner on "My Vehicles Modal" contains "Non-OE REAR Tire" fitment banner with "<Non-OELabel>" and "<OptionalRearBanner>"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | OELabel | OEFrontTireSize | OERearTireSize | OEFrontWheelSize | OERearWheelSize | Non-OELabel | OptionalFrontTire | OptionalRearTire | FrontSize | RearSize | OptionalFrontBanner | OptionalRearBanner |
      | 2010 | Chevrolet | Corvette | Base | none     | O.E     | 245/40-R18      | 285/35-R19     | 18               | 19              | +1          | 225/35-19         | 245/35-20        | 19        | 20       | 225/35-19           | 245/35-20          |

  @dt
  @at
  @dtd
  @web
  @16543
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal_Remove Recent Search (ALM#16543)
    When I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I close popup modal
    And  I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I close popup modal
    And  I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I close popup modal
    And  I open the My Vehicles popup
    And  I remove my "<Year1><Make1><Model1>" vehicle
    Then I verify recent vehicle "<Year1><Make1><Model1>" "not displayed"
    And  I verify 'My Vehicles' section displays only "2" vehicles on "My Vehicles modal"
    And  I verify that My Vehicles displays "<Year2> <Make2> <Model2>" as the current vehicle
    When I remove my "<Year2><Make2><Model2>" vehicle
    Then I verify recent vehicle "<Year2><Make2><Model2>" "not displayed"
    When I select "shop tires" link
    And  I open the My Vehicles popup
    Then I verify that My Vehicles displays "<Year> <Make> <Model> <Trim>" as the current vehicle

    Examples:
      | Year | Make   | Model | Trim      | Year1 | Make1     | Model1   | Year2 | Make2 | Model2 | Trim2    |
      | 2014 | Toyota | Camry | Hybrid SE | 2010  | Chevrolet | Corvette | 2015  | Honda | Accord | Coupe EX |

  @dt
  @at
  @dtd
  @web
  @5757
  @6553
  @15011
  @mobile
  @myVehiclesModal
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal_Save Recent Searches as guest and user (ALM#5757,6553,15011)
    When I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I close popup modal
    And  I open the My Vehicles popup
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly>"
    And  I close popup modal
    And  I open the My Vehicles popup
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year1>" "<Make1>" "<Model1>" and "<Trim1>"
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year3>" "<Make3>" "<Model3>" "<Trim3>" "<Assembly>"
    And  I close popup modal
    And  I open the My Vehicles popup
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year3>" "<Make3>" "<Model3>" and "<Trim3>"
    When I close popup modal
    And  I click the discount tire logo
    Then I am brought to the homepage
    And  I verify "Join/Sign In" element displayed
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    Then I verify "default" My Vehicles popup displays add vehicle
    When I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I close popup modal
    And  I open the My Vehicles popup
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly>"
    And  I close popup modal
    And  I open the My Vehicles popup
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year1>" "<Make1>" "<Model1>" and "<Trim1>"
    When I close popup modal
    And  I select my account header navigation
    And  I click on the "Sign-out" link
    And  I open the My Vehicles popup
    Then I verify 'My Vehicles' section displays only "3" vehicles on "My Vehicles modal"
    When I close popup modal
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I open the My Vehicles popup
    Then I verify 'My Vehicles' section displays only "2" vehicles on "My Vehicles modal"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Year1 | Make1     | Model1   | Trim1 | Year3 | Make3 | Model3 | Trim3    | Email                | password  |
      | 2012 | Honda | Civic | Coupe EX | none     | 2010  | Chevrolet | Corvette | Base  | 2014  | Honda | Accord | Coupe EX | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @16412
  @mobile
  @myVehiclesModal
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal_Vehicle searched as Guest should not persist when Signed In from HOME PAGE (ALM#16412)
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I select my account header navigation
    And  I click on the "Sign-out" link
    And  I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I close popup modal
    And  I open the My Vehicles popup
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year>" "<Make>" "<Model>" and "<Trim>"
    When I close popup modal
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    And  I should see my "first" name displayed on homepage header
    And  I verify My Vehicle in the header displays as "My Vehicles"
    When I open the My Vehicles popup
    Then I verify "default" My Vehicles popup displays add vehicle
    When I close popup modal
    And  I select my account header navigation
    And  I click on the "Sign-out" link
    And  I open the My Vehicles popup
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make   | Model | Trim      | Email                | password  |
      | 2014 | Toyota | Camry | Hybrid SE | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @5761
  @myVehiclesModal
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal_Vehicle searched as Guest should not persist when Signed In from PLP (ALM#5761)
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I select my account header navigation
    And  I click on the "Sign-out" link
    And  I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    And  I verify My Vehicle in the header displays as "<Make> <Model>"
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    And  I verify the PLP header message contains "tire result"
    And  I verify My Vehicle in the header displays as "My Vehicles"
    When I open the My Vehicles popup
    Then I verify "default" My Vehicles popup displays add vehicle
    When I close popup modal
    And  I select my account header navigation
    And  I click on the "Sign-out" link
    And  I open the My Vehicles popup
    Then I verify Vehicle Details Banner on "my vehicle" contains "<Year>" "<Make>" "<Model>" and "<Trim>"

    Examples:
      | Year | Make  | Model | Trim     | Email                | password  |
      | 2012 | Honda | Civic | Coupe EX | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @5759
  @myVehiclesModal
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal_Vehicle searched as Guest should persist when Signed In from CART (ALM#5759)
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I select my account header navigation
    And  I click on the "Sign-out" link
    And  I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify My Vehicle in the header displays as "<Make> <Model>"
    And  I verify the PLP header message contains "tire result"
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify My Vehicle in the header displays as "<Make> <Model>"
    And  I verify the required fees and add-ons sections are expanded
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I verify "Shopping cart" page is displayed
    And  I should see my "first" name displayed on homepage header
    And  I verify My Vehicle in the header displays as "<Make> <Model>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode | Email                | password  |
      | 2012 | Honda | Civic | Coupe EX | none     | 34362    | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @15108
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicle Modal_Sign In(ALM#15108)
    When I open the My Vehicles popup
    Then I verify "Create Account/Sign In" is displayed on "MyVehicles Modal"
    When I select "Create Account/Sign In" link
    Then I verify my account sign-in modal is displayed
    And  I verify that "Sign-In" is disabled by default
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    Then I should see "Sign-In" is enabled
    And  I verify keep me signed-in option is displayed

    Examples:
      | Email                | password  |
      | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @mobile
  @15108
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicle Modal_Sign In_Mobile(ALM#15108)
    When I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    Then I verify "Create Account/Sign In" is displayed on "MyVehicles Modal"
    When I select "Create Account/Sign In" link
    Then I verify my account sign-in modal is displayed
    And  I should see "Email" field is displayed
    And  I should see "Password" field is displayed
    And  I verify that "Sign-In" is disabled by default
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    Then I should see "Sign-In" is enabled
    And  I verify keep me signed-in option is displayed

    Examples:
      | Email                | password  |
      | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @6559
  @mobile
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicle Modal_Create Account_Mobile(ALM#6559)
    When I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    Then I verify "Create Account/Sign In" is displayed on "MyVehicles Modal"
    When I select "Create Account/Sign In" link
    Then I verify my account sign-in modal is displayed
    And  I should see "Email,Password" field is displayed
    And  I verify keep me signed-in option is displayed
    And  I verify that "Sign-In" is disabled by default
    When I select sign-up now
    Then I should see "First Name,Last Name,Password,Phone Type,Address Line 1,Address Line 2,Country,Zip Code,City,State" field is displayed
    And  I verify that "CREATE AN ACCOUNT" is disabled by default
    And  I populate all the required fields for "<Customer>"
    And  I enter my account email address on "homepage" page
    And  I enter my account phone number
    Then I verify "Password" field cannot be empty error message
    When I enter "<password>" in password field
    Then I should see "CREATE AN ACCOUNT" is enabled
    When I click on the "CREATE AN ACCOUNT" button
    Then I verify email sent confirmation message displayed on "account confirmation modal"

    Examples:
      | password  | Customer                |
      | Discount1 | MY_ACCOUNT_NEW_CUSTOMER |

  @dt
  @at
  @dtd
  @web
  @17352
  Scenario Outline:HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal and Tab_Cart Experience_Cart 2 as guest should replace Cart 1 as user (ALM#17352)
    When I go to the homepage
    And  I select "Join/Sign In" link
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I close popup modal
    And  I open the My Vehicles popup
    And  I select "shop tires" link
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select mini cart
    And  I select my account header navigation
    And  I click on the "Sign-out" link
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "none"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select "Join/Sign-in" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select "Join/Sign In" link
    And  I enter my account email address on "homepage" page
    And  I enter my account password
    And  I click on the "Sign-in" button
    Then I verify "Shopping cart" page is displayed
    When I click the discount tire logo
    And  I select mini cart
    Then I verify My Vehicle in the header displays as "Honda Civic"
    And  I verify the added product "<ProductName1>" is displayed in Mini Cart
    When I open the My Vehicles popup
    Then I verify that My Vehicles displays "2012 honda Civic Coupe DX" as the current vehicle
    When I close popup modal
    And  I select my account header navigation
    And  I click on the "View my account" link
    And  I select "My Vehicles" tab
    Then I verify that My Vehicles displays "2012 honda Civic Coupe DX" as the current vehicle
    When I select my account header navigation
    And  I click on the "Sign-out" link
    Then I am brought to the homepage
    When I select "Join/Sign-in" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    And  I verify My Vehicle in the header displays as "Honda Civic"
    When I open the My Vehicles popup
    Then I verify that My Vehicles displays "2012 honda civic coupe dx" as the current vehicle

    Examples:
      | Year | Make   | Model | Trim      | Year1 | Make1 | Model1 | Trim1    | ProductName | ProductName1       | Email                | password  |
      | 2014 | Toyota | Camry | Hybrid SE | 2012  | Honda | Civic  | Coupe DX | Yokohama    | Silver Edition III | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @17351
  Scenario Outline:HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal and Tab_Cart Experience_Cart is not cleared without fitment (ALM#17351)
    When  I do a "homepage" vehicle search for "VEHICLE_NON_STAGGERED_1" vehicle type
    And  I open the "TIRES" navigation link
    And  I click the "Goodyear Tires" menu option
    And  I select shop all from the Product Brand page
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    And  I see a purchase quantity of "<Quantity>"
    When I select "Join/Sign-in" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I verify "Shopping cart" page is displayed
    When I click the discount tire logo
    Then I am brought to the homepage
    When I select mini cart
    Then I verify the added product "<ProductName>" is displayed in Mini Cart
    When I select my account header navigation
    And  I click on the "Sign-out" link
    Then I am brought to the homepage
    And  I select "Join/Sign-in" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I select mini cart
    Then I verify the added product "<ProductName>" is displayed in Mini Cart

    Examples:
      | ProductName | Quantity | Email                | password  |
      | Goodyear    | 4        | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @17810
  @defect
  Scenario Outline:HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal and Tab_Cart Experience_Cart is not cleared with fitment (ALM#17810)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select from the "Brands" filter section, "single" option(s): "<ProductName>"
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    And  I see a purchase quantity of "<Quantity>"
    When I select "Join/Sign-in" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I verify "Shopping cart" page is displayed
    When I click the discount tire logo
    Then I am brought to the homepage
    When I open the My Vehicles popup
    Then I verify that My Vehicles displays "2014 Toyota Camry Hybrid SE" as the current vehicle
    When I close popup modal
    And  I select mini cart
    Then I verify the added product "<ProductName>" is displayed in Mini Cart
    When I select my account header navigation
    And  I click on the "Sign-out" link
    Then I am brought to the homepage
    And  I select "Join/Sign-in" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I open the My Vehicles popup
    Then I verify that My Vehicles displays "2014 Toyota Camry Hybrid SE" as the current vehicle
    When I close popup modal
    And  I select mini cart
    Then I verify the added product "<ProductName>" is displayed in Mini Cart

    Examples:
      | ProductName | Quantity | Year | Make   | Model | Trim      | Email                | password  |
      | Yokohama    | 4        | 2014 | Toyota | Camry | Hybrid SE | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @dtd
  @web
  @16607
  Scenario Outline: HYBRIS_CUSTOMER_MYACCOUNT_My Vehicle Modal_Recent Vehicle Details_Regular Vehicle(ALM#16607)
    When I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I close popup modal
    Then I am brought to the homepage
    When I open the My Vehicles popup
    Then I verify "selected vehicle" element displayed
    And  I verify "Vehicle icon" element displayed
    And  I verify "my vehicles image" element displayed
    And  I verify "Regular" Vehicle Details on "My Vehicles" page contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify "Regular" Vehicle Details on "My Vehicles" page contains "OE Wheel" fitment banner with "<OELabel>" and "<OEWheelSize>"
    And  I verify "edit" is displayed on "MyVehicles Modal"

    Examples:
      | Year | Make  | Model | Trim     | OELabel | OETireSize | OEWheelSize |
      | 2012 | Honda | Civic | Coupe DX | O.E     | 195/65-R15 | 15          |

  @dt
  @at
  @dtd
  @mobile
  @16607
  Scenario Outline: HYBRIS_CUSTOMER_MYACCOUNT_My Vehicle Modal_Recent Vehicle Details_Regular Vehicle_on_mobile(ALM#16607)
    When I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    And  I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I close popup modal
    Then I am brought to the homepage
    When I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    Then I verify "selected vehicle" element displayed
    And  I verify "Vehicle icon" element displayed
    And  I verify "my vehicles image" element displayed
    And  I verify "Regular" Vehicle Details on "My Vehicles" page contains "OE Tire" fitment banner with "<OELabel>" and "<OETireSize>"
    And  I verify "Regular" Vehicle Details on "My Vehicles" page contains "OE Wheel" fitment banner with "<OELabel>" and "<OEWheelSize>"
    And  I verify "edit" is displayed on "MyVehicles Modal"

    Examples:
      | Year | Make  | Model | Trim     | OELabel | OETireSize | OEWheelSize |
      | 2012 | Honda | Civic | Coupe DX | O.E     | 195/65-R15 | 15          |

  @dt
  @at
  @dtd
  @web
  @16612
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal _Verify Recent Vehicle List and clearing Recent searches(ALM#16612)
    When I open the My Vehicles popup
    Then I verify "my vehicles" element displayed
    And  I verify "recently searched vehicles" element displayed
    And  I verify "recent searches" element displayed
    When I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "none"
    And  I close popup modal
    Then I am brought to the homepage
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "No, Keep These Searches" option
    Then I verify "edit" is displayed on "MyVehicles Modal"
    When I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    Then I verify "no recent vehicles" element displayed

    Examples:
      | Year1 | Make1  | Model1 | Trim1     |
      | 2014  | Toyota | Camry  | Hybrid SE |

  @dt
  @at
  @dtd
  @mobile
  @16612
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal _Verify Recent Vehicle List and clearing Recent searches_on_mobile(ALM#16612)
    When I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    Then I verify "my vehicles" element displayed
    And  I verify "recent searches" element displayed
    And  I verify "clear recent searches" element displayed
    When I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "none"
    And  I close popup modal
    Then I am brought to the homepage
    When I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    And  I select Clear Recent Searches and "No, Keep These Searches" option
    Then I verify "edit" is displayed on "MyVehicles Modal"
    When I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    Then I verify "no recent vehicles" element displayed

    Examples:
      | Year1 | Make1  | Model1 | Trim1     |
      | 2014  | Toyota | Camry  | Hybrid SE |

  @dt
  @at
  @dtd
  @web
  @5758
  @12569
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal_Verify_shop_tires_shop_wheels_standard (ALM#5758,ALM#12569)
    When I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I close popup modal
    And  I open the My Vehicles popup
    And  I select "shop tires" link
    Then I verify the PLP header message contains "tire result"
    When I open the My Vehicles popup
    And  I select "shop wheels" link
    Then I verify the PLP header message contains "wheel result"

    Examples:
      | Year | Make      | Model    | Trim      |
      | 2014 | Toyota    | Camry    | Hybrid SE |
      | 2010 | chevrolet | corvette | base      |

  @dt
  @at
  @dtd
  @mobile
  @5758
  @12569
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal_Verify_shop_tires_shop_wheels_on_mobile(ALM#12569)
    When I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    And  I select Add Vehicle
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I close popup modal
    And  I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    And  I select "shop tires" link
    Then I verify "fitment tires tab" element displayed
    When I close popup modal
    And  I click the mobile homepage menu
    And  I click on "My Vehicles" menu link
    And  I select "shop wheels" link
    Then I verify "fitment wheels tab" element displayed

    Examples:
      | Year | Make      | Model    | Trim      |
      | 2014 | Toyota    | Camry    | Hybrid SE |
      | 2010 | chevrolet | corvette | base      |

  @dt
  @at
  @web
  @17576
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout Flow-Create Account with an existing account_BOPIS/ROPIS_Error message validation and ROPIS button is selected by default (ALM#17576)
    When I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify My Vehicle in the header displays as "<Make> <Model>"
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "Not sure of my availability"
    And  I click next step for customer information
    Then I verify "Create an account using my information" is displayed on checkout page
    When I select create an account using my information
    And  I populate all the required fields for "<Customer>"
    And  I enter "<password>" in password field
    And  I click on the "Place Order" button
    And  I enter my account phone number
    Then I am brought to the page with header "Checkout"
    And  I verify "existing email error message" is displayed

    Examples:
      | Year | Make   | Model | Trim      | Customer          | password  |
      | 2014 | Toyota | Camry | Hybrid SE | MY_ACCOUNT_USER_A | Discount1 |

  @dtd
  @web
  @6557
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout Flow-Create Account with an existing account_Error message validation_DTD (ALM#17576)
    When I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "none"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify My Vehicle in the header displays as "<Make> <Model>"
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    Then I verify "Create an account using my information" is displayed on checkout page
    When I select create an account using my information
    And  I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter "<password>" in password field
    And  I click on the "Continue To Shipping Method" button
    Then I verify "existing email error message" is displayed

    Examples:
      | Year | Make   | Model | Trim      | Customer          | password  |
      | 2014 | Toyota | Camry | Hybrid SE | MY_ACCOUNT_USER_C | Discount1 |

  @dt
  @at
  @dtd
  @web
  @19648
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Create Account with existing Email Address_Homepage (ALM#19648)
    When I go to the homepage
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    And  I enter "<First Name>" into the "First Name" field
    And  I enter "<Last Name>" into the "Last Name" field
    And  I enter "<My Email>" in email address field
    And  I enter "<Password>" into the "Password" field
    And  I enter my account phone number
    And  I enter "<Address line1>" into the "create account address line 1" field
    And  I enter "<ZipCode>" into the "create account zipcode" field
    Then I should see "CREATE AN ACCOUNT" is enabled
    When I click on the "CREATE AN ACCOUNT" button
    Then I verify "existing email error message" is displayed

    Examples:
      | My Email             | Password  | First Name | Last Name | Address line1         | ZipCode |
      | autouser_c@gmail.com | Discount1 | Autouser   | QaC       | 20225 N Scottsdale Rd | 85255   |

  @dt
  @at
  @web
  @17984
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout Flow_Create Account_DT_ROPIS with Fitment_AVS (ALM#17984)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I select create an account using my information
    Then I verify create an account using my information checkbox is selected
    When I populate all the required fields for "<Customer>"
    And  I enter my account email address on "checkout" page
    And  I enter my account phone number
    And  I enter "<incorrectAddress>" into the "create account address line 1" field
    And  I enter "<password>" in password field
    Then I verify email sent confirmation message displayed on "checkout page"
    When I click on the "Continue To Payment" button
    Then I verify the "Address Verification" modal is displayed
    When I click on the "Use USPS Corrected Address" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I verify "<zip plus four>" is listed on the order confirmation page
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"
    When I close popup modal
    And  I select my account header navigation
    And  I click on the "Sign-out" link
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click on the "Continue to Customer Details" button
    And  I select create an account using my information
    Then I verify create an account using my information checkbox is selected
    When I populate all the required fields for "<Customer>"
    And  I enter my account email address on "checkout" page
    And  I enter "<incorrectAddress>" into the "create account address line 1" field
    And  I enter "<password>" in password field
    And  I enter my account phone number
    Then I verify email sent confirmation message displayed on "checkout page"
    When I click on the "Continue To Payment" button
    Then I verify the "Address Verification" modal is displayed
    When I click on the "Use USPS Corrected Address" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I verify "<zip plus four>" is listed on the order confirmation page
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Customer                | password  | incorrectAddress        | zip plus four |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | MY_ACCOUNT_NEW_CUSTOMER | Discount1 | 20225 N Scottsadalee Rd | 85255-6456    |

  @dt
  @at
  @web
  @15160
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout Flow_Create Account_DT_FREE TEXT SEARCH_AVS (ALM#15160)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I click on the "Checkout Now" button
    And  I click on the "+ ADD NEW VEHICLE" link
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select mini cart
    And  I click on the "View Cart" button
    And  I click on the "Checkout Now" button
    And  I click on the "Continue to Checkout" button
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click next step for customer information
    And  I select create an account using my information
    And  I populate all the required fields for "<Customer>"
    And  I enter my account email address on "checkout" page
    And  I enter my account phone number
    And  I enter "<password>" in password field
    Then I verify email sent confirmation message displayed on "checkout page"
    When I click on the "Continue To Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I go to the homepage
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ProductName>" on the "cart" page
    When I click on the "Checkout Now" button
    And  I click on the "Continue to Checkout" button
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click next step for customer information
    And  I select create an account using my information
    And  I populate all the required fields for "<Customer>"
    And  I enter my account email address on "checkout" page
    And  I enter my account phone number
    And  I set "Address Line 1" to "<incorrectAddress>"
    And  I enter "<password>" in password field
    And  I click on the "Continue To Payment" button
    Then I verify the "Address Verification" modal is displayed
    When I click on the "Use USPS Corrected Address" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ProductName        | ItemCode | Reason                              | Customer                | password  | incorrectAddress       | zip plus four |
      | 2012 | Honda | Civic | Coupe DX | none     | Silver Edition III | 29935    | Make an appointment at a later time | MY_ACCOUNT_NEW_CUSTOMER | Discount1 | 20225 N Scottsdalee Rd | 85255-6456    |

  @dtd
  @web
  @12561
  @defect
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout Flow_Create Account_DTD_Invalid PSWD_AVS (ALM#12561)
  """ My account feature is decided to be turned off for DTD orders, hence taking out customer type validation"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I populate all the required fields for "<Primary Customer>"
    And  I enter my account email address on "checkout" page
    And  I enter my account phone number
    And  I set "Address Line 1" to "<incorrectAddress>"
    And  I select create an account using my information
    Then I verify create an account using my information checkbox is selected
    And  I set "Address Line 1" to "<incorrectAddress>"
    And  I enter "<password>" into the "Password" field
    Then I verify create an account using my information checkbox is selected
    When I click on the "Continue To Shipping Method" button
    Then I verify the "Address Verification" modal is displayed
    When I click on the "Use USPS Corrected Address" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I verify "<zip plus four>" is listed on the order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | webOrderOrigin |
      | siteNumber     |
      | amountOfSale   |
      | orderType      |
      | shipMethod     |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Customer                    | Primary Customer        | password  | incorrectAddress       | zip plus four |
      | 2012 | Honda | Civic | Coupe DX | none     | DEFAULT_CUSTOMER_BOPIS_VISA | MY_ACCOUNT_NEW_CUSTOMER | Discount1 | 20225 N Scottsdalee Rd | 85255-6456    |

  @dt
  @at
  @dtd
  @web
  @6067
  @18307
  @mobile
  @regression
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Profile_AVS Validation for Edit primary and alternate address (ALM#6067 and ALM#18307)
    When I select my account header navigation
    And  I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    And  I should see my "first" name displayed on homepage header
    When I click on the "View my account" link
    And  I click on "primary contact" edit link
    Then I verify "First Name, Last Name, Address Line 1, Zip Code, City, State" values for "<Customer>" are now pre-populated
    When I enter a zipcode of "<Invalid Zipcode>" in create account modal
    Then I verify "Zip Code" field cannot be empty error message
    When I enter my account zipcode by customer type "<Customer>"
    And  I update my account first name to "<NewfirstName>"
    Then I verify "City, State" values for "<Customer>" are now pre-populated
    When I enter "<incorrect address>" into the "create account address line 1" field
    And  I click on the "update" button
    And  I click on the "Use USPS Corrected Address" button
    Then I verify USPS corrected address display "<Zip>" on "My Account" page for "primary" contact by customer type "<Customer>"
    And  I should see my "<NewfirstName>" name displayed on homepage header
    And  I verify "Alternate Contact" element displayed
    When I click on "alternate contact" edit link
    And  I add contact info as "<Customer1>" for "alternate" customer
    And  I enter "<incorrect address2>" into the "create account address line 1" field
    And  I click on the "update" button
    And  I click on the "Use USPS Corrected Address" button
    Then I verify USPS corrected address display "<ZipAlternate>" on "My Account" page for "alternate" contact by customer type "<Customer1>"
    When I click on "primary contact" edit link
    And  I update my account first name to "<DefaultFirstName>"
    And  I click on the "update" button
    Then I should see my "first" name displayed on homepage header

    Examples:
      | Customer          | Customer1              | Invalid Zipcode | Email                | password  | NewfirstName | DefaultFirstName | Zip        | ZipAlternate | incorrect address      | incorrect address2 |
      | MY_ACCOUNT_USER_A | MY_ACCOUNT_USER_QATEST | 0000            | autouser_a@gmail.com | Discount1 | AutouserNew  | Autouser         | 85255-6456 | 45420-1469   | 20225 N Scottsdalee Rd | 950 Forrerr Blvd   |

  @dt
  @at
  @web
  @17577
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_My ACCOUNT_Checkout_Create Account_BOPIS_AVS (ALM#17577)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I select create an account using my information
    And  I populate all the required fields for "<Primary Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "checkout" page
    And  I enter "<password>" in password field
    Then I verify email sent confirmation message displayed on "checkout page"
    When I click on the "Continue To Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I open the My Vehicles popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I select create an account using my information
    And  I populate all the required fields for "<Primary Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "checkout" page
    And  I enter "<incorrectAddress>" into the "create account address line 1" field
    And  I enter "<password>" in password field
    Then I verify email sent confirmation message displayed on "checkout page"
    When I click on the "Continue To Payment" button
    Then I verify the "Address Verification" modal is displayed
    When I click on the "Use USPS Corrected Address" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I open the My Vehicles popup
    And  I click on the "shop products" link
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click on the "Continue to Customer Details" button
    And  I select create an account using my information
    And  I populate all the required fields for "<Primary Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "checkout" page
    And  I enter "<invalid address>" into the "create account address line 1" field
    And  I enter "<password>" in password field
    Then I verify email sent confirmation message displayed on "checkout page"
    When I click on the "Continue To Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I verify "<zip>" is listed on the order confirmation page
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Customer                    | password  | Primary Customer        | incorrectAddress        | zip plus four | invalid address      | zip   |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | DEFAULT_CUSTOMER_BOPIS_VISA | Discount1 | MY_ACCOUNT_NEW_CUSTOMER | 20225 N Scottsadalee Rd | 85255-6456    | 202564 Scottsdale Rd | 85255 |

  @dt
  @at
  @dtd
  @web
  @14994
  @mobile
  @regression
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Create Account with Field Validation and Account Authentication Email_Homepage (ALM#14994)
    When I go to the homepage
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I should see "First Name,Last Name,Email,Password,Phone Type,Address Line 1,Address Line 2,Country,Zip Code,City,State" field is displayed
    And  I verify that "CREATE AN ACCOUNT" is disabled by default
    When I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    Then I verify "City, State" values for "<Customer>" are now pre-populated
    When I click on the "CREATE AN ACCOUNT" button
    Then I am brought to the page with header "<Header>"
    And  I verify email sent confirmation message displayed on "account confirmation modal"
    And  I verify my email address displayed on account confirmation modal
    And  I verify my email authentication link message displayed on account confirmation modal
    When I select continue shopping option
    Then I verify Join/Sign-in is displayed on "homepage" page
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header

    Examples:
      | Header                                   | Customer                | password  |
      | You've successfully created your account | MY_ACCOUNT_NEW_CUSTOMER | Discount1 |

  @dt
  @at
  @dtd
  @web
  @6559
  @mobile
  @regression
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicle Modal_Create Account(ALM#6559)
    When I open the My Vehicles popup
    Then I verify "Create Account/Sign In" is displayed on "MyVehicles Modal"
    When I select "Create Account/Sign In" link
    Then I verify my account sign-in modal is displayed
    And  I verify keep me signed-in option is displayed
    And  I verify that "Sign-In" is disabled by default
    When I select sign-up now
    Then I should see "First Name,Last Name,Email,Password,Phone Type,Address Line 1,Address Line 2,Country,Zip Code,City,State" field is displayed
    And  I verify that "CREATE AN ACCOUNT" is disabled by default
    When I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    Then I verify "Password" field cannot be empty error message
    When I enter "<password>" in password field
    When I enter "<Invalid Phone Number>" into "Phone Number" field
    Then I verify "Phone number is invalid" error message
    When I enter "<Valid Phone Number>" into "Phone Number" field
    Then I should see "CREATE AN ACCOUNT" is enabled
    When I click on the "CREATE AN ACCOUNT" button
    Then I verify email sent confirmation message displayed on "account confirmation modal"

    Examples:
      | password  | Customer                | Invalid Phone Number | Valid Phone Number |
      | Discount1 | MY_ACCOUNT_NEW_CUSTOMER | 23843209809430824    | 1234567890         |

  @dt
  @at
  @dtd
  @web
  @6555
  @mobile
  @regression
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicle Modal_My Vehicle Recent Searches Add Delete Clear(ALM#6555)
    When I go to the homepage
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    And  I am brought to the homepage
    When I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "none"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I open the My Vehicles popup
    Then I verify 'My Vehicles' section displays only "1" vehicles on "My Vehicles modal"
    And  I verify that My Vehicles displays "<Year1> <Make1> <Model1>" as the current vehicle
    When I close popup modal
    And  I click the discount tire logo
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year2>" "<Make2>" "<Model2>" "<Trim2>" "none"
    And  I open the My Vehicles popup
    Then I verify 'My Vehicles' section displays only "2" vehicles on "My Vehicles modal"
    And  I verify that My Vehicles displays "<Year2> <Make2> <Model2>" as the current vehicle
    When I close popup modal
    And  I click the discount tire logo
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year3>" "<Make3>" "<Model3>" "<Trim3>" "none"
    And  I open the My Vehicles popup
    Then I verify 'My Vehicles' section displays only "3" vehicles on "My Vehicles modal"
    And  I verify that My Vehicles displays "<Year3> <Make3> <Model3>" as the current vehicle
    When I close popup modal
    And  I click the discount tire logo
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year4>" "<Make4>" "<Model4>" "<Trim4>" "none"
    And  I open the My Vehicles popup
    Then I verify 'My Vehicles' section displays only "3" vehicles on "My Vehicles modal"
    And  I verify that My Vehicles displays "<Year4> <Make4> <Model4>" as the current vehicle
    And  I verify vehicle "<Year1> <Make1> <Model1>" "not displayed" in "Recent Searches" section
    When I close popup modal
    And  I remove my "<Year2> <Make2> <Model2>" vehicle
    Then I verify vehicle "<Year2> <Make2> <Model2>" "not displayed" in "Recent Searches" section
    When I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    Then I verify "default" My Vehicles popup displays add vehicle
    When I close popup modal
    Then I verify My Vehicle in the header displays as "My Vehicles"

    Examples:
      | Year1 | Make1 | Model1 | Trim1    | Year2 | Make2     | Model2   | Trim2 | Year3 | Make3 | Model3 | Trim3    | Year4 | Make4  | Model4 | Trim4     | Email                | password  |
      | 2012  | Honda | Civic  | Coupe DX | 2010  | Chevrolet | Corvette | Base  | 2015  | Honda | Accord | Coupe EX | 2014  | Toyota | Camry  | Hybrid SE | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @web
  @17581
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout_ My Account turned off_ROPIS_AVS (ALM#17581)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I click on the "Continue to Customer Details" button
    And  I populate all the required fields for "<Customer>"
    And  I set "Address Line 1" to "<incorrectAddress 1>"
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I verify the "Address Verification" modal is displayed
    When I click on the "Use USPS Corrected Address" button
    Then I verify "<ZipCodePlusFour>" is listed on the order confirmation page
    When I click on the "<Make> <Model>" link
    And  I select "shop tires" link
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I click on the "Continue to Customer Details" button
    And  I populate all the required fields for "<Customer>"
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I verify "<ZipCodePlusFour>" is listed on the order confirmation page
    When I click on the "<Make> <Model>" link
    And  I select "shop tires" link
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I click on the "Continue to Customer Details" button
    And  I populate all the required fields for "<Customer>"
    And  I enter "invalid" address by customer type "<Customer>"
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I verify "<Zip>" is listed on the order confirmation page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Customer          | Zip   | incorrectAddress 1      | ZipCodePlusFour | password  |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | MY_ACCOUNT_USER_A | 85255 | 20225 N scottsdaleee Rd | 85255-6456      | Discount1 |

  @dt
  @at
  @web
  @17695
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout- My Account Turned off_BOPIS_AVS (ALM#17695)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I click on the "Continue to Customer Details" button
    And  I populate all the required fields for "<Customer>"
    And  I enter "<FirstName>" into the "First Name" field
    And  I enter "<LastName>" into the "Last Name" field
    And  I enter "<Email>" into the "E-mail Address" field
    And  I enter "<Phone>" into the "Phone" field
    And  I click on the "Continue To Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify "<NewZip>" is listed on the order confirmation page
    When I click on the "<Make> <Model>" link
    And  I select "shop tires" link
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I click on the "Continue to Customer Details" button
    And  I populate all the required fields for "<Customer>"
    And  I enter "<FirstName>" into the "First Name" field
    And  I enter "<LastName>" into the "Last Name" field
    And  I enter "<Email>" into the "E-mail Address" field
    And  I enter "<Phone>" into the "Phone" field
    And  I click on the "Continue To Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Customer>"
    And  I enter "<Address 1>" into the "Address Line 1" field
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Primary Customer            | Customer                    | NewZip     | Address 1            | FirstName | LastName | Email                | Phone      |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | DEFAULT_CUSTOMER_BOPIS_VISA | DEFAULT_CUSTOMER_BOPIS_VISA | 33606-1437 | 100 N ggilchrist Ave | IMA       | TEST     | autouser_a@gmail.com | 1112223334 |

  @dt
  @at
  @web
  @bvt
  @6174
  @mobile
  @regression
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicle Modal_Recent to saved vehicle in order selected vehicle ten vehicles limit messaging delete saved vehicle(ALM#6174)
    When I go to the homepage
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I select sign-up now
    And  I populate all the required fields for "<Primary Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    And  I click on the "CREATE AN ACCOUNT" button
    And  I close popup modal
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    Then I verify the reserve appointment message displayed for "Make an appointment at a later time"
    When I click on the "Continue to Customer Details" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    When I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>"
    And  I select a fitment option "<FitmentOption>"
    And  I click the discount tire logo
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year2>" "<Make2>" "<Model2>" "<Trim2>" "<Assembly2>"
    And  I select a fitment option "<FitmentOption>"
    And  I click the discount tire logo
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year3>" "<Make3>" "<Model3>" "<Trim3>" "<Assembly3>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I click the discount tire logo
    And  I open the My Vehicles popup
    And  I save all vehicles in the recent search
    And  I close popup modal
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year4>" "<Make4>" "<Model4>" "<Trim4>" "<Assembly4>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I click the discount tire logo
    And  I open the My Vehicles popup
    And  I save all vehicles in the recent search
    And  I close popup modal
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "19" fitment box option
    And  I click on the "<Size1>" link
    And  I select a fitment option "<FitmentOption>"
    And  I click the discount tire logo
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "20" fitment box option
    And  I click on the "<Size2>" link
    And  I select a fitment option "<FitmentOption>"
    And  I click the discount tire logo
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year6>" "<Make6>" "<Model6>" "<Trim6>" "<Assembly6>"
    And  I "expand" "optional tire and wheel size"
    And  I select the "20" fitment box option
    And  I click on the "<Size3>" link
    And  I select a fitment option "<FitmentOption>"
    And  I click the discount tire logo
    And  I open the My Vehicles popup
    And  I save all vehicles in the recent search
    And  I close popup modal
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year9>" "<Make9>" "<Model9>" "<Trim9>" "<Assembly9>"
    And  I select a fitment option "<FitmentOption>"
    And  I click the discount tire logo
    And  I open the My Vehicles popup
    And  I save all vehicles in the recent search
    And  I close popup modal
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year10>" "<Make10>" "<Model10>" "<Trim10>" "<Assembly10>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    Then I verify the reserve appointment message displayed for "Make an appointment at a later time"
    When I click on the "Continue to Customer Details" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    And  I go to the homepage
    And  I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "10"

    Examples:
      | FitmentOption | Primary Customer        | Bopis Customer              | Reason                              | Year | Make   | Model  | Trim  | Assembly       | Year1 | Make1     | Model1   | Trim1 | Assembly1 | Year2 | Make2      | Model2  | Trim2 | Assembly2 | Year3 | Make3 | Model3 | Trim3 | Assembly3 | Size1     | Size2     | Year4 | Make4 | Model4 | Trim4    | Assembly4 | Year6 | Make6 | Model6 | Trim6    | Assembly6 | Size3     | Year9 | Make9  | Model9 | Trim9     | Assembly9 | Year10 | Make10 | Model10 | Trim10     | Assembly10 | Email                | password  |
      | tire          | MY_ACCOUNT_NEW_CUSTOMER | DEFAULT_CUSTOMER_BOPIS_VISA | Make an appointment at a later time | 2015 | Nissan | Altima | Sedan | 215 /55 R17 SL | 2010  | Chevrolet | Corvette | Base  | none      | 2013  | Arctic Cat | 1000 XT | ATV   | none      | 2013  | Ford  | Escape | S AWD | none      | 235/40-18 | 265/30-19 | 2012  | Honda | Civic  | Coupe DX | none      | 2015  | Honda | Accord | Coupe EX | none      | 245/35-20 | 2014  | Toyota | Camry  | Hybrid SE | none      | 2015   | Ram    | 3500    | Dually 4WD | none       | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @web
  @17811
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Order History - Order Details_Regular Vehicle (ALM#17811)
    When I go to the homepage
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "<First Name>" name displayed on homepage header
    And  I am brought to the homepage
    When I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "none"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<BOPISCustomer>"
    And  I place the order for "<BOPISCustomer>"
    Then I am brought to the order confirmation page
    And  I save order number from order confirmation page in a list
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Orders" tab
    Then I verify order number on order history summary page on "Orders" tab
    When I click on order view details link in order history summary page
    Then I should see text "back to my orders" present in the page source
    And  I verify the vehicle "<Year1> <Make1> <Model1> <Trim1>" is displayed on "Order History Detail" page
    And  I should see product "<ItemCode>" on the "Order History Detail" page
    And  I verify customer "<MY_Account_user>" details are listed on the "Order History Detail" page
    And  I verify order number from order confirmation page with order history detail page
    When I click on the "back to my orders" link
    Then I am brought to the page with header "My Orders"

    Examples:
      | Year1 | Make1 | Model1 | Trim1    | ItemCode | ProductName | BOPISCustomer               | Year1 | Make1 | Model1 | Trim1    | MY_Account_user                | Email                      | password  | First Name |
      | 2012  | Honda | Civic  | Coupe DX | 34302    | FP0612 A/S  | DEFAULT_CUSTOMER_BOPIS_VISA | 2012  | Honda | Civic  | Coupe DX | MY_ACCOUNT_REGISTERED_CUSTOMER | automationtestdt@gmail.com | Discount1 | Neha       |

  @dt
  @at
  @web
  @5864
  @5776
  @mobile
  @regression
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Standard and staggered recent searches (ALM#5864 and ALM#5776)
    When I go to the homepage
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    And  I am brought to the homepage
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "All" saved vehicles
    And  I click on the "+ Add Vehicle" button
    And  I do a "default" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "none"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I close popup modal
    And  I open the My Vehicles popup
    Then I verify 'My Vehicles' section displays only "1" vehicles on "My Vehicles modal"
    And  I verify that My Vehicles displays "<Year1> <Make1> <Model1>" as the current vehicle
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PLP"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year1>" "<Make1>" "<Model1>" and "<Trim1>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    When I select edit vehicle link
    Then I should see the fitment panel with vehicle "<Year1> <Make1>"
    When I close popup modal
    Then I verify the PLP header message contains "tire result"
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These wheels fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PLP"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year1>" "<Make1>" "<Model1>" and "<Trim1>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    When I select edit vehicle link
    Then I should see the fitment panel with vehicle "<Year1> <Make1>"
    When I close popup modal
    Then I verify the PLP header message contains "wheel result"
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "All" saved vehicles
    And  I click on the "+ Add Vehicle" button
    And  I do a "default" vehicle search with details "<Year2>" "<Make2>" "<Model2>" "<Trim2>" "<Assembly>"
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I open the My Vehicles popup
    Then I verify 'My Vehicles' section displays only "1" vehicles on "My Vehicles modal"
    And  I verify that My Vehicles displays "<Year2> <Make2> <Model2>" as the current vehicle
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I open the My Vehicles popup
    Then I verify 'My Vehicles' section displays only "1" vehicles on "My Vehicles modal"
    And  I verify that My Vehicles displays "<Year2> <Make2> <Model2>" as the current vehicle
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These tires fit your vehicle"
    And  I verify the PLP header message contains "tire result"
    And  I verify "Vehicle Details Banner" image is displayed on "PLP"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year2>" "<Make2>" "<Model2>" and "<Trim2>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify the PLP header message contains "tire result"
    When I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the "PLP" banner color is "Green"
    And  I verify the message on the "PLP" banner contains "These wheels fit your vehicle"
    And  I verify "Vehicle Details Banner" image is displayed on "PLP"
    And  I verify Vehicle Details Banner on "PLP" contains "<Year2>" "<Make2>" "<Model2>" and "<Trim2>"
    And  I verify Vehicle Details Banner on "PLP" contains "Edit Vehicle"
    And  I verify the PLP header message contains "wheel result"

    Examples:
      | Year1 | Make1 | Model1 | Trim1    | Year2 | Make2 | Model2 | Trim2 | Email                | password  | Assembly       |
      | 2012  | Honda | Civic  | Coupe DX | 2014  | BMW   | 640i   | Coupe | autouser_a@gmail.com | Discount1 | 245 /35 R20 XL |

  @dt
  @at
  @dtd
  @web
  @6070
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Create Account_AVS (ALM#6070)
    When I go to the homepage
    And  I select "Join/Sign In" link
    And  I select sign-up now
    And  I populate all the required fields for "<Primary Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    And  I set "Address Line 1" to "<incorrectAddress>"
    And  I enter "<password>" in password field
    And  I click on the "CREATE AN ACCOUNT" button
    Then I verify the "Address Verification" modal is displayed
    When I click on the "Use USPS Corrected Address" button
    Then I verify email sent confirmation message displayed on "account confirmation modal"
    When I go to the homepage
    And  I select "Join/Sign In" link
    And  I select sign-up now
    And  I populate all the required fields for "<Primary Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    And  I click on the "CREATE AN ACCOUNT" button
    Then I verify email sent confirmation message displayed on "account confirmation modal"
    When I go to the homepage
    And  I select "Join/Sign In" link
    And  I select sign-up now
    And  I populate all the required fields for "<Primary Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    And  I set "Address Line 1" to "<invalidAddress>"
    And  I click on the "CREATE AN ACCOUNT" button
    Then I verify email sent confirmation message displayed on "account confirmation modal"

    Examples:
      | Primary Customer        | incorrectAddress       | invalidAddress           | password  |
      | MY_ACCOUNT_NEW_CUSTOMER | 20225 N scottssdale Rd | 250242 N Scottsdale road | Discount1 |

  @dtd
  @web
  @19273
  Scenario Outline:HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout Flow_Contact Info drop down is not displaying on shipping details page (ALM#19273)
    When I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "All" saved vehicles
    And  I close popup modal
    And  I select my account header navigation
    And  I click on the "View my account" link
    Then I should see my "first" name displayed on homepage header
    And  I verify "CONTACT INFO" is displayed
    And  I verify "Alternate Contact" element displayed
    When I click the discount tire logo
    And  I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    Then I verify contact selection drop down displays "Primary Information"
    And  I verify "First Name, Last Name, Address Line 1, Address Line 2, Zip Code, Email, Phone Number" values for "<Customer>" are now pre-populated
    When I click on the "Continue To Shipping Method" button
    And  I click on the "Edit Shipping Details" link
    Then I verify contact selection drop down displays "Primary Information"
    When I select "Alternate Information" from drop down
    Then I verify "Address Line 1, Address Line 2, Zip Code, Email, Phone Number" values for "<Alternate Customer>" are now pre-populated
    And  I click on the "Edit Shipping Details" link
    Then I verify contact selection drop down displays "Primary Information"
    And  I verify "Address Line 1, Address Line 2, Zip Code, Email, Phone Number" values for "<Customer>" are now pre-populated

    Examples:
      | ItemCode | Customer          | Email                | password  | Alternate Customer     |
      | 29935    | MY_ACCOUNT_USER_A | autouser_a@gmail.com | Discount1 | MY_ACCOUNT_USER_QATEST |

  @dt
  @at
  @web
  @17580
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Order History- Summary Page Pagination(ALM#17580)
  """FAILS due to orders page is broken waiting for fix"""
    When I select "Join/Sign In" link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Orders" tab
    Then I verify there are sufficient number of orders on order history tab
    And  I verify that each page displays 10 items and total number of pages is equal to total count / 10 on "Orders" page
    When I go to the "next" page of product list results
    Then I verify that each page displays 10 items and total number of pages is equal to total count / 10 on "Orders" page
    When I go to the "next" page of product list results
    Then I verify that each page displays 10 items and total number of pages is equal to total count / 10 on "Orders" page
    When I go to the "previous" page of product list results
    Then I verify that each page displays 10 items and total number of pages is equal to total count / 10 on "Orders" page
    When I go to the "Last" page of product list results
    And  I go to the "First" page of product list results

    Examples:
      | Email                | password  |
      | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @web
  @17989
  Scenario Outline:HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Modal_Edit Vehicle (ALM#17989)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I open the My Vehicles popup
    And  I select edit vehicle link
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I click on the "Year" button
    And  I click on the "<Year1>" button
    And  I click on the "Make" button
    And  I click on the "<Make1>" button
    And  I click on the "Model" button
    And  I click on the "<Model1>" button
    And  I click on the "Trim" button
    And  I click on the "<Trim1>" button
    And  I close popup modal
    And  I open the My Vehicles popup
    Then I verify that My Vehicles displays "<Year1> <Make1> <Model1> <Trim1>" as the current vehicle
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year2>" "<Make2>" "<Model2>" "<Trim2>" "none"
    And  I close popup modal
    And  I open the My Vehicles popup
    Then I verify 'My Vehicles' section displays only "2" vehicles on "My Vehicles modal"
    And  I verify that My Vehicles displays "<Year2> <Make2> <Model2> <Trim2>" as the current vehicle

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Year1 | Make1 | Model1 | Trim1 | Year2 | Make2 | Model2 | Trim2    |
      | 2012 | Honda | Civic | Coupe DX | none     | 2014  | Acura | ILX    | 2.0L  | 2011  | Honda | Civic  | Coupe EX |

  @dt
  @at
  @dtd
  @web
  @19620
  @myAccountCreateAccount
  @myAccountCreateAccountPLP
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Field Validation and Create Account on PLP (ALM#19620)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I should see "First Name,Last Name,Email,Password,Phone Type,Address Line 1,Address Line 2,Country,Zip Code,City,State" field is displayed
    And  I verify that "CREATE AN ACCOUNT" is disabled by default
    And  I verify email validation error message should display for all invalid email addresses
    And  I verify the password requirement validation for passed progress steps
    When I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    Then I verify "City, State" values for "<Customer>" are now pre-populated
    When I click on the "CREATE AN ACCOUNT" button
    Then I am brought to the page with header "<Header>"
    And  I verify email sent confirmation message displayed on "account confirmation modal"
    And  I verify my email address displayed on account confirmation modal
    And  I verify my email authentication link message displayed on account confirmation modal
    When I select continue shopping option
    Then I verify Join/Sign-in is displayed on "PLP" page
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<Password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Header                                   | Customer                | Password  |
      | 2012 | Honda | Civic | Coupe DX | none     | You've successfully created your account | MY_ACCOUNT_NEW_CUSTOMER | Discount1 |

  @dt
  @at
  @dtd
  @web
  @19621
  @myAccountCreateAccount
  @myAccountCreateAccountPDP
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Field Validation and Create Account on PDP (ALM#19621)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the "First" product result image on "PLP" page
    Then I verify quantity value "4" on PDP page
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I should see "First Name,Last Name,Email,Password,Phone Type,Address Line 1,Address Line 2,Country,Zip Code,City,State" field is displayed
    And  I verify that "CREATE AN ACCOUNT" is disabled by default
    When I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    Then I verify "City, State" values for "<Customer>" are now pre-populated
    When I click on the "CREATE AN ACCOUNT" button
    Then I am brought to the page with header "<Header>"
    And  I verify email sent confirmation message displayed on "account confirmation modal"
    And  I verify my email address displayed on account confirmation modal
    And  I verify my email authentication link message displayed on account confirmation modal
    When I select continue shopping option
    Then I verify Join/Sign-in is displayed on "PDP" page
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<Password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Header                                   | Customer                | FitmentOption | Password  |
      | 2012 | Honda | Civic | Coupe DX | none     | You've successfully created your account | MY_ACCOUNT_NEW_CUSTOMER | tire          | Discount1 |

  @dt
  @at
  @dtd
  @web
  @19628
  @myAccountCreateAccount
  @myAccountCreateAccountCartPage
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Field Validation and Create Account on shopping cart page (ALM#19628)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I should see "First Name,Last Name,Email,Password,Phone Type,Address Line 1,Address Line 2,Country,Zip Code,City,State" field is displayed
    And  I verify that "CREATE AN ACCOUNT" is disabled by default
    When I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    Then I verify "City, State" values for "<Customer>" are now pre-populated
    When I click on the "CREATE AN ACCOUNT" button
    Then I am brought to the page with header "<Header>"
    And  I verify email sent confirmation message displayed on "account confirmation modal"
    And  I verify my email address displayed on account confirmation modal
    And  I verify my email authentication link message displayed on account confirmation modal
    When I select continue shopping option
    Then I verify Join/Sign-in is displayed on "PDP" page
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<Password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Header                                   | Customer                | FitmentOption | Password  |
      | 2012 | Honda | Civic | Coupe DX | none     | You've successfully created your account | MY_ACCOUNT_NEW_CUSTOMER | tire          | Discount1 |

  @dt
  @at
  @dtd
  @web
  @19622
  @myAccountCreateAccount
  @myAccountCreateAccountComaparePage
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Field Validation and Create Account on compare page (ALM#19622)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I select the first "2" results to compare
    And  I click the compare products Compare button
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I should see "First Name,Last Name,Email,Password,Phone Type,Address Line 1,Address Line 2,Country,Zip Code,City,State" field is displayed
    And  I verify that "CREATE AN ACCOUNT" is disabled by default
    When I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    Then I verify "City, State" values for "<Customer>" are now pre-populated
    When I click on the "CREATE AN ACCOUNT" button
    Then I am brought to the page with header "<Header>"
    And  I verify email sent confirmation message displayed on "account confirmation modal"
    And  I verify my email address displayed on account confirmation modal
    And  I verify my email authentication link message displayed on account confirmation modal
    When I select continue shopping option
    Then I verify Join/Sign-in is displayed on "PDP" page
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<Password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Header                                   | Customer                | FitmentOption | Password  |
      | 2012 | Honda | Civic | Coupe DX | none     | You've successfully created your account | MY_ACCOUNT_NEW_CUSTOMER | tire          | Discount1 |

  @dt
  @at
  @dtd
  @web
  @19623
  @myAccountCreateAccount
  @myAccountCreateAccountStoreLocatorPage
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Field Validation and Create Account on store locator page (ALM#19623)
    When I open the Store Locator page
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I should see "First Name,Last Name,Email,Password,Phone Type,Address Line 1,Address Line 2,Country,Zip Code,City,State" field is displayed
    And  I verify that "CREATE AN ACCOUNT" is disabled by default
    When I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    Then I verify "City, State" values for "<Customer>" are now pre-populated
    When I click on the "CREATE AN ACCOUNT" button
    Then I am brought to the page with header "<Header>"
    And  I verify email sent confirmation message displayed on "account confirmation modal"
    And  I verify my email address displayed on account confirmation modal
    And  I verify my email authentication link message displayed on account confirmation modal
    When I select continue shopping option
    Then I verify Join/Sign-in is displayed on "PDP" page
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<Password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header

    Examples:
      | Header                                   | Customer                | Password  |
      | You've successfully created your account | MY_ACCOUNT_NEW_CUSTOMER | Discount1 |

  @dt
  @at
  @dtd
  @web
  @19624
  @myAccountCreateAccount
  @myAccountCreateAccountStoreDetailsPage
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Field Validation and Create Account on store details page (ALM#19624)
    When I click on "My Store" title
    Then I verify the "My Store" popup contains controls: "CHANGE STORE, STORE DETAILS, SCHEDULE APPOINTMENT"
    When I click on Store details button in My Store popup
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I should see "First Name,Last Name,Email,Password,Phone Type,Address Line 1,Address Line 2,Country,Zip Code,City,State" field is displayed
    And  I verify that "CREATE AN ACCOUNT" is disabled by default
    When I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    Then I verify "City, State" values for "<Customer>" are now pre-populated
    When I click on the "CREATE AN ACCOUNT" button
    Then I am brought to the page with header "<Header>"
    And  I verify email sent confirmation message displayed on "account confirmation modal"
    And  I verify my email address displayed on account confirmation modal
    And  I verify my email authentication link message displayed on account confirmation modal
    When I select continue shopping option
    Then I verify Join/Sign-in is displayed on "PDP" page
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<Password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header

    Examples:
      | Header                                   | Customer                | Password  |
      | You've successfully created your account | MY_ACCOUNT_NEW_CUSTOMER | Discount1 |

  @dt
  @at
  @dtd
  @web
  @19625
  @myAccountCreateAccount
  @myAccountCreateAccountServiceAppointments
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Field Validation and Create Account on service appointments (ALM#19625)
    When I schedule an appointment for my current store
    Then I am brought to the page with header "Service Appointment"
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I should see "First Name,Last Name,Email,Password,Phone Type,Address Line 1,Address Line 2,Country,Zip Code,City,State" field is displayed
    And  I verify that "CREATE AN ACCOUNT" is disabled by default
    When I populate all the required fields for "<Customer>"
    And  I enter my account email address on "homepage" page
    Then I verify "City, State" values for "<Customer>" are now pre-populated
    When I click on the "CREATE AN ACCOUNT" button
    Then I am brought to the page with header "<Header>"
    And  I verify email sent confirmation message displayed on "account confirmation modal"
    And  I verify my email address displayed on account confirmation modal
    And  I verify my email authentication link message displayed on account confirmation modal
    When I select continue shopping option
    Then I verify Join/Sign-in is displayed on "PDP" page
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<Password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header

    Examples:
      | Header                                   | Customer                | Password  |
      | You've successfully created your account | MY_ACCOUNT_NEW_CUSTOMER | Discount1 |

  @dt
  @at
  @dtd
  @web
  @19626
  @myAccountCreateAccount
  @myAccountCreateAccountTiresPage
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Field Validation and Create Account on TIRES (ALM#19626)
    When I open the "TIRES" navigation link
    And  I click the "<Tire Section>" View All link in the header
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I should see "First Name,Last Name,Email,Password,Phone Type,Address Line 1,Address Line 2,Country,Zip Code,City,State" field is displayed
    And  I verify that "CREATE AN ACCOUNT" is disabled by default
    When I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    Then I verify "City, State" values for "<Customer>" are now pre-populated
    When I click on the "CREATE AN ACCOUNT" button
    Then I am brought to the page with header "<Header>"
    And  I verify email sent confirmation message displayed on "account confirmation modal"
    And  I verify my email address displayed on account confirmation modal
    And  I verify my email authentication link message displayed on account confirmation modal
    When I select continue shopping option
    Then I verify Join/Sign-in is displayed on "PDP" page
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<Password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header

    Examples:
      | Header                                   | Customer                | Tire Section | Password  |
      | You've successfully created your account | MY_ACCOUNT_NEW_CUSTOMER | Tire Brand   | Discount1 |
      | You've successfully created your account | MY_ACCOUNT_NEW_CUSTOMER | Tire Type    | Discount1 |
      | You've successfully created your account | MY_ACCOUNT_NEW_CUSTOMER | Vehicle Type | Discount1 |

  @dt
  @at
  @web
  @19572
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout Flow_Sign In DT with only Primary Info switch to ROPIS (ALM#19572)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click next step for customer information
    Then I verify 'sign in to skip this step' is "displayed" in Checkout Page
    When I select "sign in to skip this step." link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<Password>" in password field
    And  I click on the "Sign-in" button
    Then I should see "First Name" field is displayed
    And  I am brought to the page with header "Checkout"
    And  I verify "First Name, Last Name, Address Line 1, Address Line 2, Zip Code, Email, Phone Number" values for "<Customer>" are now pre-populated
    When I enter "<Email1>" into the "E-mail Address" field
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    And  I verify "<Email1>" is displayed on order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | tax         |
      | productCode |
      | discount    |
    And  I assert order xml customer node values
    And  I assert order xml subItems
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Header      | Reason                              | Customer          | Email                | Password  | Email1                 |
      | 2012 | Honda | Civic | Coupe DX | none     | tire result | Make an appointment at a later time | MY_ACCOUNT_USER_A | autouser_a@gmail.com | Discount1 | autouser_abc@gmail.com |

  @dt
  @at
  @dtd
  @web
  @5773
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Add Specific URL Routing for My Account Tabs (ALM#5773)
    When I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I select my account header navigation
    And  I click on the "View My Account" link
    Then I am brought to the page with path "/my-account/profile"
    When I select "My Vehicles" tab
    Then I am brought to the page with path "/my-account/vehicles"
    When I select "Orders" tab
    Then I am brought to the page with path "/my-account/orders"
    When I select "Appointments" tab
    Then I am brought to the page with path "/my-account/appointments"

    Examples:
      | Email                | password  |
      | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @web
  @19630
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Checkout Flow_The values by default should always be Primary on the Checkout Page (ALM#19630)
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    Then I should see my "first" name displayed on homepage header
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    Then I verify contact selection drop down displays "Primary Information"
    And  I verify "Email, Phone Number, Address Line 1, Zip, City, State" values for "<Customer>" are now pre-populated
    When I select "Alternate Information" from drop down
    Then I verify "Email, Phone Number, Address Line 1, Zip, City, State" values for "<Alternate Customer>" are now pre-populated
    When I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I open the My Vehicles popup
    And  I select "shop tires" link
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    Then I verify contact selection drop down displays "Primary Information"
    And  I verify "Email, Phone Number" values for "<Customer>" are now pre-populated
    When I select "Alternate Information" from drop down
    Then I verify "Email, Phone Number" values for "<Alternate Customer>" are now pre-populated
    When I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Header      | Reason                              | Email                | password  | Alternate Customer     | Customer          | Credit Card | Bopis Customer              |
      | 2012 | Honda | Civic | Coupe DX | none     | tire result | Make an appointment at a later time | autouser_a@gmail.com | Discount1 | MY_ACCOUNT_USER_QATEST | MY_ACCOUNT_USER_A | Visa        | DEFAULT_CUSTOMER_BOPIS_VISA |

  @dt
  @at
  @web
  @5777
  Scenario Outline: My Vehicles Tab - Shop Tires & Shop Wheels from Saved Vehicles_standard (ALM#5777)
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    And  I populate all the required fields for "<Customer>"
    And  I enter my account email address on "homepage" page
    And  I click on the "CREATE AN ACCOUNT" button
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I open the My Vehicles popup
    And  I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year1>" "<Make1>" "<Model1>" "<Trim1>" "<Assembly1>"
    And  I select a fitment option "tire"
    And  I open the My Vehicles popup
    And  I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "<Year2>" "<Make2>" "<Model2>" "<Trim2>" "<Assembly2>"
    And  I close popup modal
    And  I open the My Vehicles popup
    And  I save all vehicles in the recent search
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"
    And  I verify "<Year1>" "<Make1>" "<Model1>" "<Trim1>" vehicle is "present" in the saved vehicles at position "2"
    And  I verify "<Year2>" "<Make2>" "<Model2>" "<Trim2>" vehicle is "present" in the saved vehicles at position "3"
    When I select "shop tires" for "<Year1>" "<Make1>" vehicle
    And  I open the My Vehicles popup
    Then I verify that My Vehicles displays "<Year1> <Make1>" as the current vehicle
    When I close popup modal
    And  I select my account header navigation
    And  I click on the "View My Account" link
    And  I select "My Vehicles" tab
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"
    And  I verify "<Year1>" "<Make1>" "<Model1>" "<Trim1>" vehicle is "present" in the saved vehicles at position "2"
    And  I verify "<Year2>" "<Make2>" "<Model2>" "<Trim2>" vehicle is "present" in the saved vehicles at position "3"
    And  I verify that My Vehicles displays "<Year1> <Make1>" as the current vehicle

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Year1 | Make1  | Model1 | Trim1      | Assembly1                           | Year2 | Make2  | Model2 | Trim2     | Assembly2 | Email                | password  | Customer                |
      | 2012 | Honda | Civic | Coupe DX | none     | 2010  | Nissan | 370Z   | Coupe Base | F 245 /40 R19 SL - R 275 /35 R19 SL | 2014  | Toyota | Camry  | Hybrid SE | none      | autouser_a@gmail.com | Discount1 | MY_ACCOUNT_NEW_CUSTOMER |

  @dt
  @at
  @web
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  @ropisOrderWithAppointmentPrimaryContact
  Scenario Outline: Create account scenario with ROPIS and with appointment for primary contact
    When I go to the homepage
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    And  I populate all the required fields for "<New Customer>"
    And  I enter "<Existing Email>" in email address field
    Then I should see "CREATE AN ACCOUNT" is enabled
    When I click on the "CREATE AN ACCOUNT" button
    Then I verify "existing email error message" is displayed
    When I enter my account email address on "homepage" page
    And  I enter my account phone number
    And  I enter "<incorrect Address>" into the "create account address line 1" field
    And  I click on the "CREATE AN ACCOUNT" button
    And  I click on the "Use USPS Corrected Address" button
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I click on the "View my account" link
    And  I click on the "add alternate contact" link
    And  I add contact info as "<Alternate Customer>" for "alternate" customer
    And  I enter "<incorrect Alternate Address>" into the "create account address line 1" field
    And  I enter "<Alternate Email>" into the "E-mail Address" field
    And  I click on the "UPDATE" button
    And  I click on the "Use USPS Corrected Address" button
    And  I click on "primary contact" edit link
    And  I enter "<incorrect Address>" into the "create account address line 1" field
    And  I enter "<Zip>" into the "create account zipcode" field
    And  I click on the "UPDATE" button
    And  I click on the "Use USPS Corrected Address" button
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I enter "<incorrect Address>" into the "create account address line 1" field
    And  I click on the "Continue to Payment" button
    And  I click on the "Use USPS Corrected Address" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I verify "<zip plus four>" is listed on the order confirmation page
    When I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | New Customer            | incorrect Address      | Existing Email       | password  | Alternate Customer     | incorrect Alternate Address | Alternate Email     | zip plus four | Zip   |
      | 2012 | Honda | Civic | Coupe DX | none     | MY_ACCOUNT_NEW_CUSTOMER | 20225 N scottssdale Rd | autouser_a@gmail.com | Discount1 | MY_ACCOUNT_USER_QATEST | 950 Forrerr Blvd            | qatest345@gmail.com | 85255-6456    | 85255 |

  @dt
  @at
  @web
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  @bopisOrderWithAppointmentPrimaryContact
  Scenario Outline: Create account scenario with BOPIS and with appointment for primary contact
    When I go to the homepage
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    And  I populate all the required fields for "<New Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    And  I click on the "CREATE AN ACCOUNT" button
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    When I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | New Customer            | password  | Alternate Customer     | incorrect Alternate Address | Alternate Email     | zip plus four | Credit Card      | Bopis Customer                    |
      | 2012 | Honda | Civic | Coupe DX | none     | MY_ACCOUNT_NEW_CUSTOMER | Discount1 | MY_ACCOUNT_USER_QATEST | 950 Forrerr Blvd            | qatest345@gmail.com | 85255-6456    | MasterCard Bopis | DEFAULT_CUSTOMER_BOPIS_MASTERCARD |

  @dt
  @at
  @web
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  @ropisOrderWithoutAppointmentPrimaryContact
  Scenario Outline: Create account scenario with ROPIS and without appointment for primary contact
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    And  I populate all the required fields for "<New Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    And  I click on the "CREATE AN ACCOUNT" button
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I enter "<incorrect Address>" into the "create account address line 1" field
    And  I click on the "Continue to Payment" button
    And  I click on the "Use USPS Corrected Address" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I verify "<zip plus four>" is listed on the order confirmation page
    When I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | New Customer            | incorrect Address      | password  | Alternate Customer     | Alternate Email     | zip plus four | Reason                              |
      | 2012 | Honda | Civic | Coupe DX | none     | MY_ACCOUNT_NEW_CUSTOMER | 20225 N scottssdale Rd | Discount1 | MY_ACCOUNT_USER_QATEST | qatest345@gmail.com | 85255-6456    | Make an appointment at a later time |

  @dt
  @at
  @web
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  @bopisOrderWithoutAppointmentPrimaryContact
  Scenario Outline: Create account scenario with BOPIS and without appointment for primary contact
    When I go to the homepage
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    And  I populate all the required fields for "<New Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    And  I click on the "CREATE AN ACCOUNT" button
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    When I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | New Customer            | password  | Alternate Customer     | Alternate Email     | Credit Card      | Bopis Customer                    | Reason                              |
      | 2012 | Honda | Civic | Coupe DX | none     | MY_ACCOUNT_NEW_CUSTOMER | Discount1 | MY_ACCOUNT_USER_QATEST | qatest345@gmail.com | MasterCard Bopis | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | Make an appointment at a later time |

  @dt
  @at
  @web
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  @ropisOrderWithAppointmentSecondaryContact
  Scenario Outline: Create account scenario with ROPIS and with appointment for secondary contact
    When I go to the homepage
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    And  I populate all the required fields for "<New Customer>"
    And  I enter "<Existing Email>" in email address field
    Then I should see "CREATE AN ACCOUNT" is enabled
    When I click on the "CREATE AN ACCOUNT" button
    Then I verify "existing email error message" is displayed
    When I enter my account phone number
    And  I enter my account email address on "homepage" page
    And  I enter "<incorrect Address>" into the "create account address line 1" field
    And  I click on the "CREATE AN ACCOUNT" button
    And  I click on the "Use USPS Corrected Address" button
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    And  I click on the "View my account" link
    And  I click on the "add alternate contact" link
    And  I add contact info as "<Alternate Customer>" for "alternate" customer
    And  I enter "<incorrect Alternate Address>" into the "create account address line 1" field
    And  I enter "<Alternate Email>" into the "E-mail Address" field
    And  I click on the "UPDATE" button
    And  I click on the "Use USPS Corrected Address" button
    And  I click on "primary contact" edit link
    And  I enter "<incorrect Address>" into the "create account address line 1" field
    And  I enter "<Zip>" into the "create account zipcode" field
    And  I click on the "UPDATE" button
    And  I click on the "Use USPS Corrected Address" button
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I select "Alternate Information" from drop down
    Then I verify "Address Line 1, Address Line 2, Zip, Phone Number" values for "<Alternate Customer>" are now pre-populated
    When I enter "<incorrect Alternate Address>" into the "create account address line 1" field
    And  I click on the "Continue to Payment" button
    And  I click on the "Use USPS Corrected Address" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    And  I verify "<zip plus four>" is listed on the order confirmation page
    When I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | New Customer            | incorrect Address      | Existing Email       | password  | Alternate Customer     | incorrect Alternate Address | Alternate Email     | zip plus four | Zip   |
      | 2012 | Honda | Civic | Coupe DX | none     | MY_ACCOUNT_NEW_CUSTOMER | 20225 N scottssdale Rd | autouser_a@gmail.com | Discount1 | MY_ACCOUNT_USER_QATEST | 950 Forrerr Blvd            | qatest345@gmail.com | 45420-1469    | 85255 |

  @dt
  @at
  @web
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  @bopisOrderWithAppointmentSecondaryContact
  Scenario Outline: Create account scenario with BOPIS and with appointment for secondary contact
    When I go to the homepage
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    And  I populate all the required fields for "<New Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    And  I click on the "CREATE AN ACCOUNT" button
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    And  I click on the "View my account" link
    And  I click on the "add alternate contact" link
    And  I add contact info as "<Alternate Customer>" for "alternate" customer
    And  I enter "<Alternate Email>" into the "E-mail Address" field
    And  I click on the "UPDATE" button
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    When I select "Alternate Information" from drop down
    Then I verify "Email,Phone Number" values for "<Alternate Customer>" are now pre-populated
    And  I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    When I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | New Customer            | password  | Alternate Customer     | incorrect Alternate Address | Alternate Email     | zip plus four | Credit Card      | Bopis Customer                    |
      | 2012 | Honda | Civic | Coupe DX | none     | MY_ACCOUNT_NEW_CUSTOMER | Discount1 | MY_ACCOUNT_USER_QATEST | 950 Forrerr Blvd            | qatest345@gmail.com | 85255-6456    | MasterCard Bopis | DEFAULT_CUSTOMER_BOPIS_MASTERCARD |

  @dt
  @at
  @web
  @mobile
  @regression
  @myAccountOrders
  @ordersRegression
  @myAccountRegression
  @myAccountOrdersRegression1
  @ropisOrderWithoutAppointmentSecondaryContact
  Scenario Outline: Create account scenario with ROPIS and without appointment for secondary contact
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    And  I populate all the required fields for "<New Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    And  I click on the "CREATE AN ACCOUNT" button
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I click on the "View my account" link
    And  I click on the "add alternate contact" link
    And  I add contact info as "<Alternate Customer>" for "alternate" customer
    And  I enter "<Alternate Email>" into the "E-mail Address" field
    And  I click on the "UPDATE" button
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I select "Alternate Information" from drop down
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | tax         |
      | productCode |
      | discount    |
    And  I assert order xml customer node values
    And  I assert order xml subItems
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |

    Examples:
      | Year | Make  | Model | Trim     | Assembly | New Customer            | password  | Alternate Customer     | incorrect Alternate Address | Alternate Email     | zip plus four | Reason                              |
      | 2012 | Honda | Civic | Coupe DX | none     | MY_ACCOUNT_NEW_CUSTOMER | Discount1 | MY_ACCOUNT_USER_QATEST | 950 Forrerr Blvd            | qatest345@gmail.com | 45420-1469    | Make an appointment at a later time |

  @dt
  @at
  @web
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  @bopisOrderWithoutAppointmentSecondaryContact
  Scenario Outline: Create account scenario with BOPIS and without appointment for secondary contact
    When I go to the homepage
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    And  I populate all the required fields for "<New Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    And  I click on the "CREATE AN ACCOUNT" button
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    And  I click on the "View my account" link
    And  I click on the "add alternate contact" link
    And  I add contact info as "<Alternate Customer>" for "alternate" customer
    And  I enter "<Alternate Email>" into the "E-mail Address" field
    And  I click on the "UPDATE" button
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    When I select "Alternate Information" from drop down
    Then I verify "Email,Phone Number" values for "<Alternate Customer>" are now pre-populated
    And  I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    When I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | New Customer            | password  | Alternate Customer     | Alternate Email     | Credit Card      | Bopis Customer                    | Reason                              |
      | 2012 | Honda | Civic | Coupe DX | none     | MY_ACCOUNT_NEW_CUSTOMER | Discount1 | MY_ACCOUNT_USER_QATEST | qatest345@gmail.com | MasterCard Bopis | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | Make an appointment at a later time |

  @dt
  @at
  @web
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  @bopisOrderWithAppointmentSecondaryContactCheckout
  Scenario Outline: Create account scenario on checkout with BOPIS and with appointment for secondary contact
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I select "sign in to skip this step." link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I click on the "Continue to Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    And  I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                | password  | Credit Card      | Bopis Customer                    | password  | Customer          |
      | 2012 | Honda | Civic | Coupe DX | none     | autouser_a@gmail.com | Discount1 | MasterCard Bopis | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | Discount1 | MY_ACCOUNT_USER_A |

  @dt
  @at
  @web
  @mobile
  @myAccountOrders
  @ordersRegression
  @myAccountRegression
  @myAccountOrdersRegression1
  @bopisOrderWithoutAppointmentPrimaryContactCheckout
  Scenario Outline: Create account scenario on checkout with BOPIS and without appointment for primary contact
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I select create an account using my information
    And  I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "checkout" page
    When I enter "<password>" into "Password" field
    Then I verify email sent confirmation message displayed on "checkout page"
    And  I verify create an account using my information checkbox is selected
    When I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Bopis Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId   |
      | amountOfSale |
      | siteNumber   |
      | orderType    |
      | customerType |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Customer                | password  | Credit Card      | Bopis Customer                    | password  | Reason                              |
      | 2012 | Honda | Civic | Coupe DX | none     | MY_ACCOUNT_NEW_CUSTOMER | Discount1 | MasterCard Bopis | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | Discount1 | Make an appointment at a later time |

  @dt
  @at
  @web
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  @ropisOrderWithAppointmentSecondaryContactCheckout
  Scenario Outline: Create account scenario on checkout with ROPIS and with appointment for Secondary contact
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I click on the "sign in to skip this step." link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    And  I verify "First Name, Last Name, Address Line 1, Zip Code, Email, Phone Number" values for "<Customer>" are now pre-populated
    And  I enter "<incorrectAddress>" into the "create account address line 1" field
    And  I click on the "Continue to Payment" button
    And  I click on the "Use USPS Corrected Address" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I verify "<zip plus four>" is listed on the order confirmation page
    And  I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Customer          | password  | incorrectAddress       | zip plus four | Email                |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | MY_ACCOUNT_USER_A | Discount1 | 20225 N scottsdalee Rd | 85255-6456    | autouser_a@gmail.com |

  @dt
  @at
  @web
  @mobile
  @regression
  @myAccountOrders
  @ordersRegression
  @myAccountRegression
  @myAccountOrdersRegression1
  @ropisOrderWithoutAppointmentSecondaryContactCheckout
  Scenario Outline: Create account scenario on checkout with ROPIS and without appointment for Secondary contact
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I click on the "sign in to skip this step." link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | tax         |
      | productCode |
      | discount    |
    And  I assert order xml customer node values
    And  I assert order xml subItems
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Customer          | password  | Email                | Reason                              |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | MY_ACCOUNT_USER_A | Discount1 | autouser_a@gmail.com | Make an appointment at a later time |

  @dtd
  @web
  @mobile
  @regression
  @myAccountOrders
  @myAccountRegression
  @dtdSignInScenarioWithAlternateContact
  Scenario Outline: Sign in on checkout scenario for alternate contact_DTD
  """ My account feature is decided to be turned off for DTD orders, hence taking out the customer type validation"""
    When I change to the default store
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select "Alternate Information" from drop down
    Then I verify " Address Line 1, Address Line 2, Zip Code, City, Email, Phone Number" values for "<Alternate Customer>" are now pre-populated
    And  I enter "<incorrect Alternate Address>" into the "create account address line 1" field
    When I click on the "Continue To Shipping Method" button
    And  I click on the "Use USPS Corrected Address" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Bopis Customer>" on order confirmation page
    And  I verify "<zip plus four>" is listed on the order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | webOrderOrigin |
      | siteNumber     |
      | amountOfSale   |
      | orderType      |
      | shipMethod     |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                | password  | Alternate Customer     | incorrect Alternate Address | zip plus four | Bopis Customer                    |
      | 2012 | Honda | Civic | Coupe DX | none     | autouser_a@gmail.com | Discount1 | MY_ACCOUNT_USER_QATEST | 950 Forrerr Blvd            | 45420-1469    | DEFAULT_CUSTOMER_BOPIS_MASTERCARD |

  @dt
  @at
  @web
  @17812
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_ORDERS_Order History_Order Detail_Staggered And Styling (ALM#17812)
    When I select my account header navigation
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I save order number from order confirmation page in a list
    And  I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Appointments" tab
    And  I select "view order" for appointment with order number
    Then I should see product "<ItemCode>" on the "Order History Detail" page
    And  I verify customer "<Customer>" details are listed on the "Order History Detail" page
    And  I verify order number from order confirmation page with order history detail page
    When I click on the "back to my orders" link
    Then I verify order number on order history summary page on "Orders" tab

    Examples:
      | Year | Make   | Model  | Trim  | Assembly       | Customer                       | password  | Email                      | ItemCode |
      | 2015 | Nissan | Altima | Sedan | 215 /55 R17 SL | MY_ACCOUNT_REGISTERED_CUSTOMER | Discount1 | automationtestdt@gmail.com | 30671    |

  @dt
  @at
  @dtd
  @web
  @19761
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_ORDERS_Order History _Sorting (ALM#19761)
    When I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    And  I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Orders" tab
    Then I verify the sorting options are displayed on "order" summary page
    And  I verify the default sorting order is "Descending"
    When I select the "Date (Ascending)" from the Sort By dropdown box
    Then I verify that orders are sorted in "Ascending" order
    When I select the "Date (Descending)" from the Sort By dropdown box
    Then I verify that orders are sorted in "Descending" order

    Examples:
      | Email                | password  |
      | autouser_a@gmail.com | Discount1 |


  @dtd
  @web
  @20037
  @regression
  @myAccountOrders
  @myAccountRegression
  @myAccountPaypalExpressCheckout
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_ORDERS_Paypal_express_checkout (ALM#20037)
    When I select my account header navigation
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select paypal checkout
    And  I switch to "Paypal" window
    And  I log into paypal as "<Customer>"
    And  I continue with the paypal payment
    And  I switch to main window
    Then I verify "Checkout" page is displayed
    And  I verify "First Name, Last Name, Address Line 1, Country, Zip, City, State, Email" values for "<Customer1>" are now pre-populated
    When I click on the "Continue To Shipping Method" button
    And  I click on the "Continue To Payment" button
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page

    Examples:
      | Year | Make   | Model  | Trim  | Assembly       | Customer          | password  | Email                | Customer1          |
      | 2015 | Nissan | Altima | Sedan | 215 /55 R17 SL | MY_ACCOUNT_USER_A | Discount1 | autouser_a@gmail.com | paypal_customer_az |

  @dtd
  @web
  @20039
  @regression
  @myAccountOrders
  @myAccountRegression
  @myAccountPaypalCheckout
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_ORDERS_Paypal_checkout (ALM#20039)
  """ This test will fail as the my account feature is decided to be turned off for DTD orders"""
    When I select my account header navigation
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    And  I delete "All" saved vehicles
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    Then I verify "First Name, Last Name, Address Line 1, Country, Zip, City, State, Email" values for "<Customer>" are now pre-populated
    When I click on the "Continue To Shipping Method" button
    And  I click on the "Continue To Payment" button
    And  I select the "paypal" payment option
    And  I select paypal checkout
    And  I switch to "Paypal" window
    And  I log into paypal as "<Customer1>"
    And  I continue with the paypal payment
    And  I switch to main window
    Then I am brought to the order confirmation page

    Examples:
      | Year | Make   | Model  | Trim  | Assembly       | Customer          | password  | Email                | Customer1          |
      | 2015 | Nissan | Altima | Sedan | 215 /55 R17 SL | MY_ACCOUNT_USER_A | Discount1 | autouser_a@gmail.com | paypal_customer_az |

  @dt
  @at
  @web
  @20040
  @myAccountOrders
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Order Details Tab_ROPIS order_Create Appointment (ALM#20040)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I select "sign in to skip this step." link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I click on the "Continue To Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I save order number from order confirmation page in a list
    And  I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Orders" tab
    Then I verify order number on order history summary page on "Orders" tab
    When I click on order view details link in order history summary page
    And  I click on the "MAKE APPOINTMENT" link
    And  I select first available appointment date
    And  I click on the "CONFIRM APPOINTMENT" button
    Then I am brought to the appointment confirmation page
    And  I verify "<Customer>" details is displayed on the "appointment confirmation" page
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Appointments" tab
    Then I verify order number on order history summary page on "Appointments" tab
    When I select "Orders" tab
    And  I click on order view details link in order history summary page
    And  I click on the "VIEW APPOINTMENT" link
    Then I verify "<Customer>" details is displayed on the "appointment details" page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Customer                       | password  | zip plus four | Reason                              | Email                      |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | MY_ACCOUNT_REGISTERED_CUSTOMER | Discount1 | 85255-6456    | Make an appointment at a later time | automationtestdt@gmail.com |

  @dt
  @at
  @web
  @20041
  @myAccountOrders
  @myAccountRegression
  Scenario Outline: HYBRIS_CUSTOMER_MY ACCOUNTS_Order Details Tab_BOPIS order_Create Appointment (ALM#20041)
  """ Will fail because order history is not being generated by CAR job immediately for new customers. It is run on the hour as it is in production. """
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I select "sign in to skip this step." link
    Then I verify my account sign-in modal is displayed
    When I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I am brought to the page with header "Checkout"
    And  I verify "First Name, Last Name, Address Line 1, Address Line 2, City, State, Zip Code, Country, Email, Phone Type, Phone Number" values for "<Customer>" are now pre-populated
    When I click on the "Continue To Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer1>"
    And  I place the order for "<Customer1>"
    Then I am brought to the order confirmation page
    And  I save order number from order confirmation page in a list
    And  I verify "<zip plus four>" is listed on the order confirmation page
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Orders" tab
    Then I verify order number on order history summary page on "Orders" tab
    When I click on order view details link in order history summary page
    And  I click on the "MAKE APPOINTMENT" link
    And  I select first available appointment date
    And  I click on the "CONFIRM APPOINTMENT" button
    Then I am brought to the appointment confirmation page
    And  I verify "<Customer>" details is displayed on the "appointment confirmation" page
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Appointments" tab
    And  I verify order number on order history summary page on "Appointments" tab
    And  I select "Orders" tab
    And  I click on order view details link in order history summary page
    And  I click on the "VIEW APPOINTMENT" link
    Then I verify "<Customer>" details is displayed on the "appointment details" page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | Customer                       | password  | zip plus four | Reason                              | Customer1                   | Credit Card | Email                      |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | MY_ACCOUNT_REGISTERED_CUSTOMER | Discount1 | 85255-6456    | Make an appointment at a later time | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | automationtestdt@gmail.com |

  @dt
  @at
  @web
  @core
  @21284
  @ordersRegression
  @coreScenarioMyAccount
  @myAccountOrdersRegression1
  Scenario Outline: HYBRIS_CUSTOMER_MY_ACCOUNTS_CoreScenarios_FreeTextSearch_CreateAnAccountOnCheckout_PayInStore_
  WithAppointment_AVSValidation_OrderHistoryValidation_UserAccountValidation_OrderXMLValidation____FitmentFlow_
  CreateAnAccountOnHomePage_PayOnline_WithoutAppointment_AVSValidation_OrderHistoryValidation_UserAccountValidation_
  AddAppointmentFromOrderSummaryPage_ValidateNewAppointment_OrderXMLValidation (ALM#21284)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I click on the "Checkout Now" button
    And  I do a "default" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I close popup modal
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I select create an account using my information
    Then I verify create an account using my information checkbox is selected
    When I populate all the required fields for "<Customer>"
    And  I enter my account email address on "checkout" page
    And  I enter my account phone number
    And  I enter "<incorrectAddress>" into the "create account address line 1" field
    And  I enter "<password>" in password field
    Then I verify email sent confirmation message displayed on "checkout page"
    When I click on the "Continue To Payment" button
    Then I verify the "Address Verification" modal is displayed
    When I click on the "Use USPS Corrected Address" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId   |
      | amountOfSale |
      | siteNumber   |
      | orderType    |
      | customerType |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml subItems
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"
    When I close popup modal
    And  I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Orders" tab
    Then I verify order number on order history summary page on "Orders" tab
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Appointments" tab
    Then I verify order number on order history summary page on "Appointments" tab
    When I select my account header navigation
    And  I click on the "Sign-out" link
    And  I go to the homepage
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I should see "First Name,Last Name,Email,Password,Phone Type,Address Line 1,Address Line 2,Country,Zip Code,City,State" field is displayed
    And  I verify that "CREATE AN ACCOUNT" is disabled by default
    When I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    Then I verify "City, State" values for "<Customer>" are now pre-populated
    When I click on the "CREATE AN ACCOUNT" button
    Then I verify email sent confirmation message displayed on "account confirmation modal"
    And  I verify my email address displayed on account confirmation modal
    And  I verify my email authentication link message displayed on account confirmation modal
    When I select continue shopping option
    Then I verify Join/Sign-in is displayed on "homepage" page
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I verify create account modal is displayed
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click on the "Continue to Customer Details" button
    And  I enter "<incorrectAddress>" into the "create account address line 1" field
    And  I click on the "Continue To Payment" button
    Then I verify the "Address Verification" modal is displayed
    When I click on the "Use USPS Corrected Address" button
    And  I enter payment info with "<Credit Card>" credit card and a different billing address and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    When I save order number from order confirmation page in a list
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    And  I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Orders" tab
    Then I verify order number on order history summary page on "Orders" tab
    When I click on order view details link in order history summary page
    And  I click on the "MAKE APPOINTMENT" link
    And  I select first available appointment date
    And  I click on the "CONFIRM APPOINTMENT" button
    Then I am brought to the appointment confirmation page
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Appointments" tab
    Then I verify order number on order history summary page on "Appointments" tab
    When I read the order xml file
    Then I assert order xml node values
      | webOrderId   |
      | amountOfSale |
      | siteNumber   |
      | orderType    |
      | customerType |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml subItems
    And  I assert order xml payment node values

    Examples:
      | Year | Make  | Model | Trim     | Assembly | FitmentOption | ProductName        | ItemCode | Customer                | password  | incorrectAddress        | zip plus four | Bopis Customer             | Credit Card      |
      | 2012 | Honda | Civic | Coupe DX | none     | tire          | Silver Edition III | 29935    | MY_ACCOUNT_NEW_CUSTOMER | Discount1 | 20225 N Scottsadalee Rd | 85255-6456    | DEFAULT_CUSTOMER_BOPIS_CC1 | CarCareOne Bopis |

  @dt
  @at
  @web
  @core
  @21285
  @ordersRegression
  @coreScenarioMyAccount
  @myAccountOrdersRegression2
  @coreScenarioMyAccountAlternateContact
  Scenario Outline: HYBRIS_CUSTOMER_MY_ACCOUNTS_CoreScenarios_FreeTextSearch_CreateAnAccountOnCheckout_
  PayInStore_WithAppointment_AVSValidation_OrderHistoryValidation_UserAccountValidation_OrderXMLValidation____
  FitmentFlow_CreateAnAccountOnHomePage_PayOnline_WithoutAppointment_AVSValidation_OrderHistoryValidation_
  UserAccountValidation_AddAppointmentFromOrderSummaryPage_ValidateNewAppointment_OrderXMLValidation_Alternate_contact (ALM#21285)
    When I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    And  I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    And  I enter "<incorrectAddress>" into the "create account address line 1" field
    And  I click on the "CREATE AN ACCOUNT" button
    And  I click on the "Use USPS Corrected Address" button
    And  I select continue shopping option
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I select my account header navigation
    And  I click on the "View my account" link
    And  I click on the "add alternate contact" link
    And  I add contact info as "<Alternate Customer>" for "alternate" customer
    And  I enter "<incorrect Alternate Address>" into the "create account address line 1" field
    And  I enter "<Alternate Email>" into the "E-mail Address" field
    And  I click on the "UPDATE" button
    And  I click on the "Use USPS Corrected Address" button
    And  I click on "primary contact" edit link
    And  I enter "<incorrectAddress>" into the "create account address line 1" field
    And  I enter "<Zip>" into the "create account zipcode" field
    And  I click on the "UPDATE" button
    And  I click on the "Use USPS Corrected Address" button
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I extract the "sales tax" on the cart page
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I select "Alternate Information" from drop down
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Orders" tab
    Then I verify order number on order history summary page on "Orders" tab
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Appointments" tab
    Then I verify order number on order history summary page on "Appointments" tab
    When I read the order xml file
    Then I assert order xml node values
      | webOrderId   |
      | amountOfSale |
      | siteNumber   |
      | orderType    |
      | customerType |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml subItems
    When I select my account header navigation
    And  I click on the "Sign-out" link
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I extract the "sales tax" on the cart page
    And  I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click next step for customer information
    And  I click on the "sign in to skip this step." link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    And  I select "Alternate Information" from drop down
    And  I click on the "Continue to Payment" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<BOPISCustomer>"
    And  I place the order for "<BOPISCustomer>"
    Then I am brought to the order confirmation page
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Orders" tab
    Then I verify order number on order history summary page on "Orders" tab
    When I click on order view details link in order history summary page
    And  I click on the "MAKE APPOINTMENT" link
    And  I select first available appointment date
    And  I click on the "CONFIRM APPOINTMENT" button
    Then I am brought to the appointment confirmation page
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Appointments" tab
    Then I verify order number on order history summary page on "Appointments" tab
    When I read the order xml file
    Then I assert order xml node values
      | webOrderId   |
      | amountOfSale |
      | siteNumber   |
      | orderType    |
      | customerType |
    And  I assert order xml list values
      | productCode |
      | discount    |
      | tax         |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml payment node values
    And  I assert order xml subItems

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCode | Customer                | password  | incorrectAddress        | zip plus four | Alternate Customer     | incorrect Alternate Address | Alternate Email     | Zip   | Credit Card      | Email                | BOPISCustomer              | Quantity | Primary Customer  |
      | 2012 | Honda | Civic | Coupe DX | none     | 29935    | MY_ACCOUNT_NEW_CUSTOMER | Discount1 | 20225 N Scottsadalee Rd | 45420-1469    | MY_ACCOUNT_USER_QATEST | 950 Forrerr Blvd            | qatest345@gmail.com | 85255 | CarCareOne Bopis | autouser_b@gmail.com | DEFAULT_CUSTOMER_BOPIS_CC1 | 4        | MY_ACCOUNT_USER_B |

  @dt
  @at
  @web
  @ordersRegression
  @myAccountOrderIntegration
  @myAccountOrdersRegression2
  @serviceAppointmentCreateAndModify
  @myAccountOrderIntegrationScenariosSet3
  Scenario Outline: Create Service appointment - cancel the appointment on web
    When I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I select mini cart and "Clear Cart"
    And  I open the "APPOINTMENTS" navigation link
    And  I select service option(s): "<ServiceOptions>"
    And  I select 'Set Appointment Details' for Date and Time
    And  I click on the "CONTINUE" button
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Make Appointment" button
    And  I store the order number
    And  I read the order xml file
    And  I assert order xml items for "<ServiceOptions>"
    And  I assert order xml node values
      | webOrderId           |
      | amountOfSale         |
      | siteNumber           |
      | orderType            |
      | customerType         |
      | webOrderOrigin       |
      | appointmentDate      |
      | appointmentStartTime |
    And  I assert order xml customer node values
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
      | appointmentId        |
      | appointmentEndTime   |
    And  I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Appointments" tab
    And  I select "view details" for appointment with order number
    And  I click on the "Cancel" button
    And  I select the reason "<reason>" for cancellation
    And  I click on the "Cancel Appointment" button
    Then I am brought to the page with header "<Header>"

    Examples:
      | Email                      | password  | ServiceOptions                | reason                               | Header                             |
      | automationtestdt@gmail.com | Discount1 | New Tires/Wheels Consultation | Appointment/service no longer needed | Appointment Successfully Cancelled |

  @dt
  @at
  @web
  @ordersRegression
  @myAccountOrderIntegration
  @myAccountOrdersRegression2
  @myAccountOrderIntegrationScenariosSet3
  @bopisOrderTireAndWheelWithCancelAppointment
  Scenario Outline: BOPIS Create - Vehicle [Tire and Wheel] - with appointment- Cancel appointment
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue to Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Customer>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId           |
      | amountOfSale         |
      | siteNumber           |
      | orderType            |
      | customerType         |
      | webOrderOrigin       |
      | vehicle              |
      | appointmentDate      |
      | appointmentStartTime |
    And  I assert order xml list values
      | tax         |
      | productCode |
      | discount    |
    And  I assert order xml payment node values
#    And  I assert order xml customer node values
    And  I assert order xml subItems
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
      | appointmentId        |
      | appointmentEndTime   |
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Appointments" tab
    And  I select "view details" for appointment with order number
    And  I click on the "Cancel" button
    And  I select the reason "<reason>" for cancellation
    And  I click on the "Cancel Appointment" button
    Then I am brought to the page with header "<Header>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                      | password  | FitmentOption | Customer                    | Customer1                            | reason                               | Header                             |
      | 2012 | Honda | Civic | Coupe DX | none     | automationtestdt@gmail.com | Discount1 | tire          | DEFAULT_CUSTOMER_BOPIS_VISA | CUSTOMER_INTEGRATION_APPOINTMENTS_AZ | Appointment/service no longer needed | Appointment Successfully Cancelled |

  @dt
  @at
  @web
  @ordersRegression
  @myAccountOrderIntegration
  @myAccountOrdersRegression2
  @ropisScheduleAppointmentWithVehicle
  @myAccountOrderIntegrationScenariosSet3
  Scenario Outline: Create ROPIS order without appointment - add the appointment on web - Vehicle [Tire and Wheel]
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue To Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | tax         |
      | productCode |
      | discount    |
    And  I assert order xml customer node values
    And  I assert order xml subItems
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Orders" tab
    Then I verify order number on order history summary page on "Orders" tab
    And  I click on order view details link in order history summary page
    And  I click on the "MAKE APPOINTMENT" link
    And  I select first available appointment date
    And  I click on the "CONFIRM APPOINTMENT" button
    Then I am brought to the appointment confirmation page


    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                      | password  | Reason                              | Customer                             |
      | 2012 | Honda | Civic | Coupe DX | none     | automationtestdt@gmail.com | Discount1 | Make an appointment at a later time | CUSTOMER_INTEGRATION_APPOINTMENTS_AZ |

  @dt
  @at
  @web
  @myAccountOrderIntegration
  @myAccountOrderIntegrationScenariosSet3
  @bopisScheduleAppointmentWithVehicle
  Scenario Outline: Create BOPIS order without appointment - add the appointment on web
  """FAILS:  due to defect where the confirm appointment page takes to homepage (OEWM-579)"""
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item "<ItemCode1>" of type "none" to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I click on the "CONTINUE TO PAYMENT" button
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<BopisCustomer>"
    And  I place the order for "<BopisCustomer>"
    Then I am brought to the order confirmation page
    And  I save order number from order confirmation page in a list
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Orders" tab
    Then I verify order number on order history summary page on "Orders" tab
    When I click on order view details link in order history summary page
    And  I click on the "MAKE APPOINTMENT" link
    And  I select first available appointment date
    And  I click on the "CONFIRM APPOINTMENT" button
    Then I am brought to the appointment confirmation page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                      | password  | FitmentOption | ItemCode | ItemCode1 | Reason                              | Customer                             | Credit Card    | BopisCustomer                   |
      | 2012 | Honda | Civic | Coupe DX | none     | automationtestdt@gmail.com | Discount1 | tire          | 26899    | 23500     | Make an appointment at a later time | CUSTOMER_INTEGRATION_APPOINTMENTS_AZ | Discover Bopis | DEFAULT_CUSTOMER_BOPIS_DISCOVER |

  @dt
  @at
  @web
  @ordersRegression
  @myAccountOrderIntegration
  @myAccountOrdersRegression3
  @myAccountOrderIntegrationScenariosSet3
  @ropisOrderTireAndWheelWithCancelAppointment
  Scenario Outline: ROPIS Create - Vehicle [Tire and Wheel] - with appointment - Cancel appointment
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue To Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId           |
      | amountOfSale         |
      | siteNumber           |
      | orderType            |
      | customerType         |
      | webOrderOrigin       |
      | vehicle              |
      | appointmentDate      |
      | appointmentStartTime |
    And  I assert order xml list values
      | tax         |
      | productCode |
      | discount    |
    And  I assert order xml customer node values
    And  I assert order xml subItems
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
      | appointmentId        |
      | appointmentEndTime   |
    When I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Appointments" tab
    And  I select "view details" for appointment with order number
    And  I click on the "Cancel" button
    And  I select the reason "<reason>" for cancellation
    And  I click on the "Cancel Appointment" button
    Then I am brought to the page with header "<Header>"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                      | password  | FitmentOption | Customer1                            | reason                               | Header                             |
      | 2012 | Honda | Civic | Coupe DX | none     | automationtestdt@gmail.com | Discount1 | tire          | CUSTOMER_INTEGRATION_APPOINTMENTS_AZ | Appointment/service no longer needed | Appointment Successfully Cancelled |

  @dt
  @at
  @web
  @ordersRegression
  @myAccountOrderIntegration
  @myAccountOrdersRegression3
  @myAccountOrderIntegrationScenariosSet3
  @ropisOrderTireAndWheelWithModifyAppointment
  Scenario Outline: ROPIS Create - Vehicle [Tire and Wheel] - with appointment - Modify appointment
  """ Failing on customer address validation due to billing address being sent to POS and ESB as the customer address.
     'I assert order xml customer node values' will be commented out until resolved. """
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue To Payment" button
    And  I select "Pay in Store" payment tab
    And  I record the time stamp "before" creating the dt order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page
    When I store the order number
    And  I record the time stamp plus "5" minutes "after" creating the web order order in YYYYMMDDHHMMSS format for customer "<Customer1>"
    And  I extract and save the sales tax for "<Customer1>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId           |
      | amountOfSale         |
      | siteNumber           |
      | orderType            |
      | customerType         |
      | webOrderOrigin       |
      | vehicle              |
      | appointmentDate      |
      | appointmentStartTime |
    And  I assert order xml list values
      | tax         |
      | productCode |
      | discount    |
#    And  I assert order xml customer node values
    And  I assert order xml subItems
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
      | appointmentId        |
      | appointmentEndTime   |
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I request a get response to the order look up url for site id "discounttire"
    And  I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header"
    And  I enter start time and end time in posdm
    And  I enter store number for DT orders in posdm
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I execute
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    When I search for hybris order number in posdt and save it to scenario data
    Then I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I compare "firstAppointmentStartTime" value from web and "APTSTRTIME" from CAR for customer with email "<Email>"
    And  I go to the homepage
    And  I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Appointments" tab
    And  I select "view details" for appointment with order number
    And  I click on the "Reschedule" link
    And  I select last available appointment date
    And  I extract date and time for validation
    And  I click on the "Confirm Appointment" button
    Then I am brought to the page "Your appointment is confirmed"

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                      | password  | FitmentOption | ItemCode | ItemCode1 | Customer1                            | Header                         | TCode 1       | Transaction Type |
      | 2012 | Honda | Civic | Coupe DX | none     | automationtestdt@gmail.com | Discount1 | tire          | 26899    | 23500     | CUSTOMER_INTEGRATION_APPOINTMENTS_AZ | Your appointment is confirmed! | /n/posdw/mon0 | 1014             |

  @dt
  @at
  @web
  @ordersRegression
  @myAccountOrderIntegration
  @myAccountOrdersRegression3
  @myAccountOrderIntegrationScenariosSet3
  @bopisOrderTireAndWheelWithModifyAppointment
  Scenario Outline: BOPIS Create - Vehicle [Tire and Wheel] - with appointment- Modify appointment
  """ Failing on customer address validation due to billing address being sent to POS and ESB as the customer address.
     'I assert order xml customer node values' will be commented out until resolved. """
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I select mini cart and "Clear Cart"
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "<FitmentOption1>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue to Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Customer>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I store the order number
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId           |
      | amountOfSale         |
      | siteNumber           |
      | orderType            |
      | customerType         |
      | webOrderOrigin       |
      | vehicle              |
      | appointmentDate      |
      | appointmentStartTime |
    And  I assert order xml list values
      | tax         |
      | productCode |
      | discount    |
    And  I assert order xml payment node values
#    And  I assert order xml customer node values
    And  I assert order xml subItems
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
      | appointmentId        |
      | appointmentEndTime   |
    When I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttire"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header"
    And  I enter BOPIS web order number into field name with label "Transaction Number"
    And  I execute
    Then I verify if tasks with errors exist in posdm/posdt
    When I search for hybris order number in posdt and save it to scenario data
    Then I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    And  I compare "firstAppointmentStartTime" value from web and "APTSTRTIME" from CAR for customer with email "<Email>"
    When I go to the homepage
    And  I select my account header navigation
    And  I click on the "View my account" link
    And  I select "Appointments" tab
    And  I select "view details" for appointment with order number
    And  I click on the "Reschedule" link
    And  I select last available appointment date
    And  I extract date and time for validation
    And  I click on the "Confirm Appointment" button
    Then I am brought to the page "Your appointment is confirmed"

    Examples:
      | Year | Make   | Model | Trim | Assembly | Email                      | password  | FitmentOption | Customer                    | FitmentOption1 | Header                         | TCode 1       | Transaction Type |
      | 2016 | Toyota | Camry | XLE  | none     | automationtestdt@gmail.com | Discount1 | tire          | DEFAULT_CUSTOMER_BOPIS_VISA | wheel          | Your appointment is confirmed! | /n/posdw/mon0 | 1014             |

  @dt
  @at
  @web
  @19608
  @ordersRegression
  @bopisOrderCancelFromWeb
  @myAccountOrderIntegration
  @myAccountOrdersRegression3
  Scenario Outline: BOPIS Order Cancel from Web - Registered Customer (ALM#19608)
  """ Failing on customer address validation due to billing address being sent to POS and ESB as the customer address.
     'I assert order xml customer node values' will be commented out until resolved. """
    When I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "<FitmentOption1>" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue to Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Customer>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I wait for "10" seconds
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId           |
      | amountOfSale         |
      | siteNumber           |
      | orderType            |
      | customerType         |
      | webOrderOrigin       |
      | vehicle              |
      | appointmentDate      |
      | appointmentStartTime |
    And  I assert order xml list values
      | tax         |
      | productCode |
      | discount    |
    And  I assert order xml payment node values
#    And  I assert order xml customer node values
    And  I assert order xml subItems
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
      | appointmentId        |
      | appointmentEndTime   |
    When I get order xml node value
      | cancellationURL |
    And  I launch the "orderCancellation" URL
    And  I sign in with "<Email>" and "<password>" if login popup modal is displayed
    Then I am brought to the order "cancellation request" page
    When I select the reason "<reason>" for cancellation
    And  I select cancel order
    Then I am brought to the order "cancellation complete" page
    When I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttire"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I wait for "60" seconds
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header"
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I enter BOPIS web order number into field name with label "Transaction Number"
    And  I execute
    Then I verify if tasks with errors exist in posdm/posdt

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Email                      | password  | FitmentOption | ItemCode | Customer                    | FitmentOption1 | ItemCode1 | reason                                  | TCode 1       | Transaction Type |
      | 2012 | Honda | Civic | Coupe DX | none     | automationtestdt@gmail.com | Discount1 | tire          | 26899    | DEFAULT_CUSTOMER_BOPIS_VISA | wheel          | 23500     | Cancelling to order a different product | /n/posdw/mon0 | 1014             |

  @dt
  @at
  @web
  @20666
  @suggestedSelling
  Scenario Outline: HYBRIS_UI_UI_PDP_Suggested Selling_Verify the carousel of suggestions is getting displayed on PDP when wheel is browsed with Vehicle in Session_Registered (ALM#20666)
  """ Commenting out steps for vaidating product names in carousel until we can determine an automated way to identify these products. Will revisit this during Suggested Selling phase 2 build. """
    When I change to the store with url "<StoreUrl>"
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the PLP header message contains "wheel result"
    When I select the "3rd" product result image on PLP page with "Add to Cart" button
    Then I verify the message on the "PDP" banner contains "These wheels fit your vehicle"
#    And  I verify that "<Product1>" "<ProductType1>" is present at position "1" of the carousel
#    And  I verify that "<Product2>" "<ProductType2>" is present at position "2" of the carousel
    And  I verify suggested selling carousel inner-tile for "PDP" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "PDP" page

    Examples:
      | StoreUrl                 | Year | Make  | Model | Trim     | Assembly | title                      | Email                | password  | Product1   | ProductType1 | Product2 | ProductType2 |
      | /store/az/phoenix/s/1028 | 2012 | Honda | Civic | Coupe DX | none     | CUSTOMERS ALSO SHOPPED FOR | autouser_b@gmail.com | Discount1 | BRAVURIS 2 | TIRES        | ICON     | WHEELS       |

  @dt
  @at
  @web
  @20668
  @suggestedSelling
  Scenario Outline: HYBRIS_ UI_UI_PDP_Suggested Selling_Verify the carousel of suggestions is getting displayed on PDP when Tire is browsed with Vehicle in Session_Registered (ALM#20668)
  """Will fail if the conditions in the backoffice does not meet the business requirements."""
    When I change to the store with url "<StoreUrl>"
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I select mini cart and "Clear Cart"
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I select the "3rd" product result image on PLP page with "Add to Cart" button
    Then I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    And  I verify suggested selling carousel inner-tile for "PDP" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "PDP" page

    Examples:
      | StoreUrl                 | Year | Make  | Model | Trim     | Assembly | title                      | Email                | password  | ItemCode | Product3 | Product4 |
      | /store/az/phoenix/s/1028 | 2012 | Honda | Civic | Coupe DX | none     | CUSTOMERS ALSO SHOPPED FOR | autouser_b@gmail.com | Discount1 | 10708    | 14       | Vector   |

  @dt
  @at
  @web
  @20699
  @suggestedSelling
  Scenario Outline: HYBRIS_SEARCH_SEARCH_SUGGESTED SELLING_Homepage_Verify SS product Carousel for standard vehicle_Registered User_First visit(ALM#20699)
    When I change to the store with url "<StoreUrl>"
    And  I go to the homepage
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I should see "First Name,Last Name,Email,Password,Phone Type,Address Line 1,Address Line 2,Country,Zip Code,City,State" field is displayed
    And  I verify that "CREATE AN ACCOUNT" is disabled by default
    When I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    Then I verify "City, State" values for "<Customer>" are now pre-populated
    When I click on the "CREATE AN ACCOUNT" button
    Then I am brought to the page with header "<Header>"
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I click the discount tire logo
    Then I am brought to the homepage
    And  I verify suggested selling carousel inner-tile for "Homepage" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    And  I verify that tooltip icon is present in the carousel for tire product

    Examples:
      | StoreUrl                 | Customer                | password  | Year | Make  | Model | Trim     | Assembly | title                                      | Header                                   |
      | /store/az/phoenix/s/1028 | MY_ACCOUNT_NEW_CUSTOMER | Discount1 |2012  | Honda | Civic | Coupe DX | none     | "Trending near [City,State (abbreviated)]" | You've successfully created your account |

  @dt
  @at
  @web
  @20702
  @suggestedSelling
  Scenario Outline: HYBRIS_SEARCH_SEARCH_SUGGESTED SELLING_Homepage_Verify SS product Carousel for standard vehicle_Registered User_No Duplicate suggestion(ALM#20702)
    When I change to the store with url "<StoreUrl>"
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I select mini cart and "Clear Cart"
    When I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I click the discount tire logo
    Then I am brought to the homepage
    And  I verify suggested selling carousel inner-tile for "Homepage" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    When I select the "2nd" product result image on "Homepage" page
    Then I verify the "PDP" banner color is "Green"
    When I add item to my cart and "View shopping Cart"
    And  I extract "ProductName" from shopping cart as "product name"
    And  I click the discount tire logo
    Then I am brought to the homepage
    And  I verify suggested selling carousel inner-tile for "Homepage" is "not displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    When I select mini cart and "Clear Cart"

    Examples:
     | StoreUrl                 | Email                | password  | Year | Make  | Model | Trim     | Assembly | title                                      |
     | /store/az/phoenix/s/1028 | autouser_a@gmail.com | Discount1 | 2012 | Honda | Civic | Coupe DX | none     | "Trending near [City,State (abbreviated)]" |

  @dt
  @at
  @web
  @20856
  @suggestedSelling
  Scenario Outline: HYBRIS_Search_Search_Homepage_Verify SS product Carousel for _Registered User_ First Visit_No vehicle_suggestions for TRENDING NEAR PHOENIX US-AZ(ALM#20856)
  """ Commenting out steps for vaidating product names in carousel until we can determine an automated way to identify these products. Will revisit this during Suggested Selling phase 2 build. """
    When I change to the store with url "<StoreUrl>"
    And  I go to the homepage
    And  I select my account header navigation
    And  I select "Join/Sign In" link
    And  I click on the "Sign-up Now" link
    Then I should see "First Name,Last Name,Email,Password,Phone Type,Address Line 1,Address Line 2,Country,Zip Code,City,State" field is displayed
    And  I verify that "CREATE AN ACCOUNT" is disabled by default
    When I populate all the required fields for "<Customer>"
    And  I enter my account phone number
    And  I enter my account email address on "homepage" page
    Then I verify "City, State" values for "<Customer>" are now pre-populated
    When I click on the "CREATE AN ACCOUNT" button
    Then I am brought to the page with header "<Header>"
    When I make a Post call to 'oauth/token' endpoint and capture the token
    And  I do Get call to My Account customer service with "random" email and token passed
    And  I navigate to My Account email authenticate url
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    Then I am brought to the homepage
    And  I verify suggested selling carousel inner-tile for "Homepage" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
#    And  I verify that "<Product1>" "<productType1>" is present at position "1" of the carousel
#    And  I verify that "<Product2>" "<productType2>" is present at position "2" of the carousel
#    And  I verify that "<Product3>" "<productType1>" is present at position "3" of the carousel
#    And  I verify that "<Product4>" "<productType2>" is present at position "4" of the carousel
#    And  I verify that "<Product5>" "<productType2>" is present at position "5" of the carousel
    And  I verify that tooltip icon is present in the carousel for tire product

    Examples:
      | StoreUrl                 | Customer                | password  | title                       | Header                                   | productType1 | productType2 | Product1     | Product2 | Product3 | Product4 | Product5 |
      | /store/az/phoenix/s/1028 | MY_ACCOUNT_NEW_CUSTOMER | Discount1 | TRENDING NEAR PHOENIX US-AZ | You've successfully created your account | TIRES        | WHEELS       | BLIZZAK WS80 | CARTHAGE | UHP      | STATIC   | STOWE    |

  @dt
  @at
  @web
  @20797
  @suggestedSelling
    Scenario Outline: HYBRIS_CART_SHOPPING CART_Suggested Selling_Verify the carousel of suggestions is getting displayed on Items Added to Cart modal when Wheel is added to cart_Registered(ALM#20797)
    When I change to the store with url "<StoreUrl>"
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "wheel" and shop for all tires/wheels
    And  I extract the product at position "2" from "PLP"
    And  I select the "3rd" product result image on PLP page with "Add to Cart" button
    And  I click on the "Add to Cart" button
    Then I verify suggested selling carousel inner-tile for "Add to Cart" is "displayed" with title "<Title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "Add to cart" page

    Examples:
      | StoreUrl                  | Email                | password  | Year | Make   | Model    | Trim     | Assembly |  Title                   |
      | /store/az/phoenix/s/1028  | autouser_a@gmail.com | Discount1 |2012  | Honda  | Civic    | Coupe DX | none     | CUSTOMERS ALSO PURCHASED |

  @dt
  @at
  @web
  @20796
  @suggestedSelling
  Scenario Outline: HYBRIS_CART_SHOPPING CART_Suggested Selling_Verify the carousel of suggestions is getting displayed on Items Added to Cart modal when Tire is added to cart_Registered(ALM#20796)
    When I change to the store with url "<StoreUrl>"
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    And  I go to the homepage
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I extract the product at position "2" from "PLP"
    And  I select the "3rd" product result image on PLP page with "Add to Cart" button
    And  I click on the "Add to Cart" button
    Then I verify suggested selling carousel inner-tile for "Add to Cart" is "displayed" with title "<Title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "Add to cart" page

    Examples:
      | StoreUrl                  | Email                | password  | Year | Make   | Model    | Trim     | Assembly |  Title                   |
      | /store/az/phoenix/s/1028  | autouser_a@gmail.com | Discount1 |2012  | Honda  | Civic    | Coupe DX | none     | CUSTOMERS ALSO PURCHASED |

  @dt
  @at
  @web
  @20745
  @suggestedSelling
  Scenario Outline: HYBRIS_SEARCH_SEARCH_SUGGESTED SELLING_Homepage_Verify SS product Carousel for standard vehicle_Registered User_No suggestion for previously removed products(ALM#20745)
    When I change to the store with url "<StoreUrl>"
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    And  I close popup modal
    Then I verify suggested selling carousel inner-tile for "Homepage" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    When I select the "2nd" product result image on "Homepage" page
    Then I verify the "PDP" banner color is "Green"
    When I add item to my cart and "View shopping Cart"
    And  I extract "ProductName" from shopping cart as "product name"
    And  I select mini cart and "Clear Cart"
    And  I click the discount tire logo
    Then I am brought to the homepage
    And  I verify the product in cart is not displayed in the "Homepage" carousel

    Examples:
      | StoreUrl                 | Email                | password  | Year | Make  | Model | Trim     | Assembly | title                                      |
      | /store/az/phoenix/s/1028 | autouser_a@gmail.com | Discount1 | 2012 | Honda | Civic | Coupe DX | none     | "Trending near [City,State (abbreviated)]" |

  @dt
  @at
  @web
  @20811
  @suggestedSelling
  Scenario Outline: HYBRIS_CART_SHOPPING CART_Suggested Selling_Verify the carousel of suggestions is getting displayed on Items Added to Cart modal when Tire is browsed via Check Availability Modal_Registered(ALM#20811)
    When I change to the store with url "<StoreUrl>"
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I extract the product at position "2" from "PLP"
    And  I select the "3rd" product result image on PLP page with "Add to Cart" button
    And  I click on the "Check nearby stores" link
    Then I should verify that the Check Availability popup loaded
    When I click on the "Add to Cart" button
    Then I verify suggested selling carousel inner-tile for "Add to Cart" is "displayed" with title "<Title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "Add to cart" page

    Examples:
      | Title                    | Year | Make  | Model | Trim     | Assembly | Email                | password  | StoreUrl                 |
      | CUSTOMERS ALSO PURCHASED | 2012 | Honda | Civic | Coupe DX | none     | autouser_a@gmail.com | Discount1 | /store/az/phoenix/s/1028 |

  @dt
  @at
  @web
  @20813
  @suggestedSelling
  Scenario Outline: HYBRIS_CART_SHOPPING CART_Suggested Selling_Verify the carousel of suggestions is getting displayed on Items Added to Cart modal when Wheel is browsed via Check Availability Modal(ALM#20813)
    When I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I select mini cart and "Clear Cart"
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I extract the product at position "1" from "PLP"
    And  I go to the homepage
    And  I click on the "<Year> <Make>" button
    And  I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the PLP header message contains "wheel result"
    When I select the "3rd" product result image on PLP page with "Add to Cart" button
    And  I click on the "check nearby stores" link
    Then I should verify that the Check Availability popup loaded
    When I click on the "Add to Cart" button
    Then I verify suggested selling carousel inner-tile for "Add to Cart" is "displayed" with title "<Title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "Add to cart" page
    And  I verify the product is displayed at position "1" in the 'suggested selling carousel'

    Examples:
      | Title                    | Year | Make  | Model | Trim     | Assembly | Email                | password  |
      | CUSTOMERS ALSO PURCHASED | 2012 | Honda | Civic | Coupe DX | none     | autouser_a@gmail.com | Discount1 |

  @dt
  @at
  @web
  @20795
  @suggestedSelling
  Scenario Outline: HYBRIS_UI_UI_PDP_Suggested Selling_Verify the carousel of suggestions for wheels getting displayed is matching the new browsed tire size considering fitment for My Account users (ALM#20795)
    When I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I select mini cart and "Clear Cart"
    And  I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select "Continue & Clear Current Cart" in the Switching vehicle popup
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the PLP header message contains "wheel result"
    When I select the "1st" product result image on PLP page with "Add to Cart" button
    And  I click on the "view details" link
    And  I add item to my cart and "View shopping Cart"
    And  I extract "ProductName" from shopping cart as "product name"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click on the "Continue to Customer Details" button
    And  I click on the "Continue To Payment" button
    And  I enter payment info with a different billing address and confirm Checkout Summary as "<Bopis Customer>"
    And  I place the order for "<Bopis Customer>"
    Then I am brought to the order confirmation page
    When I open the My Vehicles popup
    Then I verify "<Year>" "<Make>" "<Model>" "<Trim>" vehicle is "present" in the saved vehicles at position "1"
    When I go to the homepage
    And  I open the My Vehicles popup
    And  I click on the "shop products" link
    And  I "expand" "optional tire and wheel size"
    And  I select a fitment option "<SizeOption>"
    And  I select a fitment option "<TireSize>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select the "1st" product result image on PLP page with "Add to Cart" button
    Then I verify the product in cart is not displayed in the "PDP" carousel
    And  I verify suggested selling carousel inner-tile for "PDP" is "displayed" with title "<Title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the products in the carousel are of "<Size>"

    Examples:
      | Title                      | Year | Make  | Model | Trim     | Assembly | Email                | password  | Bopis Customer              | SizeOption | TireSize  | Size      | Credit Card |
      | CUSTOMERS ALSO SHOPPED FOR | 2012 | Honda | Civic | Coupe DX | none     | autouser_a@gmail.com | Discount1 | DEFAULT_CUSTOMER_BOPIS_VISA | 16”        | 195/55-16 | 195/55-16 | Visa Bopis  |

  @dt
  @at
  @web
  @20756
  @suggestedSelling
  Scenario Outline: HYBRIS_SEARCH_SEARCH_SUGGESTED SELLING_Homepage_Verify SS product Carousel for standard vehicle_Guest User_Suggestions should match based on size (ALM#20756)
    When I change to the store with url "<StoreUrl>"
    And  I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I select keep me signed-in option
    And  I click on the "Sign-in" button
    Then I should see my "first" name displayed on homepage header
    When I open the My Vehicles popup
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I delete "all" saved vehicles
    And  I close popup modal
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I extract the "Tire" size on the home page
    Then I verify suggested selling carousel inner-tile for "Homepage" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    When I extract the product at position "3" from "Homepage"
    And  I select the "3" product result image on "Homepage" page
    Then I verify the "PDP" banner color is "Green"
    And  I verify extracted "Tire" size with size on PDP page
    And  I verify product info on the PDP page

    Examples:
      | StoreUrl                 | Email                | password  | Year | Make   | Model | Trim     | Assembly | title                                      |
      | /store/az/phoenix/s/1028 | autouser_a@gmail.com | Discount1 | 2012 | Honda  | Civic | Coupe DX | none     | "TRENDING NEAR [CITY,STATE (ABBREVIATED)]" |