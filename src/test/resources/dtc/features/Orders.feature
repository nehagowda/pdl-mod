@orders
Feature: Orders

  Background:
    Given I change to the default store

  @at
  @web
  @8426
  @reservationVehicleMultiProductPromotion
  Scenario Outline: HYBRIS_ORDER_SALES_AT Hard Reservation_ Vehicle_multi-product_promotion_7 (ALM #8426)
  """first data table TODO: For validation of logic in STG (e.g. not the data requested to be used in ALM testcase)
    second data table TODO: Data Defect 6599
    | 2012 | Honda | Civic | Coupe DX | none     | All tires     | Results for Tires | 34299          | Defender A/S   | 20464          | DR-69          | default_customer_az |"""
    When I search for store within "25" miles of "92324"
    And  I select "Make This My Store" for store #"1" in the location results
    And  I open the fitment popup
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "<Header>"
    When I do a free text search for "<Product A Code>"
    And  I select "<Product A Name>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<Product A Name>"
    When I add item to my cart and "Continue Shopping"
    And  I do a free text search for "<Product B Code>"
    And  I select "<Product B Name>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<Product B Name>"
    When I add item to my cart and "View shopping Cart"
    Then I should see product "<Product A Code>" on the "cart" page
    And  I should see product "<Product B Code>" on the "cart" page
    When I select the checkout option "with appointment"
    And  I schedule an appointment for "<Customer>"
    And  I reserve items for "<Customer>"
    Then I store the order number

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Header      | Product A Code | Product A Name     | Product B Code | Product B Name | Customer            |
      | 2012 | Honda | Civic | Coupe DX | none     | tire result | 29935          | Silver Edition III | 23500          | KNIGHT         | default_customer_az |

  @web
  @dtd
  @6966
  @C006
  @mobile
  @nexusTax
  @nexusTaxSit
  @ordersRegression
  Scenario Outline: Verify Taxes for web Order created with Canadian Shipping Address having Tire (ALM # 6966)
    """The amount of sale passed incorrect in order xml for Canada customer, prod defect # OEES-1022"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "<ShipOption>" as "<Customer>"
    And  I extract the "sales tax" on the checkout page for "<Customer>"
    And  I enter payment info and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I verify the sales tax on order confirmation page matches with "checkout" sales tax for "<Customer>"
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | shipMethod     |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml payment node values
    And  I assert order xml customer node values
    And  I assert order xml subItems

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Checkout | ShipOption   | Customer             |
      | 2012 | Honda | Civic | Coupe DX | none     | default  | Next Day Air | default_customer_can |

  @web
  @dtd
  @9634
  @9635
  @ordersRegression
  @ordersRegression_APO_FPO
  Scenario Outline: Checkout More Shipping Options FPO/APO Shipping Address_4 (ALM # 9635,9634)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    Then I verify "Ground - Free" does not appear in the Delivery Method section
    When I click on the "Continue To Payment" button
    And  I enter payment info and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml payment node values
    And  I assert order xml customer node values
    And  I assert order xml subItems

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Checkout | Customer     |
      | 2012 | Honda | Civic | Coupe DX | none     | default  | fpo_customer |
      | 2012 | Honda | Civic | Coupe DX | none     | default  | apo_customer |

  @dtd
  @web
  @8226
  @ordersRegression
  Scenario Outline: HYBRIS_ORDERS_CHECKOUT_DTD_VantivPaymentChanges_CarCareOne (ALM #8226)
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    And  I add item to my cart and "View shopping Cart"
    And  I select the optional "Certificates" fee for item
    And  I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I "accept" credit card disclosure consent
    And  I "accept" credit card terms of agreement
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | shipMethod     |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml payment node values
    And  I assert order xml customer node values
    And  I assert order xml subItems

    Examples:
      | ItemCode | ProductName        | Checkout | Customer              | ShippingOption | Credit Card |
      | 29935    | Silver Edition III | default  | car_care_one_customer | Ground         | CarCareOne  |

  @web
  @dtd
  @9307
  @ordersRegression
  Scenario Outline: Checkout with More Shipping Options US Regular/Staggered (ALM #9307)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | webOrderOrigin |
      | siteNumber     |
      | amountOfSale   |
      | orderType      |
      | customerType   |
      | shipMethod     |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml payment node values
    And  I assert order xml customer node values
    And  I assert order xml subItems

    Examples:
      | Year | Make      | Model     | Trim     | Assembly | Checkout | Customer            | ShippingOption  |
      | 2012 | Honda     | Civic     | Coupe DX | none     | default  | default_customer_az | Ground          |
      | 2010 | Chevrolet | Corvette  | Base     | none     | default  | default_customer_az | Second Day Air  |

  @web
  @dtd
  @12490
  @12531
  @12532
  @regression
  Scenario Outline: Validate Order Details in Confirmation Page After Order is Placed Successfully_DTD (ALM # 12490,12531,12532)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    Then I confirm the shipping options are: "<AvailableOptions>"
    When I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    And  I extract the "sales tax" on the checkout page for "<Customer>"
    And  I extract the "order total" on the checkout page for "<Customer>"
    And  I enter payment info and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    And  I verify customer "<Customer>" details are listed on the "order confirmation" page
    And  I verify the order total on order confirmation page matches with "checkout" order total
    And  I verify the sales tax on order confirmation page matches with "checkout" sales tax for "<Customer>"
    When I expand the fee details for the item listed on the order confirmation page
    And  I select survey feedback on order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I navigate to newly opened next tab
    Then I should see "<SurveyLink>" url is launched
    When I navigate to previous browser tab
    And  I close open tabs

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ProductName        | ItemCode | Checkout | Customer            | ShippingOption | SurveyLink          | AvailableOptions                     |
      | 2012 | Honda | Civic | Coupe DX | none     | Silver Edition III | 29935    | default  | default_customer_az | Ground         | websurvey/2/execute | Ground, Second Day Air, Next Day Air |

  @dt
  @at
  @web
  @C007
  @C008
  @13872
  @mobile
  @nexusTax
  @regression
  @nexusTaxSit
  Scenario Outline: Validate Order Details in Confirmation Page After Order is Placed Successfully_DT (ALM # 13872)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I select from the "Quick Filters" filter section, "single" option(s): "In Stock"
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I select install with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    Then I verify date and time on the customer details appointment page
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I verify date and time on the order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |

    Examples:
      | Year | Make  | Model | Trim     | Assembly | Checkout | Customer                    | Credit Card |
      | 2012 | Honda | Civic | Coupe DX | none     | default  | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @at
  @dt
  @web
  @bvt
  @15848
  @15885
  @mobile
  @bvtOrders
  @ordersRegressionStg
  @extendedAssortmentOrders
  @bvtScenarioOrders_ExtendedAssortment
  Scenario Outline: HYBRIS_ORDERS_ORDERS_EXTENDEDASSORTMENT_Validate checkout flow when I add tires from secondary suppliers with 2 brands having regular fitment (ALM #15848)
    When I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<TireSize>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    And  I add item "<ItemCodeB>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders
#    And  I save the order number to the "ExtendedAssortment" excel with EA flag "YES"

    Examples:
      | Year | Make      | Model          | Trim         | Assembly | TireSize       | ItemCodeA | ItemCodeB | Customer                    | Credit Card |
      | 2014 | Chevrolet | Silverado 1500 | Crew Cab 4WD | none     | 265 /70 R17 SL | 31227     | 10845     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @at
  @dt
  @web
  @15834
  @15895
  @mobile
  @ordersRegressionStg
  @extendedAssortmentOrders
  Scenario Outline: HYBRIS_ORDERS_ORDERS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tire available in secondary supplier and wheels avaialble in my store added with Regular fitment (ALM #15834)
    When I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCodeA>" of type "none" to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCodeB>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders
#    And  I save the order number to the "ExtendedAssortment" excel with EA flag "YES"

    Examples:
      | Year | Make | Model | Trim         | Assembly       | ItemCodeA | ItemCodeB | Customer                    | Credit Card |
      | 2016 | Ram  | 2500  | MEGA CAB 4WD | 275 /70 R18 E1 | 31931     | 23985     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @at
  @dt
  @web
  @15704
  @15885
  @mobile
  @ordersRegressionStg
  @extendedAssortmentOrders
  Scenario Outline: HYBRIS_ORDERS_ORDERS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tire added with quantity less than or equal to 8 from secondary supplier and Tires from my store with regular fitment (ALM #15704)
    When I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCodeB>" of type "none" to my cart and "Continue Shopping"
    And  I add item "<ItemCodeA>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    When I update quantity for item "<ItemCodeB>" to "<UpdatedQuantity>"
    Then I verify quantity for "<ItemCodeB>" is set to "<UpdatedQuantity>" in the "cart"
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders
#    And  I save the order number to the "ExtendedAssortment" excel with EA flag "YES"

    Examples:
      | Year | Make   | Model | Trim | Assembly | ItemCodeA | ItemCodeB | UpdatedQuantity | Customer                    | Credit Card |
      | 2016 | Toyota | Camry | XLE  | none     | 31263     | 24824     | 8               | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @at
  @dt
  @web
  @15835
  @15897
  @mobile
  @ordersRegressionStg
  @extendedAssortmentOrders
  Scenario Outline: HYBRIS_ORDERS_ORDERS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tire available in secondary supplier and wheels avaialble in my store added with Staggered fitment (ALM #15835)
    When I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment
    When I add item "<ItemCodeA>" of type "sets" to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCodeC>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I should see product "<ItemCodeC>" on the "cart" page
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders
#    And  I save the order number to the "ExtendedAssortment" excel with EA flag "YES"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | ItemCodeC | Customer                    | Credit Card |
      | 2010 | Chevrolet | Corvette | Base | none     | 14175     | 14181     | 70330     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @at
  @dt
  @web
  @15836
  @15887
  @mobile
  @ordersRegressionStg
  @extendedAssortmentOrders
  Scenario Outline: HYBRIS_ORDERS_ORDERS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tires available in secondary supplier added with Staggered fitment (ALM #15836)
    When I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment
    When I add item "<ItemCodeA>" of type "sets" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    When I update quantity for item "<ItemCodeA>" to "<UpdatedQuantity>"
    And  I update quantity for item "<ItemCodeB>" to "<UpdatedQuantity>"
    Then I verify quantity for "<ItemCodeA>" is set to "<UpdatedQuantity>" in the "cart"
    And  I verify quantity for "<ItemCodeB>" is set to "<UpdatedQuantity>" in the "cart"
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders
#    And  I save the order number to the "ExtendedAssortment" excel with EA flag "YES"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | UpdatedQuantity | Customer                    | Credit Card |
      | 2012 | Chevrolet | Corvette | Base | none     | 14175     | 14181     | 2               | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @at
  @dt
  @web
  @15971
  @15887
  @mobile
  @ordersRegressionStg
  @extendedAssortmentOrders
  Scenario Outline: HYBRIS_ORDERS_ORDERS_EXTENDEDASSORTMENT_Validate checkout flow when I add tires from secondary suppliers with 2 brands having staggered fitment (ALM #15971)
    When I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "staggered" fitment
    When I add item "<ItemCodeA>" of type "sets" to my cart and "Continue Shopping"
    And  I add item "<ItemCodeC>" of type "sets" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I should see product "<ItemCodeC>" on the "cart" page
    And  I should see product "<ItemCodeD>" on the "cart" page
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders
#    And  I save the order number to the "ExtendedAssortment" excel with EA flag "YES"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | ItemCodeC | ItemCodeD | Customer                    | Credit Card |
      | 2012 | Chevrolet | Corvette | Base | none     | 14175     | 14181     | 36241     | 36259     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @at
  @dt
  @web
  @15919
  @emailRegression
  @extendedAssortmentEmail
  Scenario Outline: HYBRIS_ORDERS_ORDERS_EXTENDEDASSORTMENT_REGULAR VEHICLE_VALIDATE INSTALATION APPOINTMENT CONFIRMATION EMAIL_SECONDARY SUPPLIER(TIRES) (ALM #15919)
    When I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout store name matches my store on shopping cart
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders
    When I store the order number
    And  I login to email for the "<Customer>"
    Then I confirm customer receives an email for the "Order Confirmation"
    And  I verify "STORE DETAILS" is displayed in "Order Confirmation" email
    And  I verify customer name for "<Customer>" in "Order Confirmation" email
    And  I verify "<ItemCode>" is displayed in "Order Confirmation" email
    When I launch the "Appointment" URL
    Then I verify "APPOINTMENT DETAILS" section is displayed and active
    When I select first available appointment date
    And  I select 'CONFIRM APPOINTMENT' on Installation Appointment page
    Then I am brought to the appointment confirmation page
    And  I verify "Order Number" is displayed in appointment confirmation page
    And  I verify "Appointment Details" is displayed in appointment confirmation page
    And  I verify "<ItemCode>" is displayed in appointment confirmation page
    When I launch the "Email" URL
    Then I confirm customer receives an email for the "Appointment Confirmation"
    And  I verify "STORE DETAILS" is displayed in "Appointment Confirmation" email
    And  I verify customer name for "<Customer>" in "Appointment Confirmation" email
    And  I verify "<ItemCode>" is displayed in "Appointment Confirmation" email

    Examples:
      | Year | Make | Model | Trim  | Assembly       | ItemCode | Customer                    | Credit Card |
      | 2013 | BMW  | 335i  | Sedan | 225 /45 R18 SL | 17111    | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @dtd
  @web
  @6950
  @mobile
  @nexusTax
  @ordersRegression
  Scenario Outline: HYBRIS_ORDERS_ORDERS_AVS_Verify the Tax Estimation message displayed when the AVS response invalid (ALM#6950)
    When I do a free text search for "<ItemCode>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I enter shipping info as "<Customer>" and continue to next page
    Then I verify Tax Estimation Message is displayed for invalid response from "AVS Service" on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I verify Tax Estimation Message is displayed for invalid response from "AVS Service" on "Order Confirmation" page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | shipMethod     |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml payment node values
    And  I assert order xml customer node values
    And  I assert order xml subItems

    Examples:
      | ItemCode | Customer             | ShippingOption | Credit Card      |
      | 29935    | default_customer_nv2 | Ground         | MasterCard Bopis |

  @dtd
  @web
  @11009
  @mobile
  @nexusTax
  Scenario Outline: HYBRIS_SHOPPINGCART_Cart Checkout_Validate Environmental fee and taxes when the user performs checkout through valid zipcode_DTD_standard/staggered (ALM # 11009)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    Then I verify the "Estimated Environmental Fee" label displayed on "Shopping cart" page
    And  I verify the "Estimated Taxes" label displayed on "Shopping cart" page
    When I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify the "Est. Enviro. Fee" label displayed on "Checkout" page
    And  I verify the "Estimated Taxes" label displayed on "Checkout" page
    When I enter shipping info as "<Customer>"
    And  I set "Address Line 1" to "<NewAddressLine1>"
    And  I set "Zip / Postal Code" to "<NewZip>"
    And  I submit the updated address information and "check for AVS popup"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify the "Enviro. Fee" label displayed on "Checkout" page
    And  I verify the "Estimated Taxes" label displayed on "Checkout" page
    When I select shipping option: "<ShippingOption>" as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify the "Enviro. Fee" label displayed on "Checkout" page
    And  I verify the "Taxes" label displayed on "Checkout" page
    When I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    And  I expand the fee details for the item listed in the cart summary on the Checkout page
    Then I verify the "Enviro. Fee" label displayed on "Checkout" page
    And  I verify the "Taxes" label displayed on "Checkout" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I verify the "Environmental Fee" label displayed on "Order Confirmation" page
    And  I verify the "Estimated Taxes" label displayed on "Order Confirmation" page

    Examples:
      | Year | Make      | Model    | Trim         | Assembly       | Customer            | NewAddressLine1 | NewZip     | ShippingOption | Credit Card      |
      | 2016 | Ram       | 2500     | MEGA CAB 4WD | 275 /70 R18 E1 | default_customer_az | W 67 Elwood St  | 85040-1025 | Ground         | MasterCard Bopis |
      | 2012 | Chevrolet | Corvette | Base         | none           | default_customer_az | W 67 Elwood St  | 85040-1025 | Ground         | MasterCard Bopis |

  @dtd
  @web
  @bvt
  @core
  @6619
  @bvtOrders
  @ordersRegression
  @coreScenarioOrders
  @bvtScenarioPayPalFromCheckoutPage
  Scenario Outline: Checkout with Paypal verifying non-editable customer fields (ALM #6619)
    When I go to the homepage
    And  I do a "homepage" vehicle search for "<VehicleType>" vehicle type
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "<Checkout>"
    And  I enter shipping info as "<Customer>" and continue to next page
    And  I select the default shipping option as "<Customer>"
    And  I select the "<Checkout>" payment option
    And  I select paypal checkout
    And  I switch to "Paypal" window
    And  I log into paypal as "<Customer>"
    And  I continue with the paypal payment
    And  I switch to main window
    Then I am brought to the order confirmation page
    And  I save paypal order details
    And  I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | shipMethod     |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert paypal order xml payment node values
    And  I assert order xml customer node values
    And  I assert order xml subItems

    Examples:
      | VehicleType             | Checkout | Customer           |
      | VEHICLE_NON_STAGGERED_1 | paypal   | paypal_customer_az |
      | VEHICLE_STAGGERED_1     | paypal   | paypal_customer_az |

  @at
  @dt
  @web
  @bvt
  @15837
  @15885
  @mobile
  @bvtOrders
  @ordersRegressionStg
  @extendedAssortmentOrders
  @bvtScenarioOrders_ExtendedAssortment
  Scenario Outline: HYBRIS_ORDERS_ORDERS_EXTENDEDASSORTMENT_Validate checkout flow when I have Tire available in secondary supplier added with Regular fitment (ALM #15837)
    When I change to EA default store through url
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the product list page is displayed having "non-staggered" fitment
    When I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    And  I update quantity for item "<ItemCode>" to "<UpdatedQuantity>"
    Then I verify quantity for "<ItemCode>" is set to "<UpdatedQuantity>" in the "cart"
    When I extract my store name on the cart page
    And  I extract the "sales tax" on the cart page
    And  I extract the "order total" on the cart page
    And  I select the checkout option "none"
    Then I should see the customer information page for checkout
    And  I verify the checkout sales tax matches with sales tax amount on shopping cart
    And  I verify the checkout order total matches with shopping cart order total
    And  I verify the installation appointment message on "Checkout" page for extended assortment orders
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify the installation appointment message on "Order Confirmation" page for extended assortment orders

    Examples:
      | Year | Make   | Model | Trim  | Assembly | ItemCode | UpdatedQuantity | Customer                          | Credit Card      |
      | 2016 | Toyota | Camry | XLE   | none     | 19954    | 4               | DEFAULT_CUSTOMER_BOPIS_VISA       | Visa Bopis       |
      | 2016 | Toyota | Camry | XLE   | none     | 19954    | 8               | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis |

  @dt
  @at
  @web
  @11653
  @ordersRegression
  @bopisOrderCancelFromWeb
    Scenario Outline: BOPIS Order Cancel from Web - Guest Customer (ALM#11653)
    """ Failing on customer address validation due to billing address being sent to POS and ESB as the customer address.
     'I assert order xml customer node values' will be commented out until resolved. """
    """Order cancelled validation steps yet to be added"""
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add "in stock" item of type "none" to my cart and "View shopping Cart"
    And  I open the My Vehicles popup
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I extract date and time for validation
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I click on the "Place Order" button
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    And  I wait for "10" seconds
    And  I read the order xml file
    Then I assert order xml node values
      | webOrderId           |
      | amountOfSale         |
      | siteNumber           |
      | orderType            |
      | customerType         |
      | webOrderOrigin       |
      | vehicle              |
      | appointmentDate      |
      | appointmentStartTime |
    And  I assert order xml list values
      | tax          |
      | productCode  |
      | discount     |
    And  I assert order xml payment node values
#    And  I assert order xml customer node values
    And  I assert order xml subItems
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
      | appointmentId        |
      | appointmentEndTime   |
    When I get order xml node value
      | cancellationURL      |
    And  I launch the "orderCancellation" URL
    Then I am brought to the order "cancellation request" page
    When I select the reason "<reason>" for cancellation
    And  I select cancel order
    Then I am brought to the order "cancellation complete" page

    Examples:
      | Year | Make   | Model | Trim  | Assembly | Customer                    | Credit Card | reason                                  |
      | 2016 | Toyota | Camry | XLE   | none     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | Cancelling to order a different product |

  @dt
  @at
  @web
  @20874
  Scenario Outline: HYBRIS_ORDERS_ORDERS_Shop For Wheels_standard Vehicle with Fitment(ALM#20874)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "shop for wheels"
    And  I add item "<ItemCodeB>" of type "none" to my cart and "none"
    Then I should see Cart Popup has "Continue Shopping" option displayed
    When I select the view shopping cart button
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    When I select the checkout option "default"
    And  I select install without appointment
    And  I select the checkout without install reason "<Reason>"
    And  I click on the "Continue to Customer Details" button
    And  I populate all the required fields for "<Customer1>"
    And  I click on the "Continue to Payment" button
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I am brought to the order confirmation page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCodeA | ItemCodeB | Customer1           | Reason                              |
      | 2012 | Honda | Civic | Coupe EX | none     | 19735     | 22690     | DEFAULT_CUSTOMER_AZ | Make an appointment at a later time |

  @dtd
  @web
  @20874
  Scenario Outline: HYBRIS_ORDERS_ORDERS_Shop For Wheels_standard Vehicle with Fitment_DTD(ALM#20874)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "none" to my cart and "shop for wheels"
    And  I add item "<ItemCodeB>" of type "none" to my cart and "none"
    Then I should see Cart Popup has "Continue Shopping" option displayed
    When I select the view shopping cart button
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    When I select the checkout option "default"
    And  I populate all the required fields for "<Customer1>"
    And  I click on the "Continue To Shipping Method" button
    And  I select shipping option: "<ShippingOption>" as "<Customer1>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer1>"
    Then I am brought to the order confirmation page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | ItemCodeA | ItemCodeB | Customer1           | ShippingOption | Customer                    | Credit Card |
      | 2012 | Honda | Civic | Coupe EX | none     | 19735     | 22690     | DEFAULT_CUSTOMER_AZ | Ground         | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @at
  @dt
  @web
  @20592
  @ordersRegression
  @staggeredExperience
  Scenario Outline: HYBRIS_ORDERS_STAGGERED EXPERIENCE_With Certificates_Create Order_TIRES(ALM #20592)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the optional "Certificates" fee for item
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    Then I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml payment node values
    And  I assert order xml subItems
    And  I assert order xml customer node values

    Examples:
      | Year | Make   | Model | Trim       | Assembly       | Customer                    | Credit Card            |
      | 2010 | Nissan | 370z  | Coupe Base | 245 /40 R19 SL | DEFAULT_CUSTOMER_BOPIS_AMEX | American Express Bopis |

  @at
  @dt
  @web
  @20592
  @ordersRegression
  @staggeredExperience
  Scenario Outline: HYBRIS_ORDERS_STAGGERED EXPERIENCE_Create Order_WHEELS(ALM #20598)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "wheel" and shop for all tires/wheels
    And  I add item to my cart and "View shopping Cart"
    And  I select the checkout option "default"
    And  I select install with appointment
    And  I select first available appointment date
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    When I extract and save the sales tax for "<Customer>" on order confirmation page
    Then I read the order xml file
    And  I assert order xml node values
      | webOrderId     |
      | amountOfSale   |
      | siteNumber     |
      | orderType      |
      | customerType   |
      | webOrderOrigin |
      | vehicle        |
    And  I assert order xml list values
      | productCode  |
      | discount     |
      | tax          |
    And  I assert order xml node value is not null
      | transactionTimestamp |
      | emailAddress         |
    And  I assert order xml payment node values
    And  I assert order xml subItems
    And  I assert order xml customer node values

    Examples:
      | Year | Make     | Model | Trim                 | Assembly | Customer                    | Credit Card |
      | 2015 | Cadillac | CTS   | Sedan 3.6 Vsport RWD | none     | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |