@bvtPdp
Feature: Product Details Page

  Background:
    Given I change to the default store

  @dt
  @web
  @Pdp
  @bvt
  @17799
  @core
  @canonicalPDP
  @canonicalPLPPDP
  @coreScenarioPLPPDP
  @PDP_verifyCanonicalPDPAttributesAndAllPopups
  @bvtScenarioCanonicalPDPverifyPDPDetailsVehicleSearch
  Scenario Outline: Verify attributes and all popups on PDP page with vehicle fitment (ALM#17799)
  """ Some items won't have OE Designation in Back Office. Developers will provide API for all data for specified
    item IDs  (Story OCW1493). Then we can go to PDP page with a valid ID via View Details using Product Name associated
    with that Item ID. On PDP, select the correct size dimensions for that Item ID. Then validate OE Designation message
    reliably. Until then, OE Designation validation steps will be commented out. """
    When I open the "TIRES" navigation link
    And  I click the "TIRE SEARCH" menu option
    And  I click on the "continue without vehicle" link
    And  I click on the "All-Season tires" link
    And  I click on the "SHOP ALL" link
    Then I verify Canonical products are "displayed" on PLP page
    When I click on the "Enter vehicle" link
    And  I do a "CheckFit" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Tire/Wheel Size>"
    And  I click on the "CONTINUE" button
    And  I select the first available View Details from the results page
    Then I verify price range on PDP page
    When I set first available size options on PDP page
    Then I verify PDP has single price
    And  I verify the product availability section is displayed
    And  I verify add to cart button is "ENABLED"
    And  I verify calculation of subtotal using qty and retail price on PDP
    When I click "what is Load Range" element in PDP
    Then I verify "Load Range" description in popup
    When I close "Load Range" popup in PDP
    And  I click "Load Index / Speed Rating" element in PDP
    Then I verify "Load Index" description in popup
    When I close "Load Index" popup in PDP
 #    And  I click "what is OE Designation" element in PDP
 #    Then I verify "OE Designation" description in popup
 #    When I close "OE Designation" popup in PDP
    And  I "decrease" the quantity in "PDP" by "1"
    Then I verify calculation of subtotal using qty and retail price on PDP
    When I open the "WHEELS" navigation link
    And  I click the "WHEELS BY BRAND" menu option
    And  I click on the "<Make> <Model>" link
    And  I click on the "CONTINUE WITH THIS VEHICLE" link
    And  I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    And  I close popup modal
    And  I open the "WHEELS" navigation link
    And  I click the "WHEELS BY BRAND" menu option
    And  I do a "my vehicles" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Tire/Wheel Size>"
    And  I select a fitment option "wheel"
    And  I select the first available View Details from the results page
    And  I set first available size options on PDP page
    Then I verify add to cart button is "ENABLED"
    When I "decrease" the quantity in "PDP" by "1"
    Then I verify calculation of subtotal using qty and retail price on PDP

    Examples:
      | Year | Make | Model | Trim         | Tire/Wheel Size |
      | 2016 | Ram  | 2500  | Mega Cab 4WD | 275 /70 R18 E1  |

  @dt
  @at
  @dtd
  @web
  @20664
  @suggestedSelling
  Scenario Outline: HYBRIS_UI_UI_PDP_Suggested Selling_Verify the carousel of suggestions is getting displayed on PDP when tire is browsed with Vehicle in Session_Guest (ALM#20664)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I select the "3rd" product result image on PLP page with "Add to Cart" button
    Then I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    And  I verify suggested selling carousel inner-tile for "PDP" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "PDP" page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | title                      |
      | 2012 | Honda | Civic | Coupe DX | none     | CUSTOMERS ALSO SHOPPED FOR |

  @dt
  @at
  @dtd
  @web
  @20665
  @suggestedSelling
  Scenario Outline: HYBRIS_ UI_UI_PDP_Suggested Selling_Verify the carousel of suggestions is getting displayed on PDP when Wheel is browsed with Vehicle in Session_Guest (ALM#20665)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the PLP header message contains "wheel result"
    When I select the "3rd" product result image on PLP page with "Add to Cart" button
    Then I verify the message on the "PDP" banner contains "These wheels fit your vehicle"
    And  I verify suggested selling carousel inner-tile for "PDP" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "PDP" page

    Examples:
      | Year | Make  | Model | Trim     | Assembly | title                      |
      | 2012 | Honda | Civic | Coupe DX | none     | CUSTOMERS ALSO SHOPPED FOR |

  @dt
  @at
  @web
  @dtd
  @20634
  @suggestedSelling
  Scenario Outline: HYBRIS_UI_CONTENT_Suggested Selling_Verify the carousel of suggestions is not getting displayed on PDP when there is no vehicle in session (ALM#20634)
    When I go to the homepage
    And  I do a free text search for "<ItemCode>" and hit enter
    Then I verify the "<Breadcrumb>" link in the breadcrumb container
    And  I verify suggested selling carousel inner-tile for "PDP" is "not displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "PDP" page

    Examples:
      | ItemCode | Breadcrumb                     | title |
      | 34302    | Tires,Michelin,Product Details | none  |

  @dt
  @at
  @web
  @20816
  @suggestedSelling
  Scenario Outline: HYBRIS_UI_UI_PDP_Suggested Selling_Verify the carousel of suggestions is getting displayed on PDP when Tire is browsed and store has been changed_Site Swap(ALM#20816)
    When I change to the store with url "<StoreUrl>"
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I select the "3rd" product result image on PLP page with "Add to Cart" button
    Then I verify the message on the "PDP" banner contains "These tires fit your vehicle"
    And  I verify suggested selling carousel inner-tile for "PDP" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "PDP" page
    When I click on the "change store" link
    Then I am brought to the page with header "Change Store"
    When I save new store address in store popup
    And  I click on element name "Make my store" in change store popup
    Then I am brought to the page with header "STORE CHANGED"
    When I click on continue for successful change of store
    Then I verify suggested selling carousel inner-tile for "PDP" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "PDP" page

    Examples:
      | StoreUrl                 | Year | Make  | Model | Trim     | Assembly | title                      |
      | /store/az/phoenix/s/1028 | 2012 | Honda | Civic | Coupe DX | none     | CUSTOMERS ALSO SHOPPED FOR |

  @dt
  @at
  @web
  @20817
  @suggestedSelling
  Scenario Outline: HYBRIS_UI_UI_PDP_Suggested Selling_Verify the carousel of suggestions is getting displayed on PDP when Wheel is browsed and store has been changed_Site Swap(ALM#20817)
    When I change to the store with url "<StoreUrl>"
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "wheel" and shop for all tires/wheels
    Then I verify the PLP header message contains "wheel result"
    When I select the "3rd" product result image on PLP page with "Add to Cart" button
    Then I verify the message on the "PDP" banner contains "These wheels fit your vehicle"
    And  I verify suggested selling carousel inner-tile for "PDP" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "PDP" page
    When I click on the "change store" link
    Then I am brought to the page with header "Change Store"
    When I save new store address in store popup
    And  I click on element name "Make my store" in change store popup
    Then I am brought to the page with header "STORE CHANGED"
    When I click on continue for successful change of store
    Then I verify suggested selling carousel inner-tile for "PDP" is "displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    And  I verify that the suggested selling carousel does not display the selected product on "PDP" page

    Examples:
      | StoreUrl                 | Year | Make  | Model | Trim     | Assembly | title                      | ItemCode |
      | /store/az/phoenix/s/1028 | 2012 | Honda | Civic | Coupe DX | none     | CUSTOMERS ALSO SHOPPED FOR | 71585    |

  @dt
  @at
  @web
  @dtd
  @20633
  @suggestedSelling
    Scenario Outline: HYBRIS_CART_SHOPPING CART_Suggested Selling_Verify the carousel of suggestions is not getting displayed on Items Added to Cart modal when there is no vehicle in session(ALM#20633)
    When I go to the homepage
    And  I do a free text search for "<ItemCode>" and hit enter
    Then I verify the "<Breadcrumb>" link in the breadcrumb container
    When I select "Add To Cart"
    And  I click on the "Add to Cart" button
    Then I verify suggested selling carousel inner-tile for "Add to Cart" is "not displayed" with title "<Title>" with no more than five products and no duplicate products in the carousel

    Examples:
      | ItemCode | Breadcrumb                     | Title                    |
      | 34302    | Tires,Michelin,Product Details | CUSTOMERS ALSO PURCHASED |

  @dt
  @at
  @dtd
  @web
  @20849
  @suggestedSelling
  Scenario Outline: HYBRIS_UI_UI_PDP_Suggested Selling_Hide Suggestions on canonical PDP Page_Guest(ALM#20849)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I should see the fitment panel with vehicle "<Year> <Make>"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the PLP header message contains "tire result"
    When I select the "1st" product result image on PLP page with "View details" button
    Then I verify "Canonical" PDP page is displayed
    And  I verify suggested selling carousel inner-tile for "PDP" is "not displayed" with title "<title>" with no more than five products and no duplicate products in the carousel
    When I set first available size options on PDP page
    Then I verify suggested selling carousel inner-tile for "PDP" is "displayed" with title "<Title>" with no more than five products and no duplicate products in the carousel

    Examples:
      | Year | Make     | Model | Trim                | Assembly       | title | Title                      |
      | 2014 | CADILLAC | CTS   | Sedan 2.0 Turbo AWD | 245 /40 R18 SL | none  | CUSTOMERS ALSO SHOPPED FOR |
