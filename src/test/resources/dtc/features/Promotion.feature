@promotion
Feature: Promotions

  Background:
    Given I change to the default store

  @dt
  @8722
  @web
  @reserveFixedDollarDiscountDateRestrictionDT
  @promotionsRegression
  Scenario Outline: HYBRIS_260_PROMOTIONS&DISCOUNT_Reserve Product with Fixed Discount_Date Restrictions (ALM#8722)
    When I open the "TIRES" navigation link
    And  I click the "Goodyear Tires" menu option
    And  I select shop all from the Product Brand page
    And  I select the "On Promotion" checkbox
    And  I extract the fixed dollar promotion discount of "<ItemCode>"
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify "Instant Savings" displayed on the "cart page" for item "<ItemCode>"
    And  I verify the required fees and add-ons sections are expanded
    And  I verify the Order Summary Instant Savings box on the "Cart" page
    When I extract the "Instant Savings" on the cart page
    And  I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I reserve items and complete checkout for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify "Instant Savings" displayed on the "Order Confirmation" for item "<ItemCode>"
    And  I verify the Order Summary Instant Savings box on the "Order Confirmation" page

    Examples:
      | ItemCode | Customer            |
      | 17596    | default_customer_az |

  @at
  @dt
  @8723
  @web
  @reserveFixedDiscountPercentage
  Scenario Outline: HYBRIS_261_PROMOTIONS&DISCOUNT_Appointmentt with Fixed Percentage with_Qty Restrictions (ALM # 8723)
  """TODO: On Promotion quick filter is available for brand Michelin not for Yokohama.
    and  I select the "On Promotion" quick filter checkbox"""
    When I open the "TIRES" navigation link
    And  I click the "TIRES BY BRAND" menu option
    And  I select the "Yokohama Tires" brand image
    And  I select shop all from the Product Brand page
    And  I extract the fixed percentage promotion discount of "<ItemCode>"
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    And  I validate the fixed percentage discount has been applied
    When I select the checkout option "<Checkout>"
    Then I verify "install with appointment" option is enabled on the Checkout page
    When I create an appointment with defaults from Checkout
    And  I reserve items for "<Customer>"
    Then I verify the Total price on the order confirmation page

    Examples:
      | ItemCode | Checkout         | Customer            |
      | 43870    | with appointment | default_customer_az |

  @dt
  @8719
  @web
  @reserveFixedPercentageDiscountStoreRestrictionsDT
  @promotionsRegression
  Scenario Outline: HYBRIS_258_PROMOTIONS&DISCOUNT_Checkout with Fixed Discount_Store Restrictions (ALM#8719)
    When I open the "TIRES" navigation link
    And  I click the "Goodyear Tires" menu option
    And  I select shop all from the Product Brand page
    And  I select the "On Promotion" checkbox
    And  I extract the fixed percentage promotion discount of "<ItemCode>"
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCode>"
    And  I verify the Order Summary Instant Savings box on the "Cart" page
    When I extract the "Instant Savings" on the cart page
    And  I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I reserve items and complete checkout for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify "Instant Savings" displayed on the "Order Confirmation" for item "<ItemCode>"
    And  I verify the Order Summary Instant Savings box on the "Order Confirmation" page

    Examples:
      | ItemCode | Customer            |
      | 17596    | default_customer_az |

  @at
  @8719
  @web
  @reserveFixedPercentageDiscountStoreRestrictionsAT
  @promotionsRegression
  Scenario Outline: HYBRIS_258_PROMOTIONS&DISCOUNT_Checkout with Fixed Discount_Store Restrictions (ALM#8719)
    When I go to the homepage
    And  I search for store within "25" miles of "<Zipcode>"
    And  I "continue" the Welcome Popup
    And  I select make "<Zipcode>" my store
    And  I open the "TIRES" navigation link
    And  I click the "Goodyear Tires" menu option
    And  I select shop all from the Product Brand page
    And  I select the "On Promotion" checkbox
    And  I extract the fixed percentage promotion discount of "<ItemCode>"
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCode>"
    And  I verify the Order Summary Instant Savings box on the "Cart" page
    When I extract the "Instant Savings" on the cart page
    And  I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I reserve items and complete checkout for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify "Instant Savings" displayed on the "Order Confirmation" for item "<ItemCode>"
    And  I verify the Order Summary Instant Savings box on the "Order Confirmation" page

    Examples:
      | Zipcode | Store             | ItemCode | Customer            |
      | 91710   | 11926 Central Ave | 17596    | default_customer_ca |

  @dt
  @8720
  @web
  @reserveFixedDiscount
  Scenario Outline: HYBRIS_257_PROMOTIONS&DISCOUNT_Checkout with Fixed Discount_Product Type Restrictions (ALM #8720)
  """TODO: Fails in STG due to defect #7185 (No savings on cart page)
    Fails in QA due to defect #7191 (No tax on cart page)
    Fails in QA due to defect #7193 (Incorrect discount being applied)
    TODO: Item 34362 does not have a percentage fixed discount applied
    | 34362    | with appointment | default_customer_az |"""
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select shop all from the Product Brand page
    And  I select the "On Promotion" checkbox
    And  I extract the fixed percentage promotion discount of "<ItemCode>"
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    And  I validate the fixed percentage discount has been applied
    When I select the checkout option "<Checkout>"
    And  I create an appointment with defaults from Checkout
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I verify the Total price on the order confirmation page

    Examples:
      | ItemCode | Checkout         | Customer            |
      | 29935    | with appointment | default_customer_az |

  @at
  @8720
  @web
  @reserveFixedDiscount
  Scenario Outline: HYBRIS_257_PROMOTIONS&DISCOUNT_Checkout with Fixed Discount_Product Type Restrictions (ALM #8720)
  """TODO: Fails in STG due to defect #7185 (No savings on cart page)
    Fails in QA due to defect #7191 (No tax on cart page)
    Fails in QA due to defect #7193 (Incorrect discount being applied)
    TODO: Item 34362 does not have a percentage fixed discount applied
    | 34362    | with appointment | default_customer_az |"""
    When I open the "TIRES" navigation link
    And  I click the "Michelin Tires" menu option
    And  I select shop all from the Product Brand page
    And  I select the "On Promotion" checkbox
    And  I extract the fixed percentage promotion discount of "<ItemCode>"
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I verify the required fees and add-ons sections are expanded
    And  I validate the fixed percentage discount has been applied
    When I select the checkout option "<Checkout>"
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I verify the Total price on the order confirmation page

    Examples:
      | ItemCode | Checkout            | Customer            |
      | 29935    | without appointment | default_customer_az |

  @at
  @dt
  @web
  @mobile
  @15380
  Scenario Outline: HYBRIS_PROMOTIONS_PROMOTIONS_Verify Instant Rebates with update quantity for staggered tires (ALM #15380)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "sets" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeA>"
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeB>"
    And  I verify the Order Summary Instant Savings box on the "Cart" page
    When I update quantity for item "<ItemCodeA>" to "<QtyA>"
    And  I update quantity for item "<ItemCodeB>" to "<QtyB>"
    Then I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeA>"
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeB>"
    And  I verify the Order Summary Instant Savings box on the "Cart" page
    When I extract the "Instant Savings" on the cart page
    And  I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Instant Savings" is displayed on "Checkout" page
    When I click next step for customer information
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Instant Savings" is displayed on "Checkout" page
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Instant Savings" is displayed on "Checkout" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify "Instant Savings" displayed on the "Order Confirmation" for item "<ItemCodeA>"
    And  I verify "Instant Savings" displayed on the "Order Confirmation" for item "<ItemCodeB>"
    And  I verify the Order Summary Instant Savings box on the "Order Confirmation" page

    Examples:
      | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | QtyA | QtyB | Customer                    | Credit Card |
      | 2010 | Chevrolet | Corvette | Base | none     | 28653     | 36347     | 1    | 3    | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |


  @at
  @dt
  @web
  @mobile
  @15385
  @promotionsRegression
  Scenario Outline: HYBRIS_PROMOTIONS_PROMOTIONS_Verify Mail-In Rebates with update quantity for staggered tires (ALM #15385)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "sets" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    And  I verify "<Mail In Rebate>" displayed on the "cart page" for item "<ItemCodeA>"
    And  I verify "<Mail In Rebate>" displayed on the "cart page" for item "<ItemCodeB>"
    When I extract the "Mail In Rebate" on cart page
    And  I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Mail In Rebate" is displayed on "Checkout" page
    When I click next step for customer information
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Mail In Rebate" is displayed on "Checkout" page
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Mail In Rebate" is displayed on "Checkout" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify "<Mail In Rebate>" displayed on the "Order Confirmation" for item "<ItemCodeA>"
    And  I verify "<Mail In Rebate>" displayed on the "Order Confirmation" for item "<ItemCodeB>"

    Examples:
      | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | Mail In Rebate      | Customer                    | Credit Card |
      | 2010 | Chevrolet | Corvette | Base | none     | 36241     | 36259     | Mail In Rebate AUTO | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @at
  @dt
  @web
  @mobile
  @15413
  Scenario Outline: HYBRIS_PROMOTIONS_PROMOTIONS_DT_Verify Instant AND Mail-In Rebates for staggered tires (ALM #15413)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "sets" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    And  I verify "<Mail In Rebate>" displayed on the "cart page" for item "<ItemCodeA>"
    And  I verify "<Mail In Rebate>" displayed on the "cart page" for item "<ItemCodeB>"
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeA>"
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeB>"
    And  I verify the Order Summary Instant Savings box on the "Cart" page
    When I extract the "Instant Savings" on the cart page
    And  I extract the "Mail In Rebate" on the cart page
    And  I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Mail In Rebate" is displayed on "Checkout" page
    And  I verify the "Instant Savings" is displayed on "Checkout" page
    When I click next step for customer information
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Mail In Rebate" is displayed on "Checkout" page
    And  I verify the "Instant Savings" is displayed on "Checkout" page
    When I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Mail In Rebate" is displayed on "Checkout" page
    And  I verify the "Instant Savings" is displayed on "Checkout" page
    When I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify "<Mail In Rebate>" displayed on the "Order Confirmation" for item "<ItemCodeA>"
    And  I verify "<Mail In Rebate>" displayed on the "Order Confirmation" for item "<ItemCodeB>"
    And  I verify "Instant Savings" displayed on the "Order Confirmation" for item "<ItemCodeA>"
    And  I verify "Instant Savings" displayed on the "Order Confirmation" for item "<ItemCodeB>"
    And  I verify the Order Summary Instant Savings box on the "Order Confirmation" page

    Examples:
      | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | Mail In Rebate     | Customer                    | Credit Card |
      | 2010 | Chevrolet | Corvette | Base | none     | 14175     | 14181     | 100 Mail In Rebate | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  |

  @dtd
  @web
  @mobile
  @15495
  @promotionsRegression
  Scenario Outline: HYBRIS_PROMOTIONS_PROMOTIONS_DTD_Verify Instant AND Mail-In Rebates for staggered tires (ALM #15495)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "sets" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    And  I verify "<Mail In Rebate>" displayed on the "cart page" for item "<ItemCodeA>"
    And  I verify "<Mail In Rebate>" displayed on the "cart page" for item "<ItemCodeB>"
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeA>"
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeB>"
    And  I verify the Order Summary Instant Savings box on the "Cart" page
    When I extract the "Instant Savings" on the cart page
    And  I extract the "Mail In Rebate" on the cart page
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Mail In Rebate" is displayed on "Checkout" page
    And  I verify the "Instant Savings" is displayed on "Checkout" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Mail In Rebate" is displayed on "Checkout" page
    And  I verify the "Instant Savings" is displayed on "Checkout" page
    When I select the default shipping option as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Mail In Rebate" is displayed on "Checkout" page
    And  I verify the "Instant Savings" is displayed on "Checkout" page
    When I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify "<Mail In Rebate>" displayed on the "Order Confirmation" for item "<ItemCodeA>"
    And  I verify "<Mail In Rebate>" displayed on the "Order Confirmation" for item "<ItemCodeB>"
    And  I verify "Instant Savings" displayed on the "Order Confirmation" for item "<ItemCodeA>"
    And  I verify "Instant Savings" displayed on the "Order Confirmation" for item "<ItemCodeB>"
    And  I verify the Order Summary Instant Savings box on the "Order Confirmation" page

    Examples:
      | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | Mail In Rebate     | Customer            | Credit Card |
      | 2010 | Chevrolet | Corvette | Base | none     | 14175     | 14181     | 100 Mail In Rebate | default_customer_az | Visa        |

  @dtd
  @web
  @mobile
  @15503
  Scenario Outline: HYBRIS_PROMOTIONS_PROMOTIONS_DTD_Verify Instant Rebates with update quantity for staggered tires (ALM #15503)
    When I go to the homepage
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels"
    And  I add item "<ItemCodeA>" of type "sets" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeA>"
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeB>"
    And  I verify the Order Summary Instant Savings box on the "Cart" page
    When I update quantity for item "<ItemCodeA>" to "<QtyA>"
    And  I update quantity for item "<ItemCodeB>" to "<QtyB>"
    Then I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeA>"
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeB>"
    And  I verify the Order Summary Instant Savings box on the "Cart" page
    When I extract the "Instant Savings" on the cart page
    And  I select the checkout option "default"
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Instant Savings" is displayed on "Checkout" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Instant Savings" is displayed on "Checkout" page
    When I select the default shipping option as "DEFAULT_CUSTOMER_AZ"
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Instant Savings" is displayed on "Checkout" page
    When I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I am brought to the order confirmation page
    And  I verify "Instant Savings" displayed on the "Order Confirmation" for item "<ItemCodeA>"
    And  I verify "Instant Savings" displayed on the "Order Confirmation" for item "<ItemCodeB>"
    And  I verify the Order Summary Instant Savings box on the "Order Confirmation" page

    Examples:
      | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | QtyA | QtyB | Customer            | Credit Card |
      | 2010 | Chevrolet | Corvette | Base | none     | 14175     | 14181     | 1    | 3    | default_customer_az | Visa        |

  @dtd
  @web
  @15546
  Scenario Outline: HYBRIS_PROMOTIONS_PROMOTIONS_DTD_Verify Percentage Rebates with update quantity for staggered (ALM #15546)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I add item "<ItemCodeA>" of type "sets" to my cart and "View shopping Cart"
    Then I should see product "<ItemCodeA>" on the "cart" page
    And  I should see product "<ItemCodeB>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeA>"
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeB>"
    And  I verify the Order Summary Instant Savings box on the "Cart" page
    When I update quantity for item "<ItemCodeA>" to "<QtyA>"
    And  I update quantity for item "<ItemCodeB>" to "<QtyB>"
    Then I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeA>"
    And  I verify "Instant Savings" displayed on the "cart page" for item "<ItemCodeB>"
    And  I verify the Order Summary Instant Savings box on the "Cart" page
    When I extract the "Instant Savings" on the cart page
    And  I select the checkout option "paypal"
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Instant Savings" is displayed on "Checkout" page
    When I enter shipping info as "<Customer>" and continue to next page
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Instant Savings" is displayed on "Checkout" page
    When I select the default shipping option as "<Customer>"
    And  I expand the cart item details section of the cart summary on the Checkout page
    Then I verify the "Instant Savings" is displayed on "Checkout" page
    When I select the "paypal" payment option
    And  I continue on to PayPal checkout
    And  I log into paypal as "<Customer>"
    And  I continue with the paypal payment
    Then I am brought to the order confirmation page
    And  I verify "Instant Savings" displayed on the "Order Confirmation" for item "<ItemCodeA>"
    And  I verify "Instant Savings" displayed on the "Order Confirmation" for item "<ItemCodeB>"
    And  I verify the Order Summary Instant Savings box on the "Order Confirmation" page

    Examples:
      | Year | Make      | Model    | Trim | Assembly | ItemCodeA | ItemCodeB | QtyA | QtyB | Customer           |
      | 2010 | Chevrolet | Corvette | Base | none     | 35454     | 35455     | 1    | 3    | paypal_customer_oh |