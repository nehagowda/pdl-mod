@siteAvailability
Feature: Site Availability

  @dt
  @web
  @smoke
  @siteAvailability
  Scenario Outline: URL path has a response below 400
    When I request the URL with "<Path>"
    Then I should get a response below 400

    Examples:
      |Path                               |
      |/store-locator                     |
      |/customer-service/financing        |
      |/tires/brands                      |
      |/tires/brands/goodyear             |
      |/tires/brands/pirelli              |
      |/tires/brands/cooper               |
      |/tires/all-season                  |
      |/tires/atv-utv                     |
      |/tires/trailer                     |
      |/wheels/brands/fuel-wheels         |
      |/wheels/brands/drag                |
      |/wheels/machined                   |
      |/wheels/mesh                       |
      |/wheels                            |
      |/wheels/trailer                    |
      |/services                          |
      |/schedule-appointment              |
      |/fleet                             |
      |/blog                              |
      |/learn/tire-safety                 |
      |/learn/check-tire-pressure         |
      |/customer-service                  |
      |/customer-service/regional-offices |
      |/sitemap                           |