@fleet
Feature: Fleet

  @fleetApprovalWithOneTire
  Scenario Outline: Fleet approval scenarios with one tire
    When I make a post method call to GRAPHQL and request file with name "<FileName>" and I save the repair number for store "<Store>"
    And  I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    And  I enter "<VIN>" in "VIN" field
    And  I hit TAB key
    And  I click on "Next"
    And  I click on "Driver Waiting" on fleet app
    And  I enter "<MILEAGE>" in "Mileage" field
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "SP" in Vehicle Details page
    And  I click on replace check box for tire "<Replace>"
    And  I click on "Next"
    Then I verify the text of "VIN" field is "<VIN>" in fleet app
    And  I verify the text of "Driver Waiting" field is "<Indicator>" in fleet app
    And  I verify the text of "Towed" field is "N" in fleet app
    And  I verify the text of "Mileage" field is "<MILEAGE>" in fleet app
    And  I save all the line item details
    When I click on "Submit"
    And  I save "AI Reference" to scenario data
    And  I set baseUrl to "AutoIntegrate"
    And  I navigate to the stored Base URL
    And  I enter username and password to login into Auto Integrate
    And  I search for repair order
    And  I click on VIEW on AutoIntegrate app
    And  I close multiple orders pop up when appears
    And  I wait for "10" seconds
    And  I verify line item attributes from fleet app is being displayed in autointegrate
    And  I click on "Approve Remaining" on AutoIntegrate app
    And  I click on "Validate and Submit" on AutoIntegrate app
    And  I click on "Submit To Shop" on AutoIntegrate app
    Then I verify if the transaction is approved and save authorization number to scenario data
    When I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    Then I verify the text of "Status" field is "<Status>" in fleet app

    Examples:
      | Store  | FileName                  | VIN               | O | M | I | MILEAGE | Status     | Replace | Indicator |
      | RIP 05 | fleetAddInvoice_POST.json | YV4982DZ7A2082798 | 1 | 2 | 3 | 12000   | Authorized | LF      | Y         |
      | RIP 05 | fleetAddInvoice_POST.json | 2FMDK3GC7DBB31018 | 1 | 2 | 3 | 12000   | Authorized | RF      | Y         |
      | RIP 05 | fleetAddInvoice_POST.json | 1GCRCPEX0CZ221157 | 1 | 2 | 3 | 12000   | Authorized | RR      | Y         |
      | RIP 05 | fleetAddInvoice_POST.json | 1N4AL2AP0CN436508 | 1 | 2 | 3 | 12000   | Authorized | LR      | Y         |
      | RIP 05 | fleetAddInvoice_POST.json | 5TDDK3EH9DS176219 | 1 | 2 | 3 | 12000   | Authorized | SP      | Y         |

  @FleetRejectionWithOneTire
  Scenario Outline: Fleet Reject scenarios with one tire
    When I make a post method call to GRAPHQL and request file with name "<FileName>" and I save the repair number for store "<Store>"
    And  I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    And  I enter "<VIN>" in "VIN" field
    And  I hit TAB key
    And  I click on "Next"
    And  I click on "Driver Waiting" on fleet app
    And  I enter "<MILEAGE>" in "Mileage" field
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "SP" in Vehicle Details page
    And  I click on replace check box for tire "<Replace>"
    And  I click on "Next"
    Then I verify the text of "VIN" field is "<VIN>" in fleet app
    And  I verify the text of "Driver Waiting" field is "Y" in fleet app
    And  I verify the text of "Towed" field is "N" in fleet app
    And  I verify the text of "Mileage" field is "<MILEAGE>" in fleet app
    And  I save all the line item details
    When I click on "Submit"
    And  I save "AI Reference" to scenario data
    And  I wait for "5" seconds
    Then I verify the text of "VIN" field is "<VIN>" in fleet app
    When I set baseUrl to "AutoIntegrate"
    And  I navigate to the stored Base URL
    And  I enter username and password to login into Auto Integrate
    And  I search for repair order
    And  I click on VIEW on AutoIntegrate app
    And  I close multiple orders pop up when appears
    And  I verify line item attributes from fleet app is being displayed in autointegrate
    And  I click on "Reject Remaining" on AutoIntegrate app
    And  I select "<Reject Reason>" as reject reason on AutoIntegrate App
    And  I enter "Automation rejection" in "Notes" field in AutoIntegrate App
    And  I click on "Save" on AutoIntegrate app
    And  I click on "Validate and Submit" on AutoIntegrate app
    And  I click on "Submit To Shop" on AutoIntegrate app
    Then I verify if the transaction is rejected
    When I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    Then I verify the text of "Status" field is "<Status>" in fleet app
    And  I verify the text of "VIN" field is "<VIN>" in fleet app

    Examples:
      | Store  | FileName                  | VIN               | O  | M  | I  | MILEAGE | Status         | Replace | Reject Reason |
      | RIP 05 | fleetAddInvoice_POST.json | 2G1WG5E36C1148080 | 7  | 8  | 9  | 12000   | Not Authorized | LF      | Price         |
      | RIP 05 | fleetAddInvoice_POST.json | WBAFU7C59BDU56490 | 10 | 11 | 23 | 12000   | Not Authorized | RF      | Other         |
      | RIP 05 | fleetAddInvoice_POST.json | 1FMCU9GX3EUA32350 | 4  | 6  | 2  | 12000   | Not Authorized | RR      | Not Required  |
      | RIP 05 | fleetAddInvoice_POST.json | 2GNFLCEK9C6276712 | 5  | 14 | 17 | 12000   | Not Authorized | LR      | Goodwill      |
      | RIP 05 | fleetAddInvoice_POST.json | 2G1WF5E33D1170719 | 30 | 15 | 16 | 12000   | Not Authorized | SP      | Cycled        |

  @fleetApprovalWithTwoTires
  Scenario Outline: Fleet Approval scenario with Two Tires
    When I make a post method call to GRAPHQL and request file with name "<FileName>" and I save the repair number for store "<Store>"
    And  I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    And  I enter "<VIN>" in "VIN" field
    And  I hit TAB key
    And  I click on "Next"
    And  I click on "Driver Waiting" on fleet app
    And  I enter "<MILEAGE>" in "Mileage" field
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "SP" in Vehicle Details page
    And  I click on replace check box for tire "<Replace1>"
    And  I click on replace check box for tire "<Replace2>"
    And  I click on "Next"
    Then I verify the text of "VIN" field is "<VIN>" in fleet app
    And  I verify the text of "Driver Waiting" field is "Y" in fleet app
    And  I verify the text of "Towed" field is "N" in fleet app
    And  I verify the text of "Mileage" field is "<MILEAGE>" in fleet app
    And  I save all the line item details
    When I click on "Submit"
    And  I save "AI Reference" to scenario data
    And  I set baseUrl to "AutoIntegrate"
    And  I navigate to the stored Base URL
    And  I enter username and password to login into Auto Integrate
    And  I search for repair order
    And  I click on VIEW on AutoIntegrate app
    And  I close multiple orders pop up when appears
    And  I verify line item attributes from fleet app is being displayed in autointegrate
    And  I click on "Approve Remaining" on AutoIntegrate app
    And  I click on "Validate and Submit" on AutoIntegrate app
    And  I click on "Submit To Shop" on AutoIntegrate app
    Then I verify if the transaction is approved and save authorization number to scenario data
    When I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    Then I verify the text of "Status" field is "<Status>" in fleet app

    Examples:
      | Store  | FileName                             | VIN               | O | M | I | MILEAGE | Status     | Replace1 | Replace2 |
      | RIP 05 | fleetAddInvoiceTwoQuantity_POST.json | YV4982DZ7A2082798 | 1 | 2 | 3 | 12000   | Authorized | LF       | RF       |
      | RIP 05 | fleetAddInvoiceTwoQuantity_POST.json | 2FMDK3GC7DBB31018 | 1 | 2 | 3 | 12000   | Authorized | RF       | LR       |
      | RIP 05 | fleetAddInvoiceTwoQuantity_POST.json | 1GCRCPEX0CZ221157 | 1 | 2 | 3 | 12000   | Authorized | RR       | RF       |
      | RIP 05 | fleetAddInvoiceTwoQuantity_POST.json | 1N4AL2AP0CN436508 | 1 | 2 | 3 | 12000   | Authorized | LR       | RR       |

  @fleetApprovalWithFourTires
  Scenario Outline: Fleet Approval scenario with Four Tires
    When I make a post method call to GRAPHQL and request file with name "<FileName>" and I save the repair number for store "<Store>"
    And  I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    And  I enter "<VIN>" in "VIN" field
    And  I hit TAB key
    And  I click on "Next"
    And  I click on "Driver Waiting" on fleet app
    And  I enter "<MILEAGE>" in "Mileage" field
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "SP" in Vehicle Details page
    And  I click on replace check box for tire "<Replace1>"
    And  I click on replace check box for tire "<Replace2>"
    And  I click on replace check box for tire "<Replace3>"
    And  I click on replace check box for tire "<Replace4>"
    And  I click on "Next"
    Then I verify the text of "VIN" field is "<VIN>" in fleet app
    And  I verify the text of "Driver Waiting" field is "Y" in fleet app
    And  I verify the text of "Towed" field is "N" in fleet app
    And  I verify the text of "Mileage" field is "<MILEAGE>" in fleet app
    And  I save all the line item details
    When I click on "Submit"
    And  I save "AI Reference" to scenario data
    And  I set baseUrl to "AutoIntegrate"
    And  I navigate to the stored Base URL
    And  I enter username and password to login into Auto Integrate
    And  I search for repair order
    And  I click on VIEW on AutoIntegrate app
    And  I close multiple orders pop up when appears
    And  I verify line item attributes from fleet app is being displayed in autointegrate
    And  I click on "Approve Remaining" on AutoIntegrate app
    And  I click on "Validate and Submit" on AutoIntegrate app
    And  I click on "Submit To Shop" on AutoIntegrate app
    Then I verify if the transaction is approved and save authorization number to scenario data
    When I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    Then I verify the text of "Status" field is "<Status>" in fleet app

    Examples:
      | Store  | FileName                              | VIN               | O | M | I | MILEAGE | Status     | Replace1 | Replace2 | Replace3 | Replace4 |
      | RIP 05 | fleetAddInvoiceFourQuantity_POST.json | YV4982DZ7A2082798 | 1 | 2 | 3 | 12000   | Authorized | LF       | RF       | LR       | RR       |

  @fleetRejectionWithTwoTires
  Scenario Outline: Fleet Reject scenarios with two tires
    When I make a post method call to GRAPHQL and request file with name "<FileName>" and I save the repair number for store "<Store>"
    And  I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    And  I enter "<VIN>" in "VIN" field
    And  I hit TAB key
    And  I click on "Next"
    And  I click on "Driver Waiting" on fleet app
    And  I enter "<MILEAGE>" in "Mileage" field
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "SP" in Vehicle Details page
    And  I click on replace check box for tire "<Replace1>"
    And  I click on replace check box for tire "<Replace2>"
    And  I click on "Next"
    Then I verify the text of "VIN" field is "<VIN>" in fleet app
    And  I verify the text of "Driver Waiting" field is "Y" in fleet app
    And  I verify the text of "Towed" field is "N" in fleet app
    And  I verify the text of "Mileage" field is "<MILEAGE>" in fleet app
    And  I save all the line item details
    When I click on "Submit"
    And  I save "AI Reference" to scenario data
    And  I wait for "5" seconds
    Then I verify the text of "VIN" field is "<VIN>" in fleet app
    When I set baseUrl to "AutoIntegrate"
    And  I navigate to the stored Base URL
    And  I enter username and password to login into Auto Integrate
    And  I search for repair order
    And  I click on VIEW on AutoIntegrate app
    And  I close multiple orders pop up when appears
    And  I verify line item attributes from fleet app is being displayed in autointegrate
    And  I click on "Reject Remaining" on AutoIntegrate app
    And  I select "<Reject Reason>" as reject reason on AutoIntegrate App
    And  I enter "Automation rejection" in "Notes" field in AutoIntegrate App
    And  I click on "Save" on AutoIntegrate app
    And  I click on "Validate and Submit" on AutoIntegrate app
    And  I click on "Submit To Shop" on AutoIntegrate app
    Then I verify if the transaction is rejected
    When I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    Then I verify the text of "Status" field is "<Status>" in fleet app
    And  I verify the text of "VIN" field is "<VIN>" in fleet app

    Examples:
      | Store  | FileName                             | VIN               | O  | M  | I  | MILEAGE | Status         | Replace1 | Replace2 | Reject Reason |
      | RIP 05 | fleetAddInvoiceTwoQuantity_POST.json | WBAFU7C59BDU56490 | 10 | 11 | 23 | 12000   | Not Authorized | RF       | LR       | Other         |
      | RIP 05 | fleetAddInvoiceTwoQuantity_POST.json | 1FMCU9GX3EUA32350 | 4  | 6  | 2  | 12000   | Not Authorized | RR       | RF       | Not Required  |
      | RIP 05 | fleetAddInvoiceTwoQuantity_POST.json | 2GNFLCEK9C6276712 | 5  | 14 | 17 | 12000   | Not Authorized | LR       | RR       | Goodwill      |

  @fleetNegativeVinEntryValidation
  Scenario Outline: Negative field validation for VIN Entry
    When I make a post method call to GRAPHQL and request file with name "<FileName>" and I save the repair number for store "<Store>"
    And  I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    And  I enter "<Invalid VIN>" in "VIN" field
    And  I hit TAB key
    Then I verify the text of "VIN Error Message" field is "<Invalid Vin Error Message>" in summary
    When I enter "<Invalid VIN Length>" in "VIN" field
    And  I hit TAB key
    Then I verify the text of "VIN Error Message" field is "<Invalid length Error Message>" in summary

    Examples:
      | Store  | FileName                             | Invalid VIN       | Invalid VIN Length | Invalid length Error Message | Invalid Vin Error Message                                      |
      | RIP 05 | fleetAddInvoiceTwoQuantity_POST.json | 2G1WG5E36C1148081 | 2G1WG5E3           | VIN not 13 to 17 chars long  | The VIN entered is not recognized as an Auto Integrate vehicle |

  @fleetNegativeVehicleDetailsValidation
  Scenario Outline: Negative field validation for Quantity Mis match
    When I make a post method call to GRAPHQL and request file with name "<FileName>" and I save the repair number for store "<Store>"
    And  I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    And  I enter "<VIN>" in "VIN" field
    And  I hit TAB key
    And  I click on "Next"
    And  I click on "Driver Waiting" on fleet app
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "SP" in Vehicle Details page
    And  I click on replace check box for tire "<Replace tire 1>"
    And  I click on "Next"
    Then I verify the text of "Vehicle Details error message" field is "<Error Message 2>" in summary
    When I enter "<MILEAGE>" in "Mileage" field
    And  I click on replace check box for tire "<Replace tire 2>"
    And  I click on "Next"
    Then I verify the text of "Vehicle Details error message" field is "<Error Message 1>" in summary

    Examples:
      | Store  | FileName                  | VIN               | O | M | I | MILEAGE | Replace tire 1 | Replace tire 2 | Error Message 1                                                      | Error Message 2         |
      | RIP 05 | fleetAddInvoice_POST.json | YV4982DZ7A2082798 | 1 | 2 | 3 | 12000   | LF             | SP             | Number of tires being replaced must match number of tires on invoice | Mileage must be entered |

  @fleetApprovalWithTwoTiresDually
  Scenario Outline: Fleet Approval scenario with Two Tires for dually
    When I make a post method call to GRAPHQL and request file with name "<FileName>" and I save the repair number for store "<Store>"
    And  I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    And  I enter "<VIN>" in "VIN" field
    And  I hit TAB key
    And  I click on "Next"
    And  I click on "Driver Waiting" on fleet app
    And  I enter "<MILEAGE>" in "Mileage" field
    And  I click on "Dually?" on fleet app
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LRI" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RRI" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "SP" in Vehicle Details page
    And  I click on replace check box for tire "<Replace1>"
    And  I click on replace check box for tire "<Replace2>"
    And  I click on "Next"
    Then I verify the text of "VIN" field is "<VIN>" in fleet app
    And  I verify the text of "Driver Waiting" field is "Y" in fleet app
    And  I verify the text of "Towed" field is "N" in fleet app
    And  I verify the text of "Mileage" field is "<MILEAGE>" in fleet app
    And  I save all the line item details
    When I click on "Submit"
    And  I save "AI Reference" to scenario data
    And  I set baseUrl to "AutoIntegrate"
    And  I navigate to the stored Base URL
    And  I enter username and password to login into Auto Integrate
    And  I search for repair order
    And  I click on VIEW on AutoIntegrate app
    And  I close multiple orders pop up when appears
    And  I verify line item attributes from fleet app is being displayed in autointegrate
    And  I click on "Approve Remaining" on AutoIntegrate app
    And  I click on "Validate and Submit" on AutoIntegrate app
    And  I click on "Submit To Shop" on AutoIntegrate app
    Then I verify if the transaction is approved and save authorization number to scenario data
    When I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    Then I verify the text of "Status" field is "<Status>" in fleet app

    Examples:
      | Store  | FileName                             | VIN               | O | M | I | MILEAGE | Status     | Replace1 | Replace2 |
      | RIP 05 | fleetAddInvoiceTwoQuantity_POST.json | YV4982DZ7A2082798 | 1 | 2 | 3 | 12000   | Authorized | LRI      | RR       |
      | RIP 05 | fleetAddInvoiceTwoQuantity_POST.json | 2FMDK3GC7DBB31018 | 1 | 2 | 3 | 12000   | Authorized | RF       | LRI      |
      | RIP 05 | fleetAddInvoiceTwoQuantity_POST.json | 1GCRCPEX0CZ221157 | 1 | 2 | 3 | 12000   | Authorized | RRI      | RF       |
      | RIP 05 | fleetAddInvoiceTwoQuantity_POST.json | 1N4AL2AP0CN436508 | 1 | 2 | 3 | 12000   | Authorized | LRI      | RR       |

  @fleetApprovalWithFourTiresDually
  Scenario Outline: Fleet Approval scenario with Four Tires for dually
    When I make a post method call to GRAPHQL and request file with name "<FileName>" and I save the repair number for store "<Store>"
    And  I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    And  I enter "<VIN>" in "VIN" field
    And  I hit TAB key
    And  I click on "Next"
    And  I click on "Driver Waiting" on fleet app
    And  I enter "<MILEAGE>" in "Mileage" field
    And  I click on "Dually?" on fleet app
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RF" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RR" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "LRI" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "RRI" in Vehicle Details page
    And  I enter Tread depth value for O - "<O>" M - "<M>" I - "<I>" for Tire at location - "SP" in Vehicle Details page
    And  I click on replace check box for tire "<Replace1>"
    And  I click on reason drop down for tire at location "<Replace1>" and select "<Reason 1>" as reason
    And  I click on replace check box for tire "<Replace2>"
    And  I click on reason drop down for tire at location "<Replace2>" and select "<Reason 2>" as reason
    And  I click on replace check box for tire "<Replace3>"
    And  I click on reason drop down for tire at location "<Replace3>" and select "<Reason 3>" as reason
    And  I click on replace check box for tire "<Replace4>"
    And  I click on reason drop down for tire at location "<Replace4>" and select "<Reason 4>" as reason
    And  I click on "Next"
    Then I verify the text of "VIN" field is "<VIN>" in fleet app
    And  I verify the text of "Driver Waiting" field is "Y" in fleet app
    And  I verify the text of "Towed" field is "N" in fleet app
    And  I verify the text of "Mileage" field is "<MILEAGE>" in fleet app
    And  I save all the line item details
    When I click on "Submit"
    And  I save "AI Reference" to scenario data
    And  I set baseUrl to "AutoIntegrate"
    And  I navigate to the stored Base URL
    And  I enter username and password to login into Auto Integrate
    And  I search for repair order
    And  I click on VIEW on AutoIntegrate app
    And  I close multiple orders pop up when appears
    And  I verify line item attributes from fleet app is being displayed in autointegrate
    And  I click on "Approve Remaining" on AutoIntegrate app
    And  I click on "Validate and Submit" on AutoIntegrate app
    And  I click on "Submit To Shop" on AutoIntegrate app
    Then I verify if the transaction is approved and save authorization number to scenario data
    When I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    Then I verify the text of "Status" field is "<Status>" in fleet app

    Examples:
      | Store  | FileName                              | VIN               | O | M | I | MILEAGE | Status     | Replace1 | Replace2 | Replace3 | Replace4 | Reason 1     | Reason 2             | Reason 3           | Reason 4                 |
      | RIP 05 | fleetAddInvoiceFourQuantity_POST.json | YV4982DZ7A2082798 | 1 | 2 | 3 | 12000   | Authorized | LRI      | RR       | LR       | RF       | End of Life  | Accident Damage      | Impact Damage      | Preventative Maintenance |
      | RIP 05 | fleetAddInvoiceFourQuantity_POST.json | 2FMDK3GC7DBB31018 | 1 | 2 | 3 | 12000   | Authorized | RF       | LRI      | RRI      | LF       | Abuse        | Faulty Manufacturing | Incorrectly Fitted | Driver Request           |
      | RIP 05 | fleetAddInvoiceFourQuantity_POST.json | 1N4AL2AP0CN436508 | 1 | 2 | 3 | 12000   | Authorized | LRI      | RR       | RF       | LR       | Illegal Wear | Emergency Braking    | Cuts               | State Required           |

  @treadDepthValidation
  @fleetTreadDepthValidation
  Scenario Outline: Fleet Tread Depth Validation
    When I make a post method call to GRAPHQL and request file with name "<FileName>" and I save the repair number for store "<Store>"
    And  I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    And  I enter "<VIN>" in "VIN" field
    And  I hit TAB key
    And  I click on "Next"
    And  I click on "Driver Waiting" on fleet app
    And  I enter "<MILEAGE>" in "Mileage" field
    And  I enter random Tread depth values for Tire at location "LF" in Vehicle Details page
    And  I enter random Tread depth values for Tire at location "RF" in Vehicle Details page
    And  I enter random Tread depth values for Tire at location "RR" in Vehicle Details page
    And  I enter random Tread depth values for Tire at location "LR" in Vehicle Details page
    And  I enter random Tread depth values for Tire at location "SP" in Vehicle Details page
    And  I click on replace check box for tire "<Replace>"
    And  I click on "Next"
    And  I click on "Submit"
    And  I save "AI Reference" to scenario data
    And  I set baseUrl to "AutoIntegrate"
    And  I navigate to the stored Base URL
    And  I enter username and password to login into Auto Integrate
    And  I search for repair order
    And  I click on VIEW on AutoIntegrate app
    And  I close multiple orders pop up when appears
    And  I click on "Measurements"
    Then I verify Tread depth values for Tire at location "LF" in AutoIntegrate
    And  I verify Tread depth values for Tire at location "RF" in AutoIntegrate
    And  I verify Tread depth values for Tire at location "RR" in AutoIntegrate
    And  I verify Tread depth values for Tire at location "LR" in AutoIntegrate
    And  I verify Tread depth values for Tire at location "SP" in AutoIntegrate
    When I click on "Cancel" on AutoIntegrate app
    And  I click on "Approve Remaining" on AutoIntegrate app
    And  I click on "Validate and Submit" on AutoIntegrate app
    And  I click on "Submit To Shop" on AutoIntegrate app
    Then I verify if the transaction is approved and save authorization number to scenario data
    When I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    Then I verify the text of "Status" field is "<Status>" in fleet app

    Examples:
      | Store  | FileName                  | VIN               | MILEAGE | Status     | Replace |
      | RIP 05 | fleetAddInvoice_POST.json | YV4982DZ7A2082798 | 12000   | Authorized | LF      |

  @treadDepthValidation
  @fleetTreadDepthValidationDually
  Scenario Outline: Fleet Tread Depth Validation for Dually
    When I make a post method call to GRAPHQL and request file with name "<FileName>" and I save the repair number for store "<Store>"
    And  I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    And  I enter "<VIN>" in "VIN" field
    And  I hit TAB key
    And  I click on "Next"
    And  I click on "Driver Waiting" on fleet app
    And  I enter "<MILEAGE>" in "Mileage" field
    And  I click on "Dually?" on fleet app
    And  I enter random Tread depth values for Tire at location "LF" in Vehicle Details page
    And  I enter random Tread depth values for Tire at location "RF" in Vehicle Details page
    And  I enter random Tread depth values for Tire at location "RR" in Vehicle Details page
    And  I enter random Tread depth values for Tire at location "LR" in Vehicle Details page
    And  I enter random Tread depth values for Tire at location "RRI" in Vehicle Details page
    And  I enter random Tread depth values for Tire at location "LRI" in Vehicle Details page
    And  I enter random Tread depth values for Tire at location "SP" in Vehicle Details page
    And  I click on replace check box for tire "<Replace>"
    And  I click on "Next"
    And  I click on "Submit"
    And  I save "AI Reference" to scenario data
    And  I set baseUrl to "AutoIntegrate"
    And  I navigate to the stored Base URL
    And  I enter username and password to login into Auto Integrate
    And  I search for repair order
    And  I click on VIEW on AutoIntegrate app
    And  I close multiple orders pop up when appears
    And  I click on "Measurements"
    And  I click on "Change Vehicle Type"
    And  I wait for "5" seconds
    Then I verify Tread depth values for Tire at location "LF" in AutoIntegrate
    And  I verify Tread depth values for Tire at location "RF" in AutoIntegrate
    And  I verify Tread depth values for Tire at location "RR" in AutoIntegrate
    And  I verify Tread depth values for Tire at location "LR" in AutoIntegrate
    And  I verify Tread depth values for Tire at location "RRI" in AutoIntegrate
    And  I verify Tread depth values for Tire at location "LRI" in AutoIntegrate
    And  I verify Tread depth values for Tire at location "SP" in AutoIntegrate
    When I click on "Cancel" on AutoIntegrate app
    And  I click on "Approve Remaining" on AutoIntegrate app
    And  I click on "Validate and Submit" on AutoIntegrate app
    And  I click on "Submit To Shop" on AutoIntegrate app
    Then I verify if the transaction is approved and save authorization number to scenario data
    When I set baseUrl to "Fleet"
    And  I navigate to the stored Base URL
    Then I verify the text of "Status" field is "<Status>" in fleet app

    Examples:
      | Store  | FileName                  | VIN               | MILEAGE | Status     | Replace |
      | RIP 05 | fleetAddInvoice_POST.json | YV4982DZ7A2082798 | 12000   | Authorized | LF      |