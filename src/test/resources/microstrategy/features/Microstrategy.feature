@microstrategy

Feature: Login to MicroStrategy

  Background:
    Given I set baseUrl to "MicroStrategy"
    And   I navigate to the stored Base URL

  @companyOperations
  Scenario Outline: MicroStrategy_Login
    When I enter "<User ID>" into the field with label "User name"
    And  I enter "<Password>" into the field with label "Password"
    And  I click on "Login" on MicroStrategy homepage
    And  I click on "DT Store Operations" on MicroStrategy homepage
    And  I click on exact "Shared Reports" on MicroStrategy page
    And  I click on "DTC Executive Dashboard" on MicroStrategy homepage
    And  I click on "Company Operations" on MicroStrategy homepage
    And  I click on exact "Your selection:" on MicroStrategy page
    And  I click on "Your selection" on MicroStrategy homepage
    And  I click on "previous month" arrow
    And  I click on "previous month" arrow
    And  I click on "previous month" arrow
    And  I click on "previous month" arrow
    And  I click on "previous month" arrow
    And  I select "2" day of the month
    And  I double click on "<Region>" on MicroStrategy homepage
    And  I click on "Run Document" on MicroStrategy homepage
    And  I wait for "20" seconds
    And  I click on "UNITS" on MicroStrategy homepage
    And  I wait for "20" seconds
    And  I click on "AZP" on MicroStrategy homepage
    And  I click on "Company Operations Title" on MicroStrategy homepage
    And  I click on "NET SALES" on MicroStrategy homepage
    And  I wait for "20" seconds
    And  I click on "AZP" on MicroStrategy homepage
    And  I click on "Company Operations Title" on MicroStrategy homepage
    And  I click on "GROSS PROFIT" on MicroStrategy homepage
    And  I wait for "20" seconds
    And  I click on "AZP" on MicroStrategy homepage
    And  I click on "Company Operations Title" on MicroStrategy homepage
    And  I click on "LABOR EFFICIENCY" on MicroStrategy homepage
    And  I wait for "30" seconds
    And  I click on "AZP" on MicroStrategy homepage
    And  I click on "Company Operations Title" on MicroStrategy homepage
    And  I click on "RECOMMEND CDI" on MicroStrategy homepage
    And  I wait for "20" seconds
    And  I click on "AZP" on MicroStrategy homepage
    And  I click on "Company Operations Title" on MicroStrategy homepage

    Examples:
      | User ID           | Password  | Region      |
      | MSTRTest-CorpExec | Password1 | AZP:Arizona |