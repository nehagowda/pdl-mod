@patchtesting
@SecurityPatchTest
Feature: To Verify Safety Feature

  Background:
    Given I go to the 'Knowledge Center' home page
    And   I log in to the 'Knowledge Center' site

  @pdfSmoke
  @pdf
  Scenario Outline: Safety Library PDF 3 step
    When I select "<SubMenu>" sub-menu item under the "<Menu Item>" menu header option
    And  I select the "<PDF>" link on the page
    And  I switch to the newly opened window
    Then I verify the PDF has Content Like "<PDF Text>"

    Examples:
      | Menu Item           | SubMenu                      | PDF                                                           | PDF Text                                     |
      | Store Operations    | Adjustments                  | Managing Adjustments - Looking up Adjustments                 | From the MIM Adjustment results              |
      | Store Operations    | Inventory                    | Step by Step Instructions                                     | Start Inventory first thing in the morning.  |
      | Store Operations    | Ordering - Receiving         | Receiving Products and Non-Merchandise Orders                 | Goods Receipt is to use the handheld scanner |
      | Store Operations    | Security                     | Data Obfuscation Matrix                                       | Data Obfuscation Matrix                      |
      | Store Operations    | Security                     | Information Security Policies and Procedures (Public)         | This document is                             |
      | Store Operations    | Security                     | Records Management Policy                                     | Amendments to this Policy                    |
      | Store Operations    | Security                     | Records Retention Schedule                                    | This document is the property                |
      | CES                 | 3 Phase CES & ETV            | CES Coaching Model                                            | We would like to share                       |
      | CES                 | 3 Phase CES & ETV            | Environment Guide                                             | Do you and your team look                    |
      | CES                 | 3 Phase CES & ETV            | ETV Foundational Blueprint                                    | What does it mean to Earn the                |
      | CES                 | 3 Phase CES & ETV            | ETV Lead Actions                                              | Earn the Visit means having                  |
      | CES                 | 3 Phase CES & ETV            | Participant QRG                                               | Welcome the customer                         |
      | LEAD/Speed of Trust | LEAD                         | 2018 Company & Operations WIGs                                | Improve safety, quality                      |
      | LEAD/Speed of Trust | LEAD                         | LEAD Pre-Season Schedule                                      | COO & CCO                                    |
      | LEAD/Speed of Trust | LEAD                         | Know Your Whirlwind Gauges                                    | Pair mentees up with                         |
      | LEAD/Speed of Trust | LEAD                         | LEAD - Full Guide                                             | The concepts in this Reference               |
      | LEAD/Speed of Trust | LEAD                         | Discipline 1 - WIG Guide                                      | The purpose of Discipline                    |
      | LEAD/Speed of Trust | LEAD                         | Discipline 2 - Lead Measure Guide                             | Choose the fewest Lead Measures              |
      | LEAD/Speed of Trust | LEAD                         | Discipline 3 - Scoreboard Guide                               | Stay on track to achieve                     |
      | LEAD/Speed of Trust | LEAD                         | Discipline 4 - WIG Session/Huddle Guide                       | The purpose of Discipline 4 is               |
      | LEAD/Speed of Trust | LEAD                         | 4DX Book Summary                                              | Do you remember the last major               |
      | DTU                 | Corporate Training Resources | Employee User Guide                                           | Figure 1: Easy Clocking Login Page           |
      | DTU                 | Corporate Training Resources | Job Codes Defined                                             | Project Management: Activities               |
      | DTU                 | Corporate Training Resources | Manager User Guide                                            | Single user approval                         |
      | Regions             | Arizona                      | AZP - Dress Code and Grooming Requirements - Store Associates | The dress code and grooming                  |
      | Regions             | Arizona                      | AZP - Emergency Phone Number List                             | Guaranteed Door Service                      |
      | Regions             | Arizona                      | Arizona Regional Office                                       | Anthony Guadagnolo                           |
      | Regions             | Arizona                      | Arizona Resale and Tax Exempt Guidelines                      | These guidelines are                         |
      | Regions             | Arizona                      | AZP - 5s Visual Management Guide                              | Changers are placed                          |
      | Regions             | Arizona                      | AZP - Showroom Marketing Plan                                 | The intent with this                         |
      | Regions             | Arizona                      | 5S Assistant Manager Evaluation                               | Consistently models a Ready                  |
      | Regions             | Arizona                      | Assistant Manager Evaluation                                  | Builds relationships, under                  |
      | Regions             | Arizona                      | Crew Chief Evaluation                                         | Comes to work on time                        |
      | Regions             | Arizona                      | Marketing Assistant Manager Evaluation                        | Consistently models a                        |
      | Regions             | Arizona                      | Senior Assistant Manager Evaluation                           | Effectively models best                      |
      | Regions             | California Central           | 1. Pre-Employment Packet - CAL                                | This packet contains                         |
      | Regions             | California Central           | 2. Required For Hire Packet - CAL                             | Upon completion                              |
      | Regions             | California Central           | 3. Full Time New Hire Orientation Packet - CAL                | Please initial next to                       |
      | Regions             | California Central           | 3. Part Time New Hire Orientation Packet - CAL                | California Meal Requirements                 |
      | Regions             | California Central           | 4. Part Time to Full Time Orientation Packet - CAL            | I have read, reviewed                        |
      | Regions             | California Central           | Three Managers Interview                                      | Manager Name                                 |
      | Regions             | California Northern          | 1. Pre-Employment Packet - CAN                                | This packet contains                         |
      | Regions             | California Northern          | 2. Required For Hire Packet - CAN                             | Upon completion                              |
      | Regions             | California Northern          | 3. Full Time New Hire Orientation Packet - CAN                | enclosed information                         |
      | Regions             | California Northern          | 3. Part Time New Hire Orientation Packet - CAN                | Northern California                          |
      | Regions             | California Northern          | 4. Part Time to Full Time Orientation Packet - CAN            | Part Time to Full Time                       |
      | Regions             | California Northern          | CA Store Employment Application Form                          | Do you have the legal right to               |
      | Regions             | California Northern          | Hiring Process - Store Responsibilities                       | Before you submit the                        |
      | Regions             | California Northern          | Hiring Process Steps: Store Employees                         | The hiring of a Store                        |
      | Regions             | California Southern          | 1. Pre-Employment Packet - CAS                                | Southern California                          |
      | Regions             | California Southern          | 2. Required For Hire Packet - CAS                             | Notice & Acknowledgement                     |
      | Regions             | California Southern          | 3. Full Time New Hire Orientation Packet - CAS                | Please initial next                          |
      | Regions             | California Southern          | 3. Part Time New Hire Orientation Packet - CAS                | Part Time New                                |
      | Regions             | California Southern          | 4. Part Time to Full Time Orientation Packet - CAS            | Orientation Paperwork                        |
      | Regions             | California Southern          | California Resale and Tax Exempt Guidelines                   | These guidelines are                         |
      | Regions             | Carolinas                    | 5S Store Checklist - Inspection Sheet - NCC                   | Tool stands clean & organized                |
      | Regions             | Carolinas                    | The Dream - Mentorship Handout - NCC                          | What are your Short-term goals               |
      | Regions             | Carolinas                    | Carolina Calendar - NCC                                       | Carolina 2018 Calendar                       |
      | Regions             | Carolinas                    | warehouse Calendar-NCC-2018.xlsx                              | Warehouse Calendar                           |

  @pdfSmoke
  @pdf
  Scenario Outline: Safety Library PDF 4 Step
    When I select "<SubMenu>" sub-menu item under the "<Menu Item>" menu header option
    And  I select the "<Link>" link on the page
    And  I select the "<PDF>" link on the page
    And  I switch to the newly opened window
    Then I verify the PDF has Content Like "<PDF Text>"

    Examples:
      | Menu Item      | SubMenu                         | Link               | PDF                        | PDF Text                  |
      | Safety/Quality | Safety                          | Top Safety Leaders | 2016 Safety Leaders        | Roux Willmott             |
      | DTU            | Training Documents & Procedures | Certified Manager  | Are You A Boss Or A Leader | he Boss drives his people |

  @SafetyLibraryPowerpoint
  Scenario Outline: Safety Library
    When I select "<SubMenu>" sub-menu item under the "<Menu Item>" menu header option
    And I Download the "<Link>" link on the page
    Then  I verify the "<FileName>" file has downloaded to the Downloads folder
    When I remove the "<FileName>" from the computer

    Examples:
      | Menu Item       | SubMenu           | Link                                    | FileName                                    |
      | Human Resources | HR Home           | 2018 PTO Form (Office)                  | 2018 PTO Form (Office).XLSX                 |
      | Human Resources | HR Home           | Employee Anniversaries                  | Employee Anniversaries.xls                  |
      | Regions         | Washington        | WAS Regional Training Calendar          | WAS Regional Training Calendar 2018.xlsx    |
      | Regions         | Washington        | Bank Deposit Log                        | Bank Deposit Log.docx                       |
      | Regions         | Virginia          | VAR Regional Training Calendar          | VAR Regional Training Calendar 2018.xlsx    |
      | Regions         | Utah              | Utah 2018 Events Calendar - UTS         | Utah 2018 Events Calendar - UTS.xlsx        |
      | Regions         | Utah              | Utah Store Employee Listings - UTS      | Store Employee Listings - UTS.xls           |
      | Regions         | Utah              | UTS00 - Employee Vacation Calendar      | Utah 2018 Vacation Request Form.xlsx        |
      | Regions         | Utah              | Clinic Match List - UTS.xls             | Clinic Match List - UTS.xls                 |
      | Regions         | Utah              | Adjustment Checklist - UTS              | Adjustment Checklist - UTS.xlsx             |
      | Regions         | Utah              | DAILY Snapshot - UTS                    | DAILY Snapshot - UTS.xlsx                   |
      | Regions         | Utah              | Phone Tracker - UTS                     | Phone Tracker - UTS.xlsx                    |
      | Regions         | Texas San Antonio | TXS Regional Training Calendar          | TXS Regional Training Calendar 2018.xlsx    |
      | Regions         | Texas San Antonio | Regional Phone Roster - Updated 02-19   | Regional Phone Roster - TXS.xls             |
      | Regions         | Texas San Antonio | Bank Deposit Log                        | Bank Deposit Log.docx                       |
      | Regions         | Texas San Antonio | Torque Wrench Check List - TXS          | Torque Log.xls                              |
      | Regions         | Texas San Antonio | 2017 Schedule of Events - TXS           | 2017 Schedule of Events - TXS.xlsx          |
      | Regions         | Texas San Antonio | 2018 Schedule of Events - TXS           | 2018 Schedule of Events - TXS.xlsx          |
      | Regions         | Texas San Antonio | Full Time Interview Questionnaire - TXS | Full Time Interview Questionnaire - TXS.doc |
      | Regions         | Texas San Antonio | Part Time Interview Questionnaire - TXS | Part Time Interview Questionnaire - TXS.doc |
      | Regions         | Texas San Antonio | Mgr Leadership Questionnaire - TXS      | TXS Manager Leadership Questionnaire.docx   |

  @Videos
  Scenario Outline: Certified Best Practicies
    When I select "<SubMenu>" sub-menu item under the "<Menu Item>" menu header option
    And  I switch to the iFrame where video is playing
    And  I click on exact "<text>"
    Then I validate video is playing "<text>"

    Examples:
      | Menu Item           | SubMenu                  | text                                   |
      | Safety/Quality      | Certified Best Practices | A Message from Michael                 |
      | Safety/Quality      | Certified Best Practices | Get The Numbers                        |
      | Safety/Quality      | Certified Best Practices | Road Force Elite                       |
      | Safety/Quality      | Certified Best Practices | Tire Bar                               |
      | Safety/Quality      | Certified Best Practices | Moving Vehicles                        |
      | Safety/Quality      | Certified Best Practices | Removing and Installing the Assembly   |
      | Safety/Quality      | Certified Best Practices | Lifts                                  |
      | CES                 | 3 Phase CES & ETV        | Strategic Intent                       |
      | CES                 | 3 Phase CES & ETV        | Doctor Doctor                          |
      | CES                 | 3 Phase CES & ETV        | The Differentiator                     |
      | CES                 | 3 Phase CES & ETV        | How Important Is It to You             |
      | CES                 | 3 Phase CES & ETV        | CES = Happy Customers                  |
      | CES                 | 3 Phase CES & ETV        | Regional Ownership                     |
      | CES                 | 3 Phase CES & ETV        | CES Wheels                             |
      | CES                 | 3 Phase CES & ETV        | The Pendulum                           |
      | CES                 | 3 Phase CES & ETV        | Overcoming Objections                  |
      | CES                 | 3 Phase CES & ETV        | CES Intro to PDL                       |
      | CES                 | 3 Phase CES & ETV        | The Customer Speaks                    |
      | CES                 | 3 Phase CES & ETV        | CES Dennis                             |
      | CES                 | 3 Phase CES & ETV        | 1.3 How Safe? Listen for Nuggets       |
      | CES                 | 3 Phase CES & ETV        | 1.2 Continue to Build the Relationship |
      | CES                 | 3 Phase CES & ETV        | 1.2 Find True Needs                    |
      | CES                 | 3 Phase CES & ETV        | 1.2 Address Immediate Needs            |
      | LEAD/Speed of Trust | LEAD                     | The Purpose of LEAD                    |
      | LEAD/Speed of Trust | LEAD                     | Focus on the Wildly Important          |
      | LEAD/Speed of Trust | LEAD                     | Create a Compelling Scoreboard         |
      | LEAD/Speed of Trust | LEAD                     | LEAD Introduction                      |
      | EPIC                | EPIC                     | PDL: Data is Key                       |
      | EPIC                | EPIC                     | PDL: Customer Feedback                 |
      | EPIC                | EPIC                     | PDL & CES Partnership                  |
      | EPIC                | EPIC                     | EPIC 2: Empower Customers with Web     |
      | EPIC                | EPIC                     | EPIC 1: New Website Huddle             |


  Scenario Outline: Document
    When I select "<SubMenu>" sub-menu item under the "<Menu Item>" menu header option
    And I Download the "<Link>" link on the page
    Then  I verify the "<FileName>" file has downloaded to the Downloads folder
    Then I open the "<FileName>" file from Downloads folder
    And I validate the "<Text>" in the file
    And I close the File with "<FileName>"
    Examples:
      | Menu Item       | SubMenu           | Link                                    | FileName                                    | Text                             |
      | Human Resources | HR Home           | 2018 PTO Form (Office)                  | 2018+PTO+Form+(Office).xlsx                 | PAID TIME OFF CALENDAR           |
      | Regions         | Texas San Antonio | Full Time Interview Questionnaire - TXS | Full+Time+Interview+Questionnaire+-+TXS.doc | INTERVIEW QUESTIONS & GUIDELINES |