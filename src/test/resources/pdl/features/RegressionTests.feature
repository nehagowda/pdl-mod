     @pdl
Feature: PDL Regression Tests

  Background:
    Given I go to the pdl homepage

  @regression_pdl
  @10911
  @results1ConfirmMainTypical02Priorities
  Scenario Outline: PDL_RE1-01_RESULTS_1Confirm MAIN_Typical 02 priorities (ALM #10911)
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    When I select a vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify that the Tire Size value is "<TireSize>"
    And  I verify that the "not your tire" is displayed on driving details page
    And  I select Not your tire under Tire Size
    When I click on optional tire size modal close icon
    Then I am brought to the page with header "DRIVING DETAILS" in pdl
    When I select View Tire Recommendations
    Then I am brought to the page with header "RESULTS" in pdl
    And  I verify the Driving Details is displayed on the page
    And  I verify that the Top Recommendation ribbon is displayed only for the first tire in the list
    And  I verify the vehicle year is set to "<Year>" at upper left header
    And  I verify the vehicle make is set to "<Make>" at upper left header
    And  I verify the vehicle model is set to "<Model>" at upper left header
    And  I verify the vehicle trim is set to "<Trim>" at upper left header
    And  I verify the summary zip code is set to "<ZipCode>"
    And  I verify the summary car is set to "<Year> <Make> <Model>"
    And  I verify the summary Miles Driven is set to "<Miles>"
    And  I verify the summary Tire Size is set to "<ResultsTireSize>"
    And  I verify the "Typical" summary Driving Priorities
    And  I verify the tire brand "<Brand>" is listed on results page
    And  I verify the tire name "<TireName>" is listed on results page
    And  I verify the front tire size "<FrontSize>" is listed on results page
    And  I verify the rear tire size "<RearSize>" is listed on results page
    And  I verify the "wet climate stop" label is listed only once on results page
    And  I verify the "expected tire life" label is listed only once on results page
    And  I verify all View Details buttons are enabled on results page
    And  I verify the item stock status label is present for all the product items on results page
    And  I verify the product article number label is present for all the product items on results page
    And  I verify the item qty stock status label is present for all the product items on results page
    And  I verify all tire images are on results page

    Examples:
      | StoreID | PayrollID | Year | Make | Model | Trim  | Assembly                  | TireSize                          | ZipCode | Miles | ResultsTireSize | Brand   | TireName    | FrontSize              | RearSize                |
      | MNM29   | 919919    | 2015 | BMW  | M3    | Sedan | F 255/35-19 - R 275/35-19 | Front: 255/35 R19Rear: 275/35 R19 | 55125   | 15000 | 255/35 R19      | General | G-MAX AS-03 | 255 /35 R19 96W XL BSW | 275 /35 R19 100W XL BSW |

  @regression_pdl
  @9896
  @resultsStaggeredOnPromotion
  Scenario Outline: PDL_RE2-06_RESULTS_FILTER RESULTS - On Promotion (ALM#9896)
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    When I select a vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify that the Tire Size value is "<TireSizeF>"
    And  I verify that the Tire Size value is "<TireSizeR>"
    When I select View Tire Recommendations
    Then I am brought to the page with header "RESULTS" in pdl
    And  I verify the vehicle year is set to "<Year>" at upper left header
    And  I verify the vehicle make is set to "<Make>" at upper left header
    And  I verify the vehicle model is set to "<Model>" at upper left header
    And  I verify the vehicle trim is set to "<Trim>" at upper left header
    When I select the filter results button
    And  I select the Other checkbox with "<Text>"
    Then I verify the No Products Match error on the Filter popup
    When I select Apply Filter
    Then I am brought to the page with header "RESULTS" in pdl

    Examples:
      | StoreID | PayrollID | Year | Make | Model | Trim  | Assembly                  | TireSizeF  | TireSizeR  | Text         |
      | MNM29   | 919919    | 2015 | BMW  | M3    | Sedan | F 255/35-19 - R 275/35-19 | 255/35 R19 | 275/35 R19 | On Promotion |

  @regression_pdl
  @10940
  @primaryDrivingLocationStaggered
  Scenario Outline: PDL_DD4-04_Primary Driving Location - change zip code_Staggered (ALM #10940)
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    When I update the zip code to "<ZipCode>"
    Then I verify the zip code is set to "<ZipCode>"
    When I set miles driven per year to "<Miles>"
    Then I verify that the Miles Driven value is "5k"
    When I select a vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify that the Tire Size value is "<TireSize>"
    And  I verify that the "View Tire Recommendations" button is enabled
    When I select View Tire Recommendations
    Then I am brought to the page with header "RESULTS" in pdl

    Examples:
      | StoreID | PayrollID | Year | Make | Model | Trim  | Assembly                  | TireSize                          | ZipCode | Miles |
      | MNM29   | 919919    | 2015 | BMW  | M3    | Sedan | F 255/35-19 - R 275/35-19 | Front: 255/35 R19Rear: 275/35 R19 | 85250   | 5K    |

  @regression_pdl
  @10985
  @resultsConfirmMainStaggeredChangeZipcode
  Scenario Outline: PDL_RE1-01_RESULTS_1Confirm MAIN_Staggered_Change Zipcode (ALM #10985)
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    When I update the zip code to "<ZipCode>"
    Then I verify the zip code is set to "<ZipCode>"
    When I set miles driven per year to "<Miles>"
    Then I verify that the Miles Driven value is "5k"
    When I select a vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify that the Tire Size value is "<TireSize>"
    And  I verify that the "View Tire Recommendations" button is enabled
    When I select View Tire Recommendations
    Then I am brought to the page with header "RESULTS" in pdl
    And  I verify the Driving Details is displayed on the page
    And  I verify that the Top Recommendation ribbon is displayed only for the first tire in the list
    And  I verify the vehicle year is set to "<Year>" at upper left header
    And  I verify the vehicle make is set to "<Make>" at upper left header
    And  I verify the vehicle model is set to "<Model>" at upper left header
    And  I verify the vehicle trim is set to "<Trim>" at upper left header
    And  I verify the summary zip code is set to "<ZipCode>"
    And  I verify the summary car is set to "<Year> <Make> <Model>"
    And  I verify the summary Miles Driven is set to "<MilesDriven>"
    And  I verify the summary Tire Size is set to "<ResultsTireSize>"
    And  I verify the "Typical" summary Driving Priorities
    And  I verify all tire images are on results page
    And  I verify the tire brand "<Brand>" is listed on results page
    And  I verify the tire name "<TireName>" is listed on results page
    And  I verify the front tire size "<FrontSize>" is listed on results page
    And  I verify the rear tire size "<RearSize>" is listed on results page
    And  I verify the item code "<ItemCode1>" is listed on results page
    And  I verify the item code "<ItemCode2>" is listed on results page
    And  I verify the "wet climate stop" label is listed only once on results page
    And  I verify the "expected tire life" label is listed only once on results page
    And  I verify all View Details buttons are enabled on results page
    And  I verify the item stock status label is present for all the product items on results page
    And  I verify the product article number label is present for all the product items on results page
    And  I verify the item qty stock status label is present for all the product items on results page


    Examples:
      | StoreID | PayrollID | Year | Make | Model | Trim  | Assembly                  | TireSize                          | ZipCode | Miles | MilesDriven | ResultsTireSize | Brand   | TireName    | FrontSize              | RearSize                | ItemCode1 | ItemCode2 |
      | MNM29   | 919919    | 2015 | BMW  | M3    | Sedan | F 255/35-19 - R 275/35-19 | Front: 255/35 R19Rear: 275/35 R19 | 85250   | 5K    | 5000        | 255/35 R19      | General | G-MAX AS-03 | 255 /35 R19 96W XL BSW | 275 /35 R19 100W XL BSW | 32942     | 32943     |


  @regression_pdl
  @10987
  @resultsConfirmMainStaggeredPerformance
  Scenario Outline: PDL_RE1-01_RESULTS_1Confirm MAIN_Staggered_Peformance (ALM #10987)
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    When I switch to performance for driving priorities
    Then I verify the "performance" Driving Priorities
    And  I verify the zip code is set to "<ZipCode>"
    And  I verify the city and state are set to "<CityState>"
    When I select a vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify that the Tire Size value is "<TireSize>"
    When I select View Tire Recommendations
    Then I am brought to the page with header "RESULTS" in pdl
    And  I verify the Driving Details is displayed on the page
    And  I verify that the Top Recommendation ribbon is displayed only for the first tire in the list
    And  I verify the vehicle year is set to "<Year>" at upper left header
    And  I verify the vehicle make is set to "<Make>" at upper left header
    And  I verify the vehicle model is set to "<Model>" at upper left header
    And  I verify the vehicle trim is set to "<Trim>" at upper left header
    And  I verify the summary zip code is set to "<ZipCode>"
    And  I verify the summary car is set to "<Year> <Make> <Model>"
    And  I verify the summary Miles Driven is set to "<MilesDriven>"
    And  I verify the summary Tire Size is set to "<ResultsTireSize>"
    And  I verify the "performance" summary Driving Priorities
    And  I verify all tire images are on results page
    And  I verify the tire brand "<Brand>" is listed on results page
    And  I verify the tire name "<TireName>" is listed on results page
    And  I verify the front tire size "<FrontSize>" is listed on results page
    And  I verify the rear tire size "<RearSize>" is listed on results page
    And  I verify the item code "<ItemCode1>" is listed on results page
    And  I verify the item code "<ItemCode2>" is listed on results page
    And  I verify the "wet climate stop" label is listed only once on results page
    And  I verify the "expected tire life" label is listed only once on results page
    And  I verify all View Details buttons are enabled on results page
    And  I verify the item stock status label is present for all the product items on results page
    And  I verify the product article number label is present for all the product items on results page
    And  I verify the item qty stock status label is present for all the product items on results page

    Examples:
      | StoreID | PayrollID | Year | Make | Model | Trim  | Assembly                  | TireSize                          | ZipCode | CityState      | MilesDriven | ResultsTireSize | Brand   | TireName    | FrontSize              | RearSize                | ItemCode1 | ItemCode2 |
      | MNM29   | 919919    | 2015 | BMW  | M3    | Sedan | F 255/35-19 - R 275/35-19 | Front: 255/35 R19Rear: 275/35 R19 | 55125   | SAINT PAUL, MN | 5000        | 255/35 R19      | General | G-MAX AS-03 | 255 /35 R19 96W XL BSW | 275 /35 R19 100W XL BSW | 32942     | 32943     |


  @regression_pdl
  @11290
  Scenario Outline: PDL_DD1-05_1StandAlone_PayrollIDError (ALM #11290)
    Then I verify Store ID is displayed on the home page
    And  I verify Payroll ID is displayed on the home page
    And  I verify that the "Enter" button is disabled
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    Then I verify "Payroll ID" message on home page

    Examples:
      | StoreID | PayrollID |
      | MNM29   | 1234567   |

  @regression_pdl
  @10913
  Scenario Outline: PDL_DD1-05_1StandAlone_PayrollIDError (ALM #10913)
    Then I verify Store ID is displayed on the home page
    And  I verify Payroll ID is displayed on the home page
    And  I verify that the "Enter" button is disabled
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    Then I verify "Store ID" message on home page

    Examples:
      | StoreID | PayrollID |
      | AUTO 01 | 123456    |

  @regression_pdl
  @10991
  @tireComparisonStaggeredPerformance
  Scenario Outline: PDL_TC1-01_Tire Comparison_Staggered_Performance (ALM #10991)
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    And  I switch to performance for driving priorities
    Then I verify the "performance" Driving Priorities
    When I select a vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select Not your tire under Tire Size
    And  I select staggered set "<Set>"
    And  I select optional tire sizes for front "<FrontTireSize>" and for rear "<RearTireSize>"
    And  I select View Tire Recommendations
    And  I select the product checkbox for item "<ItemId1>" from the products list results
    And  I select the product checkbox for item "<ItemId2>" from the products list results
    And  I select the compare tires button
    Then I verify the back button is labeled "Results"
    And  I verify all tire images are on the compare page
    And  I verify vendor, model, size, and price of the tire with item id "<ItemId1>"
    And  I verify vendor, model, size, and price of the tire with item id "<ItemId2>"
    And  I verify each tire in all comparison sections contains a graph
    And  I verify each comparison section of the tire comparison page contains at least one green ribbon
    When I select "View Details" for item id "<ItemId1>" from the compare page
    Then I verify tire details page header is "TIRE DETAILS"
    And  I verify the tire image and alt text is displayed on tire details page
    And  I verify the tire brand "<Brand>" is listed on tire details page
    And  I verify the tire name "<TireName>" is listed on tire details page
    And  I verify the front tire item id "<ItemId1>" is listed on tire details page
    And  I verify the front tire quantity is "2" on tire details page
    And  I verify the rear tire quantity is "2" on tire details page
    And  I verify the stopping distance ratings labels are listed on tire details page
    And  I verify the expected tire life range labels are listed on tire details page
    And  I verify the cost rating miles per dollar label is listed on tire details page

    Examples:
      | StoreID | PayrollID | Year | Make | Model | Trim  | Assembly                  | FrontTireSize | RearTireSize | Set   | ItemId1 | ItemId2 | Brand       | TireName                   |
      | MNM29   | 919919    | 2015 | BMW  | M3    | Sedan | F 255/35-19 - R 275/35-19 | 265/35-18     | 285/35-18    | 18-18 | 36251   | 26043   | Bridgestone | POTENZA S-04 POLE POSITION |

  @regression_pdl
  @9843
  Scenario Outline: PDL_RE2-02_RESULTS_FILTER RESULTS-Brands (ALM # 9843)
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    When I select a vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select View Tire Recommendations
    Then I am brought to the page with header "RESULTS" in pdl
    And  I verify the item code "<ItemCode1>" is listed on results page
    And  I verify the item code "<ItemCode2>" is listed on results page
    And  I verify the item code "<ItemCode3>" is listed on results page
    When I select the filter results button
    And  I verify the item code "<ItemCode1>" is listed on results page
    And  I verify the item code "<ItemCode2>" is listed on results page
    And  I verify the item code "<ItemCode3>" is listed on results page

    Examples:
      | StoreID | PayrollID | Year | Make | Model | Trim  | Assembly                  | ItemCode1 | ItemCode2 | ItemCode3 |
      | MNM29   | 919919    | 2015 | BMW  | M3    | Sedan | F 255/35-19 - R 275/35-19 | 18596     | 18606     | 26022     |