@sap
@bopis
Feature: BOPIS Tests

  @bopisCreateEndToEnd
  Scenario Outline: Bopis Create_SAP
    When I change to the default store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    When I store the order number
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the web order number into "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    Then I verify if tasks with errors exist in posdm/posdt
    When I click on "document flow button" in posdm page
    And  I click on "Document flow outbound" in posdm page
    And  I click "Vertical Scroll" till "External document value 2" is visible in posdm
    And  I save the "External document value 1" element text value in PosDm page
    And  I save the "External document value 2" element text value in PosDm page
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table>" in PosDm page
    And  I hit return
    And  I enter saved "External document value 1" element value into following element "Aggregator number"
    And  I click on "Multiple selection" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter saved "External document value 2" element value into following element "Single value row"
    And  I click on "Copy button" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "Not Equal to"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDm page
    And  I save the "Transaction Id 2" element text value in PosDm page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table 2>" in PosDm page
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I click on "Transaction arrow button" in posdm page
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter saved "Transaction Id 2" element value into following element "Single value row SAP"
    And  I click on "Copy button SAP" in posdm page
    And  I switch back to the main window in SAP
    And  I click on "Execute button SAP" in posdm page
    And  I save the "Idoc value 1" element text value in PosDm page
    And  I save the "Idoc value 2" element text value in PosDm page
    And  I enter t-code "<SAPECC TCode 3>" in the command field
    And  I enter "EDIDC" information on "Table SE16N" in Posdm page
    And  I hit return
    And  I click on "Layout" in posdm page
    And  I click on "drop down" in procure to pay page
    And  I switch to the first iFrame in SAP
    And  I click on partial text "QA_IDOCTYPE"
    And  I switch back to the main window in SAP
    And  I enter saved "Idoc value 1" element value into following element "Idoc Input SE16N"
    And  I select the "Execute" button on the Idoc page
    And  I save the Idoc Number and assign it to WPUUMS/WPUTAB key in se16n
    And  I enter t-code "<SAPECC TCode 3>" in the command field
    And  I enter "EDIDC" information on "Table SE16N" in Posdm page
    And  I hit return
    And  I click on "Layout" in posdm page
    And  I click on "drop down" in procure to pay page
    And  I switch to the first iFrame in SAP
    And  I click on partial text "QA_IDOCTYPE"
    And  I switch back to the main window in SAP
    And  I enter saved "Idoc value 2" element value into following element "Idoc Input SE16N"
    And  I select the "Execute" button on the Idoc page
    And  I save the Idoc Number and assign it to WPUUMS/WPUTAB key in se16n
    And  I enter t-code "<TCode3>" in the command field
    And  I enter saved "WPUTAB" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I double click on "Billing document WPUTAB" in Idoc page
    And  I click on "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on element with value of "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on "Views" in IDOC page
    And  I click on "List Output"
    And  I enter t-code "<TCode4>" in the command field
    And  I enter saved "WPUFIB" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I save the "Bopis G/L Account from WPUFIB" element text value in PosDm page
    And  I enter t-code "<TCode5>" in the command field
    And  I enter the Bopis G/L Account number into "Document number input FB03" in fb03
    And  I hit return

    Examples:
      | TCode 1       | Table TCode | TCode3  | Outbound/Inbound Idoc Table | TCode4  | Outbound/Inbound Idoc Table 2 | SAPECC TCode 3 | Year | Make  | Model | Trim     | Assembly | FitmentOption | ItemCode | ProductName | Customer                    | Credit Card | TCode5  |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | /n wper | EDIDS                         | /n SE16N       | 2012 | Honda | Civic | Coupe DX | none     | tire          | 31098    | FP0612 A/S  | DEFAULT_CUSTOMER_BOPIS_VISA | Visa Bopis  | /n FB03 |

  @13430
  Scenario Outline: SAP_ECC_ORDERS_BOPIS_TLOGS_Create_Visa and Master_1015-2018
    When I change to the default store
    And  I click the discount tire logo
    Then I am brought to the homepage
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select a fitment option "<FitmentOption>" and shop for all tires/wheels
    And  I add item "<ItemCode>" of type "none" to my cart and "View shopping Cart"
    Then I should see product "<ItemCode>" on the "cart" page
    And  I verify the required fees and add-ons sections are expanded
    When I select the checkout option "default"
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click next step for customer information
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I enter payment info with "<Credit Card>" and confirm Checkout Summary as "<Customer>"
    And  I place the order for "<Customer>"
    Then I should see reservation confirmation message with details "<ProductName>" and "<ItemCode>"
    When I store the order number
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the web order number into "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    Then I verify if tasks with errors exist in posdm/posdt
    When I click on "document flow button" in posdm page
    And  I wait for "2" seconds
    And  I click on "Document flow outbound" in posdm page
    And  I click "Vertical Scroll" till "External document value 2" is visible in posdm
    And  I save the "External document value 1" element text value in PosDm page
    And  I save the "External document value 2" element text value in PosDm page
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table>" in PosDm page
    And  I hit return
    And  I enter saved "External document value 1" element value into following element "Aggregator number"
    And  I click on "Multiple selection" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter saved "External document value 2" element value into following element "Single value row"
    And  I click on "Copy button" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "Not Equal to"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDm page
    And  I save the "Transaction Id 2" element text value in PosDm page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table 2>" in PosDm page
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I click on "Transaction arrow button" in posdm page
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter saved "Transaction Id 2" element value into following element "Single value row SAP"
    And  I click on "Copy button SAP" in posdm page
    And  I switch back to the main window in SAP
    And  I click on "Execute button SAP" in posdm page
    And  I save the "Idoc value 1" element text value in PosDm page
    And  I save the "Idoc value 2" element text value in PosDm page
    And  I enter t-code "<SAPECC TCode 3>" in the command field
    And  I enter "EDIDC" information on "Table SE16N" in Posdm page
    And  I hit return
    And  I click on "Layout" in posdm page
    And  I click on "drop down" in procure to pay page
    And  I switch to the first iFrame in SAP
    And  I click on partial text "QA_IDOCTYPE"
    And  I switch back to the main window in SAP
    And  I enter saved "Idoc value 1" element value into following element "Idoc Input SE16N"
    And  I select the "Execute" button on the Idoc page
    And  I save the Idoc Number and assign it to WPUUMS/WPUTAB key in se16n
    And  I enter t-code "<SAPECC TCode 3>" in the command field
    And  I enter "EDIDC" information on "Table SE16N" in Posdm page
    And  I hit return
    And  I click on "Layout" in posdm page
    And  I click on "drop down" in procure to pay page
    And  I switch to the first iFrame in SAP
    And  I click on partial text "QA_IDOCTYPE"
    And  I switch back to the main window in SAP
    And  I enter saved "Idoc value 2" element value into following element "Idoc Input SE16N"
    And  I select the "Execute" button on the Idoc page
    And  I save the Idoc Number and assign it to WPUUMS/WPUTAB key in se16n
    And  I enter t-code "<TCode3>" in the command field
    And  I enter saved "WPUTAB" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I double click on "Billing document WPUTAB" in Idoc page
    And  I click on "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on element with value of "Accounting document"
    And  I switch back to the main window in SAP
    Then I validate "Account Nr in Wper" entered matches original "<G/L Account>" in Idoc page
    And  I validate "Bopis Deposit" entered matches original "<Bopis Deposit>" in Idoc page
    And  I verify "Bopis Assignment" field is not empty in idoc page
    And  I verify "Assignment Nr in Wper" field is not empty in idoc page
    When I click on "Views" in IDOC page
    And  I click on "List Output"
    And  I enter t-code "<TCode4>" in the command field
    And  I enter saved "WPUFIB" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I save the "Bopis G/L Account from WPUFIB" element text value in PosDm page
    And  I enter t-code "<TCode5>" in the command field
    And  I enter the Bopis G/L Account number into "Document number input FB03" in fb03
    And  I hit return
    Then I validate "Account Nr in Wper" entered matches original "<G/L Account in Fb03>" in Idoc page
    And  I validate "Site Number FB03" entered matches original "<Site in Fb03>" in Idoc page
    And  I validate "Profit Number FB03" entered matches original "<Store in Fb03>" in Idoc page

    Examples:
      | TCode 1       | Table TCode | TCode3  | Outbound/Inbound Idoc Table | TCode4  | Outbound/Inbound Idoc Table 2 | SAPECC TCode 3 | Year | Make  | Model | Trim     | Assembly | FitmentOption | ItemCode | ProductName | Customer                          | Credit Card      | TCode5  | G/L Account | Bopis Deposit | G/L Account in Fb03 | Site in Fb03 | Store in Fb03 |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | /n wper | EDIDS                         | /n SE16N       | 2012 | Honda | Civic | Coupe DX | none     | tire          | 31098    | FP0612 A/S  | DEFAULT_CUSTOMER_BOPIS_MASTERCARD | MasterCard Bopis | /n FB03 | 119040      | 214635        | 651600              | 8000         | 1002          |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | /n wper | EDIDS                         | /n SE16N       | 2012 | Honda | Civic | Coupe DX | none     | tire          | 31098    | FP0612 A/S  | DEFAULT_CUSTOMER_BOPIS_VISA       | Visa Bopis       | /n FB03 | 119040      | 214635        | 651600              | 8000         | 1002          |

  @15665
  @15666
  @15667
  Scenario Outline: SAP BOPIS Finalize any web order of payment types (Order = CC1,CC1_2,MasterCard,Discover,Visa and AMEX) (ALM#15665, ALM#15666 and ALM#15667)
  """ To do : Uncomment below CC1 (Column 1) and CC1_2 (Column 2) examples whenever card issues are fixed."""
    When I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I extract "invoice" Bopis orders from excel sheet which is in "2"th row and "<Column number>"th cell from sheet name "Finalize_Refund_Invoice" and excel name "BOPIS.xls"
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<T-Code1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in PosdM
    And  I click on "Execute" in posdm page
    And  I click on "document flow button" in posdm page
    And  I click on "Document flow out bound" in posdm page
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    And  I click on "document flow button" in posdm page
    And  I click on "Document flow outbound" in posdm page
    And  I click "Vertical Scroll" till "Outbox IDOC" is visible in posdm
    And  I save the "Outbox IDOC" element text value in PosDm page
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I switch to the first iFrame
    And  I enter t-code "<T-Code2>" in the command field
    And  I select "Table name" and enter "<Table value>" in PosDm page
    And  I hit return
    And  I enter saved "Outbox IDOC" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "Not Equal to"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDm page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<T-Code2>" in the command field
    And  I select "Table name" and enter "<Table value>" in PosDm page
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I click on "Execute button SAP" in posdm page
    And  I save the "Idoc value 1" element text value in PosDm page
    And  I enter t-code "<T-Code3>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I select the "Execute" button on the Idoc page
    And  I double click on "Article" in Idoc page
    Then I validate "Movement Type" entered matches original "<Movement type value>" in Idoc page
    When I click on "Back Button" in order to cash page
    And  I double click on "Billing" in Idoc page
    And  I click on "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on element with value of "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on "Views" in IDOC page
    And  I click on "List Output"
    Then I validate "Sales Tax Account" entered matches original "<GL Number Sales tax>" in wper
    And  I validate "Environment Fee Account" entered matches original "<GL Number Environmental fee>" in wper
    And  I validate "Sales Revenue Account" entered matches original "<GL Number Sales Revenue>" in wper

    Examples:

      | T-Code1       | T-Code2 | Table value | T-Code3 | Movement type value | GL Number Sales tax | GL Number Environmental fee | GL Number Sales Revenue | Column number |
#      | /n/posdw/mon0 | /n SE16 | EDIDS       | /n WPER | 251                 | 216100              | 216400                      | 410000                  | 1             |
#      | /n/posdw/mon0 | /n SE16 | EDIDS       | /n WPER | 251                 | 216100              | 216400                      | 410000                  | 2             |
      | /n/posdw/mon0 | /n SE16 | EDIDS       | /n WPER | 251                 | 216100              | 216400                      | 410000                  | 3             |
      | /n/posdw/mon0 | /n SE16 | EDIDS       | /n WPER | 251                 | 216100              | 216400                      | 410000                  | 4             |
      | /n/posdw/mon0 | /n SE16 | EDIDS       | /n WPER | 251                 | 216100              | 216400                      | 410000                  | 5             |
      | /n/posdw/mon0 | /n SE16 | EDIDS       | /n WPER | 251                 | 216100              | 216400                      | 410000                  | 6             |

