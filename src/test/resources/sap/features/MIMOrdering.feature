@sap
@mimOrdering
Feature: MIM Ordering

  @15852
  @createDTDTransfer
  @extendedAssortment
  @jenkinsRun
  Scenario Outline: INT_SAP_POC_Extended Assortment_STO Creation through MIM (ALM#15852)
    """ This will be replaced by DTD Transfer in SHOP """
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I click on "ORDERING"
    And  I click on "Create DTD Transfer"
    And  I switch to the first iFrame in SAP
    And  I enter "<SupplyingSite>" into the field with label "Supplying Site"
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I click on "Save and Print Preview"
    Then I verify the success message with green image
    When I save the "DTD Transfer po" PO
    And  I switch back to the main window in SAP
    And  I click on "GENERAL"
    And  I switch to the first iFrame in SAP
    And  I enter t-code "<TCode>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    Then I verify the purchase order number is in the page title

    Examples:
      | TCode    | SupplyingSite | Article | Quantity |
      | /n me23n | 1001          | 17899   | 4        |

  @15853
  @validatePOGoods
  @extendedAssortment
  @jenkinsRun
  Scenario Outline: INT_SAP_POC_Extended Assortment_Validate PO and Goods -15853 (ALM#15853)
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<ReceivingSite>"
    And  I click on "ORDERING"
    And  I click on "Create DTD Transfer"
    And  I switch to the first iFrame in SAP
    And  I enter "<SupplyingSite>" into the field with label "Supplying Site"
    And  I enter "<Article>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I click on "Save and Print Preview"
    Then I verify the success message with green image
    When I save the "DTD Transfer po" PO
    And  I switch back to the main window in SAP
    And  I set the site to "<SupplyingSite>"
    And  I click on "GOODS MOVEMENTS"
    And  I click on "Post Goods Issue"
    And  I switch to the first iFrame in SAP
    And  I enter Purchase Order Number into "Post Goods Issue PO"
    And  I click on "Continue"
    And  I click on "Save and Print Preview"
    Then I verify the success message with green image
    When I switch back to the main window in SAP
    And  I set the site to "<ReceivingSite>"
    And  I click on "RECEIVING"
    And  I click on "Post Goods Receipt"
    And  I switch to the first iFrame in SAP
    And  I enter Purchase Order Number into "Post Goods Issue PO"
    And  I click on "Continue"
    And  I click on "Save and Print Preview"
    Then I verify the success message with green image

    Examples:
      | SupplyingSite | ReceivingSite | Article | Quantity |
      | 1001          | 1851          | 17899   | 4        |