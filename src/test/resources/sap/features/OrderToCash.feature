@sap
@OrderToCash
Feature: Order To Cash

  @3656
  @jenkinsRun
  Scenario Outline: MD_03 - Customer Credit Management Change - FD32 (ALM#3656)
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I enter "<Credit Management Customer Id>" into the field with label "Customer" and hit tab
    And  I enter "<Credit Control Area>" into the field with label "Credit control area" and hit tab
    And  I hit F7
    And  I wait for "2" seconds
    And  I hit return
    And  I wait for "2" seconds
    And  I hit return
    And  I wait for "2" seconds
    And  I enter new price into field with label "Total amount" in position "2" in order to cash page
    And  I enter "<Individual Limit>" into the field with label "Individual limit" and hit tab
    And  I enter "<Currency>" into the field with label "Currency" and hit tab
    And  I click on element with title attribute value "Save"
    Then I verify the success status in SAP

    Examples:
      | TCode | Credit Management Customer Id | Credit Control Area | Individual Limit | Currency |
      | FD32  | 18984                         | 1000                | 6000             | USD      |

  @3637
  @3642
  @3635
  @3636
  @3634
  @3633
  @3631
  @3632
  @3630
  @3638
  @jenkinsRun
  Scenario Outline: Verification of AR Customer details when updated in SAP
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "XD02" in the command field
    And  I wait for iframe with check ok to appear
    And  I switch to the first iFrame in SAP
    And  I enter "<Customer ID>" into the field with label "Customer" and hit tab
    And  I enter "<Distribution Channel>" into the field with label "Distribution Channel" and hit tab
    And  I enter "<Division>" into the field with label "Division"
    And  I switch back to the main window in SAP
    And  I hit return
    And  I click on "Menu"
    And  I click on "Extras"
    And  I click on "Additional Data"
    And  I wait for iframe with check ok to appear
    And  I switch to the first iFrame in SAP
    And  I enter "<Certificate>" into the field with label "Certificate" and hit tab
    And  I enter "<DropDown Option>" into the field with label "Invoice Copy" and hit tab
    And  I enter "<DropDown Option>" into the field with label "PO Required" and hit tab
    And  I switch back to the main window in SAP
    And  I hit return
    And  I click on "Address"
    And  I enter "<House Number>" into the field with label "House Number/Street" at position "1"
    And  I enter "<Street>" into the field with label "House Number/Street" at position "2"
    And  I enter "<District>" into the field with label "District" and hit tab
    And  I enter "<City>" into the field with label "City/State/ZIP Code" at position "1"
    And  I enter "<State>" into the field with label "City/State/ZIP Code" at position "2"
    And  I enter "<Zip Code>" into the field with label "City/State/ZIP Code" at position "3"
    And  I enter "<Time Zone>" into the field with label "Time zone" and hit tab
    And  I hit return
    And  I enter "<Email 2>" into the field with label "E-Mail" and hit tab
    And  I enter "<Telephone>" into the field with label "Telephone" and hit tab
    And  I click on "Menu"
    And  I click on "Extras"
    And  I click on "Classification"
    And  I enter "05" into the table at row number "1" and column number "2" at position "2" in commonTcodes
    And  I hit return
    And  I go back
    And  I click on "Menu"
    And  I click on "Extras"
    And  I click on "Texts"
    And  I enter "<POS Comment>" into the table at row number "1" and column number "4" at position "1" in commonTcodes
    And  I go back
    And  I hit return
    And  I click on "Menu"
    And  I click on "Goto"
    And  I click on partial text "Sales Area Data"
    And  I click on "Partner Functions"
    And  I double click on partial text or value "Ship-to party"
    And  I click display/change
    And  I click on "Sales Area Data"
    And  I click on "Billing Documents"
    And  I click on "US" text or value
    And  I click on partial text "Licenses"
    And  I switch to the first iFrame in SAP
    And  I check for an empty field and enter "<License no>" information on "First Row" in order to Cash page where flag "Reset"
    And  I check for an empty field and enter "<valid from>" information on "Third Row" in order to Cash page where flag ""
    And  I check for an empty field and enter "<valid to>" information on "Fourth Row" in order to Cash page where flag ""
    And  I hit return
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Save"
    Then I verify the success status in SAP

    Examples:
      | Customer ID |  Distribution Channel | Division | Email 2                   | DropDown Option | Certificate | Telephone    | House Number | Street            | District | City       | State | Zip Code | Time Zone | POS Comment              | License no | valid from | valid to   |
      | 18712       |  10                   | 10       |autouser@americastire.com | 01              | 02          | 302-235-6548 | 20225        | N scottsdale Road | Maricopa | Scottsdale | AZ    | 85255    | MSTNO     | autouser from automation | 123564789  | 09/18/2018 | 09/18/2020 |

  @3657
  @jenkinsRun
  Scenario Outline: VMD_03 - Maintain Customer - Add New Customer  - XD01 - (ALM # 3657)
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "XD01" in the command field
    And  I wait for iframe with check ok to appear
    And  I switch to the first iFrame in SAP
    And  I enter "<Account Group>" into the field with label "Account group" and hit tab
    And  I select the input field with label "Distribution Channel" at position "1" and enter "<Distribution Channel>"
    And  I hit tab
    And  I enter "<Division>" into the field with label "Division"
    And  I hit return
    And  I switch back to the main window in SAP
    And  I input "AR Automation" as name
    And  I enter "<House Number>" into the field with label "House Number/Street" at position "1"
    And  I enter "<Street>" into the field with label "House Number/Street" at position "2"
    And  I enter "<City>" into the field with label "City/State/ZIP Code" at position "1"
    And  I enter "<State>" into the field with label "City/State/ZIP Code" at position "2"
    And  I enter "<Zip Code>" into the field with label "City/State/ZIP Code" at position "3"
    And  I enter "<Country>" into the field with label "Country" at position "1"
    And  I enter "<Email>" into the field with label "E-Mail" and hit tab
    And  I enter "<Phone Number>" into the field with label "Fax" and hit tab
    And  I enter "<Telephone>" into the field with label "Telephone" and hit tab
    And  I click on "Menu"
    And  I click on "Extras"
    And  I click on "Additional Data"
    And  I wait for iframe with check ok to appear
    And  I switch to the first iFrame in SAP
    And  I enter "<Certificate>" into the field with label "Certificate" and hit tab
    And  I enter "<DropDown Option>" into the field with label "Invoice Copy" and hit tab
    And  I enter "<DropDown Option>" into the field with label "PO Required" and hit tab
    And  I switch back to the main window in SAP
    And  I hit return
    And  I click on "Control Data"
    And  I enter "<Industry>" into the field with label "Industry" and hit tab
    And  I enter "<TaxNumber2>" into the field with label "Tax Number 2" and hit tab
    And  I click on "Contact Person"
    And  I enter "<Name>" into the table at row number "1" and column number "2" at position "1" in commonTcodes
    And  I enter "<Department>" into the table at row number "1" and column number "4" at position "1" in commonTcodes
    And  I enter "<Function>" into the table at row number "1" and column number "6" at position "1" in commonTcodes
    And  I hit return
    And  I click on "Company Code Data"
    And  I enter "<Recon. account>" into the field with label "Recon. account"
    And  I enter "<Cash mgmt group>" into the field with label "Cash mgmt group"
    And  I click on "Menu"
    And  I click on "Extras"
    And  I click on "Classification"
    And  I enter "<Class>" into the table at row number "1" and column number "1" at position "1" in commonTcodes
    And  I hit return
    And I wait for "5" seconds
    And  I enter "01" into the table at row number "1" and column number "2" at position "2" in commonTcodes
    And  I go back
    And  I click on "Payment Transactions"
    And  I enter "<Terms Of Payment>" into the field with label "Terms of payment" and hit tab
    And  I enter "<Credit memo payt term>" into the field with label "Credit memo payt term" and hit tab
    And  I click on "Payment history record"
    And  I click on "Correspondence"
    And  I enter "<Acctg clerk>" into the field with label "Clerk Abbrev." and hit tab
    And  I enter "<Acct at cust.>" into the field with label "Acct at cust." and hit tab
    And  I enter "<Account Statement>" into the field with label "Account Statement" and hit tab
    And  I click on "Sales Area Data"
    And  I enter "<Cust price proc>" into the field with label "Cust.pric.proc." and hit tab
    And  I click on "Shipping"
    And  I enter "<Shipping Conditions>" into the field with label "Shipping Conditions" and hit tab
    And  I click on "Billing Documents"
    And  I wait for "2" seconds
    And  I enter "<Tax Class>" into the table at row number "1" and column number "5" at position "1" in commonTcodes
    And  I click on element with title attribute value "Save"
    Then I verify the success status in SAP
    Examples:
      | Distribution Channel | Division | Name                         | Department | Function | Phone Number | Email                     | DropDown Option | Certificate | Telephone    | House Number | Street            |  City       | State | Zip Code | Account Group   | Country | Industry | TaxNumber2 | Recon. account | Cash mgmt group | Class              | Terms Of Payment | Credit memo payt term | Acctg clerk | Acct at cust.| Account Statement | Cust price proc | Shipping Conditions | Tax Class | Message                                                      |
      | 10                   | 10       | Automation1 User Company INC.| 0002       | 05       | 2200110033   | autouser@discounttire.com | 01              | 02          | 302-235-6548 | 20225        | N scottsdale Road |  Scottsdale | AZ    | 85255    |  DT AR Customer | US      | Z08      | 12345      | 121000         | AR-CUST         | POS_RQ_FIELDS_CUST | 0001             | NT00                  | 01          | 10122        | 1                 | 1               | 01                  | 1         | has been created for company code 1000 sales area 1000 10 10 |

  @3653
  @jenkinsRun
  Scenario Outline: MD_03 - Maintain Customer - Add New Employee - XD01 - (ALM # 3653)
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I cancel existing sessions while logging in
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "XD01" in the command field
    And  I wait for iframe with check ok to appear
    And  I switch to the first iFrame in SAP
    And  I enter "<Account Group>" into the field with label "Account group" and hit tab
    And  I select the input field with label "Distribution Channel" at position "1" and enter "<Distribution Channel>"
    And  I hit tab
    And  I enter "<Division>" into the field with label "Division"
    And  I hit return
    And  I switch back to the main window in SAP
    And  I enter "Mr." into the field with label "Title" and hit tab
    And  I input "Employee Automation" as name
    And  I enter "Employee ID" information on "Search term 1/2" in New Customer Page
    And I enter "<House Number>" into the field with label "House Number/Street" at position "1"
    And I enter "<Street>" into the field with label "House Number/Street" at position "2"
    And I enter "<City>" into the field with label "City/State/ZIP Code" at position "1"
    And I enter "<State>" into the field with label "City/State/ZIP Code" at position "2"
    And I enter "<Zip Code>" into the field with label "City/State/ZIP Code" at position "3"
    And I enter "<Country>" into the field with label "Country" at position "1"
    And  I enter "<Email>" into the field with label "E-Mail" and hit tab
    And  I enter "<Phone Number>" into the field with label "Fax" and hit tab
    And  I enter "<Telephone>" into the field with label "Telephone" and hit tab
    And  I click on "Company Code Data"
    And  I enter "<Recon. account>" into the field with label "Recon. account"
    And  I enter previously saved Emp ID in "Prev.acct no." field
    And  I click on "Payment Transactions"
    And  I enter "<Terms Of Payment>" into the field with label "Terms of payment"
    And  I enter "<Credit memo payt term>" into the field with label "Credit memo payt term"
    And  I click on "Payment history record"
    And  I click on "Sales Area Data"
    And  I click on "Billing Documents"
    And  I enter "<Tax Class>" into the table at row number "1" and column number "5" at position "1" in commonTcodes
    And  I click on element with title attribute value "Save"
    Then I verify the success status in SAP

    Examples:
      | Distribution Channel | Division | Phone Number | Email                     | Telephone    | House Number | Street            |  City       | State | Zip Code | Account Group | Country | Recon. account |  Terms Of Payment | Credit memo payt term | Tax Class | Message                                                      |
      | 10                   | 10       | 2200110033   | autouser@discounttire.com | 302-235-6548 | 20225        | N Scottsdale Road |  Scottsdale | AZ    | 85255    | DT Employee   | US      | 121000         |  0001             | NT00                  | 1         | has been created for company code 1000 sales area 1000 10 10 |