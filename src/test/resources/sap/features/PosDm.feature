@sap
@PosDm
Feature: PosDm

  @aggregationJob
  Scenario Outline: Automation Sap Aggregation Job creation
    When I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode RTL 1>" in the command field
    And  I click on "Get Variant" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I clear text from "Created By" in SAP
    And  I click on "Execute Button In Frame" in posdm page
    And  I switch back to the main window
    And  I switch to the first iFrame in SAP
    And  I double click "QA Aggregation" in posdm
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I confirm the job is complete
    And  I take page screenshot
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I enter t-code "<TCode RTL 2>" in the command field
    And  I click on "Get Variant" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I double click "QA Outbound PR" in posdm
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I confirm the job is complete
    And  I take page screenshot
    And  I switch back to the main window in SAP
    And  I delete all cookies
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I enter t-code "<TCode RTK>" in the command field
    And  I click on "Get Variant" in IDOC page
    And  I switch to the first iFrame in SAP
    And  I double click on "QA INBOUND" in Idoc page
    And  I switch back to the main window in SAP
    And  I execute
    And  I wait for "30" seconds
    Examples:
      | TCode RTL 1   | TCode RTL 2   | TCode RTK  |
      | /n/posdw/pdis | /n/posdw/odis | rwpos_para |

  Scenario Outline: Automation Flow to extract WPUUMS and WPUTAB Idocs for invoices created(Transaction type - 1001)
    When I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header" in posdm page
    And  I enter the Transaction Number generated in legacy system in "Transaction Number" field in Posdm
    And  I click on "Execute" in posdm page
    And  I click on "document flow button" in posdm page
    And  I click on "Document flow outbound" in posdm page
    And  I scroll to following "external document" element in PosDm page
    And  I save the "External document value 1" element text value in PosDm page
    And  I save the "External document value 2" element text value in PosDm page
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Object Key Table>" in PosDm page
    And  I hit return
    And  I enter saved "External document value 1" element value into following element "Aggregator number"
    And  I select "Logical system" and enter "<Client>" in PosDm page
    And  I click on "Multiple selection" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter saved "External document value 2" element value into following element "Single value row"
    And  I click on "Copy button" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I save the "Object key 1" element text value in PosDm page
    And  I save the "Object key 2" element text value in PosDm page
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table>" in PosDm page
    And  I hit return
    And  I enter saved "Object key 1" element value into following element "Aggregator number"
    And  I click on "Multiple selection" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter saved "Object key 2" element value into following element "Single value row"
    And  I click on "Copy button" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on "Not Equal to"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute" in posdm page
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDm page
    And  I save the "Transaction Id 2" element text value in PosDm page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<Table TCode>" in the command field
    And  I select "Table name" and enter "<Outbound/Inbound Idoc Table 2>" in PosDm page
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I click on "Transaction arrow button" in posdm page
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter saved "Transaction Id 2" element value into following element "Single value row SAP"
    And  I click on "Copy button SAP" in posdm page
    And  I switch back to the main window in SAP
    And  I click on "Execute button SAP" in posdm page
    And  I save the "Idoc value 1" element text value in PosDm page
    And  I save the "Idoc value 2" element text value in PosDm page
    And  I enter t-code "<SAPECC TCode 3>" in the command field
    And  I enter "EDIDC" information on "Table SE16N" in Posdm page
    And  I hit return
    And  I click on "Layout" in posdm page
    And  I click on "drop down" in procure to pay page
    And  I switch to the first iFrame in SAP
    And  I click on partial text "QA_IDOCTYPE"
    And  I switch back to the main window in SAP
    And  I enter saved "Idoc value 1" element value into following element "Idoc Input SE16N"
    And  I select the "Execute" button on the Idoc page
    And  I save the Idoc Number and assign it to WPUUMS/WPUTAB key in se16n
    And  I enter t-code "<SAPECC TCode 3>" in the command field
    And  I enter "EDIDC" information on "Table SE16N" in Posdm page
    And  I hit return
    And  I click on "Layout" in posdm page
    And  I click on "drop down" in procure to pay page
    And  I switch to the first iFrame in SAP
    And  I click on partial text "QA_IDOCTYPE"
    And  I switch back to the main window in SAP
    And  I enter saved "Idoc value 2" element value into following element "Idoc Input SE16N"
    And  I select the "Execute" button on the Idoc page
    And  I save the Idoc Number and assign it to WPUUMS/WPUTAB key in se16n

    Examples:
      | TCode 1       | Table TCode | Object Key Table | Outbound/Inbound Idoc Table | Client     | Outbound/Inbound Idoc Table 2 | SAPECC TCode 3 |
      | /n/posdw/mon0 | /n SE16     | /POSDW/AGGRPD    | EDIDS                       | RTLCLNT100 | EDIDS                         | /n SE16N       |

  @eapogrropis
  Scenario Outline: EA Po Goods Reciept
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date from Environment Variables into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter start time and end time from environment variables in posdm
    And  I enter store number from environment variables
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I execute
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    When I save web order from environmental variables to scenario data
    Then I search for hybris order number in posdt and save it to scenario data
    When I expand the first line item and save the purchase order number
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode 2>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on "Post"
    Then I verify the alert is displayed for the article document being posted
    When I save the article document number to scenario data

    Examples:
      | TCode 1       | Transaction Type | TCode 2 |
      | /n/posdw/mon0 | 1014             | /nmigo  |

  @eapogrbopis
  Scenario Outline: EA Po Goods Reciept for BOPIS
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date from Environment Variables into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter BOPIS web order number from environmental variables into field name with label "Transaction Number"
    And  I enter store number from environment variables
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I execute
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    When I save web order from environmental variables to scenario data
    Then I search for hybris order number in posdt and save it to scenario data
    When I expand the first line item and save the purchase order number
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode 2>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on "Post"
    Then I verify the alert is displayed for the article document being posted
    When I save the article document number to scenario data

    Examples:
      | TCode 1       | Transaction Type | TCode 2 |
      | /n/posdw/mon0 | 1015             | /nmigo  |

  @eapogrDirectPo
  Scenario Outline: EA Po Goods Reciept for PO
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode 2>" in the command field
    And  I enter the purchase order number from environment variables in the entry field
    And  I click on "Item OK"
    And  I click on "Post"
    Then I verify the alert is displayed for the article document being posted
    When I save the article document number to scenario data

    Examples:
      | TCode 1       | Transaction Type | TCode 2 |
      | /n/posdw/mon0 | 1015             | /nmigo  |

  @ropisManualWebOrderValidation
  Scenario Outline: Ropis validation for Manual web orders
    When I save web order from environmental variables to scenario data
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttire"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDM"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date from Environment Variables into field name with label "Posting Date"
    And  I click on "Header" in posdm page
    And  I enter start time and end time from environment variables in posdm
    And  I enter store number from environment variables
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I click on "Execute" in posdm page
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    When I search for hybris order number and save it to scenario data
    Then I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date from Environment Variables into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter start time and end time from environment variables in posdm
    And  I enter store number from environment variables
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I execute
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number in posdt and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I make a post call to car item header service with field name "HYBORDRNUM"
    And  I make a post call to car header service for Customer with Email/Customer ID "<Email>"
    Then I verify if "CUSTOMER_ID" field in Order History is empty
    And  I verify if "TRANSSTATUS" field in Order History is empty
    And  I verify if "ORDER_CHANNEL" field in Order History is empty
    And  I verify if "HEADERTRANSSTATUS" field in Order History is empty
    And  I verify if "HEADERORDER_CHANNEL" field in Order History is empty
    And  I verify if "VEHICLEID" field in Order History is empty
    And  I verify if "VEHTRIMID" field in Order History is empty
    And  I verify if "ASEMBLYLTR" field in Order History is empty

    Examples:
      | TCode 1       | Transaction Type | Email   |
      | /n/posdw/mon0 | 1014             | jenkins |

  @dtdManualWebOrderValidation
  Scenario Outline: dtd web order validation for Manual web orders
    When I save web order from environmental variables to scenario data
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttiredirect"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDM"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date from Environment Variables into field name with label "Posting Date"
    And  I click on "Header" in posdm page
    And  I enter start time and end time from environment variables in posdm
    And  I enter store number from environment variables
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I click on "Execute" in posdm page
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    When I search for hybris order number and save it to scenario data
    Then I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date from Environment Variables into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter start time and end time from environment variables in posdm
    And  I enter store number from environment variables
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I execute
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number in posdt and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I make a post call to car item header service with field name "HYBORDRNUM"
    And  I make a post call to car header service for Customer with Email/Customer ID "<Email>"
    Then I verify if "CUSTOMER_ID" field in Order History is empty
    And  I verify if "TRANSSTATUS" field in Order History is empty
    And  I verify if "ORDER_CHANNEL" field in Order History is empty
    And  I verify if "HEADERTRANSSTATUS" field in Order History is empty
    And  I verify if "HEADERORDER_CHANNEL" field in Order History is empty
    And  I verify if "VEHICLEID" field in Order History is empty
    And  I verify if "VEHTRIMID" field in Order History is empty
    And  I verify if "ASEMBLYLTR" field in Order History is empty

    Examples:
      | TCode 1       | Transaction Type | Email   |
      | /n/posdw/mon0 | 1014             | jenkins |

  @bopisManualWebOrderValidation
  Scenario Outline: Bopis validation for Manual web orders
    When I save web order from environmental variables to scenario data
    And  I make a Post call to 'oauth/token' endpoint and capture the token
    Then I request a get response to the order look up url for site id "discounttire"
    When I save Total Price, Article Codes, Quantities, and line item amount from the request body
    And  I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDM"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date from Environment Variables into field name with label "Posting Date"
    And  I click on "Header" in posdm page
    And  I enter BOPIS web order number from environmental variables into field name with label "Transaction Number"
    And  I enter store number from environment variables
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I click on "Execute" in posdm page
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    When I search for hybris order number and save it to scenario data
    Then I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date from Environment Variables into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter BOPIS web order number from environmental variables into field name with label "Transaction Number"
    And  I enter store number from environment variables
    And  I enter "<Transaction Type>" into the field with label "Transaction Type"
    And  I execute
    And  I double click on "Sales Movement"
    Then I verify if tasks with errors exist in posdm/posdt
    And  I search for hybris order number in posdt and save it to scenario data
    And  I verify articles and values in posdm for 1014, 1015 and 1003 transaction types
    When I make a post call to car item header service with field name "HYBORDRNUM"
    And  I make a post call to car header service for Customer with Email/Customer ID "<Email>"
    Then I verify if "CUSTOMER_ID" field in Order History is empty
    And  I verify if "TRANSSTATUS" field in Order History is empty
    And  I verify if "ORDER_CHANNEL" field in Order History is empty
    And  I verify if "HEADERTRANSSTATUS" field in Order History is empty
    And  I verify if "HEADERORDER_CHANNEL" field in Order History is empty
    And  I verify if "VEHICLEID" field in Order History is empty
    And  I verify if "VEHTRIMID" field in Order History is empty
    And  I verify if "ASEMBLYLTR" field in Order History is empty

    Examples:
      | TCode 1       | Transaction Type | Email   |
      | /n/posdw/mon0 | 1015             | jenkins |