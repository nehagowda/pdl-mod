@sap
@productionProcessAutomation
Feature: Production Process Automation

  @prod
  @apPtsRecordPostingProd
  Scenario Outline: AP PTS Record Posting Job in Production
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I wait "60" seconds for element with text of "Dolphin PTS-AP: InfoCenter" to be visible
    And  I select "0ATP" variant
    And  I click on element with title attribute value "My Action List"
    And  I wait "100" seconds for element with text of "Process" to be visible
    And  I click on element with title attribute value "Table selection menu"
    And  I click on "Select All"
    And  I click on "Process"
    Then I Attempt to Post/Resubmit each record

    Examples:
      | TCode       |
      | /n/dol/ap2n |

  @totalRecordsErrorFixProduction
  Scenario Outline: Job for AUTO_TOTAL_FIX variant in posdm
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I select variant "AUTO_TOTAL_FIX"
    And  I execute
    And  I wait for "60" seconds
    And  I switch to the iFrame at Index "1" in SAP
    And  I check, analyze and fix errors for "T_1605" type Total Record Transaction

    Examples:
      | TCode 1       |
      | /n/posdw/mon0 |

  @totalRecordsErrorFixProduction1603/1605
  Scenario Outline: Job for AUTO_TOT_1603 variant in posdm
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I select variant "AUTO_TOT_1603"
    And  I execute
    And  I wait for "60" seconds
    And  I switch to the iFrame at Index "1" in SAP
    And  I check, analyze and fix errors for "T_1603" type Total Record Transaction

    Examples:
      | TCode 1       |
      | /n/posdw/mon0 |

  @totalRecordsErrorFixProduction1603/1605part2
  Scenario Outline: Job for AUT_TOT_1603_2 variant in posdm
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I select variant "AUT_TOT_1603_2"
    And  I execute
    And  I wait for "60" seconds
    And  I switch to the iFrame at Index "1" in SAP
    And  I check, analyze and fix errors for "1603" type Total Record Transaction

    Examples:
      | TCode 1       |
      | /n/posdw/mon0 |

  @totalRecordsErrorFixProduction1602
  Scenario Outline: Job for AUTO_TOT_1602 variant in posdm
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I select variant "AUTO_TOT_1602"
    And  I execute
    And  I wait for "60" seconds
    And  I switch to the iFrame at Index "1" in SAP
    And  I check, analyze and fix errors for 1602 type Total Record Transaction

    Examples:
      | TCode 1       |
      | /n/posdw/mon0 |

  @totalRecordsErrorFixProduction1602_2
  Scenario Outline: Job for AUT_TOT_1602_2 variant in posdm
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I select variant "AUT_TOT_1602_2"
    And  I execute
    And  I wait for "60" seconds
    And  I switch to the iFrame at Index "1" in SAP
    And  I check, analyze and fix errors for 1602 type Total Record Transaction

    Examples:
      | TCode 1       |
      | /n/posdw/mon0 |

  @totalRecordsErrorFixProduction1601
  Scenario Outline: Job for AUTO_TOT_1601 variant in posdm
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I select variant "AUTO_TOT_1601"
    And  I execute
    And  I wait for "60" seconds
    And  I switch to the iFrame at Index "1" in SAP
    And  I check, analyze and fix errors for 1601 type Total Record Transaction

    Examples:
      | TCode 1       |
      | /n/posdw/mon0 |

  @totalRecordsErrorFixProduction1601part2
  Scenario Outline: Job for AUT_TOT_1601_2 variant in posdm
    When I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDM"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I select variant "AUT_TOT_1601_2"
    And  I execute
    And  I wait for "60" seconds
    And  I switch to the iFrame at Index "1" in SAP
    And  I check, analyze and fix errors for 1601 type Total Record Transaction

    Examples:
      | TCode 1       |
      | /n/posdw/mon0 |

  @layawayRefundProdErrorFixVariantBased
  Scenario Outline: Job for LAYAWAY_FIX variant in posdm
    When I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDM"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I select variant "LAYAWAY_FIX"
    And  I execute
    And  I wait for "60" seconds
    And  I switch to the iFrame at Index "1" in SAP
    And  I check, analyze and fix errors for 1009 type layaway transaction

    Examples:
      | TCode 1       |
      | /n/posdw/mon0 |

  @layawayRefundProdErrorFixVariantBased_2
  Scenario Outline: Job for LAYAWAY_FIX_2 variant in posdm
    When I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDM"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I select variant "LAYAWAY_FIX_2"
    And  I execute
    And  I wait for "60" seconds
    And  I switch to the iFrame at Index "1" in SAP
    And  I check, analyze and fix errors for 1009 type layaway transaction

    Examples:
      | TCode 1       |
      | /n/posdw/mon0 |

  @layawayRefundProdErrorFixVariantBased_3
  Scenario Outline: Job for LAYAWAY_FIX_3 variant in posdm
    When I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDM"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I select variant "LAYAWAY_FIX_3"
    And  I execute
    And  I wait for "180" seconds
    And  I switch to the iFrame at Index "1" in SAP
    And  I check, analyze and fix errors for 1009 type layaway transaction
    And  I wait for "1" seconds

    Examples:
      | TCode 1       |
      | /n/posdw/mon0 |


  @layawayRefundProdErrorFixVariantBased_4
  Scenario Outline: Job for LAYAWAY_FIX_4 variant in posdm
    When I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDM"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I select variant "LAYAWAY_FIX_4"
    And  I execute
    And  I wait for "180" seconds
    And  I switch to the iFrame at Index "1" in SAP
    And  I check, analyze and fix errors for 1009 type layaway transaction
    And  I wait for "1" seconds

    Examples:
      | TCode 1       |
      | /n/posdw/mon0 |

  @layawayRefundProdErrorFixVariantBased_5
  Scenario Outline: Job for LAYAWAY_FIX_5 variant in posdm
    When I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDM"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I select variant "LAYAWAY_FIX_5"
    And  I execute
    And  I wait for "180" seconds
    And  I switch to the iFrame at Index "1" in SAP
    And  I check, analyze and fix errors for 1009 type layaway transaction
    And  I wait for "1" seconds

    Examples:
      | TCode 1       |
      | /n/posdw/mon0 |

  @layawayRefundProdErrorFixVariantBased_6
  Scenario Outline: Job for LAYAWAY_FIX_6 variant in posdm
    When I set baseUrl to "SAP POSDM"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDM"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I select variant "LAYAWAY_FIX_6"
    And  I execute
    And  I wait for "180" seconds
    And  I switch to the iFrame at Index "1" in SAP
    And  I check, analyze and fix errors for 1009 type layaway transaction
    And  I wait for "1" seconds

    Examples:
      | TCode 1       |
      | /n/posdw/mon0 |