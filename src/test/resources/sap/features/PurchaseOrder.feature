@sap
@purchaseOrder
Feature: Purchase Order

  @15851
  @poGoodsReceiptValidation
  @poCreationConfirmReceipt
  Scenario Outline: Goods_receipt_validation (ALM#15851)
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<POType>"
    And  I click on "Org. Data"
    And  I select the input field with label "Purch. Group" at position "1" and enter "<PurchaseGroup>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    Then I verify if item Overview is open
    When I enter the PO value for "Article Number" to "<ArticleNumber>", "Quantity" to "<Quantity>", "Site" to "<Site>" from preferred data source
    And  I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on "Post"
    Then I verify the alert is displayed for the article document being posted
    When I save the article document number to scenario data
    And  I enter t-code "<TCode2>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    And  I click on "Purchase Order History"
    Then I verify the article document matches the previously saved number

    Examples:
      | TCode | POType         | Vendor | PurchaseGroup | ArticleNumber | Quantity | Site | TCode1 | TCode2  | Folder Name    | File Name  | Sheet name | row number |
      | ME21N | Store Merch PO | 10226  | 01            | 43195         | 1        | 1434 | /nMIGO | /nME23N | Purchase Order | 15851.xlsx | 15851      | 2          |

  @5078
  @test
  Scenario Outline:SM_01_Desktop MIM Enhancements - Create a Non Merch PO
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>" or get site from column name "Site"
    And  I click on "Ordering" in NWBC page
    And  I click on "Create Non Merchandise Order" in NWBC page
    And  I switch to the first iFrame in SAP
    And  I enter "Back Room Supplies" into the field with label "Merch. Category"
    And  I enter "<VendorNumber>" from preferred data source into the field with label "Vendor"
    And  I enter "<ArticleNumber>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<ArticleQuantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I click on "Save and Print Preview"
    Then I save the "Non Merchant" PO
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    Then I verify Head data tab is open
    And  I verify if item Overview is open
    And  I verify "Account Assignment" field has "K" displayed in NWBC page
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I click on "RECEIVING"
    And  I click on "Post Goods Receipt"
    And  I switch to the first iFrame in SAP
    And  I enter Purchase Order Number into "Post Goods Receipt PO"
    And  I click on "Continue"
    And  I click on "Check delivery complete" in NWBC page
    And  I click on "Save and Print Preview"
    Then I verify the success message with green image

    Examples:
      | VendorNumber | ArticleNumber | ArticleQuantity | TCode | Site  | Folder Name    | File Name   | Sheet name | row number |
      | 11577        | 92998         | 2               | ME23N | Store | Purchase Order | 5078-1.xlsx | 5078       | 2          |

  @5079
  Scenario Outline: Desktop MIM Enhancements - Create  an STO - DTD to Store
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<SiteA>" or get site from column name "SiteA"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I click on "ORDERING"
    And  I click on "Create Store Transfer" in NWBC page
    And  I switch to the first iFrame in SAP
    And  I enter "<Supplying Site>" from preferred data source into the field with label "Supplying Site"
    And  I click on "Header Text"
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on "Purchase Order dropdown" in NWBC page
    And  I switch to "Inner IFrame" Iframe in NWBC
    And  I enter comment "Customer Comment" in Purchase Order popup
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I click on "OK"
    And  I switch back to the main window in SAP
    And  I switch to "Create Store Transfer Iframe" Iframe in NWBC
    And  I enter "<Supplying Site>" from preferred data source into the field with label "Supplying Site"
    And  I enter "<ArticleNumber>" value into column name "Article" at line number "1" in items table in nwbc
    And  I enter "<Quantity>" value into column name "Quantity" at line number "1" in items table in nwbc
    And  I click on "Save and Print Preview"
    And  I save the "Store Transfer PO" PO
    Then I verify the success message with green image
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I enter t-code "<TCode>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    Then I verify if item Overview is open
    When I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<SiteB>" or get site from column name "SiteB"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I click on "GOODS MOVEMENTS"
    And  I click on "Post Goods Issue"
    And  I switch to the first iFrame in SAP
    And  I enter Purchase Order Number into "Post Goods Issue PO"
    And  I click on "Continue"
    And  I click on "Save and Print Preview"
    Then I verify the success message with green image
    When I switch back to the main window
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<SiteA>" or get site from column name "SiteA"
    And  I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I click on "RECEIVING"
    And  I click on "Post Goods Receipt"
    And  I switch to the first iFrame in SAP
    And  I enter Purchase Order Number into "Post Goods Receipt PO"
    And  I click on "Continue"
    And  I click on "Save and Print Preview"
    Then I verify the success message with green image

    Examples:
      | ArticleNumber | Quantity | Supplying Site | TCode | SiteA | SiteB | Folder Name    | File Name | Sheet name | row number |
      | 12345         | 5        | 1022           | ME23N | 1002  | 1022  | Purchase Order | 5079.xlsx | 5079       | 2          |

  @9278
  Scenario Outline: Desktop MIM Enhancements - Create a Store Misc PO
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<POType>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I click on "Org. Data"
    And  I enter "<PurchaseGroup>" into the field with label "Purch. Group"
    Then I verify if item Overview is open
    When I enter the PO value for "Article Number" to "<Article Number>", "Quantity" to "<Quantity>", "Site" to "<Site>" from preferred data source
    And  I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode2>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    And  I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>" or get site from column name "Site"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I click on "RECEIVING"
    And  I click on "Post Goods Receipt"
    And  I switch to the first iFrame in SAP
    And  I enter Purchase Order Number into "Post Goods Receipt PO"
    And  I click on "Continue"
    And  I click on "Save and Print Preview"
    Then I verify the success message with green image

    Examples:
      | TCode    | POType         | Vendor | PurchaseGroup | Article Number | Quantity | Site | TCode2  | Folder Name    | File Name | Sheet name | row number |
      | /n ME21N | Store Merch PO | 10016  | 01            | 17170          | 5        | 1022 | /nme23n | Purchase Order | 9278.xlsx | 9278       | 2          |


  @9279
  Scenario Outline: SM_01_Desktop MIM Enhancements - Create a External Vendor PO
#   This test case wont work in RTD because the search option is not there on the UI
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    When I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<POType>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I enter "<PurchaseGroup>" into the field with label "Purch. Group"
    And  I click on "Org. Data"
    Then I verify if item Overview is open
    When I enter the PO value for "Article Number" to "<ArticleNumber>", "Quantity" to "<Quantity>", "Site" to "<Site>" from preferred data source
    And  I click on "Confirmations"
    And  I enter " " into the field with label "Conf. Control"
    And  I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP
    And  I set baseUrl to "NWBC"
    And  I navigate to the stored Base URL
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I set the site to "<Site>" or get site from column name "Site"
    And  I click on "MIM ADMINISTRATOR" in NWBC page
    And  I click on "Receiving" in NWBC page
    And  I click on "Open Orders" in NWBC page
    And  I click on "Post Goods Receipt"
    And  I switch to the first iFrame in SAP
    And  I enter Purchase Order Number into "Post Goods Receipt PO"
    And  I click on "Continue"
    And  I click on "Save and Print Preview"
    Then I verify the success message with green image

    Examples:
      | TCode | TCode1   | POType        | Vendor | PurchaseGroup | ArticleNumber | Quantity | Site | Folder Name    | File Name | Sheet name | row number |
      | ME21N | /n ME23N | Corp Merch PO | 10016  | 01            | 17173         | 5        | 1022 | Purchase Order | 9279.xlsx | 9279       | 2          |

  @multiplePoCreationConfirmReceipt
  Scenario Outline: Multiple_Purchase_Order_Creation_Receipt_Confirmation (ALM#None)
    When I save "<Folder Name>", "<File Name>", "<Sheet name>" and "<row number>"
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<TCode>" in the command field
    And  I select the PO type "<POType>"
    And  I enter the purchase order value for "Vendor" to "<Vendor>" from preferred data source
    And  I enter "<PurchaseGroup>" into the field with label "Purch. Group"
    Then I verify if item Overview is open
    When I enter the PO value for "Article Number" to "<ArticleNumber>", "Quantity" to "<Quantity>", "Site" to "<Site>" from preferred data source
    And  I hit return
    And  I click on element with title attribute value "Save"
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode1>" in the command field
    And  I verify the dropdown boxes are defaulted to "Goods Receipt" and "Purchase Order"
    And  I enter the purchase order number in the entry field
    And  I click on "Item OK"
    And  I click on "Next Item Button In Migo" in po page
    And  I click on "Item OK"
    And  I click on "Post"
    And  I save the article document number to scenario data
    And  I save the purchase order number to the scenario data
    And  I enter t-code "<TCode2>" in the command field
    And  I click on element with title attribute value "Other Purchase Order"
    And  I search the desired PO number
    And  I hit return
    And  I switch back to the main window in SAP

    Examples:
      | TCode | POType         | Vendor | PurchaseGroup | ArticleNumber | Quantity | Site | TCode1 | TCode2  | Folder Name    | File Name                             | Sheet name | row number |
      | ME21N | Store Merch PO | 10226  | 01            | 43195         | 1        | 1434 | /nMIGO | /nME23N | Purchase Order | multiplePoCreationConfirmReceipt.xlsx | sheet1       | 2          |