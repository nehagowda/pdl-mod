@sap
@financialIdocs
Feature: Financial Idocs

  @WPUBON
  Scenario Outline: Financial Idoc validation - Wpubon
    When I save "<Folder Name>", "<File Name>" and "<Sheet name>"
    And  I load data from jenkins
    And  I extract "invoice" from excel sheet which is in "1"th row and "1"th cell from scenario data
    And  I extract "date" from excel sheet which is in "1"th row and "2"th cell from scenario data
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I click on "Execute"
    Then I verify "Transaction Type" field has the text of "<Transaction Type>" from examples in posdt
    When I take page screenshot
    And  I double click on Transaction Number in posdt
    And  I wait for "5" seconds
    And  I take page screenshot
    And  I go back
    And  I click on "document flow button" in posdm page
    And  I wait for "5" seconds
    And  I click on "Document flow outbound" in posdm page
    And  I click "Vertical Scroll" till "external document" is visible in posdm
    And  I click "Vertical Scroll" "5" times in posdm
    And  I save the "External document value 1" element text value in PosDm page
    And  I enter t-code "<Table TCode>" in the command field
    And  I enter "<Outbound/Inbound Idoc Table>" into the field with label "Table Name"
    And  I hit return
    And  I enter saved "External document value 1" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on element with value of "Not Equal To"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute"
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDt page
    And  I take page screenshot
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<Table TCode>" in the command field
    And  I enter "<Outbound/Inbound Idoc Table 2>" into the field with label "Table Name"
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I execute
    And  I save the "Idoc value 1" element text value for web
    And  I take page screenshot
    And  I enter t-code "<TCode>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I execute
    And  I double click on "Article" in Idoc page
    And  I take page screenshot
    And  I click on element with title attribute value "Back"
    And  I double click on "Billing" in Idoc page
    And  I take page screenshot
    And  I click on partial text "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Views"
    And  I click on "List Output"
    And  I take page screenshot
    And  I wait for "5" seconds

    Examples:
      | TCode   | TCode 1       | Table TCode | Outbound/Inbound Idoc Table | Outbound/Inbound Idoc Table 2 | GL Type     | GL Account | Folder Name    | File Name   | Sheet name |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | STR Account | 119060     | financialIdocs | WPUBON.xlsx | WPUBON     |

  @WPUUMS/WPUTAB
  Scenario Outline: Financial Idoc validation - WPUUMS
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter t-code "<POSDM TCode 1>" in the command field
    And  I click on get variant
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I hit return
    And  I double click on element with value of "QA_OUTBOUND_PR"
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute"
    And  I confirm the job is complete
    And  I take page screenshot
    And  I switch back to the main window in SAP
    And  I delete all cookies
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<SAPECC TCode 2>" in the command field
    And  I wait for "10" seconds
    And  I click on get variant
    And  I switch to the first iFrame in SAP
    And  I double click on "QA_IDOC_PROC"
    And  I switch back to the main window in SAP
    And  I execute
    And  I wait for "10" seconds
    And  I take page screenshot

    Examples:
      | POSDM TCode 1 | SAPECC TCode 2 |
      | /n/posdw/odis | rwpos_para     |

  @WPUUMS/WPUTAB
  Scenario Outline: Financial Idoc validation - WPUUMS
    When I save "<Folder Name>", "<File Name>" and "<Sheet name>"
    And  I load data from jenkins
    And  I extract "invoice" from excel sheet which is in "2"th row and "1"th cell from scenario data
    And  I extract "date" from excel sheet which is in "2"th row and "2"th cell from scenario data
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I click on "Execute"
    Then I verify "Transaction Type" field has the text of "<Transaction Type>" from examples in posdt
    When I take page screenshot
    And  I double click on Transaction Number in posdt
    And  I wait for "5" seconds
    And  I take page screenshot
    And  I go back
    And  I click on "Sales movement row" in posdm page
    And  I click on element with title attribute value "Process Tasks Online "
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Execute Processing "
    And  I click on element with title attribute value "Close Dialog"
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter t-code "<POSDM TCode 1>" in the command field
    And  I click on get variant
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I hit return
    And  I double click on element with value of "QA_OUTBOUND_PR"
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute"
    And  I confirm the job is complete
    And  I take page screenshot
    And  I switch back to the main window in SAP
    And  I delete all cookies
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<SAPECC TCode 2>" in the command field
    And  I wait for "10" seconds
    And  I click on get variant
    And  I switch to the first iFrame in SAP
    And  I double click on "QA_IDOC_PROC"
    And  I switch back to the main window in SAP
    And  I execute
    And  I save the "WPUUMS Row" field's Idoc Number and assign it to "WPUUMS" key
    And  I save the "WPUTAB Row" field's Idoc Number and assign it to "WPUTAB" key
    And  I take page screenshot
    And  I enter t-code "<SAPECC TCode 1>" in the command field
    And  I enter saved "WPUUMS" element value into following element "Idoc Field in WPER"
    And  I execute
    And  I double click on "AGGR Article WPUUMS" in Idoc page
    And  I click on element with title attribute value "Back"
    And  I double click on "AGGR Billing WPUUMS" in Idoc page
    And  I take page screenshot
    When I click on partial text "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Views"
    And  I click on "List Output"
    And  I wait for "5" seconds
    And  I take page screenshot
    And  I enter t-code "<SAPECC TCode 1>" in the command field
    And  I enter saved "WPUTAB" element value into following element "Idoc Field in WPER"
    And  I execute
    And  I double click on "Billing document WPUTAB" in Idoc page
    And  I take page screenshot
    When I click on partial text "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Views"
    And  I click on "List Output"
    And  I wait for "5" seconds
    And  I take page screenshot

    Examples:
      | TCode 1       | POSDM TCode 1 | SAPECC TCode 1 | SAPECC TCode 2 | Folder Name    | File Name   | Sheet name |
      | /n/posdw/mon0 | /n/posdw/odis | /n WPER        | rwpos_para     | financialIdocs | WPUUMS.xlsx | WPUUMS     |

  @WPUTAB/WPUFIB
  Scenario Outline: Financial Idoc validation - WPUFIB - WPUTAB
    When I save "<Folder Name>", "<File Name>" and "<Sheet name>"
    And  I load data from jenkins
    And  I extract "invoice" from excel sheet which is in "2"th row and "1"th cell from scenario data
    And  I extract "date" from excel sheet which is in "2"th row and "2"th cell from scenario data
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I click on "Execute"
    Then I verify "Transaction Type" field has the text of "<Transaction Type>" from examples in posdt
    When I take page screenshot
    And  I double click on Transaction Number in posdt
    And  I wait for "5" seconds
    And  I take page screenshot
    And  I go back
    And  I click on "document flow button" in posdm page
    And  I wait for "5" seconds
    And  I click on "Document flow outbound" in posdm page
    And  I click "Vertical Scroll" till "External document value 2" is visible in posdm
    And  I save the "External document value 1" element text value in PosDm page
    And  I save the "External document value 2" element text value in PosDm page
    And  I take page screenshot
    And  I enter t-code "<Table TCode>" in the command field
    And  I enter "<Outbound/Inbound Idoc Table>" into the field with label "Table Name"
    And  I hit return
    And  I enter saved "External document value 1" element value into following element "Aggregator number"
    And  I click on "Multiple selection" in posdm page
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter saved "External document value 2" element value into following element "Single value row"
    And  I click on element with title attribute value "Copy"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on element with value of "Not Equal To"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute"
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I wait for "10" seconds
    And  I save the "Transaction Id 1" element text value in PosDt page
    And  I save the "Transaction Id 2" element text value in PosDt page
    And  I take page screenshot
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<Table TCode>" in the command field
    And  I enter "<Outbound/Inbound Idoc Table 2>" into the field with label "Table Name"
    And  I hit return
    And  I wait for "10" seconds
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I click on "Transaction arrow button" in posdm page
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter saved "Transaction Id 2" element value into following element "Single value row SAP"
    And  I click on element with title attribute value "Copy"
    And  I switch back to the main window in SAP
    And  I execute
    And  I save the "Idoc value 1" element text value in PosDm page
    And  I save the "Idoc value 2" element text value in PosDm page
    And  I take page screenshot
    And  I enter t-code "<SAPECC TCode 3>" in the command field
    And  I enter "EDIDC" into the field with label "Table"
    And  I hit return
    And  I enter "/QA_IDOCTYPE" into the field with label "Layout"
    And  I enter saved "Idoc value 1" element value into following element "Idoc Input SE16N"
    And  I execute
    And  I save the Idoc Number and assign it to WPUUMS/WPUTAB key in se16n
    And  I enter t-code "<SAPECC TCode 3>" in the command field
    And  I enter "EDIDC" into the field with label "Table"
    And  I hit return
    And  I enter "/QA_IDOCTYPE" into the field with label "Layout"
    And  I enter saved "Idoc value 2" element value into following element "Idoc Input SE16N"
    And  I execute
    And  I save the Idoc Number and assign it to WPUUMS/WPUTAB key in se16n
    And  I enter t-code "<TCode3>" in the command field
    And  I enter saved "WPUTAB" element value into following element "Idoc Field in WPER"
    And  I execute
    And  I double click on "Billing document WPUTAB" in Idoc page
    And  I take page screenshot
    And  I click on partial text "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Views"
    And  I click on "List Output"
    And  I wait for "5" seconds
    And  I take page screenshot
    And  I enter t-code "<TCode4>" in the command field
    And  I enter saved "WPUFIB" element value into following element "Idoc Field in WPER"
    And  I execute
    And  I save the "Bopis G/L Account from WPUFIB" element text value in PosDm page
    And  I take page screenshot
    And  I enter t-code "<TCode5>" in the command field
    And  I wait for "10" seconds
    And  I enter the Bopis G/L Account number into "Document Number" field in fb03
    And  I hit return
    And  I wait for "5" seconds
    And  I take page screenshot

    Examples:
      | TCode 1       | Table TCode | TCode3  | Outbound/Inbound Idoc Table | TCode4  | Outbound/Inbound Idoc Table 2 | SAPECC TCode 3 | TCode5  | Folder Name    | File Name  | Sheet name |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | /n wper | EDIDS                         | /n SE16N       | /n FB03 | financialIdocs | BOPIS.xlsx | BOPIS      |

  @WPUFIB
  Scenario Outline: Financial Validate WPUFIB Idoc's for Transactions
    When I save "<Folder Name>", "<File Name>" and "<Sheet name>"
    And  I load data from jenkins
    And  I extract "invoice" from excel sheet which is in "2"th row and "1"th cell from scenario data
    And  I extract "date" from excel sheet which is in "2"th row and "2"th cell from scenario data
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I click on "Execute"
    Then I verify "Transaction Type" field has the text of "<Transaction Type>" from examples in posdt
    When I take page screenshot
    And  I double click on Transaction Number in posdt
    And  I wait for "5" seconds
    And  I take page screenshot
    And  I go back
    And  I click on "document flow button" in posdm page
    And  I wait for "5" seconds
    And  I click on "Document flow outbound" in posdm page
    And  I click "Vertical Scroll" till "External document value 1" is visible in posdm
    And  I save the "External document value 1" element text value in PosDm page
    And  I take page screenshot
    And  I enter t-code "<Table TCode>" in the command field
    And  I enter "<Outbound/Inbound Idoc Table>" into the field with label "Table Name"
    And  I hit return
    And  I enter saved "External document value 1" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on element with value of "Not Equal To"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute"
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDt page
    And  I take page screenshot
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<Table TCode>" in the command field
    And  I enter "<Outbound/Inbound Idoc Table 2>" into the field with label "Table Name"
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I execute
    And  I save the "Idoc value 1" element text value in PosDm page
    And  I take page screenshot
    And  I enter t-code "<TCode3>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I execute
    And  I wait for "5" seconds
    And  I take page screenshot
    And  I save the "Bopis G/L Account from WPUFIB" element text value in PosDm page
    And  I enter t-code "<TCode5>" in the command field
    And  I enter the Bopis G/L Account number into "Document Number" field in fb03
    And  I hit return
    And  I wait for "5" seconds
    And  I take page screenshot

    Examples:
      | TCode 1       | Table TCode | TCode3  | Outbound/Inbound Idoc Table | Outbound/Inbound Idoc Table 2 | Folder Name    | File Name   | Sheet name | TCode5 |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | EDIDS                         | financialIdocs | WPUFIB.xlsx | WPUFIB     | /nfb03 |

  @WPUTAB
  Scenario Outline: Financial Validate WPUTABIdoc's for Transactions
    When I save "<Folder Name>", "<File Name>" and "<Sheet name>"
    And  I load data from jenkins
    And  I extract "invoice" from excel sheet which is in "2"th row and "1"th cell from scenario data
    And  I extract "date" from excel sheet which is in "2"th row and "2"th cell from scenario data
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I click on "Execute"
    Then I verify "Transaction Type" field has the text of "<Transaction Type>" from examples in posdt
    When I take page screenshot
    And  I double click on Transaction Number in posdt
    And  I wait for "5" seconds
    And  I take page screenshot
    And  I go back
    And  I click on "document flow button" in posdm page
    And  I wait for "5" seconds
    And  I click on "Document flow outbound" in posdm page
    And  I click "Vertical Scroll" till "External document value 1" is visible in posdm
    And  I save the "External document value 1" element text value in PosDm page
    And  I take page screenshot
    And  I enter t-code "<Table TCode>" in the command field
    And  I enter "<Outbound/Inbound Idoc Table>" into the field with label "Table Name"
    And  I hit return
    And  I enter saved "External document value 1" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on element with value of "Not Equal To"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute"
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDt page
    And  I take page screenshot
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<Table TCode>" in the command field
    And  I enter "<Outbound/Inbound Idoc Table 2>" into the field with label "Table Name"
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I execute
    And  I save the "Idoc value 1" element text value in PosDm page
    And  I enter t-code "<TCode3>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I execute
    And  I double click on "Billing document WPUTAB" in Idoc page
    And  I take page screenshot
    And  I click on partial text "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Views"
    And  I click on "List Output"
    And  I wait for "5" seconds
    And  I take page screenshot

    Examples:
      | TCode 1       | Table TCode | TCode3  | Outbound/Inbound Idoc Table | Outbound/Inbound Idoc Table 2 | Folder Name    | File Name   | Sheet name |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | EDIDS                         | financialIdocs | WPUTAB.xlsx | WPUTAB     |

  @salesDistributionWpubon
  @eccIdocs
  Scenario Outline: Validate WPUBON Idoc's for Transactions
    When I save "<Folder Name>", "<File Name>" and "<Sheet name>"
    And  I extract "date" from excel sheet which is in "2"th row and "14"th cell from scenario data
    And  I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from scenario data
    And  I extract all values from excel from sheet name from scenario data
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I click on "Execute"
    And  I click on "document flow button" in posdm page
    And  I wait for "5" seconds
    And  I click on "Document flow outbound" in posdm page
    And  I click "Vertical Scroll" till "external document" is visible in posdm
    And  I click "Vertical Scroll" "5" times in posdm
    And  I save the "External document value 1" element text value in PosDm page
    And  I enter t-code "<Table TCode>" in the command field
    And  I enter "<Outbound/Inbound Idoc Table>" into the field with label "Table Name"
    And  I hit return
    And  I enter saved "External document value 1" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on element with value of "Not Equal To"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute"
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDt page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<Table TCode>" in the command field
    And  I enter "<Outbound/Inbound Idoc Table 2>" into the field with label "Table Name"
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I execute
    And  I save the "Idoc value 1" element text value for web
    And  I enter t-code "<TCode>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I execute
    And  I double click on "Article" in Idoc page
    And  I click on element with title attribute value "Back"
    And  I double click on "Billing" in Idoc page
    Then I verify the Net Values and articles in billing document
    When I click on partial text "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click value or text attribute with value "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Views"
    And  I click on "List Output"
    And  I wait for "5" seconds
    Then I validate "<GL Type>" entered matches original "<GL Account>" in wper

    Examples:
      | TCode   | TCode 1       | Table TCode | Outbound/Inbound Idoc Table | Outbound/Inbound Idoc Table 2 | GL Type     | GL Account | Folder Name       | File Name     | Sheet name        |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | STR Account | 119060     | Cross Application | invoices.xlsx | finallayaway_SPOS |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | STR Account | 119060     | Cross Application | invoices.xlsx | finallayaway_DTD  |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | STR Account | 119060     | Cross Application | invoices.xlsx | vendoradj_SPOS    |
#      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | STR Account | 119060     | Cross Application | invoices.xlsx | dropship_DTD      |
      | /n WPER | /n/posdw/mon0 | /n SE16     | EDIDS                       | EDIDS                         | STR Account | 119060     | Cross Application | invoices.xlsx | splitship_DTD      |


  @WPUTAB
  @eccIdocs
  Scenario Outline: Financial Validate WPUTABIdoc's for Transactions
    When I save "<Folder Name>", "<File Name>" and "<Sheet name>"
    And  I extract "date" from excel sheet which is in "2"th row and "14"th cell from scenario data
    And  I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from scenario data
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I click on "Execute"
    And  I click on "document flow button" in posdm page
    And  I wait for "5" seconds
    And  I click on "Document flow outbound" in posdm page
    And  I click "Vertical Scroll" till "External document value 1" is visible in posdm
    And  I save the "External document value 1" element text value in PosDm page
    And  I enter t-code "<Table TCode>" in the command field
    And  I enter "<Outbound/Inbound Idoc Table>" into the field with label "Table Name"
    And  I hit return
    And  I enter saved "External document value 1" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on element with value of "Not Equal To"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute"
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDt page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<Table TCode>" in the command field
    And  I enter "<Outbound/Inbound Idoc Table 2>" into the field with label "Table Name"
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I execute
    And  I save the "Idoc value 1" element text value for web
    And  I enter t-code "<TCode3>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I execute
    And  I double click on "Billing document WPUTAB" in Idoc page
    And  I click on partial text "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click value or text attribute with value "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Views"
    And  I click on "List Output"
    And  I wait for "5" seconds

    Examples:
      | TCode 1       | Table TCode | TCode3  | Outbound/Inbound Idoc Table | Outbound/Inbound Idoc Table 2 | Folder Name       | File Name     | Sheet name             |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | EDIDS                         | Cross Application | invoices.xlsx | additionallayaway_DTD  |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | EDIDS                         | Cross Application | invoices.xlsx | initiallayaway_DTD     |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | EDIDS                         | Cross Application | invoices.xlsx | initiallayaway_SPOS    |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | EDIDS                         | Cross Application | invoices.xlsx | additionallayaway_SPOS |

  @WPUFIB
  @eccIdocs
  Scenario Outline: Financial Validate WPUFIB Idoc's for Transactions
    When I save "<Folder Name>", "<File Name>" and "<Sheet name>"
    And  I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from scenario data
    And  I extract "date" from excel sheet which is in "2"th row and "14"th cell from scenario data
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I click on "Execute"
    And  I click on "document flow button" in posdm page
    And  I wait for "5" seconds
    And  I click on "Document flow outbound" in posdm page
    And  I click "Vertical Scroll" till "External document value 1" is visible in posdm
    And  I save the "External document value 1" element text value in PosDm page
    And  I enter t-code "<Table TCode>" in the command field
    And  I enter "<Outbound/Inbound Idoc Table>" into the field with label "Table Name"
    And  I hit return
    And  I enter saved "External document value 1" element value into following element "Aggregator number"
    And  I double click on "TID"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I double click on element with value of "Not Equal To"
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute"
    And  I click "Horizontal Scroll" "3" times in posdm
    And  I save the "Transaction Id 1" element text value in PosDt page
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<Table TCode>" in the command field
    And  I enter "<Outbound/Inbound Idoc Table 2>" into the field with label "Table Name"
    And  I hit return
    And  I enter saved "Transaction Id 1" element value into following element "Transaction input field"
    And  I execute
    And  I save the "Idoc value 1" element text value for web
    And  I enter t-code "<TCode3>" in the command field
    And  I enter saved "Idoc value 1" element value into following element "Idoc Field in WPER"
    And  I execute
    And  I wait for "5" seconds
    And  I save the "Bopis G/L Account from WPUFIB" element text value in PosDm page
    And  I enter t-code "<TCode5>" in the command field
    And  I enter the Bopis G/L Account number into "Document Number" field in fb03
    And  I hit return

    Examples:
      | TCode 1       | Table TCode | TCode3  | Outbound/Inbound Idoc Table | Outbound/Inbound Idoc Table 2 | Folder Name       | File Name     | Sheet name   | TCode5 |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | EDIDS                         | Cross Application | invoices.xlsx | cpv_SPOS     | /nfb03 |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | EDIDS                         | Cross Application | invoices.xlsx | crv_SPOS     | /nfb03 |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | EDIDS                         | Cross Application | invoices.xlsx | cpvvoid_SPOS | /nfb03 |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | EDIDS                         | Cross Application | invoices.xlsx | crvvoid_SPOS | /nfb03 |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | EDIDS                         | Cross Application | invoices.xlsx | cpv_DTD      | /nfb03 |
      | /n/posdw/mon0 | /n SE16     | /n wper | EDIDS                       | EDIDS                         | Cross Application | invoices.xlsx | crv_DTD      | /nfb03 |

  @WPUUMS/WPUTAB
  Scenario Outline: Financial Idoc validation - WPUUMS
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter t-code "<POSDM TCode 1>" in the command field
    And  I click on get variant
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I hit return
    And  I double click on element with value of "QA_OUTBOUND_PR"
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute"
    And  I confirm the job is complete
    And  I take page screenshot
    And  I switch back to the main window in SAP
    And  I delete all cookies
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<SAPECC TCode 2>" in the command field
    And  I wait for "10" seconds
    And  I click on get variant
    And  I switch to the first iFrame in SAP
    And  I double click on "QA_IDOC_PROC"
    And  I switch back to the main window in SAP
    And  I execute
    And  I wait for "10" seconds
    And  I take page screenshot

    Examples:
      | POSDM TCode 1 | SAPECC TCode 2 |
      | /n/posdw/odis | rwpos_para     |


  @WPUUMS/WPUTAB
  Scenario Outline: Financial Idoc validation - WPUUMS
    When I save "<Folder Name>", "<File Name>" and "<Sheet name>"
    And  I extract "date" from excel sheet which is in "2"th row and "14"th cell from scenario data
    And  I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from scenario data
    And  I extract all values from excel from sheet name "<Sheet name>"
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode 1>" in the command field
    And  I enter Date into field name with label "Posting Date"
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I click on "Execute"
    And  I click on "Sales movement row" in posdm page
    And  I click on element with title attribute value "Process Tasks Online "
    And  I switch back to the main window in SAP
    And  I switch to the iFrame at Index "0" in SAP
    And  I click on element with title attribute value "Execute Processing "
    And  I click on element with title attribute value "Close Dialog"
    And  I switch back to the main window in SAP
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I switch to the iFrame at Index "0" in SAP
    And  I enter t-code "<POSDM TCode 1>" in the command field
    And  I click on get variant
    And  I switch back to the main window in SAP
    And  I switch to the first iFrame in SAP
    And  I hit return
    And  I double click on element with value of "QA_OUTBOUND_PR"
    And  I switch back to the main window
    And  I switch to the iFrame at Index "1" in SAP
    And  I click on "Execute"
    And  I confirm the job is complete
    And  I switch back to the main window in SAP
    And  I delete all cookies
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I set baseUrl to "SAP ECC"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP ECC"
    And  I enter t-code "<SAPECC TCode 2>" in the command field
    And  I wait for "10" seconds
    And  I click on get variant
    And  I switch to the first iFrame in SAP
    And  I double click on "QA_IDOC_PROC"
    And  I switch back to the main window in SAP
    And  I execute
    And  I save the "WPUUMS Row" field's Idoc Number and assign it to "WPUUMS" key
    And  I save the "WPUTAB Row" field's Idoc Number and assign it to "WPUTAB" key
    And  I enter t-code "<SAPECC TCode 1>" in the command field
    And  I enter saved "WPUUMS" element value into following element "Idoc Field in WPER"
    And  I execute
    And  I double click on "AGGR Article WPUUMS" in Idoc page
    And  I click on element with title attribute value "Back"
    And  I double click on "AGGR Billing WPUUMS" in Idoc page
    And  I verify the Net Values and Articles in billing document for aggregated
    When I click on partial text "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Views"
    And  I click on "List Output"
    And  I wait for "5" seconds
    And  I enter t-code "<SAPECC TCode 1>" in the command field
    And  I enter saved "WPUTAB" element value into following element "Idoc Field in WPER"
    And  I execute
    And  I double click on "Billing document WPUTAB" in Idoc page
    When I click on partial text "Accounting"
    And  I switch to the first iFrame in SAP
    And  I double click on "Accounting document"
    And  I switch back to the main window in SAP
    And  I click on element with title attribute value "Views"
    And  I click on "List Output"
    And  I wait for "5" seconds
    Then I validate "STR Account" entered matches original "119060" in wper

    Examples:
      | POSDM TCode 1 | SAPECC TCode 1 | SAPECC TCode 2 | Folder Name       | File Name     | Sheet name          |
      | /n/posdw/odis | /n WPER        | rwpos_para     | Cross Application | invoices.xlsx | Invoice_SPOS        |
      | /n/posdw/odis | /n WPER        | rwpos_para     | Cross Application | invoices.xlsx | nonvendoradj_SPOS   |
      | /n/posdw/odis | /n WPER        | rwpos_para     | Cross Application | invoices.xlsx | refund_SPOS         |
      | /n/posdw/odis | /n WPER        | rwpos_para     | Cross Application | invoices.xlsx | regularvoid_SPOS    |
      | /n/posdw/odis | /n WPER        | rwpos_para     | Cross Application | invoices.xlsx | newCustInvoice_SPOS |
      | /n/posdw/odis | /n WPER        | rwpos_para     | Cross Application | invoices.xlsx | regularsale_DTD     |
      | /n/posdw/odis | /n WPER        | rwpos_para     | Cross Application | invoices.xlsx | regularsale2_DTD    |
      | /n/posdw/odis | /n WPER        | rwpos_para     | Cross Application | invoices.xlsx | refund_DTD          |
      | /n/posdw/odis | /n WPER        | rwpos_para     | Cross Application | invoices.xlsx | regularvoid_DTD     |

  @posdmPosdtValidationSpos
  Scenario Outline: Transaction validation for transaction types in SPOS
    When I save "<Folder Name>", "<File Name>" and "<Sheet name>"
    And  I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from scenario data
    And  I extract all values from excel from sheet name from scenario data
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame in SAP
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I execute
    Then I verify total transaction amount in posdt from scenario data
    And  I verify "Transaction Type" field has the text of "<Transaction Type>" from examples in posdt
    When I take page screenshot
    When I double click on Transaction Number in posdt
    Then I verify Article Identifier and Sales Price for Line items in sales data in posdt

    Examples:
      | Sheet name             | TCode 1       | Transaction Type | Folder Name       | File Name     |
      | nonvendoradj_SPOS      | /n/posdw/mon0 | 1002             | Cross Application | invoices.xlsx |
      | Invoice_SPOS           | /n/posdw/mon0 | 1001             | Cross Application | invoices.xlsx |
      | refund_SPOS            | /n/posdw/mon0 | 1001             | Cross Application | invoices.xlsx |
      | regularvoid_SPOS       | /n/posdw/mon0 | 1001             | Cross Application | invoices.xlsx |
      | finallayaway_SPOS      | /n/posdw/mon0 | 1202             | Cross Application | invoices.xlsx |
      | newCustInvoice_SPOS    | /n/posdw/mon0 | 1001             | Cross Application | invoices.xlsx |
#      | negative_SPOS          | /n/posdw/mon0 | 1001             | Cross Application | invoices.xlsx |
      | additionallayaway_SPOS | /n/posdw/mon0 | 1008             | Cross Application | invoices.xlsx |
      | initiallayaway_SPOS    | /n/posdw/mon0 | 1007             | Cross Application | invoices.xlsx |


  @posdmPosdtValidationDtd
  Scenario Outline: Transaction validation for transaction types in DTD
    When I save "<Folder Name>", "<File Name>" and "<Sheet name>"
    And  I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from scenario data
    And  I extract all values from excel from sheet name from scenario data
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame in SAP
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I execute
    And  I wait for "20" seconds
    Then I verify total transaction amount in posdt from scenario data
    And  I verify "Transaction Type" field has the text of "<Transaction Type>" from examples in posdt
    When I take page screenshot
    And  I double click on Transaction Number in posdt
    Then I verify Article Identifier and Sales Price for Line items in sales data in posdt

    Examples:
      | Sheet name            | TCode 1       | Transaction Type | Folder Name       | File Name     |
      | splitship_DTD         | /n/posdw/mon0 | 1204             | Cross Application | invoices.xlsx |
      | dropship_DTD          | /n/posdw/mon0 | 1203             | Cross Application | invoices.xlsx |
      | regularsale_DTD       | /n/posdw/mon0 | 1001             | Cross Application | invoices.xlsx |
      | regularsale2_DTD      | /n/posdw/mon0 | 1001             | Cross Application | invoices.xlsx |
      | refund_DTD            | /n/posdw/mon0 | 1001             | Cross Application | invoices.xlsx |
      | regularvoid_DTD       | /n/posdw/mon0 | 1001             | Cross Application | invoices.xlsx |
      | finallayaway_DTD      | /n/posdw/mon0 | 1202             | Cross Application | invoices.xlsx |
      | additionallayaway_DTD | /n/posdw/mon0 | 1008             | Cross Application | invoices.xlsx |
      | initiallayaway_DTD    | /n/posdw/mon0 | 1007             | Cross Application | invoices.xlsx |


  @posdmPosdtValidationSpos
  Scenario Outline: Transaction validation for vouchers in SPOS
    When I save "<Folder Name>", "<File Name>" and "<Sheet name>"
    And I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from scenario data
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame in SAP
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I execute
    Then I verify "Transaction Type" field has the text of "<Transaction Type>" from examples in posdt
    When I take page screenshot

    Examples:
      | Sheet name             | TCode 1       | Transaction Type |Folder Name       | File Name     |
      | cpv_SPOS               | /n/posdw/mon0 | 1302             |Cross Application | invoices.xlsx |
      | crv_SPOS               | /n/posdw/mon0 | 1303             |Cross Application | invoices.xlsx |
      | cpvvoid_SPOS           | /n/posdw/mon0 | 1302             |Cross Application | invoices.xlsx |
      | crvvoid_SPOS           | /n/posdw/mon0 | 1303             |Cross Application | invoices.xlsx |

  @posdmPosdtValidationDtd
  Scenario Outline: Transaction validation for vouchers in DTD
    When I save "<Folder Name>", "<File Name>" and "<Sheet name>"
    And I extract "invoice" from excel sheet which is in "2"th row and "10"th cell from scenario data
    And  I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame in SAP
    And  I enter t-code "<TCode 1>" in the command field
    And  I click on "Header"
    And  I enter the Transaction Number generated in legacy System in "Transaction Number" field in posdt
    And  I execute
    Then I verify "Transaction Type" field has the text of "<Transaction Type>" from examples in posdt
    When I take page screenshot

    Examples:
      | Sheet name | TCode 1       | Transaction Type | Folder Name       | File Name     |
      | cpv_DTD    | /n/posdw/mon0 | 1302             | Cross Application | invoices.xlsx |
      | crv_DTD    | /n/posdw/mon0 | 1303             | Cross Application | invoices.xlsx |