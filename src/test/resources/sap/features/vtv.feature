@sap
@vtv
Feature: VTV

  @vtvDataValidation
  Scenario Outline: VTV Service validation with VIN Input
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode>" in the command field
    And  I enter "ZVTV_VEHICLE" into the field with label "Table Name"
    And  I hit return
    And  I select the input field with the label "VIN" at position "1" and enter "VIN" for "<Scenario>" scenario
    And  I execute
    Then I check the field value of "year" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "Assembly" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "make" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "model" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "vin" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "vehicleId" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "trim" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "trimId" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "ChassisId" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "AssemblyId" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "VehicleColor" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "IsStaggered" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "IsDualRearWheel" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "IsNonOriginalEquipment" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "FrontPsi" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "FrontAssemblyDetails" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "RearPsi" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "RearAssemblyDetails" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
#    And  I check the field value of "VehicleImage" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    When I enter t-code "<TCode>" in the command field
    And  I enter "ZVTV_CUSTOMER" into the field with label "Table Name"
    And  I hit return
    And  I select the input field with the label "VTVCUSTOMERID" at position "1" and enter "VTVCUSTOMERID" for "<Scenario>" scenario
    And  I execute
    Then I check the field value of "FullName" in "ZVTV_CUSTOMER" table for "customerid" for "<Scenario>" scenario
#    And  I check the field value of "Phone" in "ZVTV_CUSTOMER" table for "customerid" for "<Scenario>" scenario
    And  I check the field value of "customerid" in "ZVTV_CUSTOMER" table for "customerid" for "<Scenario>" scenario

    Examples:
      | TCode  | Scenario                                        |
      | /nse16 | create air check activity for existing customer |
      | /nse16 | create service activity partial information     |
      | /nse16 | customer search vin with customer               |
      | /nse16 | customer search with all trim assemblies        |
      | /nse16 | customer search with no trim assemblies         |
      | /nse16 | customer search with trim many assemblies       |
      | /nse16 | vehicle search with image                       |
      | /nse16 | vehicle search with no image                    |
      | /nse16 | vehicle search trims                            |
      | /nse16 | vehicle search staggered                        |
      | /nse16 | vehicle search dually                           |
      | /nse16 | create service activity add service             |
      | /nse16 | create service activity existing customer       |
      | /nse16 | create service activity update service          |

  @vtvDataValidationVehicleId
  Scenario Outline: VTV Service validation with vehicleID Input
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode>" in the command field
    And  I enter "ZVTV_VEHICLE" into the field with label "Table Name"
    And  I hit return
    And  I select the input field with the label "VTVCUSTOMERID" at position "1" and enter "VTVCUSTOMERID" for "<Scenario>" scenario
    And  I select the input field with the label "VEHICLEID" at position "1" and enter "VEHICLEID" for "<Scenario>" scenario
    And  I execute
    Then I check the field value of "year" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "Assembly" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "make" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "model" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "vin" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "vehicleId" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "trim" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "trimId" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "ChassisId" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "AssemblyId" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "VehicleColor" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "IsStaggered" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "IsDualRearWheel" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "IsNonOriginalEquipment" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "FrontPsi" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "FrontAssemblyDetails" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "RearPsi" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "RearAssemblyDetails" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
#    And  I check the field value of "VehicleImage" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    When I enter t-code "<TCode>" in the command field
    And  I enter "ZVTV_CUSTOMER" into the field with label "Table Name"
    And  I hit return
    And  I select the input field with the label "VTVCUSTOMERID" at position "1" and enter "VTVCUSTOMERID" for "<Scenario>" scenario
    And  I execute
    Then I check the field value of "FullName" in "ZVTV_CUSTOMER" table for "customerid" for "<Scenario>" scenario
#    And  I check the field value of "Phone" in "ZVTV_CUSTOMER" table for "customerid" for "<Scenario>" scenario
    And  I check the field value of "customerid" in "ZVTV_CUSTOMER" table for "customerid" for "<Scenario>" scenario

    Examples:
      | TCode  | Scenario                                     |
      | /nse16 | customer search license plate with customer  |
      | /nse16 | vehicle search oe or non oe                  |
      | /nse16 | vehicle search lookup by year,make and model |


  @vtvDataValidationWithoutCustomerValidation
  Scenario Outline: VTV Service validation with VIN Input without customerid
    When I set baseUrl to "SAP POSDT"
    And  I navigate to the stored Base URL
    And  I login with System, Client, Username, and Password to "SAP POSDT"
    And  I switch to the first iFrame
    And  I enter t-code "<TCode>" in the command field
    And  I enter "ZVTV_VEHICLE" into the field with label "Table Name"
    And  I hit return
    And  I select the input field with the label "VIN" at position "1" and enter "VIN" for "<Scenario>" scenario
    And  I execute
    Then I check the field value of "year" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "Assembly" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "make" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "model" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "vin" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "vehicleId" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "trim" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "trimId" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "ChassisId" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "AssemblyId" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "VehicleColor" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "IsStaggered" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "IsDualRearWheel" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "IsNonOriginalEquipment" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "FrontPsi" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "FrontAssemblyDetails" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "RearPsi" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
    And  I check the field value of "RearAssemblyDetails" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario
#    And  I check the field value of "VehicleImage" in "ZVTV_VEHICLE" table for "vin" for "<Scenario>" scenario

    Examples:
      | TCode  | Scenario                                       |
      | /nse16 | create air check activity for new customer     |
      | /nse16 | customer search vin with no customer           |
      | /nse16 | customer search license plate with no customer |