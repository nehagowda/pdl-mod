@hybrisStoryBooks
Feature: Test story book validations

  @web
  @storyBook
  Scenario Outline: Story book validation
    Given I launch the "Storybook" URL
    When  I specify visual test batch id "<Batch Id>" for test name "<Test Name>" for "<Application>"
    Then  I use Eyes to verify the storybook components displayed
    And   I close the connection for visual test

    Examples:
      | Batch Id  | Test Name                | Application |
      | Storybook | UI components validation | Hybris      |
