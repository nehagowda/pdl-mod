@visualsmoke_hybris
@TreadwellVisualRegressionHybris
@at
@dt
@dtd
@hybrisVisualRegression
Feature: Test Applitools integration on fitment panel and results page

  Background:
    Given I change to the default store

  @applitoolsFitmentPanelAndResultsPage
  @web
  @mobile
  Scenario Outline: Search by Vehicle using the Homepage menu(ALM #8864, 8867, 8868, 8790, 8789, 8782)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify the Window "fitmentBoxCarName" for Batch "vehiclePanel", App "VehiclePanel" and Test "Fitment Search"
    When I select a fitment option "tire" and shop for all tires/wheels
    Then I verify the Region "results" for Batch "Page Header", App "Header" and Test "Page Header"

    Examples:
      | Year | Make  | Model  | Trim     | Assembly |
      | 2015 | Honda | Accord | Coupe EX | none     |


  @applitoolsPLPPDPPage
  @web
  Scenario Outline: Search with free text search using the Homepage menu(ALM #8864, 8867, 8868, 8790, 8789, 8782)
    When I do a free text search for "<ItemCode>"
    And  I select "<ProductName>" from the autocomplete dropdown of the search box
    Then I should see product detail page with "<ProductName>"
    Then I verify the Window "PDPPage" for Batch "PDP", App "PDP" and Test "Free Product Search"

    Examples:
      | ItemCode | ProductName   |
      | 17928    | Potenza RE-11 |

  @dt
  @at  
  @web
  @dtd
  @16814
  @16848
  @treadwell
  @applitoolsTreadwell
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Verify treadwell information modal displayed for treadwell data sections on Compare page_standard(ALM#16814,16848)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I specify visual test batch id "<Batch Id>" for test name "<Test Name>" for "<Application>"
    Then I use Eyes to verify the entire Page "treadwell fitment popup" displays
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    And  I select view recommended tires
    And  I select the first "3" results to compare
    And  I click the compare products Compare button
    And  I select "View Ride Rating Description" on treadwell section on "Compare products" page
    Then I use Eyes to verify the Page "View Ride Rating Description popup" displays
    When I close popup modal
    And  I select "View Tire Life & Cost Rating Description" on treadwell section on "Compare products" page
    Then I use Eyes to verify the Page "View Tire Life & Cost Rating Description popup" displays
    When I close popup modal
    And  I select "View Stopping Distance Rating Description" on treadwell section on "Compare products" page
    Then I use Eyes to verify the Page "View Stopping Distance Rating Description popup" displays
    When I close popup modal
    Then I use Eyes to verify the entire Page "compare products treadwell tire test data" displays

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Batch Id                     | Test Name                                                                   | Application      |
      | 2012 | Honda     | Civic    | Coupe DX | none     | Hybris 16814 16848 Regular   | Compare page Verify information modal displayed for treadwell data sections | Hybris Regular   |

  @dt
  @at
  @web
  @dtd
  @16814
  @16848
  @treadwell
  @applitoolsTreadwell
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Verify treadwell information modal displayed for treadwell data sections on Compare page_staggered (ALM#16814,16848)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I specify visual test batch id "<Batch Id>" for test name "<Test Name>" for "<Application>"
    Then I use Eyes to verify the entire Page "treadwell fitment popup" displays
    When I select find tires using Treadwell
    And  I select view recommended tires
    And  I select the first "3" results to compare
    And  I click the compare products Compare button
    And  I select "View Ride Rating Description" on treadwell section on "Compare products" page
    Then I use Eyes to verify the Page "View Ride Rating Description popup" displays
    When I close popup modal
    And  I select "View Tire Life & Cost Rating Description" on treadwell section on "Compare products" page
    Then I use Eyes to verify the Page "View Tire Life & Cost Rating Description popup" displays
    When I close popup modal
    And  I select "View Stopping Distance Rating Description" on treadwell section on "Compare products" page
    Then I use Eyes to verify the Page "View Stopping Distance Rating Description popup" displays
    When I close popup modal
    Then I use Eyes to verify the entire Page "compare products treadwell tire test data" displays

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Batch Id                     | Test Name                                                                   | Application      |
      | 2010 | Chevrolet | Corvette | Base     | none     | Hybris 16814 16848 Staggered | Compare page Verify information modal displayed for treadwell data sections | Hybris Staggered |


  @dt
  @at
  @web
  @dtd
  @16847
  @16850
  @mobile
  @treadwell
  @applitoolsTreadwell
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Verify treadwell tire test data and Winter Rating on PDP_standard (ALM#16847,16850)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I specify visual test batch id "<Batch Id>" for test name "<Test Name>" for "<Application>"
    Then I use Eyes to verify the entire Page "treadwell fitment popup" displays
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    And  I select view recommended tires
    And  I select the "First" product result image on "PLP" page
    And  I select "View Ride Rating Description" on treadwell section on "PDP" page
    Then I use Eyes to verify the Page "View Ride Rating Description popup" displays
    When I close popup modal
    And  I select "View Tire Life & Cost Rating Description" on treadwell section on "PDP" page
    Then I use Eyes to verify the Page "View Tire Life & Cost Rating Description popup" displays
    When I close popup modal
    And  I select "View Stopping Distance Rating Description" on treadwell section on "PDP" page
    Then I use Eyes to verify the Page "View Stopping Distance Rating Description popup" displays
    When I close popup modal
    Then I use Eyes to verify the entire Page "PDP treadwell tire test data and Winter Rating" displays

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Batch Id                     | Test Name                                             | Application          |
      | 2012 | Honda     | Civic    | Coupe DX | none     | Hybris 16847 16850 Regular   | Tire test data and Winter Rating on PDP for Regular   | Hybris PDP Regular   |

  @dt
  @at
  @web
  @dtd
  @16847
  @16850
  @mobile
  @treadwell
  @applitoolsTreadwell
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Verify treadwell tire test data and Winter Rating on PDP(ALM#16847,16850)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I specify visual test batch id "<Batch Id>" for test name "<Test Name>" for "<Application>"
    Then I use Eyes to verify the entire Page "treadwell fitment popup" displays
    When I select find tires using Treadwell
    And  I select view recommended tires
    And  I select the "First" product result image on "PLP" page
    And  I select "View Ride Rating Description" on treadwell section on "PDP" page
    Then I use Eyes to verify the Page "View Ride Rating Description popup" displays
    When I close popup modal
    And  I select "View Tire Life & Cost Rating Description" on treadwell section on "PDP" page
    Then I use Eyes to verify the Page "View Tire Life & Cost Rating Description popup" displays
    When I close popup modal
    And  I select "View Stopping Distance Rating Description" on treadwell section on "PDP" page
    Then I use Eyes to verify the Page "View Stopping Distance Rating Description popup" displays
    When I close popup modal
    Then I use Eyes to verify the entire Page "PDP treadwell tire test data and Winter Rating" displays

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | Batch Id                     | Test Name                                             | Application          |
      | 2010 | Chevrolet | Corvette | Base     | none     | Hybris 16847 16850 Staggered | tire test data and Winter Rating on PDP for Staggered | Hybris PDP Staggered |

  @dt
  @at
  @web
  @dtd
  @7065
  @mobile
  @treadwell
  @applitoolsTreadwell
  Scenario Outline: HYBRIS_SEARCH_SEARCH_PLP - Hide PDL Cost data for articles with MAP restrictions (ALM#7065)
    When I go to the homepage
    And  I search for store within "25" miles of "<Zipcode>"
    And  I select make "<Zipcode>" my store
    And  I click the discount tire logo
    And  I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I select find tires using Treadwell
    And  I specify visual test batch id "<Batch Id>" for test name "<Test Name>" for "<Application>"
    Then I use Eyes to verify the Page "treadwell model" displays
    When I select view recommended tires
    Then I verify if the item with "<ItemCode>" is present on the PLP page
    And  I use Eyes to verify the "<ItemCode>" region of Page "PLP MAP Product" displays
    When I click on the product "<ItemCode>"
    Then I use Eyes to verify the entire Page "PDP MAP Product" displays

    Examples:
      | Zipcode | Year | Make      | Model    | Trim | Assembly | Batch Id                              | Test Name                                             | Application      | ItemCode |
      | 68130   | 2010 | Chevrolet | Corvette | Base | none     | Hybris 7065 MAP Product for staggered | Hide PDL Cost data for articles with MAP restrictions | Hybris Staggered | 14175    |

  @dt
  @at
  @web
  @dtd
  @16883
  @mobile
  @treadwell
  @applitoolsTreadwell
  Scenario Outline: HYBRIS_SEARCH_SEARCH_Verify treadwell Winter Rating not displayed on PDP(ALM#16883)
    When I do a "homepage" vehicle search with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    And  I specify visual test batch id "<Batch Id>" for test name "<Test Name>" for "<Application>"
    Then I use Eyes to verify the Page "treadwell fitment popup" displays
    When I select a fitment option "tire"
    And  I select "Treadwell" option
    And  I set "Primary Driving Location" to "<ZipCode>"
    And  I select view recommended tires
    And  I select the "First" product result image on "PLP" page
    And  I select "View Stopping Distance Rating Description" on treadwell section on "PDP" page
    Then I use Eyes to verify the Page "View Stopping Distance Rating Description popup" displays
    When I close popup modal
    Then I use Eyes to verify the entire Page "Winter Rating not present on PDP" displays

    Examples:
      | Year | Make      | Model    | Trim     | Assembly | ZipCode | Batch Id     | Test Name                          | Application |
      | 2012 | Honda     | Civic    | Coupe DX | none     | 85254   | Hybris 16883 | Winter Rating Not displayed on PDP | Hybris PDP  |

  @dt
  @at
  @web
  @16884
  @mobile
  @applitoolsNexusTax
  Scenario Outline: HYBRIS_UI_UI_Verify Tax Exempt Message throughout the checkout process_DTD (ALM#16884)
    When I do a free text search for "<ItemCodeA>" and hit enter
    And  I add item to my cart and "Continue Shopping"
    And  I do a free text search for "<ItemCodeB>" and hit enter
    And  I add item to my cart and "View shopping Cart"
    And  I specify visual test batch id "<Batch Id>" for test name "<Test Name>" for "<Application>"
    Then I use Eyes to verify the entire Page "Shopping Cart page" displays
    When I select "tax exempt" link
    Then I use Eyes to verify the Page "Tax Policy popup modal" displays
    When I close the tax exempt modal
    And  I select the checkout option "default"
    Then I use Eyes to verify the Page "Checkout Appointment Page" displays
    When I select "tax exempt" link
    Then I use Eyes to verify the Page "Tax Policy popup modal" displays
    When I close the tax exempt modal
    And  I select the checkout without install reason "Make an appointment at a later time"
    And  I click next step for customer information
    Then I use Eyes to verify the Page "Checkout Customer info page" displays
    When I select "tax exempt" link
    Then I use Eyes to verify the Page "Tax Policy popup modal" displays
    When I close the tax exempt modal
    And  I select "Continue To Payment" after entering customer information for "<Customer>"
    And  I select "Pay in Store" payment tab
    And  I click on the "Pay in Store" button
    Then I use Eyes to verify the entire Page "Order Confirmation page" displays
    When I select "tax exempt" link
    Then I use Eyes to verify the Page "Tax Policy popup modal" displays
    When I close the tax exempt modal
    Then I am brought to the order confirmation page

    Examples:
      | ItemCodeA | ItemCodeB | Customer            | Batch Id           | Test Name                       | Application |
      | 29935     | 26899     | default_customer_tx | Hybris Nexus 16884 | Tax Exempt Message on DT and AT | Hybris      |

  @dt
  @at
  @dtd
  @web
  @5861
  @5762
  @5779
  @16845
  @16846
  @applitoolsMyAccount
  @myAccountMyVehiclesTab
  Scenario Outline:HYBRIS_CUSTOMER_MY ACCOUNTS_My Vehicles Tab_Drop old vehicle when max vehicles limit is reached in recent search_
  Clear recent searches_Validate badging and styling_Validate selected vehicle (ALM#5861,5762,16845,16846,5779)
    When I go to the homepage
    And  I select "Join/Sign In" link
    And  I enter "<Email>" in email address field
    And  I enter "<password>" in password field
    And  I click on the "Sign-in" button
    And  I select my account header navigation
    And  I click on the "View My Account" link
    Then I am brought to the page with header "<Header>"
    And  I am brought to the "Profile" tab
    When I select "My Vehicles" tab
    And  I specify visual test batch id "<Batch Id>" for test name "<Test Name>" for "<Application>"
    Then I use Eyes to verify the entire Page "My Vehicles tab with no vehicles in recent search" displays
    When I do a "my vehicles" vehicle search with details "2012" "Honda" "Civic" "Coupe DX" "none"
    And  I close popup modal
    Then I use Eyes to verify the entire Page "My Vehicles tab with One vehicle_TireSize and WheelSize in recent search" displays
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "2012" "Honda" "Civic" "Coupe DX" "none"
    And  I select a fitment option "14"
    And  I select a fitment option "185/65-14"
    And  I select my account header navigation
    And  I click on the "View My Account" link
    And  I select "My Vehicles" tab
    Then I use Eyes to verify the entire Page "My Vehicles tab with Two Vehicles_TireSize and WheelSize in recent search" displays
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "2012" "Honda" "Civic" "Coupe DX" "none"
    And  I select my account header navigation
    And  I click on the "View My Account" link
    And  I select "My Vehicles" tab
    Then I use Eyes to verify the entire Page "My Vehicles tab with Three Vehicles_TireSize and WheelSize in recent search" displays
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "2012" "Honda" "Civic" "Coupe DX" "none"
    And  I "expand" "optional tire and wheel size"
    And  I select a fitment option "<Optional Size>"
    And  I select a fitment option "<Tire/Wheel Size>"
    And  I select my account header navigation
    And  I click on the "View My Account" link
    And  I select "My Vehicles" tab
    Then I use Eyes to verify the entire Page "My Vehicles tab with Three Vehicles_WheelSize in recent search" displays
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "2010" "Chevrolet" "Corvette" "Base" "none"
    And  I close popup modal
    Then I use Eyes to verify the entire Page "My Vehicles tab with Three Vehicles_FT,RT and WheelSize in recent search" displays
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "2010" "Chevrolet" "Corvette" "Base" "none"
    And  I select the "FRONT" staggered menu option
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select my account header navigation
    And  I click on the "View My Account" link
    And  I select "My Vehicles" tab
    Then I use Eyes to verify the entire Page "My Vehicles tab with FT and WheelSize in recent search" displays
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "2010" "Chevrolet" "Corvette" "Base" "none"
    And  I select the "REAR" staggered menu option
    And  I select a fitment option "All rear tires"
    And  I select my account header navigation
    And  I click on the "View My Account" link
    And  I select "My Vehicles" tab
    Then I use Eyes to verify the entire Page "My Vehicles tab with RT and WheelSize in recent search" displays
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "2010" "Chevrolet" "Corvette" "Base" "none"
    And  I select a fitment option "17"
    And  I select my account header navigation
    And  I click on the "View My Account" link
    And  I select "My Vehicles" tab
    Then I use Eyes to verify the entire Page "My Vehicles tab with FrontWheelSize,RearWheelSize in recent search" displays
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "2013" "Arctic" "Cat" "1000 XT" "ATV"
    And  I select a fitment option "wheel"
    And  I select my account header navigation
    And  I click on the "View My Account" link
    And  I select "My Vehicles" tab
    Then I use Eyes to verify the entire Page "My Vehicles tab with FrontTire,WheelSize in recent search" displays
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "2013" "Arctic" "Cat" "1000 XT" "ATV"
    And  I select the "REAR" staggered menu option
    And  I select my account header navigation
    And  I click on the "View My Account" link
    And  I select "My Vehicles" tab
    Then I use Eyes to verify the entire Page "My Vehicles tab with RearTire,WheelSize in recent search" displays
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "2008" "Mercedes-Benz" "SLK55 AMG" "Base" "none"
    And  I select the "Front" staggered menu option
    And  I select a fitment option "wheel"
    And  I select my account header navigation
    And  I click on the "View My Account" link
    And  I select "My Vehicles" tab
    Then I use Eyes to verify the entire Page "My Vehicles tab with FT,WheelSize in recent search" displays
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "2008" "Mercedes-Benz" "SLK55 AMG" "Base" "none"
    And  I select the "Rear" staggered menu option
    And  I select a fitment option "wheel"
    And  I select my account header navigation
    And  I click on the "View My Account" link
    And  I select "My Vehicles" tab
    Then I use Eyes to verify the entire Page "My Vehicles tab with RT,WheelSize in recent search" displays
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "2008" "Mercedes-Benz" "SLK55 AMG" "Base" "none"
    And  I select a fitment option "tire" and shop for all tires/wheels
    And  I select my account header navigation
    And  I click on the "View My Account" link
    And  I select "My Vehicles" tab
    Then I use Eyes to verify the entire Page "My Vehicles tab with FrontWheelSize in recent search" displays
    When I select Add Vehicle
    And  I click on the "Add New Vehicle" button
    And  I do a "homepage" vehicle search with details "2008" "Mercedes-Benz" "SLK55 AMG" "Base" "none"
    And  I select the "Rear" staggered menu option
    And  I select a fitment option "<Rear Optional Size>"
    And  I select a fitment option "<Rear Tire/Wheel Size>"
    And  I select my account header navigation
    And  I click on the "View My Account" link
    And  I select "My Vehicles" tab
    Then I use Eyes to verify the entire Page "My Vehicles tab with RearWheelSize in recent search" displays
    When I click on the "delete" link
    And  I click on the "Yes, delete this vehicle" button
    Then I use Eyes to verify the entire Page "My Vehicles tab with Two Vehicles in recent search" displays
    When I select Clear Recent Searches and "Yes, Clear Recent Searches" option
    Then I use Eyes to verify the entire Page "My Vehicles With No Vehicles" displays

    Examples:
      | Header         | Batch Id                              | Test Name                                  | Application       | Rear Optional Size | Rear Tire/Wheel Size | Optional Size | Tire/Wheel Size | Email                | password  |
      | Hello, QA Test | Hybris 5861,16845,16846,5762,5779 web | Validate Recent Vehicles in My Vehicle Tab | Hybris My Account | 19                 | 235/35-19            | 19            | 235/35-19       | autouser_a@gmail.com | Discount1 |

  @at
  @dt
  @web
  @6228
  @6231
  @6237
  @6959
  @6234
  @6235
  @6616
  @6393
  @6395
  @storeDetailsApplitools
  Scenario Outline: HYBRIS_STORE DETAILS_Verify Store Details Page for My Store(ALM#6228,6231,6237,6959,6234,6235,6616,6393,6395)
    When I click on "My Store" title
    And  I click on Store details button in My Store popup
    And  I specify visual test batch id "<Batch Id>" for test name "<Test Name>" for "<Application>"
    Then I use Eyes to verify the entire Page "Store Details page with First store Image" displays
    When I select Store Details "Next Carousel button" on Store Details page
    Then I use Eyes to verify the entire Page "Store Details page with second store Image" displays
    When I select Store Details "Next Carousel button" on Store Details page
    Then I use Eyes to verify the entire Page "Store Details page with third store Image" displays
    When I select Store Details "Next Carousel indicator" on Store Details page
    Then I use Eyes to verify the entire Page "Store Details page with Store reviews" displays
    When I select Store Details "Store Services" on Store Details page
    Then I use Eyes to verify the entire Page "Store Details with store services" displays

    Examples:
      | Batch Id                                            | Test Name                              | Application          |
      | Hybris 6228 6237 6231 6959 6234 6235 6616 6393 6395 | Verify Store Details Page for My Store | Hybris Store Details |

  @dt
  @web
  @6232
  @6959
  @6616
  @6393
  @6395
  @6366
  @storeDetailsApplitools
  Scenario Outline: HYBRIS_STORE DETAILS_Verify Store Details Page when not on My Store(ALM#6232,6959,6616,6393,6395,6366)
    When I search for store within "75" miles of "<ZipCode>"
    And  I select "<Store>" for store details
    And  I specify visual test batch id "<Batch Id>" for test name "<Test Name>" for "<Application>"
    Then I use Eyes to verify the entire Page "Store Details page with First store Image" displays
    When I select Store Details "Next Carousel indicator" on Store Details page
    Then I use Eyes to verify the entire Page "Store Details page with second store Image" displays
    When I select Store Details "Next Carousel button" on Store Details page
    Then I use Eyes to verify the entire Page "Store Details page with third store Image and Store Reviews" displays
    When I "expand" number "1" nearest Store on store details page
    Then I use Eyes to verify the Page "Store details with additional data for nearest stores " displays
    When I "collapse" number "1" nearest Store on store details page
    Then I use Eyes to verify the Page "Store details collapsed for nearest stores " displays

    Examples:
      | ZipCode | Store  |  Batch Id                            | Test Name                 | Application          |
      | 85255   | AZP 55 | Hybris 6232 6959 6616 6393 6395 6366 | Verify Store Details Page | Hybris Store Details |

  @dt
  @at
  @web
  @6247
  @6246
  @9443
  @storeLocatorApplitools
  Scenario Outline: HYBRIS_Store_Store Locator_City State List Heading (ALM#6247,6246,9443)
    When I open the Store Locator page
    And  I search for store within "100" miles of "<ZipCode>"
    And  I specify visual test batch id "<Batch Id>" for test name "<Test Name>" for "<Application>"
    Then I use Eyes to verify the entire Page "Stores labeled for first page on google map,store badge, distance, Stores located by city collapsed button and not showing shop Tire and Shop Wheels" displays
    When I "expand" "Stores located by city"
    Then I use Eyes to verify the entire Page "Michigan Stores located by City" displays
    When I select "<City>"
    Then I use Eyes to verify the entire Page "Selected Adrian store on map" displays
    When I search for store within "100" miles of "<AzZipCode>"
    Then I use Eyes to verify the entire Page "Stores located by city collapsed button and not displaying Shop Tires and Shop Wheels" displays
    When I "expand" "Stores located by city"
    Then I use Eyes to verify the entire Page "Arizona Stores located by City" displays
    When I "collapse" "Stores located by city"
    Then I use Eyes to verify the entire Page "Searched store list details, Stores located by city collapsed button" displays
    And  I close the connection for visual test

    Examples:
      | ZipCode | AzZipCode | City   | Batch Id      | Test Name                                                                        | Application |
      | 48084   | 86001     | ADRIAN | Store Locator | Store Locator collapsible State and City list, store list results and google map | Hybris      |


  @dtd
  @web
  @8107
  @15205
  @mobile
  @storeLocatorApplitools
  Scenario Outline: HYBRIS_Store_Installer Locator_Tire and Wheel Installers located by city (ALM#8107,15205)
    When I open the "Installers" navigation link
    And  I search for store within "75" miles of "<ZipCode>"
    And  I specify visual test batch id "<Batch Id>" for test name "<Test Name>" for "<Application>"
    Then I use Eyes to verify the entire Page "Installer Locator lable, store actions and installers list" displays
    When I "expand" "Tire and Wheel Installers located by city"
    Then I use Eyes to verify the entire Page "Ohio Tire And Wheel Installers" displays
    When I select "<City>"
    Then I use Eyes to verify the entire Page "Selected avon store on map" displays
    When I search for store within "100" miles of "<AzZipCode>"
    Then I use Eyes to verify the entire Page "Searched installers list details, Tire and Wheel Installers located by city" displays
    And  I "expand" "Tire and Wheel Installers located by city"
    Then I use Eyes to verify the entire Page "Arizona Tire And Wheel Installers label with city list" displays

    Examples:
      | ZipCode | AzZipCode | City | Batch Id          | Test Name                                                | Application |
      | 43054   | 86001     | avon | Installer Locator | Tire and Wheel Installers located by city and google map | Hybris      |