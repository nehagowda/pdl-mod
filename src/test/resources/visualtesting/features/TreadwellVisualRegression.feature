@visualsmoke_pdl
@TreadwellVisualRegression
Feature: Treadwell Visual Tests

  Background:
    Given I go to the pdl homepage

  @vtHomePageTest
  Scenario: vtHomePage (ALM #NONE)
    Then I verify that the "Enter" button is disabled
    When I specify visual test batch id "Treadwell Home Page Set" for test name "Treadwell Home Page tests" for "Treadwell App"
    Then I use Eyes to verify the Page "Treadwell Home Page" displays

  @vtDrivingDetailsTest
  Scenario Outline: vtDrivingDetailsPage (ALM #NONE)
    Then I verify that the "Enter" button is disabled
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    Then I am brought to the page with header "SELECT YOUR VEHICLE" in pdl
    When I specify visual test batch id "Treadwell Driving Details Set" for test name "Treadwell Driving Details tests" for "Treadwell App"
    Then I use Eyes to verify the Page "Treadwell Driving Details Page" displays

    Examples:
      | StoreID | PayrollID |
      | AZP20   | 919919    |

  @vtRecommendations
  Scenario Outline: vtRecommendationsPage (ALM #NONE)
    Then I verify that the "Enter" button is disabled
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    And  I select a vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify that the Tire Size value is "<TireSize>"
    When I select View Tire Results
    And  I specify visual test batch id "Treadwell Recommendation Set" for test name "Treadwell Recommendation tests" for "Treadwell App"
    Then I use Eyes to verify the entire Page "Treadwell Recommendation Page" displays

    Examples:
      | StoreID | PayrollID |  Year | Make  | Model | Trim     | Assembly | TireSize   |
      | AZP20   | 919919    |  2012 | Honda | Civic | Coupe DX | none     | 195/65 R15 |

  @vtDetail
  Scenario Outline: vtDetailPage (ALM #NONE)
    Then I verify that the "Enter" button is disabled
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    And  I select a vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify that the Tire Size value is "<TireSize>"
    When I select View Tire Results
    And  I click on "Add to Cart"
    And  I specify visual test batch id "Treadwell Detail Flow" for test name "Treadwell Details tests" for "Treadwell App"
    Then I use Eyes to verify the Page "Treadwell Add to cart Page" displays
    When I click on "Checkout"
    Then I use Eyes to verify the Page "Treadwell Checkout Page" displays

    Examples:
      | StoreID | PayrollID | Year | Make  | Model | Trim     | Assembly | TireSize   |
      | AZP20   | 919919    | 2012 | Honda | Civic | Coupe DX | none     | 195/65 R15 |

  @vCompare
  Scenario Outline: PDL_RE3-01_RESULTS_Tire Comparison engage (ALM #9841)
    When  I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    Then I am brought to the page with header "SELECT YOUR VEHICLE" in pdl
    When I switch to performance for driving priorities
    Then I verify the "performance" Driving Priorities
    And  I verify the zip code is set to "<ZipCode>"
    And  I verify the city and state are set to "<CityState>"
    When I select a vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify that the Tire Size value is "<TireSize>"
    When I select View Tire Results
    And  I specify visual test batch id "Treadwell Compare Flow" for test name "Treadwell Compare tests" for "Treadwell App"
    Then I use Eyes to verify the Page "Treadwell Recommendation Page" displays
    When I select the product checkbox at position "0" from the products list results
    And  I select the product checkbox at position "3" from the products list results
    Then I use Eyes to verify the entire Page "Treadwell Recommendation with Article Selection Page" displays
    When I select the compare tires button
    Then I use Eyes to verify the entire Page "Treadwell Compare Page" displays

    Examples:
      | StoreID | PayrollID | ZipCode | CityState  | Year | Make  | Model | Trim     | Assembly | TireSize   | ItemId1 | ItemId2 | ItemId3 |
      | MNM29   | 919919    | 55125   | Saint Paul | 2012 | Honda | Civic | Coupe DX | none     | 195/65 R15 | 19600   | 34302   | 29935   |

#  @vtAppStatusTest
#  This shouldn't be part of run until status page issue is resolved
#  Scenario: vtApplicationStatusPage (ALM #NONE)
#    Then I verify that the "Enter" button is disabled
#    And  I go to application status page
#    And  I set baseUrl to "Treadwell Application Status"
#    When I specify visual test batch id "Treadwell Application Status Page Set" for test name "Treadwell Application Status Page tests" for "Treadwell App"
#    Then I use Eyes to verify the Page "Treadwell Application Status Page" displays
#    #And  I close the connection for visual test


  @vtDetail
  Scenario Outline: vtErrorPage (ALM #NONE)
    Then I verify that the "Enter" button is disabled
    When I login with Store ID "<StoreID>" and Payroll ID "<PayrollID>"
    And  I select a vehicle with details "<Year>" "<Make>" "<Model>" "<Trim>" "<Assembly>"
    Then I verify that the Tire Size value is "<TireSize>"
    When I select View Tire Results
    And  I specify visual test batch id "Treadwell Application Tire Result Error Page Set" for test name "Tire Result Error Page Set" for "Treadwell App"
    Then I use Eyes to verify the Page "Treadwell Application Tire Result Error Page" displays

    Examples:
      | StoreID | PayrollID | Year | Make  | Model | Trim     | Assembly | TireSize   |
      | AZP20   | 919919    | 2012 | Honda | Civic | Coupe DX | none     | 195/65 R15 |